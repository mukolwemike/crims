<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Advocate</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Current</th>
                        <th>New</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! $advocate->name !!}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td>Postal Code</td>--}}
                        {{--<td>{!! $data['postal_code'] !!}</td>--}}
                        {{--<td>{!! $advocate->postal_code !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Postal Address</td>--}}
                        {{--<td>{!! $data['postal_address'] !!}</td>--}}
                        {{--<td>{!! $advocate->postal_address !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Street</td>--}}
                        {{--<td>{!! $data['street'] !!}</td>--}}
                        {{--<td>{!! $advocate->street !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Town</td>--}}
                        {{--<td>{!! $data['town'] !!}</td>--}}
                        {{--<td>{!! $advocate->town !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Country</td>--}}
                        {{--<td>{!! \App\Cytonn\Models\Country::find($data['country_id'])->name !!}</td>--}}
                        {{--<td>--}}
                            {{--@if($advocate->country)--}}
                                {{--{!! $advocate->country->name !!}--}}
                            {{--@endif--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Building</td>--}}
                        {{--<td>{!! $data['building'] !!}</td>--}}
                        {{--<td>{!! $advocate->building !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Telephone Office</td>--}}
                        {{--<td>{!! $data['telephone_office'] !!}</td>--}}
                        {{--<td>{!! $advocate->telephone_office !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Telephone Cell</td>--}}
                        {{--<td>{!! $data['telephone_cell'] !!}</td>--}}
                        {{--<td>{!! $advocate->telephone_cell !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>E-mail</td>
                        <td>{!! $data['email'] !!}</td>
                        <td>{!! $advocate->email !!}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>{!! isset($data['address']) ? $data['address'] : '' !!}</td>
                        <td>{!! $advocate->address !!}</td>
                    </tr>
                    <tr>
                        <td>Projects</td>
                        <td>Existing: {!!  isset($currentProjects) ? implode(', ', $currentProjects) : '' !!}</td>
                        <td>Adding: {!! isset($newProjects) ? $newProjects->pluck('name')->implode(', ') : ''!!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>
