<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Cancel Client Top-up Instruction</h4>
        </div>
        <div class="panel-body">
            <h5>Top-up Details</h5>
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($topup->client_id) !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($topup->amount) !!}</td></tr>
                    <tr><td>Agreed Rate</td><td>{!! $topup->agreed_rate !!}%</td></tr>
                    <tr><td>Product</td><td>{!! $topup->product->name !!}</td></tr>
                    <tr><td>Tenor</td><td>{!! $topup->tenor !!} Months</td></tr>
                    @if($topup->user)
                        <tr><td>Filled By</td><td>{!! $topup->user->firstname.' '.$topup->user->lastname !!}</td></tr>
                    @endif
                    <tr><td>Invested</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo(!is_null($topup->investment)) !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>