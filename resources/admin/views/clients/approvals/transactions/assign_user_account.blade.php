<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Assign Access to User Account</h4>
        </div>
        <div class="panel-body">
            <div class="well-lg">
                <h5>User details</h5>
                <table class="table table-hover table-responsive">
                    <tbody>
                        <tr>
                            <td>Username</td><td>{!! $user->username !!}</td>
                        </tr>
                        <tr>
                            <td>Full Name</td><td>{!! $user->firstname.' '.$user->lastname !!}</td>
                        </tr>
                        <tr>
                            <td>Country Code</td>
                            <td>{!! $user->phone_country_code !!}</td>
                        </tr>
                        <tr>
                            <td>Phone Number</td><td>{!! $user->phone !!}</td>
                        </tr>
                        <tr>
                            <td>Authy ID</td><td>{!! $user->authy_id !!}</td>
                        </tr>
                        <tr>
                            <td>Email</td><td>{!! $user->email !!}</td>
                        </tr>


                        @if($signature_to_assign)
                        <tr>
                            <td>Signature File to be Linked</td>
                            <td><a target="_blank" href="/dashboard/documents/{!! $signature_to_assign->document_id !!}">View file</a></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>