<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <div class="detail-group">
                @if($oldJointHolder)
                    <div class="col-md-6">
                        <h4>Old Joint Holder Details</h4>
                        <div class="col-md-12">
                            <p>
                                <b>Name:</b> {!! $oldJointHolder->title ? $oldJointHolder->title->name : '' !!} {!! ucfirst($oldJointHolder->firstname) !!} {!! ucfirst($oldJointHolder->middlename) !!} {!! ucfirst($oldJointHolder->lastname) !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Gender:</b> {!! $oldJointHolder->gender ? $oldJointHolder->gender->abbr : '' !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Date of Birth:</b> {!! \Cytonn\Presenters\DatePresenter::formatDate($oldJointHolder->dob) !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Pin Number:</b> {!! $oldJointHolder->pin_no !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>ID/Passport Number:</b> {!! $oldJointHolder->id_or_passport !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Email:</b> {!! $oldJointHolder->email !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Postal Address:</b> {!! $oldJointHolder->postal_address !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Postal Code:</b> {!! $oldJointHolder->postal_code !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p>
                                <b>Country:</b>
                                @if($oldJointHolder->country){!! $oldJointHolder->country->name !!}@endif
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Office Telephone:</b> {!! $oldJointHolder->telephone_office !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Home Telephone:</b> {!! $oldJointHolder->telephone_home !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Cell Number:</b> {!! $oldJointHolder->telephone_cell !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Residential address:</b> {!! $oldJointHolder->residential_address !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Town:</b> {!! $oldJointHolder->town !!}</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Preferred Method of Contact:</b> {!! $oldJointHolder->methodsOfContact !!}</p>
                        </div>
                    </div>
                @endif
                <div class="col-md-6">
                    <h4>New Joint Holder Details</h4>
                    <div class="col-md-12">
                        <p>
                            <b>Name:</b> {!! $holder->title ? $holder->title->name : '' !!} {!! ucfirst($holder->firstname) !!} {!! ucfirst($holder->middlename) !!} {!! ucfirst($holder->lastname) !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Gender:</b> {!! $holder->gender ? $holder->gender->abbr : '' !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Date of Birth:</b> {!! \Cytonn\Presenters\DatePresenter::formatDate($holder->dob) !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Pin Number:</b> {!! $holder->pin_no !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>ID/Passport Number:</b> {!! $holder->id_or_passport !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Email:</b> {!! $holder->email !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Postal Address: </b>{!! $holder->postal_address !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Postal Code:</b> {!! $holder->postal_code !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Country: </b>{!! $holder->country->name !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Office Telephone:</b> {!! $holder->telephone_office !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Home Telephone:</b> {!! $holder->telephone_home !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Cell Number:</b> {!! $holder->telephone_cell !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Residential address:</b> {!! $holder->residential_address !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Town: </b>{!! $holder->town !!}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Preferred Method of Contact:</b> {!! $holder->methodsOfContact !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>