<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Update Client Signature</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                    <th></th>
                    <th>Before</th>
                    <th>Now</th>
                </thead>
                <tr>
                    <td>Signature Person</td>
                    <td>
                        @if($jointHolder_old)
                            {!! $jointHolder_old->details->present()->fullname !!}
                        @elseif($contactPerson_old)
                            {!! $contactPerson_old->name !!}
                        @elseif($relationshipPerson_old)
                            {!! $relationshipPerson_old->name !!}
                        @elseif(isset($data['name']))
                            {!! $data['name'] !!}
                        @else
                            {!! $client->contact->present()->fullname !!}
                        @endif
                    </td>
                    <td>
                        @if($jointHolder)
                            {!! $jointHolder->details->present()->fullname !!}
                        @elseif($contactPerson)
                            {!! $contactPerson->name !!}
                        @elseif($relationshipPerson)
                            {!! $relationshipPerson->name !!}
                        @elseif(isset($data['name']))
                            {!! $data['name'] !!}
                        @else
                            {!! $client->contact->present()->fullname !!}
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Email Address</td>
                    <td>
                        @if($jointHolder_old)
                            {!! $jointHolder_old->details->email !!}
                        @elseif($contactPerson_old)
                            {!! $contactPerson_old->email !!}
                        @elseif($relationshipPerson_old)
                            {!! $relationshipPerson_old->email !!}
                        @elseif(isset($data['email_address']))
                            {!! $data['email_address'] !!}
                        @else
                            {!! $client->contact->email !!}
                        @endif
                    </td>
                    <td>
                        @if($jointHolder)
                            {!! $jointHolder->details->email !!}
                        @elseif($contactPerson)
                            {!! $contactPerson->email !!}
                        @elseif($relationshipPerson)
                            {!! $relationshipPerson->email !!}
                        @elseif(isset($data['email_address']))
                            {!! $data['email_address'] !!}
                        @else
                            {!! $client->contact->email !!}
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Phone</td>
                    <td>
                        @if($jointHolder_old)
                            {!! $jointHolder_old->details->email !!}
                        @elseif($contactPerson_old)
                            {!! $contactPerson_old->email !!}
                        @elseif($relationshipPerson_old)!!}
                        @elseif(isset($data['email_address']))
                            {!! $data['email_address'] !!}
                        @else
                            {!! $client->contact->email !!}
                        @endif
                    </td>
                    <td>
                        @if($jointHolder)
                        {!! $jointHolder->details->email !!}
                        @elseif($contactPerson)
                        {!! $contactPerson->email !!}
                        @elseif($relationshipPerson)
                        {!! $relationshipPerson->email !!}
                        @elseif(isset($data['email_address']))
                        {!! $data['email_address'] !!}
                        @else
                        {!! $client->contact->email !!}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Signature File</td>
                    <td><a target="_blank" href="/dashboard/documents/{!! $signature_person->document_id !!}">View file</a></td>
                    <td><a target="_blank" href="/dashboard/documents/{!! $data['document_id'] !!}">View file</a></td>
                </tr>

                @if($user)
                    <tr>
                        <td>Client User to be Linked</td>
                        <td>{!! $user->firstname  !!} {!! $user->lastname  !!}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>