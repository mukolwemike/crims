<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            @if(!isset($data['project_id']))
                <h4>Create a new Real Estate Project</h4>
            @else
                <h4>Edit Real estate project: {!! $project($data['project_id'])->name !!}</h4>
            @endif
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $data['name'] !!}</td>
                </tr>
                <tr>
                    <td>Code</td>
                    <td>{!! $data['code'] !!}</td>
                </tr>
                <tr>
                    <td>Reservation Fees</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['reservation_fee']) !!}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{!! $projectType->name  !!}</td>
                </tr>
                <tr>
                    <td>Commission calculations</td>
                    <td>{!! $calculators[$data['commission_calculator']] !!}</td>
                </tr>
                <tr>
                    <td>Custodial Account</td>
                    <td>{!! $custodialAccount->full_name !!}</td>
                </tr>
                <tr>
                    <td>Currency</td>
                    <td>{!! $currency->name !!} ({!! $currency->code !!})</td>
                </tr>
            </table>
        </div>
    </div>
</div>