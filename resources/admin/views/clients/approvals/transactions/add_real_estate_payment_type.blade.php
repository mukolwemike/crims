<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Payment Type Details</h4>
        </div>
        <div class="panel-body">
            @if(isset($data['id']))
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th></th><th>Name</th><th>Slug</th><th>Accrues Interest?</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>From</td>
                        <td>{!! $type->name !!}</td>
                        <td>{!! $type->slug !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($type->accrues_interest) !!}</td>
                    </tr>
                    <tr class="success">
                        <td>To</td>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! $data['slug'] !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['accrues_interest']) !!}</td>
                    </tr>
                    </tbody>
                </table>
            @else
                <h5>Add Payment Type Details</h5>

                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Accrues Interest?</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! str_slug($data['name'], '_') !!}</td>
                        @if(isset($data['accrues_interest']))
                            <td>Yes</td>
                        @else
                            <td>No</td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
