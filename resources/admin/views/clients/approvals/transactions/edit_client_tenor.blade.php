<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Client Tenor</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Combined Interest Re-investment Tenor (current)</td>
                        <td>{!! $client->combine_interest_reinvestment_tenor !!} Months</td>
                    </tr>
                    <tr>
                        <td width="30%">Combined Interest Re-investment Tenor</td>
                        <td>{!! $data['combine_interest_reinvestment_tenor'] !!} Months</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>