<div>
      <div class="panel panel-success">
            <div class="panel-heading">
                  <h4>Edit Share Entity</h4>
            </div>
            <div class="panel-body">
                  <table class="table table-hover table-responsive table-striped">
                        <thead>
                              <tr>
                                    <td></td>
                                    <td>Before</td>
                                    <td>Now</td>
                              </tr>
                        </thead>
                        <tbody>
                              <tr>
                                    <td>Share Entity</td>
                                    <td>{!! $entity->name !!}</td>
                                    <td>{!! $data['name'] !!}</td>
                              </tr>
                              <tr>
                                    <td>Fund Manager</td>
                                    <td>{!! $fundManager($entity->fund_manager_id)->fullname !!}</td>
                                    <td>{!! $fundManager($data['fund_manager_id'])->fullname !!}</td>
                              </tr>
                              <tr>
                                    <td>Custodial Account</td>
                                    <td>{!! $account($entity->account_id)->account_name !!} - {!! $currency($entity->currency_id)->code !!}</td>
                                    <td>{!! $account($data['account_id'])->account_name !!} - {!! $currency($data['currency_id'])->code !!}</td>
                              </tr>
                              {{--<tr>--}}
                                    {{--<td>Maximum Holding</td>--}}
                                    {{--<td>{!! $entity->max_holding !!}</td>--}}
                                    {{--<td>{!! $data['max_holding'] !!}</td>--}}
                              {{--</tr>--}}
                        </tbody>
                  </table>
            </div>
      </div>
</div>