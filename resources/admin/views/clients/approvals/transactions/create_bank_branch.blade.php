<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Bank Branch</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                       <th class="bold" colspan="3">Bank Details</th>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Swift Code</td>
                        <td>Clearing Code</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $bank->name !!}</td>
                        <td>{!! $bank->swift_code !!}</td>
                        <td>{!! $bank->clearing_code !!}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th colspan="2">Branch Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Swift Code</td>
                        <td>{!! $data['swift_code'] !!}</td>
                    </tr>
                    <tr>
                        <td>Branch Code</td>
                        <td>{!! $data['branch_code'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>
