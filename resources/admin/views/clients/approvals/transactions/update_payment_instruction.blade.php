<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Account Cash</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td colspan="2">{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client</td>
                        <td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Custodial Account</td>
                        <td colspan="2">{!! $instruction->custodialTransaction->custodialAccount->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Project</td>
                        <td colspan="2">@if($payment->project) {!! $payment->project->name !!} @endif</td>
                    </tr>
                    <tr>
                        <td>Product</td>
                        <td colspan="2">@if($payment->product) {!! $payment->product->name !!} @endif</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td colspan="2">{!! $instruction->description !!}</td>
                    </tr>
                    @if($instruction->clientBankAccount)
                        <tr>
                            <th>Bank Details</th>
                            <td>Account Name</td>
                            <td>{!! $instruction->clientBankAccount->account_name !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Account Number</th>
                            <td>{!! $instruction->clientBankAccount->account_number !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Bank</th>
                            <td>{!! $instruction->clientBankAccount->bank !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Branch</th>
                            <td>{!! $instruction->clientBankAccount->branch !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Clearing Code</th>
                            <td>{!! $instruction->clientBankAccount->clearing_code !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Swift Code</th>
                            <td>{!! $instruction->clientBankAccount->swift_code !!}</td>
                        </tr>
                    @else
                        <tr>
                            <th>Bank Details</th>
                            <th>Account Name</th>
                            <td>{!! $client->investor_account_name !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Account Number</th>
                            <td>{!! $client->investor_account_number !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Bank</th>
                            <td>{!! $client->investor_bank !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Branch</th>
                            <td>{!! $client->investor_bank_branch !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Clearing Code</th>
                            <td>{!! $client->investor_clearing_code !!}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Swift Code</th>
                            <td>{!! $client->investor_swift_code !!}</td>
                        </tr>
                    @endif
                </tbody>
                <tr>
                    <th></th>
                    <th>New Details</th>
                    <th>Current Details</th>
                </tr>
                <tbody>
                    <tr>
                        <th>First Signatory</th>
                        <td>{!! @$instruction->firstSignatory->full_name !!}</td>
                        <td>{!! $users($data['first_signatory_id'])->full_name !!}</td>
                    </tr>
                    <tr>
                        <th>Second Signatory</th>
                        <td>{!! @$instruction->secondSignatory->full_name !!}</td>
                        <td>{!! $users($data['second_signatory_id'])->full_name !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>