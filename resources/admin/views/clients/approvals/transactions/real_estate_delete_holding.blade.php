<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Delete Unit Holding</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Project</td><td colspan="3">{!! $project->name !!}</td></tr>
                    <tr><td>Unit</td><td colspan="3">{!! $unit->number !!}</td></tr>
                    <tr><td>Size</td><td colspan="3">{!! $unit->size->name !!}</td></tr>
                    <tr><td>Type</td><td colspan="3">{!! $unit->type->name !!}</td></tr>
                    <tr><td>Client Name</td><td colspan="3">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($holding->client_id) !!}</td></tr>
                    <tr><td>Client Code</td><td colspan="3">{!! $holding->client->client_code !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>