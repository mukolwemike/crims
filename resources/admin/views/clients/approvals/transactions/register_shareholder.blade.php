<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>New Shareholder Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Share Holder Type</td>
                    <td>{!! ucfirst($data['shareholder_type']) !!}</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>
                        @if($data['shareholder_type'] == 'individual')
                            {!! $share_holder_title.' '.$data['firstname'].' ' !!} @if(isset($data['middlename'])) {!!  $data['middlename'] !!} @endif {!! ' ' . $data['lastname'] !!}
                        @else
                            {!! $registered_name !!}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Number</td>
                    <td>{!! $data['number'] !!}</td>
                </tr>
                <tr>
                    <td>Entity</td>
                    <td>{!! $shareentity->name !!}</td>
                </tr>
                </tbody>
            </table>
            <br>
            @if(!is_null($duplicate))
                @if($data['new_client'] == 3)
                    <div class="alert alert-danger">
                        <p>The sender has requested that the client be added as a duplicate of;</p>
                    </div>
                @else
                    <div class="alert alert-danger">
                        <p>You will not be able to approve this unless it's added as a duplicate</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($duplicate->clients as $client)
                        <tr>
                            <a href="/dashboard/clients/details/{!! $client->id !!}">
                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                                <td>{!! $client->client_code !!}</td>
                            </a>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if(isset($data['duplicate_reason']))
                    <div class="alert alert-info">
                        <p><strong>Reason:</strong>&nbsp;{!! $data['duplicate_reason'] !!}</p>
                    </div>
                @endif
            @endif

            @if((isset($data['risk_reason'])) && (!is_null($data['risk_reason'])))
                <div class="alert alert-warning" role="alert">
                    <p class="text-warning">The Client details entered belong to a CLIENT whose has been deemed <strong>RISKY</strong>
                    </p>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name/Organization</th>
                        <th>Email</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <a target="_blank" href="/dashboard/client/risky/show/{{$riskyClient->id}}">
                                @if(is_null($riskyClient->firstname)) {!! $riskyClient->organization  !!} @else {!! $riskyClient->firstname. ' '. $riskyClient->lastname !!} @endif
                            </a>
                        </td>
                        <td>{!! $riskyClient->email !!}</td>
                        <td>
                            @if($riskyClient->risky_status_id === 1)<span
                                    class="label label-warning">INVESTIGATING</span>
                            @elseif($riskyClient->risky_status_id === 2)<span class="label label-danger">RISKY</span>
                            @elseif($riskyClient->risky_status_id === 3)<span
                                    class="label label-success">CLEARED</span>@endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br>
                <div class="alert alert-info">
                    <p class="text-info"><strong>Reason for Risky Application:</strong></p>
                    <p class="text-capitalize">{{$data['risk_reason']}}</p>
                </div>
            @endif

        <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal"
                    @if($data['shareholder_type'] == 'individual') data-target="#individual-modal"
                    @else data-target="#corporate-modal" @endif>
                Shareholder Details
            </button>

            @if($holders != null)
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#joint-holders-modal">
                    @if($data['shareholder_type'] == 'individual')
                        View JointHolder Details
                    @else
                        Contact Person Details
                    @endif
                </button>
            @endif

            @if(count($kycDocuments))
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#holder-documents">
                    Shareholder Documents
                </button>
            @endif

            <div class="modal fade" id="holder-documents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Shareholder Documents</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body form-detail">
                                @if($kycDocuments)
                                    <h5>KYC Documents</h5>
                                    <div class="panel panel-success">
                                        <table class="table table-striped table-responsive">
                                            <tbody>
                                            @foreach($kycDocuments as $kyc)
                                                <tr>
                                                    <td> {{ $kyc['kyc_type'] }} </td>
                                                    <td>
                                                        <a href="/dashboard/investments/client-instructions/filled-application-documents/{!! $kyc['document_id'] !!}">
                                                            <i class="el-icon-view"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            @if($data['shareholder_type'] == 'individual')
            <!-- Individual Modal -->
                <div class="modal fade" id="individual-modal" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Shareholder Details</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>Personal Details</span>
                                    </div>

                                    <div class="panel-body form-detail">
                                        <div class="col-md-6">
                                            <label for="">Name</label> {!! $share_holder_title.' '.$data['firstname'].' ' !!} @if(isset($data['middlename'])) {!!  $data['middlename'] !!} @endif {!! ' ' . $data['lastname'] !!}
                                        </div>
                                        @if(isset($data['dob']))
                                            <div class="col-md-6">
                                                <label for="">Date of
                                                    Birth</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['dob']) !!}
                                            </div>
                                        @endif
                                        <div class="col-md-6">
                                            <label for="">Email</label> {!! $data['email'] !!}
                                        </div>
                                        @if(isset($data['telephone_home']))
                                            <div class="col-md-6">
                                                <label for="">Phone</label> {!! $data['telephone_home'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['telephone_office']))
                                            <div class="col-md-6">
                                                <label for="">Telephone Office</label> {!! $data['telephone_office'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['pin_no']))
                                            <div class="col-md-6">
                                                <label for="">Pin
                                                    Number</label> {!! $data['pin_no'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['franchise']))
                                            <div class="col-md-6">
                                                <label for="">Franchise</label> {!! \Cytonn\Presenters\BooleanPresenter::presentIcon($data['franchise'])  !!}
                                            </div>
                                        @endif
                                        @if(isset($data['id_or_passport']))
                                            <div class="col-md-6">
                                                <label for="">ID/Passport
                                                    Number</label> {!! $data['id_or_passport'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['postal_code']))
                                            <div class="col-md-6">
                                                <label for="">Postal
                                                    Code</label> {!! $data['postal_code'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['postal_address']))
                                            <div class="col-md-6">
                                                <label for="">Postal
                                                    Address</label> {!! $data['postal_address'] !!}
                                            </div>
                                        @endif

                                        <div class="col-md-6">
                                            <label for="">Country</label> {!! Cytonn\Presenters\CountryPresenter::present($data['country_id']) !!}
                                        </div>

                                        @if(isset($data['nationality_id']))
                                            <div class="col-md-6">
                                                <label for="">Nationality</label> {!! Cytonn\Presenters\CountryPresenter::present($data['nationality_id']) !!}
                                            </div>
                                        @endif

                                        <div class="col-md-6">
                                            <label for="">Employment Status</label> {!! $employment->name !!}
                                        </div>
                                        @if(isset($data['business_sector']))
                                            <div class="col-md-6">
                                                <label for="">Business
                                                    Sector</label> {!!  $data['business_sector'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['town']))
                                            <div class="col-md-6">
                                                <label for="">Town</label> {!! $data['town'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['street']))
                                            <div class="col-md-6">
                                                <label for="">Street</label> {!! $data['street'] !!}
                                            </div>
                                        @endif

                                        <div class="col-md-6">
                                            <label for="">Method of
                                                Contact</label> {!! \Cytonn\Presenters\InvestmentPresenter::presentMethodOfContact($data['method_of_contact_id']) !!}
                                        </div>

                                        @if($branch->bank)
                                            <div class="col-md-6">
                                                <label for="">Investor Account
                                                    Name</label> {!! $data['investor_account_name'] !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Account
                                                    Number</label> {!! $data['investor_account_number'] !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Bank</label> {!! $branch->bank->name !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Bank Branch</label> {!! $branch->name !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Clearing
                                                    Code</label> {!! $branch->bank->clearing_code !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Swift
                                                    Code</label> {!! $branch->bank->swift_code !!}
                                            </div>
                                        @endif

                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @else
            <!-- Corporate Modal -->
                <div class="modal fade" id="corporate-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Corporate Shareholder Details</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>Corporate Shareholder Details</span>
                                    </div>

                                    <div class="panel-body form-detail">
                                        <div class="col-md-6">
                                            <label for="">Investor Type</label> Corporate
                                        </div>
                                        @if(isset($data['corporate_investor_type_other']))
                                            <div class="col-md-6">
                                                <label for="">Investor Type
                                                    Other</label> {!! $data['corporate_investor_type_other']!!}
                                            </div>
                                        @endif
                                        <div class="col-md-6">
                                            <label for="">Registered Name</label> {!! $data['registered_name'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Trade Name</label> {!! $data['trade_name'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Registered Address</label> {!! $data['registered_address'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Registration
                                                number</label> {!! $data['registration_number'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Email</label> {!! $data['email'] !!}
                                        </div>
                                        @if(isset($data['phone']) )
                                            <div class="col-md-6">
                                                <label for="">Phone</label> {!! $data['phone']!!}
                                            </div>
                                        @endif
                                        @if(isset($data['telephone_office']))
                                            <div class="col-md-6">
                                                <label for="">Telephone
                                                    Office</label> {!! isset($data['telephone_office']) ? $data['telephone_office'] : '' !!}
                                            </div>
                                        @endif

                                        @if(isset($data['telephone_office']))
                                            <div class="col-md-6">
                                                <label for="">Telephone
                                                    Home</label> {!! isset($data['telephone_home']) ? $data['telephone_home'] : '' !!}
                                            </div>
                                        @endif
                                        @if(isset($data['pin_no']))
                                            <div class="col-md-6">
                                                <label for="">Pin
                                                    Number</label> {!!  $data['pin_no'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['postal_code']))
                                            <div class="col-md-6">
                                                <label for="">Postal
                                                    Code</label> {!! $data['postal_code'] !!}
                                            </div>
                                        @endif
                                        @if(isset($data['postal_address']))
                                            <div class="col-md-6">
                                                <label for="">Postal
                                                    Address</label> {!! $data['postal_address'] !!}
                                            </div>
                                        @endif
                                        <div class="col-md-6">
                                            <label for="">Country</label> {!! Cytonn\Presenters\CountryPresenter::present($data['country_id']) !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Nationality</label> {!! Cytonn\Presenters\CountryPresenter::present($data['nationality_id']) !!}
                                        </div>

                                        @if(isset($data['business_sector']))
                                            <div class="col-md-6">
                                                <label for="">Business Sector</label> {!! $data['business_sector'] !!}
                                            </div>
                                        @endif

                                        <div class="col-md-6">
                                            <label for="">Town</label> {!! $data['town'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Street</label> {!! $data['street'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Method of
                                                Contact</label> {!! \Cytonn\Presenters\InvestmentPresenter::presentMethodOfContact($data['method_of_contact_id']) !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Investor Account
                                                Name</label> {!! $data['investor_account_name'] !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Investor Account
                                                Number</label> {!! $data['investor_account_number'] !!}
                                        </div>

                                        @if($branch->bank)
                                            <div class="col-md-6">
                                                <label for="">Investor Bank</label> {!! $branch->bank->name !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Bank Branch</label> {!! $branch->name !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Clearing
                                                    Code</label> {!! $branch->bank->clearing_code !!}
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Investor Swift
                                                    Code</label> {!! $branch->bank->swift_code !!}
                                            </div>
                                        @endif

                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if($holders != null)
            <!-- Individual Modal -->
                <div class="modal fade" id="joint-holders-modal" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">
                                    @if($data['shareholder_type'] == 'individual')
                                        <h4>Joint Holder details</h4>
                                    @else
                                        <h4>Contact Person details</h4>
                                    @endif
                                </h4>
                            </div>
                            @foreach($holders as $holder)
                                <div class="modal-body">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span></span>
                                        </div>

                                        <div class="panel-body form-detail">
                                            @if($data['shareholder_type'] == 'individual')

                                                <div class="col-md-6">
                                                    <label for="">Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentTitle($holder['title_id']) . ' ' . $holder['firstname'] . ' ' !!}
                                                    @if(isset($holder['middlename']))
                                                        {!! $holder['middlename'] !!}
                                                    @endif
                                                    {!!  ' ' . $holder['lastname'] !!}
                                                </div>

                                                @if(isset($holder['dob']))
                                                    <div class="col-md-6">
                                                        <label for="">Date of
                                                            Birth</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($holder['dob']) !!}
                                                    </div>
                                                @endif

                                                <div class="col-md-6">
                                                    <label for="">Email</label> {!! $holder['email'] !!}
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Phone</label> {!! $holder['telephone_cell'] !!}
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Pin Number</label> {!! $holder['pin_no'] !!}
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">ID/Passport
                                                        Number</label> {!! $holder['id_or_passport'] !!}
                                                </div>

                                                @if(isset($holder['postal_code']))
                                                    <div class="col-md-6">
                                                        <label for="">Postal Code</label> {!! $holder['postal_code'] !!}
                                                    </div>
                                                @endif

                                                @if(isset($holder['postal_address']))
                                                    <div class="col-md-6">
                                                        <label for="">Postal
                                                            Address</label> {!! $holder['postal_address'] !!}
                                                    </div>
                                                @endif

                                                @if(isset($holder['residential_address']))
                                                    <div class="col-md-6">
                                                        <label for="">Residential
                                                            Address</label> {!! $holder['residential_address'] !!}
                                                    </div>
                                                @endif

                                                @if(isset($holder['country_id']))
                                                    <div class="col-md-6">
                                                        <label for="">Country</label> {!! Cytonn\Presenters\CountryPresenter::present($holder['country_id']) !!}
                                                    </div>
                                                @endif

                                                @if(isset($holder['nationality_id']))
                                                    <div class="col-md-6">
                                                        <label for="">Nationality</label> {!! Cytonn\Presenters\CountryPresenter::present($holder['nationality_id']) !!}
                                                    </div>
                                                @endif

                                                <div class="clearfix"></div>
                                            @else
                                                <div class="col-md-6">
                                                    <label for=""> Full
                                                        Name</label> {!! $holder['firstname'] . ' '!!} {!! $holder['lastname'] !!}
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Email</label> {!! $holder['email'] !!}
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Phone</label> {!! $holder['phone'] !!}
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Address</label> {!! $holder['address'] !!}
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @else
            @endif
        </div>
    </div>
</div>