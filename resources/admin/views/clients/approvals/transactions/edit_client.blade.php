<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                <tr><th>Item</th><th>Was</th><th>Now</th></tr>
                </thead>
                <tbody>
                <tr><td>Complete?</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($old->complete)  !!}</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($new->complete) !!}</td></tr>

                <tr>
                    <td>Client Code</td>
                    <td>{!! $old->client_code !!}</td>
                    <td>{!! $new->client_code !!}</td>
                </tr>

                <tr>
                    <td>Franchise:  </td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($old->franchise)  !!}</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($new->franchise)  !!}</td>
                </tr>

                @if($old->contact->entityType->name == 'individual')
                    <tr>
                        <td>Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($old->id) !!}</td>
                        <td>{{ $title ? $title->name : '' }} {!! ' '.$new->firstname.' '.$new->middlename.' '.$new->lastname !!}</td>
                    </tr>

                    <tr>
                        <td>Preferred Name</td>
                        <td> @if(is_null($old->preferredname)) <em> null </em> @endif{!! ($old->preferredname) !!}</td>
                        <td> {!! $new->preferredname !!} </td>
                    </tr>

                    <tr>
                        <td>Date of Birth</td>
                        <td>@if(isset($old->dob)){!! \Cytonn\Presenters\DatePresenter::formatDate($old->dob) !!}@endif</td>
                        <td>@if(isset($data['dob'])){!! \Cytonn\Presenters\DatePresenter::formatDate($data['dob']) !!} @endif</td>
                    </tr>

                    <tr>
                        <td>Residence</td>
                        <td>{!! $old->residence !!}</td>
                        <td>{!! $new->residence !!}</td></tr>
                @else
                    <tr>
                        <td>Registered Name</td>
                        <td>{!! $old->contact->corporate_registered_name !!}</td>
                        <td>{!! $new->corporate_registered_name !!}</td>
                    </tr>
                    <tr>
                        <td>Registration Number</td>
                        <td>{!! $old->contact->registration_number !!}</td>
                        <td>{!! $new->registration_number !!}</td>
                    </tr>
                    <tr><td>Trade Name</td>
                        <td>{!! $old->contact->corporate_trade_name !!}</td>
                        <td>{!! $new->corporate_trade_name !!}</td>
                    </tr>
                    <tr>
                        <td>Registered Address</td>
                        <td>{!! $old->contact->registered_address !!}</td>
                        <td>{!! $new->registered_address !!}</td>
                    </tr>
                @endif
                <tr><td>Email</td><td>{!! $old->contact->email !!}</td><td>{!! $new->email !!}</td></tr>
                <tr><td>Contact Phone number</td><td>{!! $old->contact->phone !!}</td><td>{!! $new->contact_phone !!}</td></tr>
                <tr><td>Client Phone number</td><td>{!! $old->phone !!}</td><td>{!! $new->phone !!}</td></tr>
                <tr><td>Home Telephone</td><td>{!! $old->telephone_home !!}</td><td>{!! $new->telephone_home !!}</td></tr>
                <tr><td>Office phone number</td><td>{!! $old->telephone_office !!}</td><td>{!! $new->telephone_office !!}</td></tr>
                <tr><td>Pin Number</td><td>{!! $old->pin_no !!}</td><td>{!! $new->pin_no !!}</td></tr>
                <tr><td>Taxable</td><td>{!! $old->taxable !!}</td><td>{!! $new->taxable !!}</td></tr>
                <tr><td>ID/Passport Number</td><td>{!! $old->id_or_passport !!}</td><td>{!! $new->id_or_passport !!}</td></tr>
                <tr><td>Method of Contact</td><td>{!! $old->method_of_contact_id !!}</td><td>{!! $new->method_of_contact_id !!}</td></tr>
                <tr><td>Business Sector</td><td>{!! $old->business_sector !!}</td><td>{!! $new->business_sector !!}</td></tr>
                <tr><td>Postal Code</td><td>{!! $old->postal_code !!}</td><td>{!! $new->postal_code !!}</td></tr>
                <tr><td>Postal Address</td><td>{!! $old->postal_address !!}</td><td>{!! $new->postal_address !!}</td></tr>
                <tr><td>Country</td><td>{!! $old->present()->getCountry !!}</td><td>{!! $new->present()->getCountry !!}</td></tr>
                <tr><td>Nationality</td><td>{!! $old->present()->getNationality !!}</td><td>{!! $new->present()->getNationality !!}</td></tr>
                <tr><td>Town</td><td>{!! $old->town !!}</td><td>{!! $new->town !!}</td></tr>
                <tr><td>Street</td><td>{!! $old->street !!}</td><td>{!! $new->street !!}</td></tr>
                <tr><td>Account Name or Tag</td><td>{!! $old->account_name !!}</td><td>{!! $new->account_name !!}</td></tr>
                <tr><td>Bank Account Name</td><td>{!! $old->investor_account_name !!}</td><td>{!! $new->investor_account_name  !!}</td></tr>
                <tr><td>Bank Account Number</td><td>{!! $old->investor_account_number !!}</td><td>{!! $new->investor_account_number !!}</td></tr>
                <tr><td>Bank Name</td><td>{!! $old->investor_bank !!}</td><td>{!! $new->investor_bank !!}</td></tr>
                <tr><td>Branch</td><td>{!! $old->investor_bank_branch !!}</td><td>{!! $new->investor_bank_branch !!}</td></tr>
                <tr><td>Clearing Code</td><td>{!! $old->investor_clearing_code !!}</td><td>{!! $new->investor_clearing_code !!}</td></tr>
                <tr><td>Swift Code</td><td>{!! $old->investor_swift_code !!}</td><td>{!! $new->investor_swift_code  !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>