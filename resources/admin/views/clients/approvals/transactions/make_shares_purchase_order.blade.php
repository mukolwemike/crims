<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Make Shares Purchase Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Buyer</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($buyer->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $buyer->number !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td>{!! $buyer->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['request_date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! $data['number'] !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price']) !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Recipient</td>
                        <td>{!! $recipient->name !!}</td>
                    </tr>
                    <tr>
                        <td>Current Commission Recipient Position</td>
                        <td>{!! $recipient->type->name !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Rate</td>
                        <td>{!! $data['commission_rate'] !!}%</td>
                    </tr>
                    @if(isset($data['commission_rate_name']))
                        <tr>
                            <td>Commission Rate Name</td>
                            <td>{!! $data['commission_rate_name'] !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Previous Commission Recipient Position</td>
                        <td>{!! $recipient->previous ? $recipient->previous->name : '' !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price'] * $data['number']) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Current Funds</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($buyer->sharePaymentsBalance(\Carbon\Carbon::parse($data['request_date']))) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['good_till_filled_cancelled'])  !!}</td>
                    </tr>
                    @if(!$data['good_till_filled_cancelled'])
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['expiry_date']) !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>