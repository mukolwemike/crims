<div>
      <div class="panel panel-success">
            <div class="panel-heading">
                  <h4>Add Share Entity</h4>
            </div>
            <div class="panel-body">
                  <table class="table table-hover table-responsive table-striped">
                        <thead></thead>
                        <tbody>
                              <tr><td>Share Entity</td><td>{!! $data['name'] !!}</td></tr>
                              <tr><td>Fund Manager</td><td>{!! $fundManager->fullname !!}</td></tr>
                              <tr><td>Custodial Account</td><td>{!! $account->account_name !!} - {!! $currency->code !!}</td></tr>
                              {{--<tr><td>Maximum Holding</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['max_holding'], true, 0) !!} shares</td></tr>--}}
                        </tbody>
                  </table>
            </div>
      </div>
</div>