<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Bank Details</h4>
        </div>
        <div class="panel-body">
            <h5>Delete Bank Details</h5>

            <table class="table table-responsive table-striped">
                <thead>
                    <tr><th>Account Name</th><th>AccountNumber</th><th>Bank</th><th>Branch</th><th>Clearing Code</th><th>Swift Code</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $account->account_name !!}</td>
                        <td>{!! $account->account_number !!}</td>
                        @if($branch)
                            <td>{!! $bank->name !!}</td>
                            <td>{!! $branch->name !!}</td>
                            <td>{!! $bank->clearing_code !!}</td>
                            <td>{!! $bank->swift_code !!}</td>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>