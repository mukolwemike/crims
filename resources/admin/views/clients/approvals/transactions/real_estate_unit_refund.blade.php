<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Unit Refund</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <td>Project</td>
                            <td>{!! $project->name !!}</td>
                        </tr>

                        <tr>
                            <td>Unit</td>
                            <td>{!! $holding->unit->number !!}</td>
                        </tr>

                        <tr>
                            <td>Unit Type</td>
                            <td>{!! $holding->unit->size->name.' '.$holding->unit->type->name !!}</td>
                        </tr>

                        <tr>
                            <td>Refund Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                        </tr>

                        <tr>
                            <td>Refund Amount</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                        </tr>

                        <tr>
                            <td>Total Paid</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->totalPayments()) !!}</td>
                        </tr>

                        <tr>
                            <td>Narration</td>
                            <td>{!! $data['narration'] !!}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>