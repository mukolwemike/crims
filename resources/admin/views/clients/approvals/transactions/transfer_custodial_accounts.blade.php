<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transfer Custodial Accounts</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Client Name</th>
                    <th>Account From</th>
                    <th>Amount</th>
                    <th>Fa</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($custodial_from as $acc)
                    <tr>
                        <td></td>
                        <td>{{$acc['full_name']}}</td>
                        <td>{{$acc['account']}}</td>
                        <td>{{$acc['amount']}}</td>
                        <td>{{$acc['fa']}}</td>
                        <td>{{$acc['date']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transfer Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-responsive">
                <tbody>
                <tr>
                    <td>Destination Account</td>
                    <td>{{$custodial_to->account_name}}</td>
                </tr>

                <tr>
                    <td>Exchange Rate</td>
                    <td>{{$data['exchange_rate']}} %</td>
                </tr>

                <tr>
                    <td>Date of Transfer</td>
                    <td>{{$transfer_date}}</td>
                </tr>

                <tr>
                    <td>Transfer Amount</td>
                    <td>{{$transfer_amount}}</td>
                </tr>

                <tr>
                    <td>Narrative</td>
                    <td>{{$data['narrative']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
