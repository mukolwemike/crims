<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Delete Commission Clawback</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="100%" style="text-align: center;">Investment Details</th>
                </tr>
                <tr>
                    <td>Client</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td><td>{!! $investment->client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Principal</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                </tr>
                <tr>
                    <td>Interest Rate</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td>
                </tr>
                <tr>
                    <td>Invested Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
                <tr>
                    <td>Commission Recipient</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->commission->recipient->name) !!}</td>
                </tr>
                <tr>
                    <th colspan="100%" style="text-align: center;">Commission Clawback (To Be Deleted)</th>
                </tr>
                <tr>
                    <td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clawback->date) !!}</td>
                </tr>
                <tr>
                    <td>Commission</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($clawback->amount) !!}</td>
                </tr>
                <tr>
                    <td>Narration</td><td>{!! $clawback->narration !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>