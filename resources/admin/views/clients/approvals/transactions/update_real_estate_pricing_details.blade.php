<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Pricing Information</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr><td>Project</td><td>{!! $holding->project->name !!}</td></tr>
                    <tr><td>Unit</td><td>{!! $holding->unit->number !!}</td></tr>
                    <tr><td>Tranche</td><td>{!! @$realestateUnitTranche->name !!}</td></tr>
                    <tr><td>Payment Plan</td><td>{!! @$realestatePaymentPlan->name !!}</td></tr>
                    <tr><td>FA</td><td>{!! @$commissionRecepient->name !!}</td></tr>
                    <tr><td>Current FA Position</td><td>{!! @$commissionRecepient->type->name !!}</td></tr>
                    @if(isset($data['negotiated_price']))
                        @if($data['negotiated_price'])
                            <tr><td>Negotiated price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['negotiated_price']) !!}</td></tr>
                        @endif
                    @endif
                    @if(isset($data['discount']))
                        @if($data['discount'] > 0)
                            <tr><td>Discount</td><td>{!! $data['discount'] !!}%</td></tr>
                        @endif
                    @endif
                    <tr><td>Award Commission</td>
                        <td>
                            @if($data['awarded'] == 0) NO
                            @elseif($data['awarded'] == 1) YES
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if($data['awarded'] == 1 && isset($data['commission_rate_id']))
                            <td>Commission Rate</td><td>{!! $realestateCommissionRate($data['commission_rate_id'])->recipient_type_and_amount !!}</td>
                        @endif
                    </tr>
                    <tr>
                        @if($data['awarded'] == 1 && isset($data['rate']))
                            <td>Commission Rate Percentage</td><td>{!! $data['rate'] !!}%</td>
                        @endif
                    </tr>
                    <tr>
                        @if($data['awarded'] == 1 && isset($data['commission_rate_name']))
                            <td>Commission Rate Name</td><td>{!! $data['commission_rate_name'] !!}</td>
                        @endif
                    </tr>
                    <tr>
                        @if($data['awarded'] == 1 && isset($data['reason']))
                            <td>Commission Reason</td><td>{!! $data['reason'] !!}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>