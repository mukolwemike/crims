<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Project Tranche</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <label>Name</label> {!! $data['name'] !!}<br/>
                <label>Date </label> {!! $data['date'] !!}<br/>
                <label>Project </label> {{ $project($data['project_id'])->name }}<br/>
                <label for="">Action</label> @if(isset($data['tranche_id'])) Editing @else Creating @endif
            </div>
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>Size</th><th>Number Available</th><th>Floor</th><th>Type</th><th>Payment Plan</th><th>Price</th><th>Value</th>
                </tr>
                </thead>
                <tbody>
                {{--{!! dd($data) !!}--}}
                @foreach($data['tranches']  as $tranche)
                    <tr>
                        <td>
                            @if(isset($tranche['size_id']))
                                {{ $realestateunitsize($tranche['size_id'])->name }}
                            @endif
                        </td>
                        <td>
                            @if(isset($tranche['number']))
                                {!! $tranche['number'] !!}
                            @endif
                        </td>
                        <td>
                            @if(isset($tranche['floor_id']))
                                {!! $floor($tranche['floor_id'])->floor !!}
                            @endif
                        </td>
                        <td>
                            @if(isset($tranche['type_id']))
                                {!! $type($tranche['type_id'])->name !!}
                            @endif
                        </td>
                        <td></td><td></td><td></td>
                        @foreach($tranche['prices'] as $pricing)
                            <tr>
                                <td></td><td></td><td></td><td></td>
                                <td>{{ $paymentplan($pricing['payment_plan_id'])->name }}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($pricing['price']) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($pricing['value']) !!}</td>
                            </tr>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>