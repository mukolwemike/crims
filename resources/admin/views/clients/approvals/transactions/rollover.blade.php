<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr><td>Product</td><td>{!! $clientinvestment->product->name!!}</td></tr>
                <tr><td>Old Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientinvestment->maturity_date) !!}</td></tr>
                <tr><td>Investment Principal</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($clientinvestment->amount)!!}</td></tr>
                <tr><td>Investment value (today)</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($value_today)!!}</td></tr>
                <tr><td>Investment value on maturity</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($final_value)!!}</td></tr>
                @if(isset($data['schedule']))
                    <tr><td>Value on maturity</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment()) !!}</td></tr>
                    <tr class="alert alert-success"><td>Scheduled</td><td>Yes</td></tr>
                @endif
                @if(isset($data['automatic_rollover']))
                    <tr><td>Automatic Rollover</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['automatic_rollover']) !!}</td></tr>
                @endif
                @if($scheduled)
                    <tr class="warning"><td>Scheduled to run on</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($action_date) !!}</td></tr>
                @endif
                <tr class="success"><td>Reinvest</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($reinvest) !!}</td></tr>
                <tr class="danger"><td>Withdraw</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($withdraw) !!}</td></tr>
                <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td></tr>
                <tr><td>New Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td></tr>
                <tr><td>Total Days</td><td>{!! \Cytonn\Presenters\DatePresenter::getDateDifference($data['invested_date'], $data['maturity_date']) !!}</td></tr>
                <tr><td>FA</td><td>{!! $commissionrecepient->name !!}</td></tr>
                <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                @if(isset($data['commission_start_date']) && $data['commission_start_date'])
                    <tr><td>Commission Start Date</td><td>{!! $data['commission_start_date'] !!}</td></tr>
                @endif
                <tr><td>Interest payment</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                @if(isset($data['interest_action_id']))
                    <tr><td>Interest Action</td><td>{!! $interestaction($data['interest_action_id'])->name  !!}</td></tr>
                @endif
                @if(isset($data['interest_reinvest_tenor']))
                    <tr>
                        <td>Interest Reinvest Tenor</td>
                        <td>
                            @if($data['interest_reinvest_tenor'] != 0)
                                {!! $data['interest_reinvest_tenor'] !!} months
                            @else
                                Until maturity of principal
                            @endif
                        </td>
                    </tr>
                @endif
                @if(isset($data['description']))
                    <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                @endif
                <tr><td><b>Previous Investment Details</b></td><td></td></tr>
                <tr><td>FA</td><td>{!! $investment->present()->commissionRecipientName !!}</td></tr>
                <tr><td>Interest Payment</td><td>{!! $investment->present()->getInterestPaymentDetails !!}</td></tr>
                @if($investment->interestAction)
                    <tr>
                        <td>Interest Action</td>
                        <td>{!! $investment->present()->getInterestActionDetails !!}</td>
                    </tr>
                @endif
            </table>

        </div>
    </div>


</div>