<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                    <tr>
                        <td>Client Code</td>
                        <td colspan="2">{!! $investment->client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client Name</td>
                        <td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Transaction approval</td>
                        <td colspan="2"><a href="/dashboard/investments/approve/{!! $investment->approval->id !!}">See approval details</a></td>
                    </tr>
                    <tr>
                        <td>Principal</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Value Today</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment()) !!}</td>
                    </tr>
                    <tr>
                        <td>Value on maturity</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment()) !!}</td>
                    </tr>
                    <tr>
                        <td>Investment Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Maturity Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Interest rate</td>
                        <td colspan="2">{!! $investment->interest_rate !!}%
                    </tr>
                    @if($investment->withdrawn == 1)
                        <tr>
                            <td>Withdrawal Date</td>
                            <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->withdrawal_date) !!}</td>
                        </tr>
                        <tr>
                            <td>Withdrawal Transaction Approval</td>
                            <td colspan="2"><a href="/dashboard/investments/approve/{!! $investment->withdraw_approval_id !!}">See approval details</a></td>
                        </tr>
                    @else
                        <?php $schedule = $investment->schedule()->first() ?>
                        @if($schedule)
                            <tr>
                                <td>Schedule Status</td>
                                <td colspan="2">{!! ucfirst($schedule->action) !!} Scheduled &nbsp; &nbsp; &nbsp;
                                    <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter"
                                       href = "/dashboard/investments/clientinvestments/schedule/{!! $investment->id !!}"><i class = "fa fa-list-alt"></i></a>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>Schedule Status</td>
                                <td colspan="2">No scheduled action</td>
                            </tr>
                        @endif
                    @endif
                    <tr>
                        <td>Status</td>
                        <td colspan="2">{!! \Cytonn\Presenters\InvestmentPresenter::presentInvestmentStatus($investment)!!}</td>
                    </tr>
                    <tr><th class="bold">Item</th><th class="bold">Was</th><th class="bold">Now</th><tr/>
                </thead>
                <tbody>
                    @if(isset($data['amount']))
                        <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($edited->amount) !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'] )!!}</td></tr>
                    @endif
                    @if(isset($data['interest_rate']))
                        <tr><td>Interest Rate</td><td>{!! $edited->interest_rate !!}%</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                    @endif
                    @if(isset($data['invested_date']))
                        <tr><td>Invested date</td><td>{!! \Carbon\Carbon::parse($edited->invested_date)->toDateString() !!}</td><td>{!! $data['invested_date'] !!}</td></tr>
                    @endif
                    @if(isset($data['maturity_date']))
                        <tr><td>Maturity date</td><td>{!! \Carbon\Carbon::parse($edited->maturity_date)->toDateString() !!}</td><td>{!! $data['maturity_date'] !!}</td></tr>
                    @endif
                    @if(isset($data['on_call']))
                        <tr><td>On call</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($edited->on_call) !!}</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['on_call']) !!}</td></tr>
                    @elseif($edited->on_call)
                        <tr><td>On call</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($edited->on_call) !!}</td><td>No</td></tr>
                    @endif
                    @if(isset($data['commission_recepient']))
                        <tr><td>FA</td><td>{!! $recipient($edited_commission['recipient_id'])->name !!}</td><td>{!! $recipient($data['commission_recepient'])->name !!}</td></tr>
                    @endif
                    @if(isset($data['commission_rate']))
                        <tr><td>Commission Rate</td><td>{!! $edited_commission['rate'] !!}%</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                    @endif
                    @if(isset($data['commission_rate_name']))
                        <tr><td>Commission Rate Name</td><td>{!! $edited_commission['rate'] !!}%</td><td>{!! $data['commission_rate_name'] !!}%</td></tr>
                    @endif
                    @if(isset($data['commission_start_date']))
                        <tr><td>Commission Start Date</td><td>{!! $edited_commission['start_date'] !!}</td><td>{!! $data['commission_start_date'] !!}</td></tr>
                    @endif
                    @if(isset($data['interest_payment_interval']) || isset($data['interest_payment_date']))
                        <tr><td>Interest payment</td><td>{!!$interestPayment[$edited->interest_payment_interval] !!} on date {!! $edited->interest_payment_date !!}</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                    @endif
                    @if(isset($data['interest_action_id']))
                        <tr><td>Interest Action</td><td>@if($edited->interestAction){!! $edited->interestAction->name !!}@endif</td><td>{!! $interest_action($data['interest_action_id'])->name !!}</td></tr>
                    @endif
                    @if(isset($data['interest_payment_start_date']))
                        <tr><td>Interest Payment Start Date</td><td>{!! $edited['interest_payment_start_date'] !!}</td><td>{!! $data['interest_payment_start_date'] !!}</td></tr>
                    @endif
                    @if(isset($data['interest_reinvest_tenor']))
                        <tr><td>Interest Reinvest Tenor</td><td>{!! $edited['interest_reinvest_tenor'] !!}</td><td>{!! $data['interest_reinvest_tenor'] !!}</td></tr>
                    @endif
                </tbody>
            </table>
        </div>

    </div>
</div>