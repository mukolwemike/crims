<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Delete Joint Holder details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{{$jointHolder->firstname}}&nbsp;{{$jointHolder->middlename}}&nbsp;{{$jointHolder->lastname}}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{$jointHolder->email}}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{{$jointHolder->telephone_home}}</td>
                </tr>
                <tr>
                    <td>PIN</td>
                    <td>{{$jointHolder->pin_no}}</td>
                </tr>
                <tr>
                    <td>ID/Passport</td>
                    <td>{{$jointHolder->id_or_passport}}</td>
                </tr>
                <tr>
                    <td>Reason for Delete</td>
                    <td>{{$data['reason']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>