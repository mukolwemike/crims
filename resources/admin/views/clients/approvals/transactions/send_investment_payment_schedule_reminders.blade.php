<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Investment Payment Schedule Reminders - From : {!! \Cytonn\Presenters\DatePresenter::formatDate($data['start']) !!} To : {!! \Cytonn\Presenters\DatePresenter::formatDate($data['end']) !!}</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <table class="table table-hover table-responsive table-striped">
                    <tbody>
                    <tr>
                        <th>Client Code</th>
                        <th>Client Name</th>
                        <th>Amount</th>
                        <th>Payment Date</th>
                        <th>Product</th>
                        <th>Eligible</th>
                        <th colspan="2">Action</th>
                    </tr>
                    @foreach($schedules as $schedule)
                        <tr>
                            <td>{!! $schedule->parentInvestment->client->client_code !!}</td>
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($schedule->parentInvestment->client_id) !!}</td>
                            <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($schedule->amount) !!}</td>
                            <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($schedule->date) !!}</td>
                            <td>{!! $schedule->parentInvestment->product->name  !!}</td>
                            <td>
                                @if($checkEligible($schedule->id))
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-danger">No</span>
                                @endif

                            </td>
                            @if(! in_array($schedule->id, $removedSchedules))
                                <td>
                                    <span class="label label-success">Sending</span>
                                </td>
                                <td>
                                    @if(! $approval->approved)
                                        <a class="btn btn-danger btn-sm"
                                           href="{!! route('approval_action', [$approval->id, 'remove_schedule', $schedule->id]) !!}">Remove</a>
                                    @endif
                                </td>
                            @else
                                <td>
                                    <span class="label label-danger">Not Sending</span>
                                </td>
                                <td>
                                    @if(! $approval->approved)
                                        <a class="btn btn-success btn-sm"
                                           href="{!! route('approval_action', [$approval->id, 'add_schedule', $schedule->id]) !!}">Add</a>
                                    @endif
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>