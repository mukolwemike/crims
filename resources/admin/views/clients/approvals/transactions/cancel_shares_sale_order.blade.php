<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Cancel Shares Sale Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Seller</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($order->seller->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $order->seller->number !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td>{!!$order->seller->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Current Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->seller->currentShares(\Carbon\Carbon::parse($order->request_date)), true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->number, true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->price) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->price * $order->price) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Current No. of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->seller->shareHoldings->sum('number'), true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($order->good_till_filled_cancelled)  !!}</td>
                    </tr>
                    @if(!$order->good_till_filled_cancelled)
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->expiry_date) !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>