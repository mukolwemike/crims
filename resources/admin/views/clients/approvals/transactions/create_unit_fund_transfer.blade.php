<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transfer Fund Units</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Sender's Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($sender->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Sender's Client Code</td>
                        <td>{!! $sender->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Recipient's Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($recipient->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Recipient's Client Code</td>
                        <td>{!! $recipient->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Name</td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Units</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Value of Units</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund->unitPrice(Carbon\Carbon::parse($data['date'])) * $data['number']) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>