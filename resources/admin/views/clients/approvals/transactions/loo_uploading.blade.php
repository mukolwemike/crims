<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate LOO Uploaded</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <label>Project </label> {!! $project->name !!}<br/>
                <label>Unit </label> {!! $unit->number !!}<br/>
                <label>Size </label> {!! $unit->size->name !!}<br/>
                <label>Type </label> {!! $unit->type->name !!}<br/>

                @if(isset($data['date_signed']))
                    @if(strlen($data['date_signed']))
                        <label>Date Signed</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_signed']) !!} <br>
                    @endif
                @endif
                @if(isset($data['advocate_id']))
                    @if(strlen($data['advocate_id']))
                        <label>Advocate</label> {!! $advocate($data['advocate_id'])->name !!} <br>
                    @endif
                @endif
                @if(isset($data['document_id']))
                    <label>View the LOO</label> <a href="/dashboard/documents/{!! $data['document_id'] !!}">View</a> <br>
                @endif
            </div>
        </div>
    </div>
</div>