<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Redeem Loyalty Points Details</h4>
        </div>
        <div class="panel-body">
            <h4>Client Details</h4>
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th>Name</th>
                    <td>{{\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id)}}</td>
                </tr>
                <tr>
                    <th>Investments Points</th>
                    <td>{{\Cytonn\Presenters\AmountPresenter::currency($calc->getTotalInvestmentsPoints(), false, 2)}}</td>
                </tr>
                <tr>
                    <th>Unit Trust Points</th>
                    <td>{{\Cytonn\Presenters\AmountPresenter::currency($calc->getTotalFundsPoints(), false, 2)}}</td>
                </tr>
                <tr>
                    <th>Real Estate Points</th>
                    <td>{{\Cytonn\Presenters\AmountPresenter::currency($calc->getTotalHoldingsPoints(), false, 2)}}</td>
                </tr>
                <tr>
                    <th>Total Points</th>
                    <th>{{\Cytonn\Presenters\AmountPresenter::currency($calc->getTotalPoints(), false, 2)}}</th>
                </tr>
                </tbody>
            </table>
            <br/>
            <h4>Redeem Details</h4>
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th>Points</th>
                    <td>{{\Cytonn\Presenters\AmountPresenter::currency($instruction->amount, false, 0)}}</td>
                </tr>
                <tr>
                    <th>Value</th>
                    <td>KES. {{\Cytonn\Presenters\AmountPresenter::currency($instruction->amount, false, 2)}}</td>
                </tr>
                <tr>
                    <th>Origin</th>
                    <td>
                        @if($instruction->origin === 0)
                            <span class="label label-info">ADMIN</span>
                        @else
                            <span class="label label-info">WEB</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td>{{\Cytonn\Presenters\DatePresenter::formatDate($instruction->date)}}</td>
                </tr>
                <tr>
                    <th>Vouchers</th>
                    <td>
                        <table class="table table-responsive table-striped">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Upload</th>
                            </tr>
                            @foreach($vouchers as $voucher)
                                <tr>
                                    <td>{{$voucher->name}}</td>
                                    <td>{{\Cytonn\Presenters\AmountPresenter::currency($voucher->value, false, 2)}}</td>
                                    <td>
                                        <a target="_blank" href="/document/view-document/{{$voucher->document_id}}">
                                            <i class="fa fa-file-pdf-o"></i> View
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>