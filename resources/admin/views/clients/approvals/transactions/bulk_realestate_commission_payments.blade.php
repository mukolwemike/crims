<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <p>From: {!! \Cytonn\Presenters\DatePresenter::formatDate($bulk->start) !!} To: {!! \Cytonn\Presenters\DatePresenter::formatDate($bulk->end) !!}</p>
            <p>Description: {!! $bulk->description !!}</p>
            <p class="pull-right"><a class="btn btn-info" href="{!! route('approval_action', [$approval->id, 'excel']) !!}">Export to excel</a></p>
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>FA</th>
                        <th>Commission</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recipients as $fa)
                        <tr>
                            <td>{!! $fa->name !!}</td>
                            <td> {!! \Cytonn\Presenters\AmountPresenter::currency($fa->amount_payable) !!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

             {!! $recipients->links() !!}
        </div>
    </div>
</div>