@extends('layouts.default')

@section('content')
    <approval-uploads
            :approval="{{json_encode($approvalId)}}"
            :edit="{{ json_encode($editInv) }}">

    </approval-uploads>
@endsection
