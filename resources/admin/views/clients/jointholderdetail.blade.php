@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Joint Holder Details</h3>

            <div>
                <div class="detail-group">
                    @if($holder->title_id)
                        <div class="col-md-12">
                            <p>Name: {!! \App\Cytonn\Models\Title::find($holder->title_id)->name !!} {!! ucfirst($holder->firstname) !!} {!! ucfirst($holder->middlename) !!} {!! ucfirst($holder->lastname) !!}</p>
                        </div>
                    @else
                    @endif
                    @if($holder->gender_id)
                            <div class="col-md-6">
                                <p>Gender: {!! \App\Cytonn\Models\Gender::find($holder->gender_id)->abbr !!}</p>
                            </div>
                    @else
                    @endif
                    <div class="col-md-6">
                        <p>Date of Birth: {!! \Cytonn\Presenters\DatePresenter::formatDate($holder->dob) !!}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Pin Number: {!! $holder->pin_no !!}</p>
                    </div>
                    <div class="col-md-6">
                        <p>ID/Passport Number: {!! $holder->id_or_passport !!}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Email: {!! $holder->email !!}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Postal Address: {!! $holder->postal_address !!}</p>
                    </div>
                    <div class="col-md-6">
                        <p>Postal Code: {!! $holder->postal_code !!}</p>
                    </div>
                    @if($holder->country_id)
                            <div class="col-md-6">
                                <p>Country: {!! \App\Cytonn\Models\Country::find($holder->country_id)->name !!}</p>
                            </div>
                    @else
                    @endif
                    @if($holder->telephone_office)
                            <div class="col-md-6">
                                <p>Office Telephone: {!! $holder->telephone_office !!}</p>
                            </div>
                    @else
                    @endif
                    @if($holder->telephone_home)
                            <div class="col-md-6">
                                <p>Home Telephone: {!! $holder->telephone_home !!}</p>
                            </div>
                    @else
                    @endif
                    <div class="col-md-6">
                        <p>Cell Number: {!! $holder->telephone_cell !!}</p>
                    </div>
                    @if($holder->residential_address)
                        <div class="col-md-6">
                            <p>Home Telephone: {!! $holder->residential_address !!}</p>
                        </div>
                    @else
                    @endif
                    @if($holder->town)
                        <div class="col-md-6">
                            <p>Home Telephone: {!! $holder->town !!}</p>
                        </div>
                    @else
                    @endif
                    @if($holder->method_of_contact_id)
                        <div class="col-md-6">
                            <p>Preferred Method of Contact: {!! \App\Cytonn\Models\ContactMethod::find($holder->method_of_contact_id)->description !!}</p>
                        </div>
                    @else
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection