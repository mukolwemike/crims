@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            @include('investment.partials.investmentdetail')
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <h4>Instruction Details</h4>
                <div class="form-detail">
                    <label for="">Description</label> {!! $instruction->description !!}<br>

                    <label for="">Date</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}<br>

                    <label for="">Amount</label> {!! $investment->product->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount) !!}<br>

                    <label for="">Account Name</label> {!! $bankDetails->accountName() !!}<br>

                    <label for="">Account Number</label> {!! $bankDetails->accountNumber() !!}<br>

                    <label for="">Bank</label> {!! $bankDetails->bankName() !!}<br>

                    <label for="">Branch</label> {!! $bankDetails->branch() !!}<br>

                    @if(!$instruction->combined_to)
                        <a class="btn btn-success pull-right" href="/dashboard/investments/instructions/{!! $instruction->investment_id !!}/{!! $instruction->id !!}">Generate Instruction</a>
                    @endif
                    <div class="clearfix"></div>
                </div>
                @if(!$instruction->combined && !$instruction->combined_to)
                    <p>This instruction can be merged with the instructions below:</p>
                    {!! Form::open(['route'=>['combine-bank-instruction', $instruction->id]]) !!}
                        <table class="table table-responsive table-hover">
                            <thead>
                                <tr><td></td><th>Description</th><th>Amount</th></tr>
                            </thead>
                            <tbody>
                            @foreach($similar_instructions as $instr)
                                <tr><td>{!! Form::checkbox('instr_id[]', $instr->id) !!}</td><td>{!! $instr->description !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($instr->amount) !!}</td></tr>
                            @endforeach
                            </tbody>
                        </table>
                    {!! Form::submit('Combine', ['class'=>'btn btn-success pull-right']) !!}
                    {!! Form::close() !!}
                @elseif($instruction->combined_to)
                    <p>This instruction has been combined, <a href="/dashboard/clients/bank-instructions/show/{!! $instruction->combined_to !!}">click here to view combination</a></p>
                @endif

                @if($combination_of->count() > 0)
                    <p>This instruction is a combination of:</p>

                    <table class="table table-responsive table-hover">
                        <thead>
                        <tr><th>Description</th><th>Amount</th></tr>
                        </thead>
                        <tbody>
                        @foreach($combination_of as $instr)
                            <tr><td>{!! $instr->description !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($instr->amount) !!}</td></tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! link_to_route('dissolve-bank-instruction', 'Dissolve Instruction', [$instruction->id], ['class'=>'btn btn-success pull-right']) !!}
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <div ng-controller="ClientBankInstructionsGridCtrl">
                {!! Form::text('client_id', $client_id, ['init-model'=>'client_id', 'class'=>'hide']) !!}
                <table st-pipe="getClientPage" st-table="displayed" class = "table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th colspan = "9"></th>
                        <th colspan = "3"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                    </tr>
                    <tr>
                        <th>Principal</th>
                        <th>Value Date</th>
                        <th>Maturity Date</th>
                        <th>Type</th>
                        <th>Bank</th>
                        <th>Branch</th>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th st-sort-default="reverse" st-sort="date">Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed">
                        <td><% row.principal | currency:"" %></td>
                        <td><% row.invested_date | date %></td>
                        <td><% row.maturity_date | date %></td>
                        <td><% row.type %></td>
                        <td><% row.bank %></td>
                        <td><% row.branch %></td>
                        <td><% row.account_name %></td>
                        <td><% row.account_number %></td>
                        <td><% row.date | date %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><span to-html = 'row.viewed | status:"Viewed"'></span></td>
                        <td>
                            <a ng-controller = "PopoverCtrl" uib-popover = "Details" popover-trigger = "mouseenter" href = "/dashboard/clients/bank-instructions/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr><td colspan="100%">Loading...</td></tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan = "100%" dmc-pagination></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection