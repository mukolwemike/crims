@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div ng-controller="ClientBankInstructionsGridCtrl">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan = "9"></th>
                    <th colspan="2"><input st-search="client_code" placeholder="search by client code" class="form-control" type="text"/></th>
                    <th colspan = "3"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th>Principal</th>
                    <th>Value Date</th>
                    <th>Maturity Date</th>
                    <th>Type</th>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>Account Name</th>
                    <th>Account Number</th>
                    <th st-sort-default="reverse" st-sort="date">Date</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody ng-show="!isLoading">
                <tr ng-repeat = "row in displayed">
                    <td><% row.client_code %></td>
                    <td><% row.name %></td>
                    <td><% row.principal | currency:"" %></td>
                    <td><% row.invested_date | date %></td>
                    <td><% row.maturity_date | date %></td>
                    <td><% row.type %></td>
                    <td><% row.bank %></td>
                    <td><% row.branch %></td>
                    <td><% row.account_name %></td>
                    <td><% row.account_number %></td>
                    <td><% row.date | date %></td>
                    <td><% row.amount | currency:"" %></td>
                    <td><span to-html = 'row.viewed | status:"Viewed"'></span></td>
                    <td>
                        <a ng-controller = "PopoverCtrl" uib-popover = "View Instruction" popover-trigger = "mouseenter" href = "/dashboard/clients/bank-instructions/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                    </td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr><td colspan="100%">Loading...</td></tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "100%" dmc-pagination></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection