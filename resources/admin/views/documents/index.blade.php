@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>{{ ucwords(str_replace('_', ' ', str_plural($module)))}} {{ ucwords(str_replace('_', ' ', str_plural($slug))) }}</h3>
            <div ng-controller="DocumentsGridController" data-ng-init="module='{!! $module !!}'; typeSlug='{!! $slug !!}'; sectionSlug='{!! $sectionId !!}'">
                <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <div class="btn-group">
                                <a class="btn btn-default margin-bottom-20" href="/dashboard/documents" target="_self"><i class="fa fa-long-arrow-left"></i> Go Back</a>
                                <button class="btn btn-primary margin-bottom-20" data-toggle="modal" data-target="#upload-document"><i class="fa fa-plus-square-o"></i> Upload Doc</button>
                            </div>
                        </th>
                        <th colspan="3"></th>
                        @if($module == 'investments')
                            <th>
                                {!! Form::select('product', $products, $sectionId, ['class'=>'form-control', 'st-search' => 'product']) !!}
                                {{--<select st-search="product" class="form-control">--}}
                                    {{--@foreach($products as $key => $product)--}}
                                        {{--<option value="{!! $key !!}">{!! $product !!}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            </th>
                        @elseif($module == 'realestate')
                            <th>
                                {!! Form::select('project', $projects, $sectionId, ['class'=>'form-control', 'st-search' => 'project']) !!}
                                {{--<select st-search="project" class="form-control">--}}
                                    {{--@foreach($projects as $key => $project)--}}
                                        {{--<option value="{!! $key !!}">{!! $project !!}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            </th>
                        @elseif($module == 'portfolio_investors')
                            <th>
                                {!! Form::select('portfolio_investor', $portfolioInvestors, $sectionId, ['class'=>'form-control', 'st-search' => 'portfolio_investor']) !!}
                                {{--<select st-search="project" class="form-control">--}}
                                {{--@foreach($projects as $key => $project)--}}
                                {{--<option value="{!! $key !!}">{!! $project !!}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                            </th>
                        @elseif($module == 'others')
                            <th></th>
                        @endif
                        <th colspan="4">
                            <input st-search="title" class = "form-control" placeholder = "Search by title..." type="text"/>
                        </th>
                    </tr>
                        <tr>
                            <th st-sort="id">ID</th>
                            <th>Title</th>
                            <th>Compliance Doc.</th>
                            @if($module != 'others')
                                <th>Category</th>
                                @if($module == 'investments')
                                    <th>Product</th>
                                @elseif($module == 'realestate')
                                    <th>Project</th>
                                @endif
                            @endif
                            <th>Type</th>
                            <th>Date</th>
                            {{--<th>Uploaded On</th>--}}
                            <th colspan="3">File</th>
                        </tr>
                    </thead>

                    <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.title %></td>
                        <td><% row.compliance_document %></td>
                        @if($module != 'others')
                            <td><% row.category %></td>
                            <td><% row.item %></td>
                        @endif
                        <td><% row.type %></td>
                        <td><% row.date %></td>
                        {{--<td><% row.uploaded_on %></td>--}}
                        <td><a href="/dashboard/documents/<% row.id %>"><i class="fa fa-file-pdf-o"></i></a></td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#update-document-<% row.id %>"><i class="fa fa-pencil-square-o warning"></i></a>
                            <div class="modal fade" id="update-document-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open(['route'=>['update_document'], 'method'=>'PUT', 'files'=>true]) !!}
                                        <div class="modal-header">
                                            <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                            <h4 class="modal-title" id="myModalLabel">Update Document</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="document_id" value="<% row.id %>">
                                            <div class="form-group">
                                                {!! Form::label('category', 'Document Category') !!}
                                                {!! Form::select('category', [1=>'Investments', 2=>'Real Estate', 3=>'Portfolio Investors'], null, ['class'=>'form-control', 'ng-model'=>'category', 'ng-init'=>'category = row.category_id + ""', 'ng-required'=>'true',]) !!}
                                            </div>
                                            <div class="form-group" ng-if="category == 1">
                                                {!! Form::label('product_id', 'Product') !!}
                                                {!! Form::select('product_id', $products, null, ['class'=>'form-control', 'ng-required'=>'true', 'ng-model' => 'product', 'ng-init'=>'product = row.product_id + ""']) !!}
                                            </div>
                                            <div class="form-group" ng-if="category == 2">
                                                {!! Form::label('project_id', 'Project') !!}
                                                {!! Form::select('project_id', $projects, null, ['class'=>'form-control', 'ng-required'=>'true', 'ng-model' => 'project', 'ng-init'=>'project = row.project_id + ""']) !!}
                                            </div>
                                            <div class="form-group" ng-if="category == 3">
                                                {!! Form::label('portfolio_investor_id', 'Portfolio Investor') !!}
                                                {!! Form::select('portfolio_investor_id', $portfolioInvestors, null, ['class'=>'form-control', 'ng-required'=>'true', 'ng-model' => 'portfolio_investor', 'ng-init'=>'portfolio_investor = row.portfolio_investor_id + ""']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('title', 'Title of Document') !!}
                                                {!! Form::text('title', null, ['class'=>'form-control', 'ng-required'=>"true", 'ng-model'=>'row.title']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('type', 'Document Type') !!}
                                                {!! Form::select('type', $document_types, null, ['class'=>'form-control', 'ng-required'=>'true', 'ng-model'=>'row.type_id']) !!}
                                            </div>
                                            <div class = "form-group"  ng-controller = "DatepickerCtrl">
                                                {!! Form::label('date', 'Date') !!}
                                                {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"row.date_raw", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('file', 'Select File') !!}
                                                {!! Form::file('file', ['class'=>'form-control']) !!}
                                            </div>
                                            <div class="form-group" ng-if="category == 2">
                                                {!! Form::checkbox('compliance_document', 1, false, ['ng-model'=>'row.compliance']) !!}
                                                {!! Form::label('compliance_document', 'LOO Compliance Document?') !!}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Upload', ['class'=>'btn btn-success']) !!}
                                            <a data-dismiss="modal" class="btn btn-default">Close</a>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#delete-document-<% row.id %>"><i class="fa fa-remove color-danger"></i></a>
                            <div class="modal fade" id="delete-document-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                            <h4 class="modal-title" id="myModalLabel">Delete Document</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure that you want to delete this document?</p>
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['route'=>['delete_document'],'method'=>"DELETE"]) !!}
                                            <input type="hidden" name="document_id" value="<% row.id %>">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            {!! Form::button('<i class="fa fa-remove"></i> Delete', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="100%" class="text-center">Loading ... </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop

<div class="modal fade" id="upload-document" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route' => ['upload_document'], 'files'=>true]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Document</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('category', 'Document Category') !!}
                    {!! Form::select('category', [1=>'Investments', 2=>'Real Estate', 3=>'Portfolio Investors'], null, ['class'=>'form-control', 'init-model'=>'category', 'ng-required'=>'true']) !!}
                </div>
                <div class="form-group" ng-if="category == 1">
                    {!! Form::label('product_id', 'Product') !!}
                    {!! Form::select('product_id', $products, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                </div>
                <div class="form-group" ng-if="category == 2">
                    {!! Form::label('project_id', 'Project') !!}
                    {!! Form::select('project_id', $projects, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                </div>
                <div class="form-group" ng-if="category == 3">
                    {!! Form::label('portfolio_investor_id', 'Portfolio Investor') !!}
                    {!! Form::select('portfolio_investor_id', $portfolioInvestors, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title', 'Title of Document') !!}
                    {!! Form::text('title', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('type', 'Document Type') !!}
                    {!! Form::select('type', $document_types, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                </div>
                <div class = "form-group"  ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Date') !!}
                    {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('file', 'Select File') !!}
                    {!! Form::file('file', ['class'=>'form-control']) !!}
                </div>
                <div class="form-group" ng-if="category == 2">
                    {!! Form::checkbox('compliance_document', 1) !!}
                    {!! Form::label('compliance_document', 'LOO Compliance Document?') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save Document', ['class'=>'btn btn-success pull-left']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>