@extends('layouts.default')

<style>
    * {
        margin: 0px;
        padding: 0px;
    }

    body {
        margin: 50px;
    }

    ul.tree-structure {
        margin: 0px 0px 0px 20px;
        list-style: none;
        line-height: 2em;
        font-family: Arial;
        clear: both;
        padding-left: 15px;
    }
    ul.tree-structure li {
        font-size: 16px;
        position: relative;
    }
    ul.tree-structure li:before {
        position: absolute;
        left: -15px;
        top: 0px;
        content: '';
        display: block;
        border-left: 1px solid #ddd;
        height: 1em;
        border-bottom: 1px solid #ddd;
        width: 10px;
    }
    ul.tree-structure li:after {
        position: absolute;
        left: -15px;
        bottom: -7px;
        content: '';
        display: block;
        border-left: 1px solid #ddd;
        height: 100%;
    }
    ul.tree-structure li a{
        font-weight: normal;
        color: red;
        text-decoration: none;
        font-weight: 700;
        vertical-align: middle;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.2s ease-in-out;
        display: inline-block;
        max-width: calc(100% - 50px);
        vertical-align: top;
    }
    ul.tree-structure li a:hover{
        padding-left: 5px;
    }
    ul.tree-structure ul li a{
        color: #000;
        font-weight: normal;
    }




    ul.tree-structure li.root {
        margin: 0px 0px 0px -20px;
    }
    ul.tree-structure li.root:before {
        display: none;
    }
    ul.tree-structure li.root:after {
        display: none;
    }
    ul.tree-structure li:last-child:after {
        display: none;
    }

</style>
@section('content')
    <div class="col-md-10 col-md-offset-1" ng-controller="DocumentStructureController">
        <div class="panel-dashboard">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="tree-structure">
                            <li class="root">
                                <a href="#">Document Types</a>
                            </li>
                            <li>
                                <i class="fa fa-folder-open"></i>
                                <a ng-click="displaySection('projects')" href="#">Projects</a>
                                <ul ng-if="displayedSection == 'projects'" class="tree-structure">
                                    @foreach($projects as $key => $project)
                                        <li>
                                            <i class="fa fa-folder-open"></i>
                                            <a ng-click="displayProjectItem('{!! $key !!}')" href="#">{!! $project->name !!}</a>
                                            <ul ng-if="displayedProjectItem == '{!! $key !!}'" class="tree-structure">
                                                @foreach($project->documentTypes as $type)
                                                    <li>
                                                        <i class="fa fa-folder-o"></i>
                                                        <a href="/dashboard/documents/realestate/{{ $type->slug }}/{{ $project->id }}">{{ $type->name }}</a>
                                                        <span class="label label-primary">{{ $type->numberOfDocuments }}</span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <i class="fa fa-folder-open"></i>
                                <a ng-click="displaySection('products')" href="#">Products</a>
                                <ul ng-if="displayedSection == 'products'" class="tree-structure">
                                    @foreach($products as $key => $product)
                                        <li>
                                            <i class="fa fa-folder-open"></i>
                                            <a ng-click="displayProductItem('{!! $key !!}')" href="#">{!! $product->name !!}</a>
                                            <ul ng-if="displayedProductItem == '{!! $key !!}'" class="tree-structure">
                                                @foreach($product->documentTypes as $type)
                                                    <li>
                                                        <i class="fa fa-folder-o"></i>
                                                        <a href="/dashboard/documents/investments/{{ $type->slug }}/{{ $product->id }}">{{ $type->name }}</a>
                                                        <span class="label label-primary">{{ $type->numberOfDocuments }}</span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                            <li>
                                <i class="fa fa-folder-open"></i>
                                <a ng-click="displaySection('portfolioInvestors')" href="#">Portfolio Investors</a>
                                <ul ng-if="displayedSection == 'portfolioInvestors'" class="tree-structure">
                                    @foreach($portfolioInvestorGroups as $groupKey => $portfolioInvestors)
                                        <li>
                                            <i class="fa fa-folder-open"></i>
                                            <a ng-click="displayInvestorGroupItem('{!! $groupKey !!}')" href="#">{!! $groupKey !!}</a>
                                            <ul ng-if="displayedInvestorGroupItem == '{!! $groupKey !!}'" class="tree-structure">
                                                @foreach($portfolioInvestors as $key => $portfolioInvestor)
                                                    <li>
                                                        <i class="fa fa-folder-open"></i>
                                                        <a ng-click="displayPortfolioItem('{!! $key !!}')" href="#">{!! $portfolioInvestor->name !!}</a>
                                                        <ul ng-if="displayedPortfolioItem == '{!! $key !!}'" class="tree-structure">
                                                            @foreach($portfolioInvestor->documentTypes as $type)
                                                                <li>
                                                                    <i class="fa fa-folder-o"></i>
                                                                    <a href="/dashboard/documents/portfolio_investors/{{ $type->slug }}/{{ $portfolioInvestor->id }}">{{ $type->name }}</a>
                                                                    <span class="label label-primary">{{ $type->numberOfDocuments }}</span>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                            <li>
                                <i class="fa fa-folder-open"></i>
                                <a ng-click="displaySection('others')" href="#">Other Documents</a>
                                <ul ng-if="displayedSection == 'others'" class="tree-structure">
                                    @foreach($otherTypes as $type)
                                        <li>
                                            <i class="fa fa-folder-o"></i>
                                            <a href="/dashboard/documents/others/{{ $type->slug }}">{{ $type->name }}</a>
                                            <span class="label label-primary">{{ $type->numberOfDocuments }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop