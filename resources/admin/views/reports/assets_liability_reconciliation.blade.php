@extends('reports.letterhead')

@section('content')
    <p>
        Please see below the asset and Liabilities report as at {!! $date->toFormattedDateString() !!}.
    </p>

    <table class="table table-bordered table-responsive">
        <thead>
        <tr>
            <th></th>
            <th>Cost Value</th>
            <th>Adjusted Market Value</th>
            <th>Market Value</th>
        </tr>
        <tr>
            <th colspan="4">Assets</th>
        </tr>
        </thead>
        <tbody>
            @foreach($data->assets as $name => $val)
                <tr>
                    <td>{!! $name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($val['cost_value']) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($val['adjusted_market_value']) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($val['market_value']) !!}</td>
                </tr>
            @endforeach
            <tr>
                <th>Total Assets</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['assets']['cost_value'])  !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['assets']['adjusted_market_value'])  !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['assets']['market_value'])  !!}</th>
            </tr>
            <tr><td colspan="4"></td></tr>
        </tbody>
        <thead>
        <tr>
            <th colspan="4">Liabilities</th>
        </tr>
        </thead>
        <tbody>
            @foreach($data->liabilities as $name => $val)
                <tr>
                    <td>{!! $name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($val['cost_value']) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($val['adjusted_market_value']) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($val['market_value']) !!}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total Liabilities</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['liabilities']['cost_value'])  !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['liabilities']['adjusted_market_value'])  !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['liabilities']['market_value'])  !!}</th>
            </tr>
            <tr>
                <th>Surplus</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['surplus']['cost_value'])  !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['surplus']['adjusted_market_value'])  !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data->totals['surplus']['market_value'])  !!}</th>
            </tr>
        </tfoot>
    </table>
@endsection