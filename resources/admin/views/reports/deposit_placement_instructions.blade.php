@extends('reports.plain_letterhead_space')

@section('content')

    <style>
        .left {
            float: left;
            width: 250px;
            margin: 0;
            position: absolute;
        }
        .right {
            margin-left: 300px;
            overflow: hidden;
            text-align: justify;
        }
    </style>

    <br/><br/>

    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! $deposit->custodialAccount()->bank_name !!}<br/>

    {!! $deposit->custodialAccount()->address !!}
    <br/>
    <br/>

    <span style="font-weight: bold; text-decoration: underline;">Attn: {!! $deposit->custodialAccount()->contact_person !!}</span>
    <br/>
    <br/>

    @if($deposit->security->depositType->name == 'Loan')
        <p class="bold-underline">RE: KES {!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}/- ({!! strtoupper($deposit_in_words) !!}) CASH TRANSFER</p>
    @else
        <p class="bold-underline">RE: KES {!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}/- ({!! strtoupper($deposit_in_words) !!}) DEPOSIT PLACEMENT</p>
    @endif

    @if($deposit->security->depositType->name == 'Loan')
        <p>
            Kindly transfer KES

            {!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}
            ({!!
            ucfirst((new \Cytonn\Lib\Helper())->convertNumberToWords($deposit->amount, false))
        !!}) from our {!! $deposit->custodialAccount()->alias !!} {!! $deposit->custodialAccount()->account_no !!} in favour of
            {!! $deposit->security->investor->account_name !!}.
        </p>

        <p>
            Bank details are as shown below: <br>
            <strong>Bank Name:</strong> {!! $deposit->security->investor->bank_name !!}<br>
            <strong>Account Number:</strong> {!! $deposit->security->investor->account_number !!} <br>
            <strong>Account Name:</strong> {!! $deposit->security->investor->account_name !!}<br>
            <strong>Branch Name:</strong> {!! $deposit->security->investor->branch_name !!}<br>
        </p>
    @else
        <p>
            Kindly place a deposit of Ksh. {!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}/- ({!! $deposit_in_words !!})
            only from our Investment A/C {!! $deposit->custodialAccount()->account_no !!} with {!! $deposit->security->investor->name !!} at a
            rate of {!! $deposit->interest_rate !!}% p.a value date {!! $value_date !!}
            @if($deposit->on_call)
                on call.
            @else
                to mature on {!! $maturity_date !!}.
            @endif
        </p>

        <p>
            For this investment please talk to {!! $deposit->contact_person !!} of {!! $deposit->security->investor->name !!}.
        </p>
    @endif

    <p>
        In case of any query kindly get in touch with us.
    </p>

    <p>Yours Faithfully,</p>
    For: {!! $deposit->security->fundManager->fullname !!}
    <br/><br/><br/>

    <span  style="display: inline-block; width: 70%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($first_signatory->id) !!}
    </span>

    <span style="display: inline-block;  width: 50%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($second_signatory->id) !!}
    </span>

    <p>Prepared by: {!! \Cytonn\Presenters\UserPresenter::presentFullNames($deposit->investApproval->sent_by) !!}</p>
    <p>Reviewed by: {!! \Cytonn\Presenters\UserPresenter::presentFullNames($deposit->investApproval->approved_by) !!}</p>
    <div style="position: absolute; bottom: 40px; left: 0;">
        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($deposit->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>

        <p class="left">REF: CRIMS/DEPOSIT/{!! $deposit->id !!}</p>
    </div>

@stop