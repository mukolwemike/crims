@extends($logo ? 'reports.letterhead' : 'reports.plain_letterhead_space')

@section('content')
    {!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}<br/><br/>

    @if($addr = $instruction->custodialTransaction->custodialAccount->address)
        {!! $addr !!}
    @else
        Standard Chartered Bank Kenya Limited<br/>
        48 Westlands Road<br/>
        P.O. BOX 40984 - 00100<br/>
        Custody Service Department<br/>
        Nairobi.
    @endif


    <p class="bold-underline">Attn: {!! $instruction->custodialTransaction->custodialAccount->contact_person !!}</p>

    Dear Sir/Madam,<br/>

    <p class="bold-underline"> RE:
        {!! $instruction->custodialTransaction->custodialAccount->currency->code !!}
        {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount, false, 0) !!}
        ({!!
            strtoupper((new \Cytonn\Lib\Helper())->convertNumberToWords($instruction->amount, true))
        !!}) CASH TRANSFER.
    </p>
    <p>Kindly transfer
        {!! $instruction->custodialTransaction->custodialAccount->currency->code !!}
        {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount,  false, 0) !!}
        ({!!
            ucfirst((new \Cytonn\Lib\Helper())->convertNumberToWords($instruction->amount, true))
        !!}) from our {!! $instruction->custodialTransaction->custodialAccount->alias !!} {!! $instruction->custodialTransaction->custodialAccount->account_no !!} in favour of

        @if(is_null($instruction->client_account_id))
            {!! $instruction->custodialTransaction->client->investor_account_name !!}
        @else
            {!! $clientBankAccount->account_name !!}
        @endif
        .
        <br/><br/>

        Bank details are as shown below:<br/><br/>

        @if($trans = $instruction->receivingTransaction)
            <span class="bold">Bank Name</span>: {!! $trans->custodialAccount->bank_name !!}<br/>
            <span class="bold">Account Name</span>: {!! $trans->custodialAccount->account_name  !!} <br/>
            <span class="bold">Account Number</span>: {!! $trans->custodialAccount->account_number  !!}<br/>
            <span class="bold">Branch Name</span>: {!! $trans->custodialAccount->branch_name !!}<br/>
        @elseif(is_null($instruction->client_account_id))
            <?php $bankDetails = $instruction->custodialTransaction->client->bankDetails(); ?>
            <span class="bold">Bank Name</span>: {!! $bankDetails->bankName !!}<br/>
            <span class="bold">Account Name</span>: {!! $bankDetails->accountName !!} <br/>
            <span class="bold">Account Number</span>: {!! $bankDetails->accountNumber !!}<br/>
            <span class="bold">Branch Name</span>: {!! $bankDetails->branch !!}<br/>
            @if($bankDetails->swiftCode())
                <span class="bold">Swift Code</span>: {!! $bankDetails->swiftCode !!}<br/>
            @endif
            @if($bankDetails->clearingCode())
                <span class="bold">Clearing Code</span>: {!! $bankDetails->clearingCode !!}<br/>
            @endif
        @else
            <span class="bold">Bank Name</span>: @if($branch) {!! $branch->bank->name !!} @endif<br/>
            <span class="bold">Account Name</span>: {!! $clientBankAccount->account_name !!} <br/>
            <span class="bold">Account Number</span>: {!! $clientBankAccount->account_number !!}<br/>
            <span class="bold">Branch Name</span>: @if($branch) {!! $branch->name !!} @endif<br/>
            @if($branch)
                <span class="bold">Swift Code</span>: {!! $branch->bank->swift_code !!}<br/>
                <span class="bold">Clearing Code</span>: {!! $branch->bank->clearing_code !!}<br/>
            @endif
        @endif
        <br/>
        In case of any query kindly get in touch with us.

        <br/><br/>
        Yours Faithfully,
    </p>

    </br>
    <div class="bold"> For
        @if($paymentFor)
            {{ $paymentFor }}
        @else
            Cytonn Investments Management Ltd
        @endif
    </div>
    <br/><br/><br/>

    <span style="display: inline-block; width: 70%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($instruction->first_signatory_id) !!}
    </span>

    <span style="display: inline-block;  width: 50%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($instruction->second_signatory_id) !!}
    </span>

    @if($instruction->approval)
        <div>
            <p>Prepared by: {!! \Cytonn\Presenters\UserPresenter::presentFullNames($instruction->approval->sent_by) !!}</p>
            <p>Reviewed by: {!! \Cytonn\Presenters\UserPresenter::presentFullNames($instruction->approval->approved_by) !!}</p>
        </div>
    @endif

    <div style="position: absolute; bottom: 40px; left: 0;">
        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($instruction->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>
        <p class="left">REF: CRIMS/{!! $instruction->id !!}</p>
    </div>
@stop