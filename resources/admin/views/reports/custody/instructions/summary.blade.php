@extends('reports.letterhead')

@section('content')

    From: {!! $start->toFormattedDateString() !!}
    To: {!! $end->toFormattedDateString() !!}

    <table class="table table-striped" style="font-size:12px;">
        <thead>
        <tr>
            <th>Client code</th><th style=" width: 22%; white-space:nowrap;">Name</th><th style=" width: 11%; white-space:nowrap;">Date</th><th>Amount</th><th>Bank</th><th>Account Number</th><th>Branch</th>
            {{--<th>Clearing Code</th><th>Swift Code</th>--}}
        </tr>
        </thead>
        <tbody>
            @foreach($instructions as $instruction)
                <tr>
                    <td>
                        @if($client = $instruction->custodialTransaction->client)
                            {!! $client->client_code !!}
                        @endif
                    </td>
                    <td>
                        @if($client = $instruction->custodialTransaction->client)
                            {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}
                        @endif
                    </td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount)  !!}</td>
                    @if($instruction->clientBankAccount)
                        <td>{!! $instruction->custodialTransaction->account_name  !!}</td>
                        <td>{!! $instruction->custodialTransaction->account_number !!}</td>
                        <td>{!! $instruction->custodialTransaction->branch !!}</td>
                        {{--<td>{!! $instruction->custodialTransaction->clearing_code !!}</td>--}}
                        {{--<td>{!! $instruction->custodialTransaction->swift_code !!}</td>--}}
                    @elseif($client = $instruction->custodialTransaction->client)
                        <td>{!! $client->investor_bank !!}</td>
                        <td>{!! $client->investor_account_number !!}</td>
                        <td>{!! $client->investor_bank_branch !!}</td>
                        {{--<td>{!! $client->investor_clearing_code !!}</td>--}}
                        {{--<td>{!! $client->investor_swift_code !!}</td>--}}
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
@stop
