<table>
    <thead>
    <tr>
        <th colspan="4">{!! $project->name !!}</th>
    </tr>
    <tr>
        <th>Date</th><th>Client</th><th>FA</th><th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($project->p_transactions as $transaction)
        <?php $payment = $transaction->clientPayment; ?>
        <tr>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
            <td>
                @if($payment->client)
                    {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($payment->client_id) !!}
                @else
                    {!! $transaction->received_from !!} (Not On-boarded)
                @endif
            </td>
            <td>
                @if($re = $payment->recipient)
                    {!! $re->name !!}
                @elseif($client = $payment->client)
                    {!! @$client->getLatestFa()->name !!}
                @else
                    Not Known
                @endif
            </td>
            <td>
                {!! \Cytonn\Presenters\AmountPresenter::currency($transaction->amount) !!}
            </td>
        </tr>
    @endforeach
    <tr>
        <th colspan="3" class="left">Total</th><th class="left">{!! \Cytonn\Presenters\AmountPresenter::currency($project->p_transactions->sum('amount')) !!}</th>
    </tr>
    </tbody>
</table>