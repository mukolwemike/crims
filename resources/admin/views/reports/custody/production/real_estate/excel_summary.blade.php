<html>
    <body>
        @foreach($projects as $project)
            @include('reports.custody.production.real_estate.partials.project')
        @endforeach

        <table>
            <thead>
                <tr>
                    <th>Project</th><th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php $total = 0; ?>
                @foreach($projects as $project)
                    <?php $p = 0; ?>
                    <tr>
                        <td>{!! $project->name !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($p = $project->p_transactions->sum('amount')) !!}</td>
                    </tr>
                    <?php $total += $p; ?>
                @endforeach
                <tr>
                    <th>Total</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                </tr>
            </tbody>
        </table>
    </body>
</html>