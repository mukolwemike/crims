<table>
    <thead>
        <tr>
            <th colspan="5">{!! $fundManager->name !!} {!! $currency_code !!} Summary</th>
        </tr>
        <tr>
            <th>Date</th><th>Inflow</th><th>Outflow</th><th>Net</th><th>Interest</th>
        </tr>
    </thead>
    <tbody>
        <?php $inflows = 0;
        $outflows = 0;
        $interests = 0; ?>
        @foreach($dates as $date=>$currency)
            <?php $inflow = 0;
            $outflow = 0;
            $interest = 0; ?>
            <tr>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($date) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($inflow = $currency->p_transactions->sum('amount')) !!}</td>
                <td>{!! $outflow = $currency->p_withdrawals->sum('withdraw_amount') !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($inflow - $outflow) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($interest = $currency->p_interest->sum('amount')) !!}</td>
            </tr>
            <?php $inflows += $inflow;
            $outflows += $outflow; $interests += $interest?>
        @endforeach
        <tr>
            <th>Total</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($inflows) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($outflows) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($inflows - $outflows) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($interests) !!}</th>
        </tr>
    </tbody>
</table>