<table>
    <thead>
    <tr>
        <th colspan="6">{!! $fundManager->name !!} {!! $currency->code !!} Production</th>
    </tr>
    <tr>
        <th>Entry Date</th><th>Client Code</th><th>Client</th><th>FA</th><th>Custodial Account</th><th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($currency->p_transactions as $transaction)
        <?php $payment = $transaction->clientPayment; ?>
        <tr>
            <td>{!! $payment->date !!}</td>
            <td>
                @if($payment->client)
                    {!! $payment->client->client_code !!}
                @endif
            </td>
            <td>
                @if($payment->client)
                    {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($payment->client_id) !!}
                @else
                    {!! $transaction->received_from !!} (Not On-boarded)
                @endif
            </td>
            <td>
                @if($re = $payment->recipient)
                    {!! $re->name !!}
                @elseif($client = $payment->client)
                    {!! @$client->getLatestFa()->name !!}
                @else
                    Not Known
                @endif
            </td>
            <td>
                {!! $transaction->custodialAccount->account_name !!}
            </td>
            <td>
                {!! \Cytonn\Presenters\AmountPresenter::currency($transaction->amount) !!}
            </td>
        </tr>
    @endforeach
    <tr>
        <th class="left" colspan="5">Total</th><th class="left">{!! \Cytonn\Presenters\AmountPresenter::currency($inflow = $currency->p_transactions->sum('amount')) !!}</th>
    </tr>
    </tbody>
</table>