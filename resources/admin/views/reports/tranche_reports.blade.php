@extends('emails.email_plain')

@section('content')
    <style>
        tr .green, .green, .green td
        {
            background-color: #006666;
            color: #FFFFFF;
        }
    </style>
    <p>Below is a report of the tranches as at {!! \Cytonn\Presenters\DatePresenter::formatDate(\Carbon\Carbon::today()->toDateString()) !!}.</p>
    @foreach($tranches_grouped_by_project as $project => $tranches_arr)
        <table>
            <thead>
                <tr>
                    <th class="bold" colspan="5">{!! strtoupper(App\Cytonn\Models\Project::findOrFail($project)->name) !!}</th>
                </tr>
                <tr>
                    <th>Name</th>
                    <th>Total</th>
                    <th>Sold</th>
                    <th>Remaining</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($tranches_arr as $tranche)
                    <tr>
                        <td>{!! $tranche->name !!}</td>
                        <td>{!! $tranche->sizes->sum('number') !!} units</td>
                        <td>{!! $count = $tranche->holdings()->where('active', 1)->count() !!} units</td>
                        <td>{!! $tranche->sizes->sum('number') - $count !!} units</td>
                        <td></td>
                    </tr>
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr class="green">
                        <td></td>
                        <th>Size</th>
                        <th>Total</th>
                        <th>Payment Plan</th>
                        <th>Sold</th>
                    </tr>
                    @foreach($tranche->sizes as $sizing)
                        <tr>
                            <td></td>
                            <th>{!! App\Cytonn\Models\RealestateUnitSize::findOrFail($sizing->size_id)->name !!}</th>
                            <td>
                                @if(isset($sizing->number))
                                    {!! $sizing->number !!} units
                                @endif
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        @foreach($sizing->prices as $pricing)
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{!! $pricing->paymentPlan->name !!}</td>
                                <td>{!! $sold($tranche, $pricing->paymentPlan, $sizing->size) !!}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr><td colspan="5">&nbsp;</td></tr>
                @endforeach
            </tbody>
        </table>
        <br /><br />
    @endforeach
@endsection