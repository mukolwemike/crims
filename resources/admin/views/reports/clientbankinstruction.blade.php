@extends('reports.plain_letterhead_space')

@section('content')
    {!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}<br/><br/>

    Standard Chartered Bank Kenya Limited<br/>
    48 Westlands Road<br/>
    P.O. BOX 40984 - 00100<br/>
    Custody Service Department<br/>
    Nairobi.


    <p class="bold-underline">Attn: Joseph Oballa</p>

    Dear Sir,<br/>

    <p class="bold-underline"> RE:
        {!! $investment->product->currency->code !!}
        {!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'], false, 0) !!}
        ({!!
            strtoupper((new \Cytonn\Lib\Helper())->convertNumberToWords($data['amount'], false))
        !!}) CASH TRANSFER.
    </p>
    <p>Kindly transfer {!! $investment->product->currency->code !!}
        {!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'],  false, 0) !!}
        ({!!
            ucfirst((new \Cytonn\Lib\Helper())->convertNumberToWords($data['amount'], false))
        !!}) from our {!! $investment->product->custodialAccount->alias !!} {!! $investment->product->custodialAccount->account_no !!} in favour of

        @if(is_null($investment->bank_account_id))
            {!! $investment->client->investor_account_name !!}
        @else
            {!! $investment->bankAccount->account_name !!}
        @endif
        .
        <br/><br/>

        Bank details are as shown below:<br/><br/>
        <span class="bold">Bank Name</span>: @if($instruction->clientBankAccount->bank) {!! $instruction->clientBankAccount->bank->name !!} @endif<br/>
        <span class="bold">Account Name</span>: {!! $instruction->clientBankAccount->account_name !!} <br/>
        <span class="bold">Account Number</span>: {!! $instruction->clientBankAccount->account_number !!}<br/>
        <span class="bold">Branch Name</span>: @if($instruction->clientBankAccount->branch) {!! $instruction->clientBankAccount->branch->name !!} @endif<br/>
        @if($instruction->clientBankAccount->bank)
            <span class="bold">Swift Code</span>: {!! $instruction->clientBankAccount->bank->swift_code !!}<br/>
            <span class="bold">Clearing Code</span>: {!! $instruction->clientBankAccount->bank->clearing_code !!}<br/>
        @endif
        <br/>
        In case of any query kindly get in touch with us.

        <br/><br/>
        Yours Faithfully,
    </p>

    </br>
    <div class="bold"> For Cytonn Investments Management Ltd</div>
    <br/><br/><br/>

    <span style="display: inline-block; width: 70%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($data['first_signatory']) !!}
    </span>

    <span style="display: inline-block;  width: 50%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($data['second_signatory']) !!}
    </span>

    @if($instruction->approval)
        <div>
            <p>Prepared by: {!! \Cytonn\Presenters\UserPresenter::presentFullNames($instruction->approval->sent_by) !!}</p>
            <p>Reviewed by: {!! \Cytonn\Presenters\UserPresenter::presentFullNames($instruction->approval->approved_by) !!}</p>
        </div>
    @endif

    <div style="position: absolute; bottom: 40px; left: 0;">

        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($instruction->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>

        <p class="left">REF: CRIMS/{!! $instruction->id !!}</p>
    </div>
@stop