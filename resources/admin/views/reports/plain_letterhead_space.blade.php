<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .table{
            margin-bottom: 20px;
        }
        .justify, .justify p{
            text-justify: inter-word;
        }
        .table, .table tr, .table td, .table th{
            border: 1px solid #000;
            border-collapse: collapse;
        }
        .table tr td, .table tr th{
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 3px;
            padding-right: 3px;
        }
        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }
        .table-responsive{
            width: 100%;
        }
        body, body:before, body::after{
            background-color: #FFF;
        }
        html{
            background-color: #FFF !important;
        }
        .bold-underline{
            font-weight: 600;
            text-decoration: underline;
        }
        .bold{
            font-weight: 600;
        }
        .logo {
            width: 30%;
            display: inline;
        }
        .address{
            width: 20%;
            display: inline-block;
            position: absolute;
            left: 80%;
            font-family: 'Open Sans',sans-serif;
            font-size: 11px;
            z-index: 0;
        }
        .cytonn-rule{
            color: #006666;
            border-top: 3px solid #006666;
            margin-top: 27px;
        }
        .container{
            font-family: Arial, sans-serif;
            font-size: 14px;
        }
        .footer-table{
            width: 100%;
            background-color: #006666;
            color:#FFF;
            text-align: center;
            position: absolute;
            bottom: 10px;
        }
        .footer-quote{
            position: absolute;
            bottom: 10px;
            font-style: italic;
        }
        .pull-right{
            float: right !important;
        }
        .pull-left{
            float: left !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div id="header" style="height: 100px">

    </div>

    @yield('content')

    <div class="footer" id="footer">

        @yield('quote')

        @include('reports.partials.report_footer')

    </div>
</div>
</body>
</html>