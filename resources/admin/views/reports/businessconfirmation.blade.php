@extends('reports.letterhead')

@section('content')
    @include('reports.partials.client_address')

    <p class="bold-underline">CONFIRMATION OF INVESTMENT IN {!! strtoupper($product->longname ) !!}</p>
    <div class="body">
        <P>
            On behalf of Cytonn, we take this opportunity to thank you for taking up investments in our {!! ucwords( $product->longname ) !!} and choosing us to deliver to your investment promise.
        </P>

        <p>
            As per your application, your funds have been invested as per below;
        </p>

        @if($investment->product->present()->isSeip)
            <table class="table table-responsive">
                <tbody>
                <tr>
                    <th>Principal</th>
                    <th>Amount Payable on Maturity</th>
                    <th>Duration</th>
                    <th>Start Date</th>
                    <th>Maturity date</th>
                    <th>Agreed monthly contribution</th>
                </tr>
                <tr>
                    <td>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['principal'], true, 0) !!}</td>
                    <td>{!! $product->currency->symbol . ' '. \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getSeipFinalTotalValueOfInvestment()) !!}</td>
                    <td>{!! $investment->repo->getTenorInYears() !!} years </td>
                    <td>{!! \Cytonn\Core\DataStructures\Carbon::parse($investment->invested_date)->toFormattedDateString() !!} </td>
                    <td>{!! \Cytonn\Core\DataStructures\Carbon::parse($investment->maturity_date)->toFormattedDateString() !!}</td>
                    <td>{!! $product->currency->symbol . ' '. \Cytonn\Presenters\AmountPresenter::currency($investmentPaymentScheduleDetail->amount, true, 0) !!}</td>
                </tr>
                </tbody>
            </table>

            <p>Kindly note your monthly contribution is
                 {!! $product->currency->symbol . ' '. \Cytonn\Presenters\AmountPresenter::currency($investmentPaymentScheduleDetail->amount, true, 0) !!} payable on
                day <b>{!! $investmentPaymentScheduleDetail->payment_date !!}</b> of each month. See more from the attached payment
                schedule.</p>

            <p><b>Your next payment date is {!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($nextPaymentSchedule->date) !!}
                    .</b></p>
        @else
            <table class="table table-responsive">
                <thead>
                </thead>
                <tbody>
                <tr>
                    <th>Principal</th>
                    {{--<th>Conversion Rate</th>--}}
                    <th>Interest Rate</th><th>Duration</th><th>Start date</th><th>Maturity date</th>
                    @if($interval)
                        <th>Interest payment frequency</th>
                    @endif
                </tr>
                <tr>

                    <td align="center">{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['principal'], true, 0) !!}</td>
                    {{--<td align="center">{!! \Cytonn\Presenters\AmountPresenter::currency($product->currency->exchange) !!}</td>--}}
                    <td align="center">{!! $data['interest_rate'] !!} % p.a.</td>

                    @if($data['on_call'])
                        <td align="center"> - </td>
                    @else
                        <td align="center">{!! $data['duration'] !!} Days</td>
                    @endif

                    <td align="center">{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td>

                    @if($data['on_call'])
                        <td align="center">On call</td>
                    @else
                        <td align="center">{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td>
                    @endif
                    @if($interval)
                        <td>
                            Every {!! $interval !!} {!! str_plural('Month', $interval) !!}

                        </td>
                    @endif
                </tr>
                </tbody>
            </table>
        @endif


        <p>
            Following investment of your funds, you shall receive a statement via your email address at the end of the month confirming your investment and the interest accrued to keep you updated on your investment growth.
        </p>
        <p>
            Few days prior to maturity of your investment, we shall inform you so that you can give further direction on how to treat your investment.
        </p>
        <p>
            At Cytonn we are committed to delivering innovative and differentiated financial solutions that speak to you as a client.
        </p>
        <p>
            If you have any question, comment or need any assistance, we are at your service. Our relationship team will be at hand to assist you with all your investment needs. Please do not hesitate to contact us on +254 709 101 000 or email us at <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>
        </p>
        <p>
            Once again, we thank you for choosing us to serve your investment needs.
        </p>
    </div>
    <p>Thank you for investing with us.</p><br/>
    <p>Yours Sincerely,</p>
    <p class="bold"> For {!! $product->fundManager->fullname !!}<br/>
        <img src="{{ storage_path('resources/signatures/'.$signature) }}" height="60px"/>
    </p>
    <p><strong style="text-decoration: underline;">{!! $emailSender !!}</strong></p>

    {{--@if(is_array($data['sender']))--}}
        {{--@if(array_key_exists(0, $data['sender']))--}}
            {{--<span style="display: inline-block; width: 50%;">--}}
            {{--{!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($data['sender'][0]) !!}--}}
            {{--</span>--}}
        {{--@endif--}}
        {{--@if(array_key_exists(1, $data['sender']))--}}
            {{--<span style="display: inline-block; width: 50%;">--}}
            {{--{!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($data['sender'][1]) !!}--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--@else--}}
        {{--<span style="display: inline-block; width: 70%;">--}}
            {{--{!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($data['sender']) !!}--}}
            {{--</span>--}}
    {{--@endif--}}
@stop
