<style>
    .cytonn-green-bg
    {
        background-color: #006666;
        color: #FFF;
    }
</style>

@foreach(Product::all() as $product)
    @if($client->repo->hasProduct($product))
        <table>
            <thead>
            <tr class="cytonn-green-bg">
                <th colspan="8">{!! $product->name !!}</th>
            </tr>
            <tr>
                <th>Principal</th>
                <th>Invested date</th>
                <th>Maturity date</th>
                <th>Interest rate</th>
                <th>Gross Acc Interest</th>
                <th>Net Acc interest</th>
                <th>Withdrawal</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $interests = [];
            $principals = [];
            $totals = [];
            $investments = ClientInvestment::where('client_id', $client->id)->where('product_id', $product->id)->get();
            ?>
            @foreach($investments as $investment)
                <?php
                $principal = $investment->amount;
                $gross = $investment->getCurrentGrossInterest($investment->amount, $investment->interest_rate, $investment->invested_date, $investment->maturity_date);
                $net = $investment->getNetInterest($gross, $investment->client->taxable);
                $withdraw = $investment->repo->getWithdrawnAmount();
                $total = $principal + $net - $withdraw;

                if (!$investment->withdrawn) {
                    array_push($interests, $net);
                    array_push($principals, $principal); //only add to principal if not withdrawn
                    array_push($totals, $total);
                }
                ?>
                @if($investment->investment_type_id == 3)
                    <tr><td>Rollover</td><td colspan="7"></td></tr>
                @endif
                <tr align="right">
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($principal) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($investment->invested_date) !!}</td>
                    <td>{!! Cytonn\Presenters\DatePresenter::formatShortDate($investment->maturity_date)  !!}</td>
                    <td>{!! $investment->interest_rate !!}%</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($gross) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($net) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($withdraw) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</td>
                </tr>
            @endforeach
                <tr align="right" class="right">
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency(array_sum($principals)) !!}</th>
                    <th colspan="4"></th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency(array_sum($interests)) !!}</th>
                    <th></th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency(array_sum($totals)) !!}</th>
                </tr>
            </tbody>
        </table>
    @endif
@endforeach