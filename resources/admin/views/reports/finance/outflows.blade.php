<table>
    <thead>
        <tr>
            <th>Entry Date</th>
            <th>Value Date</th>
            <th>Reference no</th>
            <th>Account Number</th>
            <th>Account Name</th>
            <th>Debit</th>
            <th>Credit</th>

            @if($currency->code !== 'KES')
                <th>Equivalence in KSH</th>
                <th>Conversion Rate</th>
            @else
            @endif

            <th>Description</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data->p_transactions as $payment)

        <?php
            $custodialAccount = $payment->custodialTransaction->custodialAccount;
            $clientName = $payment->client ? $payment->client->name() : 'Unknown Client';
        ?>
        <tr>
            <td>{!! \Carbon\Carbon::parse($payment->created_at)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($payment->date)->toFormattedDateString() !!}</td>
            <td>{!! $payment->id !!}</td>
            <td>{!! $payment->client ? $payment->client->client_code : '0000' !!}</td>
            <td>{!! $payment->client ? $payment->client->name() : 'Unknown Client' !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount)) !!}</td>
            <td></td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount_in_ksh)) !!}</td>
                <td>{!! $payment->conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $payment->description !!}  - {!! $clientName !!}</td>
        </tr>

        <tr>
            <td>{!! \Carbon\Carbon::parse($payment->created_at)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($payment->date)->toFormattedDateString() !!}</td>
            <td>{!! $payment->id !!}</td>
            <td>{!! $custodialAccount->account_no ? $custodialAccount->account_no : $custodialAccount->account_name !!}</td>
            <td>{!! $custodialAccount->account_name !!} - {!! $custodialAccount->alias ? $custodialAccount->alias : null !!}</td>
            <td></td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount)) !!}</td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount_in_ksh)) !!}</td>
                <td>{!! $payment->conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $payment->description !!}  - {!! $clientName !!}</td>
        </tr>
    @endforeach
</table>
