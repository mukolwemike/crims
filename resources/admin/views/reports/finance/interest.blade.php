<table>
    <thead>
        <tr>
            <th>Date</th>
            <th>Reference no</th>
            <th>Account Number</th>
            <th>Account Name</th>
            <th>Debit</th>
            <th>Credit</th>

            @if($currency->code !== 'KES')
                <th>Equivalence KSH</th>
                <th>Conversion Rate</th>
            @else
            @endif

            <th>Description</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $investment)

        <?php
            $transaction = $investment->custodialInvestTransaction;
        ?>

        <tr>
            <td>{!! \Carbon\Carbon::parse($investment->interest_date)->toFormattedDateString() !!}</td>
            <td>{!! $investment->id !!}</td>
            <td>{!! $investment->client ? $investment->client->client_code : '0000' !!}</td>
            <td>{!! $investment->client ? $investment->client->name() : 'Unknown Client' !!}</td>
            <td>{!! abs($investment->accrued) !!}</td>
            <td></td>

            @if($currency->code !== 'KES')
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($investment->amount_in_ksh)) }}</td>
                <td>{{ $investment->conversion_rate }}</td>
            @else
            @endif

            <td>{{ $investment->desc }}</td>
        </tr>

        <tr>
            <td>{!! \Carbon\Carbon::parse($investment->interest_date)->toFormattedDateString() !!}</td>
            <td>{!! $investment->id !!}</td>
            <td>Interest Expense</td>
            <td>Interest Expense A\C</td>
            <td></td>
            <td>{!! abs($investment->accrued) !!}</td>

            @if($currency->code !== 'KES')
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($investment->amount_in_ksh)) }}</td>
                <td>{{ $investment->conversion_rate }}</td>
            @else
            @endif

            <td>{{ $investment->desc }}</td>
        </tr>

    @endforeach
</table>
