<table>
    <thead>
        <tr>
            <th>Reference no</th>
            <th>Account Number</th>
            <th>Account Name</th>
            <th>Entry Date</th>
            <th>Value Date</th>
            <th>Debit</th>
            <th>Credit</th>

            @if($currency->code !== 'KES')
                <th>Equivalence in KSH</th>
                <th>Conversion Rate</th>
            @else
            @endif

            <th>Description</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $tax)

        <?php
            $transaction = $tax->custodialTransaction;
            $custodialAccount = $transaction->custodialAccount;
            $investment = $tax->withdrawal->investment;
        ?>

        <tr>
            <td>{!! $transaction->id !!}</td>
            <td>{!! $investment->client ? $investment->client->client_code : '0000' !!}</td>
            <td>{!! $investment->client ? $investment->client->name() : 'Unknown Client' !!}</td>
            <td>{!! \Carbon\Carbon::parse($transaction->date)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($transaction->date)->toFormattedDateString() !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($transaction->amount)) !!}</td>
            <td></td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($tax->amount_in_ksh)) !!}</td>
                <td>{!! $tax->conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $tax->description !!}</td>
        </tr>

        <tr>
            <td>{!! $transaction->id !!}</td>
            <td>{!! $custodialAccount->account_no ? $custodialAccount->account_no : $custodialAccount->account_name !!}</td>
            <td>{!! $custodialAccount->account_name !!} - {!! $custodialAccount->alias ? $custodialAccount->alias : null !!}</td>
            <td>{!! \Carbon\Carbon::parse($transaction->date)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($transaction->date)->toFormattedDateString() !!}</td>
            <td></td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($transaction->amount)) !!}</td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($tax->amount_in_ksh)) !!}</td>
                <td>{!! $tax->conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $transaction->description !!}</td>
        </tr>
    @endforeach
</table>
