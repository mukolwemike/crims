<table>
    <thead>
    <tr>
        <th>Entry Date</th>
        <th>Value Date</th>
        <th>Reference no</th>
        <th>Account Number</th>
        <th>Account Name</th>
        <th>Debit</th>
        <th>Credit</th>

        @if($currency->code !== 'KES')
            <th>Equivalence in KSH</th>
            <th>Conversion Rate</th>
        @else
        @endif

        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $payment)

        <tr>
            <td>{!! \Carbon\Carbon::parse($payment->created_at)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($payment->date)->toFormattedDateString() !!}</td>
            <td>{!! $payment->id !!}</td>
            <td>{!! $payment->client ? $payment->client->client_code : '0000' !!}</td>
            <td>{!! $payment->client ? $payment->client->name() : 'Unknown Client' !!}</td>
            <td></td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount)) !!}</td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount_in_ksh)) !!}</td>
                <td>{!! $payment->conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $payment->description !!}</td>
        </tr>

        <tr>
            <td>{!! \Carbon\Carbon::parse($payment->parent->created_at)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($payment->parent->date)->toFormattedDateString() !!}</td>
            <td>{!! $payment->parent->id !!}</td>
            <td>{!! $payment->parent->client ? $payment->parent->client->client_code : '0000'  !!}</td>
            <td>{!! $payment->parent->client ? $payment->parent->client->name() : 'Unknown Client' !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->parent->amount)) !!}</td>
            <td></td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount_in_ksh)) !!}</td>
                <td>{!! $payment->conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $payment->parent->description !!}</td>
        </tr>
    @endforeach
</table>
