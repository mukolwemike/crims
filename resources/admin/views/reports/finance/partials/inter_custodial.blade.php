<table>
    <thead>
    <tr>
        <th>Entry Date</th>
        <th>Value Date</th>
        <th>Reference no</th>
        <th>Account Number</th>
        <th>Account Name</th>
        <th>Debit</th>
        <th>Credit</th>

        @if($currency->code !== 'KES')
            <th>Equivalence in KSH</th>
            <th>Conversion Rate</th>
        @else
        @endif

        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $transfer)

        <?php
                $sender = $transfer->sender;
                $receiver = $transfer->receiver;
                $outAccount = $sender->custodialAccount;
                $inAccount = $receiver->custodialAccount;
        ?>

        <tr>
            <td>{!! \Carbon\Carbon::parse($sender->created_at)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($sender->date)->toFormattedDateString() !!}</td>
            <td>{!! $transfer->id !!}</td>
            <td>{!! $outAccount->account_no ? $outAccount->account_no : $outAccount->account_name !!}</td>
            <td>{!! $outAccount->account_name !!} - {!! $outAccount->alias ? $outAccount->alias : null !!}</td>
            <td></td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($sender->amount)) !!}</td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->sender_amount_in_ksh)) !!}</td>
                <td>{!! $payment->sender_conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $sender->description !!}</td>
        </tr>

        <tr>
            <td>{!! \Carbon\Carbon::parse($sender->created_at)->toFormattedDateString() !!}</td>
            <td>{!! \Carbon\Carbon::parse($receiver->date)->toFormattedDateString() !!}</td>
            <td>{!! $transfer->id !!}</td>
            <td>{!! $inAccount->account_no ? $inAccount->account_no : $inAccount->account_name !!}</td>
            <td>{!! $inAccount->account_name !!} - {!! $inAccount->alias ? $inAccount->alias : null !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($receiver->amount)) !!}</td>
            <td></td>

            @if($currency->code !== 'KES')
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->receiver_amount_in_ksh)) !!}</td>
                <td>{!! $payment->receiver_conversion_rate !!}</td>
            @else
            @endif

            <td>{!! $receiver->description !!}</td>
        </tr>
    @endforeach
</table>
