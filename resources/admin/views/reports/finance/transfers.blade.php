<div class="">
    @if($type === 'InterClient')
        @include('reports.finance.partials.inter_client')
    @else
        @include('reports.finance.partials.inter_custodial')
    @endif
</div>