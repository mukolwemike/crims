@extends('reports.letterhead')

@section('content')
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Client Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif
    <p class="bold-underline uppercase">CONFIRMATION OF {!! $payment->present()->scheduleName() !!} FOR {!! $unit->size->name.' '.$unit->type->name !!} AT {!! $project->name !!}</p>
    <div class="body">
        <p>Cytonn Real Estate takes this opportunity to thank you for investing in our Real Estate product, {!! $project->name !!}.</p>

        This is to confirm that we are in receipt of your {!! $payment->present()->scheduleName() !!} for the {!! $unit->size->name !!} unit within the development. Below is the summary of the transaction:
        <br/><br>

        <table class="table table-bordered" style="min-width: 60%;">
            <?php $bc = $business_confirmation;
            if (!isset($bc->discount)) {
                $bc->discounted_price = $bc->price;
                $bc->discount = 0;
            }
            ?>
            <tbody>
            <tr><td>Project</td><td>{!! $bc->project !!}</td></tr>
            <tr><td>Unit price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($bc->price) !!}</td></tr>
            @if((float)$bc->discount > 0)
                <tr><td>Discount</td><td>{!! $bc->discount !!}%</td></tr>
                <tr><td>Discounted price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($bc->discounted_price) !!}</td></tr>
            @endif
            <tr><td>Unit number</td><td>{!! $bc->unit_number !!}</td></tr>
            @foreach($bc->payments as $p)
                <tr class="@if($p->id == $payment->id) bold @endif">
                    <td>{!! $p->name !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($p->amount) !!}</td>
                </tr>
            @endforeach
            <tr><td>Total Amount Paid</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($bc->payments->sum('amount')) !!}</td></tr>
            <tr><td>Payment Pending</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($amountRemaining) !!}</td></tr>
            </tbody>
        </table>

        <p>We shall keep you posted on any developments as they occur.</p>

        <p>If you have any question, comment or need any assistance, we are at your service. Our relationship team will be at hand to assist you with all your real-estate investment needs. Please do not hesitate to contact us on +254 709 101 000 email us at
            <a href="mailto:operations@cytonn.com">operations@cytonn.com</a></p>

        <p>Once again, we thank you for choosing us to serve your investment needs and look forward to delivering to our investment promise.
        </p><br/>
        <p>Yours Sincerely,</p>
        <p class="bold"> For Cytonn Investments Management Ltd<br/>
            @if($sender_id != 1)
                {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender_id) !!}
            @else
                <img src="{{ storage_path('resources/signatures/'.$signature) }}" height="60px"/>
                <p><strong style="text-decoration: underline;">{!! $emailSender !!}</strong></p>
            @endif
        </p>
    </div>
@stop
