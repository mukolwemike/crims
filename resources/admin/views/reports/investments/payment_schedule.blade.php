@extends('reports.letterhead')

@section('content')
    @include('reports.partials.client_address')

    <p class="bold-underline">{!! strtoupper($product->longname ) !!} INVESTMENT SCHEDULE</p>
    <div class="body">
        <p>
            Please see below the schedule of your investment in our {!! ucwords( $product->longname ) !!}.
        </p>

        <?php $currency = $product->currency ?>

        <table class="table table-bordered table-responsive">
            <thead>
            <tr class="green-header">
                <th>Month</th>
                <th>Amount Contributed ({!! $currency->code !!})</th>
                <th>Interest ({!! $currency->code !!})</th>
                <th>Total ({!! $currency->code !!})</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{!! \Carbon\Carbon::parse($investment->invested_date)->toFormattedDateString() !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount, true, 0) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalNetInterest(), true, 0) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment(), true, 0) !!}</td>
            </tr>
            @foreach($schedules as $schedule)
                <tr>
                    <td>{!! Carbon\Carbon::parse($schedule->date)->toFormattedDateString() !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount, true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->repo->calculate()->net_interest, true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->repo->calculate()->total, true, 0) !!}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Totals</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount + (float) $schedules->sum('amount'), true, 0) !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->totalNetInterestOfSchedules() + $investment->repo->getFinalNetInterest(), true, 0) !!}</th>
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->totalValueOfSchedules() + $investment->repo->getFinalTotalValueOfInvestment(), true, 0) !!}</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <p>Thank you for investing with us.</p>
    <p>Yours Sincerely,</p>
    <p class="bold"> For {!! $product->fundManager->fullname !!}<br/>
        <img src="{{ storage_path('resources/signatures/'.$signature) }}" height="60px"/>
    </p>
    <p><strong style="text-decoration: underline;">{!! $emailSender !!}</strong></p>
@stop