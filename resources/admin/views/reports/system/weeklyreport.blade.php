@extends('reports.letterhead')

@section('content')
 <div>
     <p>From: {{ $start->toDayDateTimeString() }}</p>
     <p>To: {{ $end->toDayDateTimeString() }}</p>

     <p class="bold-underline">CRIMS ACTIVITY REPORT</p>

     @foreach($sections as $title => $rows)
         <h4>{{ $title }}</h4>

         @if(count($rows))
             <table class="table table-striped table-responsive">
                 <thead>
                 <tr>
                     @foreach(array_keys(array_first($rows)) as $header)
                         <th> {{ $header }}</th>
                     @endforeach
                 </tr>
                 </thead>
                 <tbody>
                 @foreach($rows as $row)
                     <tr>
                         @foreach($row as $c)
                             <td>{{ $c }}</td>
                         @endforeach
                     </tr>
                 @endforeach
                 </tbody>
             </table>
         @else
             <p>No Records found.</p>
         @endif
     @endforeach
 </div>
@endsection