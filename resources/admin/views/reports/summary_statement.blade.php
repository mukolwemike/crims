@extends('reports.letterhead')

@section('content')
{{--    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}--}}
    {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Client Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">RE: STATEMENT OF ACCOUNT</p>

    <p>Kindly see below your {!! strtoupper($product->shortname) !!} statement showing account status as at {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}.<br/><br/></p>
    <table class="table table-striped">

        <thead>
            <tr>
                <th>Principal</th><th>Invested date</th><th>Statement date</th><th>Interest rate</th><th>Gross Acc Interest</th>
                <th>Net Acc interest</th><th>Withdrawal</th>
                @if($data['has_topup'] == true)<th>Top up</th>@endif
                <th>Total</th>
                <th>Maturity date</th>
            </tr>
        </thead>

        <tbody>
            @foreach($data['investments'] as $investment)
                @if($investment->statement->rollover)
                    @if($data['has_topup'] == true)
                        <tr><td>Rollover</td><td colspan="9"></td></tr>
                    @else
                        <tr><td>Rollover</td><td colspan="8"></td></tr>
                    @endif
                @endif
                <tr align="right">
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->statement->principal, false, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($investment->statement->value_date) !!}</td>
                    @if($investment->on_call)
                        <td>On call</td>
                    @else
                        <td>{!! Cytonn\Presenters\DatePresenter::formatShortDate($statementDate)  !!}</td>
                    @endif
                    <td>{!! $investment->statement->interest_rate !!}%</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->statement->gross_interest, false, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->statement->net_interest, false, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->statement->withdrawal, false, 0) !!}</td>
                    @if($data['has_topup'] == true)<td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->statement->topup, false, 0) !!}</td>@endif
                    <td class="@if(!$investment->statement->withdrawn) bold @endif">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->statement->total, false, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($investment->statement->maturity_date) !!}</td>
                </tr>
            @endforeach
            <tr align="right" class="right">
                <th>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['total_principal'], false, 0) !!}</th>
                <th colspan="4"></th>
                <th>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['total_interest'], false, 0) !!}</th>
                <th></th>
                @if($data['has_topup'] == true)<th></th>@endif
                <th>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['total_client_value'], false, 0) !!}</th>
                <th></th>
            </tr>
        </tbody>
    </table>

    <br/>
    <p>Thank you for investing with us.</p><br/>
    <p>Yours Sincerely,</p>
    </br>
    <p class="bold"> For Cytonn Investments Management Ltd<br/>
        {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender) !!}
    </p>
@stop

@section('quote')
    <p class="footer-quote">
        <?php $quote = StatementQuote::latest()->first(); ?>

        @if(!is_null($quote))
            {!! $quote->quote !!}
        @endif

    </p>
@stop