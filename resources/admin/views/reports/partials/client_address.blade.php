{!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

{!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}
@if($client->account_name)
    - {!! $client->account_name !!}
@endif
<br/>

@if($client->clientType->name == 'corporate')
    C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!} <br/>
@endif
(Partner Code – {!! $client->client_code !!})<br/>
{!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

@if($client->clientType->name == 'corporate')
    Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
@else
    Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
@endif