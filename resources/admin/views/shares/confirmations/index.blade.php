@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "SharesBusinessConfirmationController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <th colspan = "6"></th>
                        <th>
                            <select st-search="confirmation" class="form-control">
                                <option value="">All</option>
                                <option value="sent">Sent</option>
                                <option value="not_sent">Not Sent</option>
                            </select>
                        </th>
                        <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                    </tr>
                    <tr>
                        <th>Membership No.</th>
                        <th>Shareholder</th>
                        <th>No. of Shares</th>
                        <th>Shares Price</th>
                        <th>Shares Purchase</th>
                        <th>Narrative</th>
                        <th>Date</th>
                        <th>Confirmation</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed">
                        <td><% row.number %></td>
                        <td><% row.fullName %></td>
                        <td><% row.number %></td>
                        <td><% row.share_price %></td>
                        <td><% row.share_purchase | currency:"" %></td>
                        <td><% row.narrative %></td>
                        <td><% row.date | date %></td>
                        <td><span to-html = "row.confirmation | confirmationStatus"></span></td>
                        <td>
                            <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/shares/confirmations/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="9" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "2" class = "text-center">
                        Items per page
                    </td>
                    <td colspan = "3" class = "text-center">
                        <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-displayed-pages = "10"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop