@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Send Shares Business Confirmation</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Membership No.</td>
                        <td>{!! $holding->shareHolder->number !!}</td>
                    </tr>
                    <tr>
                        <td>Shareholder</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($holding->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>No. of Share</td>
                        <td>{!! $holding->number !!}
                    </tr>
                    <tr>
                        <td>Share Price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->purchase_price) !!}
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($holding->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Narrative</td>
                        <td>{!! @$holding->payment->narrative !!}
                    </tr>
                    <tr>
                        <td>Sent</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($is_bc_sent) !!}</td>
                    </tr>
                </tbody>
            </table>

            <div class = "panel panel-success">
                <div class = "panel-heading">
                    <h4>Shares Business confirmation</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>No. of Shares</th>
                                <th>Purchase Price</th>
                                <th>Shares Purchase</th>
                                <th>Date</th>
                                <th>Narrative</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{!! $holding->number !!}</td>
                                <td>{!! $holding->purchase_price !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->number * $holding->purchase_price) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($holding->date) !!}</td>
                                <td>{!! @$holding->payment->narrative !!}</td>
                            </tr>
                        </tbody>
                    </table>
                    {!! Form::open(['url'=>'/dashboard/shares/confirmations/send/'.$holding->id]) !!}
                        <a href = "/dashboard/shares/confirm/preview/{!! $holding->id !!}"
                           class = "btn btn-primary"><i class = "fa fa-file-pdf-o"></i> Preview business confirmation</a>
                        @if($is_bc_sent)
                            <a class="btn btn-danger" data-toggle="modal" data-target="#confirmBCResend"><i class="fa fa-envelope"></i> Resend Confirmation</a>
                        @else
                            {!! Form::button('<i class="fa fa-envelope"></i> Send Confirmation', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                        @endif
                    {!! Form::close() !!}
                </div>

                <div class = "panel-heading">
                    <h4>Previous Shares Business confirmations</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>No. of Shares</th>
                                <th>Purchase Price</th>
                                <th>Shares Purchase</th>
                                <th>Date</th>
                                <th>Narrative</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($previousConfirmations as $confirmation)
                                <tr>
                                    <td>{!! $confirmation->holding->number !!}</td>
                                    <td>{!! $confirmation->holding->purchase_price !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($confirmation->holding->number * $confirmation->holding->purchase_price) !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($confirmation->holding->date) !!}</td>
                                    <td>{!! $confirmation->holding->narrative !!}</td>
                                    <td>
                                      <a ng-controller = "PopoverCtrl" uib-popover = "View PDF" popover-trigger = "mouseenter" href = "/dashboard/shares/confirmations/previous/preview/{!! $confirmation->id !!}"><i class = "fa fa-list-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
        <!-- Modal -->
        <div class="modal fade" id="confirmBCResend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['url'=>'/dashboard/shares/confirmations/send/'.$holding->id]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send Business Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>This business confirmation has already been sent. Are you sure that you want to resend it?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        {!! Form::submit('Resend', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection