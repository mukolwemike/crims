@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Send Shares Sale Order Confirmation</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Seller</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($seller->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $seller->number !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale_order->number, true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale_order->price) !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($sale_order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Recipient</td>
                        <td>@if($sale_order->recipient) {!! $sale_order->recipient->name !!} @endif</td>
                    </tr>
                    <tr>
                        <td>Commission Rate</td>
                        <td>{!! $sale_order->commission_rate !!}</td>
                    </tr>
                    <tr>
                        <td>Matched</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($sale_order->matched) !!}</td>
                    </tr>
                </tbody>
            </table>

            <div class = "panel panel-success">
                <div class = "panel-heading">
                    <h4>Shares Business confirmation</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>No. of Shares</th>
                                <th>Price</th>
                                <th>Shares Purchase</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{!! $sale_order->number !!}</td>
                                <td>{!! $sale_order->price !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale_order->number * $sale_order->price) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($sale_order->request_date) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                    {!! Form::open(['url'=>'Dashboard'.$sale_order->id]) !!}
                        <a href = "/dashboard/shares/sales/confirmations/preview/{!! $sale_order->id !!}"
                           class = "btn btn-primary"><i class = "fa fa-file-pdf-o"></i> Preview Sale Order confirmation</a>
                        @if($is_soc_sent)
                            <a class="btn btn-danger" data-toggle="modal" data-target="#confirmSOCResend"><i class="fa fa-envelope"></i> Resend Confirmation</a>
                        @else
                            {!! Form::button('<i class="fa fa-envelope"></i> Send Confirmation', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                        @endif
                    {!! Form::close() !!}
                </div>

                <div class = "panel-heading">
                    <h4>Previous Shares Sale Order Confirmations</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>No. of Shares</th>
                                <th>Sale Price</th>
                                <th>Shares Purchase</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($previousConfirmations as $confirmation)
                                <tr>
                                    <td>{!! $confirmation->saleOrder->number !!}</td>
                                    <td>{!! $confirmation->saleOrder->price !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($confirmation->saleOrder->number * $confirmation->saleOrder->price) !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($confirmation->saleOrder->request_date) !!}</td>
                                    <td>
                                      <a ng-controller = "PopoverCtrl" uib-popover = "View PDF" popover-trigger = "mouseenter" href = "/dashboard/shares/sales/confirmations/previous-preview/{!! $confirmation->id !!}"><i class = "fa fa-list-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
        <!-- Modal -->
        <div class="modal fade" id="confirmSOCResend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['url'=>'/dashboard/shares/sales/confirmations/send/'.$sale_order->id]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send Shares Sale Order Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>This shares sale order confirmation has already been sent. Are you sure that you want to resend it?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        {!! Form::submit('Resend', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection