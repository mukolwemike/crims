@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1" xmlns="http://www.w3.org/1999/html">
        <div class="panel-dashboard">
            <div ng-controller="ShareholdersSharesSalesGridController">
                <div class="btn-group">
                    <a type="button" href="/dashboard/shares/shareholders/show/{!! $holder->id !!}" class="btn btn-primary margin-bottom-20" target="_self"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sales-order">Sell Shares</button>
                </div>
                <h4 class="col-md-12 bold">Shares Sales Orders</h4>
                <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <th colspan="6"></th>
                        <th>
                            <select st-search="matched" class="form-control">
                                <option value="">Filter</option>
                                <option value="matched">Matched</option>
                                <option value="unmatched">Unmatched</option>
                            </select>
                        </th>
                        <th>
                            <select st-search="cancelled" class="form-control">
                                <option value="">All Orders</option>
                                <option value="cancelled">Cancelled</option>
                                <option value="active">Active</option>
                            </select>
                        </th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search seller..." type="text"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Number</th>
                        <th>Sold</th>
                        <th>Price</th>
                        <th>Remaining</th>
                        <th>Request Date</th>
                        <th>GTF/C</th>
                        <th>Expiry Date</th>
                        <th>Matched</th>
                        <th>Cancelled</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed | filter:search">
                        <td><% row.number %></td>
                        <td><% row.sold %></td>
                        <td><% row.price %></td>
                        <td><% row.number - row.sold %></td>
                        <td><% row.request_date %></td>
                        <td><span to-html ="row.good_till_filled_cancelled | yesno"></span></td>
                        <td><% row.expiry_date %></td>
                        <td><span to-html ="row.matched | booleanIcon"></span></td>
                        <td><span to-html ="row.cancelled | yesno"></span></td>
                        <td><a href="/dashboard/shares/sales/show/<% row.id%>" target="_self"><i class="fa fa-list-alt"></i> </a></td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="10" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan = "5" class = "text-center">
                            Items per page <input type = "text" ng-model = "itemsByPage"/>
                        </td>
                        <td colspan = "5" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop


<!-- Make Shares Sales Order Modal -->
<div class="modal fade" id="sales-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['make_shares_sales_order', $holder->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Shares Sales Order</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden('seller_id', $holder->id) !!}
                    {!! Form::label('request_date', 'Date of Request') !!}

                    {!! Form::text('request_date', null, ['class'=>'form-control', 'init-model'=>'request_date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'request_date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number', 'Number of Shares ( Current - '.$current_shares.' shares )') !!}

                    {!! Form::number('number', null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'number') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'Price per Share') !!}

                    {!! Form::number('price', null, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'price') !!}
                </div>

                <div class="form-group">
                    {!! Form::checkbox('good_till_filled_cancelled', true, true, ['ng-model'=>'good_till_filled_cancelled', 'ng-init'=>'good_till_filled_cancelled = true']) !!}

                    {!! Form::label('good_till_filled_cancelled', 'Good Till Filled / Cancelled') !!}<br/></br>
                </div>

                <div class="form-group" ng-show="!good_till_filled_cancelled">
                    {!! Form::label('expiry_date', 'Good Till Expiry. Expiry Date') !!}

                    {!! Form::text('expiry_date', null, ['class'=>'form-control', 'init-model'=>'expiry_date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt_.open', 'ng-focus'=>'dt_.open = !dt_.open']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'expiry_date') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Sell Shares', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>