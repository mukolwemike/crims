@extends('layouts.default')
@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <h3>Sale Order Details</h3>
            <table class="table table-striped table-hover table-responsive">
                <tbody>
                    <tr>
                        <td>Seller</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($seller->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $seller->number !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! $order->number !!}</td>
                    </tr>
                    <tr>
                        <td>Bought Shares</td>
                        <td>{!! $sold !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! $order->price !!}</td>
                    </tr>
                    <tr>
                        <td>Remaining Shares</td>
                        <td>{!! $order->number - $sold !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($order->good_till_filled_cancelled) !!}</td>
                    </tr>
                    @if(!$order->good_till_filled_cancelled)
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->expiry_date) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Matched</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($order->matched) !!}</td>
                    </tr>
                </tbody>
            </table>
            <div class="btn-group">
                @if(!$order->cancelled)
                    <a href="/dashboard/shares/sales/confirmations/show/{!! $order->id !!}" class="btn btn-default">Order Confirmation</a>
                    @if(!$order->matched)
                        <button class="btn btn-danger" data-toggle="modal" data-target="#cancel-order"><i class="fa fa-remove"></i> Cancel Order</button>
                    @else
                        <button class="btn btn-danger" disabled><i class="fa fa-remove"></i> Cancel Order</button>
                    @endif
                @else
                    <button class="btn btn-danger" disabled><i class="fa fa-remove"></i> Already Cancelled</button>
                @endif
            </div>
        </div>

        @if(count($pendingPurchases))
            <div class="panel-dashboard">
                <settle-share-purchases
                        :order="{{ $order->id }}"
                        :purchases="{{ json_encode($pendingPurchases) }}"
                >

                </settle-share-purchases>
            </div>
        @endif


        @if(! $order->matched)
            <div class="panel-dashboard">
                <h3>Open Purchase Orders</h3>
                @if($purchases->count())
                    {!! Form::open(['route'=>['match_sale_to_purchase', $order->id], 'method'=>'POST']) !!}
                    <table class="table table-striped table-hover table-responsive">
                        <thead>
                        <tr>
                            <th>Buyer</th>
                            <th>Buyer Number</th>
                            <th>No.of Shares</th>
                            <th>Price</th>
                            <th>Request Date</th>
                            <th>GTF/C</th>
                            <th>Expiry Date</th>
                            <th>Match</th>
                        </tr>
                        </thead>
                        <tbody ng-controller="SellSharesController" ng-init="sales_order = {!! $order->number - $sold !!}">
                        @foreach($purchases as $purchase)
                            <tr>
                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($purchase->buyer->client_id) !!}</td>
                                <td>{!! $purchase->buyer->number !!}</td>
                                <td>{!! $purchase->number !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase->price) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchase->request_date) !!}</td>
                                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($purchase->good_till_filled_cancelled) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchase->expiry_date) !!}</td>
                                <td>{!! Form::checkbox('purchase_order_id[]', $purchase->id, false, ['ng-disabled'=>'checkboxes_disabled', 'ng-model'=>'checkboxes['. $purchase->id .']', 'ng-change'=>'selected(' . $purchase->id . ', ' . $purchase->number . ')']) !!}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><div ng-bind-html="hidden_form_data"></div></td>

                            <td colspan="1" ng-controller="DatepickerCtrl" class="form-inline">
                                {!! Form::text('date', NULL, [
                                    'class' => 'form-control',
                                    'datepicker-popup init-model' => "date",
                                    'is-open' => "status.opened",
                                    'ng-focus' => 'open($event)',
                                    'ng-required' => 'true',
                                    'placeholder' => 'Enter Match Date'
                                ]) !!}
                            </td>

                            <td colspan="2" class="form-inline">
                                {!! Form::text('bargain_price', NULL, ['class'=>'form-control', 'placeholder' => 'Bargained Price (optional)']) !!}
                            </td>

                            <td colspan="6">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" ng-click="clearSelected()">Clear</button>
                                    <button type="submit" class="btn btn-success" ng-disabled="submit_disabled">Make Sale (<small><% total_count %> shares</small>)</button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {!! Form::close() !!}
                @else
                    There are no open purchase orders that can match this order available!
                @endif
            </div>
        @endif
    </div>

    <div class="modal fade" id="cancel-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">Cancel Sale Order</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to cancel this shares sale order?</p>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>['cancel_shares_sale_order', $order->id]]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Go Back</button>
                    {!! Form::button('<i class="fa fa-remove"></i> Cancel Order', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop