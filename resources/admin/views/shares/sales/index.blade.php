@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="SharesSalesGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="8">
                            <a href="/dashboard/shares/sales/notify-unmatched-orders/" class="btn btn-info"><i class="fa fa-envelope"></i> Notify Unmatched Orders</a>
                        </th>
                        <th>
                            <select st-search="matched" class="form-control">
                                <option value="">Filter</option>
                                <option value="matched">Matched</option>
                                <option value="unmatched">Unmatched</option>
                            </select>
                        </th>
                        <th>
                            <select st-search="cancelled" class="form-control">
                                <option value="">All Orders</option>
                                <option value="cancelled">Cancelled</option>
                                <option value="active">Active</option>
                            </select>
                        </th>
                        <th colspan="3"><input st-search="" class="form-control" placeholder="Search seller..." type="text"/></th>
                    </tr>
                    <tr>
                        <th st-sort="id">ID</th>
                        <th st-sort="seller_number">Seller Number</th>
                        <th>Seller</th>
                        <th>Number</th>
                        <th>Price</th>
                        <th>Sold</th>
                        <th>Remaining</th>
                        <th>Request Date</th>
                        <th>GTF/C</th>
                        <th>Expiry Date</th>
                        <th>Matched</th>
                        <th>Cancelled</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed | filter:search">
                        <td><% row.id %></td>
                        <td><% row.seller_number %></td>
                        <td><% row.seller %></td>
                        <td><% row.number %></td>
                        <td><% row.price %></td>
                        <td><% row.sold %></td>
                        <td><% row.number - row.sold %></td>
                        <td><% row.request_date %></td>
                        <td><span to-html ="row.good_till_filled_cancelled | yesno"></span></td>
                        <td><% row.expiry_date %></td>
                        <td><span to-html ="row.matched | booleanIcon"></span></td>
                        <td><span to-html ="row.cancelled | yesno"></span></td>
                        <td><a href="/dashboard/shares/sales/show/<% row.id%>" target="_self"><i class="fa fa-list-alt"></i> </a></td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="13" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "7" class = "text-center">
                        Items per page <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "6" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop