@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <table class="table table-striped table-responsive">
            <thead>
                <th colspan="3">
                    <button class="btn btn-primary margin-bottom-20" data-toggle="modal" data-target="#add-share-entity"><i class="fa fa-plus-square-o"></i> Add Share Entity</button>
                </th>
                <tr>
                    <th>Entity</th>
                    <th>Max Holding</th>
                    <th>Share Price</th>
                    <th>Sold Shares</th>
                    <th>Remaining Shares</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($entities as $entity)
                <tr>
                    <td>{!! $entity->name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->maximumIssues(), true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->sharePrice(), true, 2) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->soldShares(), true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->remainingShares(), true, 0) !!}</td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="View" popover-trigger="mouseenter" href="/dashboard/shares/entities/show/{!! $entity->id !!}"><i class="fa fa-list-alt"></i></a>
                    </td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="Edit Entity" popover-trigger="mouseenter" data-toggle="modal" data-target="#edit-share-entity{!! $entity->id !!}"><i class="fa fa-edit"></i></a>

                        <!-- START of edit of Entity Share Modal -->
                        <div class="modal fade" id="edit-share-entity{!! $entity->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::model($entity, ['route'=>['edit_share_entity', $entity->id]]) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit Entity Share</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="form-group">
                                              {!! Form::label('name', 'Name') !!}
                                              {!! Form::text('name', $entity->name, ['class'=>'form-control', 'required'=>'true']) !!}
                                              {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                                        </div>

                                        <div class="form-group">
                                              {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                                              {!! Form::select('fund_manager_id', $fundManagers, null, ['class'=>'form-control', 'required'=>'true']) !!}
                                              {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_manager_id') !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('account_id', 'Custodial Account') !!}
                                            {!! Form::select('account_id', $custodialAccounts, null, ['class'=>'form-control', 'required'=>'true']) !!}
                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_id') !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('currency_id', 'Currency') !!}
                                            {!! Form::select('currency_id', $currencies, null, ['class'=>'form-control', 'required'=>'true']) !!}
                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'currency_id') !!}
                                        </div>

                                        {{--<div class="form-group">--}}
                                            {{--{!! Form::label('max_holding', 'Maximum Holding') !!}--}}
                                            {{--{!! Form::text('max_holding', $entity->maximumIssues(), ['class'=>'form-control', 'required'=>'true']) !!}--}}
                                            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'max_holding') !!}--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <!-- END of edit Share Enity modal -->

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @include('shares.entities.partials.add_share_entity')
@endsection