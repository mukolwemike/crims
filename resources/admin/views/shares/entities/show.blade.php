@extends('layouts.default')
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div class="btn-group" role="group" aria-label="...">
                <a class="btn btn-primary" data-toggle="modal" data-target="#issue-shares">Issue Shares</a>
            </div>
            <h3>Entity Details</h3>
            <table class="table table-striped table-hover table-responsive">
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Share Price</td>
                        <td>
                            {!! \Cytonn\Presenters\AmountPresenter::currency($entity->sharePrice()) !!}
                            <span style="margin-left: 20px;cursor: pointer;"><a data-target="#edit-share-price" data-toggle="modal"><i class="fa fa-edit"></i></a></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Total Number of Issuable Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->maximumIssues(), true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares Sold</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->soldShares(), true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares Remaining</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->remainingShares(), true, 0) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

<div class="modal fade" id="issue-shares" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['make_shares_entity_issue', $entity->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Issue Shares</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('issue_date', 'Date of Issue') !!}

                    {!! Form::text('issue_date', null, ['class'=>'form-control', 'init-model'=>'issue_date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'issue_date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number', 'Number of Shares') !!}

                    {!! Form::number('number', null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'number') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'Price per Share') !!}

                    {!! Form::number('price', null, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'price') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
                {!! Form::submit('Issue Shares', ['class'=>'btn btn-primary pull-right']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="edit-share-price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['edit_shares_price', $entity->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Share Price</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('price', 'Price per Share') !!}

                    {!! Form::number('price', $entity->sharePrice(), ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'price') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
                {!! Form::submit('Edit Price', ['class'=>'btn btn-primary pull-right']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>