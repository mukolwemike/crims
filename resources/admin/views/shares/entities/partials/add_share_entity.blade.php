<!-- Add share entity modal -->
<div class="modal fade" id="add-share-entity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
            <div class="modal-content">
                  {!! Form::open(['route'=>'add_share_entity']) !!}
                  <div class="modal-header">
                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">Add Share Entity</h4>
                  </div>
                  <div class="modal-body">

                        <div class="form-group">
                              {!! Form::label('name', 'Name') !!}
                              {!! Form::text('name', null, ['class'=>'form-control', 'required'=>'true']) !!}
                              {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                        </div>

                        <div class="form-group">
                              {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                              {!! Form::select('fund_manager_id', $fundManagers, null, ['class'=>'form-control']) !!}
                              {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_manager_id') !!}
                        </div>

                        <div class="form-group">
                              {!! Form::label('account_id', 'Custodial Account') !!}
                              {!! Form::select('account_id', $custodialAccounts, null, ['class'=>'form-control']) !!}
                              {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_id') !!}
                        </div>

                        <div class="form-group">
                              {!! Form::label('currency_id', 'Currency') !!}
                              {!! Form::select('currency_id', $currencies, null, ['class'=>'form-control']) !!}
                              {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'currency_id') !!}
                        </div>

                        {{--<div class="form-group">--}}
                              {{--{!! Form::label('max_holding', 'Maximum Holding') !!}--}}
                              {{--{!! Form::number('max_holding', null, ['class'=>'form-control', 'min'=>100, 'step'=>1, 'required'=>'true']) !!}--}}
                              {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'max_holding') !!}--}}
                        {{--</div>--}}

                  </div>
                  <div class="modal-footer clearfix">
                        {!! Form::submit('Add', ['class'=>'btn btn-success']) !!}
                        <a data-dismiss="modal" class="btn btn-default">Close</a>
                  </div>
                  {!! Form::close() !!}
            </div>
      </div>
</div>