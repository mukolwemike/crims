@extends('layouts.default')
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Purchase Order Details</h3>
            <table class="table table-striped table-hover table-responsive">
                <tbody>
                    <tr>
                        <td>Buyer</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($buyer->client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $buyer->number !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! $order->number !!}</td>
                    </tr>
                    <tr>
                        <td>Bought Shares</td>
                        <td>{!! $bought !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! $order->price !!}</td>
                    </tr>
                    <tr>
                        <td>Remaining Shares</td>
                        <td>{!! $order->number - $bought !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($order->good_till_filled_cancelled) !!}</td>
                    </tr>
                    @if(!$order->good_till_filled_cancelled)
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->expiry_date) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Matched</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($order->matched) !!}</td>
                    </tr>
                    @if($order->recipient)
                        <tr>
                            <td>Commission Recipient</td>
                            <td>{!! $order->recipient->name !!}</td>
                        </tr>
                        <tr>
                            <td>Commission rate</td>
                            <td>{!! $order->commission_rate !!}%</td>
                        </tr>
                        <tr>
                            <td>Commission Start Date</td>
                            <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($order->commission_start_date) !!}</td>
                        </tr>
                    @endif

                </tbody>
            </table>
            <div class="btn-group">
                @if(!$order->cancelled)
                    <a href="/dashboard/shares/purchases/confirmations/show/{!! $order->id !!}" class="btn btn-default">Order Confirmation</a>
                    @if(!$order->matched)
                        <button class="btn btn-danger" data-toggle="modal" data-target="#cancel-order"><i class="fa fa-remove"></i> Cancel Order</button>
                    @else
                        @if($order->boughtFromEntity())
                            <button class="btn btn-danger" data-toggle="modal" data-target="#reverse-order-from-entity"><i class="fa fa-remove"></i> Reverse Order from Entity</button>
                        @endif
                        <button class="btn btn-danger" disabled><i class="fa fa-remove"></i> Cancel Order</button>
                    @endif
                @else
                    <button class="btn btn-danger" disabled><i class="fa fa-remove"></i> Already Cancelled</button>
                @endif
                    <button class="btn btn-info" data-toggle="modal" data-target="#edit-order"><i class="fa fa-edit"></i> Edit Commission Recipient</button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="reverse-order-from-entity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">Reverse Shares Purchase from Entity</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to reverse this shares purchase from entity?</p>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>['reverse_shares_purchase_from_entity', $order->id], 'method'=>'POST']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Go Back</button>
                    {!! Form::button('<i class="fa fa-undo"></i> Reverse Shares Purchase', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cancel-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">Cancel Purchase Order</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to cancel this shares purchase order?</p>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>['cancel_shares_purchase_order', $order->id]]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Go Back</button>
                    {!! Form::button('<i class="fa fa-remove"></i> Cancel Order', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">Edit Purchase Order</h4>
                </div>
                <div class="modal-body" data-ng-controller="editShareCommissionCtrl"
                     ng-init="date='{!! $order->request_date !!}'; client_id = '{!! $order->buyer->client_id !!}';
                     commission_rate='{!! $order->commission_rate !!}'; previous_recipient='{!! $order->commission_recipient_id !!}'; commission_recipient='{!! $order->commission_recipient_id !!}';">
                    {!! Form::open(['route'=>['edit_shares_purchase_order', $order->id]]) !!}

                    <div class = "form-group">
                        {!! Form::label('commission_recepient', 'Commission paid to') !!}
                        {!! Form::select('commission_recepient', $recipients, $order->commission_recipient_id,  ['id'=>'commission_recepient','init-model'=>'commission_recepient','class'=>'form-control', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('commission_start_date', 'Commission Start Date') !!}
                        {!! Form::text('commission_start_date', $order->commission_start_date, ['class'=>'form-control', 'datepicker-popup init-model'=>'commission_start_date', 'is-open'=>'status.c', 'ng-focus'=>'status.c = true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_start_date') !!}
                    </div>

                    <div class="form-group alert alert-info">
                        {!! Form::label('commission_rate', 'Commission Rate') !!} :- <i class="fa fa-spinner fa-spin" ng-show="loadingRate === true"></i> {!! Form::label('commission_rate', '<% commission_rate_name %>') !!}
                        <br />
                    </div>

                    <div class = "form-group" ng-show="edit_rate">
                        <br />
                        {!! Form::label('commission_rate', 'Commission Rate') !!}
                        {!! Form::number('commission_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'data-ng-value' => 'commission_rate', 'required', 'ng-model' => 'commission_rate']) !!}
                        {!! Form::hidden('commission_rate_name', NULL, [ 'data-ng-value' => 'commission_rate_name', 'ng-model' => 'commission_rate_name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                    </div>

                    <div class="form-group">
                        <br />
                        {!! Form::checkbox('edit', true, true, ['ng-model'=>'edit_rate']) !!} {!! Form::label('edit', "Edit Commission Rate") !!}
                        <br />
                    </div>
                    <br>
                    <div class="form-group">
                        {!! Form::button('<i class="fa fa-edit"></i> Edit Order', array('class'=>'btn btn-success pull-right', 'type'=>'submit')) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop