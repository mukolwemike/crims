@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Send Shares Purchase Order Confirmation</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Buyer</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($buyer->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $buyer->number !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase_order->number, true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase_order->price) !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchase_order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Recipient</td>
                        <td>@if($purchase_order->recipient) {!! $purchase_order->recipient->name !!} @endif</td>
                    </tr>
                    <tr>
                        <td>Commission Rate</td>
                        <td>{!! $purchase_order->commission_rate !!}</td>
                    </tr>
                    <tr>
                        <td>Matched</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($purchase_order->matched) !!}</td>
                    </tr>
                </tbody>
            </table>

            <div class = "panel panel-success">
                <div class = "panel-heading">
                    <h4>Shares Business confirmation</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>No. of Shares</th>
                                <th>Price</th>
                                <th>Shares Purchase</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{!! $purchase_order->number !!}</td>
                                <td>{!! $purchase_order->price !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase_order->number * $purchase_order->price) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchase_order->request_date) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                    {!! Form::open(['url'=>'Dashboard'.$purchase_order->id]) !!}
                        <a href = "/dashboard/shares/purchases/confirmations/preview/{!! $purchase_order->id !!}"
                           class = "btn btn-primary"><i class = "fa fa-file-pdf-o"></i> Preview Purchase Order confirmation</a>
                        @if($is_poc_sent)
                            <a class="btn btn-danger" data-toggle="modal" data-target="#confirmPOCResend"><i class="fa fa-envelope"></i> Resend Confirmation</a>
                        @else
                            {!! Form::button('<i class="fa fa-envelope"></i> Send Confirmation', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                        @endif
                    {!! Form::close() !!}
                </div>

                <div class = "panel-heading">
                    <h4>Previous Shares Purchase Order Confirmations</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>No. of Shares</th>
                                <th>Purchase Price</th>
                                <th>Shares Purchase</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($previousConfirmations as $confirmation)
                                <tr>
                                    <td>{!! $confirmation->purchaseOrder->number !!}</td>
                                    <td>{!! $confirmation->purchaseOrder->price !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($confirmation->purchaseOrder->number * $confirmation->purchaseOrder->price) !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($confirmation->purchaseOrder->request_date) !!}</td>
                                    <td>
                                      <a ng-controller = "PopoverCtrl" uib-popover = "View PDF" popover-trigger = "mouseenter" href = "/dashboard/shares/purchases/confirmations/previous-preview/{!! $confirmation->id !!}"><i class = "fa fa-list-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
        <!-- Modal -->
        <div class="modal fade" id="confirmPOCResend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['url'=>'/dashboard/shares/purchases/confirmations/send/'.$purchase_order->id]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send Shares Purchase Order Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p>This shares purchase order confirmation has already been sent. Are you sure that you want to resend it?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        {!! Form::submit('Resend', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection