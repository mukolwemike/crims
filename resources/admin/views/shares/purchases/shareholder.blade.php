@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div ng-controller="ShareholdersSharesPurchaseGridController">
                <div class="btn-group">
                    <a type="button" href="/dashboard/shares/shareholders/show/{!! $holder->id !!}" class="btn btn-primary margin-bottom-20" target="_self"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#purchase-order">Make Purchase Order</button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#buy-entity-shares">Buy Shares from Entity</button>
                </div>
                <h4 class="col-md-12 bold">Shares Purchase Orders</h4>
                <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <th colspan="7"></th>
                        <th>
                            <select st-search="matched" class="form-control">
                                <option value="">Filter</option>
                                <option value="matched">Matched</option>
                                <option value="unmatched">Unmatched</option>
                            </select>
                        </th>
                        <th>
                            <select st-search="cancelled" class="form-control">
                                <option value="">All Orders</option>
                                <option value="cancelled">Cancelled</option>
                                <option value="active">Active</option>
                            </select>
                        </th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search buyer..." type="text"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Number</th>
                        <th>Bought</th>
                        <th>Price</th>
                        <th>Remaining</th>
                        <th>Request Date</th>
                        <th>GTF/C</th>
                        <th>Expiry Date</th>
                        <th>Commission Recipient</th>
                        <th>Rate</th>
                        <th>Matched</th>
                        <th>Cancelled</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed | filter:search">
                        <td><% row.number %></td>
                        <td><% row.bought %></td>
                        <td><% row.price %></td>
                        <td><% row.number - row.bought %></td>
                        <td><% row.request_date %></td>
                        <td><span to-html ="row.good_till_filled_cancelled | yesno"></span></td>
                        <td><% row.expiry_date %></td>
                        <td><% row.recipient %></td>
                        <td><% row.rate %></td>
                        <td><span to-html ="row.matched | booleanIcon"></span></td>
                        <td><span to-html ="row.cancelled | yesno"></span></td>
                        <td><a href="/dashboard/shares/purchases/show/<% row.id%>" target="_self"><i class="fa fa-list-alt"></i> </a></td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="12" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan = "6" class = "text-center">
                            Items per page <input type = "text" ng-model = "itemsByPage"/>
                        </td>
                        <td colspan = "5" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop


<!-- Make Shares Purchase Order Modal -->
<div class="modal fade" id="purchase-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['make_shares_purchase_order', $holder->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Shares Purchase Order <small>( Current funds : {!! \Cytonn\Presenters\AmountPresenter::currency($holder->sharePaymentsBalance()) !!})</small></h4>
            </div>
            <div class="modal-body" data-ng-controller="CreateSharePurchaseOrderCtrl" data-ng-init="client_id = '{!! $holder->client_id !!}'">

                <div class="form-group">
                    {!! Form::hidden('buyer_id', $holder->id) !!}
                    {!! Form::label('request_date', 'Date of Request') !!}

                    {!! Form::text('request_date', null, ['class'=>'form-control', 'init-model'=>'date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'request_date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number', 'Number of Shares ( Current - '.$current_shares.' shares )') !!}

                    {!! Form::number('number', null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'number') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'Price per Share') !!}

                    {!! Form::number('price', null, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'price') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('commission_recipient_id', 'Commission Recipient') !!}
                    {!! Form::select('commission_recipient_id', $commissionRecepient, null, ['class'=>'form-control', 'required', 'ng-model' => 'recipientId']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_recipient_id') !!}
                </div>

                <div class="form-group alert alert-info">
                    {!! Form::label('commission_rate', 'Commission Rate') !!} :- <i class="fa fa-spinner fa-spin" ng-show="loading === true"></i> {!! Form::label('commission_rate', '<% commission_rate_name %>') !!}
                    <br />

                </div>

                <div class = "form-group" ng-show="edit_rate">
                    <br />
                    {!! Form::label('commission_rate', 'Commission Rate') !!}
                    {!! Form::number('commission_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'data-ng-value' => 'commission_rate', 'required', 'ng-model' => 'commission_rate']) !!}
                    {!! Form::hidden('commission_rate_name', NULL, [ 'data-ng-value' => 'commission_rate_name', 'ng-model' => 'commission_rate_name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                </div>

                <div class="form-group">
                    <br />
                    {!! Form::checkbox('edit', true, true, ['ng-model'=>'edit_rate']) !!} {!! Form::label('edit', "Edit Commission Rate") !!}
                    <br />
                </div>

                <div class="form-group">
                    {!! Form::checkbox('good_till_filled_cancelled', true, true, ['ng-model'=>'good_till_filled_cancelled', 'ng-init'=>'good_till_filled_cancelled = true']) !!}

                    {!! Form::label('good_till_filled_cancelled', 'Good Till Filled / Cancelled') !!}<br/></br>
                </div>

                <div class="form-group" ng-show="!good_till_filled_cancelled">
                    {!! Form::label('expiry_date', 'Good Till Expiry. Expiry Date') !!}

                    {!! Form::text('expiry_date', null, ['class'=>'form-control', 'init-model'=>'expiry_date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt_.open', 'ng-focus'=>'dt_.open = !dt_.open']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'expiry_date') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Buy Shares', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<!-- Buy Entity Shares Modal -->
<div class="modal fade" id="buy-entity-shares" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['buy_entity_shares', $holder->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Buy Shares From Entity <small>( Current funds : {!! \Cytonn\Presenters\AmountPresenter::currency($holder->sharePaymentsBalance()) !!})</small></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden('buyer_id', $holder->id) !!}
                    {!! Form::label('request_date', 'Date of Request') !!}

                    {!! Form::text('request_date', null, ['class'=>'form-control', 'init-model'=>'date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'request_date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number', 'Number of Shares ( Current - '.$current_shares.' shares )') !!}

                    {!! Form::number('number', null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'number') !!}
                </div>

                <div class="form-group">
                    {!! Form::button('Set Price Exemption', ['class'=>'btn btn-default', 'ng-click'=>'show_exemption = !show_exemption']) !!} <br/>
                </div>

                <div class="clearfix"></div>
                <br/>

                <div class="form-group" ng-if="show_exemption">
                    {!! Form::label('exemption', 'Share Purchase Price') !!}
                    {!! Form::number('exemption', null, ['class'=>'form-control', 'step'=>'0.01', 'ng-required'=>'true']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Buy Shares', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>