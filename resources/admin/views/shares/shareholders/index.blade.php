@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="ShareholdersGridController">
            <a href="/dashboard/shares/statements" class="btn btn-success margin-bottom-20">Statements</a>
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="2"></th>
                        <th>
                            <select st-search="entity_id" class="form-control">
                                <option value="">Select Entity</option>
                                @foreach($shareentities as $entity)
                                    <option value="{!! $entity->id !!}">{!! $entity->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search shareholder name..." type="text"/></th>
                        <th colspan="3"><input st-search="shareholder_number" class="form-control" placeholder="Search shareholder number..." type="text"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th st-sort="client_code">Number</th>
                        <th>Shareholder's Name</th>
                        <th>Entity</th>
                        <th>Phone</th>
                        <th>E-Mail Address</th>
                        <th>No. of Shares</th>
                        <th>Value of Shares</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.number %></td>
                        <td><% row.fullName %></td>
                        <td><% row.entity %></td>
                        <td><% row.phone %></td>
                        <td><% row.email %></td>
                        <td><% row.number_of_shares %></td>
                        <td><% row.value_of_shares %></td>
                        <td>
                            <a class="pull-right" href="/dashboard/shares/shareholders/show/<% row.id %>"><i class="fa fa-user"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop