@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div class="detail-group">
                <div class="btn-group">
                    <a type="button" href="/dashboard/shares/payments/shareholders/{!! $share_holder->id !!}"
                       class="btn btn-info">Share Payments</a>
                    <a type="button" href="/dashboard/shares/purchases/shareholders/{!! $share_holder->id !!}"
                       class="btn btn-warning">Share Purchases</a>
                    <a type="button" href="/dashboard/shares/sales/shareholders/{!! $share_holder->id !!}"
                       class="btn btn-success">Share Sales</a>
                    <a type="button" href="/dashboard/shares/redeem/shareholders/{!! $share_holder->id !!}"
                       class="btn btn-primary">Redeem Shares</a>
                    <a type="button" href="#" data-toggle="modal" data-target="#deleteShareHolder"
                       data-backdrop="static" data-keyboard="false" class="btn btn-danger">Delete ShareHolder</a>
                </div>
            </div>
            <div class="detail-group">
                <h3>Shareholder Details</h3>
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($share_holder->client_id) !!}</td>
                        <td>
                            <button type="button" class="btn btn-success pull-right" data-toggle="modal"
                                    data-target="#statement">Statements
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Added on</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($share_holder->created_at) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td colspan="2">{!! $share_holder->number !!}</td>
                    </tr>
                    <tr>
                        <td>E-Mail Address</td>
                        <td colspan="2">{!! $share_holder->client->contact->email !!}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>{!! $share_holder->client->contact->phone !!}</td>
                        <td><a class="btn btn-success pull-right"
                               href="/dashboard/clients/details/{!! $share_holder->client_id !!}">View Client
                                Details</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>


            <div class="detail-group">
                <h3>Statement</h3>

                <div>
                    {!! Form::open(['route'=>['view_shares_statement', $share_holder->id]]) !!}

                    <div class="col-md-6">
                        <input type="text" name="date" placeholder="Select date to preview statement"
                               class="form-control" ng-model="date" uib-datepicker-popup="yyyy-MM-dd"
                               is-open="status.isopen" ng-focus="status.isopen = true">
                    </div>


                    <div class="col-md-6">
                        {!! Form::submit('View statement', ['class'=>'btn btn-success inline']) !!}
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="detail-group">
                <h3>Share Holdings</h3>

                <div ng-controller="ShareholdingsController" ng-init="share_holder_id = {!! $share_holder->id !!}">
                    <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Number</th>
                            <th>Share Price</th>
                            <th>Purchase Price</th>
                            <th>Date</th>
                            <th>Entity</th>
                            <th>Narrative</th>
                            <th>Stamp Duty</th>
                            <th colspan="3">Acknowledgement</th>
                        </tr>
                        </thead>
                        <tbody ng-show="!isLoading">
                        <tr ng-repeat="row in displayed">
                            <td><% row.number %></td>
                            <td><% row.share_price | currency:"" %></td>
                            <td><% row.share_purchase | currency:"" %></td>
                            <td><% row.date %></td>
                            <td><% row.entity %></td>
                            <td><% row.narrative %></td>
                            <td>
                                <span ng-show="row.customDuty > 0"><% row.customDuty | currency:"" %></span>
                                <el-button type="danger" size="mini" ng-show="row.customDuty == 0" href="#" ng-controller="PopoverCtrl" uib-popover="Pay Custom Duty" popover-trigger="mouseenter" data-toggle="modal" data-target="#pay-custom-duty-<% row.id %>"><i class="fa fa-arrow-left"></i> PAY</el-button>

                                <div class="modal fade" id="pay-custom-duty-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['pay_custom_duty', $share_holder->id]]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="myModalLabel">Pay Stamp Duty</h4>
                                            </div>
                                            <div class="modal-body row">
                                                <div class="form-group">
                                                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                                                        {!! Form::text('share_holding_id', null, ['ng-model'=>'row.id', 'style'=>'display:none;']) !!}
                                                        {!! Form::label('date', 'Choose date to pay stamp duty') !!}
                                                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::submit('Pay', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td colspan="3">
                                <a class="btn btn-primary" type="info" href="/dashboard/shares/purchase/<%row.id%>/acknowledgement/preview" ng-show="row.customDuty > 0"><i class="fa fa-eye"></i></a>
                                <a class="btn btn-success" ng-if="!row.sent" type="success" href="/dashboard/shares/purchase/<%row.id%>/acknowledgement/send" ng-show="row.customDuty > 0"><i class="fa fa-mail-forward"></i> send</a>
                                <a class="btn btn-danger" ng-if="row.sent" type="success" href="/dashboard/shares/purchase/<%row.id%>/acknowledgement/send" ng-show="row.customDuty > 0"><i class="fa fa-refresh"></i> resend</a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="100%">Loading shareholder's share holdings...</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3" class="text-center">
                                Items per page <input type="text" ng-model="itemsByPage"/>
                            </td>
                            <td colspan="4" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage"
                                     st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                    <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                        <tbody>
                        <tr style="background: yellow; color: red;">
                            <th>Purchased Shares : {!! $purchased_shares !!}</th>
                            <th>Sold Shares : {!! $sold_shares !!}</th>
                            <th>Current Shares : {!! $current_shares !!}</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!-- Statement Modal -->
        <div class="modal fade" id="statement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Statements</h4>
                    </div>
                    <div class="modal-body">
                        @if(count($open_campaigns_arr) > 0)
                            {!! Form::open(['route'=>'add_shareholder_to_shares_statement_campaign']) !!}
                            <div class="form-group">
                                {!! Form::label('campaign', 'Select campaign') !!}

                                {!! Form::select('campaign', $open_campaigns_arr, null, ['class'=>'form-control']) !!}
                            </div>

                            {!! Form::hidden('share_holder_id', $share_holder->id) !!}

                            {!! Form::submit('Add to campaign', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}
                        @else
                            <div class="well well-lg">
                                <p>No open campaigns</p>
                            </div>
                        @endif

                        <div ng-controller="CollapseCtrl" class="padding-top-50">
                            <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">More
                                options
                            </button>

                            <div uib-collapse="isCollapsed" class="padding-top-50">
                                {!! Form::open(['route'=>['mail_shares_statement', $share_holder->id]]) !!}
                                <input type="text" name="date" class="form-control" ng-model="date"
                                       uib-datepicker-popup="yyyy-MM-dd" is-open="status.isopen"
                                       ng-focus="status.isopen = true">
                                {!! Form::submit('Send Statement', ['class'=>'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        {{--<div class="modal fade" id="cutomDuty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
            {{--<div class="modal-dialog" role="document">--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                            {{--<span aria-hidden="true">&times;</span></button>--}}
                        {{--<h4 class="modal-title" id="myModalLabel">Pay Custom Duty</h4>--}}
                    {{--</div>--}}

                    {{--<div class="modal-body">--}}
                        {{--<div class="">--}}
                            {{--{!! Form::open(['route'=>'pay_share_purchase_custom_duty']) !!}--}}

                            {{--<div class="form-group">--}}
                                {{--<p>Are you sure you want to pay the custom duty?</p>--}}
                            {{--</div>--}}

                            {{--{!! Form::submit('Yes', ['class'=>'btn btn-success']) !!}--}}
                            {{--{!! Form::close() !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>


    <!-- Delete ShareHolder Modal -->
    <div class="modal fade" id="deleteShareHolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                {!! Form::open(['url'=>'dashboard/shares/shareholders/remove/'.$share_holder->id, 'method'=>'POST']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Remove ShareHolder</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Give a Reason <span class="label label-info">required</span></label>
                        {!! Form::textarea('reason', null, [ 'class'=>'form-control', 'rows'=>3 , 'required' => 'true']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    {!! Form::submit('Remove', ['class'=>'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection