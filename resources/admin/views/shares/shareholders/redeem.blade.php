@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard" ng-controller="ShareholdersRedemptionGridController" ng-init="holderId='{!! $share_holder->id !!}'; date='{!! $date !!}'">
            <div class="detail-group">
                <div class="btn-group">
                    <a type="button" href="/dashboard/shares/shareholders/show/{!! $share_holder->id !!}" class="btn btn-primary margin-bottom-20" target="_self"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
                </div>
                <h3>Shareholder Details</h3>
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($share_holder->client_id) !!}</td>
                        <td><button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#statement">Statements</button></td>
                    </tr>
                    <tr>
                        <td>Added on</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($share_holder->created_at) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td colspan="2">{!! $share_holder->number !!}</td>
                    </tr>
                    <tr>
                        <td>E-Mail Address</td>
                        <td colspan="2">{!! $share_holder->client->contact->email !!}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>{!! $share_holder->client->contact->phone !!}</td>
                        <td><a class="btn btn-success pull-right" href="/dashboard/clients/details/{!! $share_holder->client_id !!}">View Client Details</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div>
                <h3>Share Redemption</h3>
                <div>
                    {!! Form::open(['route'=>['save_redeem_shares', $share_holder->id]]) !!}
                    <div class="col-md-12">
                        {!! Form::label('date', 'Redemption Date') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::text('date', $date, ['id'=>'date','class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}
                        {!! Form::hidden('share_holder_id', $share_holder->id) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('Redeem membership fee? ') !!}
                        {!! Form::checkbox('membership_fee', true, ['id'=>'membership_fee','class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-3">
                        <a href="#redeem-shares-modal" class="btn btn-success" data-toggle="modal" role="button">Redeem Shares</a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal fade" id="redeem-shares-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                    <h4 class="modal-title" id="myModalLabel">Redeem Shares</h4>
                                </div>
                                <div class="modal-body row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p>Are you sure you would like to redeem the shares for the client?</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::submit('Yes', ['class'=>'btn btn-success']) !!}
                                    <a data-dismiss="modal" class="btn btn-default">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <br>
                <table st-table="displayed" st-safe-src="rowCollection"
                       class="table table-hover table-responsive table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Rate</th>
                        <th>Gross</th>
                        <th>Withholding Tax</th>
                        <th>Net</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading && displayed.length > 0">
                    <tr ng-repeat="row in displayed">
                        <td><% row.date %></td>
                        <td><% row.number %></td>
                        <td><% row.price %></td>
                        <td><span ng-if="row.amount > 0"><% row.amount | currency:"" %></span></td>
                        <td><% row.rate %></td>
                        <td><span ng-if="row.gross_interest > 0"><% row.gross_interest | currency:"" %></span></td>
                        <td><span ng-if="row.withholding > 0"><% row.withholding | currency:"" %></span></td>
                        <td><span ng-if="row.net_interest > 0"><% row.net_interest | currency:"" %></span></td>
                        <td><span ng-if="row.total > 0"><% row.total | currency:"" %></span></td>
                    </tr>
                    <tr style="color:red; background:yellow;">
                        <th colspan="7" class="text-center">TOTAL SHARES</th>
                        <th colspan="2" class="text-center"><span ng-if="total_shares > 0"><% total_shares %></span></th>
                    </tr>
                    <tr style="color:red; background:yellow;">
                        <th colspan="7" class="text-center">TOTAL VALUE</th>
                        <th colspan="2" class="text-center"><span ng-if="total > 0"><% total | currency:"" %></span></th>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="9" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                </table>
                <p ng-if="displayed.length == 0 && ! isLoading">No shares available for redemption</p>
            </div>
        </div>
    </div>
@stop
