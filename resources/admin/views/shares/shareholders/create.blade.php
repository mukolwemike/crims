@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <client-application :edit="{{ json_encode($edit) }}" :type="{{ json_encode($type) }}" :application="{{ json_encode($applicationId) }}"></client-application>
    </div>
@stop