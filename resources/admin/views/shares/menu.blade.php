@extends('layouts.default')

@section('content')

    <bread-crumb title="">
        <a class="current" slot="sub_1" href="#">Shares</a>
    </bread-crumb>

    <div class="admin_menus dashboard__account_summary">
        <div class="summaries">
            <menu-card-details title="Shares" title_time="Today" description="Shares Entities" description_sub="Manage share entities" footer="All Entities" footer_sub="Manage all entities">

                <a slot="description" href="/dashboard/shares/entities">
                    <span>View All</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>

                <a  slot="footer" href="/dashboard/shares/entities">
                    <span>View All</span>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>

            </menu-card-details>

            <menu-card
                    title="Sale Orders"
                    statistics="{!! $gen->salesOrdersCount($manager) !!}"
                    :fetch="false"
                    statistics_title="Pending"
                    link="/dashboard/shares/sales"
                    :first="true"
                    description="Manage Share Sales Orders">
                <i class = "fa fa-money"></i>
            </menu-card>

            <menu-card
                    title="Register Shareholder"
                    link="/dashboard/shares/shareholders/register"
                    :first="false"
                    description="Onboarding of share holders">
                <i class = "fa fa-user-plus"></i>
            </menu-card>

            <menu-card
                    title="Shareholders"
                    statistics="{!! $gen->shareHoldersCount($manager) !!}"
                    :fetch="false"
                    statistics_title="ACTIVE"
                    link="/dashboard/shares/shareholders"
                    :first="false"
                    description="Manage Share Holders">
                <i class = "fa fa-users "></i>
            </menu-card>

            <menu-card
                    title="Payments"
                    link="/dashboard/shares/payments"
                    :first="false"
                    description="Manage Share Payments">
                <i class = "fa fa-dollar"></i>
            </menu-card>


            <menu-card
                    title="Commission"
                    link="/dashboard/shares/commissions"
                    :first="false"
                    description="Manage FA Commissions">
                <i class = "fa fa-line-chart"></i>
            </menu-card>


            <menu-card
                    title="Purchase Orders"
                    statistics="{!! $gen->purchaseOrdersCount($manager) !!}"
                    :fetch="false"
                    statistics_title="Pending"
                    link="/dashboard/shares/purchases"
                    :first="false"
                    description="Manage Share Purchase Orders">
                <i class = "fa fa-cart-plus"></i>
            </menu-card>


            <menu-card
                    title="Business Confirmations"
                    link="/dashboard/shares/confirmations"
                    :first="false"
                    description="Manage Shares Business Confirmations">
                <i class = "fa fa-file-pdf-o"></i>
            </menu-card>


            <menu-card
                    title="Shares Reports"
                    link="/dashboard/shares/reports"
                    :first="false"
                    description="Export Shares Reports">
                <i class = "fa fa-files-o"></i>
            </menu-card>

        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/entities"><i class="fa fa-building-o fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Share Entities</div>--}}
            {{--</div>--}}


            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/shareholders/register"><i class="fa fa-user-plus fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Register Shareholder</div>--}}
            {{--</div>--}}


            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/shareholders"><i class="fa fa-users fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Shareholders</div>--}}
            {{--</div>--}}



            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/payments"><i class="fa fa-dollar fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Payments</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/commissions"><i class="fa fa-line-chart fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Commission</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/sales"><i class="fa fa-money fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Sale Orders</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/purchases"><i class="fa fa-cart-plus fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Purchase Orders</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-2 panel grid-menu">--}}
                {{--<a href="/dashboard/shares/confirmations"><i class="fa fa-file-pdf-o fa-5x"></i> </a>--}}
                {{--<div class="panel-footer">Business Confirmations</div>--}}
            {{--</div>--}}

            {{--<menu-tile icon="fa-files-o" link="/dashboard/shares/reports" text="Shares Reports">--}}
                {{--Reports--}}
            {{--</menu-tile>--}}

        </div>
    </div>
@endsection