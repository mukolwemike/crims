@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div ng-controller="ShareCommissionGridController">
                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th colspan="3">
                            <a class="margin-bottom-20 btn btn-success"
                               href="/dashboard/investment/commission_recipients/list"> <i class="fa fa-plus"></i> View
                                FAs</a>
                            <a href="/dashboard/investments/commission/payment-dates/list"
                               class="btn btn-success margin-bottom-20"><i class="fa fa-calendar-check-o"></i> Payment
                                Dates</a>
                        </th>
                        <th colspan="2">
                            <input st-search="month" placeholder="date" type="date" class="form-control"
                                   ng-model="date">
                        </th>
                        <th colspan="2">
                            <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                        </th>
                    </tr>
                    <tr>
                        <td class="alert alert-info" colspan="100%"> Commission from <% meta.dates.start | date %> to <% meta.dates.end | date %></td>
                    </tr>
                    <tr>
                        <th st-sort="id">FA ID</th>
                        <th>Name</th>
                        <th st-sort="email">Email</th>
                        <th>Earned</th>
                        <th>Overrides</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.name %></td>
                        <td><% row.email %></td>
                        <td><% row.earned | currency:"" %></td>
                        <td><% row.override | currency:"" %></td>
                        <td><% row.total | currency:"" %></td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="Payment Details" popover-trigger="mouseenter"
                               href="/dashboard/shares/commission/payment/<%row.id%>"><i
                                        class="fa fa-money"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ...</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2" class="text-center">
                            Items per page
                        </td>
                        <td colspan="2" class="text-center">
                            <input type="text" ng-model="itemsByPage"/>
                        </td>
                        <td colspan="3" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage"
                                 st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@stop