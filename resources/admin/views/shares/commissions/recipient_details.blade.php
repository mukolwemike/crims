@extends('layouts.default')

@section('content')

    <div class="panel-dashboard">

        <div class="row" data-ng-controller="ShareCommissionRecipientController"
             data-ng-init="start = '{!! $start->toDateString() !!}'; end = '{!! $end->toDateString() !!}';
                recipientId = '{!! $recipient->id !!}'; ">
            <div class="col-md-6">
                <div class="detail-group">
                    <h4>FA Details</h4>

                    <table class="table table-hover table-responsive">
                        <thead>
                        </thead>
                        <tbody>
                        <tr><td>Name</td><td>{!! $recipient->name !!}</td></tr>
                        <tr><td>Type</td><td>{!! $recipient->type->name !!}</td></tr>
                        <tr><td>Compliance Required</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo((new \Cytonn\Investment\Commission\Base())->shouldCheckCompliance($recipient)) !!}</td></tr>
                        </tbody>
                    </table>
                </div>

                <div class="detail-group">
                    <h4>Commission Calculation ({!! $start->toFormattedDateString() !!} to {!! $end->toFormattedDateString() !!})</h4>

                    <table class="table table-hover table-responsive">
                        <thead>
                        <tr><th>Entry</th><th>Amount</th></tr>
                        </thead>
                        <tbody>
                        <tr><td>Commission earned</td><td><% totals.earned | number:2 %></td></tr>
                        <tr><td>Commission Override</td><td><% totals.override | number:2 %></td></tr>
                        </tbody>
                        <tfoot>
                        <tr><th>Total</th><th><% totals.total | number:2 %></th></tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="col-md-6">
                <div class="detail-group">
                    <h4>Payment Date</h4>

                    {!! Form::open(['method'=>'GET']) !!}

                    <div class="form-group" ng-controller = "DatepickerCtrl">
                        {!! Form::label('Select Payment Date') !!}
                        {!! Form::text('date', $date->toDateString(), ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Change Date', ['class'=>'btn btn-primary']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>

                <div class="detail-group">
                    <div class="btn-group">
                        <button class="btn btn-info" ng-click="toggleAddSchedule = !toggleAddSchedule"><i class="fa fa-calendar-plus-o"></i> Add Schedule</button>
                    </div>
                    <br/>

                    <!-- Schedule form -->
                    <div class="form" ng-if="toggleAddSchedule">
                        <h3>Add Commission Payment Schedule</h3>
                        {!! Form::open(['route'=>['add_shares_commission_payment_schedule']]) !!}
                        <div class="form-group">
                            {!! Form::label('share_purchase_id', 'Share Purchase') !!}
                            <select name class="form-control"
                                    ng-model="selectedSchedule"
                                    ng-options="schedule.combined_description for schedule in schedules"
                            >
                            </select>
                            {!! Form::hidden('schedule_id', null, ['data-ng-value' => 'selectedSchedule.id']) !!}
                            {!! Form::hidden('commission_recipient_id', $recipient->id) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('date', 'Scheduled Date') !!}
                            {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"schedule_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'schedule_date']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('amount', 'Amount') !!}
                            {!! Form::number('amount', null, ['class'=>'form-control', 'init-model'=>'schedule_amount', 'ng-required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description') !!}
                            {!! Form::text('description', null, ['class'=>'form-control', 'init-model'=>'schedule_description']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save Schedule', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <uib-accordion>
                    <uib-accordion-group>
                        <uib-accordion-heading>
                            Commission Earned <i class="pull-right glyphicon"
                                                 ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                        </uib-accordion-heading>

                        <table class="table table-bordered table-hover table-striped table-responsive">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <th>Client Name</th>
                                <th>Shares Num</th>
                                <th>Price</th>
                                <th>Amount</th>
                                <th>Value Date</th>
                                <th>Rate</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody ng-show="!schedulesLoading">
                            <tr ng-repeat="row in schedules">
                                <td><% row.id %></td>
                                <td><% row.client_name %></td>
                                <td><% row.shares_num | number %></td>
                                <td><% row.purchase_price %></td>
                                <td><% row.total | number:2 %></td>
                                <td><% row.value_date %></td>
                                <td><% row.rate %></td>
                                <td><% row.date %></td>
                                <td><% row.description %></td>
                                <td><% row.amount | number:2 %></td>
                                <td>
                                    <a href="#" data-toggle="modal"
                                       data-target="#edit-schedule-<% row.id %>"><i
                                                class="fa fa-pencil-square-o"></i> </a>
                                    <div class="modal fade" id="edit-schedule-<% row.id %>"
                                         tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {!! Form::open(['route'=>['edit_shares_commission_payment_schedule', ]]) !!}
                                                <div class="modal-header">
                                                    <a href="#" type="button" class="close"
                                                       data-dismiss="modal" aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></a>
                                                    <h4 class="modal-title" id="myModalLabel">Edit
                                                        Commission Payment Schedule</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        {!! Form::label('date', 'Scheduled Date') !!}
                                                        <div ng-controller="DatepickerCtrl">{!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"row.date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::hidden('id', null, ['data-ng-value' => 'row.id']) !!}
                                                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tbody ng-show="schedulesLoading">
                            <tr><td colspan="100%">Loading...</td></tr>
                            </tbody>
                            <tfoot ng-show="!schedulesLoading">
                            <tr>
                                <th colspan="9">Total</th>
                                <th><% totals.earned | number:2 %></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </uib-accordion-group>
                    <uib-accordion-group>
                        <uib-accordion-heading>
                            Commission Override <i class="pull-right glyphicon"
                                                   ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                        </uib-accordion-heading>

                        <div>
                            <table class="table table-bordered table-hover table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Rank</th>
                                    <th>Commission</th>
                                    <th>Rate</th>
                                    <th>Override</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody ng-show="!overridesLoading">
                                <tr ng-repeat="row in overrides">
                                    <td><% row.name %></td>
                                    <td><% row.rank %></td>
                                    <td><% row.commission %></td>
                                    <td><% row.rate %></td>
                                    <td><% row.override | number:2 %></td>
                                    <td></td>
                                </tr>
                                </tbody>
                                <tbody ng-show="overridesLoading">
                                <tr><td colspan="100%">Loading...</td></tr>
                                </tbody>
                                <tfoot ng-show="!overridesLoading">
                                <tr>
                                    <th colspan="4">Total</th>
                                    <th><% totals.override | number:2 %></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </uib-accordion-group>
                </uib-accordion>
            </div>
        </div>
    </div>
@endsection