@extends('reports.letterhead')

@section('content')
    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Partner Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">
        ACKNOWLEDGEMENT OF FULL PAYMENT FOR OVER THE COUNTER SHARES OF CYTONN INVESTMENT MANAGEMENT PLC ‘CYTONN INVESTMENTS’
    </p>

    <p>
        On behalf of Cytonn Investments, I take this opportunity to thank you for the full payment for your Over-The-Counter Shares in {{ $fundManager->fullname }}.
    </p>

    <p>As per your application, below are details of your transactions;</p>


    <p>
        We hereby acknowledge receipt of <b>Kshs {{ \Cytonn\Presenters\AmountPresenter::currency($amount) }}</b>
        for the <b>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number, true, 0) }}</b> shares purchased for in Cytonn Investments at
        <b>Kshs {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->purchase_price) }}</b> per share.
    </p>

    <p>You will be issued with a Share Certificate for the shares applied for and fully paid by end of October 2018.</p>

    <p>
        If you have any question, comment or need any assistance, we are at your service. Our OTC Team will be at hand to assist you with all your queries needs. Please do not hesitate to contact us on +254 719 101 100 or email us at <a href="mailto:otc@cytonn.com">otc@cytonn.com</a>
    </p>

    <p>Once again, we thank you for choosing to invest in Cytonn Investments, and we look forward to being your Investment partner of choice.</p>

    <p>Yours sincerely,</p>
    <p><strong>For: {!! $entity->fundManager->fullname !!}</strong></p>

    <p><strong>{!! $emailSender !!}</strong></p>
@stop