@extends('reports.letterhead')

@section('content')
    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Partner Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">
        CONFIRMATION OF OVER THE COUNTER SHARE PURCHASE IN CYTONN INVESTMENT MANAGEMENT PLC
    </p>

    <p>
        On behalf of Cytonn, we take this opportunity to thank you for participating in our over the counter shares for {{ $fundManager->fullname }} and choosing as to deliver for your investment promise.
    </p>

    <p>As per your application, below are details of your transactions;</p>

    <table class="table table-responsive">
        <tbody>
            <tr>
                <th>Transaction type</th>
                <th>No of Shares</th>
                <th>Share Price (Ksh</th>
                <th>Settlement Amount</th>
                <th>Stamp Duty</th>
                <th>Total</th>
                <th>Month of Purchase</th>
                <th>Total Order Purchased</th>
                <th>Balance of Order</th>
            </tr>
            <tr>
                <td>Buy</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($holding->number, true, 0) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($holding->purchase_price) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($holding->payment->amount)) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($stampDuty)) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($holding->payment->amount)) }}</td>
                <td>{{ \Carbon\Carbon::parse($holding->date)->toFormattedDateString() }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($holding->purchaseOrder->number, true, 0) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($holding->number - $holding->purchaseOrder->number, true, 0) }}</td>
            </tr>
        </tbody>
    </table>

    <p>
        We acknowledge receipt of KES. {{ \Cytonn\Presenters\AmountPresenter::currency(abs($payment)) }}
    </p>

    @if(!$stampDuty)
    <p>
        You can now continue with payment of the stamp duty, which is 1% of the values of the shares, to the account below by {{ \Carbon\Carbon::parse($holding->date)->endOfMonth()->toFormattedDateString()}}.
        <br>
        <br>
        <b>Account Name: </b> Cytonn Investment Management Limited-OTC.
        <br>
        <b>Account Number: </b> 0105040476704
        <br>
        <b>Bank Name: </b> Standard Chartered Bank of Kenya
        <br>
        <b>Bank Branch: </b> Chiromo
    </p>

    <p>
        We shall be providing you with a Share Certificate for your shareholdings in Cytonn Investments Management PLC once the shares are fully paid.

    </p>
    @else
        <p>
            We shall be providing you with a Share Certificate for your shareholdings in Cytonn Investments Management PLC by {{ \Carbon\Carbon::parse($holding->date)->addMonth(1)->endOfMonth()->toFormattedDateString()}}.
        </p>
    @endif


    <p>
        Please do not hesitate to contact us on +254709 101 000 or email us on <a href="mailto:otc@cytonn.com">otc@cytonn.com</a> should you need any assistance.
    </p>

    <p>
        Once again, we thank you for choosing us to serve your investment needs.
    </p>


    <p>Yours sincerely,</p>
    <p><strong>For: {!! $entity->fundManager->fullname !!}</strong></p>

    <p><strong>{!! $email_sender !!}</strong></p>
@stop