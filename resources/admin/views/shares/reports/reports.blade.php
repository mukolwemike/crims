@extends('layouts.default')

@section('content')
    {!! HTML::style('/css/select2.new.css') !!}
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="panel">
                <div class="panel-heading">
                    <h3>Shares Reports</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Shareholdings Report</td>
                                <td>
                                    <a href="#shareholdings-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{--start shareholdings report modals--}}
        <div class="modal fade" id="shareholdings-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route'=>['export_shareholdings_report_path']]) !!}
                    <div class="modal-header">
                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">Shareholdings Report</h4>
                    </div>
                    <div class="modal-body row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <p>Select the shares entity that you wish to generate a holdings report for.</p>
                            </div>

                            <div class="col-md-12">
                                {!! Form::label('share_entity_id', 'Share Entity') !!}
                                {!! Form::select('share_entity_id', \App\Cytonn\Models\SharesEntity::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                        <a data-dismiss="modal" class="btn btn-default">Close</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    {{--end of ishareholdings report modal--}}
@stop