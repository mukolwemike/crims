@extends('reports.letterhead')

@section('content')
    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Shareholder's Code – {!! $buyer->number !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <!-- RE -->
    <p class="bold-underline">CONFIRMATION OF OVER THE COUNTER SHARE PURCHASE IN {!! strtoupper($entity->name) !!}</p>

    <!-- Body -->
    <p>
        On behalf of Cytonn, we take this opportunity to thank you for participating in our over the counter shares for {!! $entity->name !!} and choosing us to deliver to your investment promise.
    </p>

    <p>
        As per your application, below are details of your transaction;
    </p>

    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Transaction Type</th>
                <th>No. of Shares</th>
                <th>Share Price (Kshs)</th>
                <th>Settlement Amount</th>
                <th>Month of Purchase</th>
                <th>Total Order Placed</th>
                <th>Balance of Order</th>
            </tr>
        </thead>
        <tbody>
            <?php $number = 0; ?>
            @foreach($purchase_order->shareSales as $sale)
                <tr>
                    <td>Buy</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale->number, true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale->sale_price) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale->sale_price * $sale->number) !!}</td>
                    <td>{!! \Carbon\Carbon::parse($sale->date)->format('M Y') !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase_order->number, true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase_order->number - ($number += $sale->number), true, 0) !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>
        Kindly arrange to transfer your funds to the account below by {!! \Carbon\Carbon::parse($purchase_order->shareSales()->latest()->first()->date)->addWeek()->toFormattedDateString() !!}.
    </p>
    <p>
        <strong>Account Name:</strong> {!! $entity->custodialAccount->account_name !!}<br>
        <strong>Account Number:</strong> {!! $entity->custodialAccount->account_no !!} <br>
        <strong>Bank Name:</strong> {!! $entity->custodialAccount->bank_name !!}<br>
        {{--<strong>Branch Name:</strong> {!! $entity->custodialAccount->branch_name !!}<br>--}}
    </p>
    <p>
        We shall be providing you with a Share Certificate for your shareholdings in {!! $entity->name !!}.
    </p>
    <p>
        If you have any question, comment or need any assistance, we are at your service. Our OTC Team will
        be at hand to assist you with all your queries needs. Please do not hesitate to contact us on +254 719 101 100 or
        email us at <a href="mailto:otc@cytonn.com">otc@cytonn.com</a>
        Once again, we thank for choosing us to serve your investment needs.
    </p>


    <p>Yours sincerely,</p>
    <p><strong>For: {!! $client->fundManager->fullname !!}</strong></p>

    <p><strong>{!! $email_sender !!}</strong></p>
@stop