@extends('reports.letterhead')

@section('content')
    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Shareholder Number – {!! $share_holder->number !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <!-- RE -->
    <p><strong style="border-bottom: 2px solid #006B5A;">CONSOLIDATED SHARE PURCHASE STATEMENT AS AT {!! strtoupper(\Cytonn\Presenters\DatePresenter::formatDate($date)) !!}</strong></p>

    <!-- Body -->
    <p>{!! $entity->name !!} takes this opportunity to thank you for investing with us.</p>
    <p>Below is a summary of your investments with the {!! $entity->name !!} to date.</p>

    <table class="table table-responsive">
        <thead>
            <tr style="color: #FFF; background-color: #006B5A">
                <th>Action</th>
                <th>Date</th>
                <th>No. of Shares</th>
                <th>Price per Share</th>
                <th>Cost Value</th>
                <th>Market Price / Share</th>
                <th>Market Value</th>
            </tr>
        </thead>
        <tbody>
            @foreach($processed->actions as $action)
                <tr>
                    <td>{{ $action->action }}</td>
                    <td>{!! $action->date !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::accounting($action->number) !!}</td>
                    <td>{!! $action->cost_price !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::accounting($action->cost_value) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($action->market_price) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::accounting($action->market_value - $action->redeem_interest) !!}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td class="bold">Totals</td>
                <td></td>
                <td class="bold">{!! \Cytonn\Presenters\AmountPresenter::currency($processed->total->number) !!}</td>
                <td></td>
                <td class="bold"></td>
                <td class="bold"></td>
                <td class="bold">{!! \Cytonn\Presenters\AmountPresenter::currency($processed->total->market_value) !!}</td>
            </tr>
        </tfoot>
    </table>

    <br/>
    @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif

    <p>Once again, we thank you for choosing us to serve your investment needs and look forward to delivering to our investment promise.</p>

    <p>Yours sincerely,</p>
    <?php $fm = $entity->fundManager; ?>

    <p class="bold"> For {!! $entity->name !!}<br/>
        <img src="{{ storage_path('resources/signatures/' . $fm->signature) }}" height="60px"/>
    </p>
    <p><strong style="text-decoration: underline;">{!! $fm->fullname !!}</strong></p>
@stop