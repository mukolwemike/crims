@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="SharesPaymentsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="4"></th>
                        <th><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th st-sort="id">Payment ID</th>
                        {{--<th st-sort="number">Number</th>--}}
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Narrative</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed | filter:search">
                        <td><% row.id %></td>
                        {{--<td><% row.number %></td>--}}
                        <td><% row.full_name %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><% row.date %></td>
                        <td><% row.description %></td>
                        <td><a href="/dashboard/shares/shareholders/show/<% row.share_holder_id %>" target="_self"><i class="fa fa-user"></i> </a> </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="6" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3" class="text-center">
                        Items per page <input type="text" ng-model="itemsByPage"/>
                    </td>
                    <td colspan="3" class="text-center">
                        <div st-pagination="" st-items-by-page="itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop