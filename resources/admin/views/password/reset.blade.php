@extends('layouts.default')

@section('content')
    <div class="col-md-5 col-md-offset-3">
        <div class="panel-dashboard">
            {!! Form::open() !!}
            <div class="form-group">
                <div class="col-md-4">
                    {!! Form::label('password', 'Create a new password') !!}
                </div>
                <div class="col-md-8">
                    {!! Form::password('password', ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'password') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    {!! Form::label('password_confirmation', 'Confirm your password') !!}
                </div>
                <div class="col-md-8">
                    {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'password_confirmation') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    {!! Form::submit('Save Password', ['class'=>'btn btn-success btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop