<?php $title = 'Reset Password'; ?>
@extends('layouts.default')

@section('content')
    <div class="col-md-4  col-md-offset-4 login-form">
        <div class="panel panel-login">
            <div class="login-icon">
                <p class="alert-info">Enter the email/username for your account to reset the password</p>
            </div>

            {!! Form::open([]) !!}

                <div class="form-group">
                    {!! Form::label('username', 'Username') !!}

                    {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Username']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'username') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}

                    {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Account email']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>

            <p class="alert-warning">Enter any of the details above that you remember, if you remember none, kindly add an email that we can contact you through</p>
                <div class="form-group">
                    {!! Form::button('Submit', ['type'=>'submit', 'name'=>'login', 'class'=>'form-control btn btn-success btn-block']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection