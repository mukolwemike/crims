<div id="flash-overlay-modal{!! $id = microtime() !!}" class="modal fade {!! $modalClass or '' !!}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">{!! $msg->title !!}</h4>
            </div>

            <div class="modal-body">
                <p>{!! $msg->message !!}</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>