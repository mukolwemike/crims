@if(Session::has('flash_notifier_message'))
    @foreach($msgs = Session::get('flash_notifier_message') as $msg)
        @if($msg->overlay)
            <script>
                swal(
                    '{{ $msg->title }}',
                    '{{ $msg->message }}',
                    'info'
                )
            </script>
            {{--@include('notifications.overlay')--}}
        @else
            <script>
                window.toastr.options = {
                    "closeButton": true,
//                    "debug": true,
//                    "newestOnTop": false,
//                    "progressBar": false,
                    "positionClass": "toast-top-right",
//                    "preventDuplicates": false,
//                    "onclick": null,
//                    "showDuration": "800",
//                    "hideDuration": "1000",
//                    "timeOut": 0,
//                    "extendedTimeOut": 0,
//                    "showEasing": "swing",
//                    "hideEasing": "linear",
//                    "showMethod": "fadeIn",
//                    "hideMethod": "fadeOut",
                    "tapToDismiss": true
                };

                setTimeout(function () {
                    window.toastr["{!! $msg->level !!}"]( "{!! \Cytonn\Core\DataStructures\Str::removeLineBreaks($msg->message) !!}" )
                }, 100);
            </script>
        @endif
    @endforeach
@endif

<?php Session::forget('flash_notifier_message')?>
