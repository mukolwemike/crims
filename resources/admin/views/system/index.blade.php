@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <menu-tile icon="fa-list-alt" link="/dashboard/system/closed_periods" text="Closing Periods">
                Closing Periods
            </menu-tile>
            <menu-tile icon="fa-list-alt" link="/dashboard/system/exemptions" text="Exemptions">
                Exemptions
            </menu-tile>

            <menu-tile icon="fa-list-alt" link="/dashboard/system/approvals" text="Approval Setup">
                Approvals
            </menu-tile>
        </div>
    </div>
@stop