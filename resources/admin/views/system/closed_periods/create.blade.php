@extends('layouts.default')

@section('content')
    <div class = "col-md-6 col-md-offset-3">
        <div class = "panel-dashboard">
            <div class = "detail-group">
                {!! Form::model($closedPeriod, ['route' => ['closed_periods.store', $closedPeriod->id]]) !!}

                <div class = "form-group">
                    {!! Form::label('start', 'Enter the Closed Period Start Date') !!}
                    {!! Form::text('start', NULL, ['id'=>'start', 'class'=>'form-control','datepicker-popup init-model'=>"start", 'is-open'=>"status.opened_1", 'ng-focus'=>'status.opened_1 = !status.opened_1', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'start') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('end', 'Enter the Closed Period End Date') !!}
                    {!! Form::text('end', NULL, ['id'=>'end', 'class'=>'form-control','datepicker-popup init-model'=>"end", 'is-open'=>"status.opened_2", 'ng-focus'=>'status.opened_2 = !status.opened_2', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'end') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('active', 'Status') !!}
                    {!! Form::select('active', ['1' => 'Active', '0' => 'Inactive'], null , ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'active') !!}
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
