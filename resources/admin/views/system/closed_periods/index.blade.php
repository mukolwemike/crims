@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <div ng-controller = "closedPeriodsController">
                <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                    <thead>
                    <tr class="no-print">
                        <th  class="no-print" colspan="2"><st-date-range predicate="date" before="query.before" after="query.after"></st-date-range></th>
                        <th>
                            {!! Form::label('active', 'Status') !!}
                            {!! Form::select('active', ['' => 'All', '1' => 'Active', '0' => 'Inactive' ], null, ['ng-model'=>'active', 'class'=>'form-control', 'ng-change' => 'callServer(tableState)']) !!}
                        </th>
                        <th>
                            <a class="margin-bottom-20 pull-right btn btn-success" href="/dashboard/system/closed_periods/create"> <i class="fa fa-plus"></i> Add new</a>
                        </th>
                    </tr>
                    <tr>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed" class="no-page-break">
                        <td><% row.start %></td>
                        <td><% row.end %></td>
                        <td><% row.active %></td>
                        <td><a href="/dashboard/system/closed_periods/create/<% row.id %>" class="fa fa-edit"></a> </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="4" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot class="no-print">
                    <tr>
                        <td colspan = "100%" class = "text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">Start: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">End: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop