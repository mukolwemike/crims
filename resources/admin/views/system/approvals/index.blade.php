@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h4>Client Approvals</h4>
            <table class="table table-responsive table-striped">
                <thead>
                    <tr><th>Name</th><th>Applies to</th><th>Transactions</th><th>Approvers</th></tr>
                </thead>
                <tbody>
                    @foreach($stages as $stage)
                        <tr>
                            <td>{{ $stage->name }}</td>
                            <td>
                                @if($stage->applies_to_all)
                                    All Transactions
                                @else
                                    Selected Transactions Only
                                @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-success" href="#link-transaction{{ $stage->id }}" data-toggle="modal">Add</a>
                                    <a class="btn btn-sm btn-info" href="#view-transaction{{ $stage->id }}" data-toggle="modal">View</a>
                                    @include('system.approvals.partials.link_transaction', ['stage' => $stage])
                                    @include('system.approvals.partials.view_transactions', ['stage' => $stage])
                                </div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-success" href="#add-approver{{ $stage->id }}" data-toggle="modal">Add</a>
                                    <a class="btn btn-sm btn-info" href="#view-approvers{{ $stage->id }}" data-toggle="modal">View</a>
                                    @include('system.approvals.partials.view_approvers', ['stage' => $stage])
                                    @include('system.approvals.partials.add_approver', ['stage' => $stage])
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class = "panel-dashboard">
            <h4>Portfolio Approvals</h4>
            <table class="table table-responsive table-striped">
                <thead>
                <tr><th>Name</th><th>Applies to</th><th>Transactions</th><th>Approvers</th></tr>
                </thead>
                <tbody>
                @foreach($portfolioStages as $portfolioStage)
                    <tr>
                        <td>{{ $portfolioStage->name }}</td>
                        <td>
                            @if($portfolioStage->applies_to_all)
                                All Transactions
                            @else
                                Selected Transactions Only
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-sm btn-success" href="#link-portfolio-transaction{{ $portfolioStage->id }}" data-toggle="modal">Add</a>
                                <a class="btn btn-sm btn-info" href="#view-portfolio-transaction{{ $portfolioStage->id }}" data-toggle="modal">View</a>
                                @include('system.approvals.partials.link_portfolio_transaction', ['portfolioStage' => $portfolioStage])
                                @include('system.approvals.partials.view_portfolio_transactions', ['portfolioStage' => $portfolioStage])
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-sm btn-success" href="#add-portfolio-approver{{ $portfolioStage->id }}" data-toggle="modal">Add</a>
                                <a class="btn btn-sm btn-info" href="#view-portfolio-approvers{{ $portfolioStage->id }}" data-toggle="modal">View</a>
                                @include('system.approvals.partials.view_portfolio_approvers', ['portfolioStage' => $portfolioStage])
                                @include('system.approvals.partials.add_portfolio_approver', ['portfolioStage' => $portfolioStage])
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop