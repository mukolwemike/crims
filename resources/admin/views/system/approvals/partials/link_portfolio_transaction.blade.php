<div class="modal fade" id="link-portfolio-transaction{{ $portfolioStage->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">{{ $portfolioStage->name }}</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    <p>Select a transaction from the list below:</p>

                    {{ Form::open(['route' => ['add_porfolio_transaction_type_to_approval_stage', $portfolioStage->id]]) }}
                    <div class="form-group">
                        {{ Form::label('transaction', 'Transaction') }}

                        {{ Form::select('transaction', $portfolioTransactions->sortBy('name')->lists('name', 'id'), null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
        </div>
    </div>
</div>