<div class="modal fade" id="view-approvers{{ $stage->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">{{ $stage->name }}</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>The following users can approve transactions in this stage</p>

                        <table class="table table-responsive table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th><th>Email</th><th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($stage->approvers as $user)
                                <tr>
                                    <td>{{ \Cytonn\Presenters\UserPresenter::presentFullNames($user->id) }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td><a href="{{ route('delink_transaction_stage_to_approver', [$stage->id, $user->id]) }}" class="btn btn-sm btn-danger">Remove</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
        </div>
    </div>
</div>