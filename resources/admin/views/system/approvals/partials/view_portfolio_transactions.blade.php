<div class="modal fade" id="view-portfolio-transaction{{ $portfolioStage->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">{{ $portfolioStage->name }}</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    <p>The following transaction types have to be approved in this stage:</p>

                    <table class="table table-responsive table-hover">
                        <thead>
                        <tr>
                            <th>Transaction</th><th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($portfolioStage->transactionTypes as $portfolioType)
                            <tr>
                                <td>{{ $portfolioType->name }}</td>
                                <td><a href="{{ route('remove_portfolio_transaction_type_to_approval_stage', [$portfolioStage->id, $portfolioType->id]) }}" class="btn btn-sm btn-danger">Remove</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
        </div>
    </div>
</div>