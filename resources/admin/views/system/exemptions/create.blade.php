@extends('layouts.default')

@section('content')
    <div class = "col-md-6 col-md-offset-3">
        <div class = "panel-dashboard" ng-controller = "exemptionsCreateController">
            <div class = "detail-group">
                {!! Form::model($exemption, ['route' => ['exemptions.store', $exemption->id]]) !!}

                <div class = "form-group">
                    {!! Form::label('type', 'Type') !!}
                    {!! Form::select('type', ['' => 'Select exemption type', '1' => 'Withdrawal Gaurd', '2' => 'Payment Dates', '3' => 'Real Estate', '4' => 'Investment Edit'], null , ['class'=>'form-control', 'ng-model'=>'type', 'ng-init' => "type='$exemption->type'"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('client_transaction_approval_id', 'Client Transaction Approval') !!}
                    {!! Form::select('client_transaction_approval_id', $approvals, null , ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_transaction_approval_id') !!}
                </div>

                <div class = "form-group" ng-if="type != 1 && type != 4">
                    {!! Form::label('date', 'Date') !!}
                    {!! Form::text('date', NULL, ['id'=>'date', 'class'=>'form-control','datepicker-popup init-model'=>"date", 'is-open'=>"status.opened_1", 'ng-focus'=>'status.opened_1 = !status.opened_1']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('reason', 'Enter the reason for exemption') !!}
                    {!! Form::textarea('reason', NULL, ['class'=>'form-control', 'rows' => '3', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                </div>

                {!! Form::hidden('id', $exemption->id) !!}
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
