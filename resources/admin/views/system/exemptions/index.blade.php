@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <div ng-controller = "exemptionsController">
                <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                    <thead>
                    <tr class="no-print">
                        <th class="no-print" colspan="3"></th>
                        <th>
                            {!! Form::label('type', 'Type') !!}
                            {!! Form::select('type', ['' => 'All', '1' => 'Withdrawal Gaurd', '0' => 'Closed Period' ], null, ['ng-model'=>'type', 'class'=>'form-control', 'ng-change' => 'callServer(tableState)']) !!}
                        </th>
                        <th>
                            <a class="margin-bottom-20 pull-right btn btn-success" href="/dashboard/system/exemptions/create"> <i class="fa fa-plus"></i> Add new</a>
                        </th>
                    </tr>
                    <tr>
                        <th>Client Code</th>
                        <th>Name</th>
                        <th>Reason</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed" class="no-page-break">
                        <td><% row.client_code %></td>
                        <td><% row.name %></td>
                        <td><% row.reason %></td>
                        <td><% row.date %></td>
                        <td><a href="/dashboard/system/exemptions/create/<% row.id %>" class="fa fa-edit"></a> </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="5" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot class="no-print">
                    <tr>
                        <td colspan = "100%" class = "text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@stop