<p style="text-align: right">{!! \Cytonn\Presenters\DatePresenter::formatDate($as_at_date) !!}</p>

<h1 style="text-align: center">Portfolio Summary for {!! $institution->name !!}</h1>

<table>
    <thead>
        <tr><th>Invested Principal</th><th>Principal Repaid</th><th>Current Principal</th><th>Rate</th><th>Value Date</th><th>Maturity Date</th><th>Gross Interest</th><th>Withholding Tax</th><th>Net Interest</th><th>Interest Repaid</th><th>Withdrawn</th><th>Total Value</th></tr>
    </thead>
    <tbody>
        @foreach($investments as $investment)
            <tr>
                <td>{!! $investment->amount !!}</td>
                <td>{!! $investment->repayments()->ofType('principal_repayment')->before($as_at_date)->sum('amount') !!}</td>
                <td>{!! $investment->repo->principal($as_at_date) !!}</td>
                <td>{!! $investment->interest_rate !!}%</td>
                <td>{!! $investment->invested_date !!}</td>
                @if($investment->on_call)
                    <td>On call</td>
                @else
                    <td>{!! $investment->maturity_date !!}</td>
                @endif
                <td>{!! $investment->repo->getGrossInterestForInvestment($as_at_date, true) !!}</td>
                <td>{!! $investment->repo->getWithholdingTaxForInvestment() !!}</td>
                <td>{!! $investment->repo->getNetInterestForInvestment($as_at_date, true) !!}</td>
                <td> {!! $investment->repayments()->ofType('interest_repayment')->before($as_at_date)->sum('amount') !!}</td>
                <td>{!! $investment->withdrawal_date !!}</td>
                <td>{!! $investment->repo->getTotalValueOfAnInvestmentAsAtDate($as_at_date, true) !!}</td>
            </tr>
        @endforeach
            <tr>
                <th>{!! $institution->depositRepo($currency)->principal($as_at_date, true) !!}</th>
                <th colspan="10"><span style="font-style: italic;">Totals for Investments not withdrawn</span></th>
                <th>{!! $institution->depositRepo($currency)->total($as_at_date, true) !!}</th>
            </tr>
    </tbody>
</table>