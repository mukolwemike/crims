@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2" ng-controller="portfolioInvestmentWithdrawalCtrl">
        <div class="panel-dashboard">
            <h3>Redeem Investment</h3>

            @include('portfolio.partials.investmentdetails')

            <div class="detail-group group">
                <div class="col-md-6">
                    <h4> Redeem Investment</h4>
                    {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}
                    {!! Form::hidden('deposit_id', Crypt::encrypt($deposit->id)) !!}

                    <div class="form-group">
                        {!! Form::checkbox('premature', null, false, ['ng-model'=>'premature', 'id'=>'premature', 'ng-init'=>'premature', 'aria-label'=>'Toggle ngHide']) !!}

                        {!! Form::label('premature', 'Premature Redemption' ) !!}
                    </div>
                    <br ng-hide="premature"/>

                    <div ng-show="premature">
                        <div class="form-group" ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'Enter the date when investment ends', ['class'=>'','ng-show'=>'premature']) !!}

                            {!! Form::date('end_date', null, ['id'=>'end_date','class'=>'form-control', 'ng-show'=>'premature', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'end_date') !!}
                        </div>

                        <div class="form-group">
                            {!! Form::checkbox('partial', null, false, ['ng-model'=>'partial', 'id'=>'partial']) !!}

                            {!! Form::label('Partial Redemption') !!}
                        </div>

                        <div class="form-group" ng-show="partial">
                            {!! Form::label('amount', 'Amount to Withdraw') !!}

                            {!! Form::number('amount', null, ['class'=>'form-control', 'id'=>'amount', 'step'=>'0.001']) !!}
                        </div>
                    </div>


                    <div class="form-group">
                        {!! Form::submit('Redeem', ['class'=>'btn btn-success']) !!}
                    </div>

                    {!! Form::close() !!}

                    <div ng-view></div>
                    <script type = "text/ng-template" id = "portfoliowithdraw.htm">
                        <div class = "modal fade">
                            <div class = "modal-dialog">
                                <div class = "modal-content">

                                    <div class = "modal-header no-bottom-border">
                                        <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                                data-dismiss = "modal" aria-hidden = "true">&times;</button>
                                        <h4 class = "modal-title">Confirm Investment Redemption</h4>
                                    </div>
                                    <div class = "modal-body">

                                        <div class = "detail-group">
                                            <p>Are you sure you want to Redeem this investment with the details below?</p>
                                            <table class="table table-hover">
                                                <tbody>
                                                <tr><td>Institution</td><td>{!! $deposit->security->investor->name !!}</td></tr>
{{--                                                <tr><td>Investment Type</td><td>{!! $deposit->security->subAsstClass->assetClass->name !!}</td></tr>--}}
                                                <tr><td>Principal</td><td>{!! $deposit->amount !!}</td></tr>
                                                <tr><td>Value Today</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestment()) !!}</td></tr>
                                                <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date) !!}</td></tr>
                                                <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td></tr>
                                                <tr><td>Interest rate</td><td>{!! $deposit->interest_rate !!}</tr>
                                                <tr id="endDate" ng-show="endDate" class="ng-hide"><td>Investment End Date</td><td class="end-date"><% endDate %></tr>
                                                <tr ng-show="partial" class="alert alert-danger"><td colspan="2"><p class="center">Partial Withdrawal</p></td></tr>
                                                <tr ng-show="partial" class="alert alert-danger"><td>Partial Withdraw Amount</td><td><p><% amount | currency:"" %></p></td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class = "detail-group">
                                            <div class = "pull-right">
                                                {!! Form::open(['route'=>'portfolio_withdraw']) !!}
                                                {!! Form::hidden('deposit_id', Crypt::encrypt($deposit->id)) !!}
                                                <div style="display: none !important;">
                                                    {!! Form::text('premature', null, ['ng-model'=>'premature']) !!}
                                                    {!! Form::text('end_date', null, ['ng-model'=>'endDate ']) !!}
                                                    {!! Form::text('partial', null, ['ng-model'=>'partial']) !!}
                                                    {!! Form::text('amount', null, ['ng-model'=>'amount']) !!}
                                                </div>

                                                <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                                        data-dismiss = "modal">No
                                                </button>
                                                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </script>

                </div>
            </div>
        </div>
    </div>
@stop