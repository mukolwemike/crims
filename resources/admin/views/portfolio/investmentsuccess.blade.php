@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h1>New Deposit holding added Successful</h1>
            <div class="detail-group">
                <table class="table table-hover">
                    <thead> </thead>
                    <tbody>
                        <tr>
                            <td><h4>Deposit Details</h4></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Fund Type</td>
                            <td>{!! $fundType->name !!}</td>
                        </tr>
                        @if(isset($investment['type_id']))
                            <tr>
                                <td>Portfolio Investment Type</td>
                                <td>{!! $investmentType->name !!}</td>
                            </tr>
                        @endif
                        @if(isset($investment['nominal_value']))
                        <tr>
                            <td>Nominal Value</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment['nominal_value'] , true) !!}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>Cost Value</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment['amount'] , true) !!}</td>
                        </tr>
                        <tr>
                            <td>Value Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment['invested_date']) !!}</td>
                        </tr>
                        <tr>
                            <td>Maturity Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment['maturity_date'] )!!}</td>
                        </tr>
                        <tr>
                            <td>Interest rate</td>
                            <td>{!! $investment['interest_rate'] !!}%</td>
                        </tr>
                        @if(isset($investment['contact_person']))
                            <tr>
                                <td>Contact Person</td>
                                <td>{!! $investment['contact_person'] !!}</td>
                            </tr>
                        @endif
                        @if(isset($investment['description']))
                            <tr>
                                <td>Transaction Description</td>
                                <td>{!! $investment['description'] !!}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
