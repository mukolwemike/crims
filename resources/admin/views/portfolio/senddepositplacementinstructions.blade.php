@extends('layouts.default')

@section('content')
    <div>
        <div class="panel-dashboard">
            <div class="col-md-6">
                @include('portfolio.partials.investmentdetails')
            </div>
            <div class="col-md-6">
                <div class="detail-group">
                    <h3>Send Deposit Placement Instructions</h3>

                    @if($deposit->security->depositType && $deposit->security->contact_person)
                        {!! Form::model($deposit, ['route'=>['send_deposit_placement_instructions', $deposit->id], 'method'=>'POST']) !!}

                        <div class="form-group">
                            {!! Form::label('first_signatory', 'First Signatory') !!}

                            <select class="form-control" name="first_signatory">
                                @foreach($users as $user)
                                    <option value="{!! $user->id !!}">{!! $user->fullName !!}</option>
                                @endforeach
                            </select>

                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory') !!}
                        </div>

                        <div class="form-grouMp">
                            {!! Form::label('second_signatory', 'Second Signatory') !!}

                            <select class="form-control" name="second_signatory">
                                @foreach($users as $user)
                                    <option value="{!! $user->id !!}">{!! $user->fullName !!}</option>
                                @endforeach
                            </select>

                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory') !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Send Instructions', ['class'=>'btn btn-success']) !!}
                        </div>

                        {!! Form::close() !!}
                    @else
                        <p>Portfolio investment type has not been specified.</p>
                        <p>Kindly edit the investment to include the investment type so as to allow you to generate the deposit place instruction.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection