@extends('reports.letterhead')

@section('content')

    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! $institution->name !!}
    <br/>

    @if($institution->contact_person_firstname) C/O {!! $institution->contact_person_firstname . ' ' . $institution->contact_person_lastname !!} <br/> @endif

    {!! $institution->address_line1 !!} <br/>

    {!! $institution->address_line2 !!} <br/><br/>

    @if($institution->contact_person_firstname) Dear {!! $institution->contact_person_firstname !!}, @endif

    <p class="bold-underline">RE: STATEMENT OF ACCOUNT</p>

    <p>Kindly see below your loan statement showing account status as at {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}.<br/><br/></p>

    <table class="table table-striped" style="font-size:12px;">
        <thead>
        <tr>
            <th>Description</th><th>Amount</th><th>Rate</th><th>Action Date</th><th>Gross Interest</th><th>Net Interest</th><th>Action</th><th>Total</th><th>Maturity Date</th>
        </tr>
        </thead>
        <?php $count = 1 ?>
        <tbody>
        @foreach($data['investments'] as $investment)
            @include('portfolio.reports.partials.investment')
            <?php $count++ ?>
        @endforeach
        <tr><td colspan="9" height="14px"></td></tr>
        <tr class="right">
            <th class="left">Grand Total</th>
            <th class="right">{!! $currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['principal'], false, 0) !!}</th>
            <th colspan="3"></th>
            <th class="right">{!! $currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['net_interest'], false, 0) !!}</th>
            <th></th>
            <th class="right">{!! $currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['value'], false, 0) !!}</th>
            <th></th>
        </tr>
        </tbody>
    </table>
    <br/>
    @if($fundManager->id != 2 && $campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif
    <p>Thank you for doing business with us.</p><br/>
    <p>Yours Sincerely,</p>
    </br>
    <p class="bold"> For {!! $fundManager->fullname !!}

        <br/>
        {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender->id) !!}
    </p>
@stop
