@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="panel">
                <div class="panel-heading">
                    <h3>Portfolio Reports</h3>
                </div>
                <div class="panel-body">
                    <el-tabs type="card">
                        <el-tab-pane label="SP Reports">
                            <table class="table table-striped table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Interest Income Report</td>
                                    <td>
                                        <a href="#interest-expense-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Portfolio Withholding Tax</td>
                                    <td>
                                        <a href="#portfolio-withholding-tax-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Custodial Accounts</td>
                                    <td>
                                        <a href="#custodial-accounts-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Deposit Analysis</td>
                                    <td>
                                        <a href="#deposit-analysis-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Residual Income Analysis</td>
                                    <td>
                                        <a href="#residual-income-analysis-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Investments On Call</td>
                                    <td>
                                        {!! Form::open(['route'=>['export_portfolio_investments_on_call_path'], 'method'=>'POST']) !!}
                                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Deposit Holdings</td>
                                    <td>
                                        <a href="#export-deposit-holdings" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Portfolio Summary</td>
                                    <td>
                                        <a href="#portfolio-summary-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Yearly Deposit Holdings Summary</td>
                                    <td>
                                        {!! Form::open(['route'=>['export_yearly_deposit_holdings'], 'method'=>'POST']) !!}
                                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Investors Valuation Report</td>
                                    <td>
                                        {!! Form::open(['route'=>['export_investor_valuation'], 'method'=>'POST']) !!}
                                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Portfolio Movement Report</td>
                                    <td>
                                        <a href="#portfolio-movement-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Loan Tenor Range Report</td>
                                    <td>
                                        <a href="#loan-tenor-range-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </el-tab-pane>
                        <el-tab-pane label="UTF Reports">
                            <table class="table table-striped table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Transactions Report</td>
                                        <td>
                                            <a href="#unit-fund-transaction-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Holdings Report</td>
                                        <td>
                                            <a href="#unit-fund-holdings-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Performance Attribution Summary</td>
                                        <td>
                                            <a href="#performance-attribution-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Fund Pricing Summary</td>
                                        <td>
                                            <a href="#fund-pricing-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Fund Management Summary</td>
                                        <td>
                                            <a href="#fund-management-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Summary Valuation</td>
                                        <td>
                                            <a href="#fund-valuation-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </el-tab-pane>
                    </el-tabs>
                </div>
            </div>
        </div>
    </div>
    {{--start of interest expense report modal--}}
    <div class="modal fade" id="interest-expense-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_portfolio_interest_expense_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Interest Expense report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Choose the start and end dates</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of interest expense report modal--}}
    {{--start of portfolio withholding tax modal--}}
    <div class="modal fade" id="portfolio-withholding-tax-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_portfolio_withholding_tax_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Portfolio Withholding Tax</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Choose the start and end dates</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of portfolio withholding tax modal--}}
    {{--start of custodial accounts modal--}}
    <div class="modal fade" id="custodial-accounts-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_custodial_accounts_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Custodial Account</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the custodial account,start date and end date</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('custodial_account_id', 'Custodial Account') !!}
                            {!! Form::select('custodial_account_id', \App\Cytonn\Models\CustodialAccount::all()->lists('full_name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of custodial accounts modal--}}
    {{--start of deposit analysis modal--}}
    <div class="modal fade" id="residual-income-analysis-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_residual_income_analysis_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Residual Income Analysis</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the fund manager and the date</p>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('fund_manager_id', 'Custodial Account') !!}
                            {!! Form::select('fund_manager_id', \App\Cytonn\Models\FundManager::all()->lists('fullname', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Date') !!}
                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--start of deposit analysis modal--}}


    {{--start of deposit analysis modal--}}
    <div class="modal fade" id="deposit-analysis-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_deposit_analysis_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Deposit Analysis</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the fund manager and the date</p>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('fund_manager_id', 'Custodial Account') !!}
                            {!! Form::select('fund_manager_id', \App\Cytonn\Models\FundManager::all()->lists('fullname', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Date') !!}
                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--start of deposit analysis modal--}}

    {{--end of portfolio summary modal--}}
    <div class="modal fade" id="portfolio-summary-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_portfolio_summary_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Portfolio Summary</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the fund manager and the date</p>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                            {!! Form::select('fund_manager_id', \App\Cytonn\Models\FundManager::all()->lists('fullname', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Date') !!}
                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of portfolio summary modal--}}

    {{--start of interest expense report modal--}}
    <div class="modal fade" id="portfolio-movement-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_portfolio_movement_audit_report'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Portfolio Movement Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the fund manager and the start and end dates</p>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                            {!! Form::select('fund_manager_id', \App\Cytonn\Models\FundManager::all()->lists('fullname', 'id'), NULL, ['class'=>'form-control', 'required']) !!}
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--LOAN TENOR RANGE--}}
    <div class="modal fade" id="loan-tenor-range-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_loan_tenor_range_report'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Loan Tenor Range Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the fund manager and the start and end dates</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'End Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                            {!! Form::select('fund_manager_id', \App\Cytonn\Models\FundManager::all()->lists('fullname', 'id'), NULL, ['class'=>'form-control', 'required']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('type_id', 'Tenor Based On') !!}
                            {!! Form::select('type_id', [1 => 'Loan Invested Date', 2 => 'Selected End Date'], 1, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--Unit Fund Transactions Summary--}}
    <div class="modal fade" id="unit-fund-transaction-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_unit_fund_transaction_summary'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Transaction Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the unit fund ,start date and end date</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of custodial accounts modal--}}

    {{--Unit Fund Holdings Summary--}}
    <div class="modal fade" id="unit-fund-holdings-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_unit_fund_holdings_summary'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Holdings Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the unit fund and datee</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Date') !!}
                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of custodial accounts modal--}}

    {{--Performance Attribution Summary--}}
    <div class="modal fade" id="performance-attribution-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_performance_attribution_summary'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Performance Attribution Summary</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select fund and dates</p>
                        </div>

                        {{--<div class="col-md-12">--}}
                            {{--{!! Form::label('for', 'For Fund/Fund Manager') !!}--}}
                            {{--{!! Form::select('for', ['fund' => 'For fund', 'fm' => 'For Fund Manager'], NULL, ['class'=>'form-control', 'required']) !!}--}}
                        {{--</div>--}}



                        {{--<div class="col-md-12">--}}
                            {{--{!! Form::label('fundmanager', 'Fund Manager') !!}--}}
                            {{--{!! Form::select('fundmanager', $fundManagers, NULL, ['class'=>'form-control', 'required']) !!}--}}
                        {{--</div>--}}
                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of custodial accounts modal--}}

    <div class="modal fade" id="fund-pricing-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_unit_fund_pricing_summary'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Unit Fund Pricing Summary</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the unit fund</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Start Date') !!}
                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="fund-management-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_unit_fund_management_summary'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Unit Fund Management Summary</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the unit fund</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="fund-valuation-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_unit_fund_valuation_summary'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Summary Valuation</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the unit fund</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="export-deposit-holdings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_unit_fund_deposit_holdings'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Unit Fund Deposit Holdings</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select the unit fund</p>
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                            {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop