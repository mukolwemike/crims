<style>
    tbody tr td, tbody tr th, .bordered td, .bordered th{
        border: 1px solid #000;
    }
</style>
<table>
    <thead>
        <tr>
            <td colspan="4">Summary Valuation Report</td>
        </tr>
        <tr>
            <td>Fund </td><td colspan="3">{{ $fund->name }}</td>
        </tr>
        <tr>
            <td>From </td><td>{{ $start->toFormattedDateString() }}</td><td>To </td><td>{{ $end->toFormattedDateString() }}</td>
        </tr>
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr class="bordered">
            <th></th>
            @foreach($quarters as $quarter)
                <th>{{ $quarter->end->toFormattedDateString() }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Opening Fund Value</th>
            @foreach($performances as $performance)
                <th>{{ $performance->opening_balance }}</th>
            @endforeach
        </tr>
        <tr>
            <td>Contributions</td>
            @foreach($performances as $performance)
                <td>{{ $performance->contributions }}</td>
            @endforeach
        </tr>
        <tr>
            <td>Withdrawals</td>
            @foreach($performances as $performance)
                <td>{{ $performance->withdrawals }}</td>
            @endforeach
        </tr>
        <tr>
            <td>Investment Growth (Gross)</td>
            @foreach($performances as $performance)
                <td>{{ $performance->gross_growth }}</td>
            @endforeach
        </tr>
        <tr>
            <td>Investment Growth (Net)</td>
            @foreach($performances as $performance)
                <td>{{ $performance->net_growth }}</td>
            @endforeach
        </tr>
        <tr>
            <th>Closing Fund Value</th>
            @foreach($performances as $performance)
                <th>{{ $performance->closing_balance }}</th>
            @endforeach
        </tr>
        <tr>
            <td style="font-style: italic; padding-left: 4px">Performance Statistics</td>
            @foreach($performances as $performance)
                <td></td>
            @endforeach
        </tr>
        <tr>
            <td>Time weighted return</td>
            @foreach($performances as $performance)
                <td>{{ $performance->weighted_return }}</td>
            @endforeach
        </tr>
    </tbody>
</table>