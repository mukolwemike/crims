<?php $interest_date = $investment->investment->invested_date; ?>
<tr><td colspan="9" style="padding-left: 10px"><b>{!! $count !!}.</b></td></tr>
<tr>
    <td>Principal</td>
    <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->investment->amount, false, 0) !!}</td>
    <td align="center">{!! $investment->investment->interest_rate !!}%</td>
    <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($investment->investment->invested_date) !!}</td>
    <td align="right"></td>
    <td align="right"></td>
    <td align="right"></td>
    <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->investment->amount, false, 0) !!}</td>
    @if($investment->investment->on_call)
        <td>On call</td>
    @else
        <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($investment->investment->maturity_date) !!}</td>
    @endif
</tr>

@foreach($investment->actions as $payment)
    <tr>
        <td>{!! $payment->description !!}</td>
        <td></td>
        <td></td>
        <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($payment->date) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($payment->gross_interest, false, 0) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($payment->net_interest, false, 0) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::accounting($payment->amount, false, 0) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($payment->total, false, 0) !!}</td>
        <td></td>
    </tr>
@endforeach

<tr class="bold">
    <td class="bold">Total</td>
    <td class="bold right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->total->principal, false, 0) !!}</td>
    <td align="center">{!! $investment->investment->interest_rate !!}%</td>
    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($statementDate) !!}</td>
    <td class="bold right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->total->gross_interest_cumulative, false, 0) !!}</td>
    <td class="bold right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->total->net_interest_cumulative, false, 0) !!}</td>
    <td></td>
    <td class="bold right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->total->total,false, 0) !!}</td>
    <td class="bold">{!! \Cytonn\Presenters\DatePresenter::formatShortDate($investment->investment->maturity_date) !!}</td>
</tr>