@extends('reports.letterhead')

@section('content')
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    Dear Sir/Madam,

    <p class="bold-underline">REF : ORDER PLACEMENT REQUEST</p>
    <div class="body">
        <p>Kindly find the order below for execution;</p>

        <table class="table table-responsive">
            <tbody>
            <?php $assetClass = $order->security->subAssetClass->assetClass->name ?>
            <tr class="green-header">
                <th colspan="3">
                    Order Details
                </th>
            </tr>
            <tr>
                <td>Company</td>
                <td colspan="2">{!! $order->security->name !!}</td>
            </tr>
            <tr>
                <td>Order Type</td>
                <td colspan="2">{!! $order->type->name !!}</td>
            </tr>
            <tr>
                @if($assetClass == 'Equities')
                    <td>Cut of Price</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($order->cut_off_price) !!}</td>
                @else
                    <td>Minimum Rate</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($order->minimum_rate) !!}</td>
                @endif
            </tr>
            <tr class="green-header">
                <th colspan="3">Allocations</th>
            </tr>

            <tr class="green-header">
                <th>Unit Fund</th>
                <th>CDSC Account</th>
                @if($assetClass == 'Equities')
                    <th>No. of Shares</th>
                @else
                    <th>Amount</th>
                @endif
            </tr>
            @foreach($order->unitFundOrders as $unitFundOrder)
                <tr>
                    <td>{!! $unitFundOrder->unitFund->name !!}</td>
                    <td>{!! $unitFundOrder->custodialAccount->cdsc_number !!}</td>
                    @if($assetClass == 'Equities')
                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($unitFundOrder->shares) !!}</td>
                    @else
                        <td>{!! $unitFundOrder->custodialAccount->currency->code !!} {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($unitFundOrder->amount) !!}</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <p>Yours sincerely,</p><br>

    <span style="display: inline-block; width: 70%;">
    {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing(auth()->id()) !!}
    </span>
@stop