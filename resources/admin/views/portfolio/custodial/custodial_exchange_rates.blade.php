@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
    <div class = "panel-dashboard">
        <div ng-controller = "custodialExchangeRatesController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr class="no-print">
                    <th  class="no-print" colspan="2"><st-date-range predicate="date" before="query.before" after="query.after"></st-date-range></th>
                    <th>
                        {!! Form::label('base', 'Base') !!}
                        {!! Form::select('base_id', $baseCurrencies, null, ['ng-model'=>'base_id', 'class'=>'form-control', 'ng-change' => 'callServer(tableState)']) !!}
                    </th>
                    <th>
                        {!! Form::label('to', 'To') !!}
                        {!! Form::select('to_id', $toCurrencies, null, ['ng-model'=>'to_id', 'class'=>'form-control', 'ng-change' => 'callServer(tableState)']) !!}
                    </th>
                </tr>
                <tr>
                    <th colspan="2">Currency Combination</th>
                    <th>Rate</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody ng-show="!isLoading">
                <tr ng-repeat = "row in displayed" class="no-page-break">
                    <td colspan="2"><% row.combination %></td>
                    <td><% row.rate %></td>
                    <td><% row.date %></td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="4" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot class="no-print">
                <tr>
                    <td colspan = "100%" class = "text-center">
                        <dmc-pagination></dmc-pagination>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop