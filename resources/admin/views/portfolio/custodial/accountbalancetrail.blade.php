@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">

            <div ng-controller="CustodialAccountBalanceTrailGridController">
                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="2" ng-controller="DatepickerCtrl">
                                <div class="col-md-3 margin-top-10">Filter by date</div>
                                <div class="col-md-9"><input type="date" st-search="date" class="form-control" /></div>
                            </th>
                            <th>
                                <a href="/dashboard/portfolio/custodials/account-balance-trail/{!! $custodial->id !!}/export" class="btn btn-primary margin-bottom-20" target="_self"><i class="fa fa-download"></i> Download</a>
                            </th>
                        </tr>
                        <tr>
                            <th st-sort="id">Trail ID</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody  ng-show="!isLoading">
                        <tr ng-repeat="row in displayed">
                            <td><% row.id %></td>
                            <td><% row.date %></td>
                            <td><% row.amount | currency:"" %></td>
                            <td>
                                <a href="#" style="color: red;" title="Delete balance trail" data-toggle="modal" data-target="#confirm-delete-<% row.id %>"><i class="fa fa-close"></i> </a> </a>

                                <!-- Confirm Delete Modal -->
                                <div class="modal fade" id="confirm-delete-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="myModalLabel">Delete Balance Trail ID: <% row.id %></h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure that you want to delete this custodial account balance trail?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <a href="/dashboard/portfolio/custodials/account-balance-trail/delete/<% row.id %>" target="_self" class="btn btn-danger pull-left">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="3" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td  colspan="1" class="text-center">
                                Items per page
                            </td>
                            <td colspan="1" class="text-center">
                                <input type="text"  ng-model="itemsByPage"/>
                            </td>
                            <td colspan="2" class="text-center">
                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@endsection