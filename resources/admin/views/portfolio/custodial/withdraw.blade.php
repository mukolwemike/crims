<?php $custodial = $account ?>

@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="detail-group">
                <h3>Account Details</h3>

                <table class="table table-hover table-responsive">
                    <thead></thead>
                    <tbody>
                    <tr><td>Account Name</td><td>{!! $custodial->account_name !!} </td></tr>
                    <tr><td>Account Number</td><td>{!! $custodial->account_no !!}</td></tr>
                    <tr><td>Bank</td><td>{!! $custodial->bank_name !!}</td></tr>
                    <tr><td>Currency</td><td>{!! $custodial->currency->name !!}</td></tr>
                    </tbody>
                </table>
            </div>

            <div>
                <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class=""><a href="#withdraw" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Withdraw</a></li>
                    <li role="presentation" class=""><a href="#transfer" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Transfer</a></li>
                    <li role="presentation" class=""><a href="#reconcile" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Reconciliation</a></li>
                    <li role="presentation" class=""><a href="#deposit" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Deposit</a></li>
                    <li role="presentation" class=""><a href="#withholdingTax" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Withholding Tax Payment</a></li>
                    <li role="presentation" class=""><a href="#transferMismatch" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Transfer Cash Accounts</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade" id="withdraw" aria-labelledby="home-tab">
                        <div class="detail-group" ng-controller="CustodialWithdrawController" ng-init="">

                            {!! Form::open(['name'=>'withdrawForm', 'ng-submit'=>'submit($event)']) !!}

                            <div class="alert alert-info">
                                <p>Select transaction and enter amount</p>
                            </div>

                            <div class="form-group">
                                {!! Form::label('transaction', 'Select transaction type') !!}

                                {!! Form::select('transaction', $withdrawTypes, null, ['class'=>'form-control', 'id'=>'transaction', 'init-model'=>'transaction', 'ng-model' => 'transaction', 'ng-required'=>"true"]) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'transaction') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('amount', 'Enter Amount') !!}

                                {!! Form::number('amount', null, ['class'=>'form-control', 'step'=>'0.01', 'ng-required'=>"true", 'init-model'=>'amount']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                            </div>

                            <div class="form-group" ng-controller = "DatepickerCtrl">
                                {!! Form::label('date', 'Date of Transfer') !!}

                                {!! form::text('date', null, ['class'=>'form-control', 'id'=>'date', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('narrative', 'Narrative') !!}

                                {!! Form::text('narrative', null, ['class'=>'form-control', 'ng-required'=>"true", 'init-model'=>'narrative']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                            </div>

                            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="transfer" aria-labelledby="home-tab">
                        <div class="detail-group" ng-controller="AccountTransferController">

                            <div class="alert alert-info">
                                <p>Select destination account, enter exchange rate and amount</p>
                            </div>

                            {!! Form::open(['ng-submit'=>'submit($event)', 'name'=>'transferForm']) !!}

                            <div class="form-group" ng-init="account_id = {!! $custodial->id !!}">
                                {!! Form::label('account', 'Select Destination Account') !!}

                                {!! Form::select('account', $other_accounts, null, ['class'=>'form-control', 'init-model'=>'destination_id']) !!}

                                {{--{!!--<select class="form-control" name="account" id="account"--!!}--}}
                                {{--{!!--ng-options="acc.name for acc in otherAccounts track by acc.id"--!!}--}}
                                {{--{!!--init-model="account">--!!}--}}
                                {{--{!!--</select>--!!}--}}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'destination_id') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('exchange', 'Enter the exchange rate as at the date of transfer') !!}

                                {!! Form::text('exchange', null, ['class'=>'form-control', 'init-model'=>'exchange', 'required']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'effective_rate') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('amount', 'Amount to transfer') !!}

                                <a ng-click="reverse = !reverse">Reverse Exchange Calculation</a>

                                <div class="row">
                                    <div class="col-md-6">
                                        {!! $account->currency->code !!}

                                        {!! Form::text('amount', null, ['class'=>'form-control', 'init-model'=>'amount', 'required']) !!}

                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                    </div>
                                    <div class="col-md-6">
                                        <% account.currency_code %>

                                        {!! Form::text('converted', null, ['class'=>'form-control', 'init-model'=>'converted', 'ng-disabled'=>'true']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" ng-controller = "DatepickerCtrl">
                                {!! Form::label('date', 'Date of Transfer') !!}

                                {!! Form::text('date', null, ['class'=>'form-control' , 'id'=>'transfer_date', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'transfer_date') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('narrative', 'Narrative') !!}

                                {!! Form::text('narrative', null, ['class'=>'form-control', 'required', 'init-model'=>'narrative']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Transfer', ['class'=>'btn btn-success']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- Reconcile -->
                    <div role="tabpanel" class="tab-pane fade" id="reconcile" aria-labelledby="home-tab">
                        <div class="detail-group" ng-controller="CustodialReconcileController">

                            {!! Form::open(['ng-submit'=>'submit($event)', 'name'=>'reconcileForm']) !!}

                            <div class="form-group">
                                {!! Form::label('transaction', 'Select transaction type') !!}
                                {!! Form::select('type', ['credit'=>'Credit', 'debit'=>'Debit'], null, ['class'=>'form-control', 'id'=>'type', 'init-model'=>'type', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('amount', 'Enter Amount') !!}
                                {!! Form::number('amount', null, ['class'=>'form-control', 'step'=>'0.01', 'ng-required'=>"true", 'init-model'=>'amount']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                            </div>

                            <div class="form-group" ng-controller = "DatepickerCtrl">
                                {!! Form::label('date', 'Date of Reconciliation') !!}
                                {!! form::text('reconciliation_date', null, ['class'=>'form-control', 'id'=>'reconciliation_date', 'datepicker-popup init-model'=>"reconciliation_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('narrative', 'Narrative') !!}
                                {!! Form::text('narrative', null, ['class'=>'form-control', 'ng-required'=>"true", 'init-model'=>'narrative']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Reconcile', ['class'=>'btn btn-success']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- Deposit -->
                    <div role="tabpanel" class="tab-pane fade" id="deposit" aria-labelledby="home-tab">
                        <div class="detail-group" ng-controller="CustodialDepositController">

                            <div class="alert alert-info">
                                <p>Deposit by Principal Partner</p>
                            </div>

                            {!! Form::open(['ng-submit'=>'submit($event)', 'name'=>'depositForm']) !!}

                            <div class="form-group">
                                {!! Form::label('transaction', 'Select transaction type') !!}
                                {!! Form::select('deposit_type', ['FMI'=>'Deposit by Principal Partner', 'FBC'=>'Bank Charges'], 'FMI', ['class'=>'form-control', 'id'=>'deposit_type', 'init-model'=>'deposit_type', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('amount', 'Enter Amount') !!}
                                {!! Form::number('amount', null, ['class'=>'form-control', 'step'=>'0.01', 'ng-required'=>"true", 'init-model'=>'amount']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                            </div>

                            <div class="form-group" ng-controller = "DatepickerCtrl">
                                {!! Form::label('date', 'Date of Deposit') !!}
                                {!! form::text('deposit_date', null, ['class'=>'form-control', 'id'=>'deposit_date', 'datepicker-popup init-model'=>"deposit_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('narrative', 'Narrative') !!}
                                {!! Form::text('narrative', null, ['class'=>'form-control', 'ng-required'=>"true", 'init-model'=>'narrative']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Deposit', ['class'=>'btn btn-success']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="withholdingTax" aria-labelledby="home-tab">
                        <div class="detail-group">

                            <div class="alert alert-info">
                                <p>Withholding Tax Payment</p>
                            </div>

                            <div class="">
                                <a href="/dashboard/portfolio/custodial/{{ $custodial->id }}/wht" class="btn btn-success">View Tax/ Make Payment</a>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="transferMismatch" aria-labelledby="home-tab">
                        <div class="detail-group">

                            <div class="alert alert-info">
                                <p>Transfer Mismatched Funds</p>
                            </div>

                            <div class="">
                                <a href="/dashboard/portfolio/custodial/transfer-mismatch/{{ $custodial->id }}" class="btn btn-success">Make Transfer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


<div ng-view></div>
<script type = "text/ng-template" id = "withdraw.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">

                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Custodial Withdrawal</h4>
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Are you sure you want to make the withdrawal with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>Account Name</td>
                                <td>{!! $custodial->account_name !!}</td>
                            </tr>
                            <tr>
                                <td>Bank</td>
                                <td>{!! $custodial->bank_name !!}</td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td>{!! $custodial->currency->name !!}</td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td><% amount | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td><% date | date %></td>
                            </tr>
                            <tr>
                                <td>Transaction</td>
                                <td><% transaction_text %></td>
                            </tr>
                            <tr>
                                <td>Narrative</td>
                                <td><% narrative %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>['custodial_withdraw', $custodial->id]]) !!}
                            <div style = "display: none !important;">
                                {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                                {!! Form::text('date', NULL, ['ng-model'=>'date']) !!}
                                {!! Form::text('transaction', NULL, ['ng-model'=>'transaction']) !!}
                                {!! Form::text('narrative', NULL, ['ng-model'=>'narrative']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<div ng-view></div>
<script type = "text/ng-template" id = "transfer.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">

                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Transfer</h4>
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Are you sure you want to make the transfer with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>Source Account Name</td>
                                <td>{!! $custodial->account_name !!}</td>
                            </tr>
                            <tr>
                                <td>Destination Account Name</td>
                                <td><% account.name %></td>
                            </tr>
                            <tr>
                                <td>Bank</td>
                                <td>{!! $custodial->bank_name !!}</td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td>{!! $custodial->currency->name !!}</td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td><% amount | currency:"" %> {!! $account->currency->code !!} = <% converted | currency:"" %> <% account.currency_code %></td>
                            </tr>
                            <tr>
                                <td>Exchange Rate</td>
                                <td><% exchange %></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td><% transfer_date | date %></td>
                            </tr>
                            <tr>
                                <td>Narrative</td>
                                <td><% narrative %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>['custodial_transfer', $custodial->id]]) !!}
                            <div style = "display: none !important;">
                                {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                                {!! Form::text('transfer_date', NULL, ['ng-model'=>'transfer_date']) !!}
                                {!! Form::text('effective_rate',null, ['ng-model'=>'effective_rate'] ) !!}
                                {!! Form::text('narrative', NULL, ['ng-model'=>'narrative']) !!}
                                {!! Form::text('destination_id', null, ['ng-model'=>'destination_id']) !!}

                                {!! Form::text('exchange', null, ['ng-model'=>'exchange']) !!}
                                {!! Form::text('reverse', null, ['ng-model'=>'reverse']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>


<div ng-view></div>
<script type = "text/ng-template" id = "reconcile.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">
                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Custodial Reconciliation</h4>
                </div>
                <div class = "modal-body">
                    <div class = "detail-group">
                        <p>Are you sure you want to make the reconciliation with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>Account Name</td>
                                <td>{!! $custodial->account_name !!}</td>
                            </tr>
                            <tr>
                                <td>Bank</td>
                                <td>{!! $custodial->bank_name !!}</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td><% type_text %></td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td>{!! $custodial->currency->name !!}</td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td><% amount | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Date <% date %></td>
                                <td><% reconciliation_date | date %></td>
                            </tr>
                            <tr>
                                <td>Narrative</td>
                                <td><% narrative %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>['custodial_reconcile', $custodial->id]]) !!}
                            <div style = "display: none !important;">
                                {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                                {!! Form::text('date', NULL, ['ng-model'=>'reconciliation_date']) !!}
                                {!! Form::text('type', NULL, ['ng-model'=>'type']) !!}
                                {!! Form::text('narrative', NULL, ['ng-model'=>'narrative']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>


<div ng-view></div>
<script type = "text/ng-template" id = "deposit.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">
                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Custodial Deposit</h4>
                </div>
                <div class = "modal-body">
                    <div class = "detail-group">
                        <p>Are you sure you want to make the Deposit by Principal Partner with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>Account Name</td>
                                <td>{!! $custodial->account_name !!}</td>
                            </tr>
                            <tr>
                                <td>Bank</td>
                                <td>{!! $custodial->bank_name !!}</td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td>{!! $custodial->currency->name !!}</td>
                            </tr>
                            <tr>
                                <td>Deposit Type</td>
                                <td><% deposit_type_name %></td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td><% amount | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Date <% date %></td>
                                <td><% deposit_date | date %></td>
                            </tr>
                            <tr>
                                <td>Narrative</td>
                                <td><% narrative %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>['custodial_deposit', $custodial->id]]) !!}
                            <div style = "display: none !important;">
                                {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                                {!! Form::text('date', NULL, ['ng-model'=>'deposit_date']) !!}
                                {!! Form::text('narrative', NULL, ['ng-model'=>'narrative']) !!}
                                {!! Form::text('deposit_type', NULL, ['ng-model' => 'deposit_type']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>