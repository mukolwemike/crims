@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <td colspan="3"><h4>Unit Funds</h4></td>
                    <td colspan="2">
{{--                        <a class="margin-bottom-20 pull-right btn btn-success" href="/dashboard/portfolio/custodials/add/"> <i class="fa fa-plus"></i> Add new</a>--}}
{{--                        <a class="margin-bottom-20 margin-right-10 pull-right btn btn-success" href="/dashboard/portfolio/custodials/exchange_rates/"> <i class="fa fa-eye"></i> View exchange rates</a>--}}
                    </td>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Alias</th>
                    <th>Fund Manager</th>
                    <td>View</td>
                </tr>
                </thead>
                <tbody>
                @foreach($funds as $fund)
                    <tr>
                        <td>{!! $fund['code'] !!}</td>
                        <td>{!! $fund['name'] !!}</td>
                        <td>{!! $fund['short_name'] !!}</td>
                        <td>{!! $fund['fundManager'] !!}</td>
                        <td>
                            <a href="/dashboard/portfolio/unit-fund/details/{!! $fund['id'] !!}" ><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection