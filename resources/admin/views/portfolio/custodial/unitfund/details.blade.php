@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="row">
                <div class="col-md-6">
                    <div class="detail-group">
                        <h4>Fund Details</h4>

                        <table class="table table-hover table-responsive">
                            <thead></thead>
                            <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{!! $fund['name'] !!} </td>
                            </tr>
                            <tr>
                                <td>Short Number</td>
                                <td>{!! $fund['short_name'] !!}</td>
                            </tr>
                            <tr>
                                <td>Fund Manager</td>
                                <td>{!! $fund['fundManager'] !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="detail-group">
                        <h4>Actions</h4>
                        <div class="clearfix"></div>
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addAccount">
                                <i class="fa fa-plus"></i> Link Account
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="detail-group">
                <h4>Receiving Accounts</h4>

                @include('portfolio.custodial.unitfund.partials.receiving-custodials')
            </div>

            <div class="detail-group">
                <h4>Operating Accounts</h4>

                @include('portfolio.custodial.unitfund.partials.operating-custodials')
            </div>
        </div>
    </div>

    <div class="modal fade" id="addAccount" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'link_unit_fund_custodial_account']) !!}

                {!! Form::hidden('unit_fund_id', $fund['code']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Link Account</h4>
                </div>
                <div class="modal-body">
                    <link-fund-account
                            custodials="{{json_encode($custodials)}}"
                            fund="{{json_encode($fund)}}"
                    ></link-fund-account>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Link Account', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@include('portfolio.custodial.unitfund.partials.action-modals')