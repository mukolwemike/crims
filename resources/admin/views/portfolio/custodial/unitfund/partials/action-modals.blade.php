@foreach($receivingAccounts as $custodial)
    <div class="modal fade" id="remove-receiving-account-modal-{{$custodial->id}}" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'remove_unit_fund_custodial_account']) !!}

                {!! Form::hidden('custodial_id', $custodial->id) !!}
                {!! Form::hidden('fund_id', $fund['code']) !!}
                {!! Form::hidden('type', 'receiving') !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Remove Account</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        <p class="text-center">Are you sure you want to remove this Receiving Account?</p>
                    </div>
                    <table class="table table-hover table-responsive">
                        <thead></thead>
                        <tbody>
                        <tr>
                            <td>Fund</td>
                            <td>{!! $fund['name'] !!} </td>
                        </tr>
                        <tr>
                            <td>Account</td>
                            <td>{!! $custodial->account_name !!}</td>
                        </tr>
                        <tr>
                            <td>Reason</td>
                            <td>
                                {!! Form::textarea('reason', '', ['class' => 'form-control', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Remove', ['class'=>'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endforeach

@foreach($operatingAccs as $custodial)
    <div class="modal fade" id="remove-operating-account-modal-{{$custodial->id}}" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'remove_unit_fund_custodial_account']) !!}

                {!! Form::hidden('custodial_id', $custodial->id) !!}
                {!! Form::hidden('fund_id', $fund['code']) !!}
                {!! Form::hidden('type', 'operating') !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Remove Account</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        <p class="text-center">Are you sure you want to remove this Operating Account?</p>
                    </div>
                    <table class="table table-hover table-responsive">
                        <thead></thead>
                        <tbody>
                        <tr>
                            <td>Fund</td>
                            <td>{!! $fund['name'] !!} </td>
                        </tr>
                        <tr>
                            <td>Account</td>
                            <td>{!! $custodial->account_name !!}</td>
                        </tr>
                        <tr>
                            <td>Reason</td>
                            <td>
                                {!! Form::textarea('reason', '', ['class' => 'form-control', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Remove', ['class'=>'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endforeach