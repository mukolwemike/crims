@if(count($operatingAccs))
    <table class="table table-hover table-responsive table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Account Name</th>
            <th>Alias</th>
            <th>Bank Name</th>
            <th>Account Number</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($operatingAccs as $custodial)
            <tr>
                <td>{!! $custodial->id !!}</td>
                <td>{!! $custodial->account_name !!}</td>
                <td>{!! $custodial->alias !!}</td>
                <td>{!! $custodial->bank_name !!}</td>
                <td>{!! $custodial->account_no !!}</td>
                <td>
                    <a href="/dashboard/portfolio/custodials/details/{!! $custodial->id !!}"><i class="fa fa-list-alt"></i></a>
                </td>
                <td>
                    <button data-toggle="modal" class="text-danger"
                            style="background: transparent;border: none;"
                            data-target="#remove-operating-account-modal-{{$custodial->id}}">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-info" style="width: 50%;">
        <p>No Operating Accounts available</p>
    </div>
@endif
