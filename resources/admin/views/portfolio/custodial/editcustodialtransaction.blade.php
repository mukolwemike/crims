@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h4>Edit Custodial Transaction</h4>

            {!! Form::model($transaction, ['route'=>['update_custodial_transaction', $transaction->custodial_account_id]]) !!}
            {!! Form::hidden('transaction_id', $transaction->id) !!}
            {!! Form::hidden('type_name', $transaction->typeName()) !!}
            <div class="form-group">
                {!! Form::label('amount', 'Amount') !!}
                {!! Form::text('amount', abs($transaction->amount), ['class'=>'form-control', 'ng-required'=>"true"]) !!}
            </div>
            <div class="form-group">
                <?php
                $type = $transaction->amount < 0 ? 'credit' : 'debit';
                $narrative = explode(":", $transaction->description)[1];
                ?>
                {!! Form::label('type', 'Type') !!}
                {!! Form::select('type', ['credit'=>'Credit', 'debit'=>'Debit'], $type, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('date', 'Date') !!}
                <div ng-controller="DatepickerCtrl">
                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('narrative', 'Narrative') !!}
                {!! Form::text('narrative', $narrative, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection