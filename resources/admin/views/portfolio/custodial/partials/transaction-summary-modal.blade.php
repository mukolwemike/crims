
<!-- Modal -->
<div class="modal fade" id="transactionSummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-slg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cash Statement: {!! $custodial->account_name !!}</h4>
            </div>
            <div class="modal-body" ng-controller="CustodialAccountTransactionSummaryController" ng-init="account_id = {!! $custodial->id !!}">
                {!! Form::open(['ng-submit'=>'fetchSummary($event)']) !!}

                <div class="row">
                    <div class="col-md-5">
                        {!! Form::label('start', 'Start date') !!}
                        {!! Form::text('start', \Carbon\Carbon::today()->toDateString(), ['class'=>'form-control', 'datepicker-popup init-model'=>'start', 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened']) !!}
                    </div>

                    <div class="col-md-5">
                        {!! Form::label('end', 'End date') !!}
                        {!! Form::text('end', \Carbon\Carbon::today()->toDateString(), ['class'=>'form-control', 'datepicker-popup init-model'=>'end', 'is-open'=>"status.opened_", 'ng-focus'=>'status.opened_ = !status.opened_']) !!}
                    </div>

                    <div class="col-md-2">
                        <button class="btn btn-success margin-top-25">Submit <i class="fa fa-spinner fa-spin" ng-show="loading"></i></button>
                    </div>
                </div>

                {!! Form::close() !!}

                <div class="row">
                    <div class="col-md-6">
                        <h5>Balances</h5>
                        <table class="table table-responsive table-striped">
                            <tbody>
                            <tr ng-repeat="(key, value) in summary.transactions track by $index">
                                <td><% key %></td><td>{!! $custodial->currency->code !!} <% value | currency: "" %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h5>Product Summary</h5>
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr><th>Product</th><th>Cash In</th><th>Cash Out</th></tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="product in summary.products track by $index">
                                    <td><% product.name %></td><td><% product.inflows | currency:"" %></td><td><% product.outflows | currency:"" %></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h5>Transfer Summary</h5>

                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr><th>Account</th><th>Transfer In</th><th>Transfer Out</th><th></th></tr>
                            </thead>
                            <tbody  ng-repeat="account in summary.transfers track by $index">
                            <tr>
                                <td><% account.name %></td><td><% account.in | currency: "" %></td><td><% account.out | currency: "" %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>