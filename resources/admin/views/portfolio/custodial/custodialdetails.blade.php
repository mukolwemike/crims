@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="row">
                <div class="col-md-6">
                    <div class="detail-group">
                        <h3>Account Details</h3>

                        <table class="table table-hover table-responsive">
                            <thead></thead>
                            <tbody>
                                <tr><td>Account Name</td><td>{!! $custodial->account_name !!} </td></tr>
                                <tr><td>Account Number</td><td>{!! $custodial->account_no !!}</td></tr>
                                <tr><td>Bank</td><td>{!! $custodial->bank_name !!}</td></tr>
                                <tr><td>Currency</td><td>{!! $custodial->currency->name !!}</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="detail-group">
                        <h5>Balance</h5>

                        <table class="table table-hover table-responsive">
                            <thead></thead>
                            <tbody>
                            <tr><td>Account Balance</td><td colspan="2"><span class="bold">{!! $custodial->currency->code.' '.\Cytonn\Presenters\AmountPresenter::currency($custodial->balance()) !!} </span></td></tr>
                            @if($custodial->latestBankBalance())
                                <tr>
                                    <td>Bank Balance</td>
                                    <td><span class="bold">{!! $custodial->currency->code.' '.\Cytonn\Presenters\AmountPresenter::currency($custodial->latestBankBalance()->amount) !!} updated {!! \Cytonn\Presenters\DatePresenter::formatDate($custodial->latestBankBalance()->date) !!} </span></td>
                                    <td><a ng-controller="PopoverCtrl" uib-popover="View Account Balance Trail" popover-trigger="mouseenter" href="/dashboard/portfolio/custodials/account-balance-trail/{!! $custodial->id !!}"><i class="fa fa-list-alt"></i> Trail</a></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="detail-group" ng-init=" account_id = {!! $custodial->id !!}" ng-controller="CustodialAccountController">
                <div class="col-md-10 margin-bottom-10">
                    <h3>Transactions</h3>
                </div>

                <div class="col-md-6 pull-left margin-bottom-10 margin-top-10">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#bankBalance">
                            Add Bank Balance
                        </button>

                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#transactionSummary">
                            Cash Statement
                        </button>
                    </div>
                </div>

                <div class="col-md-6 pull-right margin-bottom-10 margin-top-10">
                    <div class="btn-group pull-right" role="group" aria-label="...">
                        <a class="btn btn-success" href="/dashboard/portfolio/custodials/transact/{!!$custodial->id!!}">Transact</a>
                        <!-- Button trigger modal -->
                        <a type="button" href="/dashboard/portfolio/custodials/details/export/excel" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-file-excel-o"></i> Export to Excel
                        </a>
                        <a class="btn btn-success" href="/dashboard/portfolio/suspense_transactions/{!!$custodial->id!!}">Suspense Transactions</a>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="alert alert-info">
                    <p>All values are in {!! $custodial->currency->name !!}</p>
                </div>

                <custodial-transactions :id="{{$custodial->id}}" :types="{{json_encode($transaction_types)}}"></custodial-transactions>
            </div>
            <div class="detail-group">
                <div class="col-md-10 margin-bottom-10">
                    <h3>Link Products/ Projects</h3>
                </div>

                <div class="col-md-6 pull-left margin-bottom-10 margin-top-10">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#link_products_projects">Link Products/ Projects</button>
                    </div>
                </div>
                <table class="table table-hover table-responsive">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($accountProducts)
                        @foreach($accountProducts as $prod)
                            <tr>
                                <td>Products</td>
                                <td>{!! $prod->description !!}</td>
                            </tr>
                        @endforeach
                    @endif
                    @if($unitFunds)
                        @foreach($unitFunds as $fund)
                            <tr>
                                <td>Unit Fund</td>
                                <td>{!! $fund->name !!}</td>
                            </tr>
                        @endforeach
                    @endif
                    @if($shares)
                        @foreach($shares as $share)
                            <tr>
                                <td>Share Entities</td>
                                <td>{!! $share->name !!}</td>
                            </tr>
                        @endforeach
                    @endif
                    @if($reProjects)
                        @foreach($reProjects as $reProj)
                            <tr>
                                <td>Projects</td>
                                <td>{!! $reProj->name !!}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Modal -->
<div class="modal fade" id="bankBalance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['custodial_balance_trail', $custodial->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('date', 'Date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"balance_date", 'is-open'=>"status.opened_date", 'ng-focus'=>'status.opened_date = !status.opened_date', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('amount', 'Enter the balance') !!}

                    {!! Form::text('amount', null, ['class'=>'form-control', 'init-model'=>'amount']) !!}

                    <% amount | currency:"" %>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

    <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" ng-controller="DatepickerCtrl">
        <div class="modal-content">
            {!! Form::open(['route' => 'export_details_to_excel_path']) !!}

            {!! Form::hidden('id', $custodial->id) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Export Transactions to Excel</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('from', 'From') !!}
                    {!! Form::date('from', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"from", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('to', 'To') !!}
                    {!! Form::date('to', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"to", 'is-open'=>"statusSecond.opened", 'ng-focus'=>'openSecond($event)', 'ng-required'=>"true"]) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--link product project --}}

<div class="modal fade" id="link_products_projects" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" ng-controller="DatepickerCtrl">
        <div class="modal-content">
            {!! Form::open(['route' => 'link_product_projects_receiving_accounts']) !!}

            {!! Form::hidden('id', $custodial->id) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Link Product/ Project Receiving Account</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            {!! Form::radio('category', 'product', true, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Product') !!}
                        </div>

                        <div class="col-md-3">
                            {!! Form::radio('category',  'project', null, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Project') !!}
                        </div>

                        <div class="col-md-3">
                            {!! Form::radio('category', 'shares', null, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Shares') !!}
                        </div>

                        <div class="col-md-3">
                            {!! Form::radio('category', 'funds', null, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Unit Funds') !!}
                        </div>
                    </div>
                </div>

                <div class="form-group" ng-if="category == 'product'">
                    {!! Form::label('product_id', 'Products') !!}
                    {!! Form::select('product_id', $products, null, ['class'=>'form-control', 'init-model'=>'product_id', 'ng-required' => 'true', 'placeholder' => 'Select Product']) !!}
                </div>

                <div class="form-group" ng-if="category == 'project'">
                    {!! Form::label('project_id', 'Projects') !!}
                    {!! Form::select('project_id', $projects, null, ['class'=>'form-control', 'init-model'=>'project_id', 'ng-required'=> 'true', 'placeholder' => 'Select Project']) !!}
                </div>

                <div class="form-group" ng-if="category == 'shares'">
                    {!! Form::label('entity_id', 'Share Entities') !!}
                    {!! Form::select('entity_id', $shareentities, null, ['class'=>'form-control', 'init-model'=>'entity_id', 'ng-required'=> 'true', 'placeholder' => 'Select Share Entity']) !!}
                </div>

                <div class="form-group" ng-if="category == 'funds'">
                    {!! Form::label('unit_fund_id', 'Unit Funds') !!}
                    {!! Form::select('unit_fund_id', $funds, null, ['class'=>'form-control', 'init-model'=>'unit_fund_id', 'ng-required'=> 'true', 'placeholder' => 'Select Unit Fund']) !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@include('portfolio.custodial.partials.transaction-summary-modal')