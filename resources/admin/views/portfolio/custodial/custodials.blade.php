@extends('layouts.default')

@section('content')
        <div class="col-md-10 col-md-offset-1">
            <div class="panel-dashboard">
                <table class="table table-hover table-responsive table-striped">
                    <thead>
                        <tr><td colspan="3"></td>
                            <td colspan="2">
                                <a class="margin-bottom-20 pull-right btn btn-success" href="/dashboard/portfolio/custodials/add/"> <i class="fa fa-plus"></i> Add new</a>
                                <a class="margin-bottom-20 margin-right-10 pull-right btn btn-success" href="/dashboard/portfolio/custodials/exchange_rates/"> <i class="fa fa-eye"></i> View exchange rates</a>
                            </td>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>Account name</th>
                            <th>Alias</th>
                            <th>Bank name</th>
                            <th>Account number</th>
                            <th>CDSC Number</th>
                            <th>Currency</th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($custodials as $custodial)
                            <tr>
                                <td>{!! $custodial->id !!}</td>
                                <td>{!! $custodial->account_name !!}</td>
                                <td>{!! $custodial->alias !!}</td>
                                <td>{!! $custodial->bank_name !!}</td>
                                <td>{!! $custodial->account_no !!}</td>
                                <td>{!! $custodial->cdsc_number !!}</td>
                                <td>{!! $custodial->currency->code !!}</td>
                                <td>
                                    <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/portfolio/custodials/details/{!! $custodial->id !!}" ><i class="fa fa-list-alt"></i></a>
                                    <a  ng-controller="PopoverCtrl" uib-popover="Edit details" popover-trigger="mouseenter" href="/dashboard/portfolio/custodials/add/{!! $custodial->id !!}" ><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection