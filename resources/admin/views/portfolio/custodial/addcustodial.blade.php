@extends('layouts.default')

@section('content')
    <div class = "col-md-6 col-md-offset-3">
        <div class = "panel-dashboard">
            <div class = "detail-group">
                {!! Form::model($custodial, ['url'=>'/dashboard/portfolio/custodial/add/'.$custodial->id]) !!}

                <div class = "form-group">
                    {!! Form::label('account_name', 'Account name') !!}

                    {!! Form::text('account_name', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_name') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('alias', 'Alias') !!}

                    {!! Form::text('alias', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'alias') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('bank_id', 'Bank Name') !!}

                    <div class = "input-group">
                        {!! Form::select('bank_id', $banks, null, ['class'=>'form-control'] )!!}
                        <span class = "input-group-btn" data-toggle = "modal" data-target = "#myModal">
                                <button class = "btn btn-default" type = "button">New bank</button>
                        </span>
                    </div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_id') !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class="form-group" ng-controller="SummerNoteController">
                    {!! Form::label('address', 'Bank Address') !!}
                    <summernote  config="options" ng-model="summernote" ng-init="summernote='{!! $custodial->address !!}'"></summernote>
                    {!! Form::text('address', null, ['ng-model'=>'summernote', 'style'=>'display: none;']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('contact_person', 'Bank Contact Person') !!}

                    {!! Form::text('contact_person', null, ['class'=>'form-control', 'init-model'=>'contact_person', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('account_no', 'Account number') !!}

                    {!! Form::text('account_no', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_no') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('cdsc_number', 'CDSC number') !!}

                    {!! Form::text('cdsc_number', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'cdsc_number') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('currency_id', 'Currency') !!}

                    {!! Form::select('currency_id', $currencies, null, ['class'=>'form-control'] )!!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'currency_id') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('fund_manager_id', 'Fund Manager') !!}

                    {!! Form::select('fund_manager_id', $fund_managers, null, ['class'=>'form-control'] )!!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_manager_id') !!}
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    @endsection

            <!-- Modal for adding new Bank -->
    <div class = "modal fade" id = "myModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel">
        <div class = "modal-dialog" role = "document">
            <div class = "modal-content">
                <div class = "modal-body">
                    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                                aria-hidden = "true">&times;</span></button>
                    <h4 class = "modal-title" id = "myModalLabel">Add New Bank</h4>

                    <div class = "form-group">
                        {!! Form::open(['route'=>'new_bank_path']) !!}
                        <div class = "col-md-3">{!! Form::label('name', 'Bank Name') !!}</div>

                        <div class = "col-md-8">{!! Form::text('name', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                        </div>
                    </div>
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>

                </div>
            </div>
        </div>
    </div>