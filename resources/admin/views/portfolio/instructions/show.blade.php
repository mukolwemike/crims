@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Portfolio Instruction Details</h3>
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $instruction->sender->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $instruction->description !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}</td>
                    </tr>
                </tbody>
            </table>

            <div class="col-md-12">
                <div class="btn-group">
                    <button class="btn btn-default" ng-click="link = !link">Generate PDF</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-10 col-md-offset-1" ng-show="link">
        <div class="panel-dashboard">
            <h4>Generate PDF</h4>
            {!! Form::open(['route'=>['generate_portfolio_instruction_pdf', $instruction->id]]) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('first_signatory_id', 'First Signatory') !!}</div>
                        <div class="col-md-8">{!! Form::select('first_signatory_id', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('second_signatory_id', 'Second Signatory') !!}</div>
                        <div class="col-md-8">{!! Form::select('second_signatory_id', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::submit('Generate', ['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection