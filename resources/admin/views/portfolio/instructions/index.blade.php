@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="PortfolioInstructionsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="6"></th>
                        <th>
                            <select st-search="sender" class="form-control">
                                <option value="">All Senders</option>
                                @foreach($custodialaccounts as $account)
                                    <option value="{!! $account->id !!}">{!! $account->account_name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th colspan="2">
                            <select st-search="receiver" class="form-control">
                                <option value="">All Receivers</option>
                                @foreach($custodialaccounts as $account)
                                    <option value="{!! $account->id !!}">{!! $account->account_name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <th st-sort="sender">Sender</th>
                        <th>Bank</th>
                        <th>Acc. Name</th>
                        <th>Acc. No.</th>
                        <th>Currency</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th st-sort="receiver">Receiver</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.sender %></td>
                        <td><% row.bank %></td>
                        <td><% row.account_name %></td>
                        <td><% row.account_number %></td>
                        <td><% row.currency %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><% row.description %></td>
                        <td><% row.date | date %></td>
                        <td><% row.receiver %></td>
                        <td>
                            <a href="/dashboard/portfolio/instructions/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop