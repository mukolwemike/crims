@extends('reports.plain_letterhead_space')

@section('content')
    {!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}<br/><br/>

    {!! $instruction->sender->bank_name !!} <br/>
    {!! $instruction->sender->address !!}<br/>

    <p class="bold-underline">Attn: {!! $instruction->sender->contact_person !!}</p>

    Dear Sir,<br/>

    <p class="bold-underline"> RE:
        {!! $instruction->sender->currency->code !!}
        {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount, false, 0) !!}
        ({!!
            strtoupper((new \Cytonn\Lib\Helper())->convertNumberToWords($instruction->amount, false))
        !!}) CASH TRANSFER.
    </p>
    <p>Kindly transfer {!! $instruction->sender->currency->code !!}
        {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount,  false, 0) !!}
        ({!!
            ucfirst((new \Cytonn\Lib\Helper())->convertNumberToWords($instruction->amount, false))
        !!}) from our {!! $instruction->sender->alias !!} {!! $instruction->sender->account_no !!} in favour of

        {!! $instruction->receiver->account_no !!}
        <br/><br/>

        Bank details are as shown below:<br/><br/>
        <span class="bold">Bank Name</span>: {!! $instruction->receiver->bank_name !!}<br/>
        <span class="bold">Account Name</span>: {!! $instruction->receiver->account_name !!} <br/>
        <span class="bold">Account Number</span>: {!! $instruction->receiver->account_no !!}<br/>
        <br/>
        In case of any query kindly get in touch with us.

        <br/><br/>
        Yours Faithfully,
    </p>

    </br>
    <div class="bold"> For Cytonn Investments Management Ltd</div>
    <br/><br/><br/>

    <span style="display: inline-block; width: 70%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($first_signatory->id) !!}
    </span>

    <span style="display: inline-block;  width: 50%;">
        {!! \Cytonn\Presenters\UserPresenter::presentSignOff($second_signatory->id) !!}
    </span>

    <div style="position: absolute; bottom: 40px; left: 0;">

        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($instruction->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>

        <p class="left">REF: CRIMS/PORTFOLIO/{!! $instruction->id !!}</p>
    </div>
@stop