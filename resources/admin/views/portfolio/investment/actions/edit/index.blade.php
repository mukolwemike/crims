@extends('layouts.default')

@section('content')
    <div>
        <div class="panel-dashboard">
            <div class="col-md-6">
                @include('portfolio.partials.investmentdetails')
            </div>
            <div class="col-md-6">
                <div class="detail-group">
                    <h3>Edit Investment</h3>

                    <div class="" ng-controller="addPortfolioInvestmentCtrl">

                        {!! Form::model($deposit, ['name'=>'form', 'ng-submit'=>'submit($event)']) !!}

                        {{--<div class = "form-group">--}}
                        {{--<div class = "col-md-3">{!! Form::label('custodial_account_id', 'Custodial account') !!}</div>--}}
                        {{--<div class = "col-md-9">{!! Form::select('custodial_account_id', $custodialaccounts, $deposit->repo->getCustodialAccountId(), ['class'=>'form-control','id'=>'custodial_account_id', 'required']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}</div>--}}
                        {{--</div>--}}

                        {{--<div class = "form-group">--}}
                        {{--<div class = "col-md-3">{!! Form::label('type_id', 'Portfolio Investment Type') !!}</div>--}}
                        {{--<div class = "col-md-9">{!! Form::select('type_id', $portfolioinvestmenttype, $deposit->type_id, ['class'=>'form-control','id'=>'type_id', 'required']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type_id') !!}</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}

                        {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                        {{--<div class = "form-group" ng-controller = "DatepickerCtrl">--}}
                        {{--<div class = "col-md-3">{!! Form::label('invested_date', 'Investment date') !!}</div>--}}
                        {{--<div class = "col-md-9">{!! Form::text('invested_date', $deposit->invested_date->toDateString(), ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}</div>--}}
                        {{--</div>--}}

                        <div class="form-group" ng-controller="DatepickerCtrl">
                            <div class="col-md-3">{!! Form::label('maturity_date', 'Maturity date') !!}</div>
                            <div class="col-md-9">{!! Form::text('maturity_date', $deposit->maturity_date->toDateString(), ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}</div>

                            <div class="col-md-3"></div>
                            <div class="col-md-9">{!! Form::checkbox('on_call', true, null, ['id'=>'on_call']) !!} {!! Form::label('on_call', 'On call') !!}</div>
                        </div>

                        <div class="form-group">

                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">{!! Form::label('interest_rate', 'Interest rate') !!}</div>
                            <div class="col-md-9">{!! Form::number('interest_rate', null, ['class'=>'form-control', 'required',  'step'=>0.001, 'init-model'=>'interest_rate']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}</div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">{!! Form::label('tax_rate_id', 'Tax Type') !!}</div>
                            <div class="col-md-9">{!! Form::select('tax_rate_id', $taxes, $deposit->tax_rate_id, ['class'=>'form-control', 'required', 'init-model'=>'tax_rate_id']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tax_rate_id') !!}</div>
                        </div>

                        {{--<div class="form-group">--}}
                        {{--<div class="col-md-3">{!! Form::label('taxable', 'Tax Rate') !!}</div>--}}
                        {{--<div class="col-md-9">{!! Form::select('taxable', [true=>'Taxable', false=>'Zero rated'], null, ['class'=>'form-control', 'init-model'=>'taxable']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}</div>--}}
                        {{--</div>--}}

                        {{--<div class = "form-group">--}}
                        {{--<div class = "col-md-3">{!! Form::label('amount', 'Amount') !!}</div>--}}
                        {{--<div class = "col-md-9">{!! Form::number('amount', null, ['class'=>'form-control', 'step'=>0.01, 'init-model'=>'amount', 'required']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <div class="col-md-3">{!! Form::label('contact_person', 'Contact Person') !!}</div>
                            <div class="col-md-9">{!! Form::text('contact_person', null, ['class'=>'form-control', 'init-model'=>'contact_person', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person') !!}</div>

                        </div>
                        <div class="form-group">
                            <div class="col-md-3">{!! Form::label('description', 'Transaction Description') !!}</div>
                            <div class="col-md-9">{!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>2, 'init-model'=>'description', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}</div>

                        </div>

                        {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}


                        <div ng-view></div>
                        <script type="text/ng-template" id="addPortfolioInvestment.htm">
                            <div class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <div class="modal-header no-bottom-border">
                                            <button type="button" class="close margin-bottom-20" ng-click="close(false)"
                                                    data-dismiss="modal" aria-hidden="true">&times;
                                            </button>
                                            <h4 class="modal-title">Confirm New Portfolio Investment</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div ng-show="overdraft" class="detail-group">
                                                <div class="alert alert-danger">
                                                    <p>This transaction will result in an overdraft of <% balance |
                                                        currency:"" %></p>
                                                </div>

                                                <a class="btn btn-danger btn-sm" href="#" ng-click="allowoverdraft()">Continue</a>
                                                <button type="button" ng-click="close(false)"
                                                        class="btn btn-default btn-sm" data-dismiss="modal">Go Back
                                                </button>
                                            </div>
                                            <div ng-hide="overdraft" class="detail-group">
                                                <p class="danger">Are you sure you want to add a new investment with the
                                                    details below?</p>
                                                <table class="table table-hover table-responsive table-striped">
                                                    <tbody>
                                                    {{--<tr>--}}
                                                    {{--<td>Institution</td>--}}
                                                    {{--<td><% institutionText %></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<td>Fund Type</td>--}}
                                                    {{--<td><% fundTypeText %></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<td>Investment Type</td>--}}
                                                    {{--<td><% investmentTypeText %></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<td>Custodial Account</td>--}}
                                                    {{--<td><% custodialAccountText %></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<td>Investment Date</td>--}}
                                                    {{--<td><% investedDate %></td>--}}
                                                    {{--</tr>--}}
                                                    <tr>
                                                        <td>Maturity Date</td>
                                                        <td><% maturityDate %></td>
                                                    </tr>
                                                    <tr class="success" ng-if="on_call">
                                                        <td>On call</td>
                                                        <td>Yes</td>
                                                    </tr>
                                                    {{--<tr>--}}
                                                    {{--<td> Amount</td>--}}
                                                    {{--<td><% amount | currency:"" %></td>--}}
                                                    {{--</tr>--}}
                                                    <tr>
                                                        <td>Interest Rate</td>
                                                        <td><% interestRate %></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Tax Rate</td>
                                                        <td><% tax_rate_id %></td>
                                                    </tr>
                                                    {{--<tr ng-class="taxable_class">--}}
                                                    {{--<td>Taxable</td>--}}
                                                    {{--<td><% taxable_text %></td>--}}
                                                    {{--</tr>--}}
                                                    <tr>
                                                        <td>Contact Person</td>
                                                        <td><% contactPerson %></td>
                                                    </tr>
                                                    <tr class="alert alert-danger" ng-show="is_overdraft">
                                                        <td colspan="2">Transaction results to an overdraft</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Transaction Description</td>
                                                        <td><% description %></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div ng-hide="overdraft" class="detail-group">
                                                <div class="pull-right">
                                                    {!! Form::open(['route'=>['edit_portfolio_investment', $deposit->id]]) !!}
                                                    <div style="display:none !important;">
                                                        {{--                                                        {!! Form::text('portfolio_investor_id', null, ['init-model'=>' institution']) !!}--}}
                                                        {{--                                                        {!! Form::text('fund_type_id', null,  ['init-model'=>' fundType']) !!}--}}
                                                        {{--                                                        {!! Form::text('type_id', null,  ['init-model'=>' investmentType']) !!}--}}
                                                        {{--                                                        {!! Form::text('custodial_account_id', null, ['init-model'=>' custodialAccount']) !!}--}}
                                                        {{--                                                        {!! Form::text('invested_date', null, ['init-model'=>' investedDate']) !!}--}}
                                                        {!! Form::text('maturity_date', null, ['init-model'=>'maturityDate']) !!}
                                                        {{--                                                        {!! Form::text('amount',  null, ['init-model'=>' amount']) !!}--}}
                                                        {!! Form::text('interest_rate', null, ['init-model'=>' interestRate']) !!}
                                                        {!! Form::text('tax_rate_id', null, ['init-model'=>' tax_rate_id']) !!}
                                                        {{--                                                        {!! Form::text('taxable', null, ['init-model'=>'taxable']) !!}--}}
                                                        {!! Form::text('on_call', null, ['init-model'=>'on_call']) !!}
                                                        {!! Form::text('contact_person', null, ['init-model'=>'contactPerson']) !!}
                                                        {!! Form::text('description', null, ['init-model'=>'description']) !!}
                                                    </div>
                                                    <button type="button" ng-click="close(false)"
                                                            class="btn btn-default"
                                                            data-dismiss="modal">No
                                                    </button>
                                                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection