@extends('layouts.default')

@section('content')
    <div ng-controller="SubAssetClassController" class="col-md-12">
        <div class="panel-dashboard">
            {{--<button class="btn btn-success" data-toggle="modal" data-target="#myModal">Portfolio allocation</button>--}}

            {{--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
                {{--<div class="modal-dialog modal-md" role="document">--}}
                    {{--<div class="modal-content">--}}
                        {{--{!! Form::open(['route'=>['add_portfolio_limit']]) !!}--}}

                        {{--<div class="modal-header">--}}
                            {{--<h4 class="modal-title" id="myModalLabel">Add Portfolio Allocation</h4>--}}
                        {{--</div>--}}
                        {{--<div class="modal-body">--}}
                            {{--<div class="row">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--{!! Form::label('unit_fund_id', 'Unit Fund') !!}--}}
                                        {{--{!! Form::select('fund_id', $unitFunds, null, ['class'=>'form-control', 'id'=>'fund_id', 'init-model'=>'fund_id', 'required']) !!}--}}
                                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_id') !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--{!! Form::label('asset_class_id', 'Asset Class') !!}--}}
                                        {{--{!! Form::select('asset_class_id', $assetClasses, null, ['class'=>'form-control', 'id'=>'asset_class_id', 'init-model'=>'asset_class_id', 'required']) !!}--}}
                                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'asset_class_id') !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--{!! Form::label('sub_asset_class_id', 'Sub Asset Class') !!}--}}
                                        {{--<div  ng-show="asset_class_id">--}}
                                            {{--<select class="form-control" name="sub_asset_class_id">--}}
                                                {{--<option ng-repeat="subAssetClass in subAssetClasses" value="<% subAssetClass.id %>" required><% subAssetClass.name %></option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'branch_id') !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--{!! Form::label('limit', 'Limit') !!}--}}
                                        {{--{!! Form::number('limit', NULL, ['class'=>'form-control']) !!}--}}
                                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'limit') !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                            {{--{!! Form::submit('Save', ['class'=>'btn btn-success']) !!}--}}
                        {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    @foreach($currencies as $currency)
                        <li role="presentation" class=""><a href="#{!! $currency->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $currency->name !!}</a></li>
                    @endforeach
                </ul>
                <div id="myTabContent" class="tab-content">
                    @foreach($currencies as $currency)
                        <div role="tabpanel" class="tab-pane fade" id="{!! $currency->id !!}" aria-labelledby="home-tab">
                            <div ng-init="currency_id = {!! $currency->id !!}" ng-controller="PortfolioSummaryController" data-ng-init="fund_type_id='{!! $type !!}'">
                                <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td colspan="1">
                                                <a class="btn btn-success" href="/dashboard/portfolio/analysis">Deposit Analysis</a>
                                            </td>
                                            <td colspan="1">
                                                <a class="btn btn-primary" href="/dashboard/portfolio/fund-summary">Fund Summary</a>
                                            </td>
                                            <td>
                                                <select st-search="unitFund" class="form-control">
                                                    <option value="">All Unit Funds</option>
                                                    @foreach($unitFunds as $key => $fund)
                                                        <option value="{!! $key !!}">{!! $fund !!}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                {!! Form::select('fund_type_id', $subAssetClasses, null, ['ng-init' => "fund_type_id = '$type'", 'ng-model' => 'fund_type_id', 'class' => 'form-control', 'ng-change' => 'callServer(tableState)'] ) !!}
                                            </td>
                                            <th ng-controller="DatepickerCtrl">
                                                <input type="date" st-search="date" class="form-control" placeholder="As at date..." />
                                            </th>
                                            <th colspan="1">
                                                <select st-search="active" class="form-control">
                                                    <option value="">All Institutions</option>
                                                    <option value="active">Active Institutions</option>
                                                </select>
                                            </th>
                                            <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                                        </tr>
                                        <tr>
                                            <th>Institution</th>
                                            <th>Amount Invested</th>
                                            <th>Gross Interest</th>
                                            <th>Adjusted Gross Interest</th>
                                            <th>Withholding Tax</th>
                                            <th>Net Interest</th>
                                            <th>Interest Repaid</th>
                                            <th>Current Value</th>
                                            <th colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody ng-show="!isLoading">
                                        <tr ng-repeat = "row in displayed">
                                            <td><% row.name %></td>
                                            <td><% row.amount | currency:"" %></td>
                                            <td><% row.gross_interest | currency:"" %></td>
                                            <td><% row.adjusted_gross_interest | currency:"" %></td>
                                            <td><% row.withholding_tax | currency:"" %></td>
                                            <td><% row.net_interest | currency:"" %></td>
                                            <td><%  row.interest_repaid | currency:"" %></td>
                                            <td><% row.value | currency:"" %></td>
                                            <td><a href="/dashboard/portfolio/security/<% row.id %>/details"><i class="fa fa-list-alt"></i></a></td>
                                        </tr>
                                        <tr>
                                            <th colspan="100%" style="text-align: center;color: red; background-color: yellow;">TOTALS</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Cost Value</th>
                                            <th colspan="2">Market Value</th>
                                            <th colspan="2">Adjusted Market Value</th>
                                            <th colspan="2">Weighted Tenor</th>
                                            <th colspan="2">Weighted Rate</th>
                                        </tr>
                                        <tr ng-show="isLoading2"><td colspan="100%">Loading...</td></tr>
                                        <tr ng-show="!isLoading2" ng-cloak="true" ng-if="meta !== {}">
                                            <td colspan="2"><% meta.cost_value | currency:"" %></td>
                                            <td colspan="2"><% meta.market_value | currency:"" %></td>
                                            <td colspan="2"><% meta.adjusted_market_value | currency:"" %></td>
                                            <td colspan="2"><% meta.weighted_tenor | currency:"" %></td>
                                            <td colspan="2"><% meta.weighted_rate | currency:"" %></td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-show="isLoading">
                                        <tr><td colspan="100%">Loading...</td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan = "4" class = "text-center">
                                                Items per page
                                            </td>
                                            <td colspan = "3" class = "text-center">
                                                <input type = "text" ng-model = "itemsByPage"/>
                                            </td>
                                            <td colspan = "3" class = "text-center">
                                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                            </td>
                                            <td class="text-center" st-pagination="" st-items-by-page="10" colspan="6"></td>
                                        </tr>
                                    </tfoot>
                                </table>

                                {{--<h4>Totals</h4>--}}

                                {{--<table class="table table-striped table-hover table-responsive">--}}
                                    {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<th>Cost Value</th><th>Market Value</th><th>Weighted Tenor</th><th>Weighted Rate</th>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($analytics($currency)->costValue()) !!}</td>--}}
                                            {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($analytics($currency)->marketValue()) !!}</td>--}}
                                            {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($analytics($currency)->weightedTenor()) !!}</td>--}}
                                            {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($analytics($currency)->weightedRate()) !!}%</td>--}}
                                        {{--</tr>--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection