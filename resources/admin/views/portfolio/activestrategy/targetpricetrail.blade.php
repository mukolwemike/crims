@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="row">
            <div class="col-md-6">
                <div class="detail-group">
                    <h4>{!! $security->name !!}</h4>

                    <table class="table table-responsive table-striped table-hover">
                        <thead>
                        <tr><th>Date</th><th>Price</th></tr>
                        </thead>
                        <tbody>
                        @foreach($priceTrail as $price)
                            <tr><td>{!! $price->date !!}</td><td>{!! $price->price !!}</td></tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! $priceTrail->links() !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="detail-group">
                    <h4>Set a price at a date</h4>

                    {!! Form::open(['route'=>['set_target_price', $security->id]]) !!}

                    <div class="form-group" ng-controller = "DatepickerCtrl">
                        {!! Form::label('date', 'Date') !!}

                        {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price at the date') !!}

                        {!! Form::number('price', null, ['class'=>'form-control', 'step'=>0.001, 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'price') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection