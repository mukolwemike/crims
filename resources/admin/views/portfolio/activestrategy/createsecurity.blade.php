@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div>
                {!! Form::open(['route'=>'create_active_strategy_security']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Name of Security') !!}

                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection