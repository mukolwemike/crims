@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div ng-controller="ActivestrategyGridCtrl">
                <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>
                                <div class="btn-group">
                                    <a href="/dashboard/portfolio/activestrategy/security/create" class="btn btn-success margin-bottom-20">
                                        <i class="fa fa-plus"></i> Add new
                                    </a>

                                    {{--<a href="/dashboard/portfolio/investments/maturity" class="btn btn-success margin-bottom-20">--}}
                                        {{--Maturity--}}
                                    {{--</a>--}}

                                    <a href="/dashboard/portfolio/summary" class="btn btn-success margin-bottom-20">
                                        <i class="fa fa-list-alt"></i> Summary
                                    </a>
                                </div>
                                {{--<div class="btn-group">--}}
                                    {{--<a href="/dashboard/portfolio/activestrategy/security/create" class="btn btn-success margin-bottom-20">Create Security</a>--}}
                                    {{--<a href="/dashboard/portfolio/security/create" class="btn btn-success margin-bottom-20">Add New Security</a>--}}
                                    {{--<a href="/dashboard/portfolio/asset-classes" class="btn btn-primary margin-bottom-20">Asset Classes</a>--}}
                                {{--</div>--}}
                            </th>
                            <th colspan = "10"></th>
                            <th colspan = "3"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                        </tr>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Total Cost</th>
                        <th>Number of Shares</th>
                        <th>Market Value</th>
                        <th>Market Price</th>
                        <th>Target Price</th>
                        <th>Alpha</th>
                        <th>Portfolio Cost</th>
                        <th>Portfolio Value</th>
                        <th>Dividend</th>
                        <th>Portfolio Return</th>
                        <th>Exposure</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td><% row.name %></td>
                            <td><% row.cost | currency:"" %></td>
                            <td><% row.total_cost | currency:"" %></td>
                            <td><% row.total_number | currency:"":0 %></td>
                            <td><% row.market_value | currency:"" %></td>
                            <td><% row.market_price | currency:"" %></td>
                            <td><% row.target_price | currency:"" %></td>
                            <td><% row.alpha | currency:"" %></td>
                            <td><% row.portfolio_cost | currency:"" %></td>
                            <td><% row.portfolio_value | currency:"" %></td>
                            <td><% row.dividend | currency:"" %></td>
                            <td><% row.portfolio_return | currency:"" %></td>
                            <td><% row.exposure | currency:"" %>%</td>
                            <td>
                                <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/portfolio/activestrategy/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr><td colspan="100%">Loading...</td></tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Total</th>
                        <td colspan="2"></td>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($valuation->numberOfShares(), false, 0) !!}</th>
                        <td colspan="4"></td>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($valuation->portfolioCost()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($valuation->portfolioValue()) !!}</th>
                        <td></td>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($valuation->portfolioReturn()) !!}</th>
                        <th>100%</th>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan = "3" class = "text-center">
                            Items per page
                        </td>
                        <td colspan = "3" class = "text-center">
                            <input type = "text" ng-model = "itemsByPage"/>
                        </td>
                        <td colspan = "4" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div ng-controller="CollapseCtrl">
                <div>
                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">View valuation charts</button>
                </div>

                <div class="clearfix"></div>

                <hr>
                <div uib-collapse="isCollapsed">
                    <div class="row" ng-controller="ActiveStrategyValuationChartsCtrl">
                        <div class="col-md-6">
                            <div class="detail-group">
                                <h4>Asset allocation</h4>

                                <canvas id="allocationPieChart" class="chart chart-pie"
                                        chart-data="d"
                                        chart-labels="l">
                                </canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="detail-group">
                                <h4>Portfolio return for the week</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection