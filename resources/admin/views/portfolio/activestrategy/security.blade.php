@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div ng-controller="ActiveStrategyHoldingsCtrl">

            <table ng-init="security_id = {!! $security->id !!}" st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <th colspan="80%">
                            <div class="btn-group">
                                <a href="" class="btn btn-success margin-bottom-20" data-toggle="modal" data-target="#buyModal">Buy Shares</a>
                                <a href="" class="btn btn-danger margin-bottom-20" data-toggle="modal" data-target="#sellModal">Sell Shares</a>
                                <a href="" class="btn btn-info margin-bottom-20" data-toggle="modal" data-target="#dividendModal">Accrue dividends</a>
                                <a href="/dashboard/portfolio/activestrategy/{!! $security->id !!}/marketprice" class="btn btn-warning margin-bottom-20">Market Price</a>
                                <a href="/dashboard/portfolio/activestrategy/{!! $security->id !!}/targetprice" class="btn btn-primary margin-bottom-20">Target Price</a>
                            </div>
                        </th>
                        <th colspan="20%">
                            <a href="{{ route('dividends.index', $security->id) }}" class="btn btn-default margin-bottom-20">View Dividends</a>
                        </th>
                    </tr>
                    <tr><td colspan="100%"><h4>Shares bought</h4></td> </tr>
                    <tr>
                        <th>Purchase Date</th>
                        <th>Number of Shares</th>
                        <th>Purchase Cost</th>
                        <th>Purchase Value</th>
                        <th>Target Price</th>
                        <th>Total Purchase Cost</th>
                        <th>Total Purchase Value</th>
                        <th>Market Price</th>
                        <th>Market Value</th>
                        <th>Gain/Loss</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed">
                        <td><%  row.purchase_date | date %></td>
                        <td><% row.number | currency:"":0 %></td>
                        <td><% row.cost | currency:"" %></td>
                        <td><% row.value | currency:"" %></td>
                        <td><% row.target_price | currency:"" %></td>
                        <td><% row.total_cost | currency:"" %></td>
                        <td><% row.total_value | currency:"" %></td>
                        <td><% row.market_price | currency:"" %></td>
                        <td><% row.market_value | currency:"" %></td>
                        <td><% row.gain_loss | currency:"" %></td>
                        <td><a href="/dashboard/portfolio/activestrategy/holdings/update/<% row.id %>"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr><td colspan="100%">Loading...</td></tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total Bought</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->numberOfSharesBought(), false, 0) !!}</th>
                        <td colspan="4"></td>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalPurchaseValueHeld()) !!}</th>
                        <td></td>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalMarketValueHeld()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalGainLoss()) !!}</th>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan = "100%" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>

                    </tr>
                </tfoot>
            </table>
        </div>

        <h4>Shares sold</h4>
        <div ng-controller="ActiveStrategyShareSaleCtrl">

            <table ng-init="security_id = {!! $security->id !!}" st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th>Sale Date</th>
                    <th>Number of Shares</th>
                    <th>Sale Price</th>
                    <th>Average Purchase Price</th>
                    <th>Target Price</th>
                    <th>Total Purchase Value</th>
                    <th>Total Sale Value</th>
                    <th>Market Price</th>
                    <th>Market Value</th>
                    <th>Gain/Loss</th>
                    <th></th>
                </tr>
                </thead>
                <tbody ng-show="!isLoading">
                <tr ng-repeat = "row in displayed">
                    <td><%  row.date | date %></td>
                    <td><% row.number | currency:"":0 %></td>
                    <td><% row.sale_price | currency:"" %></td>
                    <td><% row.average_purchase_value | currency:"" %></td>
                    <td><% row.target_price | currency:"" %></td>
                    <td><% row.total_purchase_value | currency:"" %></td>
                    <td><% row.sale_value | currency:"" %></td>
                    <td><% row.market_price | currency:"" %></td>
                    <td><% row.market_value | currency:"" %></td>
                    <td><% row.gain_loss | currency:"" %></td>
                    <td><a href="/dashboard/portfolio/activestrategy/shares/sold/update/<% row.id %>"><i class="fa fa-pencil-square-o"></i></a></td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr><td colspan="100%">Loading...</td></tr>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total Sold</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->numberOfSharesSold(), false, 0) !!}</th>
                    <td colspan="4"></td>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalSalesValue()) !!}</th>
                    <td></td>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalMarketValueOfSoldShares()) !!}</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalGailLossInSales()) !!}</th>
                    <td></td>
                </tr>
                <tr>
                    <td colspan = "100%" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>

        <div>
            <h4>Valuation</h4>
            <table class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Cost</th>
                        <th>Total Cost</th>
                        <th>Number of Shares</th>
                        <th>Market Price</th>
                        <th>Market Value</th>
                        <th>Target Price</th>
                        <th>Alpha</th>
                        <th>Portfolio Cost</th>
                        <th>Portfolio Value</th>
                        <th>Dividend</th>
                        <th>Portfolio Return</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->averagePrice()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->currentTotalCost()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->numberOfSharesHeld(), false, 0) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->marketPrice()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalCurrentMarketValue()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->targetPrice()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->alpha()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->totalCurrentPurchaseValue()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->currentPortfolioValue()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->dividend()) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($security->repo->currentGainLoss()) !!}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

<!-- Modal -->
<div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['buy_active_strategy_shares', $security->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Buy Shares</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    {!! Form::label('number', 'Number of shares') !!}

                    {!! Form::number('number', null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Date of Purchase') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('cost', 'Purchase cost per share') !!}

                    {!! Form::number('cost', null, ['class'=>'form-control', 'step'=>0.001]) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="dividendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['save_dividends', $security->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Scrip Bonus Dividend</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Payment date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('book_closure_date', 'Book closure date') !!}

                    {!! Form::text('book_closure_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('dividend', 'Dividend per share (before Tax)') !!}

                    {!! Form::number('dividend', null, ['class'=>'form-control', 'step'=>0.001]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('dividend_type', 'Dividend Type') !!}

                    {!! Form::select('dividend_type', ['cash'=>'Cash', 'shares'=>'Shares'], null, ['class'=>'form-control', 'required', 'init-model'=>'dividend_type']) !!}
                </div>

                <div class="form-group" ng-show="dividend_type == 'shares'">
                    {!! Form::label('method_of_calculation', 'Method of Calculation') !!}

                    {!! Form::select('method_of_calculation', [null => 'Select method','ratio'=>'Ratio', 'conversion'=>'Conversion Price'], null, ['class'=>'form-control', 'ng-model'=>'method_of_calculation']) !!}
                </div>

                <div class="form-group" ng-show="method_of_calculation == 'conversion' || dividend_type == 'cash'">
                    {!! Form::label('conversion_price', 'Conversion Price per share') !!}

                    {!! Form::number('conversion_price', null, ['class'=>'form-control', 'step'=>0.001]) !!}
                </div>

                <div class="form-group" ng-show="method_of_calculation == 'ratio' && dividend_type == 'shares'">
                    {!! Form::label('old_shares', 'Old Shares') !!}

                    {!! Form::number('old_shares', null, ['class'=>'form-control', 'step'=>1]) !!}
                </div>

                <div class="form-group" ng-show="method_of_calculation == 'ratio' && dividend_type == 'shares'">
                    {!! Form::label('new_shares', 'New Shares') !!}

                    {!! Form::number('new_shares', null, ['class'=>'form-control', 'step'=>1]) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="sellModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['sell_active_strategy_shares', $security->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sell Shares</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Sale date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'Market price at sale date') !!}

                    {!! Form::number('price', null, ['class'=>'form-control', 'step'=>0.001]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number', 'Number of shares sold') !!}

                    {!! Form::number('number', null, ['class'=>'form-control', 'step'=>0.001]) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>