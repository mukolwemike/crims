@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">

            <div>
                {!! Form::model($share_sale, ['route'=>['update_sold_active_strategy_shares', $share_sale->id], 'method'=>'PUT']) !!}
                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Sell Date') !!}
                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'Market Price at Sale Date') !!}
                    {!! Form::text('price', $share_sale->sale_price, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number', 'Number of Shares Sold') !!}
                    {!! Form::number('number', null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection