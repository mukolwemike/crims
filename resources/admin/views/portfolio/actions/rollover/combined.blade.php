@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard" ng-controller = "portfolioRolloverCtrl">
            <h1>Rollover Investment</h1>
            @include('portfolio.partials.investmentdetails')
            {!! Form::open(['url'=>'/dashboard/portfolio/performcombinedrollover/'.$deposit->id]) !!}
                <div class = "detail-group">
                    <table class="table table-hover table-responsive table-striped">
                        <thead>
                            <tr><td></td><th>Amount</th><th>Value Date</th><th>Maturity Date</th><th>Value</th></tr>
                        </thead>

                            <tbody>
                                @foreach($same_day_investments as $investment)
                                    <tr>
                                        <td>{!! Form::checkbox($investment->id, true) !!} </td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date)) !!}</td>
                                    </tr>
                                @endforeach
                                <tr><td colspan="100%">{!! Form::submit('Rollover', ['class'=>'btn btn-success']) !!}</td></tr>
                            </tbody>
                    </table>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@stop