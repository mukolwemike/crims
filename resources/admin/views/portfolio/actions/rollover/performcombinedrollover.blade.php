@extends('layouts.default')

@section('content')
    <div class = "col-md-8 col-md-offset-2">
        <div class = "panel-dashboard" ng-controller = "PortfolioCombinedRolloverCtrl">
            <h1>Rollover Investment</h1>

            <div class="detail-group">
                <table class="table table-hover table-responsive">
                    <thead></thead>
                    <tbody>
                        <tr><td>Institution</td><td>{!! $investment->security->investor->name !!}</td></tr>
                    </tbody>
                </table>
            </div>

            <div class="detail-group">
                <table class="table table-hover table-responsive table-striped">
                    <thead>
                    <tr><th>Value Date</th><th>Maturity Date</th><th>Amount</th><th>Value</th></tr>
                    </thead>
                    <tbody>
                    @foreach($same_day_investments as $investment)
                        <tr>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date)) !!}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <th>Total</th>
                            <td></td>
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($same_day_investments->sum('amount')) !!}</th>
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($same_day_investments->sum('value')) !!}</th>
                        </tr>
                    </tbody>
                    {!! Form::close() !!}
                </table>
            </div>

            <div class="detail-group" ng-init="total_value={!! $same_day_investments->sum('value') !!}">

                <div class="hide">
                    {!! Form::text('investments', json_encode($same_day_investments->lists('id')), ['init-model'=>'investments'] ) !!}
                </div>

                {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}

                {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('invested_date', 'Enter the date Investment resumes') !!}

                    {!! Form::date('invested_date', null, ['class'=>'form-control', 'id'=>'invested_date', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                </div>

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('maturity_date', 'Enter the new maturity date') !!}

                    {!! Form::date('maturity_date', null, ['class'=>'form-control', 'id'=>'maturity_date', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                </div>

                <div class="form-group">
                    {!! Form::radio('reinvest', 'reinvest', true, ['init-model'=>'reinvest']) !!} {!! Form::label('reinvest', 'Reinvest Amount') !!} <br/>
                    {!! Form::radio('reinvest', 'withdraw', false, ['init-model'=>'reinvest']) !!} {!! Form::label('reinvest', 'Withdraw Amount') !!} <br/>
                    {!! Form::radio('reinvest', 'topup', false, ['init-model'=>'reinvest']) !!} {!! Form::label('reinvest', 'Topup Amount') !!} <br/>
                </div>

                <div class = "form-group">
                    {!! Form::label('amount', 'Enter the amount') !!}

                    {!! Form::number('amount', null, ['step'=>'0.01', 'class'=>'form-control','init-model'=>'amount']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}

                    {!! Form::number('interest_rate', null, ['class'=>'form-control','init-model'=>'interest_rate', 'step'=>'0.0001']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('taxable', 'Tax Rate') !!}

                    {!! Form::select('taxable', [true=>'Taxable', false=>'Zero rated'], null, ['class'=>'form-control', 'init-model'=>'taxable']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('description', 'Transaction Description') !!}

                    {!! Form::textarea('description', null, ['class'=>'form-control','init-model'=>'description', 'rows'=>2]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                </div>

                {!! Form::submit('Rollover', ['class'=>'btn btn-success']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type = "text/ng-template" id = "rollover.htm">
        <div class = "modal fade">
            <div class = "modal-dialog">
                <div class = "modal-content">
                    <div class = "modal-header no-bottom-border">
                        <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                data-dismiss = "modal" aria-hidden = "true">&times;</button>
                        <h4 class = "modal-title">Confirm Investment Rollover</h4>
                    </div>
                    <div class = "modal-body">

                        <div class = "detail-group">
                            <p>Are you sure you want to rollover this investment with the details below?</p>
                            <table class = "table table-hover table-responsive table-striped">
                                <tbody>
                                <tr>
                                    <td>New investment date</td>
                                    <td><% invested_date | date %></td>
                                </tr>
                                <tr>
                                    <td>New maturity date</td>
                                    <td><% maturity_date | date %></td>
                                </tr>
                                <tr>
                                    <td>Total from combined Investments</td>
                                    <td><% total_value | currency:"" %></td>
                                </tr>
                                <tr class = "success" ng-if="topup">
                                    <td>Topup</td>
                                    <td><% amount | currency:"" %></td>
                                </tr>
                                <tr class="danger">
                                    <td>Total that will be withdrawn</td>
                                    <td><% withdraw_val | currency:"" %></td>
                                </tr>
                                <tr class = "success">
                                    <td>Total that will be reinvested</td>
                                    <td><% reinvest_val | currency:"" %></td>
                                </tr>
                                <tr>
                                    <td>Interest Rate</td>
                                    <td><% interest_rate %>%</td>
                                </tr>
                                <tr>
                                    <td>Transaction Description</td>
                                    <td><% description %>%</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "detail-group">
                            <div class = "pull-right">
                                {!! Form::open(['route'=>['save_combined_portfolio_rollover', $investment->id]]) !!}
                                <div style = "display:none !important;">
                                    {!! Form::text('investment', Crypt::encrypt($investment->id)) !!}
                                    {!! Form::text('investments', null, ['init-model'=>'investments']) !!}
                                    {!! Form::text('reinvest', NULL, ['init-model'=>'reinvest']) !!}
                                    {!! Form::text('invested_date', NULL, ['init-model'=>'invested_date']) !!}
                                    {!! Form::text('maturity_date', NULL, ['init-model'=>'maturity_date']) !!}
                                    {!! Form::text('interest_rate', NULL, ['init-model'=>'interest_rate']) !!}
                                    {!! Form::text('amount', NULL, ['init-model'=>'amount']) !!}
                                    {!! Form::text('taxable', NULL, ['init-model'=>'taxable']) !!}
                                    {!! Form::text('description', NULL, ['init-model'=>'description']) !!}
                                </div>
                                <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                        data-dismiss = "modal">No
                                </button>
                                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
@stop


