@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            <div class="detail-group">
                <h4>Repayments</h4>

                <div class="row scrollable">
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Narrative</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($all_repayments as $repayment)
                            <tr>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($repayment->date ) !!}</td>
                                <td>{!! $repayment->narrative !!}</td>
                                <td>{!! $repayment->type->name !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($repayment->amount) !!}</td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#delete{!! $repayment->id !!}"><i class="fa fa-rotate-left alert-danger"></i></a>

                                    <!-- Modal -->
                                    <div class="modal fade" id="delete{!! $repayment->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {!! Form::open(['route'=>['portfolio_repayment_delete', $repayment->id]]) !!}

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Reverse repayment</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure you want to reverse the repayment? <br>

                                                    <label for="">Date:</label> {!! $repayment->date->toFormattedDateString() !!} <br>
                                                    <label for="">Amount:</label> {!! \Cytonn\Presenters\AmountPresenter::currency($repayment->amount) !!}
                                                    <br>
                                                    <label for="">Description:</label> {!! $repayment->narrative !!} <br><br>

                                                    <div class="form-group">
                                                        {!! Form::label('reason', 'Give a reason') !!}
                                                        {!! Form::text('reason', null, ['class'=>'form-control']) !!}
                                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    {!! Form::submit('Yes, Reverse', ['class'=>'btn btn-danger']) !!}
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        @if($all_repayments->count() != 0)
                            <tr><th></th><th>Total</th><td></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($all_repayments->sum('amount')) !!}</th><td></td></tr>
                        @else
                            <tr><td colspan="100%">No repayments made so far</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="margin-top-40 visible-md-block visible-lg-block"></div>
                <h4 class="inline-block pull-left">Loan Position as at {!! $date_eff->toFormattedDateString() !!}</h4>


                <div class="row">

                    {!! Form::open(['method'=>'GET', 'class'=>'form-inline pull-right']) !!}

                    {!! Form::text('date_eff', $date_eff->toDateString(), ['class'=>'form-control', 'dmc-datepicker', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>'date_eff']) !!}

                    {!! Form::submit('Change', ['class'=>'btn btn-success margin-bottom-20']) !!}
                    {!! Form::close() !!}

                    <?php $total_int = 0; ?>
                    @if($date_eff->copy()->gt(new \Carbon\Carbon($deposit->maturity_date)))
                        <div class="clearfix"></div>
                        <div class="alert alert-warning">
                            <p>Interest calculated up to maturity on {!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</p>
                        </div>
                    @endif
                </div>
                <div class="row scrollable">
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Net Interest</th>
                            <th>Action</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Principal</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date) !!}</td>
                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}</td>
                            <td align="right"> - </td>
                            <td align="right"> - </td>
                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}</td>
                            <td></td>
                        </tr>
                        @foreach($prepared->actions as $action)
                            <tr>
                                <td>{{ $action->description }}</td>
                                <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($action->date) }}</td>
                                <td> - </td>
                                <td>{{ \Cytonn\Presenters\AmountPresenter::accounting($action->net_interest) }}</td>
                                <td>{{ \Cytonn\Presenters\AmountPresenter::accounting($action->amount) }}</td>
                                <td>{{ \Cytonn\Presenters\AmountPresenter::accounting($action->total) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th colspan="3">Grand Total</th>
                            <th align="right"><strong>{{ \Cytonn\Presenters\AmountPresenter::accounting($prepared->total->net_interest) }}</strong></th>
                            <th><strong> - </strong></th>
                            <td align="right"><strong>{!! \Cytonn\Presenters\AmountPresenter::accounting($prepared->total->total)  !!}</strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="detail-group" ng-controller="PortfolioRepaymentCtrl"
                ng-init="maturityDate = '{!! \Carbon\Carbon::parse($deposit->maturity_date)->toDateString() !!}'">
                <h4>Add a payment</h4>

                {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}

                {!! Form::hidden('total_amount', $prepared->total->total, ['class'=>'form-control', 'init-model'=>'total_amount', 'ng-model' => 'total_amount']) !!}

                <div class="form-group">
                    {!! Form::label('payment_date_type', 'Date of Payment') !!}
                    {!! Form::select('payment_date_type', [ 0 => 'Before/After Maturity', 1 => 'On Maturity'], '0', ['ng-model' => 'payment_date_type','required', 'class'=>'form-control', 'id'=>'payment_date_type']) !!}
                </div>

                <div ng-show="payment_date_type == 0" class="form-group">
                    {!! Form::label('date', 'Repayment Date') !!}
                    {!! Form::text('date', null, ['class'=>'form-control', 'dmc-datepicker', 'ng-model' => 'date', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'id'=>'date']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('payment_amount_type', 'Amount of Payment') !!}
                    {!! Form::select('payment_amount_type', [0 => 'Partial Repayment',  1 => 'Full Repayment'], '0', ['ng-model' => 'payment_amount_type','required', 'class'=>'form-control', 'id'=>'payment_amount_type']) !!}
                </div>

                <div ng-show="payment_amount_type == 0" class="form-group">
                    {!! Form::label('amount', 'Repayment Amount') !!}
                    {!! Form::text('amount', null, ['class'=>'form-control', 'init-model'=>'amount', 'ng-model' => 'amount']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                </div>

                <div ng-show="payment_amount_type == 0" class="form-group">
                    {!! Form::label('type_id', 'Payment Type') !!}
                    {!! Form::select('type_id', $repaymenttypes, null, ['class'=>'form-control', 'id'=>'payment_type', 'ng-model' => 'payment_type']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('narrative', 'Narration') !!}
                    {!! Form::text('narrative', null, ['class'=>'form-control', 'init-model'=>'narrative']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

                {!! Form::close() !!}
            </div>

            <div ng-view></div>
            <script type = "text/ng-template" id = "portfoliorepay.htm">
                <div class = "modal fade">
                    <div class = "modal-dialog">
                        <div class = "modal-content">

                            <div class = "modal-header no-bottom-border">
                                <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                        data-dismiss = "modal" aria-hidden = "true">&times;</button>
                                <h4 class = "modal-title">Add a portfolio deposit repayment</h4>
                            </div>
                            <div class = "modal-body">

                                <div class = "detail-group">
                                    <p>Are you sure you want to add the repayment with the details below?</p>
                                    <table class = "table table-hover table-responsive table-striped">
                                        <tbody>
                                            <tr>
                                                <td>Institution</td>
                                                <td>{!! $deposit->security->investor->name !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Payment Date Type</td>
                                                <td><% payment_date_type %></td>
                                            </tr>
                                            <tr>
                                                <td>Date of payment</td>
                                                <td><% date %></td>
                                            </tr>
                                            <tr>
                                                <td>Payment Type</td>
                                                <td><% payment_type %></td>
                                            </tr>
                                            <tr>
                                                <td>Payment Amount Type</td>
                                                <td><% payment_amount_type %></td>
                                            </tr>
                                            <tr ng-if="payment_amount_type_id != 1">
                                                <td>Amount</td>
                                                <td><% amount | currency:"" %></td>
                                            </tr>
                                            <tr ng-if="payment_amount_type_id != 1">
                                                <td>Amount Before Repayment</td>
                                                <td><% total_amount | currency:"" %></td>
                                            </tr>
                                            <tr ng-if="payment_amount_type_id != 1">
                                                <td>Amount After Repayment</td>
                                                <td><% (total_amount - amount) | currency:"" %></td>
                                            </tr>
                                            <tr>
                                                <td>Narration</td>
                                                <td><% narrative %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class = "detail-group">
                                    <div class = "pull-right">
                                        {!! Form::open(['route'=>['portfolio_repay', $deposit->id]]) !!}
                                            <div style = "display: none !important;">
                                                {!! Form::text('narrative', NULL, ['ng-model'=>'narrative']) !!}
                                                {!! Form::text('amount', null, ['ng-model'=>'amount']) !!}
                                                {!! Form::text('date', null, ['ng-model'=>'date']) !!}
                                                {!! Form::text('payment_type', null, ['ng-model'=>'payment_type_id']) !!}
                                                {!! Form::text('payment_amount_type_id', null, ['ng-model'=>'payment_amount_type_id']) !!}
                                                {!! Form::text('payment_date_type_id', null, ['ng-model'=>'payment_date_type_id']) !!}
                                            </div>
                                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                                    data-dismiss = "modal">No
                                            </button>
                                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </script>
        </div>
    </div>
@endsection