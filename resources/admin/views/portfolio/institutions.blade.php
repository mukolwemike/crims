@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">

        <div ng-controller="InstitutionsGridCtrl">
            <table st-table="displayedCollection" st-safe-src="rowCollection" class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan="1"><a class="margin-bottom-20 btn btn-success" href="/dashboard/portfolio/institutions/add"> <i class="fa fa-plus"></i> Add new</a></th>
                    <th colspan="2"></th>
                    <th colspan="1"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                </tr>
                <tr>
                    <th st-sort="id">ID</th>
                    <th st-sort="code">Code</th>
                    <th st-sort="name">Name</th>
                    <th>Details</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in displayedCollection">
                    <td> <% row.id %></td>
                    <td><% row.code %></td>
                    <td><% row.name %></td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="View institution details" popover-trigger="mouseenter" href="/dashboard/portfolio/institutions/details/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                    </td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="Edit institution details" popover-trigger="mouseenter" href="/dashboard/portfolio/institutions/add/<%row.id%>"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>

                </tbody>
                <tfoot>
                <tr>
                    <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                </tr>
                </tfoot>
            </table>
        </div>

    </div>
@stop