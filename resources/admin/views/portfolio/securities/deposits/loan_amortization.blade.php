@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <loan-amortization :security="{{ $security }}" :holding="{{ $holding }}"></loan-amortization>
        </div>
    </div>
@endsection