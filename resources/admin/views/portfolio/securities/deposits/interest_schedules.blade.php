@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <deposit-holdings-interest-schedules :security-id="{!! $holding->portfolio_security_id !!}" :holding-id="{!! $holding->id !!}"></deposit-holdings-interest-schedules>
        </div>
    </div>
@endsection