@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Suspense Transaction Details</h3>
            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Custodial Account</td>
                    <td>{!! $transaction->custodialAccount->account_name !!}</td>
                </tr>
                <tr>
                    <td>Account Number</td>
                    <td>{!! $transaction->account_no !!}</td>
                </tr>
                <tr>
                    <td>Currency</td>
                    <td>{!! $transaction->custodialAccount->currency->code !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::accounting($transaction->amount) !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
                </tr>
                <tr>
                    <td>Reference</td>
                    <td>{!! $transaction->reference !!}</td>
                </tr>
                <tr>
                    <td>Bank Reference</td>
                    <td>{!! $transaction->bank_reference !!}</td>
                </tr>
                <tr>
                    <td>Transaction Id</td>
                    <td>{!! $transaction->transaction_id !!}</td>
                </tr>
                <tr>
                    <td>Outgoing Reference</td>
                    <td>{!! $transaction->outgoing_reference !!}</td>
                </tr>
                <tr>
                    <td>Source Identity</td>
                    <td>{!! $transaction->source_identity !!}</td>
                </tr>
                <tr>
                    <td>Matched</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($transaction->matched) !!}</td>
                </tr>
                </tbody>
            </table>

            @if($transaction->repo->hasApproval() && $transaction->matched != 1)
                <div class="alert alert-info">
                    <p>The suspense transaction already has already been processed for approval</p>
                </div>
                @if($transaction->clientApproval)
                    <a class="btn btn-success margin-bottom-10"
                       href="/dashboard/investments/approve/{!! $transaction->client_transaction_approval_id !!}">View
                        Approval</a>
                @endif
                @if($transaction->approval)
                    <a class="btn btn-success margin-bottom-10"
                       href="/dashboard/portfolio/approve/{!! $transaction->approval_id !!}">View Approval</a>
                @endif
            @else
                @if($transaction->matched != 1)
                    <div>
                        @if($transaction->amount > 0)
                            <button class="btn btn-info" data-toggle="modal" data-target="#saveDepositModal">Record
                                Deposit
                            </button>
                            <suspense-transaction-add-inflow :transaction-id="{{ $transaction->id }}"></suspense-transaction-add-inflow>
                        @else
                            <button class="btn btn-info" data-toggle="modal" data-target="#recordFeeModal">Record Fee
                            </button>
                            <suspense-transaction-bounce-cheque :transaction-id="{{ $transaction->id }}">
                            </suspense-transaction-bounce-cheque>
                            <suspense-transaction-match-custodial-transaction :transaction-id="{{ $transaction->id }}">
                            </suspense-transaction-match-custodial-transaction>
                        @endif
                        <suspense-transaction-match-suspense-transaction :transaction-id="{{ $transaction->id }}">
                        </suspense-transaction-match-suspense-transaction>
                    </div>
                @else
                    <div class="alert alert-info">
                        <p>The suspense transaction already has been matched</p>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection

<div class="modal fade" id="recordFeeModal" tabindex="-1" role="dialog" aria-labelledby="recordFeeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['suspense_transactions.record_fees', $transaction->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Record Fees</h4>
            </div>
            <div class="modal-body">
                <p><b>Transaction Details</b></p>
                <table class = "table table-hover table-responsive table-striped">
                    <tbody>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::accounting($transaction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    {!! Form::label('transaction', 'Select transaction type') !!}
                    {!! Form::select('transaction', $withdrawTypes, null, ['class'=>'form-control', 'id'=>'transaction', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'transaction') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('narrative', 'Narrative') !!}
                    {!! Form::text('narrative', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-info']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="saveDepositModal" tabindex="-1" role="dialog" aria-labelledby="saveDepositModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['suspense_transactions.record_deposits', $transaction->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Record Deposits</h4>
            </div>
            <div class="modal-body">
                <p><b>Transaction Details</b></p>
                <table class = "table table-hover table-responsive table-striped">
                    <tbody>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::accounting($transaction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    {!! Form::label('deposit_type', 'Select deposit type') !!}
                    {!! Form::select('deposit_type', $transactionTypes, null, ['class'=>'form-control', 'id'=>'deposit_type', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'deposit_type') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('narrative', 'Narrative') !!}
                    {!! Form::text('narrative', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-info']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>