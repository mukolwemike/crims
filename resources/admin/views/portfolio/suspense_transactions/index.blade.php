@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="row">
                <div class="col-md-12">
                    <div class="detail-group">
                        <h3>Account Details</h3>

                        <table class="table table-hover table-responsive">
                            <thead></thead>
                            <tbody>
                            <tr><td>Account Name</td><td>{!! $custodialAccount->account_name !!} </td></tr>
                            <tr><td>Account Number</td><td>{!! $custodialAccount->account_no !!}</td></tr>
                            <tr><td>Bank</td><td>{!! $custodialAccount->bank_name !!}</td></tr>
                            <tr><td>Currency</td><td>{!! $custodialAccount->currency->name !!}</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="detail-group" ng-init=" account_id = '{!! $custodialAccount->id !!}'" ng-controller="CustodialAccountController">
                <div class="col-md-12 margin-bottom-10">
                    <h3>SuspenseTransactions</h3>
                </div>

                <div class="alert alert-info">
                    <p>All values are in {!! $custodialAccount->currency->name !!}</p>
                </div>

                <div>
                    <suspense-transactions custodial_account_id="{{$custodialAccount->id}}"></suspense-transactions>
                </div>
            </div>
        </div>
    </div>
@endsection


