@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach($currencies as $currency)
                    <li role="presentation" class=""><a href="#{!! $currency->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $currency->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach($currencies as $currency)
                    <div role="tabpanel" class="tab-pane fade" id="{!! $currency->id !!}" aria-labelledby="home-tab"  ng-controller="MaturityAnalysisCtrl">
                        {!! Form::open() !!}
                        <div class="col-md-6 pull-right">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('start', 'Start Date') !!}

                                    {!! Form::text('start', $start, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>'dt', 'ng-focus'=>'dt=!dt']) !!}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('end', 'End Date') !!}

                                    {!! Form::text('end', $end, ['class'=>'form-control', 'datepicker-popup init-model'=>"dateSecond", 'is-open'=>'sdt', 'ng-focus'=>'sdt=!sdt']) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                {!! Form::submit('Submit', ['class'=>'btn btn-success margin-top-25']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>

                        <div class="col-md-6">
                            <h4>Weekly Analysis</h4>
                        </div>
                        <div class="col-md-6 pull-right">
                            <button type="button" class="btn btn-default pull-right" ng-click="weeklyCollapsed = !weeklyCollapsed">Toggle collapse</button>
                        </div>

                        <div class="clearfix"></div>


                        <hr>
                        <div uib-collapse="weeklyCollapsed">
                            <?php $weeklies = (new \Cytonn\Portfolio\MaturityRepository())->getWeeklyMaturityAnalysisFor($start, $end, $currency) ?>
                            <table class="table table-responsive table-striped table-hover">
                                <thead>
                                <tr><th>From</th><th>To</th><th>Amount</th></tr>
                                </thead>
                                <tbody>
                                    @foreach($weeklies as $weekly)
                                        <tr>
                                            <td>{!! $weekly['start']->toFormattedDateString() !!}</td>
                                            <td>{!! $weekly['end']->toFormattedDateString() !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($weekly['amount']) !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="hide">
                                {!! Form::text('currency_id', $currency->id, ['init-model'=>'currency', 'ng-init'=>$currency->id ]) !!}
                            </div>

                            <canvas id="line" class="chart chart-line" chart-data="weekly_data"
                                    chart-labels="weekly_labels" chart-legend="true" chart-series="weekly_series" options="weekly_options"
                                    chart-click="onClick" >
                            </canvas>
                        </div>

                        <div class="col-md-6">
                            <h4>Monthly Analysis</h4>
                        </div>
                        <div class="col-md-6 pull-right">
                            <button type="button" class="btn btn-default pull-right" ng-click="monthlyCollapsed = !monthlyCollapsed">Toggle collapse</button>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div uib-collapse="monthlyCollapsed">
                            <?php $monthlies = (new \Cytonn\Portfolio\MaturityRepository())->getMonthlyMaturityAnalysisFor($start, $end, $currency) ?>
                            <table class="table table-responsive table-striped table-hover">
                                <thead>
                                <tr><th>From</th><th>To</th><th>Amount</th></tr>
                                </thead>
                                <tbody>
                                    @foreach($monthlies as $monthly)
                                        <tr>
                                            <td>{!! $monthly['start']->toFormattedDateString() !!}</td>
                                            <td>{!! $monthly['end']->toFormattedDateString() !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($monthly['amount']) !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <canvas id="line" class="chart chart-line" chart-data="monthly_data"
                                    chart-labels="monthly_labels" chart-legend="true" chart-series="monthly_series" options="monthly_options"
                                    chart-click="onClick" >
                            </canvas>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection