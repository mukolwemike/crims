@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h1>Investment Rollover Successful</h1>
            <div class="detail-group">
                <table class="table table-hover">
                    <p>The Investment was succesfully rolled over</p>
                    <thead>

                    </thead>
                    <tbody>
                    <tr><td>Institution</td><td>{!! $deposit->security->investor->name !!}</td></tr>
                    <tr><td>Investment Type</td><td>{!! $deposit->security->subAssetClass->assetClass->name !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}</td></tr>
                    <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date )!!}</td></tr>
                    <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td></tr>


                    <tr><td><h4>New Investment</h4></td><td></td></tr>


                    <tr><td>Amount Rolled over</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($depositRollover['amount'] , true) !!}</td></tr>
                    <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($depositRollover['invested_date']) !!}</td></tr>
                    <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($depositRollover['maturity_date'] )!!}</td></tr>
                    <tr><td>Interest rate</td><td>{!! $depositRollover['interest_rate'] !!}%</tr>
                    <tr><td>Transaction Description</td><td>{!! $depositRollover['description'] !!}%</tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
