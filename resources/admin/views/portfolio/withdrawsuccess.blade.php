@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h1>Withdrawal Details</h1>
            <p>Investment has been succesfully withdrawn</p>
            @include('portfolio.partials.investmentdetails')
        </div>
    </div>
@stop