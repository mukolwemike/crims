@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <h1>{!! $institution->name !!}</h1>

            {!! Form::model($institution, ['url'=>'/dashboard/portfolio/institutions/add/'.$institution->id]) !!}

            <div class="form-group">
                {!! Form::label('name', 'Name') !!}

                {!! Form::text('name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_name', 'Bank Name') !!}

                {!! Form::text('bank_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('branch_name', 'Branch Name') !!}

                {!! Form::text('branch_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'branch_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('account_name', 'Account Name') !!}

                {!! Form::text('account_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('account_number', 'Account Number') !!}

                {!! Form::text('account_number', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_number') !!}
            </div>

            <div class="form-group">
                {!! Form::label('contact_person_firstname', 'Contact Person Firstname') !!}

                {!! Form::text('contact_person_firstname', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_firstname') !!}
            </div>

            <div class="form-group">
                {!! Form::label('contact_person_lastname', 'Contact Person Lastname') !!}

                {!! Form::text('contact_person_lastname', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_lastname') !!}
            </div>

            <div class="form-group">
                {!! Form::label('address_line1', 'Address Line 1') !!}

                {!! Form::text('address_line1', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address_line1') !!}
            </div>

            <div class="form-group">
                {!! Form::label('address_line2', 'Address Line 1') !!}

                {!! Form::text('address_line2', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address_line2') !!}
            </div>

            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop