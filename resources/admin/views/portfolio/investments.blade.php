@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="">
                <div class="btn-group">
                    {{--<a href="/dashboard/portfolio/investments/add" class="btn btn-success margin-bottom-20">--}}
                        {{--<i class="fa fa-plus"></i> Add new--}}
                    {{--</a>--}}

                    <a href="/dashboard/portfolio/investments/maturity" class="btn btn-success margin-bottom-20">
                        Maturity
                    </a>

                    <a href="/dashboard/portfolio/summary" class="btn btn-primary margin-bottom-20">
                        <i class="fa fa-list-alt"></i> Summary
                    </a>
                </div>
            </div>
            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    @foreach($currencies as $currency)
                        <li role="presentation" class=""><a href="#{!! $currency->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $currency->name !!}</a></li>
                    @endforeach
                </ul>
                <div id="myTabContent" class="tab-content">
                    @foreach($currencies as $currency)
                        <div role="tabpanel" class="tab-pane fade" id="{!! $currency->id !!}" aria-labelledby="home-tab">
                            <div ng-controller="PortfolioInvestmentsController">
                                {!! Form::hidden('currency', $currency->id, ['init-model'=>'currency_id']) !!}
                                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                                    <thead>
                                    <tr>
                                        <th colspan="7">

                                        </th>
                                        <th colspan="2">
                                            <select st-search="institution" class="form-control">
                                                <option value="">All Institutions</option>
                                                @foreach($institutions as $key => $institution)
                                                    <option value="{!! $key !!}">{!! $institution !!}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th colspan="2">
                                            <select st-search="" class="form-control">
                                                <option value="">Investment Type</option>
                                                @foreach($fund_types as $type)
                                                    <option value="{!! $type !!}">{!! $type !!}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th colspan="2" >
                                            <select st-search="on_call" class="form-control">
                                                <option value="">All Investments</option>
                                                <option value="1">On Call</option>
                                                <option value="0">Not On Call</option>
                                            </select>
                                        </th>
                                        <th colspan="3"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                                    </tr>
                                    <tr>
                                        <th>ID</th>
                                        <th>Invested in</th>
                                        <th>Investment type</th>
                                        <th st-sort="invested_date">Invested Date</th>
                                        <th st-sort="maturity_date">Maturity Date</th>
                                        <th st-sort="interest_rate">Interest rate</th>
                                        <th st-sort="amount">Principal</th>
                                        <th>Principal Repayments</th>
                                        <th>Balance</th>
                                        <th>Withholding Tax</th>
                                        <th>Net Interest</th>
                                        <th>Interest Repayments</th>
                                        <th>Interest Balance</th>
                                        <th>Total Net Value</th>
                                        <th st-sort="withdrawn">Status</th>
                                        <th>Details</th>
                                    </tr>
                                    </thead>
                                    <tbody ng-show="!isLoading">
                                    <tr ng-repeat="row in displayed">
                                        <td><% row.id %></td>
                                        <td><% row.investor %></td>
                                        <td><% row.subAssetClass %></td>
                                        <td><% row.invested_date | date %></td>
                                        <td><% row.maturity_date | date %></td>
                                        <td><% row.interest_rate %>%</td>
                                        <td><% row.principal %></td>
                                        <td><% row.principal_repayments %></td>
                                        <td><% row.balance %></td>
                                        <td><% row.withholding_tax %></td>
                                        <td><% row.net_interest %></td>
                                        <td><% row.interest_repayments %></td>
                                        <td><% row.interest_balance %></td>
                                        <td><% row.total %></td>
                                        <td><span to-html="row | portfolioInvestmentStatus"></span></td>
                                        <td>
                                            <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/portfolio/securities/<%row.portfolio_security_id%>/deposit-holdings/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody ng-show="isLoading">
                                    <tr><td colspan="100%">Loading...</td></tr>
                                    </tbody>
                                    <tfoot>
                                    <?php $repo = new \Cytonn\Portfolio\DepositHoldingRepository(); ?>
                                    <tr>
                                        <td colspan="16"></td>
                                    </tr>
                                    {{--<tr>--}}
                                    {{--<th>Totals</th>--}}
                                    {{--<th colspan="5"></th>--}}
                                    {{--<th>{!! \Cytonn\Presenters\AmountPresenter::currency($repo->getTotalportfolioInvestmentsForCurrency($currency->id)) !!}</th>--}}
                                    {{--<th>{!! \Cytonn\Presenters\AmountPresenter::currency($repo->getTotalportfolioInvestmentsGrossInterestForCurrency($currency->id)) !!}</th>--}}
                                    {{--<th>{!! \Cytonn\Presenters\AmountPresenter::currency($repo->getTotalWithholdingTaxForCurrency($currency->id) ) !!}</th>--}}
                                    {{--<th>{!! \Cytonn\Presenters\AmountPresenter::currency($repo->getTotalportfolioInvestmentsValueForCurrency($currency->id) ) !!}</th>--}}
                                    {{--<th colspan="2"><% '' %></th>--}}
                                    {{--</tr>--}}
                                    <tr>
                                        <td  colspan="5" class="text-center">
                                            Items per page
                                        </td>
                                        <td colspan="5" class="text-center">
                                            <input type="text"  ng-model="itemsByPage"/>
                                        </td>
                                        <td colspan="6" class="text-center">
                                            <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="10"></div>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop