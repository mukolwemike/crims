<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Security</td>
                    <td>{!! $security->name !!}</td>
                </tr>
                <tr>
                    <td>Sub Asset Class</td>
                    <td>{!! $security->subAssetClass->name !!}</td>
                </tr>
                <tr>
                    <td>Date of Sale</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                @if(isset($data['settlement_date']))
                    <tr>
                        <td>Date of Settlement</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['settlement_date']) !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Number of Shares Sold</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], false, 0) !!}</td>
                </tr>
                <tr>
                    <td>Price per share</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['sale_price'], false, 4) !!}</td>
                </tr>
                <tr>
                    <td>Fund</td><td>@if($fund) {{ $fund->name }} @endif</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>