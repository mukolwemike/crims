<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Fund Liquidity Limit</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <tbody>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{!! $unitFund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Warn</td>
                        <td>{!! $data['warn'] !!}%</td>
                    </tr>
                    <tr>
                        <td>Limit</td>
                        <td>{!! $data['limit'] !!}%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>