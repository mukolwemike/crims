<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <th colspan="2">Old Investment Details</th>
                </tr>
                <tr>
                    <td>Principal</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}</td>
                </tr>
                <tr>
                    <td>Investment Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date) !!}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</td>
                </tr>
                <tr>
                    <th colspan="2">New Investment Details</th>
                </tr>
                <tr>
                    <td>Amount to reinvest</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                </tr>
                <tr>
                    <td>Withdraw</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($remaining) !!}</td>
                </tr>
                <tr>
                    <td>Interest Rate</td>
                    <td>{!! $data['interest_rate'] !!}%</td>
                </tr>
                <tr>
                    <td>New Investment date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td>
                </tr>
                <tr>
                    <td>New Maturity date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td>
                </tr>
                <tr>
                    <td>Taxable</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['taxable']) !!}</td>
                </tr>
                @if(isset($data['description']))
                    <tr>
                        <td>Transaction Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
    </div>
</div>
