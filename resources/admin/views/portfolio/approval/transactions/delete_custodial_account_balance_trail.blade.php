<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Custodial Account Balance Trail details</h4>
        </div>

        <table class="table table-responsive">
            <thead></thead>
            <tbody>
                <tr>
                    <td>Account</td><td>{!! $trail->custodialAccount->account_name !!}</td>
                </tr>
                <tr>
                    <td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($trail->date) !!}</td>
                </tr>
                <tr>
                    <td>Balance</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($trail->amount) !!}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

