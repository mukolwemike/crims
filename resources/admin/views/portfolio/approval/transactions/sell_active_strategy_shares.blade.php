<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Security</td>
                    <td>{!! $activeStrategySecurity($data['security_id'])->name !!}</td>
                </tr>
                <tr>
                    <td>Date of Sale</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                <tr>
                    <td>Number of Shares Sold</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], false, 0) !!}</td>
                </tr>
                <tr>
                    <td>Price per share</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price'], false, 4) !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>