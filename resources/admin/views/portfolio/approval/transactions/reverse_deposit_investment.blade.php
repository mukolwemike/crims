<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            {{--<a class = "pull-right" ng-click = "toggleEdit()" ng-hide = "showedit" href = ""><i--}}
            {{--class = "fa fa-edit"></i></a>--}}
            <h4>Reverse Deposit Investment</h4>
        </div>

        <div class = "panel-body">

            <h3>Reason</h3>

            {!! $data['reason'] !!}

            <h3>Deposit Holding Details</h3>
            <table class = "table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Institution</td>
                        <td>{!! $portfolioInvestor->name !!}</td>
                    </tr>
                    <tr>
                        <td>Investment date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Maturity date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Interest rate</td>
                        <td>{!! $deposit->interest_rate !!} %</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
