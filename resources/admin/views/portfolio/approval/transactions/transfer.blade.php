<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transfer details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Amount</td>
                    <td>{!!$sender->currency->code.' '. \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!} =>
                        {!! $recipient->currency->code.' ' . \Cytonn\Presenters\AmountPresenter::currency((float) isset($data['credit_amount']) ? $data['credit_amount'] : $data['amount'] * (float) $data['effective_rate']) !!}
                    </td>
                </tr>
                <tr>
                    <td>Exchange Rate</td>
                    <td>{!! $data['exchange'] !!}</td>
                </tr>
                <tr>
                    <td>Source Account</td>
                    <td>{!! $sender->fullname !!}</td>
                </tr>
                <tr>
                    <td>Source Account Number</td>
                    <td>{!! $sender->account_no !!}</td>
                </tr>
                <tr>
                    <td>Source Fund Manager</td>
                    <td>{!! $sender->fundManager->name !!}</td>
                </tr>
                <tr>
                    <td>Destination Account</td>
                    <td>{!! $recipient->fullname !!}</td>
                </tr>
                <tr>
                    <td>Destination Account Number</td>
                    <td>{!! $recipient->account_no !!}</td>
                </tr>
                <tr>
                    <td>Destination Fund Manager</td>
                    <td>{!! $recipient->fundManager->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['transfer_date']) !!}</td>
                </tr>
                <tr>
                    <td>Narrative</td>
                    <td>{!! $data['narrative'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>