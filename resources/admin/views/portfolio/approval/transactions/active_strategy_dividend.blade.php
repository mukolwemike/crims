<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $ctiveStrategySecurity($data['security_id'])->name !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['book_closure_date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares at Book Closure Date</td>
                        <td>{!! number_format($shares_at_closure) !!}</td>
                    </tr>
                    <tr>
                        <td>Dividend Type</td>
                        @if(isset($data['dividend_type']))
                            <td>{!! $dividend_types[$data['dividend_type']] !!}</td>
                        @else
                            <td>{!! $dividend_types['cash'] !!}</td>
                        @endif
                    </tr>

                    <tr>
                        @if(isset($data['dividend_type']))
                            @if($data['dividend_type'] == 'shares')
                                <td>Shares</td>
                            @elseif($data['dividend_type'] == 'cash')
                                <td>Dividend per share (before Tax)</td>
                            @endif
                        @else
                            <td>Dividend per share (before Tax)</td>
                        @endif
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['dividend'], false, 4) !!}</td>
                    </tr>

                    @if(isset($data['method_of_calculation']) && $data['method_of_calculation'])
                        <tr>
                            <td>Method of Calculation</td>
                            <td>{!! $methods_of_calculation[$data['method_of_calculation']] !!}</td>
                        </tr>
                    @endif
                    @if(isset($data['method_of_calculation']) and $data['method_of_calculation'] == 'ratio')
                        <tr>
                            <td>Old Shares</td>
                            <td>
                                @if(isset($data['old_shares']))
                                    {!! $data['old_shares'] !!}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>New Shares</td>
                            <td>
                                @if(isset($data['new_shares']))
                                    {!! $data['new_shares'] !!}
                                @endif
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>