<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="30%">Security Code:</td>
                        <td>{!! $data['code'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Security Name:</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Asset Class:</td>
                        <td>{!! $subAssetClass->assetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Sub Asset Class:</td>
                        <td>{!! $subAssetClass->name !!}</td>
                    </tr>

                    @if($depositType)
                        <tr>
                            <td width="30%">Portfolio Deposit Type:</td>
                            <td>{!! $depositType->name !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td width="30%">Fund Manager:</td>
                        <td>{!! $fundManager->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Contact Person:</td>
                        <td>{!! $data['contact_person'] !!}</td>
                    </tr>

                    @if($country)
                    <tr>
                        <td width="30%">Country:</td>
                        <td>{!! $country->name !!}</td>
                    </tr>
                    @endif

                    <tr>
                        <td width="30%">Currency:</td>
                        <td>{!! $currency->name !!}</td>
                    </tr>

                    @if($sector)
                    <tr>
                        <td width="30%">Sector:</td>
                        <td>{!! $sector->name !!}</td>
                    </tr>
                    @endif

                    @if(isset($data['issuer']))
                    <tr>
                        <td width="30%">Issuer:</td>
                        <td>{!! $data['issuer'] !!}</td>
                    </tr>
                    @endif

                    @if(isset($data['isin_code']))
                    <tr>
                        <td width="30%">ISIN Code:</td>
                        <td>{!! $data['isin_code'] !!}</td>
                    </tr>
                    @endif

                    @if(isset($data['batch_pricing']))
                    <tr>
                        <td width="30%">Batch Pricing:</td>
                        <td>{!! $data['batch_pricing'] !!}</td>
                    </tr>
                    @endif

                    @if($branch)
                    <tr>
                        <td width="30%">Account Name:</td>
                        <td>{!! $data['investor_account_name'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Account Number:</td>
                        <td>{!! $data['investor_account_number'] !!}</td>
                    </tr>

                    <tr>
                        <td width="30%">Bank Name:</td>
                        <td>{!! $branch->bank->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Branch Name:</td>
                        <td>{!! $branch->name !!}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>