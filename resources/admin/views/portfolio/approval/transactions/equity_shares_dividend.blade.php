<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $security($data['security_id'])->name !!}</td>
                    </tr>
                    <tr>
                        <td>Book Closure Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['book_closure_date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares at Book Closure Date</td>
                        <td>{!! number_format($shares_at_closure) !!}</td>
                    </tr>
                    <tr>
                        <td>Dividend Type</td>
                        @if(isset($data['dividend_type']))
                            <td>{!! $data['dividend_type'] !!}</td>
                        @else
                            <td>{!! $dividend_types['cash'] !!}</td>
                        @endif
                    </tr>

                    @if($data['dividend_type'] == 'shares')
                        @if($data['method_of_calculation'] == 'ratio')
                            <tr>
                                <td>Ratio - old:new</td>
                                <td>{{ $data['old_shares'] }} : {{ $data['new_shares'] }}</td>
                            </tr>
                            <tr>
                                <td>Bonus shares</td>
                                <td>{{ number_format($shares_at_closure * $data['new_shares']  / $data['old_shares'] ) }}</td>
                            </tr>
                        @elseif($data['method_of_calculation'] == 'conversion')
                            <tr>
                                <td>Conversion price per share</td>
                                <td>{{ $data['conversion_price'] }}</td>
                            </tr>
                            <tr>
                                <td>Dividend per share (before Tax)</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['dividend'], false, 4) !!}</td>
                            </tr>
                        @else
                            <tr>
                                <td>Not supported</td>
                            </tr>
                        @endif
                    @elseif($data['dividend_type'] == 'cash')
                        <tr>
                            <td>Dividend per share (before Tax)</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['dividend'], false, 4) !!}</td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="100%">Not supported</td>
                        </tr>
                    @endif

                    @if($fund)
                        <tr>
                            <td>Unit Fund</td>
                            <td>{!! $fund->name !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>