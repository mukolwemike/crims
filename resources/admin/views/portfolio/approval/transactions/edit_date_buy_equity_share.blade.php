<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead>
                <tr>
                    <td>Description</td>
                    <td>Old Date</td>
                    <td>Description</td>
                    <td>New Date</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <div class="row">
                        <div class="col-md-6">
                            <td>Trade Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->date) !!}</td>
                        </div>
                        <div class="col-md-6">
                            <td>Trade Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['trade_date']) !!}</td>
                        </div>
                    </div>
                </tr>

                <tr>
                    <div class="row">
                        <div class="col-md-6">
                            <td>Settlement Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->settlement_date) !!}</td>
                        </div>
                        <div class="col-md-6">
                            <td>Settlement Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['settlement_date']) !!}</td>
                        </div>
                    </div>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>