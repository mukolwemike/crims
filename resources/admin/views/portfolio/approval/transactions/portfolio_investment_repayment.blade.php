<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Institution</td>
                        <td>{!! $deposit->security->investor->name !!}</td>
                    </tr>
                    <tr>
                        <td>Investment Type</td>
                        <td>{!! $deposit->security->subAssetClass->assetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td>Principal</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->principal()) !!}</td>
                    </tr>
                    <tr>
                        <td>Gross Interest</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getGrossInterestForInvestment()) !!}</td>
                    </tr>
                    <tr>
                        <td>Taxable </td>
                        @if($deposit->taxable)
                            <td>Yes</td>
                        @else
                            <td>No</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Withholding Tax</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getWithholdingTaxForInvestment()) !!}</td>
                    </tr>
                    <tr>
                        <td>Net Interest</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getNetInterestForInvestment()) !!}</td>
                    </tr>
                    <tr>
                        <td>Today Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestment()) !!}</td>
                    </tr>
                    <tr>
                        <td>Invested date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date )!!}</td>
                    </tr>
                    <tr>
                        <td>Maturity date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td>
                    </tr>
                    <tr>
                        <th>Repayment Amount</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</th>
                    </tr>
                    <tr>
                        <th>Balance As at Date of Repayment </th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestment(\Carbon\Carbon::parse($data['date']))) !!}</th>
                    </tr>
                    <tr>
                        <th>Balance after Repayment </th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestment(\Carbon\Carbon::parse($data['date'])) - $data['amount']) !!}</th>
                    </tr>
                    <tr>
                        <th>Repayment Date</th>
                        <th>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</th>
                    </tr>
                    @if(isset($data['payment_type']))
                        <tr>
                            <th>Type</th>
                            <th>{!! $repaymentType->name !!}</th>
                        </tr>
                    @endif
                    <tr>
                        <th>Narration</th>
                        <th>{!! $data['narrative'] !!}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>