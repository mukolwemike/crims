<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>

        <table class="table table-responsive">
            <thead></thead>
            <tbody>
                <tr>
                    <td>Account</td><td>{!! $account->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                <tr>
                    <td>Balance</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                </tr>
                <tr>
                    <td>Fund Manager</td><td>{!! $account->fundManager->name !!}</td>
                </tr>
                <tr>
                    <td>Custodial Account</td><td>{!! $account->account_name .  ' - ' . $account->account_no !!}</td>
                </tr>
                <tr>
                    <td>Currency</td><td>{!! $account->currency->code !!}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

