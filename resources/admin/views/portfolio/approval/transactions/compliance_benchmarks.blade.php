<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Fund Compliance Benchmarks</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="30%">Benchmark Name</td>
                        <td>{{ isset($data['name']) ? $data['name'] : null }}</td>
                    </tr>
                    @if($complianceType)
                    <tr>
                        <td width="30%">Compliance Type</td>
                        <td>{{ $complianceType->name }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Compliance Description</td>
                        <td>{{ $complianceType->description }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            @if($limits)
                <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                    <thead>
                        <tr><th colspan="4">Compliance Limits</th></tr>
                        <tr>
                            <th>Asset Class</th>
                            <th>Sub Asset Class</th>
                            <th>Lower limit</th>
                            <th>Upper limit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($limits as $limit)
                            @if($limit)
                                <tr>
                                    <td>{{ $limit['sub_class']->assetClass->name }}</td>
                                    <td>{{ $limit['sub_class']->name }}</td>
                                    <td>{{ $limit['lower'] }}%</td>
                                    <td>{{ $limit['upper'] }}%</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @endif

        </div>
    </div>
</div>