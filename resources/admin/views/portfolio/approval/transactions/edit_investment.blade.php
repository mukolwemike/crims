<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            {{--<a class = "pull-right" ng-click = "toggleEdit()" ng-hide = "showedit" href = ""><i--}}
            {{--class = "fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            @if(!is_null($investment))
                <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Old Values</th>
                            <th>New Values</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Institution</td>
                        <td colspan="2">{!! $investment->security->investor->name !!}</td>
                    </tr>
                    <tr>
                        <td>Sub Asset Class</td>
                        <td colspan="2">{!! $investment->security->subAssetClass->name !!}</td>
                    </tr>

                    {{--<tr>--}}
                        {{--<td>Custodial account</td>--}}
                        {{--<td>{!! $investment->repo->getCustodialAccountName() !!}</td>--}}
                        {{--<td>{!! $custodialaccounts($data['custodial_account_id'])->account_name !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Investment date</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date )!!}</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>Maturity date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td>
                    </tr>
                    <tr class="success">
                        <td>On call</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($investment->on_call) !!}</td>
                        <td>
                            @if(isset($data['on_call']))
                                {!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['on_call']) !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Interest rate</td>
                        <td>{!! $investment->interest_rate !!} %</td>
                        <td>{!! $data['interest_rate'] !!} %</td>
                    </tr>

                    @if($tax)
                        <tr>
                            <td>Tax Rate</td>
                            <td> @if($investment->wtaxRate){{ $investment->wtaxRate->key }} + {{ $investment->wtaxRate->value }}@endif</td>
                            <td>{{ $tax->key }} {{ $tax->value }}</td>
                        </tr>
                    @endif
                    {{--<tr>--}}
                        {{--<td>Amount</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Taxable</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($investment->taxable) !!}</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['taxable']) !!}</td>--}}
                    {{--</tr>--}}
                    @if(isset($data['description']))
                        <tr>
                            <td>Description</td>
                            <td>{!! $investment->description !!}</td>
                            <td>{!! $data['description'] !!}</td>
                        </tr>
                    @endif
                    @if(isset($data['contact_person']))
                        <tr>
                            <td>Contact Person</td>
                            <td>{!! $investment->contact_person !!}</td>
                            <td>{!! $data['contact_person'] !!}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger">
                    <p>The investment cannot be found.</p>
                </div>
            @endif
        </div>
    </div>
</div>
