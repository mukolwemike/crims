<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Update Active Strategy Bought Shares</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $holding->security->name !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares Bought</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], false, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Cost per share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['cost']) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>