<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            {{--<a class = "pull-right" ng-click = "toggleEdit()" ng-hide = "showedit" href = ""><i--}}
                        {{--class = "fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Institution</td>
                    <td>{!! $investor->name !!}</td>
                </tr>
                <tr>
                    <td>Fund Type</td>
                    <td>{!! $fundType->name !!}</td>
                </tr>
                @if($fundManager)
                    <tr>
                        <td>Fund Manager</td>
                        <td>{!! $fundManager->name !!}</td>
                    </tr>
                @endif
                @if(isset($data['type_id']))
                    <tr>
                        <td>Portfolio Investment Type</td>
                        <td>{!! $investmentType($data['type_id'])->name !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Custodial account</td>
                    <td>{!! $custodialAccount->account_name !!}</td>
                </tr>
                <tr>
                    <td>Investment date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td>
                </tr>
                <tr>
                    <td>Maturity date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td>
                </tr>
                @if(isset($data['on_call']))
                    @if($data['on_call'])
                        <tr class="success">
                            <td>On call</td><td>Yes</td>
                        </tr>
                    @endif
                @endif
                <tr>
                    <td>Interest rate</td>
                    <td>{!! $data['interest_rate'] !!} %</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                </tr>
                <tr>
                    <td>Taxable</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['taxable']) !!}</td>
                </tr>
                @if(isset($data['description']))
                <tr>
                    <td>Description</td>
                    <td>{!! $data['description'] !!}</td>
                </tr>
                @endif
                @if(isset($data['contact_person']))
                <tr>
                    <td>Contact Person</td>
                    <td>{!! $data['contact_person'] !!}</td>
                </tr>
                @endif
                </tbody>
            </table>

            <div ng-show = "showedit">
                <h4>Edit details</h4>
                {!! Form::model((object) $data, []) !!}

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('portfolio_investor_id', 'Institution') !!}</div>
                    <div class = "col-md-9">{!! Form::select('portfolio_investor_id', $investorsLists, NULL, ['class'=>'form-control','id'=>'portfolio_investor_id', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'portfolio_investor_id') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('fund_type_id', 'Investment Type') !!}</div>
                    <div class = "col-md-9">{!! Form::select('fund_type_id', $fundTypeLists, NULL, ['class'=>'form-control','id'=>'fund_type_id', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_type_id') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('custodial_account_id', 'Custodial account') !!}</div>
                    <div class = "col-md-9">{!! Form::select('custodial_account_id', $custodialAccountsLists, NULL, ['class'=>'form-control','id'=>'custodial_account_id', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}</div>
                </div>

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    <div class = "col-md-3">{!! Form::label('invested_date', 'Investment date') !!}</div>
                    <div class = "col-md-9">{!! Form::text('invested_date', NULL, ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}</div>
                </div>

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    <div class = "col-md-3">{!! Form::label('maturity_date', 'Maturity date') !!}</div>
                    <div class = "col-md-9">{!! Form::text('maturity_date', NULL, ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('interest_rate', 'Interest rate') !!}</div>
                    <div class = "col-md-9">{!! Form::number('interest_rate', NULL, ['class'=>'form-control', 'required','init-model'=>'interest_rate']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('amount', 'Amount') !!}</div>
                    <div class = "col-md-9">{!! Form::number('amount', NULL, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">{!! Form::label('taxable', 'Tax Rate') !!}</div>
                    <div class="col-md-9">{!! Form::select('taxable', [true=>'Taxable', false=>'Zero rated'], null, ['class'=>'form-control', 'init-model'=>'taxable']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">{!! Form::label('contact_person', 'Contact Person') !!}</div>
                    <div class="col-md-9">{!! Form::text('contact_person', null, ['class'=>'form-control', 'init-model'=>'contact_person']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person') !!}</div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('description', 'Transaction Description') !!}</div>
                    <div class = "col-md-9">{!! Form::textarea('description', NULL, ['class'=>'form-control', 'rows'=>2, 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}</div>
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
