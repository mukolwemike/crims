<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Before</th>
                        <th>Now</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%">Security Code:</td>
                        <td>{!! $security->code !!}</td>
                        <td>{!! $data['code'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Security Name:</td>
                        <td>{!! $security->name !!}</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Asset Class:</td>
                        <td>{!! $security->subAssetClass->assetClass->name !!}</td>
                        <td>{!! $subAssetClass->assetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Sub Asset Class:</td>
                        <td>{!! $security->subAssetClass->name !!}</td>
                        <td>{!! $subAssetClass->name !!}</td>
                    </tr>

                    @if($depositType)
                        <tr>
                            <td width="30%">Portfolio Deposit Type:</td>
                            <td>@if($security->depositType){!! $security->depositType->name !!}@endif</td>
                            <td>{!! $depositType->name !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td width="30%">Fund Manager:</td>
                        <td>{!! $security->fundManager->name !!}</td>
                        <td>{!! $fundManager->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Contact Person:</td>
                        <td>{!! $security->contact_person !!}</td>
                        <td>{!! $data['contact_person'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Account Name:</td>
                        <td>{!! $security->investor->account_name !!}</td>
                        <td>{!! $data['investor_account_name'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Account Number:</td>
                        <td>{!! $security->investor->account_number !!}</td>
                        <td>{!! $data['investor_account_number'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Bank Name:</td>
                        <td>{!! $security->investor->bank_name !!}</td>
                        <td>{!! $data['investor_bank'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Branch Name:</td>
                        <td>{!! $security->investor->branch_name !!}</td>
                        <td>{!! $data['investor_bank_branch'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>