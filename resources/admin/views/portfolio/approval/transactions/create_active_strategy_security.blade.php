<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $data['name'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>