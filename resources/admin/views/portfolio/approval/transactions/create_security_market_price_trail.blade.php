<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class="panel-body">
            <h4>Security Details</h4>
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <td width="30%">Security Code:</td>
                    <td>{!! $security->code !!}</td>
                </tr>
                <tr>
                    <td width="30%">Security Name:</td>
                    <td>{!! $security->name !!}</td>
                </tr>
                <tr>
                    <td width="30%">Sub Asset Class:</td>
                    <td>{!! $security->subAssetClass->name !!}</td>
                </tr>
                <tr>
                    <td width="30%">Fund Manager:</td>
                    <td>{!! $security->fundManager->name !!}</td>
                </tr>

                </tbody>
            </table>

            <h4>Market Price Trail Details</h4>
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <td width="30%">Date:</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                <tr>
                    <td width="30%">Price:</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price'], false, 4) !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>