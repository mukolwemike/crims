<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Remove Operating Account</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>Custodial Account</th>

                    <th>Fund Name</th>

                    <th>Reason</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! $account->account_name !!}</td>

                    <td>{!! $fund->name !!}</td>

                    <td>{!! $data['reason'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>