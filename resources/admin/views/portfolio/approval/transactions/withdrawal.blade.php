<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->amount)!!}</td></tr>
                <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td></tr>
                <tr><td>Interest rate</td><td>{!! $deposit->interest_rate !!}%</td></tr>

                @if($data['premature'] == true)
                    <tr><td>Total</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestmentAsAtDate($data['end_date'])) !!}</td></tr>
                    <tr class="danger"><td>Premature end date</td><td> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['end_date']) !!} </td></tr>

                    @if($data['partial'] === true || $data['partial'] == 'true')
                        <tr class="danger"><td>Partial Withdraw</td><td>Yes</td></tr>
                        <tr class="danger"><td>Partial Withdraw Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                        <tr class="success"><td>Reinvested amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestmentAsAtDate($data['end_date']) - $data['amount']) !!}</td></tr>
                    @else
                        <tr><td colspan="2">Withdraw full amount</td></tr>
                    @endif

                @else
                    <tr class="success"><td>Mature Withdrawal</td><td>Yes</td></tr>
                @endif
            </table>
        </div>
    </div>
</div>