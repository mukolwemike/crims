<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Portfolio Order Allocations</h4>
        </div>

        <div class = "panel-body">

            @if($limits->count() > 0)
            <table>
                <p class="danger"> You will not be able to settle this allocation because:</p>
                <tr>
                    @foreach($limits as $limit)
                        <span class="danger-tags"><el-tag type="danger">{{ $limit->reason }}</el-tag> </span>
                    @endforeach
                </tr>
            </table>
            @endif

            <br>

            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $portfolioOrder->security->name !!}</td>
                    </tr>

                    <tr>
                        <td>Asset Class</td>
                        <td>{!! $asset_class !!}</td>
                    </tr>

                    <tr>
                        <td>Portfolio Order Type</td>
                        <td>{!! $portfolioOrder->type->name !!}</td>
                    </tr>

                    @if($asset_class == 'Equities')
                        <tr>
                            <td>No of shares ordered</td>
                            <td>{!! $portfolioOrder->unitFundOrders->sum('shares') !!}</td>
                        </tr>

                        <tr>
                            <td>No of shares allocated</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation->shares, 0, false) !!}</td>
                        </tr>

                        <tr>
                            <td>Cut off price</td>
                            <td>{!! $portfolioOrder->cut_off_price !!}</td>
                        </tr>
                        <tr>
                            <th width="25%"> Allocation price</th>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation->price) !!}</td>
                        </tr>
                        @if($fees)
                            <tr>
                                <th width="25%"> Fee per share</th>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fees->sum('amount') / $allocation->shares) !!}</td>
                            </tr>
                        @endif
                    @endif

                    @if($asset_class == 'Deposits' || $asset_class == 'Bonds')
                        <tr>
                            <td>Amount ordered</td>
                            <td >{!! \Cytonn\Presenters\AmountPresenter::currency($portfolioOrder->unitFundOrders->sum('amount')) !!}</td>
                        </tr>

                        <tr>
                            <td>Amount allocated</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation->amount, 0, false) !!}</td>
                        </tr>

                        <tr>
                            <td>Minimum rate</td>
                            <td >{!! $portfolioOrder->minimum_rate !!}</td>
                        </tr>

                        <tr>
                            <td>Rate allocated</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation->rate) !!}</td>
                        </tr>
                    @endif

                </tbody>
            </table>
                <hr>

                @if($fundOrders)
                    <h5>Allocations per fund ordered</h5>
                    <hr>
                    <table class = "table table-hover table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Unit Fund</th>
                            <th>
                                @if($asset_class == 'Equities')
                                    Shares ordered
                                @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                    Amount ordered
                                @endif
                            </th>
                            <th>
                                @if($asset_class == 'Equities')
                                    Shares to allocate
                                @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                    Amount to allocate
                                @endif
                            </th>
                            <th>
                                @if($asset_class == 'Equities')
                                    Cost Price
                                @endif
                            </th>
                            <th>
                                @if($asset_class == 'Equities')
                                    Gross Amount
                                @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                    Amount
                                @endif
                            </th>

                            @if($asset_class == 'Equities')
                                <th>
                                    Net Amount
                                </th>
                            @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                            @endif

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fundOrders as $fundOrder)
                            <tr>
                                <td> {{ $fundOrder['unitFund']->name }} </td>
                                <td>
                                    @if($asset_class == 'Equities')
                                        {{ $fundOrder['shares'] }}
                                    @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                        {{ \Cytonn\Presenters\AmountPresenter::currency($fundOrder['amount']) }}
                                    @endif
                                </td>
                                <td>
                                    @if($asset_class == 'Equities')
                                        {{ $fundOrder['allocated'] }}
                                    @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                        {{ \Cytonn\Presenters\AmountPresenter::currency($fundOrder['allocated']) }}
                                    @endif
                                </td>
                                <td>
                                    {{ $fundOrder['cost_price'] }}
                                </td>
                                <td>
                                    @if($asset_class == 'Equities')
                                        {{ \Cytonn\Presenters\AmountPresenter::currency($fundOrder['value']) }}
                                    @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                        {{ \Cytonn\Presenters\AmountPresenter::currency($fundOrder['allocated']) }}
                                    @endif
                                </td>
                                @if($asset_class == 'Equities')
                                <td>
                                    {{ \Cytonn\Presenters\AmountPresenter::currency(($fundOrder['cost_price'] + $fundOrder['fee_per_share']) * $fundOrder['allocated']) }}
                                </td>
                                @elseif($asset_class == 'Deposits' || $asset_class == 'Bonds')
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

            @if($asset_class == 'Equities' && $fees)
                    <br>
                <table class="table table-hover table-responsive table-striped">
                    <tbody>
                        <tr>
                            <th>Gross Amount</th>
                            <th></th>
                            <th style="text-align: right">{{ \Cytonn\Presenters\AmountPresenter::currency($gross_amount = $allocation->shares *  $allocation->price) }}</th>
                        </tr>
                        @foreach($fees as $fee)
                            <tr>
                                <td>{{ $fee['description']  }}</td>
                                <td>@if($fee['percentage']){{ $fee['percentage'] }} % @endif</td>
                                <td style="text-align: right">{{ $fee['amount'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total Fees</th>
                            <th> KES: </th>
                            <th style="text-align: right">
                                {{ \Cytonn\Presenters\AmountPresenter::currency($fees->sum('amount')) }}
                            </th>
                        </tr>
                        <tr>
                            <th>Net Amount</th>
                            <th> KES: </th>
                            <th style="text-align: right">
                                @if($portfolioOrder->type->slug == 'buy')
                                    {{ \Cytonn\Presenters\AmountPresenter::currency($gross_amount + $fees->sum('amount')) }}
                                @else
                                    {{ \Cytonn\Presenters\AmountPresenter::currency($gross_amount - $fees->sum('amount')) }}
                                @endif
                            </th>
                        </tr>
                    </tfoot>
                </table>
            @endif

            @if($asset_class == 'Deposits')
                    <br>
                    <table class="table table-hover table-responsive table-striped">
                        <tbody>
                        <tr>
                            <th>Consideration</th>
                            <th></th>
                            <th style="text-align: right">{{ \Cytonn\Presenters\AmountPresenter::currency($data['purchase_cost'] - $data['commission']) }}</th>
                        </tr>
                        <tr>
                            <td>Prices</td><td colspan="2"><b>Par price:</b> {{ $data['par_price'] }}  <b>Clean price:</b> {{ $data['clean_price'] }} <b>Dirty price:</b> {{ $data['dirty_price'] }}</td>
                        </tr>
                        <tr>
                            <td>Rates</td><td colspan="2"><b>Coupon rate:</b> {{ $data['coupon_rate'] }}% <b>Yield to maturity:</b> {{ $data['ytm'] }}%</td>
                        </tr>
                        <tr>
                            <td>Dates</td><td colspan="2"><b>Value Date: </b> {{ \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) }}  <b>Purchase Date: </b> {{ \Cytonn\Presenters\DatePresenter::formatDate($data['purchase_date']) }} <b>Maturity Date: </b> {{ \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) }}</td>
                        </tr>
                        <tr>
                            <td>Commission</td><td></td><td style="text-align: right"> {{ \Cytonn\Presenters\AmountPresenter::currency($data['commission']) }}</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Total Amount Payable</th>
                            <th></th>
                            <th style="text-align: right">
                               KES {{ \Cytonn\Presenters\AmountPresenter::currency($data['purchase_cost']) }}
                            </th>
                        </tr>

                        </tfoot>
                    </table>
            @endif
        </div>
    </div>
</div>