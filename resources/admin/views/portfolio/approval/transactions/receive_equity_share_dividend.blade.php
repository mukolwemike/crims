<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $security->name !!}</td>
                    </tr>
                    <tr>
                        <td>Book Closure Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($dividend->book_closure_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Previous Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($dividend->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Current Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares at Book Closure Date</td>
                        <td>{!! number_format($shares_at_closure) !!}</td>
                    </tr>
                    <tr>
                        <td>Dividend Type</td>
                        @if(isset($dividend->dividend_type))
                            <td>{!! $dividend->dividend_type !!}</td>
                        @else
                            <td>{!! $dividend_types['cash'] !!}</td>
                        @endif
                    </tr>

                    <tr>
                        @if($dividend->dividend_type == 'shares')
                            @if($dividend->method_of_calculation == 'ratio')
                                <td>Ratio - old:new</td>
                                <td>{{ $dividend->old_shares }} : {{ $dividend->new_shares }}</td>
                            @elseif($dividend->method_of_calculation == 'conversion')
                                <td>Conversion price per share</td>
                                <td>{{ $dividend->conversion_price }}</td>
                            @else
                                <td>Not supported</td>
                            @endif
                        @elseif($dividend->dividend_type == 'cash')
                            <td>Dividend per share (before Tax)</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($dividend->dividend, false, 4) !!}</td>
                        @else
                            <td colspan="100%">Not supported</td>
                        @endif
                    </tr>

                    @if($fund)
                        <tr>
                            <td>Unit Fund</td>
                            <td>{!! $fund->name !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>