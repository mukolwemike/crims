<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="30%">Security Name:</td>
                        <td>{!! $data['portfolio_security'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Asset Class:</td>
                        <td>{!! $data['asset_class'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Sub Asset Class:</td>
                        <td>{!! $data['sub_asset_class'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Portfolio Investor:</td>
                        <td>{!! $data['investor'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Number of Shares:</td>
                        <td>{!! $data['number'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Cost per Share:</td>
                        <td>{!! $data['cost'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Investment Date:</td>
                        <td>{!! $data['investment_date'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Market Price:</td>
                        <td>{!! $data['market_price'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>