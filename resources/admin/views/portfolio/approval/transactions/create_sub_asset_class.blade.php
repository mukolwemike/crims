<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped">
                <tbody>
                    <tr>
                        <th>No.</th>
                        <th>Asset Class.</th>
                        <th>Sub Asset Class:</th>
                    </tr>
                    @foreach($data as $asset)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{!! $asset['asset_class'] !!}</td>
                        <td>{!! $asset['sub_asset_class'] !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>