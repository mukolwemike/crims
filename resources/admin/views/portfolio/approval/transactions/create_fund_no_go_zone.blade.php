<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Fund Liquidity Limit</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <tbody>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{!! $unitFund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Security</td>
                        <td>{!! $security->name !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>