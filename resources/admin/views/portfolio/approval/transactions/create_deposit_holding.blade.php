<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="30%">Security Name:</td>
                        <td>{!! $portfolioSecurity->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Custodial Account:</td>
                        <td>{!! $custodialAccount->full_name !!}</td>
                    </tr>
                    @if($fund)
                        <tr>
                            <td>Fund</td>
                            <td>{{ $fund->name }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td width="30%">Portfolio Investment Type:</td>
                        <td>{!! $investmentType->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Asset Class:</td>
                        <td>{!! $portfolioSecurity->subAssetClass->assetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Sub Asset Class:</td>
                        <td>{!! $portfolioSecurity->subAssetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Fund Manager:</td>
                        <td>{!! $portfolioSecurity->fundManager->name !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">InvestmentDate:</td>
                        <td>{!! $data['invested_date'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Maturity Date:</td>
                        <td>{!! $data['maturity_date'] !!}</td>
                    </tr>
                    <tr>
                        <td>Interest rate</td>
                        <td>{!! $data['interest_rate'] !!}%</td>
                    </tr>
                    @if(isset($data['coupon_payment_duration']))
                    <tr>
                        <td>Interest Payment Duration</td>
                        @if($data['coupon_payment_duration'] > 0)
                            <td>Every {!!  $data['coupon_payment_duration'] !!} months</td>
                        @endif
                    </tr>
                    @endif
                    <tr>
                        <td>Tax rate</td>
                        <td>{!! $tax->key !!} - ({!! $tax->value !!}%)</td>
                    </tr>
                    @if(isset($data['nominal_value']))
                    <tr>
                        <td>Nominal Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['nominal_value']) !!}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>Cost Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>On Call</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo(get_optional($data, 'on_call')) !!}</td>
                    </tr>
                    @if(isset($data['description']))
                        <tr>
                            <td>Description</td>
                            <td>{!! $data['description'] !!}</td>
                        </tr>
                    @endif
                    @if(isset($data['contact_person']))
                        <tr>
                            <td>Contact Person</td>
                            <td>{!! $data['contact_person'] !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>