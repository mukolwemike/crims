<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="30%">Security Name:</td>
                        <td>{!! $data['portfolio_security'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Asset Class:</td>
                        <td>{!! $data['asset_class'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Portfolio Investor:</td>
                        <td>{!! $data['investor'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Face Value:</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['face_value']) !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Value Date:</td>
                        <td>{!! $data['value_date'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Maturity Date:</td>
                        <td>{!! $data['maturity_date'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Tenor:</td>
                        <td>{!! $data['tenor'] !!} years</td>
                    </tr>
                    <tr>
                        <td width="30%">Coupon Rate:</td>
                        <td>{!! $data['coupon_rate'] !!}%</td>
                    </tr>
                    @if($data['quoted_yield'])
                    <tr>
                        <td width="30%">Quoted Yield:</td>
                        <td>{!! $data['quoted_yield'] !!}%</td>
                    </tr>
                    @endif
                    <tr>
                        <td width="30%">Taxable:</td>
                        @if($data['taxable'])
                        <td>true</td>
                        @else
                        <td>False</td>
                        @endif
                    </tr>

                    @if($data['tax_rate'])
                    <tr>
                        <td width="30%">Tax Rate:</td>
                        <td>{!! $data['tax_rate'] !!}%</td>
                    </tr>
                    @endif
                    <tr>
                        <th width="30%">Interest Payment Dates</th>
                        <td></td>
                    </tr>
                    @if($data['interest_payment_dates'])
                        @foreach($data['interest_payment_dates'] as $interestDates)
                        <tr>
                            <td scope="row" width="30%">{{ $loop->iteration }}</td>
                            <td>{!! $interestDates['date'] !!}</td>
                        </tr>
                        @endforeach
                    @endif

                </tbody>
            </table>
        </div>
    </div>
</div>