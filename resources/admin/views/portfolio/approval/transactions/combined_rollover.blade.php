<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">

            <h3>Investments to be Rolled over</h3>

            <table class="table table-responsive table-hover">
                <thead>
                <tr><td>Value Date</td><td>Amount</td><td>Maturity date</td><td>Value</td><td>Description</td></tr>
                </thead>
                <tbody>
                @foreach($investments  as $investment)
                    <tr>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->value) !!}</td>
                        <td>{!! $investment->description !!}</td>
                    </tr>
                @endforeach
                <tr>
                    <th>Total</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('amount')) !!}</th>
                    <th></th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value')) !!}</th>
                    <th></th>
                </tr>

                @if($data['reinvest'] == 'topup')
                    <tr class="success"><td>Topup</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td><td></td></tr>
                    <tr><td>Total Value of new Investment</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'] + $investments->sum('value')) !!}</td><td></td></tr>
                @elseif($data['reinvest'] == 'reinvest')
                    <tr class="danger"><td>Withdraw</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value') - $data['amount'] ) !!}</td><td></td></tr>
                    <tr class="success"><td>Reinvest</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td><td></td></tr>
                @elseif($data['reinvest'] == 'withdraw')
                    <tr class="danger"><td>Withdraw</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td><td></td></tr>
                    <tr class="success"><td>Reinvest</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value') - $data['amount'] ) !!}</td><td></td></tr>
                @endif
                </tbody>
            </table>

            <h3>New Investment Details</h3>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                    <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td></tr>
                    <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td></tr>
                    <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>


</div>