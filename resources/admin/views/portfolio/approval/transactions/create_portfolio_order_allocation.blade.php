<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Portfolio Order Allocations</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td colspan="2">{!! $portfolioOrder->security->name !!}</td>
                    </tr>

                    <tr>
                        <td>Asset Class</td>
                        <td colspan="2">{!! $asset_class = $portfolioOrder->security->subAssetClass->assetClass->name !!}</td>
                    </tr>

                    <tr>
                        <td>Portfolio Order Type</td>
                        <td colspan="2">{!! $portfolioOrder->type->name !!}</td>
                    </tr>

                    @if($asset_class == 'Equities')
                        <tr>
                            <td>No of shares ordered</td>
                            <td colspan="2">{!! $portfolioOrder->unitFundOrders->sum('shares') !!}</td>
                        </tr>

                        <tr>
                            <td>Cut off price</td>
                            <td colspan="2">{!! $portfolioOrder->cut_off_price !!}</td>
                        </tr>
                        @if($allocations)
                            <tr>
                                <th width="25%"></th>
                                <th>No of shares</th>
                                <th>Price</th>
                            </tr>

                            @foreach($allocations as $allocation)
                                <tr>
                                    <th width="25%"></th>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation['shares'], 0, false) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation['price']) !!}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <th width="25%">Totals</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($allocations->sum('shares'), 0, false) !!}</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($allocations->sum('price')) !!}</th>
                                </tr>

                        @endif
                    @endif

                    @if($asset_class == 'Deposits' || $asset_class == 'Bonds')
                        <tr>
                            <td>Amount ordered</td>
                            <td  colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($portfolioOrder->unitFundOrders->sum('amount')) !!}</td>
                        </tr>

                        <tr>
                            <td>Minimum rate</td>
                            <td  colspan="2">{!! $portfolioOrder->minimum_rate !!}</td>
                        </tr>

                        @if($allocations)
                            <tr>
                                <th width="25%"></th>
                                <th>Amount</th>
                                <th>Rate</th>
                            </tr>

                            @foreach($allocations as $allocation)
                                <tr>
                                    <th width="25%"></th>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation['amount'], 0, false) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($allocation['rate']) !!}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th width="25%">Totals</th>
                                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($allocations->sum('amount'), 0, false) !!}</th>
                                <th></th>
                            </tr>

                        @endif
                    @endif

                </tbody>
            </table>
        </div>
    </div>
</div>