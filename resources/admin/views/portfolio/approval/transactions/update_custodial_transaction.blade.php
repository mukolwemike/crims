<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Update Custodial Transaction</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                <tr><th>Item</th><th>Was</th><th>Now</th></tr>
                </thead>
                <tbody>
                <tr><td>Custodial Account</td><td>{!! $old->custodialAccount->account_name !!}</td><td>{!! App\Cytonn\Models\CustodialAccount::findOrFail($new->custodial_id)->account_name !!}</td></tr>
                <tr>
                    <td>Type</td>
                    <td>
                        @if($old->amount < 0) Credit @else Debit @endif
                    </td>
                    <td>{!! ucfirst($new->type) !!}</td>
                </tr>
                <tr><td>Type Name</td><td>{!! $old->typeName() !!}</td><td>{!! $new->type_name !!}</td></tr>
                <tr><td>Type Description</td><td>{!! App\Cytonn\Models\CustodialTransactionType::where('name', $old->typeName())->first()->description !!}</td><td>{!! App\Cytonn\Models\CustodialTransactionType::where('name', $new->type_name)->first()->description !!}</td></tr>
                <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($old->amount)) !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($new->amount) !!}</td></tr>
                <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($old->date) !!}</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($new->date) !!}</td></tr>
                <tr><td>Narrative</td><td>{!! explode(":", $old->description)[1] !!}</td><td>{!! $new->narrative !!}</td></tr>

                </tbody>
            </table>
        </div>
    </div>
</div>