<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <th></th>
                    <th>Suspense Transaction Details</th>
                    <th>Custodial Transaction Details</th>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($suspenseTransaction->date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($custodialTransaction->date) !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{!! $suspenseTransaction->custodialAccount->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency($suspenseTransaction->amount) !!}</td>
                    <td>{!! $custodialTransaction->custodialAccount->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency($custodialTransaction->amount) !!}</td>
                </tr>
                <tr>
                    <td>Account</td>
                    <td>{!! $suspenseTransaction->custodialAccount->account_name !!}</td>
                    <td>{!! $custodialTransaction->custodialAccount->account_name !!}</td>
                </tr>
                <tr>
                    <td>Outgoing Reference</td>
                    <td>{!! $suspenseTransaction->outgoing_reference !!}</td>
                    <td>{!! $custodialTransaction->outgoing_reference !!}</td>
                </tr>
                <tr>
                    <td>Transaction Id</td>
                    <td>{!! $suspenseTransaction->transaction_id !!}</td>
                    <td>{!! $custodialTransaction->bank_transaction_id !!}</td>
                </tr>
                <tr>
                    <td>Transaction Type</td>
                    <td></td>
                    <td>{!! $custodialTransaction->transactionType->description !!}</td>
                </tr>
                <tr>
                    <td>Bank Reference No</td>
                    <td>{!! $suspenseTransaction->bank_reference !!}</td>
                    <td>{!! $custodialTransaction->bank_reference_no !!}</td>
                </tr>
                <tr>
                    <td>Client</td>
                    <td></td>
                    <td>{!! $custodialTransaction->client ? \Cytonn\Presenters\ClientPresenter::presentFullNames($custodialTransaction->client_id) : '' !!}</td>
                </tr>
                @if($approval->suspenseTransaction)
                    <tr>
                        <td>Suspense Transaction</td>
                        <td colspan="2">
                            <a class="btn btn-sm btn-info margin-bottom-10"
                               href="/dashboard/portfolio/suspense-transactions/details/{!! $approval->suspenseTransaction->id !!}">View Suspense Transaction</a>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>