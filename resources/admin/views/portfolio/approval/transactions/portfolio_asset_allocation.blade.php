<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Portfolio Asset Allocation</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{!! $unitFund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Asset Class</td>
                        <td>{!! $subAssetClass->assetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td>Sub Asset Class</td>
                        <td>{!! $subAssetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td>Limit</td>
                        <td>{!! $data['limit'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>