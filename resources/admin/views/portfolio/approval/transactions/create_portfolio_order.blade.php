<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $security->name !!}</td>
                    </tr>

                    <tr>
                        <td>Portfolio Order Type</td>
                        <td>{!! $orderType->name !!}</td>
                    </tr>

                    @if($security->subAssetClass->assetClass->name == 'Equities')
                        <tr>
                            <td> Cut off price </td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['cut_off_price']) !!}</td>
                        </tr>
                    @elseif($security->subAssetClass->assetClass->name == 'Deposits' || $security->subAssetClass->assetClass->name == 'Bonds')
                        <tr>
                            <td> Cut off price </td>
                            <td>{!! isset($data['minimum_rate']) ? \Cytonn\Presenters\AmountPresenter::currency($data['minimum_rate']) : '' !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <br>

            <table class="table table-hover table-responsive table-striped">
                @if($fund_orders)
                    <thead>
                        <tr>
                            <th>Unit Fund</th>
                            <th>Account</th>
                            @if($security->subAssetClass->assetClass->name == 'Deposits' || $security->subAssetClass->assetClass->name == 'Bonds')
                                <th>Amount</th>
                            @elseif($security->subAssetClass->assetClass->name == 'Equities')
                                <th>No of shares</th>
                                <th>Value of shares</th>
                            @endif
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($fund_orders as $fund_order)
                        <tr>
                            <td>{!! $fund_order['unitFund']->name !!}</td>
                            <td>{!! isset($fund_order['account']) ? $fund_order['account']->account_name : ''!!}</td>
                            @if($security->subAssetClass->assetClass->name == 'Deposits' || $security->subAssetClass->assetClass->name == 'Bonds')
                                <td>{!! $value = \Cytonn\Presenters\AmountPresenter::currency($fund_order['amount']) !!}</td>
                            @elseif($security->subAssetClass->assetClass->name == 'Equities')
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund_order['shares'], true, 0) !!}</td>
                                <td>{!! $value = \Cytonn\Presenters\AmountPresenter::currency($fund_order['shares'] * $data['cut_off_price']) !!}</td>
                            @endif
                            <td>
                                @if($fund_order['status'])
                                    <el-alert
                                            title="{{ $fund_order['status'][0]->description }}"
                                            type="warning"
                                            show-icon>
                                    </el-alert>
                                    {{--<el-tag type="danger" size="mini" icon>{{ $fund_order['status'][0]->reason }}</el-tag>--}}
                                @else
                                    <el-tag type="success" size="mini">Compliant</el-tag>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</div>