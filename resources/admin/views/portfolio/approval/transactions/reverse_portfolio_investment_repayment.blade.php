<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Reverse Portfolio Investment Repayment</h4>
        </div>

        <div class = "panel-body">

            <h3>Reason</h3>

            {!! $data['reason'] !!}

            <h3>Investment Details</h3>
            <table class = "table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Institution</td>
                    <td>{!! $portfolioInvestor->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($repayment->date) !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($repayment->amount) !!}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{!! $repayment->narrative !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
