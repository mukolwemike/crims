<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Edit Portfolio Order</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td colspan="2">{!! $security->name !!}</td>
                    </tr>

                    <tr>
                        <td>Portfolio Order Type</td>
                        <td colspan="2">{!! $orderType->name !!}</td>
                    </tr>

                    @if($security->subAssetClass->assetClass->name == 'Equities')
                        <tr>
                            <td> Cut off price </td>
                            <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($data['cut_off_price']) !!}</td>
                        </tr>
                    @elseif($security->subAssetClass->assetClass->name == 'Deposits' || $security->subAssetClass->assetClass->name == 'Bonds')
                        <tr>
                            <td> Minimum Rate </td>
                            <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($data['minimum_rate']) !!}</td>
                        </tr>
                    @endif

                    <tr><td colspan="3"></td></tr>
                    <tr>
                        <th colspan="3">Before</th>
                    </tr>

                    @if($prev_fund_orders)
                        <tr>
                            <th>Fund</th>
                            <th>Account</th>
                            <th>Amount</th>
                        </tr>
                        @foreach($prev_fund_orders as $fund_order)
                            <tr>
                                <td>{!! $fund_order->unitFund->name !!}</td>
                                <td>{!! $fund_order->custodialAccount->account_name !!}</td>
                                @if($security->subAssetClass->assetClass->name == 'Equities')
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund_order->shares) !!}</td>
                                @else
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund_order->amount) !!}</td>
                                @endif
                            </tr>
                        @endforeach
                    @endif

                    <tr><td colspan="3"></td></tr>
                    <tr>
                        <th colspan="3">After</th>
                    </tr>

                    @if($fund_orders_now)

                        <tr>
                            <th>Fund</th>
                            <th>Account</th>
                            <th>Amount</th>
                        </tr>
                        @foreach($fund_orders_now as $fund_order)
                            <tr>
                                <td>{!! $fund_order['fund'] !!}</td>
                                <td>{!! $getCustodialAccountName($fund_order['custodial_account_id']) !!}</td>
                                @if($security->subAssetClass->assetClass->name == 'Equities')
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund_order['shares']) !!}</td>
                                @else
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund_order['amount']) !!}</td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>