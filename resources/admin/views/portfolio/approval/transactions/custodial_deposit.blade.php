<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Custodial Deposit by Principal Partner Details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $account->account_name !!}</td>
                    </tr>
                    @if($transactionType)
                        <tr>
                            <td>Deposit Type</td>
                            <td>{!! $transactionType->description !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Narrative</td>
                        <td>{!! $data['narrative'] !!}</td>
                    </tr>
                    @if($approval->suspenseTransaction)
                        <tr>
                            <td>Suspense Transaction</td>
                            <td>
                                <a class="btn btn-sm btn-info margin-bottom-10"
                                   href="/dashboard/portfolio/suspense-transactions/details/{!! $approval->suspenseTransaction->id !!}">View Suspense Transaction</a>
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
   </div>
</div>