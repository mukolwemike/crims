<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Link Receiving Account</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Custodial Account</th>
                        <th>Category</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $account->account_name !!}</td>
                        @if($product)
                            <td>Product</td>
                        @elseif($project)
                            <td>Project</td>
                        @elseif($shares)
                            <td>Share Entity</td>
                        @elseif($fund)
                            <td>Unit Fund</td>
                        @endif

                        @if($product)
                            <td>{!! $product->description !!}</td>
                        @elseif($project)
                            <td>{!! $project->name !!}</td>
                        @elseif($shares)
                            <td>{!! $shares->name !!}</td>
                        @elseif($fund)
                            <td>{!! $fund->name !!}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>