<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Remove portfolio order</h4>
        </div>

        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Security</td>
                    <td>{!! $security->name !!}</td>
                </tr>

                <tr>
                    <td>Portfolio Order Type</td>
                    <td>{!! $portfolioOrder->type->name !!}</td>
                </tr>

                @if($security->asset_class = 'Equities')
                    <tr>
                        <td> Cut off price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($portfolioOrder->cut_off_price) !!}</td>
                    </tr>
                @elseif($security->asset_class = 'Equities' || $security->asset_class = 'Bonds')
                    <tr>
                        <td> Cut off price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($portfolioOrder->minimum_rate) !!}</td>
                    </tr>
                @endif

                @if($fund_orders)
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Fund</th>
                        <th>Amount</th>
                    </tr>
                    @foreach($fund_orders as $fund_order)
                        <tr>
                            <td>{!! $fund_order->unitFund->name !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund_order->amount) !!}</td>
                        </tr>
                    @endforeach
                @endif

                <tr>
                    <td>Reason</td>
                    <td>{!! $data['reason'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>