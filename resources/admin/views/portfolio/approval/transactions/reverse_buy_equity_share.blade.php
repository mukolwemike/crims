<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                @if($equityHolding->trashed())
                    <tr class="danger">
                        <td colspan="2">The share purchase has already been reversed</td>
                    </tr>
                @endif
                <tr>
                    <td>Security</td>
                    <td>{!! $security->name !!}</td>
                </tr>
                <tr>
                    <td>Sub Asset Class</td>
                    <td>{!! $security->subAssetClass->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->date) !!}</td>
                </tr>
                <tr>
                    <td>Number of Shares Bought</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->number, false, 0) !!}</td>
                </tr>
                <tr>
                    <td>Cost per share</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->cost, false, 4) !!}</td>
                </tr>
                <tr>
                    <td>Reverse Reason</td>
                    <td>{!! $data['reason'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>