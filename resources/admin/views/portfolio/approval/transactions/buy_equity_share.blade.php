<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Security</td>
                        <td>{!! $security->name !!}</td>
                    </tr>
                    <tr>
                        <td>Sub Asset Class</td>
                        <td>{!! $security->subAssetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    @if(isset($data['settlement_date']))
                    <tr>
                        <td>Settlement Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['settlement_date']) !!}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>Number of Shares Bought</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], false, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Cost per share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['cost'], false, 4) !!}</td>
                    </tr>
                    <tr>
                        <td>Fund</td><td>@if($fund) {{ $fund->name }} @endif</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>