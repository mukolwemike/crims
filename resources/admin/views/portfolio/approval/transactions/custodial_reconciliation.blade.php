<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Custodial Reconciliation Details</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $account->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>{!! ucfirst($data['type']) !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Narrative</td>
                        <td>{!! $data['narrative'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
   </div>
</div>