@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <h5>Summary</h5>
        <table class="table table-responsive table-striped table-hover">
            <tbody>
            <tr>
                <td>Batch Name</td><td>{{ $batch->name }}</td>
            </tr>
            <tr>
                <td>Approval Stage</td><td>{{ $stage->name }}</td>
            </tr>
            </tbody>
        </table>

        <h5>Transactions</h5>

        <div ng-controller="BatchApprovalController" data-ng-init="approvalType = 'portfolio'">
            {{ Form::open() }}
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>{{ Form::checkbox('all', false, false, ['id'=>'select_all']) }}</th>
                    <th>Transaction</th>
                    @foreach($columns as $column)
                        <th>{{ $column->name }}</th>
                    @endforeach
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                @foreach($transactions as $transaction)
                    <tr>
                        <td class="select">{{ Form::checkbox('select_'.$transaction->id, 1, false, ['ng-model'=>'selected["'.$i.'"]', 'index'=>$i, 'ng-true-value'=>'"'.$transaction->id.'"', 'ng-false-value'=>"null"]) }}</td>
                        <td>{{ ucfirst(str_replace('_', ' ', $transaction->transaction_type))  }}</td>
                        @foreach($columns as $column)
                            <td>{{ $transaction->handler()->get($column->slug) }}</td>
                        @endforeach
                        <td>
                            <button id="status{{ $transaction->id }}" type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Not approved"><i class="fa fa-info"></i></button>
                        </td>
                        <td style="min-width: 115px;">
                            <!-- Split button -->
                            <div class="btn-group">
                                <a href="/dashboard/portfolio/approve/{{ $transaction->id }}" type="button" class="btn btn-info btn-xs">Details</a>
                                <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="/dashboard/portfolio/approve/{{ $transaction->id }}/stage/{{ $stage->id }}">Approve</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#rejectModal{{ $transaction->id }}">Reject</a></li>
                                </ul>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="rejectModal{{ $transaction->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {{ Form::open() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                {{ Form::label('reason', 'Give a comment for rejecting transaction') }}

                                                {{ Form::text('reason', null, ['class'=>'form-control', 'id'=>'reject_field'.$transaction->id, 'placeholder'=>'Give a reason for rejecting']) }}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a href="#" class="btn btn-success" ng-click="reject($event, '{{ $transaction->id }}', '{{ $stage->id }}')">Submit <i ng-show="rejecting" class="fa fa-spinner fa-pulse fa-fw"></i></a>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php $i++ ?>
                @endforeach
                <tr><td colspan="50%"><a href="#" ng-click="submit($event, '{{ $stage->id }}')" class="btn btn-success">Approve <i ng-show="submitting" class="fa fa-spinner fa-pulse fa-fw"></i></a></td><td colspan="50%"></td></tr>
                </tbody>
            </table>
            {{ Form::close() }}
        </div>
    </div>
@endsection