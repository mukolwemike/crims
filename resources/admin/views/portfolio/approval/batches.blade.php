@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-dashboard">
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Batch Name</th><th>Stage</th><th>Transactions</th><td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($batches as $batch)
                        <tr>
                            <td>{{ $batch->batch->name }}</td>
                            <td>{{ $batch->stage->name }}</td>
                            <td>{{ $batch->transaction_count }}</td>
                            <td><a href="/dashboard/portfolio/approvals/batch/{{ $batch->stage->id }}/{{ $batch->batch->id }}" class="btn btn-success btn-sm">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection