@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard" ng-controller = "approvePortfolioInvCtrl">
            <div class = "detail-group">
                <h3>Approval Request Details</h3>

                <div>
                    <table class = "table table-hover table-striped table-responsive">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>Institution</td>
                                <td>
                                    {!! \Cytonn\Presenters\InstitutionPresenter::presentName($approval->institution_id) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Transaction type</td>
                                <td>{!! ucfirst(str_replace('_', ' ', $approval->transaction_type)) !!}</td>
                            </tr>
                            <tr>
                                <td>Sent by</td>
                                <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($approval->sent_by) !!}</td>
                            </tr>
                            <tr>
                                <td>Sent on</td>
                                <td>{!! $approval->created_at->toRfc850String() !!}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    @if($approval->approved)
                                        <table class="table table-responsive">
                                            <thead><tr><td>Stage</td><td>Status</td><td>Approved By</td><td>
                                                    Approved On</td></tr></thead>
                                            <tbody>
                                            @foreach($approval->steps as $approvalStep)
                                                <tr>
                                                    <td>{{ $approvalStep->stage->name }}</td>
                                                    <td><span class="label label-success">Approved</span></td>
                                                    <td>{{ \Cytonn\Presenters\UserPresenter::presentFullNames($approvalStep->approver->id) }}
                                                    </td>
                                                    <td>{{ $approvalStep->created_at->toRfc850String() }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <table class="table table-responsive">
                                            <thead><tr><td>Stage</td><td>Status</td><td>Approved By</td>
                                                <td>Approved On</td></tr></thead>
                                            <tbody>
                                            @foreach($approval->type()->allStages() as $approvalStage)
                                                <tr>
                                                    <?php $approvedStep = $approved_step($approval, $approvalStage); ?>
                                                    <td>{{ $approvalStage->name }}</td>
                                                    <td>
                                                        @if($approvedStep) <span class="label label-success">
                                                            Approved</span>
                                                        @else
                                                            <span class="label label-danger">Awaiting Approval</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($approvedStep)
                                                            {{ \Cytonn\Presenters\UserPresenter::presentFullNames($approvedStep->approver->id) }}
                                                        @else - @endif</td>
                                                    <td>
                                                        @if($approvedStep)
                                                            {{ $approvedStep->created_at->toRfc850String() }}
                                                        @else - @endif</td>
                                                </tr>
                                            @endforeach
                                            @foreach($rejections as $rejection)
                                                <tr>
                                                    <td>Rejected</td>
                                                    <td>
                                                        By: {{ \Cytonn\Presenters\UserPresenter::presentFullNames($rejection->reject_by) }}
                                                    </td>
                                                    <td colspan="2">
                                                        {{ $rejection->reason }}
                                                        <a href="/dashboard/investments/approve/{{ $approval->id }}/resolve/{{ $rejection->id }}"
                                                           class="btn btn-info pull-right">Resolve</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            @if(View::exists($view_folder.'.'.$approval->transaction_type))
                @include($view_folder.'.'.$approval->transaction_type)
                @include('portfolio.approval.partials.actions')
            @else
                <div class="alert alert-danger"><p>Transaction type is not yet supported</p></div>
            @endif
        </div>
    </div>
@endsection