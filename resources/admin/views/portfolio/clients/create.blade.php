@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        {!! Form::open(['route' => ['portfolio.client.store', $institution->id]]) !!}

        <div class="detail-group">
            <h4 class="col-md-12">Client Details</h4>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">{!! Form::label('client_type_id', 'Client type') !!}</div>
                    <div class="col-md-4">{!! Form::select('client_type_id', $clientTypes , null, ['class'=>'form-control', 'init-model'=>'client_type']) !!}</div>
                </div>
            </div>

            <div class="col-md-12" ng-show="client_type == '1'">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::label('name', 'Full Name') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::select('title_id',$titles, null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}
                    </div>

                    <div class="col-md-3">{!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'First Name'])  !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>

                    <div class="col-md-2">{!! Form::text('middlename', null, ['class'=>'form-control', 'placeholder'=>'Middle Name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}</div>

                    <div class="col-md-3">{!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Last Name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>
                </div>
            </div>

            <div class="col-md-6" ng-hide="client_type == '1'">
                <div class="row">
                    <div class="col-md-4">{!! Form::label('corporate_registered_name', 'Registered name') !!}</div>

                    <div class="col-md-8">{!! Form::text('corporate_registered_name', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_registered_name') !!}
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        {!! Form::label('email', 'Email') !!}
                    </div>

                    <div class="col-md-8">{!! Form::text('email', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>
            </div>
        </div>

        <div class="detail-group" ng-hide="client_type == '1'">
            <h4 class="col-md-12">Company contact person</h4>

            <div class="col-md-12">
                <div class="col-md-2"> <span class="required-field">{!! Form::label('contact_person', 'Contact person') !!}</span></div>

                <div class="col-md-2">{!! Form::select('contact_person_title', $titles , null, ['class'=>'form-control', 'placeholder'=>'Title']) !!}</div>

                <div class="col-md-4">{!! Form::text('contact_person_lname', null, ['class'=>'form-control', 'placeholder'=>'Last name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'surname') !!}</div>


                <div class="col-md-4">{!! Form::text('contact_person_fname', null, ['class'=>'form-control', 'placeholder'=>'First name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div ng-controller="ClientUserCtrl">
            <div class="detail-group">
                <h4 class="col-md-12">Link to existing client</h4>
                <br/><br/>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::label('new_client', 'New or Existing Client') !!}
                        </div>

                        <div class="col-md-8">
                            {!! Form::select('new_client', [1 => 'New Client', 0 => 'Existing Client', 2 => 'Duplicate Client'], null, ['class'=>'form-control',  'ng-model' => 'existing_client']) !!}
                        </div>

                        <div class="col-md-8">
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}</div>
                    </div>
                </div>
            </div>
            <div class="row" ng-show="existing_client == '0'">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Search Client by Name</label>
                        <input type="text" ng-model="selectedClient" placeholder="Enter the client name"
                               uib-typeahead="client as client.fullName for client in searchClients($viewValue)"
                               class="form-control" uib-typeahead-loading="loading" uib-typeahead-show-hint="true"
                               uib-typeahead-min-length="1" uib-typeahead-no-results="noResults">
                        <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <div class="hide">
                            <input name="client_id" type="text" ng-model="selectedClient.id">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h4>Selected client</h4>
                        <table class="table table-responsive">
                            <thead></thead>
                            <tbody>
                            <tr>
                                <td>Client Name</td>
                                <td><% selectedClient.fullName %></td>
                            </tr>
                            <tr>
                                <td>Client code</td>
                                <td><% selectedClient.client_code %></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><% selectedClient.email %></td>
                            </tr>
                            <tr>
                                <td>Client Type</td>
                                <td><% selectedClient.type %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="detail-group">
                <div class="col-md-12">
                    {!! Form::submit('Save Client', ['class'=>'btn btn-success']) !!}
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
    </div>
@endsection