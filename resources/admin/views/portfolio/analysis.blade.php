@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    @foreach($currencies as $currency)
                        <li role="presentation" class=""><a href="#{!! $currency->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $currency->name !!}</a></li>
                    @endforeach
                </ul>
                <div id="myTabContent" class="tab-content">
                    @foreach($currencies as $currency)
                        <div role="tabpanel" class="tab-pane fade" id="{!! $currency->id !!}" aria-labelledby="home-tab">
                            <div class="panel panel-success margin-top-20" ng-controller="DepositAnalysisGridController">
                                <div class="panel-heading">
                                    <h3>Deposit Analysis: {!! $currency->name !!}</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped table-responsive table-hover" st-pipe="callServer" st-table="displayed" ng-model="currency_id" ng-init="currency_id = {!! $currency->id !!}">
                                        <thead>
                                            <tr>
                                                <th colspan="6"></th>
                                                <th ng-controller="DatepickerCtrl">
                                                    <input type="date" st-search="date" class="form-control" placeholder="As at date..." />
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Fund Type</th>
                                                <th>Principal</th>
                                                <th>Gross Interest</th>
                                                <th>Adjusted GI (AGI)</th>
                                                <th>Adjusted Market Value (P + AGI)</th>
                                                <th>Market Value</th>
                                                <th>Percentage</th>
                                            </tr>
                                        </thead>
                                        <tbody  ng-show="!isLoading">
                                        <tr ng-repeat="row in displayed">
                                            {{--<td><a href="/dashboard/portfolio/summary/<%row.id%>"><% row.fund_type %></a></td>--}}
                                            <td><% row['Name'] %></td>
                                            <td><% row['Cost value'] | currency:"" %></td>
                                            <td><% row['Gross Interest'] | currency:"" %></td>
                                            <td><% row['Adjusted Gross Interest'] | currency:"" %></td>
                                            <td><% row['Adjusted Market Value'] | currency:"" %></td>
                                            <td><% row['Market Value'] | currency:"" %></td>
                                            <td><% row['Exposure'] | currency:"" %>%</td>
                                        </tr>
                                        <tr>
                                            <th>Totals</th>
                                            <th><% total['Cost value'] | currency:"" %></th>
                                            <th><% total['Gross Interest'] | currency:"" %></th>
                                            <th><% total['Adjusted Gross Interest'] | currency:"" %></th>
                                            <th><% total['Adjusted Market Value'] | currency:"" %></th>
                                            <th><% total['Market Value'] | currency:"" %></th>
                                            <th><% total['Exposure'] | currency:"" %>%</th>
                                        </tr>
                                        </tbody>
                                        <tbody ng-show="isLoading">
                                        <tr>
                                            <td colspan="100%" class="text-center">Loading ... </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection