@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <portfolio-fund-summary :manager="{{ $fundManager }}"></portfolio-fund-summary>
        </div>
    </div>
@stop