@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <portfolio-fund-pricing-summary :manager="{{ $fundManager }}" :fund="{{ $unit_fund_id }}"></portfolio-fund-pricing-summary>
        </div>
    </div>
@stop