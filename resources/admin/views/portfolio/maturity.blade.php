@extends('layouts.default')

@section('content')

    <div class="panel-dashboard">
        <div class="col-md-4">
            <a class="btn btn-success margin-top-25" href="/dashboard/portfolio/investments/maturity/analysis">Maturity Analysis</a>
        </div>
        <div class="col-md-8 pull-right">
            {!! Form::open() !!}

            <div class="col-md-4 table-search-checkbox">
                <div class="form-group">
                    {!! Form::checkbox('before', true, $before) !!}

                    {!! Form::label('before', 'Include before date') !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Maturities before date') !!}

                    {!! Form::text('date', $date, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                </div>
            </div>

            <div class="col-md-4">
                {!! Form::submit('Search', ['class'=>'btn btn-success margin-top-25']) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <table class="table table-responsive table-striped table-hover">
            <thead>
                <tr>
                    <th>Institution</th><th>Invested Date</th><th>Maturity Date</th><th>Invested Amount</th><th>Interest Rate</th>
                </tr>
            </thead>
            <tbody>
                <?php $total = 0; ?>
                @foreach($investments as $investment)
                    <?php
                        $amt = $investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date, true);
                        $total += $amt;
                    ?>
                    <tr>
                        <td>{!! $investment->security->name !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($amt) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Total</th><td colspan="2"></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total)  !!}</th><td></td>
                </tr>
            </tfoot>
        </table>
    </div>

@endsection