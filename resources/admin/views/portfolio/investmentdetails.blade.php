@extends('layouts.default')

@section('content')
    <div>
        <div class = "panel-dashboard">
            <h3>{!! $investment->security->investor->name !!}</h3>

            <div class="col-md-6">
                @include('portfolio.partials.investmentdetails')
            </div>

            <div class="detail-group">
                <h4>Actions</h4>

                <div class="margin-bottom-20">
                    <a href="/dashboard/portfolio/{!! $investment->id !!}/repay" class="btn btn-success">Repayments</a>
                    @if(!$investment->withdrawn)
                        <a class = "btn btn-primary btn-danger" href = "/dashboard/portfolio/investments/redeem/{!!$investment->id!!}">Redemption</a>

                        @if((new \Carbon\Carbon($investment->maturity_date))->isPast())
                            <a class = "btn btn-primary btn-warning" href = "/dashboard/portfolio/investments/rollover/{!!$investment->id!!}">Rollover</a>
                        @else
                            <a class = "btn btn-primary btn-warning disabled" disabled href = "#">Rollover</a>
                        @endif

                        <a class="btn btn-info" href="/dashboard/portfolio/investments/edit/{!! $investment->id !!}">Edit</a>
                    @else
                        <a class = "btn btn-primary btn-danger disabled" href = "#">Redemption</a>

                        <a class = "btn btn-primary btn-warning disabled" role="button" href = "#">Rollover</a>

                        <a class="btn btn-info disabled" href="#">Edit</a>
                    @endif
                </div>

                <div ng-controller="CollapseCtrl">
                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">More Actions</button>
                    <hr>
                    <div uib-collapse="isCollapsed">
                        @if($investment->withdrawn)
                            <a class="btn btn-danger margin-bottom-10" href="#" data-toggle="modal" data-target="#reverse-withdraw-modal">Reverse Withdrawal</a>
                        @else
                            <a class="btn btn-danger margin-bottom-10" href="#" data-toggle="modal" data-target="#rollback-modal">Rollback Investment</a>
                            <a class="btn btn-info margin-bottom-10" href="/dashboard/portfolio/investments/send-instructions/{!! $investment->id !!}">Deposit Placement Instructions</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


<!--Rollback Investment Modal -->
<div class="modal fade" id="rollback-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['portfolio_reverse', $investment->id ]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure you want to Rollback Investment?</h4>
            </div>
            <div class="modal-body">
                <p>The investment will be removed and all affected records undone.</p>

                <div class="form-group">
                    {!! Form::label('reason', 'Please provide a reason') !!}

                    {!! Form::text('reason', null, ['class'=>'form-control', 'placeholder'=>'Reason', 'required']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Proceed to Rollback</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Reverse Withdrawal Modal -->
<div class="modal fade" id="reverse-withdraw-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['portfolio_rollback_withdraw', $investment->id ]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure you want to reverse withdrawal/rollover of this investment</h4>
            </div>
            <div class="modal-body">
                <p>The withdrawal/rollover will be undone</p>

                <div class="form-group">
                    {!! Form::label('reason', 'Please provide a reason') !!}

                    {!! Form::text('reason', null, ['class'=>'form-control', 'placeholder'=>'Reason', 'required']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Proceed to Rollback</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
