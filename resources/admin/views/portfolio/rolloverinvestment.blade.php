@extends('layouts.default')

@section('content')
    <div class = "col-md-8 col-md-offset-2">
        <div class = "panel-dashboard" ng-controller = "portfolioRolloverCtrl">
            <h1>Rollover Investment</h1>
            @include('portfolio.partials.investmentdetails')

            <a href="/dashboard/portfolio/combinedrollover/{!! $deposit->id !!}" class="btn btn-success">Combine with maturing investments and Rollover</a>

            <div class = "detail-group">
                {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}

                {!! Form::hidden('investment', Crypt::encrypt($deposit->id)) !!}

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('invested_date', 'Enter the date Investment resumes') !!}

                    {!! Form::date('invested_date', null, ['class'=>'form-control', 'id'=>'invested_date', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                </div>

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('maturity_date', 'Enter the new maturity date') !!}

                    {!! Form::date('maturity_date', null, ['class'=>'form-control', 'id'=>'maturity_date', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('amount', 'Enter the amount to reinvest') !!}

                    {!! Form::number('amount', null, ['step'=>'0.01', 'class'=>'form-control','init-model'=>'amount']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}

                    {!! Form::number('interest_rate', null, ['class'=>'form-control','init-model'=>'interest_rate', 'step'=>'0.0001']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('taxable', 'Tax Rate') !!}
                    {!! Form::select('taxable', [true=>'Taxable', false=>'Zero rated'], null, ['class'=>'form-control', 'init-model'=>'taxable']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('description', 'Transaction Description') !!}

                    {!! Form::textarea('description', null, ['class'=>'form-control','init-model'=>'description', 'rows'=>2]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                </div>

                {!! Form::submit('Rollover', ['class'=>'btn btn-success']) !!}

                {!! Form::close() !!}

                <div ng-view></div>
                <script type = "text/ng-template" id = "portfolioRollover.htm">
                    <div class = "modal fade">
                        <div class = "modal-dialog">
                            <div class = "modal-content">

                                <div class = "modal-header no-bottom-border">
                                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                                    <h4 class = "modal-title">Confirm Investment Rollover</h4>
                                </div>
                                <div class = "modal-body">
                                    <div class = "detail-group">
                                        <p>Are you sure you want to rollover this investment with the details below?</p>
                                        <table class = "table table-hover table-responsive table-striped">
                                            <tbody>
                                            <tr>
                                                <td>Institution</td>
                                                <td>{!! $deposit->security->investor->name !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Investment Type</td>
                                                <td>{!! $deposit->security->subAssetClass->assetClass->name !!}</td>
                                            </tr>
                                            <tr>
                                                <td>New investment date</td>
                                                <td><% investedDate %></td>
                                            </tr>
                                            <tr>
                                                <td>New maturity date</td>
                                                <td><% maturityDate %></td>
                                            </tr>
                                            <tr>
                                                <td>Rollover amount</td>
                                                <td><% amount | currency:"" %></td>
                                            </tr>
                                            <tr>
                                                <td>Interest Rate</td>
                                                <td><% interestRate %></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction Description</td>
                                                <td><% description %></td>
                                            </tr>
                                            <tr></tr>
                                            <tr ng-class="taxable_class">
                                                <td>Taxable</td>
                                                <td><% taxable_text %></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class = "detail-group">
                                        <div class = "pull-right">
                                            {!! Form::open(['route'=>'portfolio_rollover']) !!}
                                            <div style="display: none !important;">
                                                {!! Form::text('deposit', Crypt::encrypt($deposit->id)) !!}
                                                {!! Form::text('invested_date',null, ['ng-model'=>'investedDate']) !!}
                                                {!! Form::text('maturity_date', null, ['ng-model'=>'maturityDate']) !!}
                                                {!! Form::text('amount', null, ['ng-model'=>'amount']) !!}
                                                {!! Form::text('interest_rate', null, ['ng-model'=>'interestRate']) !!}
                                                {!! Form::text('taxable', null, ['ng-model'=>'taxable']) !!}
                                                {!! Form::text('description', null, ['ng-model'=>'description']) !!}
                                            </div>
                                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                                    data-dismiss = "modal">No
                                            </button>
                                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                            {!! Form::close() !!}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </script>

            </div>
        </div>
    </div>
@stop