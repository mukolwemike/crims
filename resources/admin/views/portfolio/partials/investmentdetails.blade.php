<div class = "detail-group">
    <table class = "table table-responsive table-hover">
        <thead></thead>
        <tbody>
        <tr>
            <td>Institution</td>
            <td>{!! $deposit->security->investor->name !!}</td>
        </tr>
        <tr>
            <td>Fund Type</td>
            <td>{!! $deposit->security->subAssetClass->assetClass->name !!}</td>
        </tr>
        @if($deposit->type)
            <tr>
                <td>Portfolio Investment Type</td>
                <td>{!! $deposit->type->name !!}</td>
            </tr>
        @else
            <tr>
                <td>Portfolio Investment Type</td>
                <td>No investment type specified</td>
            </tr>
        @endif
        @if($deposit->invest_transaction_approval_id)
            <tr>
                <td>Investment Transaction</td>
                <td><a href="/dashboard/portfolio/approve/{!! $deposit->invest_transaction_approval_id !!}">See investment transaction approval</a></td>
            </tr>
        @endif
        <tr>
            <td>Principal</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->principal()) !!}</td>
        </tr>
        <tr>
            <td>Gross Interest</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getGrossInterestForInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Taxable </td>
            @if($deposit->taxable)
                <td>Yes</td>
            @else
                <td>No</td>
            @endif
        </tr>
        <tr>
            <td>Withholding Tax</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getWithholdingTaxForInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Net Interest</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getNetInterestForInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Today Value</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deposit->repo->getTotalValueOfAnInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Invested date</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->invested_date )!!}</td>
        </tr>
        <tr>
            <td>Maturity date</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->maturity_date) !!}</td>
        </tr>
        <tr>
            <td>On Call</td>
            <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($deposit->on_call) !!}</td>
        </tr>
        <tr>
            <td>Contact Person</td>
            <td>{!! $deposit->contact_person !!}</td>
        </tr>

        @if($deposit->withdrawn == 1)
            <tr>
                <td>Withdrawal Date</td>
                <td>
                    {!! \Cytonn\Presenters\DatePresenter::formatDate($deposit->withdrawal_date) !!}
                </td>
            </tr>
            @if($deposit->withdraw_transaction_approval_id)
                <tr>
                    <td>Withdraw Transaction</td>
                    <td><a href="/dashboard/portfolio/approve/{!! $deposit->withdraw_transaction_approval_id !!}">See withdrawal transaction approval</a></td>
                </tr>
            @endif
        @endif
        <tr>
            <td>Status</td>
            <td>{!! $deposit->repo->withdrawn() == 1 ? '<span class="label label-danger">Redeemed</span>': '<span class="label label-success">Active</span>'!!}</td>
        </tr>
        </tbody>
    </table>
</div>
