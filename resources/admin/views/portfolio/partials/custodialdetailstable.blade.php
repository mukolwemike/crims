<table class="table table-hover table-responsive table-striped table-bordered">
    <thead>
        <tr><th>Entry Date</th><th>Value Date</th><th>Client Code</th><th>Client</th><th>Product</th><th>Financial Advisor</th><th>Narrative</th><th>Type</th><th>Receipts</th><th>Payment</th><th>Balance</th>
            @if($custodial->currency->code != 'KES')
                <th>Conversion Rate</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($transactions as $transaction)
            <?php $payment = $transaction->clientPayment; ?>
            <tr>
                <td>
                    @if($transaction->entry_date)
                        {!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->entry_date) !!}
                    @else
                        {!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}
                    @endif
                </td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
                <td>
                    @if($transaction->client_id)
                        {!! $transaction->client->client_code !!}
                    @endif
                </td>
                <td>
                    @if($c = $transaction->client_id)
                        {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($c) !!}
                    @else
                        {!! $transaction->received_from !!}
                    @endif
                </td>
                <td>
                    @if($payment)
                        {{ $payment->present()->paymentFor }}
                    @endif
                </td>
                <td>
                    @if($payment)
                        {{ $payment->present()->commissionRecipientName }}
                    @endif
                </td>
                <td>
                    {!! $transaction->description !!}
                    @if($trans_id = $transaction->transaction_id) Src Trans ID: {{ $trans_id }} @endif
                    @if($mpesa = $transaction->mpesa_confirmation_code) MPESA: {!! $mpesa !!} @endif
                    @if($cheque_no = $transaction->cheque_number) CHQ NO: {!! $cheque_no !!}@endif
                </td>
                <td>{!! $transaction->typeName() !!}</td>
                <td align="right">{!! \Cytonn\Presenters\CustodialAmountPresenter::presentReceiptAmount($transaction->amount, false) !!}</td>
                <td align="right">{!! \Cytonn\Presenters\CustodialAmountPresenter::presentPaymentAmount($transaction->amount, false) !!}</td>
                <td align="right">{!! $custodial->transactionBalance($transaction) !!}</td>
                @if($custodial->currency->code != 'KES')
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($custodial->currency->exchange) !!}</td>
                @endif
            </tr>
        @endforeach
    <tr>
        <td></td>
        <td colspan="5">Balance b/f</td>
        @if(isset($transaction))
            <td align="right">{!! $custodial->transactionBalance($transaction) - $transaction->amount !!}</td>
        @else
            <td>0.00</td>
        @endif
    </tr>
    </tbody>
</table>