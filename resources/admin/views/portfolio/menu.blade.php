@extends('layouts.default')

@section('content')

    <bread-crumb title="">
        <a class="current" slot="sub_1" href="#">Portfolio</a>
    </bread-crumb>


    <div class="admin_menus dashboard__account_summary">
        <div class="summaries">
            <menu-card-details title="Orders" title_time="Today" description="Orders Management" description_sub="Managing orders" footer="All Orders" footer_sub="All orders summary">

                <a slot="description" href="/dashboard/portfolio/orders">
                    <span>View All</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>

                <a  slot="footer" href="/dashboard/portfolio/orders">
                    <span>View All</span>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>

            </menu-card-details>


            <menu-card
                    title="Securities"
                    link="/dashboard/portfolio/securities"
                    :first="true"
                    description="Manage Portfolio Securities">
                <i class = "fa fa-credit-card-alt "></i>
            </menu-card>

            <menu-card
                    title="Deposit Holdings"
                    statistics="{!! $gen->portfolioInvestments($manager) !!}"
                    statistics_title="ACTIVE HOLDINGS"
                    :fetch="false"
                    link="/dashboard/portfolio/deposit-holdings"
                    :first="false"
                    description="Manage Portfolio Investments">
                <i class = "fa fa fa-navicon x"></i>
            </menu-card>

            <menu-card
                    title="Approve Transactions"
                    statistics="{!! $gen->transactions($manager) !!}"
                    statistics_title="PENDING"
                    :fetch="false"
                    link="/dashboard/portfolio/approve"
                    :first="false"
                    description="Manage Portfolio Approvals">
                <i class="fa fa-check-square-o"></i>
            </menu-card>


            <menu-card
                    title="Custodial Accounts"
                    link="/dashboard/portfolio/custodials"
                    :first="false"
                    description="Manage Custodial Accounts">
                <i class="fa fa-money"></i>
            </menu-card>


            <menu-card
                    title="Reports"
                    link="/dashboard/portfolio/reports"
                    :first="false"
                    description="Export Portfolio Reports">
                <i class="fa fa-files-o"></i>
            </menu-card>


            <menu-card
                    title="Portfolio Instructions"
                    statistics="0"
                    statistics_title="Pending"
                    link="/dashboard/portfolio/instructions"
                    :first="false"
                    description="Manage Portfolio Instructions">
                <i class="fa fa-bank"></i>
            </menu-card>


            <menu-card
                    title="Deposits Summary"
                    link="/dashboard/portfolio/deposit/summary/"
                    :first="false"
                    description="View Deposit Summaries">
                <i class="fa fa-pie-chart"></i>
            </menu-card>

            <menu-card
                    title="Equities Summary"
                    link="/dashboard/portfolio/equity/summary"
                    :first="false"
                    description="View Equities Summaries">
                <i class="fa fa-pie-chart"></i>
            </menu-card>

            <menu-card
                    title="Compliance"
                    link="/dashboard/portfolio/compliance"
                    :first="false"
                    description="Manage Portfolio Conpliance">
                <i class="fa fa-adn"></i>
            </menu-card>


            <menu-card
                    title="Fund Summary"
                    link="/dashboard/portfolio/fund-summary"
                    :first="false"
                    description="View Fund Summaries">
                <i class="fa fa-area-chart "></i>
            </menu-card>

            <menu-card
                    title="Unit Fund Accounts"
                    link="/dashboard/portfolio/unit-funds"
                    :first="false"
                    description="Manage Unit Fund Accounts">
                <i class="fa fa-hashtag"></i>
            </menu-card>

        </div>
    </div>




    {{--<div class="col-md-2 panel grid-menu">--}}
        {{--<a href = ><i class = "fa fa-adjust fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Order Management</div>--}}
    {{--</div>--}}

    {{--<div class="col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/securities"><i class = "fa fa-credit-card-alt fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Securities</div>--}}
    {{--</div>--}}

    {{--<div class="col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/deposit-holdings"><i class = "fa fa fa-navicon fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Deposit Holdings</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
    {{--<a href = "/dashboard/portfolio/institutions">--}}
    {{--<i class = "fa fa-institution fa-5x"></i>--}}
    {{--</a>--}}

    {{--<div class = "panel-footer">Institutions</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
    {{--<a href = "/dashboard/portfolio/investments">--}}
    {{--<i class = "fa fa-credit-card-alt fa-5x"></i>--}}
    {{--</a>--}}

    {{--<div class = "panel-footer">Deposits</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
    {{--<a href = "/dashboard/portfolio/activestrategy">--}}
    {{--<i class = "fa fa-line-chart fa-5x"></i>--}}
    {{--</a>--}}

    {{--<div class = "panel-footer">Equities</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
    {{--<a href = "/dashboard/portfolio/bonds">--}}
    {{--<i class = "fa fa-mars-double fa-5x"></i>--}}
    {{--</a>--}}
    {{--<div class = "panel-footer">Bonds</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
    {{--<a href = "/dashboard/portfolio/investments/add"><i class = "fa fa-plus-square-o fa-5x"></i></a>--}}

    {{--<div class = "panel-footer">New Investment</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
    {{--<a href = "/dashboard/portfolio/investments"><i class = "fa fa-navicon fa-5x"></i></a>--}}

    {{--<div class = "panel-footer">Portfolio Investments</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/approve"><i class = "fa fa-check-square-o fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Approve Transactions</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/custodials"><i class = "fa fa-money fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Custodial Accounts</div>--}}
    {{--</div>--}}

    {{--<div class="col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/reports"><i class = "fa fa-files-o fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Reports</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/instructions"><i class = "fa fa-bank fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Portfolio Instructions</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/deposit/summary/"><i class = "fa fa-pie-chart fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Deposit Summary</div>--}}
    {{--</div>--}}


    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/summary"><i class = "fa fa-pie-chart fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Portfolio Summary</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/compliance"><i class = "fa fa-adn fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Compliance</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/fund-summary"><i class = "fa fa-area-chart fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Portfolio Summary</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/fund-pricing-summary"><i class = "fa fa-credit-card fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Fund Pricing Summary</div>--}}
    {{--</div>--}}

    {{--<div class = "col-md-2 panel grid-menu">--}}
        {{--<a href = "/dashboard/portfolio/asset"><i class = "fa fa-align-left fa-5x"></i></a>--}}

        {{--<div class = "panel-footer">Asset Summary</div>--}}
    {{--</div>--}}

@stop
