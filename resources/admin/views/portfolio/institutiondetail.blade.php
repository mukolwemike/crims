@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-12">
            <div class="detail-group">
                <h1>{!! $security->name !!}</h1>
            </div>
        </div>

        <div class="col-md-6">
            <div class="detail-group">
                <h3>Statement</h3>

                {!! Form::open(['route'=>['portfolio_statement', $security->id]]) !!}

                <div class="form-group">
                    {!! Form::label('currency_id', 'Currency') !!}

                    {!! Form::select('currency_id', $currencies, null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('from_date', 'From Date') !!}

                    {!! Form::text('from_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"from_date", 'is-open'=>"status.opened1", 'ng-focus'=>'status.opened1 = !status.opened1', 'ng-required'=>"true"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('date', 'As at Date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}
                </div>

                {!! Form::submit('View', ['class'=>'btn btn-success']) !!}

                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <h3>Portfolio Summary</h3>

                {!! Form::open(['url'=>['/dashboard/portfolio/institutions/export'], 'method'=>'POST']) !!}

                <div class="form-group">
                    {!! Form::hidden('investor_id', $security->id) !!}
                    {!! Form::label('date', 'As at date') !!}
                    <div ng-controller = "DatepickerCtrl">{!! Form::text('date', NULL, ['class'=>'form-control', 'placeholder'=>'As at date...', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</div>
                </div>

                {!! Form::submit('Download Summary', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="detail-group">
                <h3>Client</h3>

                @if($security->client)
                    <table class="table table-responsive table-striped">
                        <thead>
                        <th>Name</th><th>Phone</th><th>Email</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{!! \Cytonn\Presenters\ContactPresenter::fullName($security->client->contact_id) !!}</td>
                            <td>{!! $security->client->contact->phone !!}</td>
                            <td>{!! $security->client->contact->email !!}</td>
                            <td>
                                <a class="fa fa-eye"
                                   href="/dashboard/clients/details/{!! $security->client_id !!}"></a>
                            </td>
                            {{--<td>--}}
                                {{--<a class="fa fa-edit" href="/dashboard/clients/create/{!! $institution->client_id !!}"></a>--}}
                            {{--</td>--}}
                        </tr>
                        </tbody>
                    </table>
                @else
                    <a href="/dashboard/portfolio/institutions/{!! $security->id !!}/client/create" class="btn btn-success">Add Client</a>
                @endif
            </div>
        </div>
    </div>
@stop