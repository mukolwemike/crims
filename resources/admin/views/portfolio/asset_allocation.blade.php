@extends('layouts.default')

@section('content')
    <div ng-controller="PortfolioAllocationSummary" class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="row">
                <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Portfolio allocation</button>
                <br>
                <hr>
            </div>

            <div class="row">
                <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-striped table-hover">
                    <thead>
                    <tr>
                        <td colspan="5">

                        </td>
                        <td colspan = "3">
                            {!! Form::select('unit_fund_id', $unitFunds, null,
                                [
                                    'ng-init' => "unit_fund_id",
                                    'ng-model' => 'unit_fund_id',
                                    'class' => 'form-control',
                                    'ng-change' => 'callServer(tableState)',
                                    'placeholder' => 'Select unit fund'
                                ])
                            !!}
                        </td>

                        <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                    </tr>
                        <tr>
                            <th colspan = "2">Unit Fund</th>
                            <th colspan = "2">Asset Class</th>
                            <th colspan = "2">Sub Asset Class</th>
                            <th colspan = "4">Limit</th>
                        </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td colspan = "2"><% row.fund %></td>
                            <td colspan = "3"><% row.assetClass %></td>
                            <td colspan = "3"><% row.subAssetClass %></td>
                            <td colspan = "2"><% row.limit %></td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="100%">Loading...</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan = "3" class = "text-center">
                                Items per page
                            </td>
                            <td colspan = "3" class = "text-center">
                                <input type = "text" ng-model = "itemsByPage"/>
                            </td>
                            <td colspan = "3" class = "text-center">
                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                            </td>
                            <td class="text-center" st-pagination="" st-items-by-page="10" colspan="6"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-controller="SubAssetClassController">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        {!! Form::open(['route'=>['add_portfolio_limit']]) !!}

                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Portfolio Allocation</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                                        {!! Form::select('fund_id', $unitFunds, null, ['class'=>'form-control', 'id'=>'fund_id', 'init-model'=>'fund_id', 'required']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_id') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('asset_class_id', 'Asset Class') !!}
                                        {!! Form::select('asset_class_id', $assetClasses, null, ['class'=>'form-control', 'id'=>'asset_class_id', 'init-model'=>'asset_class_id', 'required']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'asset_class_id') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('sub_asset_class_id', 'Sub Asset Class') !!}
                                        <div  ng-show="asset_class_id">
                                            <select class="form-control" name="sub_asset_class_id">
                                                <option ng-repeat="subAssetClass in subAssetClasses" value="<% subAssetClass.id %>" required><% subAssetClass.name %></option>
                                            </select>
                                        </div>
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'branch_id') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('limit', 'Limit') !!}
                                        {!! Form::number('limit', NULL, ['class'=>'form-control']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'limit') !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection