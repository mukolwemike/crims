@extends('layouts.default')

@section('content')
    <div class = "col-md-8 col-md-offset-2">
        <div class = "panel-dashboard" ng-controller = "addPortfolioInvestmentCtrl">
            <div class="detail-group">

                {{ Form::open(['ng-submit'=>'submit($event)', 'name'=>'form']) }}

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('portfolio_investor_id', 'Institution') !!}</div>
                    <div class = "col-md-9">{!! Form::select('portfolio_investor_id', $portfolioinvestors, null, ['class'=>'form-control', 'ui-select2', 'init-model'=>'portfolio_investor_id', 'data-placeholder'=>"Select the Institution", 'id'=>'portfolio_investor_id']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'portfolio_investor_id') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('fund_type_id', 'Fund Type') !!}</div>
                    <div class = "col-md-9">{!! Form::select('fund_type_id', $fundtypes, null, ['class'=>'form-control','id'=>'fund_type_id', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_type_id') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('type_id', 'Portfolio Investment Type') !!}</div>
                    <div class = "col-md-9">{!! Form::select('type_id', $portfolioinvestmenttypes, null, ['class'=>'form-control','id'=>'type_id', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type_id') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('custodial_account_id', 'Custodial account') !!}</div>
                    <div class = "col-md-9">{!! Form::select('custodial_account_id', $custodialaccounts, null, ['class'=>'form-control','id'=>'custodial_account_id', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}</div>
                </div>

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    <div class = "col-md-3">{!! Form::label('invested_date', 'Investment date') !!}</div>
                    <div class = "col-md-9">{!! Form::date('invested_date', null, ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}</div>
                </div>

                <div class = "form-group" ng-controller = "DatepickerCtrl">
                    <div class = "col-md-3">{!! Form::label('maturity_date', 'Maturity date') !!}</div>
                    <div class = "col-md-9">{!! Form::date('maturity_date', null, ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}</div>

                    <div class="col-md-3"></div><div class="col-md-9">{!! Form::checkbox('on_call', true, false, ['id'=>'on_call']) !!} {!! Form::label('on_call', 'On call') !!}</div>
                </div>

                <div class="form-group">

                    <div class="clearfix"></div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('interest_rate', 'Interest rate') !!}</div>
                    <div class = "col-md-9">{!! Form::number('interest_rate', null, ['class'=>'form-control', 'required',  'step'=>0.001, 'init-model'=>'interest_rate']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">{!! Form::label('taxable', 'Tax Rate') !!}</div>
                    <div class="col-md-9">{!! Form::select('taxable', [true=>'Taxable', false=>'Zero rated'], null, ['class'=>'form-control', 'init-model'=>'taxable']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('amount', 'Amount') !!}</div>
                    <div class = "col-md-9">{!! Form::number('amount', null, ['class'=>'form-control', 'step'=>0.01, 'init-model'=>'amount', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>
                </div>

                <div class="form-group">
                    <div class = "col-md-3">{!! Form::label('contact_person', 'Contact Person') !!}</div>
                    <div class = "col-md-9">{!! Form::text('contact_person', null, ['class'=>'form-control', 'init-model'=>'contact_person', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person') !!}</div>

                </div>
                <div class = "form-group">
                    <div class = "col-md-3">{!! Form::label('description', 'Transaction Description') !!}</div>
                    <div class = "col-md-9">{!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>2, 'init-model'=>'description', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}</div>

                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>

            <div ng-view></div>
            <script type = "text/ng-template" id = "addPortfolioInvestment.htm">
                <div class = "modal fade">
                    <div class = "modal-dialog">
                        <div class = "modal-content">

                            <div class = "modal-header no-bottom-border">
                                <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                        data-dismiss = "modal" aria-hidden = "true">&times;</button>
                                <h4 class = "modal-title">Confirm New Portfolio Investment</h4>
                            </div>
                            <div class = "modal-body">
                                <div ng-show="overdraft" class="detail-group">
                                    <div class="alert alert-danger">
                                        <p>This transaction will result in an overdraft of <% balance | currency:"" %></p>
                                    </div>

                                    <button type="button" class="btn btn-danger btn-sm" href="" ng-click="allowoverdraft()">Continue</button>
                                    <button type = "button" ng-click = "close(false)" class = "btn btn-default btn-sm" data-dismiss = "modal">Go Back</button>
                                </div>
                                <div ng-hide="overdraft" class = "detail-group">
                                    <p class="danger">Are you sure you want to add a new investment with the details below?</p>
                                    <table class = "table table-hover table-responsive table-striped">
                                        <tbody>
                                        <tr>
                                            <td>Institution</td>
                                            <td><% institutionText %></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Type</td>
                                            <td><% fundTypeText %></td>
                                        </tr>
                                        <tr>
                                            <td>Investment Type</td>
                                            <td><% investmentTypeText %></td>
                                        </tr>
                                        <tr>
                                            <td>Custodial Account</td>
                                            <td><% custodialAccountText %></td>
                                        </tr>
                                        <tr>
                                            <td>Investment Date</td>
                                            <td><% investedDate %></td>
                                        </tr>
                                        <tr>
                                            <td>Maturity Date</td>
                                            <td><% maturityDate %></td>
                                        </tr>
                                        <tr class="success" ng-if="on_call">
                                            <td>On call</td><td>Yes</td>
                                        </tr>
                                        <tr>
                                            <td> Amount</td>
                                            <td><% amount | currency:"" %></td>
                                        </tr>
                                        <tr>
                                            <td>Interest Rate</td>
                                            <td><% interestRate %>%</td>
                                        </tr>
                                        <tr ng-class="taxable_class">
                                            <td>Taxable</td>
                                            <td><% taxable_text %></td>
                                        </tr>
                                        <tr>
                                            <td>Contact Person</td>
                                            <td><% contactPerson %></td>
                                        </tr>
                                        <tr class="alert alert-danger" ng-show="is_overdraft">
                                            <td colspan="2">Transaction results to an overdraft</td>
                                        </tr>
                                        <tr>
                                            <td>Transaction Description</td>
                                            <td><% description %></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div ng-hide="overdraft" class = "detail-group">
                                    <div class = "pull-right">
                                        {!! Form::model($investment, ['url'=>'/dashboard/portfolio/investments/add/'.$investment->id]) !!}
                                        <div style = "display:none !important;">
                                            {!! Form::text('portfolio_investor_id', null, ['init-model'=>' institution']) !!}
                                            {!! Form::text('fund_type_id', null,  ['init-model'=>' fundType']) !!}
                                            {!! Form::text('type_id', null,  ['init-model'=>' investmentType']) !!}
                                            {!! Form::text('custodial_account_id', null, ['init-model'=>' custodialAccount']) !!}
                                            {!! Form::text('invested_date', null, ['init-model'=>' investedDate']) !!}
                                            {!! Form::text('maturity_date', null, ['init-model'=>'maturityDate']) !!}
                                            {!! Form::text('amount',  null, ['init-model'=>' amount']) !!}
                                            {!! Form::text('interest_rate', null, ['init-model'=>' interestRate']) !!}
                                            {!! Form::text('taxable', null, ['init-model'=>'taxable']) !!}
                                            {!! Form::text('on_call', null, ['init-model'=>'on_call']) !!}
                                            {!! Form::text('contact_person', null, ['init-model'=>'contactPerson']) !!}
                                            {!! Form::text('description', null, ['init-model'=>'description']) !!}
                                        </div>
                                        <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                                data-dismiss = "modal">No
                                        </button>
                                        {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </script>

        </div>
    </div>
@stop