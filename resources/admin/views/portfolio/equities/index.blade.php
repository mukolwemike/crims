@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div ng-controller="EquitySecurityGridCtrl">
                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                    <tr colspan="4">
                        <th>
                            <div class="butonn-group">
                                <a href="/dashboard/portfolio/securities" class="btn btn-default margin-bottom-20">
                                    <i class="fa fa-long-arrow-left"></i> Securities
                                </a>
                            </div>
                        </th>
                        <th>
                            <div class="">
                                <a href="/dashboard/portfolio/fund-summary" class="btn btn-success margin-bottom-20">
                                    <i class="fa fa-long-arrow-right"></i> Fund Summaries
                                </a>
                            </div>
                        </th>
                        <th>
                            <div class="">
                                <button type="button" class="btn btn-primary margin-bottom-20" data-toggle="modal"
                                        data-target=".portfolio-equities-report-modal">
                                    <i class="fa fa-file"></i> Export
                                </button>

                                <div class="modal fade portfolio-equities-report-modal" tabindex="-1" role="dialog"
                                     aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['export_portfolio_equities_report_path']]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close" data-dismiss="modal"
                                                   aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="myModalLabel">Portfolio Equities Report</h4>
                                            </div>
                                            <div class="modal-body row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <p>Select the fund that you wish to generate a report
                                                            for.</p>
                                                    </div>

                                                    <div class="col-md-12">
{{--                                                        {!! Form::label('unit_fund_id', 'Unit Fund') !!}--}}
                                                        {{-- {!! Form::select('unit_fund_id', $unitFunds, null, ['class'=>'form-control']) !!}--}}
                                                        <label for="unitFundSelect"> Unit Fund</label>
                                                        <select id="unitFundSelect" name="unit_fund_id" class="form-control">
                                                            <option value="">All Unit Funds</option>
                                                            @foreach($unitFunds as $key => $fund)
                                                                <option value="{!! $key !!}">{!! $fund !!}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </th>
                        <th colspan="6"></th>
                        <td colspan="2">
                            {{--                                {!! Form::select('unit_fund_id', $unitFunds, null, ['ng-model' => 'unit_fund_id', 'class' => 'form-control', 'ng-change' => 'callServer(tableState)'] ) !!}--}}
                            <select st-search="unitFund" class="form-control">
                                <option value="">All Unit Funds</option>
                                @foreach($unitFunds as $key => $fund)
                                    <option value="{!! $key !!}">{!! $fund !!}</option>
                                @endforeach
                            </select>
                        </td>
                        <th colspan="3"><input st-search="" class="form-control" placeholder="Search..." type="text"/>
                        </th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Cost</th>
                        <th>Total Cost</th>
                        <th>Number of Shares</th>
                        <th>Market Value</th>
                        <th>Market Price</th>
                        <th>Target Price</th>
                        <th>Alpha</th>
                        <th>Portfolio Cost</th>
                        <th>Portfolio Value</th>
                        <th>Dividend</th>
                        <th>Portfolio Return</th>
                        <th>Exposure</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.name %></td>
                        <td><% row.cost | currency:"" %></td>
                        <td><% row.total_cost | currency:"" %></td>
                        <td><% row.total_number | currency:"":0 %></td>
                        <td><% row.market_value | currency:"" %></td>
                        <td><% row.market_price | currency:"" %></td>
                        <td><% row.target_price | currency:"" %></td>
                        <td><% row.alpha | currency:"" %></td>
                        <td><% row.portfolio_cost | currency:"" %></td>
                        <td><% row.portfolio_value | currency:"" %></td>
                        <td><% row.dividend | currency:"" %></td>
                        <td><% row.portfolio_return | currency:"" %></td>
                        <td><% row.exposure | currency:"" %>%</td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter"
                               href="/dashboard/portfolio/securities/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%">Loading...</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Total</th>
                        <td colspan="2"></td>
                        <th><% totals.number_of_shares | currency:"" %></th>
                        <td colspan="4"></td>
                        <th><% totals.portfolio_cost | currency:"" %></th>
                        <th><% totals.portfolio_value | currency:"" %></th>
                        <td></td>
                        <th><% totals.portfolio_return | currency:"" %></th>
                        <th>100%</th>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">
                            Items per page
                        </td>
                        <td colspan="3" class="text-center">
                            <input type="text" ng-model="itemsByPage"/>
                        </td>
                        <td colspan="4" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage"
                                 st-template="pagination.custom.html"></div>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div ng-controller="CollapseCtrl">
                <div>
                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">View valuation
                        charts
                    </button>
                </div>

                <div class="clearfix"></div>

                <hr>
                <div uib-collapse="isCollapsed">
                    <div class="row" ng-controller="EquitySecurityValuationChartCtrl">
                        <div class="col-md-6">
                            <div class="detail-group">
                                <h4>Asset allocation</h4>

                                <canvas id="allocationPieChart" class="chart chart-pie"
                                        chart-data="d"
                                        chart-labels="l">
                                </canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="detail-group">
                                <h4>Portfolio return for the week</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection