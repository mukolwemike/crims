@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            <div class = "detail-group">
                <h4>Share Purchase Details </h4>
                <table class = "table table-hover">
                    <tbody>
                    <tr>
                        <td>Portfolio Security</td>
                        <td>{!! $equityHolding->security->name !!}</td>
                    </tr>
                    <tr>
                        <td>Portfolio Security</td>
                        <td>{!! $equityHolding->security->investor->name !!}</td>
                    </tr>
                    <tr>
                        <td>Asset Class</td>
                        <td>{!! $equityHolding->security->subAssetClass->assetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td>Sub Asset Class</td>
                        <td>{!! $equityHolding->security->subAssetClass->name !!}</td>
                    </tr>
                    <tr>
                        <td>Trade Purchase Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Settlement Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->settlement_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->number) !!}</td>
                    </tr>
                    <tr>
                        <td>Cost</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->cost) !!}</td>
                    </tr>
                    <tr>
                        <td>Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->purchaseValuePerShare()) !!}</td>
                    </tr>
                    <tr>
                        <td>Target Price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->latestTargetPrice())  !!}</td>
                    </tr>
                    <tr>
                        <td>Total Cost</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->totalCost())  !!}</td>
                    </tr>
                    <tr>
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->totalValue())  !!}</td>
                    </tr>
                    <tr>
                        <td>Market Price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->marketPricePerShare())  !!}</td>
                    </tr>
                    <tr>
                        <td>Market Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->marketValue())  !!}</td>
                    </tr>
                    <tr>
                        <td>Gain/Loss</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($equityHolding->repo->gainLoss())  !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <h4>Actions</h4>
                <a class="btn btn-danger margin-bottom-10" href="#" data-toggle="modal" data-target="#reverse_shares_buy">Reverse</a>

                <a class="btn btn-info margin-bottom-10" href="#" data-toggle="modal" data-target="#edit_buy_dates">Edit Dates</a>
            </div>
        </div>
    </div>
@stop


<!--Rollback Investment Modal -->
<div class="modal fade" id="reverse_shares_buy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['equities.post_reverse_buy', $equityHolding->id ]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure you want to reverse the following share buy?</h4>
            </div>
            <div class="modal-body">
                <p>The equity holding will be removed and all affected records undone.</p>

                <div class="form-group">
                    {!! Form::label('reason', 'Please provide a reason') !!}

                    {!! Form::text('reason', null, ['class'=>'form-control', 'placeholder'=>'Reason', 'required']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                    {!! Form::hidden('id', $equityHolding->id) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Proceed to Reverse</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!--Edit the Trade/Settlement Modal -->
<div class="modal fade" id="edit_buy_dates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['equities.edit_dates_buy', $equityHolding->id ]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Provide the dates</h4>
            </div>
            <div class="modal-body">

                <div class="form-group"  ng-controller="DatepickerCtrl">
                    {!! Form::label('trade_date', 'Trade Purchase Date') !!}
                        <div class="input-group">

                        {!! Form::text('trade_date', \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->date), ['class'=> 'form-control', 'datepicker-popup init-model'=>"trade_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}

                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                    </div>
                </div>

                <div class="form-group"  ng-controller="DatepickerCtrl">
                    <div class="input-group">
                        {!! Form::label('settlement_date', 'Settlement  Date') !!}

                        {!! Form::text('settlement_date', \Cytonn\Presenters\DatePresenter::formatDate($equityHolding->settlement_date), ['class'=> 'form-control', 'datepicker-popup init-model'=>"settlement_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Edit Dates</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
