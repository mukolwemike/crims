@extends('layouts.default')

@section('content')
    <div class = "col-md-8 col-md-offset-2">
        <div class = "panel-dashboard">
            <h4>
                Dividend Details
                <div class="btn-group pull-right">
                    <a href="/dashboard/portfolio/security/{!! $security->id !!}/dividends" class="btn btn-default"> Back </a>
                </div>
            </h4>
            <table class = "table table-hover">
                <tbody>
                <tr>
                    <td width="30%">Dividend per share</td>
                    <td>{!! $dividend['dividends'] !!}</td>
                </tr>
                <tr>
                    <td width="30%">Number of Shares at Book Closure</td>
                    <td>{!! $dividend['number'] !!}</td>
                </tr>
                <tr>
                    <td width="30%">Total Amount Paid</td>
                    <td>{!! $dividend['total_amount_paid'] !!}</td>
                </tr>
                <tr>
                    <td width="30%">Date</td>
                    <td>{!! $dividend['date'] !!}</td>
                </tr>
                <tr>
                    <td width="30%">Prevailing Tax Rate</td>
                    <td>{!! $dividend['prevailing_tax_rate'] !!}</td>
                </tr>
                <tr>
                    <td width="30%">Book Closure Date</td>
                    <td>{!! $dividend['book_closure_date'] !!}</td>
                </tr>
                <tr>
                    <td width="30%">Status</td>
                    <td>{!! $dividend['received'] !!}</td>
                </tr>
                </tbody>
            </table>
            <tr>
                <td colspan="100%">
                    <div class="btn-group">
                        @if($dividend['received'] == 'Not Received')
                            <a href="#receive-dividend" class="btn btn-success" data-toggle="modal" role="button">Receive</a>
                        @endif
                    </div>
                </td>
            </tr>
        </div>
    </div>


    <div class="modal fade" id="receive-dividend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['receive_dividend', $security->id, $dividend['id']]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Receive dividend</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group" ng-init="category='investment'">

                        <div class="col-md-12">
                            {!! Form::label('Date', 'Date') !!}
                            {!! Form::text('date', null, ['id'=>'date','class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Receive', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection