@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="#cash" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Cash</a></li>
                <li role="presentation" class=""><a href="#shares" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Shares</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="cash" aria-labelledby="home-tab" ng-controller="CashDividendsGridController">
                    <table ng-init="security_id = {!! $security->id !!}"  st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Dividends per Share</th>
                                <th>Number of Shares at Book Closure</th>
                                <th>Total Amount Paid</th>
                                <th>Date</th>
                                <th>Prevailing Tax Rate</th>
                                <th>Book Closure Date</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody  ng-show="!isLoading">
                            <tr ng-repeat="row in displayed">
                                <td><% row.dividends %></td>
                                <td><% row.number %></td>
                                <td><% row.total_amount_paid %></td>
                                <td><% row.date %></td>
                                <td><% row.prevailing_tax_rate %>%</td>
                                <td><% row.book_closure_date %></td>
                                <td><% row.received %></td>
                                <td>
                                    <a ng-controller = "PopoverCtrl" uib-popover = "Details" popover-trigger = "mouseenter" href = "/dashboard/portfolio/security/{!! $security->id !!}/dividends/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                            <tr>
                                <td colspan="10" class="text-center">Loading ... </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan = "2" class = "text-center">
                                    Items per page
                                </td>
                                <td colspan = "1" class = "text-center">
                                    <input type = "text" ng-model = "itemsByPage"/>
                                </td>
                                <td colspan = "7" class = "text-center">
                                    <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th colspan="50%"> Total Dividends:</th>
                            <th colspan="50%">{!! \Cytonn\Presenters\AmountPresenter::currency($security->dividends()->where('dividend_type', 'cash')->sum('dividend'), true, 0) !!}</th>
                        </tr>
                        </thead>
                    </table>
                </div>



                <div role="tabpanel" class="tab-pane fade" id="shares" aria-labelledby="home-tab" ng-controller="SharesDividendsGridController">
                    <table ng-init="security_id = {!! $security->id !!}"  st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Shares Awarded</th>
                                <th>Number of Shares at Book Closure</th>
                                <th>Date</th>
                                <th>Prevailing Tax Rate</th>
                                <th>Book Closure Date</th>
                                <th>Conversion Price</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody  ng-show="!isLoading">
                            <tr ng-repeat="row in displayed">
                                <td><% row.shares %></td>
                                <td><% row.number %></td>
                                <td><% row.date %></td>
                                <td><% row.prevailing_tax_rate %>%</td>
                                <td><% row.book_closure_date %></td>
                                <td><% row.conversion_price %></td>
                                <td><% row.received %></td>
                                <td>
                                    <a ng-controller = "PopoverCtrl" uib-popover = "Details" popover-trigger = "mouseenter" href = "/dashboard/portfolio/security/{!! $security->id !!}/dividends/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                            <tr>
                                <td colspan="10" class="text-center">Loading ... </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan = "2" class = "text-center">
                                    Items per page
                                </td>
                                <td colspan = "1" class = "text-center">
                                    <input type = "text" ng-model = "itemsByPage"/>
                                </td>
                                <td colspan = "7" class = "text-center">
                                    <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                </td>
                        </tr>
                        </tfoot>
                    </table>
                    <table class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th colspan="50%"> Total Shares:</th>
                                <th colspan="50%">{!! \Cytonn\Presenters\AmountPresenter::currency($security->dividends()->where('dividend_type', 'shares')->sum('shares'), true, 0) !!}</th>
                            </tr>
                        </thead>
                    </table>
                </div>



            </div>
        </div>
    </div>
@endsection