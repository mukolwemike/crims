@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class = "panel panel-dashboard">
            <el-tabs type="card">
                <el-tab-pane label="Compliance">
                    <bench-marks-index></bench-marks-index>
                </el-tab-pane>
                <el-tab-pane label="No Go Zone">
                    <no-go-zone-index></no-go-zone-index>
                </el-tab-pane>
            </el-tabs>
        </div>
    </div>
@endsection