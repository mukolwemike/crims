@extends('reports.letterhead')

@section('content')
    <h1 style="text-decoration: underline">CRIMS ACCESS LIST</h1>
    <h2>User List</h2>
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <td></td><th>Name</th><th>Username</th><th>Job Title</th><th>Roles</th><th>Active</th><th>Created At</th><th>Removed At</th>
            </tr>
        </thead>
        <tbody>
            <?php $index = 1 ?>
            @foreach($users as $user)
                <tr>
                    <td>{!! $index++ !!}</td>
                    <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($user->id) !!}</td>
                    <td>{!! $user->username !!}</td>
                    <td>{!! $user->jobtitle !!}</td>
                    <td>{!! $user->all_roles !!}</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($user->active) !!}</td>
                    <td>{{ $user->created_at ? $user->created_at->toFormattedDateString() : '' }}</td>
                    <td>{{ !$user->active && $user->updated_at ? $user->updated_at->toFormattedDateString() : '' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>


    <h2>Roles</h2>

    <table  class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>Role</th><th>Permissions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{!! $role->name !!}</td>
                    <td>
                        <ul>
                            @foreach($role->permission_access as $permission)
                                <li>{!! $permission->description !!}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>Generated from {!! getenv('DOMAIN') !!} at {!! \Carbon\Carbon::now()->toDayDateTimeString() !!}</p>
@endsection