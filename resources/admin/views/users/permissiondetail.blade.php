@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1 group">
        <div class="panel-dashboard">
            <h3>Permission Details</h3>
            <table class="table table-hover table-responsive">
                <tbody>
                    <tr>
                        <td>Permission ID</td><td>{!! $permission->id !!}</td>
                    </tr>
                    <tr>
                        <td>Name</td><td>{!! $permission->name !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td><td> {!! $permission->description !!}</td>
                    </tr>
                </tbody>
            </table>

            <h4>Assignment</h4>
            <p>This permission is available to the following roles</p>
            <table class="table table-responsive table-bordered table-striped table-hover">
                <thead>
                    <tr><th>Role ID</th><th>Role Name</th><th>Role Description</th></tr>
                </thead>
                <tbody>
                    @foreach($permission->roles as $role)
                        <tr><td>{!! $role->id !!}</td><td>{!! $role->name !!}</td> <td>{!! $role->description !!}</td></tr>
                    @endforeach
                </tbody>
            </table>
            <p>In addition, the permission has also been directly assigned to the following users</p>
            <table class="table table-responsive table-bordered table-striped table-hover">
                <thead>
                    <tr><th>User ID</th><th>Username</th><th>Name</th><th>Email</th></tr>
                </thead>
                <tbody>
                    @foreach($permission->directlyAssigned as $user)
                        <tr><td>{!! $user->id !!}</td><td>{!! $user->username !!}</td><td>{!! \Cytonn\Presenters\UserPresenter::presentNames($user->id) !!}</td><td>{!! $user->email !!}</td></tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop