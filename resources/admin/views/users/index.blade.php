@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="UsersGridCtrl">
            <table st-table="displayedCollection" st-safe-src="rowCollection" class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan="1"><a class="margin-bottom-20 btn btn-success" href="/dashboard/users/add"> <i class="fa fa-plus"></i> Add new</a></th>
                    <th colspan="5"></th>
                    <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                </tr>
                <tr>
                    <th st-sort="id">ID</th>
                    <th st-sort="firstname">First name</th>
                    <th st-sort="middlename">Middle name</th>
                    <th st-sort="lastname">Last name</th>
                    <th st-sort="username">Username</th>
                    <th st-sort="email">Email Address</th>
                    <th st-sort="phone">Job Title</th>
                    <th st-sort="active">Active</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in displayedCollection">
                    <td><% row.id %></td>
                    <td><% row.firstname %></td>
                    <td><% row.middlename %></td>
                    <td><% row.lastname %></td>
                    <td><% row.username %></td>
                    <td><% row.email %></td>
                    <td><% row.jobtitle%></td>
                    <td><span to-html = "row.active | activeStatus"></span></td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter"  href="/dashboard/users/details/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                        <a ng-controller="PopoverCtrl" uib-popover="Edit details" popover-trigger="mouseenter"  href="/dashboard/users/add/<%row.id%>"><i class="fa fa-edit"></i></a>
                    </td>

                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td  colspan="2" class="text-center">
                        Items per page
                    </td>
                    <td colspan="2" class="text-center">
                        <input type="text"  ng-model="itemsByPage"/>
                    </td>
                    <td colspan="4" class="text-center">
                        <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="10"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop