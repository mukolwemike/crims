@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">

            <uib-accordion>
                <uib-accordion-group heading="Edit Profile" is-open="true">
                    <p>You can edit the following information as listed in your profile:</p>

                    <div class = "col-md-12 panel panel-dashboard ">
                        {!! Form::model($user, ['route'=>'edit_profile_path']) !!}

                        <div class="form-group">
                            <div class="col-md-2">{!! Form::label('title_id', 'Title') !!}</div>

                            <div class="col-md-4">{!! Form::select('title_id', $titles,  $user->title_id,  ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}</div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2"> {!! Form::label('firstname', 'First Name') !!} </div>

                            <div class="col-md-4">{!! Form::text('firstname', $user->firstname, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-2"> {!! Form::label('middlename', 'Middle Name') !!} </div>

                            <div class="col-md-4">{!! Form::text('middlename', $user->middlename, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}</div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">{!! Form::label('lastname', 'Last name') !!}</div>

                            <div class="col-md-4">{!! Form::text('lastname', $user->lastname, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">  {!! Form::label('email', 'Email') !!} </div>

                            <div class="col-md-4"> {!! Form::text('email', $user->email, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!} </div>
                        </div>

                        <div class="form-group">
                           <div class="col-md-2"> {!! Form::label('jobtitle', 'Job Title') !!} </div>

                           <div class="col-md-4"> {!! Form::text('jobtitle', null, ['class'=>'form-control']) !!} </div>

                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'jobtitle') !!}
                        </div>

                        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

                        {!! Form::close() !!}
                    </div>

                </uib-accordion-group>

                <uib-accordion-group heading="Sign Off & Signature" is-open="true">
                    <p>Documents you approve or send will use the sign off below</p>

                    <div>
                        <img src="{!! \Cytonn\Presenters\UserPresenter::signatureBase64($user->id) !!}" height="60px">
                    </div>

                    {!! \Cytonn\Presenters\UserPresenter::presentLetterClosingNoSignature($user->id) !!}
                </uib-accordion-group>

                <uib-accordion-group heading="Two Factor Authentication">
                    <p>You can now use two factor authentication applications such as Authy or Google Authenticator to log in. Just scan the QR code below in the application to get started</p>

                    @if($sudo_mode)
                        {!! HTML::image($google2fa_url) !!}
                    @else
                        <div class="col-md-6">
                            {!! Form::open(['route'=>'set_sudo_mode', '']) !!}

                            <div class="form-group danger">
                                <div class="alert alert-danger"><p>Enter your password below to reveal QR code</p></div>

                                {!! Form::password('password', ['class'=>'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Submit', ['class'=>'btn btn-danger']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    @endif
                </uib-accordion-group>

            </uib-accordion>
        </div>
    </div>
@endsection