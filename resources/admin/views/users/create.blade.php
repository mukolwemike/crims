@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <h3>Add a new user</h3>

            <div class="detail-group">
                {!! Form::model($user, ['url'=>'/dashboard/users/add/'.$user->id]) !!}

                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('title_id', 'Title') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::select('title_id', $titles, $user->title_id,  ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('firstname', 'First Name') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::text('firstname', isset($user->firstname) ?  $user->firstname : null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('middlename', 'Middle Name') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::text('middlename', isset($user->firstname) ?  $user->middlename : null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('lastname', 'Last Name') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::text('lastname', isset($user->lastname) ?  $user->lastname : null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('username', 'Username') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::text('username', isset($user->username) ?  $user->username : null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'username') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('email', 'Email') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::text('email', isset($user->email) ?  $user->email : null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::label('jobtitle', 'Job Title') !!}
                    </div>
                    <div class="col-md-9">
                        {!! Form::text('jobtitle', isset($user->jobtitle) ?  $user->jobtitle : null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'jobtitle') !!}
                    </div>
                </div>
                <div class="col-md-12 alert-warning margin-bottom-20">
                    <p>*If email exists in contacts, the user account will be linked to the email</p>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::submit('Submit', ['class'=>'btn btn-block btn-success']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop