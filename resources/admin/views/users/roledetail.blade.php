<?php
?>
@extends('layouts.default')

@section('content')

    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Role Details</h3>

            <table class = "table table-hover table-responsive">
                <tbody>
                <tr>
                    <td>Role ID</td>
                    <td>{!! $role->id !!}</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>{!! $role->name !!}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td> {!! $role->description !!}</td>
                </tr>
                </tbody>
            </table>

            <h4>Membership</h4>

            <p>The following are the members of this role</p>
            <table class = "table table-responsive table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>Member ID</th>
                    <th>Username</th>
                    <td>Name</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                @foreach($role->memberships()->where('authorizable_type', 'App\Cytonn\Models\User')->get() as $membership)
                    <?php  $user = App\Cytonn\Models\User::find($membership->authorizable_id) ?>
                    @if($user)
                    <tr>
                        <td>{!! $user->id !!}</td>
                        <td>{!! $user->username !!}</td>
                        <td>{!! \Cytonn\Presenters\UserPresenter::presentNames($user->id) !!}</td>
                        <td>
                            {!! Form::open(['url'=>'/dashboard/users/roles/removemember']) !!}
                            {!! Form::hidden('member', $membership->id) !!}
                            {!! Form::submit('Remove', ['class'=>'btn btn-danger btn-xs pull-right']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

            <h4>Permissions</h4>

            <p>This role has been assigned the following permissions</p>

            <button type = "button" class = "btn form-control-static margin-bottom-20" data-toggle = "modal"
                    data-target = "#myModal">
                Add Permission to role
            </button>

            <table class = "table table-responsive table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>Permission ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($role->permissions as $permission)
                    <tr>
                        <td>{!! $permission->id !!}</td>
                        <td>{!! $permission->name !!}</td>
                        <td>{!! $permission->description !!}</td>
                        <td>
                            {!! Form::open(['url'=>'/dashboard/users/roles/revoke']) !!}
                            {!! Form::hidden('permission', $permission->id) !!}
                            {!! Form::hidden('role', $role->id) !!}
                            {!! Form::submit('Revoke', ['class'=>'btn btn-danger btn-xs']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Add Permission Modal -->
    <div class = "modal fade" id = "myModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel">
        <div class = "modal-dialog modal-lg" role = "document">
            <div class = "modal-content">
                <div class = "modal-header">
                    <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                                aria-hidden = "true">&times;</span></button>
                    <h4 class = "modal-title" id = "myModalLabel">Add permission</h4>
                </div>

                <div class = "modal-body">
                    {!! Form::open(['url'=>'/dashboard/users/roles/grant']) !!}

                    {!! Form::hidden('role', $role->id) !!}

                    <div class = "permissions">
                        <div class = "form-group">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <p>Select the permissions to add to the group</p>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                @foreach($groups as $group)
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">{{ explode(':', array_first($group)->name)[0] }}</div>
                                            <div class="panel-body">
                                                @foreach($group as $permission)
                                                    {!! Form::checkbox($permission->name, true, in_array($permission->id, $assigned)) !!} &nbsp;&nbsp; {!! $permission->name !!}<br/><br/>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-md-12">{!! Form::submit('Add', ['class'=>'btn btn-success']) !!}</div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class = "modal-footer">
                    <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal -->
    <script>
        $('#select-all').click(function (event) {
            if (this.checked) {
                $(':checkbox').prop('checked', true);
            } else {
                $(':checkbox').prop('checked', false);
            }
        });
    </script>
@stop
