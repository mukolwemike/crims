@extends('layouts.default')

@section('content')
    <div class="col-md-4  col-md-offset-4 login-form">
        <div class="panel panel-login">
            <div class="login-icon warning">
                <i class="fa fa-exclamation-triangle fa-4x"></i>
                <p>
                    You are about to enter a restricted area, please enter your password to confirm
                </p>
            </div>

            {!! Form::open(['route'=>['set_sudo_mode', 'dashboard']]) !!}

                <div class="form-group">
                    {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password', 'autocomplete'=>'off']) !!}
                </div>

                <div class="form-group">
                    {!! Form::button('Submit', ['name'=>'submit', 'type'=>"submit", 'class'=>"btn btn-success btn-block"]) !!}
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop