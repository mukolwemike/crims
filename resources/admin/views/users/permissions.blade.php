@extends('layouts.default')
@section('content')

    <div class="col-md-12">
        <div class="panel-dashboard" ng-controller="PermissionsGridCtrl">
                <table st-table="displayedCollection" st-safe-src="rowCollection" class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th colspan="2">
                        <th colspan="1"></th>
                        </th><th colspan="1"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                    </tr>
                    <tr>
                        <th st-sort="id">ID</th>
                        <th st-sort="name">Name</th>
                        <th st-sort="description" class="wide">Description</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in displayedCollection">
                        <td><% row.id %></td>
                        <td><% row.name %></td>
                        <td><% row.description %></td>
                        <td><a  ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/users/permissions/details/<%row.id%>"><i class="fa fa-list-alt"></i></a></td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td  colspan="2" class="text-center">
                            Items per page
                        </td>
                        <td colspan="1" class="text-center">
                            <input type="text"  ng-model="itemsByPage"/>
                        </td>
                        <td colspan="2" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="10"></div>
                        </td>
                    </tr>
                    </tfoot>
                </table>

        </div>
    </div>

@stop