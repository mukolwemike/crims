@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="detail-group">
                <div class="form-group">
                    {!! Form::model($user, []) !!}

                    <div class="form-group">
                        {!! Form::label('username', 'Username') !!}

                        {!! Form::text('username', null, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'username') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('firstname', 'First name') !!}

                        {!! Form::text('firstname', null, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('lastname', 'Last name') !!}

                        {!! Form::text('lastname', null, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}

                        {!! Form::text('email', null, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone_country_code', 'Country Dialing Code') !!}

                        {!! Form::text('phone_country_code', null, ['class'=>'form-control', 'placeholder'=>'+254']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone_country_code') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Phone Number') !!}

                        {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'0727272727']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('ussd_account', 'Set as Default Ussd Account') !!}

                        {!! Form::select('ussd_account',  ['0' => 'Disabled for Ussd Account', '1' => 'Enabled for Ussd Account'], null, ['id'=>'ussd_account', 'class'=>'form-control',]) !!}
                    </div>

                    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection