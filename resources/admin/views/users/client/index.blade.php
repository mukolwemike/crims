@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div ng-controller="ClientUsersGridCtrl">
                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <a class="margin-bottom-20 btn btn-success" href="/dashboard/users/client/add">
                                <i class="fa fa-plus"></i> Add new</a>
                            <a class="margin-bottom-20 btn btn-info" href="#export-accounts-modal"
                               data-toggle="modal" role="button"> <i class="fa fa-download"></i> Export</a>
                        </th>
                        <th colspan="6"></th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/>
                        </th>
                    </tr>
                    <tr>
                        <th st-sort="id">ID</th>
                        <th st-sort="firstname">First name</th>
                        <th st-sort="lastname">Last name</th>
                        <th st-sort="username">Username</th>
                        <th st-sort="email">Email Address</th>
                        <th st-sort="phone_country_code">Country code</th>
                        <th st-sort="phone">Phone Number</th>
                        <th st-sort="authy_id">Authy ID</th>
                        <th st-sort="emailed">Emailed</th>
                        <th st-sort="active">Active</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody ng-if="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.firstname %></td>
                        <td><% row.lastname %></td>
                        <td><% row.username %></td>
                        <td><% row.email %></td>
                        <td><% row.phone_country_code %></td>
                        <td><% row.phone %></td>
                        <td><% row.authy_id %></td>
                        <td><% row.emailed | yesno %></td>
                        <td><span to-html="row.active | activeStatus"></span></td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter"
                               href="/dashboard/users/client/details/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                            <a ng-controller="PopoverCtrl" uib-popover="Edit details" popover-trigger="mouseenter"
                               href="/dashboard/users/client/add/<%row.id%>"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-if="isLoading">
                    <tr>
                        <td colspan="100%" align="center">Loading...</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="100%" dmc-pagination></td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
    {{--the start of the export-accounts-modal window--}}
    <div class="modal fade" id="export-accounts-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content p-4">
                {!! Form::open(['route'=>['get_export_user_account_path']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export User Accounts</h4>
                </div>
                <div class="modal-body">
                    <div class=" row">
                        <div class="col-md-6" ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'style'=>'width: 100%;', 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6" ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'style'=>'width: 100%;', 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('active', 'Active Status') !!}
                            {!! Form::select('active', array('' =>'Select Status','1' => 'Active account', '0' => 'Inactive Account'), null, array('class'=>'form-control','style'=>'width: 100%;' )) !!}
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('account', 'Active Status') !!}
                            {!! Form::select('account_type', array('' =>'Select Account Option', '1' => 'USSD Account', '0' => 'Other Account'), null, array('class'=>'form-control','style'=>'width: 100%;' )) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of export-accounts-modal--}}
@endsection
