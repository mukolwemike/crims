@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard" ng-controller="ClientUserCtrl">
            <div class="detail-group">
                <h3>User Details</h3>

                <table class="table table-responsive table-hover">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <td>Username</td><td>{!! $user->username !!}</td>
                        </tr>
                        <tr>
                            <td>Name</td><td>{!! $user->firstname.' '.$user->lastname !!}</td>
                        </tr>
                        <tr>
                            <td>Email Address</td><td>{!! $user->email !!}</td>
                        </tr>
                        <tr>
                            <td>Phone Number</td><td>{!! $user->phone !!}</td>
                        </tr>
                        <tr>
                            <td>Authy ID</td><td>{!! $user->authy_id !!}</td>
                        </tr>
                        <tr>
                            <td>Authy Registration</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($authy_status) !!}</td>
                        </tr>
                        <tr>
                            <td>Default Ussd Account</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($user->ussd_account) !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="detail-group">
                <h3>Actions</h3>

                @if($user->password)
                    <a href="/dashboard/users/client/resend_password/{!! Crypt::encrypt($user->id) !!}" class="btn btn-primary">Reset password</a>
                @else
                    @if(!$user->emailed)
                        <div class="alert alert-warning">
                            <p>The activation email has not been sent to the user, please make sure the user has an authy id and at least one client he/she can access</p>
                        </div>
                    @endif
                    <a href="/dashboard/users/client/resendlink/{!! Crypt::encrypt($user->id) !!}" class="btn btn-primary">Resend activation link</a>
                @endif
                @if($user->active)
                    <a href="/dashboard/users/client/{!! $user->id !!}/deactivate" class="btn btn-danger">Deactivate user</a>
                @else
                    <a href="/dashboard/users/client/{!! $user->id !!}/reactivate" class="btn btn-success">Reactivate user</a>
                @endif
            </div>

            <div class="detail-group">
                <h3>Access Management</h3>

                <p>This user account has access to the following clients</p>

                <table class="table table-responsive table-hover table-striped">
                    <thead>
                        <tr><th>Client Code</th><th>Client Name</th><th></th></tr>
                    </thead>
                    <tbody>
                        @foreach($access as $a)
                            <tr>
                                <td>{!! $a->client->client_code !!}</td>
                                <td>{!! $name = \Cytonn\Presenters\ClientPresenter::presentFullNames($a->client_id) !!}</td>
                                <td><a ng-click="revokeClient('{!! $name !!}', '{!! $a->id !!}')" href="#" class="btn btn-danger btn-xs">Remove</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="col-md-6">
                    {!! Form::open(['route'=>['client_user_assign_path', $user->id]]) !!}

                    <h4>Assign access to a client's information</h4>


                    <div class="form-group">
                        {!! Form::label('client_name', 'Enter the client name to search') !!}

                        <input autocomplete="off" name="client" type="text" ng-model="selectedClient" placeholder="Enter the client name" typeahead="client as client.fullName for client in searchClients($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                        <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <div class="hide">
                            <input name="client_id" type="text" ng-model="selectedClient.id">
                        </div>
                    </div>

                    {!! Form::submit('Assign', ['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="col-md-6" ng-show="selectedClient != undefined">
                    <h4>Selected client</h4>
                    <table class="table table-responsive">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>Client Name</td><td><% selectedClient.fullName %></td>
                            </tr>
                            <tr>
                                <td>Client code</td><td><% selectedClient.client_code %></td>
                            </tr>
                            <tr>
                                <td>Email</td><td><% selectedClient.contact.email %></td>
                            </tr>
                            <tr>
                                <td>Client Type</td><td><% selectedClient.type %></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="detail-group">
                <h3>Financial Advisor Accounts</h3>

                @if($fas->count() == 0)
                    <div class="alert alert-info">
                        <p>This user has no FA accounts linked</p>
                    </div>
                @else
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr><th>ID</th><th>Name</th><th></th></tr>
                        </thead>
                        <tbody>
                            @foreach($fas as $fa)
                                <tr>
                                    <td>{!! $fa->id !!}</td>
                                    <td><a href="/dashboard/investments/commission/owner/{!! $fa->id !!}">{!! $fa->name !!}</a></td>
                                    <td><a ng-click="revokeFa('{!! $fa->name !!}', '{!! $fa->id !!}', '{!! $user->id !!}')" href="#" class="btn btn-danger btn-xs">Remove</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                {!! $fas->links() !!}

                <div class="col-md-6">
                    {!! Form::open(['route'=>['fa_user_assign_path', $user->id]]) !!}

                    <h4>Assign access to a Financial Advisor</h4>

                    <div class="form-group">
                        {!! Form::label('client_name', 'Enter the fa name to search') !!}

                        <input name="fa" autocomplete="off" type="text" ng-model="selectedFa" placeholder="Enter the FA name" typeahead="fa as fa.name for fa in searchFas($viewValue)" class="form-control" typeahead-loading="loading_fas" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                        <i ng-show="loading_fas" class="fa fa-refresh fa-spin"></i>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <div class="hide">
                            <input name="fa_id" type="text" ng-model="selectedFa.id">
                        </div>
                    </div>

                    {!! Form::submit('Assign', ['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="col-md-6" ng-show="selectedFa != undefined">
                    <h4>Selected client</h4>
                    <table class="table table-responsive">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>FA Name</td><td><% selectedFa.name %></td>
                            </tr>
                            <tr>
                                <td>Email</td><td><% selectedFa.email %></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="detail-group">
                <div class="col-md-12">
                    <h3>Client Signatures</h3>
                </div>

                <hr>

                <div class="col-md-12">
                @if($user->clientSignatures->count())
                    <table class="table table-responsive table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Date</th>
                            <th>Active</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->clientSignatures as $clientSignature)
                            <tr>
                                <td>{!! $clientSignature->present()->getName !!}</td>
                                <td>{!! $clientSignature->present()->getEmail !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientSignature->updated_at) !!}</td>
                                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($clientSignature->active) !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-xs" target="_blank"
                                           href="/dashboard/investments/client-instructions/filled-application-documents/{!! $clientSignature->document_id !!}">
                                            View File
                                        </a>
                                        <a ng-click="removeSignature('{!! $clientSignature->present()->getName !!}', '{!! $clientSignature->id !!}', '{!! $user->id !!}')" href="#" class="btn btn-danger btn-xs">Remove</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info">
                        <p>This user has no client signatures linked.</p>
                    </div>
                @endif
                </div>

                <div class="col-md-6">
                    {!! Form::open([ 'route' => ['client_user_assign_signature', $user->id ]]) !!}
                    {!! Form::hidden('user_id', $user->id) !!}

                    <h4>Assign client signature to user</h4>

                    <div class="form-group">
                        {!! Form::label('signature', 'Select client signature to assign') !!}
                        {!! Form::select('client_signature_id', $signature_options , '', [ 'class'=>'form-control', 'required', 'ng-model' => 'client_signature_id']) !!}
                    </div>

                    {!! Form::submit('Assign', ['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection