@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <h3>Add a new Role</h3>
            <div class="detail-group">
                <p>Describe the role below:</p>
                {!! Form::model($role, ['url'=>'/dashboard/users/roles/add/'.$role->id]) !!}

                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('name', 'Role name') !!}
                    </div>

                    <div class="col-md-8">
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('description', 'Role description') !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('description', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                    </div>
                </div>

                {!! Form::submit('Add', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop