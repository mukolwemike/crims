@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1 group">
        <div class="panel-dashboard">
            <div class="detail-group">
                <h3>User Details</h3>
                <table class="table table-hover">
                    <tbody>
                    <tr><td>Name:</td><td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($user->id) !!}</td></tr>
                    <tr><td>Email:</td><td>{!! $user->email !!}</td></tr>
                    <tr><td>Phone Number</td><td>{!! $user->phone !!}</td></tr>
                    </tbody>
                </table>
            </div>

            <div>
                <div class="detail-group">
                    <div class="col-md-12">
                        <h3>Actions</h3>

                        <div class="form-group">
                            @if($status == 'active')
                                <a href="/dashboard/users/reset/{!! Crypt::encrypt($user->id) !!}" class="btn btn-primary">Reset Password</a>
                            @elseif($status == 'not-activated')
                                <a href="/dashboard/users/resendlink/{!! Crypt::encrypt($user->id) !!}" class="btn btn-primary">Resend activation link</a>
                            @endif
                        </div>
                        <div class="form-group">
                            @if($status == 'active')
                                <a href="/dashboard/users/deactivate/{!! Crypt::encrypt($user->id) !!}" class="btn btn-danger">Deactivate account</a>
                            @elseif($status == 'deactivated')
                                <a href="/dashboard/users/reactivate/{!! Crypt::encrypt($user->id) !!}" class="btn btn-danger">Reactivate account</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="detail-group">
                    <h3>Access Control</h3>

                    <div ng-controller="UserRolesCtrl">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">Roles</button>
                            <hr>
                            <div collapse="isCollapsed">
                                <div class="col-md-6">
                                    <h4>Assigned roles</h4>
                                    <div class="well">
                                        <p>The user has been assigned the following roles:</p>
                                        <ol>
                                            @foreach($user->roles as $role)
                                                <li> {!! $role->description !!}</li>
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h4>Assign Roles to User</h4>
                                    <div class="well">
                                        {!! Form::open(['route'=>'assign_user_role']) !!}

                                        {!! Form::hidden('user_id', Crypt::encrypt($user->id)) !!}
                                        <div class="form-group">
                                            {!! Form::label('role', 'Select Role to Assign user') !!}

                                            {!! Form::select('role', $roles, null, ['class'=>'form-control']) !!}
                                        </div>
                                        {!! Form::submit('Assign Role', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div ng-controller="UserRolesCtrl">
                <div class="detail-group">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-default" ng-init="isCollapsed=true" ng-click="isCollapsed = !isCollapsed">Permissions</button>
                        <hr>
                        <div collapse="isCollapsed">
                            <div class="col-md-6">
                                <div class="well">
                                    <p>The user has been assigned the following permissions: </p>
                                    <ol>
                                        @foreach($user->permissions() as $permission)
                                            <li>{!! $permission->description !!}</li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="well">
                                    {!! Form::open(['route'=>'assign_user_permission']) !!}

                                    {!! Form::hidden('user_id', Crypt::encrypt($user->id)) !!}

                                    <div class="form-group">
                                        {!! Form::label('permission', 'Select Role to Assign user') !!}

                                        {!! Form::select('permission', $permissions, null, ['class'=>'form-control']) !!}
                                    </div>

                                    {!! Form::submit('Assign Permission', ['class'=>'btn btn-success']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop