@extends('layouts.default')

@section('content')
<div class="col-md-8 col-md-offset-2">
    <div class="panel-dashboard">
        <h1>Withdrawal Details</h1>
        <p>Investment has been succesfully withdrawn</p>
        <div class="detail-group">
            <table class="table table-hover">
                <thead>

                </thead>
                <tbody>
                <tr><td>Investment ID</td><td>{!! $investment->id !!}</td></tr>
                <tr><td>Client ID</td><td>{!! $investment->client_id !!}</td></tr>
                <tr><td>Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td></tr>
                <tr><td>Amount Invested</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount, true) !!}</td></tr>
                <tr><td>Amount Withdrawn</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($withdrawal->custodial->amount , true)!!}</td></tr>
                <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                <tr><td>Withdrawal Date</td><td>{!!\Cytonn\Presenters\DatePresenter::formatDate( $withdrawal->client->withdrawal_date )!!}</td></tr>
                <tr><td>Interest rate</td><td>{!! $investment->interest_rate !!}%</tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
@stop