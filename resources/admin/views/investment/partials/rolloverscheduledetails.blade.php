<div class = "detail-group">
    <table class = "table table-hover">
        <tbody>
        <tr>
            <td>Execution Date</td>
            <td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($schedule->action_date) !!}</td>
        </tr>
        <tr>
            <td>Investment Date</td>
            <td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($schedule->payload['invested_date']) !!}</td>
        </tr>
        <tr>
            <td>Maturity Date</td>
            <td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($schedule->payload['maturity_date']) !!}</td>
        </tr>
        <tr>
            <td>Interest Rate</td>
            <td>{!! $schedule->payload['interest_rate'] !!}</td>
        </tr>
        <tr>
            <td>Amount</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->withdrawInstruction->repo->calculateAmountAffected()) !!}</td>
        </tr>
        <tr>
            <td>Commission Recipient</td>
            <td>{!! $recipient($schedule->payload['commission_recepient'])->name !!}</td>
        </tr>
        <tr>
            <td>Commission Rate</td>
            <td>{!! $schedule->payload['commission_rate'] !!}%</td>
        </tr>
        <tr>
            <td>Scheduled Withdraw Amount</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($amount) !!}</td>
        </tr>
        <tr>
            <td>Interest Payment Interval</td>
            @if($schedule->payload['interest_payment_interval'] ==0)
                <td>On Maturity</td>
            @elseif($schedule->payload['interest_payment_interval'] == '1')
                <td>Monthly</td>
            @elseif($schedule->payload['interest_payment_interval'] == '4')
                <td>Quarterly</td>
            @elseif($schedule->payload['interest_payment_interval'] == '6')
                <td>Semi Annually</td>
            @elseif($schedule->payload['interest_payment_interval'] == '12')
                <td>Annually</td>
            @endif
        </tr>
        <tr>
            <td>Interest Payment Date</td>
            <td>{!! $schedule->payload['interest_payment_date'] !!}</td>
        </tr>
        </tbody>
    </table>
    <div class="btn-group">
        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#generate-instruction"><i class="fa fa-download"></i> Generate Instruction</a>
    </div>
    <div class="modal fade" id="generate-instruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['generate_instruction_for_scheduled_transaction', $schedule->id]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Generate Instruction</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('first_signatory', 'First Signatory') !!}</div>
                        <div class="col-md-8">{!! Form::select('first_signatory', $sigs, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory') !!}
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('second_signatory', 'Second Signatory') !!}</div>
                        <div class="col-md-8">{!! Form::select('second_signatory', $sigs, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory') !!}
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('client_bank_account_id', 'Client Bank Account') !!}</div>
                        <div class="col-md-8">{!! Form::select('client_bank_account_id', [null=>$schedule->investment->client->default_bank_account] + $accounts, null, ['class'=>'form-control']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_bank_account_id') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @if(!$investment->withdrawInstruction)
        <a href="/dashboard/investments/clientinvestments/schedule/cancel/{!! $schedule->investment_id !!}" class="btn btn-danger pull-right"><i class="fa fa-remove"></i> Cancel Schedule</a>
    @endif
</div>