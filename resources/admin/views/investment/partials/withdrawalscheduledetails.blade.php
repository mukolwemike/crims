<div class = "detail-group">
    <table class = "table table-hover">
        <tbody>
            @if(isset($schedule->payload['premature']))
                <tr>
                    <td>Premature Withdraw</td>
                    <td> {!! $schedule->payload['premature'] !!}</td>
                </tr>
            @endif
            <tr>
                <td>Invested Amount</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($clientinvestment->amount) !!}</td>
            </tr>
            <tr>
                <td>Invested Date</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientinvestment->invested_date) !!}</td>
            </tr>
            <tr>
                <td>Maturity Date</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientinvestment->maturity_date) !!}</td>
            </tr>
            <tr>
                <td>Scheduled Total</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</td>
            </tr>
            <tr>
                <td>Scheduled Penalty</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($penalty) !!}</td>
            </tr>
            <tr>
                <td>Scheduled Withdrawal</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($amount) !!}</td>
            </tr>
        </tbody>
    </table>
    <div class="btn-group">
        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#generate-instruction"><i class="fa fa-download"></i> Generate Instruction</a>
    </div>
    <div class="modal fade" id="generate-instruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['generate_instruction_for_scheduled_transaction', $schedule->id]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Generate Instruction</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('first_signatory', 'First Signatory') !!}</div>
                        <div class="col-md-8">{!! Form::select('first_signatory', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory') !!}
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('second_signatory', 'Second Signatory') !!}</div>
                        <div class="col-md-8">{!! Form::select('second_signatory', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory') !!}
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('client_bank_account_id', 'Client Bank Account') !!}</div>
                        <div class="col-md-8">{!! Form::select('client_bank_account_id', [null=>$schedule->investment->client->default_bank_account] + $accounts, null, ['class'=>'form-control']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_bank_account_id') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @if(! $clientinvestment->withdrawInstruction)
        <a href="/dashboard/investments/clientinvestments/schedule/cancel/{!! $schedule->investment_id !!}" class="btn btn-danger pull-right"><i class="fa fa-remove"></i> Cancel Schedule</a>
    @endif
</div>