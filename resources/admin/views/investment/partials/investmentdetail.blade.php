<div class = "detail-group">
    @if(isset($inv_title))
        <h4>{!! $inv_title !!}</h4>
    @endif
    <table class = "table table-hover">
        <tbody>
        <tr>
            <td>Investment ID</td>
            <td>{!! $investment->id !!}</td>
        </tr>
        <tr>
            <td>Client Code</td>
            <td>{!! $investment->client->client_code !!}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id) !!}</td>
        </tr>
        <tr>
            <td>Transaction approval</td>
            <td><a href="/dashboard/investments/approve/{!! $investment->approval->id !!}">See approval details</a></td>
        </tr>
        <tr>
            <td>Principal</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
        </tr>
        <tr>
            <td>Net Interest</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getNetInterestForInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Amount Withdrawn</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getWithdrawnAmount()) !!}</td>
        </tr>
        <tr>
            <td>Value Today</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Value on maturity</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment()) !!}</td>
        </tr>
        <tr>
            <td>Investment Date</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
        </tr>
        @if($investment->on_call)
            <tr>
                <td>On Call</td>
                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($investment->on_call) !!}</td>
            </tr>
        @endif
        <tr>
            <td>Maturity Date</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
        </tr>
        <tr>
            <td>Interest rate</td>
            <td>{!! $investment->interest_rate !!}%
        </tr>
        <tr>
            <td>Interest payment frequency</td>
            <?php $interval = $investment->interest_payment_interval ?>
            <td>@if(is_null($interval) || $interval == 0)
                    On Maturity
                @else
                    Every {!! $interval !!} Months on date {!! $investment->interest_payment_date !!}
                @endif
            </td>
        </tr>
        @if($investment->withdrawn == 1)
            <tr>
                <td>Withdrawal Date</td>
                <td>
                    {!! \Cytonn\Presenters\DatePresenter::formatDate($investment->withdrawal_date) !!}
                </td>
            </tr>
            <tr>
                <td>Withdrawal Transaction Approval</td>
                <td><a href="/dashboard/investments/approve/{!! $investment->withdraw_approval_id !!}">See approval details</a></td>
            </tr>
        @else
            <?php $schedule = $investment->approvals()->where('scheduled', true)->whereNotNull('action_date')->whereNull('run_date')->latest()->first() ?>
            @if($schedule)
                <tr>
                    <td>Schedule Status</td>
                    <td>{!! ucfirst(str_replace('_', ' ', $schedule->transaction_type)) !!} Scheduled &nbsp; &nbsp; &nbsp;
                        <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter"
                           href = "/dashboard/investments/clientinvestments/schedule/{!! $investment->id !!}"><i class = "fa fa-list-alt"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    <td>Schedule Status</td>
                    <td>No scheduled action</td>
                </tr>
            @endif
        @endif
        <tr>
            <td>Status</td>
            <td>{!! \Cytonn\Presenters\InvestmentPresenter::presentInvestmentStatus($investment)!!}</td>
        </tr>
        @if(!is_null($investment->description))
            <tr>
                <td>Transaction Description</td>
                <td>{!! $investment->description !!}</td>
            </tr>
        @endif
        <tr>
            <td>Interest Action</td>
            @if(!is_null($investment->interest_action_id))
                <td>{!! $investment->interestAction->name !!}</td>
            @else
                <td>Send to Bank (Default)</td>
            @endif
        </tr>
        </tbody>
    </table>
</div>