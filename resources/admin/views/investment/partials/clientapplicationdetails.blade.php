<div class="detail-group">
    <h4 class="form-separator col-md-12">Application details</h4>

    <div class="form-group">
        <div class="col-md-2">{!! Form::label('product_id', 'Product') !!}</div>

        <div class="col-md-4">{!! Form::select('product_id', $products , null, ['class'=>'form-control', 'init-model'=>'product_id', 'required']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}</div>
    </div>

    <div class="form-group" ng-if="client_id_is_set">
        <div class="col-md-2"> <span class="required-field"><label for="amount">Total Commitment <small>(Current payments balance : <% payments_balance | currency:"" %>)</small></label> </span></div>

        <div class="col-md-4">{!! Form::text('amount', null, ['class'=>'form-control', 'placeholder'=>'Amount invested', 'ng-model'=>'payments_balance', 'required']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>
    </div>

    <div class="form-group" ng-if="!client_id_is_set">
        <div class="col-md-2"> <span class="required-field">{!!Form::label('amount', 'Total Commitment') !!}</span></div>

        <div class="col-md-4">{!! Form::number('amount', null, ['class'=>'form-control', 'placeholder'=>'Amount invested', 'required']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>
    </div>


    <div class="form-group">
        <div class="col-md-2"> <span class="required-field">{!! Form::label('tenor', 'Investment Duration (Tenor in Months)') !!}</span></div>

        <div class="col-md-4">{!! Form::number('tenor', null, ['class'=>'form-control', 'required']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tenor') !!}</div>
    </div>
    <div class="form-group">
        <div class="col-md-2"> <span class="required-field">{!! Form::label('agreed_rate', 'Agreed rate (%)') !!}</span></div>

        <div class="col-md-4">{!! Form::number('agreed_rate', null, ['class'=>'form-control', 'placeholder'=>'Interest rate', 'step'=>'0.01', 'required']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'agreed_rate') !!}</div>
    </div>

</div>

