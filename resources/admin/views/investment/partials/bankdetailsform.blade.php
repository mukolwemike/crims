<?php
    $branch = empty($branch) ? new \App\Cytonn\Models\ClientBankBranch() : $branch;
    $account = empty($account) ? new \App\Cytonn\Models\ClientBankAccount() : $account;
?>
<div  ng-controller="BankBranchController">
    <h4 class="form-separator col-md-12">Bank Account Details</h4>

    <!-- Investor bank informaion -->
    <div class="form-group">
        <div class="col-md-2">{!! Form::label('investor_account_name', 'Account name') !!}</div>

        <div class="col-md-4">{!! Form::text('investor_account_name', $account->account_name, ['class'=>'form-control']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_account_name') !!}</div>
    </div>

    <div class="form-group">
        <div class="col-md-2">{!! Form::label('investor_account_number', 'Account number') !!}</div>

        <div class="col-md-4">{!! Form::text('investor_account_number', $account->account_number, ['class'=>'form-control']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_account_number') !!}</div>
    </div>

    <div class="form-group">
        <div class="col-md-2">{!! Form::label('currency_id', 'Select Currency') !!}</div>
        <div class="col-md-4">
            {!! Form::select('currency_id', $currencies, null, ['class'=>'form-control', 'id'=>'currency_id', 'init-model'=>'currency_id']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'currency_id') !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-2">{!! Form::label('bank_id', 'Select Bank') !!}</div>
        <div class="col-md-4">
            {!! Form::select('bank_id', $banks, $branch->bank_id, ['class'=>'form-control', 'id'=>'bank_id', 'init-model'=>'bank_id']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_id') !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-2">
            <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
            {!! Form::label('branch_id', 'Select Bank Branch') !!}</div>
        <div class="col-md-4">
            <div  ng-show="bank_id">
                <select class="form-control" name="branch_id">
                    <option ng-repeat="(id, name) in branches" value="<% id %>" ng-selected="<% id %> == {{ $branch->id}}" required><% name %></option>
                </select>
            </div>
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'branch_id') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6">
            @if($account->active === 0)
                {!! Form::checkbox('active', 1, $account->active) !!} {!! Form::label('active', 'Active') !!}
            @else
                {!! Form::checkbox('active', 1, true) !!} {!! Form::label('active', 'Active') !!}
            @endif
        </div>
    </div>
</div>