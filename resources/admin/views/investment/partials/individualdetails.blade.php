<div class="detail-group">
    <h4 class="col-md-12">Applicant Details</h4>

    <p class="col-md-4">
        Name:
        @if($app->firstname) {!! $app->firstname !!} @endif
        @if($app->middlename) {!! $app->middlename !!} @endif
        @if($app->lastname) {!! $app->lastname !!} @endif
    </p>
    <p class="col-md-4">Title: @if($app->getTitle) {!! $app->getTitle->name !!} @endif </p>
    <p class="col-md-4">Email: {!!$app->email !!} </p>
    <p class="col-md-4">Gender:  {!!Cytonn\Presenters\GenderPresenter::presentAbbr($app->gender_id) !!} </p>
    <p class="col-md-4">Date of Birth:  {!!Cytonn\Presenters\DatePresenter::formatDate($app->dob) !!}</p>
    <p class="col-md-4">Pin No.: {!! $app->pin_no !!}</p>
    <p class="col-md-4">ID/Passport Number: {!! $app->id_or_passport !!}</p>
    <p class="col-md-4">Postal code: {!! $app->postal_code !!}</p>
    <p class="col-md-4">Postal address: {!! $app->postal_address !!}</p>
    <p class="col-md-4">Country: {!! Cytonn\Presenters\CountryPresenter::present($app->country_id) !!}</p>
    <p class="col-md-4">Phone: {!! $app->phone !!}</p>
    <p class="col-md-4">Home Tel no.: {!!$app->telephone_home!!} </p>
    <p class="col-md-4">Office Tel no.: {!! $app->telephone_office !!}</p>
    <p class="col-md-4">Residential Address: {!! $app->residential_address !!}</p>
    <p class="col-md-4">Town: {!! $app->town !!}</p>
</div>

<div class="detail-group">
    <h4 class="col-md-12">Employment Details</h4>

    <p class="col-md-4">Employment: @if($app->employment){!! $app->employment->name !!}@endif </p>
    <p class="col-md-4">Details: {!! $app->employment_other !!}</p>
    <p class="col-md-4">Sector: {!! $app->business_sector !!}</p>
    <p class="col-md-4">Present Occupation: {!!$app->present_occupation!!} </p>
    <p class="col-md-4">Employer Name: {!! $app->employer_name !!}</p>
    <p class="col-md-4">Employer Address: {!! $app->employer_address !!}</p>
    <p class="col-md-4">Preferred method of Contact: {!! Cytonn\Presenters\ContactMethodPresenter::present($app->method_of_contact_id) !!}</p>
</div>