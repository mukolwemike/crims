<div class="detail-group">
    <h4 class="col-md-12">Type of Investor</h4>

    <p class="col-md-4">Type of Investor: {!! $corporateinvestortype ? $corporateinvestortype->name : '' !!}</p>
    <p class="col-md-4">Details: {!! $app->corporate_investor_type_other !!}</p>
</div>

<div class="detail-group">
    <h4 class="col-md-12">Investor Details</h4>

    <p class="col-md-4">Registered name: {!! $app->registered_name !!}</p>
    <p class="col-md-4">Trade name: {!! $app->trade_name !!}</p>
    <p class="col-md-4">Registered address: {!! $app->registered_address !!}</p>
    <p class="col-md-4">registration number: {!!  $app->registration_number !!}</p>
    <p class="col-md-4">Telephone number: {!! $app->telephone_office !!}</p>
    <p class="col-md-4">Email: {!! $app->email !!}</p>
    <p class="col-md-4">Company pin: {!! $app->pin_no !!}</p>
    <p class="col-md-4">Physical Address: {!! $app->street !!} </p>
    <p class="col-md-4">Preferred method of contact: {!! $app->methodOfContact !!} </p>
    <p class="col-md-4"></p>
</div>

{{--<div class="detail-group">--}}
    {{--<h4 class="col-md-12">Next of kin</h4>--}}
    {{--<table class = "table table-striped table-responsive">--}}
        {{--<thead>--}}
        {{--<tr>--}}
            {{--<th>Name</th>--}}
            {{--<th>Email</th>--}}
            {{--<th>Address</th>--}}
            {{--<th>Phone Number</th>--}}
        {{--</tr>--}}
        {{--</thead>--}}
        {{--<tbody>--}}
        {{--@if($app->kin_phone)--}}
            {{--<tr>--}}
                {{--<td>{!! $app->kin_name !!}</td>--}}
                {{--<td>{!! $app->kin_email !!}</td>--}}
                {{--<td>{!! $app->kin_phone !!}</td>--}}
                {{--<td>{!! $app->kin_postal !!}</td>--}}
            {{--</tr>--}}
        {{--@endif--}}
        {{--@if($contactPersons)--}}
            {{--@foreach($contactPersons as $persons)--}}
                {{--<tr>--}}
                    {{--<td>{!! $persons->name !!}</td>--}}
                    {{--<td>{!! $persons->email !!}</td>--}}
                    {{--<td>{!! $persons->address !!}</td>--}}
                    {{--<td>{!! $persons->phone !!}</td>--}}
                {{--</tr>--}}
            {{--@endforeach--}}
        {{--@endif--}}
        {{--</tbody>--}}
    {{--</table>--}}
{{--</div>--}}