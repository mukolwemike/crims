
<?php
try {
    $approved = $app->approval->approved;
    $account = ($app->client) ? $app->client->bankAccounts()->first(): null;
    $branch = is_null($account) ? null : \App\Cytonn\Models\ClientBankBranch::find($account->branch_id);
    $branch = is_null($branch) ? $app->branch : $branch;
} catch (Exception $e) {
    $approved = false;
}
?>

<div class="col-md-12">
    <br>
    <h5>INVESTMENT DETAILS</h5>
    <hr>
    @include('clients.application.partials.investment')
</div>


<div class="col-md-12">
    <br>
    <h5>APPLICATION DETAILS</h5>
    <hr>
    @include('clients.application.partials.application')
</div>

<div class="col-md-12">
    <br>
    <h5>SOURCE OF FUNDS</h5>
    <hr>
    @include('clients.application.partials.source')
</div>

<div class="col-md-12">
    <br>
    <h5>INVESTOR BANK INFORMATION</h5>
    <hr>
    @include('clients.application.partials.bank')
</div>

<div class="col-md-12">
    <div class="">
        <el-collapse>
            <el-collapse-item title="CONTACT PERSONS" name="CONTACT PERSONS">
                <div class="panel panel-success">
                    @include('clients.application.partials.contact-persons')
                </div>
            </el-collapse-item>
        </el-collapse>
    </div>
</div>

<div class="col-md-12">
    <div class="">
        <el-collapse>
            <el-collapse-item title="JOINT HOLDERS" name="JOINT HOLDERS">
                <div class="panel panel-success">
                    @include('clients.application.partials.joint-holder')
                </div>
            </el-collapse-item>
        </el-collapse>
    </div>
</div>

<div class="col-md-12">
    <div class="">
        <el-collapse>
            <el-collapse-item title="DOCUMENTS" name="DOCUMENTS">
                @include('clients.application.partials.documents')
            </el-collapse-item>
        </el-collapse>
    </div>
</div>

@if($approved)
    <div class="col-md-12">
        @if($app->repo->status() != 'invested')
            @if(Auth::user()->isAbleTo('addinvestment'))
                <div class = "">
                    <div class = "col-md-6">
                        <h4>Invest Application</h4>
                        <a href = "/dashboard/investments/applications/invest/{!! $app->id !!}"
                           class = "btn btn-success">Investment application</a>
                    </div>
                </div>
            @else
                <div class = "alert alert-warning"><p>This application has not been invested</p></div>
            @endif
        @else
            <div class = "alert alert-info"><p>This application has already been invested</p></div>
        @endif
    </div>
@else
    <div class="col-md-12">
        <div class = "alert alert-warning"><p>This application has not been approved, it has to be approved before you can invest it</p></div>

        <a href = "/dashboard/investments/applications/{!! $app->id !!}/request"
           class = "btn btn-success">Resend Approval Request</a>
    </div>
@endif
