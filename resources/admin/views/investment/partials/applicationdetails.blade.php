<div class="col-md-12"><h3 class="center">Application Details</h3></div>
<div class="clearfix"></div>

<div class="detail-group">
    <h4 class="col-md-12">Application</h4>
    <p class="col-md-4">Product: {!! Product::find($app->product_id)->name !!} </p>
    <p class="col-md-4">Total Commitment:  {!!$app->amount!!} </p>
    <p class="col-md-4">Partnership Account Number: {!! $app->partnership_account_number !!}</p>
    <p class="col-md-4">Agreed rate of return: {!! $app->agreed_rate.'%'!!} </p>
    <p class="col-md-4">Investment Date: {!! Cytonn\Presenters\DatePresenter::formatDate($app->date) !!}</p>
    <p class="col-md-4">Tenor In Months: {!! $app->tenor !!}</p>
</div>


@if($app->type_id == 1)
    @include('investment.partials.individualdetails', ['app'=>$app])
@endif

@if($app->type_id == 2)
    @include('investment.partials.corporatedetails', ['app'=>$app])
@endif

<div class="detail-group">
    <h4 class="col-md-12">Source of Funds</h4>

    <p class="col-md-4">Source of Funds: {!! FundSource::find($app->funds_source_id)->name !!}</p>
    <p class="col-md-6">Details: {!! $app->funds_source_other !!}</p>
</div>

<div class="detail-group">
    <h4 class="col-md-12">Investor Bank information</h4>

    <p class="col-md-4">Account name: {!! $app->investor_account_name !!}</p>
    <p class="col-md-4">Account number: {!! $app->investor_account_number !!}</p>
    <p class="col-md-4">Bank: {!! $app->investor_bank !!}</p>
    <p class="col-md-4">Branch: {!! $app->investor_bank_branch !!}</p>
    <p class="col-md-4">Clearing Code: {!! $app->investor_clearing_code !!}</p>
    <p class="col-md-4">Swift code: {!! $app->investor_swift_code !!}</p>
    <p class="col-md-4">Terms accepted: {!! Cytonn\Presenters\BooleanPresenter::presentIcon($app->terms_accepted) !!}</p>
</div>