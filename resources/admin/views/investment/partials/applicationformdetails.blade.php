<div class="detail-group">
    <h4 class="form-separator col-md-12">Source of funds</h4>
    <!-- Source of funds -->

    <div class="form-group">
        <div class="col-md-2">{!! Form::label('funds_source_id', 'Source of funds') !!}</div>

        <div class="col-md-4">{!! Form::select('funds_source_id', $sources_of_funds, null, ['class'=>'form-control']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'funds_source_id') !!}</div>
    </div>


    <div class="form-group">
        <div class="col-md-2">{!! Form::label('funds_source_other', 'Other source of funds') !!}</div>

        <div class="col-md-4">{!! Form::text('funds_source_other', null, ['class'=>'form-control']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'funds_source_other') !!}</div>
    </div>

</div>

<div class="detail-group" ng-if="new_client == 1 || new_client == 2">
    @include('investment.partials.bankdetailsform')

    {{--<div class="form-group">--}}
        {{--<div class="col-md-2"> <span class="required-field">{!! Form::label('investor_bank', 'Bank') !!}</span></div>--}}

        {{--<div class="col-md-4">{!! Form::text('investor_bank', null, ['class'=>'form-control', 'required']) !!}--}}
            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_bank') !!}</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--<div class="col-md-2"> <span class="required-field">{!! Form::label('investor_bank_branch', 'Branch') !!}</span></div>--}}

        {{--<div class="col-md-4">{!! Form::text('investor_bank_branch', null, ['class'=>'form-control', 'required']) !!}--}}
            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_bank_branch') !!}</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--<div class="col-md-2">{!! Form::label('investor_clearing_code', 'Clearing code') !!}</div>--}}

        {{--<div class="col-md-4">{!! Form::text('investor_clearing_code', null, ['class'=>'form-control']) !!}--}}
            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_clearing_code') !!}</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
        {{--<div class="col-md-2">{!! Form::label('investor_swift_code', 'Swift Code') !!}</div>--}}

        {{--<div class="col-md-4">{!! Form::text('investor_swift_code', null, ['class'=>'form-control']) !!}--}}
            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_swift_code') !!}</div>--}}
    {{--</div>--}}

    <div class="form-group col-md-12">
        <div class="col-md-3">{!! Form::checkbox('terms_accepted', true, ['class'=>'form-control']) !!}

        {!! Form::label('terms_accepted', 'Terms accepted') !!}

            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'terms_accepted') !!}</div>
        <div class="pull-right">All fields with  (<span class="required-field"></span>) are required and must be filled before submitting.</div>
    </div>
</div>
