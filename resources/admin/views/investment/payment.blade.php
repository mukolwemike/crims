@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @include('investment.partials.investmentdetail')
                <div class="detail-group">
                    <h4>Payment trail</h4>

                    <table class="table table-hover table-responsive table-striped">
                        <thead></thead>
                        <tbody>
                        <tr><th>Date</th><th>Amount</th></tr>
                            @foreach($investment->payments as $payment)
                                <tr><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date_paid) !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount)!!}<a class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#reverse-pay-modal-{!! $payment->id !!}"><i class="fa fa-undo"></i> Reverse</a></td></tr>

                                <!-- Modal -->
                                <div class="modal fade" id="reverse-pay-modal-{!! $payment->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Reverse Interest Payment</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to reverse the
                                                    {!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}

                                                    paid on {!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date_paid) !!}?
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                                <a href="/dashboard/investments/payment/reverse/{!! $payment->id !!}" type="button" class="btn btn-danger">Yes, reverse payment</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                                <tr>
                                    <th>Total</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->payments()->sum('amount')) !!}</th>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6">
                <div class="detail-group">
                    <h4>Interest Payment Schedule</h4>

                    <table class="table table-responsive table-hover">
                        <thead>
                            <tr><th>Date</th><th>Description</th><th>Amount</th></tr>
                        </thead>
                        <tbody>
                            @foreach($schedules as $schedule)
                                <tr>
                                    <td>{!! (new \Carbon\Carbon($schedule->date))->toDateString() !!}</td>
                                    <td>{!! $schedule->description !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                                    <td>

                                        <!-- Button to trigger edit modal for interest payment schedule  -->

                                        <a ng-controller="PopoverCtrl" uib-popover="Edit" popover-trigger="mouseenter" data-toggle="modal" data-target="#editModal{!! $schedule->id !!}">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>

                                        <!-- Start of Edit Interest Payment Schedule modal-->

                                        <div class="modal fade" id="editModal{!! $schedule->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header"><h3>Edit Interest Payment Schedule</h3></div>
                                                    {!! Form::model($schedule,['route'=>['update_interest_payment_schedule', $schedule->id]]) !!}
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            {!! Form::hidden('schedule_id', $schedule->id) !!}
                                                            {!! Form::hidden('investment_id', $investment->id) !!}
                                                        </div>
                                                        <div class="form-group" ng-controller="DatepickerCtrl">
                                                            {!! Form::label('date', 'Scheduled Date') !!}
                                                            {!! Form::text('date', null, ['id'=>'date', 'class'=>'form-control', 'datepicker-popup init-model'=>'date', 'is-open'=>'status.opened', 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::label('amount', 'Amount') !!}
                                                            {!! Form::number('amount', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::label('description', 'Description') !!}
                                                            {!! Form::text('description', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::checkbox('fixed') !!}
                                                            {!! Form::label('fixed', 'Fixed Payment') !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fixed') !!}
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Update</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>

                                        <!-- End of interest payment edit modal -->

                                    </td>
                                </tr>
                            @endforeach
                                <tr><td colspan="4"></td></tr>
                                {{--<tr><td></td><th>Total Interest</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($schedules->sum('amount')) !!}</th></tr>--}}
                                <tr><td></td><td>Total Withdrawn</td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->payments()->sum('amount')) !!}</th></tr>
                                {{--<tr><td>{!! \Carbon\Carbon::now()->toDateString() !!}</td><th>Interest Available (As at next day)</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($remaining) !!}</th></tr>--}}
                        </tbody>
                    </table>

                    <a href="#" class="btn btn-default" ng-click="show_form=!show_form">Add Schedule</a>
                    <div class="detail-group" ng-show="show_form">
                        <h4>Add Schedule</h4>
                        {!! Form::open(['route'=>['add_interest_payment_schedule', $investment->id]]) !!}
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('date', 'Scheduled Date') !!}
                                {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'payment_date']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('amount', 'Amount') !!}
                                {!! Form::text('amount', null, ['class'=>'form-control', 'ng-required'=>"true", 'init-model'=>'amount']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::text('description', null, ['class'=>'form-control', 'ng-required'=>"true", 'init-model'=>'description']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::checkbox('fixed') !!}
                                {!! Form::label('fixed', 'Fixed Payment') !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fixed') !!}
                            </div>
                        </div>
                            <div class="form-group">
                                {!! Form::submit('Schedule', ['class'=>'btn btn-success']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <a href="{{ route('withdraw_path', ['id'=>$investment->id]) }}" class="btn btn-primary">Withdraw</a>
                {{--<a href="#" class="btn btn-default" ng-click="show=!show">Add Payment</a>--}}


                {{--<div class="detail-group" ng-controller = "InterestPaymentCtrl" ng-show="show">--}}
                    {{--<h4>New payment</h4>--}}

                    {{--<div class="hide">--}}
                        {{--{!! Form::text('investment_id', $investment->id, ['init-model'=>'investment_id']) !!}--}}
                    {{--</div>--}}

                    {{--{!! Form::open(['name'=>'form', 'url'=>['/dashboard/investments/pay/' . $investment->id], 'method'=>'POST']) !!}--}}

                    {{--<div class="form-group">--}}
                        {{--{!! Form::label('date_paid', 'Date of payment') !!}--}}

                        {{--{!! Form::text('date_paid', null,--}}
                        {{--[--}}
                            {{--'class'=>'form-control', 'datepicker-popup init-model'=>"payment_date", 'is-open'=>"status.opened",--}}
                            {{--'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'date'--}}
                        {{--]) !!}--}}

                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date_paid') !!}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--{!! Form::label('amount', 'Amount Paid (Available: <% payable | currency:"" %>)') !!}--}}

                        {{--{!! Form::text('amount', null, ['class'=>'form-control', 'ng-required'=>"true", 'init-model'=>'amount']) !!}--}}

                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}--}}
                    {{--</div>--}}

                    {{--<div class = "form-group">--}}
                        {{--{!! Form::label('interest_action_id', 'Select Action') !!}--}}

                        {{--{!! Form::select('interest_action_id', $interestaction, NULL, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}--}}
                    {{--</div>--}}

                    {{--{!! Form::submit('Pay', ['class'=>'btn btn-success']) !!}--}}

                    {{--{!! Form::close() !!}--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection



<script type = "text/ng-template" id = "paymentconfirm.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">

                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Payment</h4>
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Are you sure you want to topup this investment with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                            </tr>
                            <tr>
                                <td>Investment amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            </tr>
                            <tr>
                                <td>Payment Amount</td><td><% amount | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Date of Payment</td><td><% payment_date | date %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open() !!}
                            <div style="display: none !important;">
                                {!! Form::text('amount', null, ['ng-model'=>'amount']) !!}
                                {!! Form::text('date_paid', null, ['ng-model'=>'payment_date']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>