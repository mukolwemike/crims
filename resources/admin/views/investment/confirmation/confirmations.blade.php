@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "BusinessConfirmationController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan = "6">
                        <div class="btn-group">
                            <a type="button" href="#" class="btn btn-success">Investment</a>
                            <a href="/dashboard/coop/membership/confirmations" class="btn btn-info">Memberships</a>
                            <a type="button" href="/dashboard/shares/confirmations" class="btn btn-primary">Shares</a>
                            <router-link v-bind:to="{ name: 'unit-fund-business-confirmations.index' }" class="btn btn-default">Unitization</router-link>
                            <router-link v-bind:to="{ name: 'unit-fund-redemption-confirmations.index' }" class="btn btn-warning">Redemptions</router-link>
                        </div>
                    </th>
                    {{--<th>--}}
                        {{--<select st-search="strictSelectValue">--}}
                            {{--<option value="">All</option>--}}
                            {{--<option ng-repeat="row in rowCollection | unique:'confirmation'" value="<% row.confirmation %>"><span to-html = "row.confirmation | confirmationStatus"></span></option>--}}
                        {{--</select>--}}
                    {{--</th>--}}
                    {{--<th colspan="2">--}}
                        {{--<st-date-range predicate="invested_date" before="query.before" after="query.after"></st-date-range>--}}
                    {{--</th>--}}
                    {{--<th colspan="2"><input st-search="client.client_code" placeholder="search by client code" class="form-control" type="text"/></th>--}}

                    <th colspan="2">
                        <select st-search="confirmation" class="form-control">
                            <option value="">All</option>
                            <option value="sent">Sent</option>
                            <option value="not_sent">Not Sent</option>
                        </select>
                    </th>
                    <th colspan="2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th st-sort="invested_date" st-sort-default="reverse">Investment Date</th>
                    <th st-sort="maturity_date">Maturity Date</th>
                    <th st-sort="amount">Amount</th>
                    <th>Confirmation</th>
                    <th>Investment Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                    <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td><% row.id %></td>
                            <td><% row.client_code %></td>
                            <td><% row.fullName %></td>
                            <td><% row.type %></td>
                            <td><% row.description %></td>
                            <td><% row.invested_date | date%></td>
                            <td><% row.maturity_date | date%></td>
                            <td><% row.amount | currency:"" %></td>
                            <td><span to-html = "row.confirmation | confirmationStatus"></span></td>
                            <td><span to-html = "row | investmentStatus"></span></td>
                            <td>
                                <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/investments/confirmation/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="10" class="text-center">Loading ... </td>
                        </tr>
                    </tbody>
                <tfoot>
                <tr>
                    <td colspan = "4" class = "text-center">
                        Items per page
                    </td>
                    <td colspan = "2" class = "text-center">
                        <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-displayed-pages = "10"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop