@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Send Business Confirmation</h3>
            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Client Code</td>
                    <td>{!! $investment->client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                </tr>
                <tr>
                    <td>Principal</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                </tr>
                <tr>
                    <td>Interest rate</td>
                    <td>{!! $investment->interest_rate !!}
                </tr>
                <tr>
                    <td>Start Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
                <tr>
                    <td>Duration</td>
                    <td>{!! $investment->repo->getTenorInDays() !!} Days</td>
                </tr>
                <tr>
                    <td>Compliance</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($investment->client->repo->compliant()) !!}</td>
                </tr>
                <tr>
                    <td>Sent</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($is_bc_sent) !!}</td>
                </tr>
                </tbody>
            </table>

            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>Business confirmation</h4>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Principal</th>
                            <th>Interest Rate</th>
                            <th>Duration</th>
                            <th>Start date</th>
                            <th>Maturity date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            <td>{!! $investment->interest_rate !!} %</td>
                            <td>{!! $investment->repo->getTenorInDays() !!} Days</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        </tr>
                        </tbody>
                    </table>

                    @if($investment->withdrawn != 1)
                        <div class="">
                            @if($pendingKycs->count())
                                <div class="alert alert-warning">
                                    <p class="text-warning">
                                        The Following Documents have not been uploaded for this client
                                    </p>
                                    <ol>
                                        @foreach($pendingKycs as $pendingKyc)
                                            <li>{{$pendingKyc}}</li>
                                        @endforeach
                                    </ol>
                                </div>

                                <div>
                                    <a href="/dashboard/investments/confirmation/acknowledgement_notification/{!! $investment->id !!}"
                                       class="btn btn-warning"><i class="fa fa-envelope"></i> Send Acknowledgement
                                        Notification</a>
                                </div>
                            @endif
                        </div>
                        <br>

                        @if($is_bc_sent)
                            <a href="/dashboard/investments/confirm/preview/{!! $investment->id !!}"
                               class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Preview business
                                confirmation</a>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#confirmBCResend"><i
                                        class="fa fa-envelope"></i> Resend Business Confirmation
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="confirmBCResend" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open() !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Send Business Confirmation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>This business confirmation has already been sent. Are you sure that you
                                                want
                                                to resend it?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            {!! Form::submit('Resend', ['class'=>'btn btn-danger']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @else

                            {!! Form::open() !!}
                            <a href="/dashboard/investments/confirm/preview/{!! $investment->id !!}"
                               class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Preview business
                                confirmation</a>
                            {!! Form::button('<i class="fa fa-envelope"></i> Send Business Confirmation', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                            {!! Form::close() !!}

                        @endif
                    @endif
                </div>

                <div class="panel-heading">
                    <h4>Previous Business confirmations</h4>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Principal</th>
                            <th>Interest Rate</th>
                            <th>Duration</th>
                            <th>Start date</th>
                            <th>Maturity date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($previousConfirmations as $prevConfirmation)
                            <tr>
                                <td> {!! $prevConfirmation->payload['principal'] !!}</td>
                                <td>{!! $prevConfirmation->payload['interest_rate'] !!} %</td>
                                <td>{!! $prevConfirmation->payload['duration'] !!} Days</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($prevConfirmation->payload['invested_date']) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($prevConfirmation->payload['maturity_date']) !!}</td>
                                <td>
                                    <a ng-controller="PopoverCtrl" uib-popover="View PDF" popover-trigger="mouseenter"
                                       href="/dashboard/investments/confirmation/previous/preview/{!! $prevConfirmation->id !!}"><i
                                                class="fa fa-list-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-heading">
                    <h4>Payment Schedules</h4>
                </div>
                <div class="panel-body">
                    <a href="/dashboard/investments/investment_payment_schedules/preview/{!! $investment->id !!}"
                       class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Preview Payment Schedule</a>
                </div>
            </div>
        </div>
    </div>
@endsection