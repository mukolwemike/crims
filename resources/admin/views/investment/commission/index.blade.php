@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="#investments" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">Investments</a></li>
                <li role="presentation" class=""><a href="#unitization" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">Unitization</a></li>
            </ul>
            <div id="myTabContent" class="tab-content margin-top-25">
                <div role="tabpanel" class="tab-pane fade" id="investments" aria-labelledby="section-tab">
                    @include('investment.commission.partials.index')
                </div>
                <div role="tabpanel" class="tab-pane fade" id="unitization" aria-labelledby="section-tab">
                    <unit-fund-commissions></unit-fund-commissions>
                </div>
            </div>
        </div>
    </div>
@endsection