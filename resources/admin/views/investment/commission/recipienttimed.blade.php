@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach(Product::all() as $product)
                    <li role="presentation" class=""><a href="#{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>

            <div id="myTabContent" class="tab-content">
                @foreach(Product::all() as $product)
                    <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">
                        <div ng-controller = "recipientTimedScheduleCtrl">
                            {!! Form::text('product_id', $product->id, ['init-model'=>'product_id', 'style'=>'display: none !important;']) !!}
                            <table st-table = "displayedCollection" st-safe-src = "rowCollection" class = "table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th colspan = "3"></th>
                                    <th colspan = "2">{!! Form::text('date', \Carbon\Carbon::today()->toDateString(), ['class'=>'form-control', 'datepicker-popup init-model'=>"date_picked", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</th>
                                    <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                                </tr>
                                <tr>
                                    <th st-sort = "name">FA Name</th>
                                    <th>Value Date</th>
                                    <th>Maturity Date</th>
                                    <th>Commission Rate</th>
                                    <th st-sort = "date">Due Date</th>
                                    <th st-sort = "amount">Payment Amount</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat = "row in displayedCollection">
                                    <td><% row.name %></td>
                                    <td><% row.value_date  | date %></td>
                                    <td><% row.maturity_date  | date %></td>
                                    <td><% row.commission_rate%>%</td>
                                    <td><% row.date | date %></td>
                                    <td><% row.amount %></td>
                                    <td><a href="/dashboard/investments/commission/schedule/<% row.id%>"><i class="fa fa-list-alt"></i></a></td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan = "2" class = "text-center">
                                        Items per page
                                    </td>
                                    <td colspan = "2" class = "text-center">
                                        <input type = "text" ng-model = "itemsByPage"/>
                                    </td>
                                    <td colspan = "4" class = "text-center">
                                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-displayed-pages = "10"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection