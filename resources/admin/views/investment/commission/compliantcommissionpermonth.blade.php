@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">

        <div class="col-md-8">
            <div class="alert alert-info margin-top-10">
                <p>The date will be automatically adjusted to end of month, select any date within the month of interest</p>
            </div>
        </div>
        <div class="col-md-4 pull-right">
            {!! Form::open() !!}

            <div class="col-md-8">
                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('Enter date') !!}

                    {!! Form::text('date', $date, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::submit('Submit', ['class'=>'btn btn-success margin-top-25']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="clearfix"></div>
        <ul id = "myTabs" class = "nav nav-tabs" role = "tablist">
            @foreach($currencies as $currency)
                <li role = "presentation" class = "">
                    <a href = "#{!! $currency->id !!}" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">{!! $currency->name !!}</a>
                </li>
            @endforeach
        </ul>

        <div id = "myTabContent" class = "tab-content">
            @foreach($currencies as $currency)
                <div role = "tabpanel" class = "tab-pane fade" id = "{!! $currency->id !!}" aria-labelledby = "home-tab" ng-controller = "DatepickerCtrl">


                    <table class="table table-responsive table-hover margin-top-20">
                        <thead>
                            <tr>
                                <th>FA Name</th><th>Amount</th><th>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($currency->recipients as $recipient)
                                <?php
                                    $v = $recipient->amount;
                                    $total += $v;
                                ?>
                                <tr>
                                    <td>{!! $recipient->name !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($v) !!}</td>
                                    <td>
                                        <a href="/dashboard/investments/commission/schedule/permonth/compliant/{!! $recipient->id !!}"><i class="fa fa-list-alt"></i></a>
                                        <a href="/dashboard/investments/commission/payment/{!! $recipient->id !!}"><i class="fa fa-money"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            @endforeach
        </div>
    </div>
@endsection