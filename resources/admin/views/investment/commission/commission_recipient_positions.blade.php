@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                <div ng-controller="CommissionRecipientPositionGridController"
                     ng-init="commission_recepient_id ='{!! $commissionRecipient->id !!}'">
                    <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                        <thead>
                        <input type="text" ng-model="commission_recepient_id" style="display:none;">
                        <tr>
                            <th colspan="4">
                                {{--<a class="margin-bottom-20 btn btn-success" href="/dashboard/investment/commission/add/"> <i class="fa fa-plus"></i> Sync FAs</a>--}}
                                <h2>{{$commissionRecipient->name}}&nbsp;Positions</h2>
                            </th>
                            <th colspan="2">
                                <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                            </th>
                        </tr>
                        <tr>
                            {{--<th st-sort="id">FA ID</th>--}}
                            <th width="20%;">FA Type</th>
                            <th width="20%;">Rank</th>
                            <th width="20%;">Supervisor</th>
                            <th width="20%;">Start Date</th>
                            <th width="20%;">End Date</th>
                        </tr>
                        </thead>
                        <tbody ng-show="!isLoading">
                        <tr ng-repeat="row in displayed">
                            {{--<td><% row.id %></td>--}}
                            <td width="20%;"><% row.fa_type %></td>
                            <td width="20%;"><% row.fa_rank %></td>
                            <td width="20%;"><% row.supervisor %></td>
                            <td width="20%;"><% row.start_date %></td>
                            <td width="20%;"><% row.end_date %></td>
                            <td></td>
                        </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="100%" class="text-center">Loading ...</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2" class="text-center">
                                Items per page
                            </td>
                            <td colspan="1" class="text-center">
                                <input type="text" ng-model="itemsByPage"/>
                            </td>
                            <td colspan="4" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage"
                                     st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection