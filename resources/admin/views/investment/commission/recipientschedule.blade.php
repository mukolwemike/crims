@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <h3>{!! $recipient->name !!}</h3>

            <hr/>
            <ul id="myTabs" class="nav nav-tabs margin-bottom-20" role="tablist">
                <li role="presentation"><a href="#present" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">This Month</a></li>
                <li role="presentation"><a href="#past" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Past Payments</a></li>
                <li role="presentation"><a href="#future" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Future Payments</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="present" aria-labelledby="present-tab">
                    <ul id="myTabs" class="nav nav-pills" role="tablist">
                        @foreach(Product::all() as $product)
                            <li role="presentation" class=""><a href="#present{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                        @endforeach
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        @foreach(Product::all() as $product)
                            <div role="tabpanel" class="tab-pane fade" id="present{!! $product->id !!}" aria-labelledby="home-tab">
                                <div ng-controller="faPaymentGridCtrl">
                                    <div style="display: none !important;">
                                        {!! Form::text('fa_id', $recipient->id, ['init-model'=>'fa_id']) !!}
                                        {!! Form::text('period', 'present', ['init-model'=>'period']) !!}
                                        {!! Form::text('product_id', $product->id, ['init-model'=>'product_id']) !!}
                                    </div>
                                    @include('investment.commission.partials.schedule_table')
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade in active" id="past" aria-labelledby="present-tab">
                <ul id="myTabs" class="nav nav-pills" role="tablist">
                    @foreach(Product::all() as $product)
                        <li role="presentation" class=""><a href="#past{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                    @endforeach
                </ul>
                <div id="myTabContent" class="tab-content">
                    @foreach(Product::all() as $product)
                        <div role="tabpanel" class="tab-pane fade" id="past{!! $product->id !!}" aria-labelledby="home-tab">
                            <div ng-controller="faPaymentGridCtrl">
                                <div style="display: none !important;">
                                    {!! Form::text('fa_id', $recipient->id, ['init-model'=>'fa_id']) !!}
                                    {!! Form::text('period', 'past', ['init-model'=>'period']) !!}
                                    {!! Form::text('product_id', $product->id, ['init-model'=>'product_id']) !!}
                                </div>
                                @include('investment.commission.partials.schedule_table')
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade in active" id="future" aria-labelledby="present-tab">
            <ul id="myTabs" class="nav nav-pills" role="tablist">
                @foreach(Product::all() as $product)
                    <li role="presentation" class=""><a href="#future{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach(Product::all() as $product)
                    <div role="tabpanel" class="tab-pane fade" id="future{!! $product->id !!}" aria-labelledby="home-tab">
                        <div ng-controller="faPaymentGridCtrl">
                            <div style="display: none !important;">
                                {!! Form::text('fa_id', $recipient->id, ['init-model'=>'fa_id']) !!}
                                {!! Form::text('period', 'future', ['init-model'=>'period']) !!}
                                {!! Form::text('product_id', $product->id, ['init-model'=>'product_id']) !!}
                            </div>
                            @include('investment.commission.partials.schedule_table')
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

            </div>
        </div>
    </div>
    <script>
        $('#present1').tab('show');
    </script>
@endsection