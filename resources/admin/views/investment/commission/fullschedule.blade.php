@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard">
            <div>
                <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    @foreach(Product::all() as $product)
                        <li role="presentation" class=""><a href="#{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                    @endforeach
                </ul>
                <div id="myTabContent" class="tab-content">
                    @foreach(Product::all() as $product)
                        <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">
                            <div ng-controller = "fullScheduleGridCtrl">
                                {!! Form::text('product_id', $product->id, ['init-model'=>'product_id', 'style'=>'display: none !important;']) !!}
                                <table st-set-filter = "customRangeFilter" st-table = "displayedCollection" st-safe-src = "rowCollection" class = "table table-striped table-responsive">
                                    <thead>
                                    <tr>
                                        <th colspan = "2"></th>
                                        <th colspan = "2">
                                            <st-date-range predicate = "date" before = "query.before" after = "query.after"></st-date-range>
                                        </th>
                                        <th><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                                    </tr>
                                    <tr>
                                        <th st-sort = "client_name">Client Name</th>
                                        <th st-sort = "investment.amount">Client Invested Amount</th>
                                        <th st-sort = "date">Due Date</th>
                                        <th st-sort = "description">Description</th>
                                        <th st-sort = "amount">Payment Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat = "row in displayedCollection">
                                        <td><% row.client_name %></td>
                                        <td><% row.investment.amount | currency:"" %></td>
                                        <td><% row.date | date %></td>
                                        <td><% row.description %></td>
                                        <td><% row.amount %></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan = "2" class = "text-center">
                                            Items per page
                                        </td>
                                        <td colspan = "2" class = "text-center">
                                            <input type = "text" ng-model = "itemsByPage"/>
                                        </td>
                                        <td colspan = "4" class = "text-center">
                                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-displayed-pages = "10"></div>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class = "row">
            <span class = "col-md-6">From: <input is-open = "isAfterOpen" ng-change = "rangeChanged()" datepicker-popup = "yyyy/MM/dd" ng-focus = "openAfter($event)" ng-model = "after" type = "text" class = "form-control margin-bottom-20"/></span>
            <span class = "col-md-6">To: <input is-open = "isBeforeOpen" ng-change = "rangeChanged()" ng-model = "before" datepicker-popup = "yyyy/MM/dd" ng-focus = "openBefore($event)" type = "text" class = "form-control margin-bottom-20"/></span>
        </div>
    </script>
@endsection