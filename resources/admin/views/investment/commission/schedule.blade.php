@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach(Product::all() as $product)
                    <li role="presentation" class=""><a href="#{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach(Product::all() as $product)
                    <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <td><a href="/dashboard/investments/commission/fullschedule" class="btn btn-success"><i class="fa fa-calendar-o"></i> View full schedule</a></td>
                                <td></td>
                            </tr>
                            <tr><td colspan="7"><div class="alert alert-info"><p>Displaying upcoming commission payments for the month
                                            <a href="/dashboard/investments/commission/schedule/timed" class="btn btn-sm btn-primary pull-right">View other months</a></p></div></td></tr>
                            <tr>
                                <th>FA Name</th>
                                <th>Value Date</th>
                                <th>Maturity Date</th>
                                <th>Commission Rate</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                                <th>details</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $recipients = $repo->getRecipientsScheduleForProductThisMonth($product); ?>
                            @foreach($recipients as $recipient)
                                <tr>
                                    <td>{!! $recipient->name !!}</td>
                                    @if($recipient->schedules->first())
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($repo->getClientInvestmentValueDate($recipient->schedules->first()->commission_id ))!!}</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($repo->getClientInvestmentMaturityDate($recipient->schedules->first()->commission_id ))!!} </td>
                                        <td>{!! $repo->getCommissionRateForRecipient($recipient->recipient_type_id) !!}%</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($recipient->schedules[0]->date) !!}</td>
                                    @else
                                        <td></td>
                                        <td></td>
                                        <td>{!! $repo->getCommissionRateForRecipient($recipient->recipient_type_id) !!}%</td>
                                        <td></td>
                                    @endif
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($repo->getTotalCommissionForProductForRecipientThisMonth($product, $recipient)) !!}</td>
                                    <td><a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/commission/schedule/{!! $recipient->id !!}"><i class="fa fa-list-alt"></i></a></td></tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection