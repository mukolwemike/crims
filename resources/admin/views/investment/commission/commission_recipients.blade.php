@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                <div ng-controller="CommissionRecipientGridController">
                    <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th colspan="4">
                                {{--<a class="margin-bottom-20 btn btn-success" href="/dashboard/investment/commission/add"> <i class="fa fa-plus"></i> Sync FAs</a>--}}
                            </th>
                            <th colspan="4">
                                <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                            </th>
                        </tr>
                        <tr>
                            <th st-sort="id">FA ID</th>
                            <th>Name</th>
                            <th st-sort="email">Email</th>
                            <th>Report To</th>
                            <th>Type</th>
                            <th>Rank</th>
                            <th>Status</th>
                            <th>Positions</th>
                        </tr>
                        </thead>
                        <tbody  ng-show="!isLoading">
                        <tr ng-repeat="row in displayed">
                            <td><% row.id %></td>
                            <td><% row.name %></td>
                            <td><% row.email %></td>
                            <td><% row.supervisor %></td>
                            <td><% row.type %></td>
                            <td><% row.rank %></td>
                            <td><% row.status %></td>
                            <td>
                                <a ng-controller="PopoverCtrl" uib-popover="View FA Positions" popover-trigger="mouseenter" href="/dashboard/investment/commission_recipients/details/<%row.id%>"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="100%" class="text-center">Loading ... </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td  colspan="3" class="text-center">
                                Items per page
                            </td>
                            <td colspan="2" class="text-center">
                                <input type="text"  ng-model="itemsByPage"/>
                            </td>
                            <td colspan="3" class="text-center">
                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection