@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard">
            <ul id = "myTabs" class = "nav nav-tabs" role = "tablist">
                @foreach(Product::all() as $product)
                    <li role = "presentation" class = "">
                        <a href = "#{!! $product->id !!}" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">{!! $product->name !!}</a>
                    </li>
                @endforeach
            </ul>

            <div id = "myTabContent" class = "tab-content">
                @foreach(Product::all() as $product)
                    <div role = "tabpanel" class = "tab-pane fade" id = "{!! $product->id !!}" aria-labelledby = "home-tab">

                        <div ng-controller="CommissionPerMonthGridController">

                            <div class="hide">
                                {!! Form::text('product_id', $product->id, ['init-model'=>'product_id']) !!}
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-success margin-top-20" href="/dashboard/investments/commission/schedule/permonth/compliant">Commission Compliance</a>
                            </div>

                            <table st-pipe="callServer" st-table="displayed" class = "table table-responsive table-hover table-striped margin-top-20">
                                <thead>
                                    <tr>
                                        <th colspan="100%" class="">
                                            <div class="in_table_search_box">
                                                <input st-search = "" class = "form-control margin-top-20" placeholder = "Search..." type = "text"/>
                                            </div>

                                            <st-date-range predicate="date" before="query.before" after="query.after"></st-date-range>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>FA Name</th>
                                        <th ng-repeat="date in meta.dates"><% date | amDateFormat:'MMM YYYY' %></th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody ng-show="!isLoading">
                                    <tr ng-repeat = "row in displayed">
                                        <td><% row.name %></td>
                                        <td ng-repeat="amount in row.commissions_per_month track by $index"><% amount %></td>
                                        <td>
                                            <a href="/dashboard/investments/commission/schedule/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody ng-show="isLoading">
                                    <tr>
                                        <td colspan="100%" class="text-center">Loading ... </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th ng-repeat="total in meta.monthly_totals  track by $index"><% total | currency:"" %></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td colspan = "100%" class = "text-center">
                                            <div class="col-md-4 margin-top-20">
                                                Items per page

                                                <input type = "text" ng-model = "itemsByPage"/>
                                            </div>
                                            <div class="col-md-8">
                                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="in_table_search_box">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" id="before" ng-model="before" uib-datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
            <span class="in_table_search_box">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" id="after" ng-model="after" uib-datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@endsection