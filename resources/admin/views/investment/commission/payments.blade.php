@extends('layouts.default')

@section('content')

    <div class="panel-dashboard">
        <ul id = "myTabs" class = "nav nav-tabs" role = "tablist">
            @foreach($currencies as $currency)
                <li role = "presentation" class = "">
                    <a href = "#{!! $currency->id !!}" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">{!! $currency->name !!}</a>
                </li>
            @endforeach
        </ul>

        <div id = "myTabContent" class = "tab-content">
            @foreach($currencies as $currency)
                <div role = "tabpanel" class = "tab-pane fade" id = "{!! $currency->id !!}" aria-labelledby = "home-tab">
                    <div class="row" data-ng-controller="CommissionOverridesCtrl"
                         data-ng-init="recipientId='{!! $recipient->id !!}'; currencyId='{!! $currency->id !!}';
                         startDate='{!! $start->toDateString() !!}'; endDate='{!! $end->toDateString() !!}'; totalCommission='{!! $currency->total !!}'">
                        <div class="col-md-6">
                            <div class="detail-group">
                                <h4>FA Details</h4>

                                <table class="table table-hover table-responsive">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    <tr><td>Name</td><td>{!! $recipient->name !!}</td></tr>
                                    <tr><td>Type</td><td>{!! $recipient->type->name !!}</td></tr>
                                    <tr><td>Compliance Required</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo((new \Cytonn\Investment\Commission\Base())->shouldCheckCompliance($recipient)) !!}</td></tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="detail-group">
                                <h4>Commission Calculation ({!! $start->toFormattedDateString() !!} to {!! $end->toFormattedDateString() !!})</h4>

                                <table class="table table-hover table-responsive">
                                    <thead>
                                    <tr><th>Entry</th><th>Amount</th></tr>
                                    </thead>
                                    <tbody>
                                    <tr><td>Commission this month</td><td><% overrides.commission.compliance %></td></tr>
                                    <tr><td>Commission claw back</td><td>(<% overrides.commission.clawbacks %>)</td></tr>
                                    <tr><td>Commission Override</td><td><% overrides.total %></td></tr>
                                    <tr ng-if="additional_commission.additional_commission != 0"><td>Advance Commission</td><td><% additional_commission.additional_commission %></td></tr>
                                    <tr ng-if="additional_commission.net_unpaid_commission != 0"><td>Net Unrecovered Clawbacks</td><td><% additional_commission.net_unpaid_commission %></td></tr>
                                    <tr ng-if="additional_commission.net_repayments != 0"><td>Advance Commission Repayment</td><td><% additional_commission.net_repayments %></td></tr>
                                    </tbody>
                                    <tfoot>
                                    <tr><th>Total</th><th><% overrides.commission.finalCommission %></th></tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="detail-group">
                                <h4>Payment Date</h4>

                                {!! Form::open(['method'=>'GET']) !!}

                                <div class="form-group" ng-controller = "DatepickerCtrl">
                                    {!! Form::label('Select Payment Date') !!}

                                    {!! Form::text('date', $date->toDateString(), ['class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Change Date', ['class'=>'btn btn-primary']) !!}
                                </div>

                                {!! Form::close() !!}
                            </div>

                            <div class="detail-group">
                                <div class="btn-group">
                                    <a href="#addCommissionSchedule" class="btn btn-info" data-toggle="modal" role="button"><i class="fa fa-calendar-plus-o"></i> Add Schedule</a>
                                    <a href="#addCommissionClawback" class="btn btn-warning" data-toggle="modal" role="button"><i class="fa fa-plus-square-o"></i> Add Clawback</a>
                                    <a href="#addAdditionalCommission" class="btn btn-success" data-toggle="modal" role="button"><i class="fa fa-plus-square-o"></i> Add Additional Commission</a>
                                </div>
                                <br/>

                                <!-- Schedule form modal -->
                                <div class="modal fade" id="addCommissionSchedule"
                                     tabindex="-1" role="dialog" aria-labelledby="addCommissionScheduleLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['add_commission_payment_schedule', $recipient->id]]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close"
                                                   data-dismiss="modal" aria-label="Close"><span
                                                            aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="addCommissionScheduleLabel">Add Commission Payment Schedule</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('investment_id', 'Investment') !!}
                                                    <select name="investment_id" class="form-control" required>
                                                        @foreach($currency->compliant as $cs)
                                                            <option value="{!! $cs->commission->investment->id !!}">
                                                                Principal : {!! \Cytonn\Presenters\AmountPresenter::currency($cs->commission->investment->amount) !!},
                                                                Invested Date : {!! \Cytonn\Presenters\DatePresenter::formatDate($cs->commission->investment->invested_date) !!},
                                                                Maturity Date : {!! \Cytonn\Presenters\DatePresenter::formatDate($cs->commission->investment->maturity_date) !!},
                                                                Interest Rate : {!! \Cytonn\Presenters\AmountPresenter::currency($cs->commission->investment->interest_rate) !!}%
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('date', 'Scheduled Date') !!}
                                                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'date']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('amount', 'Amount') !!}
                                                    {!! Form::number('amount', null, ['class'=>'form-control', 'init-model'=>'number', 'ng-required'=>'true']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('description', 'Description') !!}
                                                    {!! Form::text('description', null, ['class'=>'form-control', 'init-model'=>'description']) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::submit('Save Schedule', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Schedule form modal -->

                                <!-- Clawback form modal -->
                                <div class="modal fade" id="addCommissionClawback"
                                     tabindex="-1" role="dialog" aria-labelledby="addCommissionClawbackLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['add_commission_clawback', $recipient->id]]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close"
                                                   data-dismiss="modal" aria-label="Close"><span
                                                            aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="addCommissionClawbackLabel">Add Commission Clawback</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('investment_id', 'Investment') !!}
                                                    <select name="investment_id" class="form-control" required>
                                                        @foreach($currency->compliant as $cs)
                                                            <option value="{!! $cs->commission->investment->id !!}">
                                                                Principal : {!! \Cytonn\Presenters\AmountPresenter::currency($cs->commission->investment->amount) !!},
                                                                Invested Date : {!! \Cytonn\Presenters\DatePresenter::formatDate($cs->commission->investment->invested_date) !!},
                                                                Maturity Date : {!! \Cytonn\Presenters\DatePresenter::formatDate($cs->commission->investment->maturity_date) !!},
                                                                Interest Rate : {!! \Cytonn\Presenters\AmountPresenter::currency($cs->commission->investment->interest_rate) !!}%
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('date', 'Date') !!}
                                                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'date']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('amount', 'Amount') !!}
                                                    {!! Form::number('amount', null, ['class'=>'form-control', 'init-model'=>'number', 'ng-required'=>'true']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('narration', 'Narration') !!}
                                                    {!! Form::text('narration', null, ['class'=>'form-control', 'init-model'=>'narration']) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::submit('Save Clawback', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Clawback form modal end -->

                                <!-- Advance Commission form modal -->
                                <div class="modal fade" id="addAdditionalCommission"
                                     tabindex="-1" role="dialog" aria-labelledby="addAdditionalCommissionLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['store_additional_commission']]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close"
                                                   data-dismiss="modal" aria-label="Close"><span
                                                            aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="addCommissionClawbackLabel">Store Additional Commission</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group" ng-controller="DatepickerCtrl">
                                                    {!! Form::label('date', 'Date') !!}
                                                    {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('amount', 'Amount') !!}
                                                    {!! Form::text('amount', NULL, ['class'=>'form-control', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('type_id', 'Additional Commission Type') !!}
                                                    {!! Form::select('type_id', $advanceCommissionTypes, null, ['class'=>'form-control', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('repayable', 'Repayable') !!}
                                                    {!! Form::select('repayable', ['0' => 'Non Repayable', '1' => 'To Be Repayed'], 1, ['class'=>'form-control', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('description', 'Description') !!}
                                                    {!! Form::text('description', null, ['class'=>'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::hidden('recipient_id', $recipient->id) !!}
                                                {!! Form::submit('Save Advance Commission', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Advance Commission form modal end -->
                            </div>
                        </div>

                        <div class="col-md-12">
                            <uib-accordion>
                                <uib-accordion-group>
                                    <uib-accordion-heading>
                                        Commission Earned <i class="pull-right glyphicon"
                                                             ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                                    </uib-accordion-heading>

                                    <table class="table table-bordered table-hover table-striped table-responsive">
                                        <thead>
                                        <tr>
                                            <td>ID</td>
                                            <th>Client Name</th>
                                            <th>Principal</th>
                                            <th>Value Date</th>
                                            <th>Maturity Date</th>
                                            <th>Compliant</th>
                                            <th>Compliance Date</th>
                                            <th>Rate</th>
                                            <th>Date</th>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($currency->compliant as $cs)
                                            <tr>
                                                <td>{!! $cs->commission->investment->id !!}</td>
                                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($cs->investment->client_id) !!}</td>
                                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->investment->amount) !!}</td>
                                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($cs->investment->invested_date) !!}</td>
                                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($cs->investment->maturity_date) !!}</td>
                                                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($cs->investment->client->compliant) !!}</td>
                                                <td>{!! $cs->commission->investment->client->compliance_date !!}</td>
                                                <td>{!! $cs->commission->rate !!}%</td>
                                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($cs->date) !!}</td>
                                                <td>{!! $cs->description !!}</td>
                                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->amount) !!}</td>
                                                <td>
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#edit-schedule-{!! $cs->id !!}"><i
                                                                class="fa fa-pencil-square-o"></i> </a>
                                                    <div class="modal fade" id="edit-schedule-{!! $cs->id !!}"
                                                         tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                {!! Form::model($cs, ['route'=>['edit_commission_payment_schedule', $cs->id]]) !!}
                                                                <div class="modal-header">
                                                                    <a href="#" type="button" class="close"
                                                                       data-dismiss="modal" aria-label="Close"><span
                                                                                aria-hidden="true">&times;</span></a>
                                                                    <h4 class="modal-title" id="myModalLabel">Edit
                                                                        Commission Payment Schedule</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        {!! Form::label('date', 'Scheduled Date') !!}
                                                                        <div ng-controller="DatepickerCtrl">{!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                                </div>
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="10">Total</th>
                                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($currency->summary->getCompliant()) !!}</th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </uib-accordion-group>
                                <uib-accordion-group>
                                    <uib-accordion-heading>
                                        Commission claw back <i class="pull-right glyphicon"
                                                                ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                                    </uib-accordion-heading>

                                    <table class="table table-bordered table-hover table-striped table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th colspan="2">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($currency->calculator->clawbacks() as $cs)
                                            <tr>
                                                <td>{!! $cs->narration !!}</td>
                                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($cs->date) !!}</td>
                                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->amount) !!}</td>
                                                <td colspan="2">
                                                    <div class="btn-group">
                                                        <a href="#" class="btn btn-sm btn-info" data-toggle="modal"
                                                           data-target="#edit-clawback-{!! $cs->id !!}"><i
                                                                    class="fa fa-pencil-square-o"></i> Edit</a>
                                                        <!-- Edit Commission Clawback Version -->
                                                        <div class="modal fade" id="edit-clawback-{!! $cs->id !!}"
                                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    {!! Form::model($cs, ['route'=>['update_commission_clawback', $cs->id], 'method'=>'PUT']) !!}
                                                                    <div class="modal-header">
                                                                        <a href="#" type="button" class="close"
                                                                           data-dismiss="modal" aria-label="Close"><span
                                                                                    aria-hidden="true">&times;</span></a>
                                                                        <h4 class="modal-title" id="myModalLabel">Update
                                                                            Commission Clawback</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div ng-controller="DatepickerCtrl" class="form-group">
                                                                            {!! Form::label('date', 'Date') !!}
                                                                            {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'date']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                            {!! Form::label('amount', 'Amount') !!}
                                                                            {!! Form::text('amount', null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                            {!! Form::label('narration', 'Narration') !!}
                                                                            {!! Form::text('narration', null, ['class'=>'form-control', 'required']) !!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                                        <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                                    </div>
                                                                    {!! Form::close() !!}
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <a href="#" class="btn btn-sm btn-danger" data-toggle="modal"
                                                           data-target="#delete-clawback-{!! $cs->id !!}"><i
                                                                    class="fa fa-remove"></i> Delete</a>
                                                        <!-- Delete Commission Clawback Modal -->
                                                        <div class="modal fade" id="delete-clawback-{!! $cs->id !!}"
                                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <a href="#" type="button" class="close"
                                                                           data-dismiss="modal" aria-label="Close"><span
                                                                                    aria-hidden="true">&times;</span></a>
                                                                        <h4 class="modal-title" id="myModalLabel">Delete
                                                                            Commission Clawback</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Are you sure that you want to delete this
                                                                            commission clawback?</p>
                                                                        <table class="table table-bordered">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>Description</td>
                                                                                <td>{!! $cs->narration !!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Date</td>
                                                                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($cs->date) !!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Amount</td>
                                                                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->amount) !!}</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {!! Form::open(['route'=>['delete_commission_clawback', $cs->id], 'method'=>'DELETE']) !!}
                                                                        <button type="button" class="btn btn-default"
                                                                                data-dismiss="modal">Cancel
                                                                        </button>
                                                                        {!! Form::button('<i class="fa fa-remove"></i> Delete', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="2">Total</th>
                                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($currency->summary->getClawBacks()) !!}</th>
                                            <th colspan="2"></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </uib-accordion-group>
                                <uib-accordion-group>
                                    <uib-accordion-heading>
                                        Commission Override <i class="pull-right glyphicon"
                                                               ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                                    </uib-accordion-heading>

                                    <div>
                                        <table class="table table-bordered table-hover table-striped table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Rank</th>
                                                <th>Commission</th>
                                                <th>Rate</th>
                                                <th>Override</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody ng-show="!isLoading">
                                            <tr ng-repeat="row in overrides.overrides">
                                                <td><% row.name %></td>
                                                <td><% row.rank %></td>
                                                <td><% row.commission %></td>
                                                <td><% row.rate %></td>
                                                <td><% row.override %></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                            <tbody ng-show="isLoading">
                                            <tr><td colspan="100%">Loading...</td></tr>
                                            </tbody>
                                            <tfoot ng-show="!isLoading">
                                            <tr>
                                                <th colspan="4">Total</th>
                                                <th><% overrides.total %></th>
                                                <th></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </uib-accordion-group>
                                @if(count($additionalCommissionCalculator->additionalCommissions()) > 0 && $currency->code == 'KES')
                                    <uib-accordion-group>
                                        <uib-accordion-heading>
                                            Additional Commission <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                                        </uib-accordion-heading>

                                        <div>
                                            <table class="table table-bordered table-hover table-striped table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Type</th>
                                                    <th>Repayable</th>
                                                    <th>Date</th>
                                                    <th>Amount</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($additionalCommissionCalculator->additionalCommissions() as $advanceCommission)
                                                    <tr>
                                                        <td>{!! $advanceCommission->description !!}</td>
                                                        <td>{!! $advanceCommission->type->name !!}</td>
                                                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($advanceCommission->repayable) !!}</td>
                                                        <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($advanceCommission->date) !!}</td>
                                                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($advanceCommission->amount) !!}</td>
                                                        <td>
                                                            <a href="#editAdditionalCommission-{!! $advanceCommission->id !!}" class="btn btn-success" data-toggle="modal" role="button"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                                            <!-- Update Advance Commission form modal -->
                                                            <div class="modal fade" id="editAdditionalCommission-{!! $advanceCommission->id !!}"
                                                                 tabindex="-1" role="dialog" aria-labelledby="editAdditionalCommissionLabel-{!! $advanceCommission->id !!}">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        {!! Form::open(['route'=>['store_additional_commission', $advanceCommission->id]]) !!}
                                                                        <div class="modal-header">
                                                                            <a href="#" type="button" class="close"
                                                                               data-dismiss="modal" aria-label="Close"><span
                                                                                        aria-hidden="true">&times;</span></a>
                                                                            <h4 class="modal-title" id="addCommissionClawbackLabel">Update Additional Commission</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group" ng-controller="DatepickerCtrl">
                                                                                {!! Form::label('date', 'Date') !!}
                                                                                {!! Form::text('date', $advanceCommission->date, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                                                                            </div>

                                                                            <div class="form-group">
                                                                                {!! Form::label('amount', 'Amount') !!}
                                                                                {!! Form::text('amount', $advanceCommission->amount, ['class'=>'form-control', 'required']) !!}
                                                                            </div>

                                                                            <div class="form-group">
                                                                                {!! Form::label('type_id', 'Additional Commission Type') !!}
                                                                                {!! Form::select('type_id', $advanceCommissionTypes, $advanceCommission->type_id, ['class'=>'form-control', 'required']) !!}
                                                                            </div>

                                                                            <div class="form-group">
                                                                                {!! Form::label('repayable', 'Repayable') !!}
                                                                                {!! Form::select('repayable', ['0' => 'Non Repayable', '1' => 'To Be Repayed'], $advanceCommission->repayable, ['class'=>'form-control', 'required']) !!}
                                                                            </div>

                                                                            <div class="form-group">
                                                                                {!! Form::label('description', 'Description') !!}
                                                                                {!! Form::text('description', $advanceCommission->description, ['class'=>'form-control', 'required']) !!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            {!! Form::hidden('recipient_id', $recipient->id) !!}
                                                                            {!! Form::submit('Update Advance Commission', ['class'=>'btn btn-success']) !!}
                                                                            <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                                        </div>
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Update Advance Commission form modal end -->
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot ng-show="!isLoading">
                                                <tr>
                                                    <th colspan="4">Total</th>
                                                    <th><% additional_commission.additional_commission %></th>
                                                    <th></th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </uib-accordion-group>
                                @endif
                                @if(count($additionalCommissionCalculator->allUnpaidAdditionalCommissions()) > 0 && $currency->code == 'KES')
                                    <uib-accordion-group>
                                        <uib-accordion-heading>
                                            Unrecovered Clawbacks <i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
                                        </uib-accordion-heading>

                                        <div>
                                            <table class="table table-bordered table-hover table-striped table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Type</th>
                                                    <th>Repayable</th>
                                                    <th>Date</th>
                                                    <th>Amount</th>
                                                    <th>Unpaid Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($additionalCommissionCalculator->allUnpaidAdditionalCommissions() as $advanceCommission)
                                                    <tr>
                                                        <td>{!! $advanceCommission->description !!}</td>
                                                        <td>{!! $advanceCommission->type->name !!}</td>
                                                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($advanceCommission->repayable) !!}</td>
                                                        <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($advanceCommission->date) !!}</td>
                                                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($advanceCommission->amount) !!}</td>
                                                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($advanceCommission->unpaid_amount) !!}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot ng-show="!isLoading">
                                                <tr>
                                                    <th colspan="5">Total</th>
                                                    <th><% additional_commission.net_unpaid_commission %></th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </uib-accordion-group>
                                @endif
                            </uib-accordion>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection