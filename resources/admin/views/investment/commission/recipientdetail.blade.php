@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <table class="table table-hover table-striped">
                <thead></thead>
                <tbody>
                    <tr><td><h3>{!! $recipient->name !!}</h3></td><td class="align-middle"><h5 class="pull-right">{!! $recipient->type->name !!}</h5></td></tr>
                </tbody>
            </table>

            <a href="/dashboard/investments/commission/schedule/permonth/compliant/{!! $recipient->id !!}" class="btn btn-success margin-bottom-10">Commission Compliance</a>
            <a href="/dashboard/investments/commission/payment/{!! $recipient->id !!}" class="btn btn-success margin-bottom-10">Commission Payment</a>
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach($commissions as $commission)
                    <li role="presentation" class=""><a href="#{!! $commission->currency->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $commission->currency->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach($commissions as $commission)
                    <div role="tabpanel" class="tab-pane fade" id="{!! $commission->currency->id !!}" aria-labelledby="home-tab">

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th colspan="7"></th>
                                    <th></th>
                                </tr>
                                <tr><th>Client Name</th><th>Invested Amount</th><th>Tenor</th><th>Rate</th><th>Total Commission</th><th>First Installment</th><th>Monthly Installment</th><th>Amount Paid</th><th>Balance</th></tr>
                                </thead>
                                <tbody>

                                @foreach($commission->commissions as $c)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($c->investment->client->id) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($c->investment->amount) !!}</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::tenorInMonths($c->investment->id) !!} Months</td>
                                        <td>{!! $c->rate !!}%</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::getTotalCommission($c, true) !!}</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::getCommissionFirstInstallment($c, true) !!}</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::getCommissionMonthlyInstallment($c, true) !!}</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::getTotalCommissionPaid($c, true) !!}</td>
                                        <td>
                                            {!! \Cytonn\Presenters\InvestmentPresenter::getCommissionPaymentBalance($c, true) !!}

                                            <a ng-controller="PopoverCtrl" uib-popover="View Schedules" popover-trigger="mouseenter" class="pull-right" href="/dashboard/investments/commission/{!! $c->investment->id !!}"><i class="fa fa-list-alt margin-left-20"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr rowspan="1"></tr>
                                <tr>
                                    <th>Total</th>
                                    <th colspan="3">{!! \Cytonn\Presenters\AmountPresenter::currency($commission->totals['principal']) !!}</th>
                                    <th colspan="3">{!! \Cytonn\Presenters\AmountPresenter::currency($commission->totals['commissions']) !!}</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($commission->totals['payments']) !!}</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($commission->totals['balances']) !!}</th>
                                </tr>
                                </tfoot>
                            </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection