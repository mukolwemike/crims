@extends('layouts.default')

@section('content')
    <div class = "col-md-6 col-md-offset-3">
        <div class = "panel-dashboard">
            <h3>FA Commission</h3>

            <div class="detail-group">
                {!! Form::model($recipient, ['url'=>'/dashboard/investments/commission/add/'.$recipient->id]) !!}

                <div class = "group margin-bottom-20">
                    <div class="form-group">
                        {!! Form::label('recipient_type_id', 'Select FA Type') !!}
                        <div class="input-group">
                            {!! Form::select('recipient_type_id', $recepienttypes, null, ['class'=>'form-control']) !!}
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"  data-toggle = "modal" data-target = "#myModal"><span>Add</span></button>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class = "form-group clearfix" ng-hide="select-name">
                    {!! Form::label('name', 'Enter the FA Name') !!}
                    {!! Form::text('name', null, ['class'=>'form-control', 'ng-hide'=>"select-name"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('rank_id', 'Rank') !!}

                    {!! Form::select('rank_id', $recepientbanks, null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'rank_id') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}

                    {!! Form::text('email', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone_country_code', 'Phone Country Code') !!}

                    {!! Form::text('phone_country_code', null, ['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Phone Number') !!}

                    {!! Form::text('phone', null, ['class'=>'form-control']) !!}
                </div>

                <div class = "form-group clearfix"  ng-controller = "DatepickerCtrl">
                    {!! Form::label('reporting_date', 'Reporting Date') !!}

                    {!! Form::text('reporting_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"reporting_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reporting_date') !!}
                </div>

                <div class="form-group clearfix" ng-controller="ClientUserCtrl">
                    {!! Form::label('reports_to', 'Reports to') !!}
                    <input autocomplete="off" type="text" ng-model="selectedFA" placeholder="Reports to..." typeahead="fa as fa.name for fa in searchFas($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                    <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                    <div ng-show="noResults">
                        <i class="glyphicon glyphicon-remove"></i> No Results Found
                    </div>
                    <div class="form-group hide">
                        <input name="reports_to" type="text" init-model="selectedFA.id">
                    </div>
                </div>

                @if(!$has_account)
                    <div class="form-group">
                        {!! Form::checkbox('create_account', true, true) !!}

                        {!! Form::label('create_account', 'Create a login account for FA') !!} <br><br>
                    </div>
                @endif

                <div class="form-group">
                    {!! Form::checkbox('zero_commission', null, false, ['id'=>'zero_commission']) !!}

                    {!! Form::label('zero_commission', 'Zero Commission') !!} <br/><br/>
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

<div class = "modal fade" id = "myModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel">
<div class = "modal-dialog" role = "document">
    <div class = "modal-content">
        <div class = "modal-body">
            <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close"><span
                        aria-hidden = "true">&times;</span></button>
            <h4 class = "modal-title" id = "myModalLabel">Add FA Type</h4>
            <br/><br/>
            {!! Form::open(['route'=>'fa_type_add']) !!}
            <div class = "form-group">
                {!! Form::label('Enter the type name') !!}

                {!! Form::text('name', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Select Applicable rate') !!}

                {!! Form::select('rate_id', $commissionrates, null, ['class'=>'form-control']) !!}
            </div>

            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>