@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            <div class = "detail-group">
                <h4>Payment Date Details</h4>
                <table class = "table table-hover">
                    <tbody>
                        <tr>
                            <td>Start Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($bcp->start) !!}</td>
                        </tr>
                        <tr>
                            <td>End Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($bcp->end) !!}</td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>{!! $bcp->description !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-12"></div>

        <div class="col-md-4">
            <div class="detail-group">
                <h4>Investments</h4>

                @if(!$bcp->investmentsApproval)
                    <div>
                        {!! Form::open(['route'=>['bulk_commission_payment', $bcp->id]]) !!}
                        {!! Form::submit('Send Investment Commissions for Approval', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>
                @else
                    <p>Investments commissions have been sent for approval.</p><br>

                    <a href="/dashboard/investments/approve/{!! $bcp->investmentsApproval->id !!}" class="btn btn-success">See approval details</a>
                @endif
            </div>
        </div>

        <div class="col-md-4">
            <div class="detail-group">
                <h4>Real Estate</h4>

                @if(!$bcp->realEstateApproval)
                    <div>
                        {!! Form::open(['route'=>['bulk_realestate_commission_payment', $bcp->id]]) !!}
                        {!! Form::submit('Send Real Estate Commissions for Approvals', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>
                @else
                    <p>Real estate commissions have been sent for approval.</p><br>

                    <a href="/dashboard/investments/approve/{!! $bcp->realEstateApproval->id !!}" class="btn btn-success">See approval details</a>
                @endif
            </div>
        </div>

        <div class="col-md-4">
            <div class="detail-group">
                <h4>Unitization</h4>

                <div class="margin-bottom-10">
                    <a href="#award-unitfund-commissions" class="btn btn-success" data-toggle="modal" role="button">Award Commission</a>
                </div>

                {{--start of award commission modal--}}
                <div class="modal fade" id="award-unitfund-commissions" tabindex="-1" role="dialog" aria-labelledby="award-unitfund-commissions-label">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open(['route'=>['award_unititization_commission', $bcp->id], 'method'=>'POST']) !!}
                            <div class="modal-header">
                                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                <h4 class="modal-title" id="myModalLabel">Award Unitization Commission</h4>
                            </div>
                            <div class="modal-body row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <p>Are you sure you would like to award unitization commission for the current commission payment dates?</p>
                                    </div>

                                    <div class="form-group col-md-12">
                                        {!! Form::label('date', 'Select Commission Date For Schedules') !!}
                                        {!! Form::text('date', $bcp->end, ['class'=>'form-control', 'datepicker-popup init-model'=>'start', 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                {!! Form::hidden('bcp', $bcp->id) !!}
                                {!! Form::submit('Award', ['class'=>'btn btn-success']) !!}
                                <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                {{--end of award commission modal--}}

                @if(!$bcp->unitizationApproval)
                    <div>
                        {!! Form::open(['route'=>['bulk_unitization_commission_payments', $bcp->id]]) !!}
                        {!! Form::submit('Send Unitization Commissions for Approval', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>
                @else
                    <p>Unitization commissions have been sent for approval.</p><br>

                    <a href="/dashboard/investments/approve/{!! $bcp->unitizationApproval->id !!}" class="btn btn-success">See approval details</a>
                @endif
            </div>
        </div>
        <div class="col-md-12"></div>

        <div class="col-md-4">
            <div class="detail-group">
                <h4>Shares</h4>

                @if(!$bcp->sharesApproval)
                    <div>
                        {!! Form::open(['route'=>['bulk_shares_commission_payments', $bcp->id]]) !!}
                        {!! Form::submit('Send Shares Commissions for Approval', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>
                @else
                    <p>Unitization commissions have been sent for approval.</p><br>

                    <a href="/dashboard/investments/approve/{!! $bcp->sharesApproval->id !!}" class="btn btn-success">See approval details</a>
                @endif
            </div>
        </div>

        <div class="col-md-12">

            <div class="detail-group">
                <h4>Combined</h4>

                @if(!$bcp->combinedApproval)
                    <div>
                        {!! Form::open(['route'=>['combined_bulk_commission_payment', $bcp->id]]) !!}
                        {!! Form::submit('Send Combined Commissions for Approvals', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>
                @else
                    <p>Combined commissions have been sent for approval.</p><br>

                    <a href="/dashboard/investments/approve/{!! $bcp->combinedApproval->id !!}" class="btn btn-success">See approval details</a>

                    @if($bcp->combinedApproval)
                        <div class="margin-top-10 margin-bottom-10">
                            <a href="#export-pension-commission" class="btn btn-primary" data-toggle="modal"
                               role="button">Export Pension Commission</a>
                        </div>

                        {{--start of award commission modal--}}
                        <div class="modal fade" id="export-pension-commission" tabindex="-1" role="dialog"
                             aria-labelledby="award-unitfund-commissions-label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>['pension_commission.export', $bcp->id], 'method'=>'POST']) !!}
                                    <div class="modal-header">
                                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></a>
                                        <h4 class="modal-title" id="myModalLabel">Export Pension Commission</h4>
                                    </div>
                                    <div class="modal-body row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p>Are you sure you would like to export pension commissions for the
                                                    current commission payment dates?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::hidden('bcp_id', $bcp->id) !!}
                                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                        <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endif
                @endif

            </div>
{{--            {!! Form::open(['route'=>['export_combined_commissions', $bcp->id]]) !!}--}}
{{--            {!! Form::hidden('bcp_id', $bcp->id) !!}--}}
{{--            {!! Form::submit('Download Commissions', ['class'=>'btn btn-primary']) !!}--}}
{{--            {!! Form::close() !!}--}}
        </div>
    </div>
@stop
