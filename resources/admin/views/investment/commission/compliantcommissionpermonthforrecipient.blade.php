@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div class="col-md-8">
                <div class="alert alert-info margin-top-10">
                    <p>The date will be automatically adjusted to end of month, select any date within the month of interest</p>
                </div>
            </div>
            <div class="col-md-4 pull-right">
                {!! Form::open() !!}

                <div class="col-md-8">
                    <div class="form-group" ng-controller = "DatepickerCtrl">
                        {!! Form::label('Enter date') !!}

                        {!! Form::text('date', $date, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success margin-top-25']) !!}
                </div>
                {!! Form::close() !!}
            </div>

            <div class="col-md-12">
                <h3>FA Name: {!! $recipient->name !!}</h3>
            </div>

            <div class="col-md-12">
                <ul id = "myTabs" class = "nav nav-tabs" role = "tablist">
                    @foreach(Product::all() as $product)
                        <li role = "presentation" class = "">
                            <a href = "#{!! $product->id !!}" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">{!! $product->name !!}</a>
                        </li>
                    @endforeach
                </ul>

                <div id = "myTabContent" class = "tab-content">
                    @foreach(Product::all() as $product)
                        <div role = "tabpanel" class = "tab-pane fade" id = "{!! $product->id !!}" aria-labelledby = "home-tab">
                            <h3>Compliant</h3>

                            <?php
                                $compliant = (new \Cytonn\Investment\CommissionRepository())->getCompliantSchedulesForRecipientForProductForMonth($product, $recipient, $date);
                            ?>

                            <table class="table table-responsive table-hover margin-top-20">
                                <thead>
                                <tr><th>Client Name</th><th>Value Date</th><th>Amount</th><th>Tenor</th><th>Rate</th><th>Description</th><th>Amount</th></tr>
                                </thead>
                                <tbody>
                                @foreach($compliant as $c)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($c->commission->investment->client_id) !!}</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($c->commission->investment->invested_date) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($c->commission->investment->amount) !!}</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::tenorInMonths($c->commission->investment_id) !!} Months</td>
                                        <td>{!! $c->commission->rate !!}%</td>
                                        <td>{!! $c->description !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($c->amount) !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr><th>Total</th><th colspan="5"></th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($compliant->sum('amount')) !!}</th></tr>
                                </tfoot>
                            </table>

                            <h3>Incompliant</h3>

                            <?php
                                $incompliant = (new \Cytonn\Investment\CommissionRepository())->getNonCompliantSchedulesForRecipientForProductForMonth($product, $recipient, $date);
                            ?>

                            <table class="table table-responsive table-hover margin-top-20">
                                <thead>
                                <tr><th>Client Name</th><th>Value Date</th><th>Amount</th><th>Tenor</th><th>Rate</th><th>Description</th><th>Amount</th></tr>
                                </thead>
                                <tbody>
                                @foreach($incompliant as $c)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($c->commission->investment->client_id) !!}</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($c->commission->investment->invested_date) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($c->commission->investment->amount) !!}</td>
                                        <td>{!! \Cytonn\Presenters\InvestmentPresenter::tenorInMonths($c->commission->investment_id) !!} Months</td>
                                        <td>{!! $c->commission->rate !!}%</td>
                                        <td>{!! $c->description !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($c->amount) !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr><th>Total</th><th colspan="5"></th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($incompliant->sum('amount')) !!}</th></tr>
                                </tfoot>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection