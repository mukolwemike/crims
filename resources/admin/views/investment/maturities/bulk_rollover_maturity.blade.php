@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <table class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th>Client code</th><th>Client Name</th><th>Amount Invested</th><th>Invested Date</th><th>Maturity Date</th>
                <th>Interest Rate</th><th>Value</th><th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($investments as $investment)
                <tr>
                    <td>{!! $investment->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment()) !!}</td>
                    <td><a href="/dashboard/investments/clientinvestments/maturity/{!! $investment->id !!}"><i class="fa fa-list-alt"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $investments->links() !!}
    </div>
@stop