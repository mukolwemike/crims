@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "DeductionsGridController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan="3">
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="/dashboard/investments/clientinvestments" class="btn btn-success">Client Investments</a>
                            <a href="/dashboard/investments/clientinvestments/maturity" class="btn btn-success">Maturities</a>
                            <a href="/dashboard/investments/interest" class="btn btn-success">Interest Payments</a>
                            <a href="/dashboard/investments/clientinvestments/withdrawals" class="btn btn-success">Withdrawals</a>
                        </div>
                    </th>
                    <th colspan="2">
                        <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                    </th>
                    <th colspan="2">
                        <button type="button" class="btn btn-primary pull-right margin-bottom-20" data-toggle="modal" data-target="#exportToExcel">Export to Excel</button>
                        <div class="modal fade" id="exportToExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>['export_deductions_to_excel']]) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Export Deduction to Excel</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="col-md-4">{!! Form::label('startDate', 'Start Date') !!}</div>
                                            <div ng-controller="DatepickerCtrl" class="col-md-8">{!! Form::text('startDate', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"startDate", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4">{!! Form::label('endDate', 'End Date') !!}</div>
                                            <div ng-controller="DatepickerCtrl" class="col-md-8">{!! Form::text('endDate', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"endDate", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th st-sort = "date">Deduction Date</th>
                    <th st-sort = "amount">Amount</th>
                    <th>Interest rate</th>
                    <th>Currency</th>
                    <th>Action</th>
                </tr>
                </thead>
                    <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td><% row.client_code %></td>
                            <td><% row.fullName %></td>
                            <td><% row.deduction_date | date%></td>
                            <td><% row.amount | currency:"" %></td>
                            <td><% row.interest_rate %>%</td>
                            <td><% row.currency %></td>
                            <td>
                                <a ng-controller = "PopoverCtrl" uib-popover = "Details" popover-trigger = "mouseenter" href = "/dashboard/investments/clientinvestments/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr><td colspan="100%">Loading...</td></tr>
                    </tbody>
                <tfoot>
                    <tr>
                        <td colspan = "100%" dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop