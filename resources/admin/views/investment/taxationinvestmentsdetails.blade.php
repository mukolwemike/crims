@extends('layouts.default')

@section('content')
    <div class = "panel-dashboard" ng-controller = "taxationInvestmentsDetailsCtrl">
        <h3>{!!  \Cytonn\Presenters\ClientPresenter::presentFullNames($client_id); !!}</h3>

        <div style = "display: none !important;">
            {!! Form::text($client_id, $client_id, ['init-model'=>'client_id']) !!}
        </div>
        <table st-table = "displayedCollection" st-safe-src = "rowCollection"  st-set-filter="InvestedDateRangeFilter" class = "table table-responsive table-striped">
            <thead>
            <tr>
                <th colspan = "4"></th>
                <th colspan="2">
                    <st-date-range predicate="invested_date" before="query.before" after="query.after"></st-date-range>
                </th>
            </tr>
            <tr>
                <th>Investment Date</th>
                <th>Maturity Date</th>
                <th>Amount</th>
                <th>Interest rate</th>
                <th>Gross Interest</th>
                <th>Withholding Tax</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat = "row in displayedCollection">
                <td> <%row.invested_date | date %></td>
                <td><% row.maturity_date | date %></td>
                <td><% row.amount | currency:""  %></td>
                <td><% row.interest_rate %>%</td>
                <td><% row.gross_interest | currency:"" %></td>
                <td><% row.withholding_tax | currency:"" %></td>
            </tr>
            <tr>
                <th colspan = "6">Total Investment</th>
            </tr>
            <tr>
                <td colspan = "6"> <% invested_amount_total | currency:"" %></td>
            </tr>
            <tfoot>
            <tr>
                <td colspan = "2" class = "text-center">
                    Items per page
                </td>
                <td colspan = "2" class = "text-center">
                    <input type = "text" ng-model = "itemsByPage"/>
                </td>
                <td colspan = "4" class = "text-center">
                    <div st-pagination = "" st-items-by-page = "itemsByPage" st-displayed-pages = "10"></div>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop