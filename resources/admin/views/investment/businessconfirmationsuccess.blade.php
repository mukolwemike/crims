@extends('layouts.default')

@section('content')
<div class="col-md-6 col-md-offset-3">
    <div class="panel-dashboard">
        <div class="panel">
            <div class="panel-heading">
                <h1>Business Confirmation Successful</h1>
            </div>
            <div class="panel-body">
                <p>The application has been confirmed.</p>
                <table class="table table-hover">
                    <thead></thead>
                    <tbody>
                    <tr><td>Name: {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($application->client_id) !!} </td></tr>
                    <tr><td>Product: {!! Product::find($application->product_id)->name !!}</td></tr>
                    <tr><td>Amount: {!! $application->amount !!}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop