@extends('layouts.default')

@section('content')
    <div class = "col-md-8 col-md-offset-2">
        <div class = "panel-dashboard">
            <h3>Compose Message</h3>
            <div class = "detail-group">
                {!! Form::model($message, ['route'=>['client_bulk_messaging'.$message->id]]) !!}

                <div class = "form-group">
                    {!! Form::label('subject', 'Subject') !!}

                    {!! Form::text('subject', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'subject') !!}
                </div>

                <div class="form-group" ng-controller="SummerNoteController">
                    {!! Form::label('body', 'Body') !!}
                    <summernote  config="options" ng-model="summernote" ng-init="summernote='{!! $message->body !!}'"></summernote>
                    {!! Form::text('body', null, ['ng-model'=>'summernote', 'style'=>'display: none;']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'body') !!}
                </div>

                @if($message->sent)
                    <button class="btn btn-primary" disabled><i class="fa fa-paper-plane-o"></i> Save Message</button>
                @else
                    {!! Form::button('<i class="fa fa-paper-plane-o"></i> Save Message', ['class'=>'btn btn-success', 'type'=>'submit']) !!}
                @endif
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection