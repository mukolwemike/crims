@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Client Bulk Message</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td>{!! $message->id !!}</td>
                    </tr>
                    <tr>
                        <td>Subject</td>
                        <td>{!! $message->subject !!}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{!! $message->body !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($message->updated_at) !!}</td>
                    </tr>
                    <tr>
                        <td>Composer</td>
                        <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($message->composer) !!}</td>
                    </tr>
                    <tr>
                        <td>Approved</td>
                        <td>
                            {!! \Cytonn\Presenters\BooleanPresenter::presentIcon($message->approval_id) !!}
                            @if($message->approval_id)
                                Approved by {!! \Cytonn\Presenters\UserPresenter::presentFullNames($message->approval->sender->id) !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Sent</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($message->sent) !!}</td>
                    </tr>
                </tbody>
            </table>
            @if($message->sent)
                <button class="btn btn-default" disabled><i class="fa fa-pencil-square-o"></i> Edit Message</button>
            @else
                <a href="/dashboard/investments/bulk-messages/{!! $message->id !!}" class="btn btn-default"><i class="fa fa-pencil-square-o"></i> Edit Message</a>
            @endif
        </div>
    </div>
@endsection