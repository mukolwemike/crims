@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="BulkMessagesGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="4">
                            <a href="/dashboard/investments/bulk-messages/create" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Create Message</a>
                        </th>
                        <th>
                            <select st-search="approved" class="form-control">
                                <option value="">Approval Status</option>
                                <option value="1">Approved</option>
                                <option value="0">Not Approved</option>
                            </select>
                        </th>
                        <th>
                            <select st-search="sent" class="form-control">
                                <option value="">Sent Status</option>
                                <option value="1">Sent</option>
                                <option value="0">Not sent</option>
                            </select>
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <th st-sort="id">ID</th>
                        <th>Subject</th>
                        <th>Composer</th>
                        <th>Date</th>
                        <th>Approved</th>
                        <th>Sent</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.subject %></td>
                        <td><% row.composer %></td>
                        <td><% row.date | date %></td>
                        <td>
                            <i ng-show="row.approved == true" class="fa fa-check-circle-o" style="color:green;"></i>
                            <i ng-show="row.approved == false" class="fa fa-close" style="color:red;"></i>
                        </td>
                        <td>
                            <i ng-show="row.sent == true" class="fa fa-check-circle-o" style="color:green;"></i>
                            <i ng-show="row.sent == false" class="fa fa-close" style="color:red;"></i>
                        </td>
                        <td>
                            <a class="pull-right" href="/dashboard/investments/bulk-messages/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop