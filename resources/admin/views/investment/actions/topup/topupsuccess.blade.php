@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h1>Investment Topup Successful</h1>
            <div class="detail-group">
                <table class="table table-hover">
                    <p>The Investment was topped up. Here are the details of the new investment</p>
                    <thead>

                    </thead>
                    <tbody>
                    <tr><td>Investment ID</td><td>{!! $investment->id !!}</td></tr>
                    <tr><td>Client Code</td><td>{!! $investment->client->client_code !!}</td></tr>
                    <tr><td>Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td></tr>
                    <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                    <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                    <tr><td>Interest rate</td><td>{!! $investment->interest_rate !!}%</tr>
                    <tr><td>Transaction Description</td><td>{!! $investment->description !!}</tr>
                    @if($investment->interest_action_id)
                        <tr><td>Interest Action</td><td>{!! $investment->interestAction->name !!}</td></tr>
                    @endif
                    @if($investment->interest_reinvest_tenor != 0)
                        <tr><td>Interest Reinvest Tenor</td><td>{!! $investment->interest_reinvest_tenor !!} months</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
