@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard" ng-controller = "clientTopupCtrl" ng-init="setClientId({!! $investment->client_id !!});currentCommissionRate='{!! $investment->repo->getCommissionRate() !!}'; commission_recipient='{!! $investment->repo->getCommissionRecipientId() !!}'">
            <h1>Topup Investment</h1>
            @include('investment.partials.investmentdetail')

            {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}
            <div class = "row">
                <div class = "col-md-6">
                    <div class = "detail-group">
                        <h4>Investment</h4>
                        {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                        {!! Form::hidden('topup_form', NULL, ['init-model'=>'topup_form', 'id'=>'topup_form']) !!}

                        <div class = "form-group">
                            {!! Form::label('product_id', 'Select product') !!}

                            {!! Form::select('product_id', $products, $investment->product_id, ['class'=>'form-control', 'required', 'init-model'=>'product_id']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                        </div>

                        <div class = "form-group" ng-controller = "DatepickerCtrl">
                            {!! Form::label('invested_date', 'Enter the investment date for Topup') !!}

                            {!! Form::text('invested_date', NULL, ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                        </div>

                        <div class = "form-group">
                            {!! Form::label('tenor', 'Tenor (months)') !!} <span ng-if="tenordays"><b>( Tenor Days : <% tenordays %> days )</b></span>
                            <input type="text" step="1" id="tenor" init-model="tenor" class="form-control" required />
                        </div>

                        <div class = "form-group">
                            {!! Form::label('maturity_date', 'Enter the maturity date for topup') !!}

                            {!! Form::text('maturity_date', NULL, ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}

                            {!! Form::checkbox('on_call', null, false, ['id'=>'on_call']) !!} {!! Form::label('on_call', 'On call') !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                        </div>

                        <div class = "form-group">
                            <label for="amount">Enter the amount to topup <small>(Current payments balance : <% payments_balance | currency:"" %>)</small></label>
                            {!! Form::text('amount', null, ['class'=>'form-control', 'step'=>'0.01','ng-model'=>'amount', 'required']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                        </div>

                        <div class = "form-group">
                            {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}

                            {!! Form::number('interest_rate', NULL, ['class'=>'form-control', 'step'=>'0.01','init-model'=>'interest_rate', 'required']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                        </div>

                        <div class = "form-group">
                            {!! Form::label('description', 'Transaction Description') !!}

                            {!! Form::textarea('description', null, ['init-model'=>'description','class'=>'form-control', 'rows'=>2, 'required']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                        </div>
                    </div>
                </div>
                <div class = "col-md-6">
                    <div class = "detail-group">
                        <h4>Commission</h4>

                        <div class = "row">
                            <div class = "col-md-6">
                                <div class = "form-group">

                                    <commission-select recipients="recipients"></commission-select>

                                    {{--{!! Form::label('commission_recepient', 'Commission paid to') !!}--}}

                                    {{--{!! Form::select('commission_recepient',  ['id'=>'commission_recepient','class'=>'form-control', 'ui-select2', 'init-model'=>'commission_recepient', 'required','ng-disabled'=> '!checked']) !!}--}}
                                </div>
                            </div>

                            <div class = "col-md-6">
                                <i class="fa fa-spinner fa-spin" ng-show="loading === true"></i>
                                <div class = "form-group" ng-show="zero_commission_rate === '0'">
                                    {!! Form::label('commission_rate', 'Commission rate') !!}
                                    <select id="commission_rate"
                                            ng-model="commission_rate"
                                            ng-options="commissionRate.name for commissionRate in rates"
                                            class="form-control"
                                            required
                                            ng-disabled="!checked"
                                            ng-change="getCommissionRate()"
                                            ng-if="zero_commission_rate === '0'">
                                    </select>
                                    {!! Form::hidden('commission_rate',null, ['id' => 'commission_rate_value', 'data-ng-value' => 'checkCommissionRateValue()']) !!}
                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                                </div>

                                <div class="form-group" ng-show="zero_commission_rate === '1'">
                                    <p class="margin-top-20">This FA has Zero Commission Rate</p>
                                    {!! Form::hidden('commission_rate', 0, ['id'=>'commission_rate','class'=>'form-control', 'required','ng-disabled'=> '!checked', 'ng-if'=>"zero_commission_rate === '1'"]) !!}
                                </div>
                            </div>

                            <div class = "col-md-6">
                                <div class = "form-group">
                                    {!! Form::label('commission_start_date', 'Commission Start Date') !!}
                                    {!! Form::text('commission_start_date', NULL, ['id'=>'commission_start_date' , 'class'=>'form-control', 'datepicker-popup init-model'=>"commission_start_date", 'is-open'=>"status.opened_2", 'ng-focus'=>'status.opened_2 = ! status.opened_2']) !!}
                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_start_date') !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                {!! Form::checkbox('edit', true, true, ['ng-model'=>'checked']) !!} {!! Form::label('edit', "Edit FA") !!}
                            </div>
                        </div>
                    </div>

                    <div class = "detail-group">
                        <h4>Interest payment</h4>
                        <div class = "col-md-6">
                            <div class = "form-group">
                                {!! Form::label('interest_payment_interval', 'Interest payment interval') !!}

                                {!! Form::select('interest_payment_interval', $interest_payment_intervals, $investment->interest_payment_interval, ['class'=>'form-control', 'id'=>'interest_payment_interval', 'init-model'=>'interest_payment_interval']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_interval') !!}
                            </div>
                        </div>
                        <div class = "col-md-6">
                            <div class = "form-group">
                                {!! Form::label('interest_payment_date', 'Preferred payment date (1 - 31)') !!}

                                {!! Form::number('interest_payment_date', $investment->interest_payment_date, ['class'=>'form-control', 'init-model'=>'interest_payment_date']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_date') !!}
                            </div>
                        </div>
                        <div class = "col-md-12">
                            <div class = "alert alert-info"><p>When date is entered as 31st it will automatically be converted to end of month</p>
                            </div>

                            <div class = "form-group" ng-show="interest_payment_interval != 0">
                                {!! Form::label('interest_action_id', 'Select Interest Action') !!}
                                {!! Form::select('interest_action_id', $interestActions, NULL, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}
                            </div>
                        </div>
                        <div ng-show="interest_action_id == 2" class = "col-md-12">
                            <div class = "alert alert-info"><p>When no reinvest tenor is entered the interest will be reivested upto the maturity date of the investment</p>
                            </div>
                            <div class = "form-group" ng-show="interest_action_id == 2">
                                {!! Form::label('interest_reinvest_tenor', 'Interest Reinvest Tenor (months)') !!}
                                {!! Form::number('interest_reinvest_tenor', null, ['class' => 'form-control' , 'init-model' => 'interest_reinvest_tenor', 'id' => 'interest_reinvest_tenor'] ) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_reinvest_tenor') !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div class = "col-md-12">
                    <div class = "detail-group">
                        {!! Form::submit('Topup', ['class'=>'btn btn-success']) !!}
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

            <div ng-view></div>
            <script type = "text/ng-template" id = "clienttopup.htm">
                <div class = "modal fade">
                    <div class = "modal-dialog">
                        <div class = "modal-content">

                            <div class = "modal-header no-bottom-border">
                                <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                        data-dismiss = "modal" aria-hidden = "true">&times;</button>
                                <h4 class = "modal-title">Confirm Investment Topup</h4>
                            </div>
                            <div class = "modal-body">

                                <div class = "detail-group">
                                    <p>Are you sure you want to topup this investment with the details below?</p>
                                    <table class = "table table-hover table-responsive table-striped">
                                        <tbody>
                                        <tr>
                                            <td>Client Name</td>
                                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                                        </tr>
                                        <tr>
                                            <td>New investment date</td>
                                            <td><% investedDate | date %></td>
                                        </tr>
                                        <tr>
                                            <td>New maturity date</td>
                                            <td><% maturityDate | date%></td>
                                        </tr>
                                        <tr>
                                            <td>On call</td>
                                            <td><span ng-if="on_call">Yes</span><span ng-if="!on_call">No</span></td>
                                        </tr>
                                        <tr>
                                            <td>Topup amount</td>
                                            <td><% amount | currency:"" %></td>
                                        </tr>
                                        <tr>
                                            <td>Interest Rate</td>
                                            <td><% interestRate %></td>
                                        </tr>
                                        <tr>
                                            <td>Commission paid to</td>
                                            <td><% commissionRecipient %></td>
                                        </tr>
                                        <tr>
                                            <td>Commission rate</td>
                                            <td><% commissionRate %></td>
                                        </tr>
                                        <tr ng-if="commissionStartDate">
                                            <td>Commission Start Date</td>
                                            <td><% commissionStartDate %></td>
                                        </tr>
                                        <tr>
                                            <td>Interest paid</td>
                                            <td><% interest_payment %></td>
                                        </tr>
                                        <tr>
                                            <td>Interest paid on date</td>
                                            <td><% interest_payment_date %></td>
                                        </tr>
                                        <tr ng-if="interestAction">
                                            <td>Interest Action</td>
                                            <td><% interestAction %></td>
                                        </tr>
                                        <tr ng-if="interest_reinvest_tenor">
                                            <td>Interest Reinvest Tenor</td>
                                            <td><% interest_reinvest_tenor %> months</td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td><% description %></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class = "detail-group">
                                    <div class = "pull-right">
                                        {!! Form::open(['route'=>'topup_path']) !!}
                                        {!! Form::hidden('topup_form', NULL, ['init-model'=>'topup_form']) !!}
                                        {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                                        {!! Form::hidden('topup_form_id', null) !!}
                                        <div style = "display: none !important;">
                                            {!! Form::text('product_id', NULL, ['ng-model'=>'product_id']) !!}
                                            {!! Form::text('invested_date', NULL, ['ng-model'=>'investedDate']) !!}
                                            {!! Form::text('maturity_date', NULL, ['ng-model'=>'maturityDate']) !!}
                                            {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                                            {!! Form::text('interest_rate', NULL, ['ng-model'=>'interestRate']) !!}
                                            {!! Form::text('commission_recepient', NULL, ['init-model'=>'commissionRecipientId']) !!}
                                            {!! Form::text('commission_rate', NULL, ['init-model'=>'commissionRateId']) !!}
                                            {!! Form::text('commission_start_date', NULL, ['init-model'=>'commissionStartDate']) !!}
                                            {!! Form::text('interest_payment_interval', NULL, ['init-model'=>'interest_payment_interval']) !!}
                                            {!! Form::text('interest_payment_date', NULL, ['init-model'=>'interest_payment_date']) !!}
                                            {!! Form::text('on_call', NULL, ['init-model'=>'on_call']) !!}
                                            {!! Form::text('description', NULL, ['init-model'=>'description']) !!}
                                            {!! Form::text('interest_action_id', NULL, ['ng-model'=>'interest_action_id', 'ng-if'=>'interest_action_id']) !!}
                                            {!! Form::text('interest_reinvest_tenor', NULL, ['ng-model'=>'interest_reinvest_tenor', 'ng-if'=>'interest_reinvest_tenor']) !!}
                                        </div>
                                        <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                                data-dismiss = "modal">No
                                        </button>
                                        {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </script>

        </div>
    </div>
    </div>
@stop