@extends('layouts.default')

@section('content')
    <div>
        <div class = "panel-dashboard">
            <div class="col-md-6">
                @include('investment.partials.investmentdetail')
            </div>
            <div class="col-md-6 margin-top-10">
                @foreach($errors->all() as $e)
                    <li>{!! $e !!}</li>
                @endforeach
                <uib-accordion>
                    {{--<uib-accordion-group heading="Edit Principal">--}}
                        {{--{!! Form::model((object) $investment, ['route'=>'edit_investment_path']) !!}--}}
                            {{--{!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}--}}
                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('amount', 'New Principal') !!}--}}

                                {{--{!! Form::text('amount', null, ['class'=>'form-control']) !!}--}}
                                {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--{!! Form::submit('Save', ['class'=>'btn btn-success']) !!}--}}
                            {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</uib-accordion-group>--}}
                    <uib-accordion-group heading="Edit Interest Rates">
                        {!! Form::model((object) $investment, ['route'=>'edit_investment_path']) !!}
                            {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                            {!! Form::hidden('edit_type', 'investment_interest_rate') !!}
                            <div class="form-group">
                                {!! Form::label('interest_rate', 'New Rate') !!}

                                {!! Form::text('interest_rate', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>
                        {!! Form::close() !!}
                    </uib-accordion-group>
                    {{--<uib-accordion-group heading="Edit Value Date">--}}
                        {{--{!! Form::model((object) $investment, ['route'=>'edit_investment_path']) !!}--}}
                            {{--{!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}--}}
                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('invested_date', 'New Value Date') !!}--}}

                                {{--{!! Form::text('invested_date', $value_date, ['class'=>'form-control', 'datepicker-popup init-model'=>'invested_date', 'is-open'=>'status.v', 'ng-focus'=>'status.v = true']) !!}--}}

                                {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--{!! Form::submit('Save', ['class'=>'btn btn-success']) !!}--}}
                            {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</uib-accordion-group>--}}
                    <uib-accordion-group heading="Edit Maturity Date">
                        {!! Form::model((object) $investment, ['route'=>'edit_investment_path']) !!}
                            {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                            {!! Form::hidden('edit_type', 'investment_maturity_date') !!}
                            <div class="form-group">
                                {!! Form::label('maturity_date', 'New Maturity Date') !!}

                                {!! Form::text('maturity_date', $maturity_date, ['class'=>'form-control', 'datepicker-popup init-model'=>'maturity_date', 'is-open'=>'status.m', 'ng-focus'=>'status.m = true']) !!}
                                {!! Form::hidden('on_call', '0') !!}
                                {!! Form::checkbox('on_call', true, $investment->on_call) !!} {!! Form::label('on_call', 'On call') !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>
                        {!! Form::close() !!}
                    </uib-accordion-group>
                    <uib-accordion-group heading="Edit Commission">
                        <div data-ng-controller="editInvestmentCommissionCtrl"
                             ng-init="commission_recipient='{!! $investment->repo->getCommissionRecipientId() !!}';
                             commission_rate='{!! $investment->commission->rate !!}';
                             product_id='{!! $investment->product_id !!}';
                             client_id = '{!! $investment->client_id !!}';
                             invested_date = '{!! $investment->invested_date !!}';
                             investment_type_id = '{!! $investment->investment_type_id !!}';
                             isParent = '{!! $investment->investmentPaymentScheduleDetail ? 1 : 0 !!}'
                            ">
                            {!! Form::model((object) $investment, ['route'=>'edit_investment_path']) !!}
                            {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                            {!! Form::hidden('edit_type', 'investment_commission') !!}
                            <div class = "form-group">
                                {!! Form::label('commission_recepient', 'Commission paid to') !!}
                                {!! Form::select('commission_recepient', $commissionRecepient, $investment->repo->getCommissionRecipientId(),  ['id'=>'commission_recepient','init-model'=>'commission_recepient','class'=>'form-control', 'required']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('commission_start_date', 'Commission Start Date') !!}
                                {!! Form::text('commission_start_date', $commissionStartDate, ['class'=>'form-control', 'datepicker-popup init-model'=>'commission_start_date', 'is-open'=>'status.c', 'ng-focus'=>'status.c = true']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_start_date') !!}
                            </div>

                            <div class="form-group alert alert-info">
                                {!! Form::label('commission_rate', 'Commission Rate') !!} :- <i class="fa fa-spinner fa-spin" ng-show="loadingRate === true"></i> {!! Form::label('commission_rate', '<% commission_rate_name %>') !!}
                                <br />

                            </div>

                            <div class = "form-group" ng-show="edit_rate">
                                <br />
                                {!! Form::label('commission_rate', 'Commission Rate') !!}
                                {!! Form::number('commission_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'data-ng-value' => 'commission_rate', 'required', 'ng-model' => 'commission_rate']) !!}
                                {!! Form::hidden('commission_rate_name', NULL, [ 'data-ng-value' => 'commission_rate_name', 'ng-model' => 'commission_rate_name']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                            </div>

                            <div class="form-group">
                                <br />
                                {!! Form::checkbox('edit', true, true, ['ng-model'=>'edit_rate']) !!} {!! Form::label('edit', "Edit Commission Rate") !!}
                                <br />
                            </div>

                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </uib-accordion-group>
                    <uib-accordion-group heading="Edit Interest Payment Details">
                        {!! Form::model((object) $investment, ['route'=>'edit_investment_path']) !!}
                        {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                        {!! Form::hidden('edit_type', 'investment_payment_details') !!}
                        <div class="form-group">
                            {!! Form::label('interest_payment_interval', 'Interest payment interval') !!}

                            {!! Form::select('interest_payment_interval', $interest_payment_intervals, NULL, ['class'=>'form-control', 'id'=>'interest_payment_interval', 'init-model'=>'interest_payment_interval']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_interval') !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('interest_payment_date', 'Preferred payment date (1 - 31)') !!}

                            {!! Form::number('interest_payment_date', NULL, ['class'=>'form-control', 'init-model'=>'interest_payment_date']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_date') !!}
                        </div>

                        <div class="form-group" ng-if="interest_payment_interval != 0">
                            {!! Form::label('interest_payment_start_date', 'Interest Start Date') !!}
                            {!! Form::text('interest_payment_start_date', $investment->interest_payment_start_date, ['class'=>'form-control', 'datepicker-popup init-model'=>'interest_payment_start_date', 'is-open'=>'status.i', 'ng-focus'=>'status.i = true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_start_date') !!}
                        </div>

                        <div ng-if="interest_payment_interval != 0">
                            <div class="form-group">
                                {!! Form::label('interest_action_id', 'Select Interest Action') !!}
                                {!! Form::select('interest_action_id', $interestactions, null, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}
                            </div>
                            <div class="form-group" ng-show="interest_action_id == 2">
                                <div class="alert alert-info"><p>When no reinvest tenor is entered the interest will be
                                        reivested upto the maturity date of the investment</p>
                                </div>
                                <div class="form-group" ng-show="interest_action_id == 2">
                                    {!! Form::label('interest_reinvest_tenor', 'Interest Reinvest Tenor (months)') !!}
                                    {!! Form::number('interest_reinvest_tenor', null, ['class' => 'form-control' , 'init-model' => 'interest_reinvest_tenor', 'id' => 'interest_reinvest_tenor'] ) !!}
                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_reinvest_tenor') !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </uib-accordion-group>
                </uib-accordion>
            </div>
        </div>
    </div>

@stop