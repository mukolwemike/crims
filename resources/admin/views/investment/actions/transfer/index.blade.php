@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2" ng-controller="clientWithdrawCtrl">
        <div class="panel-dashboard">
            <h1>Transfer Client Investment</h1>
            @include('investment.partials.investmentdetail')

            <div class="detail-group group" ng-controller="ClientInvestmentTransferCtrl">

                <h4>Transfer investment to another client</h4>

                {!! Form::open() !!}

                <div class="form-group">
                    {!! Form::label('name', 'Enter Client Name') !!}

                    {!! Form::text('name', null, ['class'=>'form-control', 'ng-model'=>'name', 'ng-change'=>'getClients()', 'autocomplete'=>'off', 'typeahead-editable'=>false, 'uib-typeahead'=>"client as client.fullName for client in clients"]) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_id') !!}
                    <div class="hide">
                        {!! Form::text('client_id', null, ['init-model'=>'name.id']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <p>
                        Client Code: <span ng-bind="name.client_code"></span><br/><br/>
                        Email: <span ng-bind="name.email"></span><br/><br/>
                        Phone: <span ng-bind="name.phone"></span><br><br/>
                    </p>
                </div>

                <div class="form-group" ng-controller = "DatepickerCtrl">
                    {!! Form::label('date', 'Transfer date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('narration', 'Describe why you are transferring') !!}

                    {!! Form::text('narration', null, ['class'=>'form-control', 'placeholder'=>'narration']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narration') !!}
                </div>

                {!! Form::submit('Transfer', ['class'=>'btn btn-success']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
