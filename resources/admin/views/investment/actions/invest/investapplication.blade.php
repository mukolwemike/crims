@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard" ng-controller = "investApplicationCtrl">
            <div class = "detail-group">
                <div class = "row">
                    <div class = "col-md-12">
                        <h4>Application Details</h4>

                        <table class = "table table-hover">
                            <thead></thead>
                            <tbody>
                            <tr>
                                <td>Name</td>
                                <td> {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($application->client_id) !!} </td>
                            </tr>
                            <tr>
                                <td>Product</td>
                                <td> {!! $application->product->name !!}</td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td> {!! \Cytonn\Presenters\AmountPresenter::currency($application->amount) !!}</td>
                            </tr>
                            <tr>
                                <td>Tenor</td>
                                <td>{!! $application->tenor !!} Months</td>
                            </tr>
                            <tr>
                                <td>Compliant</td>
                                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($application->repo->compliance()->approved) !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class = "detail-group">
                {!! Form::open(['name'=>'form', 'url'=>'investment']) !!}

                {!! Form::hidden('app_id', Crypt::encrypt($application->id)) !!}

                <div class = "row">
                    <div class = "col-md-12">
                        <div class = "alert alert-info"> Fill in all the details below to confirm and save the investment to the system
                        </div>
                    </div>
                </div>
                <div class = "row">
                    <div class = "col-md-6">
                        <div class = "detail-group">
                            <h4>Investment</h4>

                            <div class = "form-group">
                                {!! Form::label('product_id', 'Select the product') !!}

                                {!! Form::select('product_id', $productlists, $application->product_id, ['id'=>'product_id','class'=>'form-control', 'init-model'=>'product_id', 'required', 'readonly']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                            </div>
                            <div class = "form-group">
                                {!! Form::label('invested_date', 'Investment Date') !!}

                                {!! Form::text('invested_date', NULL, ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened2", 'ng-focus'=>'status.opened2 = !status.opened2', 'ng-required'=>"true"]) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}

                            </div>

                            <div class = "form-group">
                                {!! Form::label('tenor', 'Tenor (months)') !!} <span ng-if="tenordays"><b>( Tenor Days : <% tenordays %> days )</b></span>
                                <input type="number" step="1" id="tenor" init-model="tenor" class="form-control" required />
                            </div>

                            <div class = "form-group">
                                {!! Form::label('maturity_date', 'Maturity Date') !!}

                                {!! Form::text('maturity_date', NULL, ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}

                                {!! Form::checkbox('on_call', null, false, ['id'=>'on_call', 'init-model'=>'on_call']) !!} {!! Form::label('on_call', 'On call') !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                            </div>

                            <div class = "form-group">
                                {!! Form::label('interest_rate', 'Interest rate') !!}

                                {!! Form::text('interest_rate', $application->agreed_rate, ['init-model'=>'interest_rate','class'=>'form-control', 'required']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                            </div>

                            <div class = "form-group">
                                {!! Form::label('client_code', 'Client Code') !!}

                                {!! Form::text('client_code', $code, ['init-model'=>'client_code','class'=>'form-control', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}
                            </div>

                            <div class = "form-group">
                                {!! Form::label('description', 'Transaction Description') !!}

                                {!! Form::textarea('description', null, ['init-model'=>'description','class'=>'form-control', 'rows'=>2, 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                            </div>
                        </div>
                    </div>
                    <div class = "col-md-6">
                        <div class = "detail-group">
                            <h4>Commission</h4>

                            <div>
                                <div class = "form-group">
                                    {!! Form::label('commission_recepient', 'Commission paid to') !!}
                                    {!! Form::select('commission_recepient', $commissionrecepients, NULL,  ['id'=>'commission_recepient','class'=>'form-control', 'ng-model'=>'commission_recepient', 'required']) !!}
                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_recepient') !!}
                                </div>
                            </div>

                            <div class = "form-group">
                                <i class="fa fa-spinner fa-spin" ng-show="loading === true"></i>
                                <div class = "form-group" ng-show="zero_commission_rate === '0'">
                                    {!! Form::label('commission_rate', 'Commission rate') !!}
                                    <select id="commission_rate"
                                            ng-model="commission_rate"
                                            ng-options="commissionRate.name for commissionRate in rates"
                                            class="form-control"
                                            required
                                            ng-change="getCommissionRate()"
                                            ng-if="zero_commission_rate === '0'">
                                    </select>
                                    {!! Form::hidden('commission_rate',null, ['id' => 'commission_rate_value', 'data-ng-value' => 'checkCommissionRateValue()']) !!}
                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                                </div>

                                <div class="form-group" ng-show="commission_rate.name === 'Other'">
                                    <p class="margin-top-10">Input commission rate if none</p>
                                    {!! Form::number('commission_rate_input', null, ['step'=>'0.001', 'min'=>0.001, 'max'=>2.00, 'id'=>'commission_rate_input','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group" ng-show="zero_commission_rate === '1'">
                                    <p class="margin-top-20">This FA has Zero Commission Rate</p>
                                    {!! Form::hidden('commission_rate', 0, ['id'=>'commission_rate','class'=>'form-control', 'required','ng-disabled'=> '!checked', 'ng-if'=>"zero_commission_rate === '1'"]) !!}
                                </div>
                            </div>
                        </div>

                        <div class = "detail-group">
                            <h4>Interest payment</h4>

                            <div class = "row">
                                <div class = "col-md-6">
                                    <div class = "form-group">
                                        {!! Form::label('interest_payment_interval', 'Interest payment interval') !!}

                                        {!! Form::select('interest_payment_interval', $interest_payment_intervals, NULL, ['class'=>'form-control', 'id'=>'interest_payment_interval', 'init-model'=>'interest_payment_interval']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_interval') !!}
                                    </div>
                                </div>
                                <div class = "col-md-6">
                                    <div class = "form-group">
                                        {!! Form::label('interest_payment_date', 'Preferred payment date (1 - 31)') !!}

                                        {!! Form::number('interest_payment_date', NULL, ['class'=>'form-control', 'init-model'=>'interest_payment_date']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_date') !!}
                                    </div>
                                </div>
                                <div class = "col-md-12">
                                    <div class = "alert alert-info"><p>When date is entered as 31st it will automatically be converted to end of month</p></div>

                                    <div class = "form-group" ng-show="interest_payment_interval != 0">
                                        {!! Form::label('interest_action_id', 'Select Interest Action') !!}

                                        {!! Form::select('interest_action_id', $interestactions, NULL, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}
                                    </div>
                                </div>
                                <div ng-show="interest_action_id == 2" class = "col-md-12">
                                    <div class = "alert alert-info"><p>When no reinvest tenor is entered the interest will be reivested upto the maturity date of the investment</p>
                                    </div>
                                    <div class = "form-group" ng-show="interest_action_id == 2">
                                        {!! Form::label('interest_reinvest_tenor', 'Interest Reinvest Tenor (months)') !!}
                                        {!! Form::number('interest_reinvest_tenor', null, ['class' => 'form-control' , 'init-model' => 'interest_reinvest_tenor', 'id' => 'interest_reinvest_tenor'] ) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_reinvest_tenor') !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class = "col-md-12">
                        <div class = "row">
                            <div class = "detail-group">
                                <button ng-click="confirmModal()" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Save</button>

                                <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Invest Application</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to add an investment with the details
                                                    below?</p>
                                                <table class = "table table-hover">
                                                    <thead></thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td> {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($application->client_id) !!} </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Client code</td>
                                                        <td><% client_code %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Product</td>
                                                        <td> {{ $application->product->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Amount</td>
                                                        <td> {!! \Cytonn\Presenters\AmountPresenter::currency($application->amount )!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Investment Date</td>
                                                        <td><% invested_date | date %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Maturity Date</td>
                                                        <td><% maturity_date | date %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>On call</td>
                                                        <% on_call %>
                                                        <td><span ng-if="on_call == true">Yes</span><span ng-if="on_call != true">No</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Interest Rate</td>
                                                        <td><% interest_rate %>%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Commission Recipient</td>
                                                        <td><% recepientName %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Commission Rate</td>
                                                        <td ng-if="zero_commission_rate === '0'"><% checkCommissionRateValue() %>%</td>
                                                        <td ng-if="zero_commission_rate === '1'">Zero Commission Rate</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Interest paid</td>
                                                        <td><% interest_payment %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Interest paid on date</td>
                                                        <td><% interest_payment_date %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Transaction Description</td>
                                                        <td><% description %></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Interest Payment Interval</td>
                                                        <td>Every <% interest_payment_interval %> month(s)</td>
                                                    </tr>
                                                    <tr ng-if="interestAction">
                                                        <td>Interest Action</td>
                                                        <td><% interestAction %></td>
                                                    </tr>
                                                    <tr ng-if="interest_reinvest_tenor">
                                                        <td>Interest Reinvest Tenor</td>
                                                        <td>Every <% interest_reinvest_tenor %> month(s)</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::submit('Invest', ['class'=>'btn btn-success']) !!}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                {!! Form::close() !!}
            </div>


        </div>


    </div>
    </div>
    </div>
@stop