@extends('emails.default')

@section('content')
    <p>A {{ $type }} was made in CRIMS. See the details below:</p>

    <ul style="list-style: none">
        <li>Client Name: {{ $client->name() }}</li>
        {{--<li>Amount: {{ number_format($instruction->amount, 2) }}</li>--}}
        {{--<li>Tenor: {{ $form->tenor }} months</li>--}}
    </ul>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection