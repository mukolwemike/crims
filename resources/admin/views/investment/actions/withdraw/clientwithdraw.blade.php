@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @include('investment.partials.investmentdetail', ['inv_title'=>'Investment Details'])
            </div>
            <div class="col-md-6">
                <client-withdrawal
                        :investment="{{ $investment }}"
                        :client="{{ $investment->client->id }}"
                        :kyc-validated="{{ json_encode($kycValidated) }}">
                </client-withdrawal>
            </div>
        </div>
    </div>
@stop
