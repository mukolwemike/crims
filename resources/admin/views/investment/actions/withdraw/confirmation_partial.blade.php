<div ng-view></div>
<script type = "text/ng-template" id = "clientwithdraw.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">

                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20"
                            ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;
                    </button>

                    @if(isset($schedule_transaction))
                        <h4 class="modal-title">Schedule Investment Withdrawal</h4>
                    @else
                        <h4 class = "modal-title">Confirm Investment Withdrawal</h4>
                    @endif
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Are you sure you want to withdraw this investment with the details
                            below?</p>
                        <table class = "table table-hover">
                            <tbody>
                            <tr>
                                <td>Investment ID</td>
                                <td>{!! $investment->id !!}</td>
                            </tr>
                            <tr>
                                <td>Client Code</td>
                                <td>{!! $investment->client->client_code !!}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                            </tr>
                            <tr>
                                <td>Principal</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            </tr>
                            @if(isset($schedule_transaction))
                                <tr>
                                    <td>Value on maturity</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment()) !!}</td>
                                </tr>
                            @else
                                <tr>
                                    <td>Value Today</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment()) !!}</td>
                                </tr>
                                <tr>
                                    <td>Value At Selected Withdrawal Date</td>
                                    <td><% total_value_of_investment_at_date | currency:"" %></td>
                                </tr>
                            @endif
                            <tr>
                                <td>Investment Date</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                            </tr>
                            <tr>
                                <td>Maturity Date</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                            </tr>
                            <tr>
                                <td>Interest rate</td>
                                <td>{!! $investment->interest_rate !!}%</td>
                            </tr>
                            <tr ng-show="premature" class="alert alert-danger">
                                <td>Net Interest At Withdraw Date</td>
                                <td><% net_interest_at_date | currency:"" %></td>
                            </tr>
                            <tr ng-show="deduct_penalty" class="alert alert-danger">
                                <td>Penalty Percentage</td>
                                <td><% penalty_percentage %>%</td>
                            </tr>
                            <tr ng-show="deduct_penalty" class="alert alert-danger">
                                <td>Penalty</td>
                                <td><% penalty | currency:"" %></td>
                            </tr>
                            <tr ng-show="premature" class = "alert alert-danger">
                                <td>Premature Investment End Date</td>
                                <td><% endDate | date %></td>
                            </tr>
                            <tr ng-show="deduct_penalty && !partial_withdraw" class="alert alert-danger">
                                <td>Amount to be Withdrawn</td>
                                <td><% total_value_of_investment_at_date - penalty | currency:"" %></td>
                            </tr>
                            <tr ng-show="partial_withdraw" class="alert alert-danger">
                                <td>Partial Withdraw Amount</td>
                                <td><% amount | currency:"" %></td>
                            </tr>
                            <tr ng-show="partial_withdraw" class="alert alert-danger">
                                <td>New maturity date</td>
                                <td><% new_maturity_date | date %></td>
                            </tr>
                            <tr ng-hide="partial_withdraw"  class="alert-success">
                                <td colspan="2"><p class="center">Full amount will be withdrawn</p></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div ng-hide="callback" class="alert alert-danger">
                            <p>Confirm you have called the client to proceed</p>
                        </div>
                        <div class = "pull-right">
                            {!! Form::open(['route'=>'withdraw_path']) !!}

                            @if(isset($schedule_transaction))
                                {!! Form::hidden('schedule', true) !!}
                            @endif

                            {!! Form::hidden('investment_id', Crypt::encrypt($investment->id)) !!}
                            {!! Form::hidden('rollover_form_id', null) !!}
                            <div style="display: none !important;">
                                {!! Form::text('premature', null, ['ng-model'=>'premature']) !!}
                                {!! Form::text('new_maturity_date', null, ['ng-model'=>'new_maturity_date']) !!}
                                {!! Form::text('end_date', null, ['ng-model'=>'endDate']) !!}
                                <input ng-if="premature" name="penalty" ng-model="penalty">
                                <input ng-if="premature" name="penalty_percentage" ng-model="penalty_percentage">
                                <input ng-if="premature" name="deduct_penalty" ng-model="deduct_penalty">
                                {!! Form::text('partial_withdraw', null, ['ng-model'=>'partial_withdraw']) !!}
                                {!! Form::text('amount', null, ['ng-model'=>'amount']) !!}
                                {!! Form::text('callback', null, ['ng-model'=>'callback']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default" data-dismiss = "modal">Go back</button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success', 'ng-disabled'=>'!callback']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>