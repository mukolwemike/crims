<div class="">
    <h4> Withdraw Investment</h4>


    {!! Form::open(['name'=>'form', 'route'=>['withdraw_path', $investment->id]]) !!}


    <div class="alert alert-warning col-md-12">
        <div class="form-group">
            {!! Form::checkbox('callback', null, false, ['required', 'ng-model'=>'callback', 'id'=>'callback']) !!}

            {!! Form::label('callback', 'I confirm that I have called the client and confirmed instructions to withdraw') !!}

            <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'callback') !!}</div>
        </div>
    </div>

    @if($premature)
        <span ng-init="premature= {!! $premature !!} == 1"></span>
    @endif

    @if($partial_withdraw)
        <span ng-init="partial={!! $partial_withdraw !!} == 1"></span>
    @endif

    <div class="col-md-12">
        <div class="row">
            <div class="form-group">
                <div class="col-md-4">{{ Form::label('On/Before Maturity') }}</div>
                <div class="col-md-8">{{ Form::select('premature', [0 => 'On Maturity', 1 => 'Before Maturity'], null, ['class' => 'form-control', 'init-model' => 'premature', 'id' => 'premature']) }}</div>
            </div>
            <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'premature') !!}</div>
        </div>

        <div ng-show="premature == 1">
            <div class="row" >
                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('end_date', 'Enter the date of withdrawal') !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::text('end_date', null, ['id'=>'end_date','class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened']) !!}
                    </div>
                    <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'end_date') !!}</div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-4">Full/Partial Withdrawal</div>
                    <div class="col-md-4">{{ Form::select('partial_withdraw', [0 => 'Full withdrawal', 1 => 'Partial Withdrawal', 2 => 'Interest'], 'null', ['class'=>'form-control', 'init-model'=>'partial', 'id' => 'partial_withdraw']) }}</div>
                    <div class="col-md-4">{{ Form::text('amount', null, ['init-model'=>'amount', 'class'=>'form-control', 'placeholder' => 'Amount to withdraw', 'ng-show' => 'partial == 1', 'id' => 'amount']) }}</div>
                    <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'partial_withdraw') !!}</div>
                    <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    {!! Form::hidden('penalty_percentage', 3.00, ['init-model'=>'penalty_percentage', 'class'=>'form-control', 'id'=>'penalty_percentage']) !!}
                    <div class="col-md-4">{!! Form::label('deduct_penalty', 'Deduct Penalty?') !!}</div>
                    <div class="col-md-8">
                        {!! Form::select('deduct_penalty', [0 => 'No', 1 => 'Yes'], true, ['init-model'=>'deduct_penalty',  'id'=>'deduct_penalty', 'class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'deduct_penalty') !!}</div>
                </div>
            </div>
        </div>
    </div>

    <!-- Button HTML (to Trigger Modal) -->
    <a href="#withdrawModal" role="button" class="btn btn-large btn-primary" data-toggle="modal">Confirm</a>

    <!-- Modal HTML -->
    <div id="withdrawModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirm Withdrawal</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to withdraw this investment?</p>
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>Client Code</td>
                            <td>{!! $investment->client->client_code !!}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                        </tr>
                        <tr>
                            <td>Principal</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                        </tr>
                        <tr>
                            <td>On/Before Maturity</td><td><span ng-show="premature != 1">On Maturity</span><span ng-show="premature == 1">Premature</span></td>
                        </tr>
                        <tr>
                            <td>Date of withdrawal</td><td> <% end_date | date %></td>
                        </tr>
                        <tr>
                            <td>Full/Partial</td><td><span ng-show="partial != 1">Full withdrawal</span><span ng-show="partial == 1">Partial: {{ $investment->product->currency->code }} <% amount | currency:"" %></span></td>
                        </tr>
                        <tr>
                            <td>Deduct penalty</td><td><span ng-show="deduct_penalty == 1">Yes: <% penalty_percentage %>%</span><span ng-show="deduct_penalty != 1">No</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Withdraw', ['class'=>'btn btn-success']) !!}
                </div>
            </div>
        </div>
    </div>
</div>