@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @include('investment.partials.investmentdetail', ['inv_title'=>'Investment Details'])
            </div>
            <div class="col-md-6">
                <div>
                    <div class="pull-right">
                        <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <hr>
                <client-withdrawal-edit
                        :investment="{{ $investment }}"
                        :instruction="{{ $instruction }}"
                        :client="{{ $investment->client->id }}">

                </client-withdrawal-edit>
            </div>
        </div>
    </div>
@stop
