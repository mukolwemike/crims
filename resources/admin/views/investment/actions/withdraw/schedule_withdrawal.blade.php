<div ng-controller="clientWithdrawCtrl">
    <h3>Schedule a withdrawal</h3>
    @include('investment.partials.investmentdetail')

    <div class = "detail-group group">
        <div class = "col-md-8">
            @include('investment.actions.withdraw.form_partial')
        </div>
    </div>
</div>


@include('investment.actions.withdraw.confirmation_partial')