@extends('layouts.default')

@section('content')
    <div class = "panel-dashboard">
        <div class = "bs-example bs-example-tabs" data-example-id = "togglable-tabs">
            <ul id = "myTabs" class = "nav nav-tabs" role = "tablist">
                <li role = "presentation" class = ""><a href = "#withdrawal" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">Withdrawal</a></li>
                <li role = "presentation" class = ""><a href = "#rollover" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">Rollover</a></li>
            </ul>
        </div>

        <div id = "myTabContent" class = "tab-content">
            <div role = "tabpanel" class = "tab-pane fade" id = "withdrawal" aria-labelledby = "home-tab"
                 ng-controller = "clientWithdrawCtrl">
                @include('investment.actions.withdraw.schedule_withdrawal')
            </div>

            <div role = "tabpanel" class = "tab-pane fade" id = "rollover" aria-labelledby = "home-tab">
                @include('investment.actions.rollover.schedule_rollover')
            </div>
        </div>
    </div>
@stop
