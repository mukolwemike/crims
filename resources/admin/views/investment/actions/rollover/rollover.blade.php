@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard" ng-controller = "clientRollOverCtrl">
            <h1>Rollover Investment</h1>
            @include('investment.partials.investmentdetail')

           <div class="hide">
               {!! Form::text('value', $investment->repo->getFinalTotalValueOfInvestment(), ['id'=>'value']) !!}
           </div>

            <div class="clearfix">
                <a href="/dashboard/investments/combinedrollover/{!! $investment->id !!}" class="btn btn-primary" >Combine With Maturing Investment and Rollover</a>
            </div>

            @include('investment.actions.rollover.rollover_form_partial')
        </div>
    </div>
@stop

@include('investment.actions.rollover.rollover_confirmation_popup')