<div ng-controller="clientRollOverCtrl">
    <h3>Schedule a rollover</h3>
    @include('investment.partials.investmentdetail')

    <div class="hide">
        {!! Form::text('value', $investment->repo->getFinalTotalValueOfInvestment(), ['id'=>'value']) !!}
    </div>

    @include('investment.actions.rollover.rollover_form_partial')
</div>


@include('investment.actions.rollover.rollover_confirmation_popup')