@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard" ng-controller="clientCombinedRolloverCtrl" ng-init="currentCommissionRate='{!! $mainInvestment->repo->getCommissionRate() !!}'; commission_recipient='{!! $mainInvestment->repo->getCommissionRecipientId() !!}'; product_id='{!! $mainInvestment->product_id !!}'">
            <h3>Combine and Rollover Investments</h3>

            <div class="detail-group">
                <table class="table table-responsive table-hover">
                    <tbody>
                        <tr><td>Client Code</td><td>{!! $investments->first()->client->client_code !!}</td></tr>
                        <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investments->first()->client_id) !!}</td></tr>
                    </tbody>
                </table>
            </div>

            <div class="detail-group">
                <table class="table table-responsive table-hover">
                    <thead>
                    <tr>
                        <th>Value Date</th>
                        <th>Maturity Date</th>
                        <th>Combine Date</th>
                        <th>Amount</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($investments as $investment)
                            <tr>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($combineDate) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->value) !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('amount')) !!}</th>
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value')) !!}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class = "detail-group">
                {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}
                <div class = "row">
                    <div class = "col-md-6">
                        <div class = "detail-group">
                            <h4>Rollover Investment</h4>
                            {!! Form::hidden('investment', Crypt::encrypt($mainInvestment->id)) !!}
                            {!! Form::hidden('combine_date', $combineDate->toDateString()) !!}

                            <div class="hide">
                                {!! Form::text('previous_total', $investments->sum('value'), ['class'=>'form-control', 'init-model'=>'previous_total']) !!}

                                {!! Form::text('investments', json_encode($investments->lists('id')), ['init-model'=>'investments']) !!}
                            </div>

                            <div class = "form-group margin-bottom-20">
                                {!! Form::radio('reinvest', 'reinvest', true) !!}
                                {!! Form::label('reinvest', 'Enter amount to reinvest') !!}<br/>

                                {!! Form::radio('reinvest', 'withdraw') !!}
                                {!! Form::label('reinvest', 'Enter the amount to withdraw') !!}<br/>

                                {!! Form::radio('reinvest', 'topup') !!}
                                {!! Form::label('reinvest', 'Enter the amount to topup') !!}<br/>
                            </div>

                            <div class = "form-group">
                                {!! Form::label('amount', 'Enter the amount') !!}

                                {!! Form::number('amount', NULL, ['class'=>'form-control', 'step'=>'0.01', 'init-model'=>'investedAmount', 'id'=>'amount', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                            </div>

                            <div class = "form-group">
                                {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}
                                {!! Form::text('interest_rate', NULL, ['class'=>'form-control','init-model'=>'interest_rate', 'step'=>'0.01', 'id'=>'rate', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                            </div>

                            <div class = "form-group" ng-controller = "DatepickerCtrl">
                                {!! Form::label('invested_date', 'Enter the date Investment resumes (Investment date)') !!}
                                {!! Form::text('invested_date', NULL, ['id'=>'invested_date', 'class'=>'form-control','datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                            </div>

                            <div class = "form-group">
                                {!! Form::label('tenor', 'Tenor (months)') !!} <span ng-if="tenordays"><b>( Tenor Days : <% tenordays %> days )</b></span>
                                <input type="number" step="1" id="tenor" init-model="tenor" class="form-control" required />
                            </div>

                            <div class = "form-group">
                                {!! Form::label('maturity_date', 'Enter the new maturity date') !!}

                                {!! Form::text('maturity_date', NULL, ['class'=>'form-control','id'=>'maturity_date', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}

                                {!! Form::checkbox('on_call', null, false, ['id'=>'on_call']) !!} {!! Form::label('on_call', 'On call') !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                            </div>

                            <div class = "form-group">
                                {!! Form::label('description', 'Transaction Description') !!}

                                {!! Form::textarea('description', null, ['init-model'=>'description','class'=>'form-control', 'rows'=>2, 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                            </div>
                        </div>
                    </div>
                    <div class = "col-md-6">
                        <div class = "detail-group">
                            <h4>Commission</h4>

                            <div class = "row">
                                <div class = "col-md-6">
                                    <div class = "form-group">
                                        {!! Form::label('commission_recepient', 'Commission paid to') !!}

                                        {!! Form::select('commission_recepient', $recipients, $mainInvestment->repo->getCommissionRecipientId(),  ['id'=>'commission_recepient', 'init-model'=>'commission_recepient','class'=>'form-control', 'required','ng-disabled'=> '!checked']) !!}
                                    </div>
                                </div>

                                <div class = "col-md-6">
                                    <i class="fa fa-spinner fa-spin" ng-show="loading === true"></i>
                                    <div class = "form-group" ng-show="zero_commission_rate === '0'">
                                        {!! Form::label('commission_rate', 'Commission rate') !!}
                                        <select id="commission_rate"
                                                ng-model="commission_rate"
                                                ng-options="commissionRate.name for commissionRate in rates"
                                                class="form-control"
                                                required
                                                ng-disabled="!checked"
                                                ng-change="getCommissionRate()"
                                                ng-if="zero_commission_rate === '0'">
                                        </select>
                                        {!! Form::hidden('commission_rate',null, ['id' => 'commission_rate_value', 'data-ng-value' => 'checkCommissionRateValue()']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                                    </div>

                                    <div class="form-group" ng-show="zero_commission_rate === '1'">
                                        <p class="margin-top-20">This FA has Zero Commission Rate</p>
                                        {!! Form::hidden('commission_rate', 0, ['id'=>'commission_rate','class'=>'form-control', 'required','ng-disabled'=> '!checked', 'ng-if'=>"zero_commission_rate === '1'"]) !!}
                                    </div>
                                </div>

                                <div class = "col-md-6">
                                    <div class = "form-group">
                                        {!! Form::label('commission_start_date', 'Commission Start Date') !!}
                                        {!! Form::text('commission_start_date', NULL, ['id'=>'commission_start_date' , 'class'=>'form-control', 'datepicker-popup init-model'=>"commission_start_date", 'is-open'=>"status.opened_cs", 'ng-focus'=>'status.opened_cs = ! status.opened_cs']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_start_date') !!}
                                    </div>
                                </div>
                            </div>
                            <div class = "clearfix"></div>

                            <div class = "col-md-4 row">
                                <div class = "form-group">
                                    {!! Form::label('commissionRecipient', 'Edit Recipient' ) !!}
                                    {!! Form::checkbox('commissionRecipient', NULL, false, ['ng-model'=>'checked', 'id'=>'commissionRecipient']) !!}
                                </div>
                            </div>
                            <div class = "clearfix"></div>
                            <br/>
                        </div>
                        <div class = "detail-group">
                            <h4>Interest payment</h4>

                            <div class = "row">
                                <div class = "col-md-6">
                                    <div class = "form-group">
                                        {!! Form::label('interest_payment_interval', 'Interest payment interval') !!}

                                        {!! Form::select('interest_payment_interval', $interest_payment_intervals, $mainInvestment->interest_payment_interval, ['class'=>'form-control', 'id'=>'interest_payment_interval', 'init-model'=>'interest_payment_interval']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_interval') !!}
                                    </div>
                                </div>
                                <div class = "col-md-6">
                                    <div class = "form-group">
                                        {!! Form::label('interest_payment_date', 'Preferred payment date(1 - 31)') !!}

                                        {!! Form::number('interest_payment_date', $mainInvestment->interest_payment_date, ['class'=>'form-control', 'init-model'=>'interest_payment_date']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_date') !!}
                                    </div>
                                </div>
                                <div class = "col-md-12">
                                    <div class = "alert alert-info"><p>When date is entered as 31st it will automatically be converted to end of month</p>
                                    </div>

                                    <div class = "form-group" ng-show="interest_payment_interval != 0">
                                        {!! Form::label('interest_action_id', 'Select Interest Action') !!}

                                        {!! Form::select('interest_action_id', $interestActions, NULL, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}
                                    </div>
                                </div>
                                <div ng-show="interest_action_id == 2" class = "col-md-12">
                                    <div class = "alert alert-info"><p>When no reinvest tenor is entered the interest will be reivested upto the maturity date of the investment</p>
                                    </div>
                                    <div class = "form-group" ng-show="interest_action_id == 2">
                                        {!! Form::label('interest_reinvest_tenor', 'Interest Reinvest Tenor (months)') !!}
                                        {!! Form::number('interest_reinvest_tenor', null, ['class' => 'form-control' , 'init-model' => 'interest_reinvest_tenor', 'id' => 'interest_reinvest_tenor'] ) !!}
                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_reinvest_tenor') !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class = "col-md-12">
                        <div class = "detail-group">
                            {!! Form::submit('Rollover', ['class'=>'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


<script type = "text/ng-template" id="rollover.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">
                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Investment Rollover</h4>
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Are you sure you want to rollover this investment with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>New investment date</td>
                                <td><% investedDate | date %></td>
                            </tr>
                            <tr>
                                <td>New maturity date</td>
                                <td><% maturityDate | date %></td>
                            </tr>
                            <tr>
                                <td>On call</td>
                                <td><span ng-if="on_call">Yes</span><span ng-if="!on_call">No</span></td>
                            </tr>
                            <tr>
                                <td>Total from combined Investments</td>
                                <td><% value | currency:"" %></td>
                            </tr>
                            <tr class = "success" ng-if="topup">
                                <td>Topup</td>
                                <td><% amount | currency:"" %></td>
                            </tr>
                            <tr class="danger">
                                <td>Total that will be withdrawn</td>
                                <td><% withdraw_val | currency:"" %></td>
                            </tr>
                            <tr class = "success">
                                <td>Total that will be reinvested</td>
                                <td><% reinvest_val | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Interest Rate</td>
                                <td><% interestRate %>%</td>
                            </tr>
                            <tr>
                                <td>Commission paid to</td>
                                <td><% commissionRecipient %></td>
                            </tr>
                            <tr>
                                <td>Commission rate</td>
                                <td><% commissionRate %></td>
                            </tr>
                            <tr ng-if="commissionStartDate">
                                <td>Commission Start Date</td>
                                <td><% commissionStartDate %></td>
                            </tr>
                            <tr>
                                <td>Interest paid</td>
                                <td><% interest_payment %></td>
                            </tr>
                            <tr>
                                <td>Interest paid on date</td>
                                <td><% interest_payment_date %></td>
                            </tr>
                            <tr>
                                <td>Interest Action</td>
                                <td><% interestAction %></td>
                            </tr>
                            <tr ng-if="interest_reinvest_tenor">
                                <td>Interest Reinvest Tenor</td>
                                <td><% interest_reinvest_tenor %> months</td>
                            </tr>
                            <tr>
                                <td>Transaction Description</td>
                                <td><% description %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>['combined_rollover_path', $mainInvestment->id]]) !!}
                            <div style = "display:none !important;">
                                {!! Form::text('investment', Crypt::encrypt($mainInvestment->id)) !!}
                                {!! Form::text('combine_date', $combineDate->toDateString()) !!}
                                {!! Form::text('investments', null, ['init-model'=>'investments']) !!}
                                {!! Form::text('reinvest', NULL, ['init-model'=>'reinvest']) !!}
                                {!! Form::text('invested_date', NULL, ['init-model'=>'investedDate']) !!}
                                {!! Form::text('maturity_date', NULL, ['init-model'=>'maturityDate']) !!}
                                {!! Form::text('interest_rate', NULL, ['init-model'=>'interestRate']) !!}
                                {!! Form::text('amount', NULL, ['init-model'=>'amount']) !!}
                                {!! Form::text('on_call', null, ['ng-model'=>'on_call']) !!}
                                {!! Form::text('description', null, ['ng-model'=>'description']) !!}
                                {!! Form::text('interest_action_id', null, ['ng-model'=>'interest_action_id', 'ng-if'=>'interest_action_id']) !!}
                                {!! Form::text('interest_reinvest_tenor', null, ['ng-model'=>'interest_reinvest_tenor', 'ng-if'=>'interest_reinvest_tenor']) !!}

                                {!! Form::text('commission_recepient', NULL, ['init-model'=>'commissionRecipientId']) !!}
                                {!! Form::text('commission_rate', NULL, ['init-model'=>'commissionRateId']) !!}
                                {!! Form::text('commission_start_date', NULL, ['init-model'=>'commissionStartDate']) !!}

                                {!! Form::text('interest_payment_interval', null, ['init-model'=>'interest_payment_interval']) !!}
                                {!! Form::text('interest_payment_date', null, ['init-model'=>'interest_payment_date']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
