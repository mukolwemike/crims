@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h1>Combine and Rollover Investment</h1>

            @include('investment.partials.investmentdetail')

            <div class="hide">
                {!! Form::text('value', $investment->repo->getTotalValueOfAnInvestment(), ['id'=>'value']) !!}
            </div>

            <div class="detail-group">
                {!! Form::open() !!}
                <div class = "form-group">
                    {!! Form::label('combine_date', 'Enter the combine date') !!}
                    {!! Form::text('combine_date', NULL, ['class'=>'form-control','id'=>'combine_date', 'datepicker-popup init-model'=>"combine_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'combine_date') !!}
                </div>
                <div class="alert alert-info">
                    <p>Select the Investments to combine</p>
                </div>
                <table class="table table-responsive table-hover">
                    <thead>
                        <tr><th></th><th>Amount</th><th>Value Date</th><th>Maturity Date</th><th>Value</th></tr>
                    </thead>
                    <tbody>
                            @foreach($same_day_investments as $investment)
                                <tr>
                                    <td>{!! Form::checkbox($investment->id, true) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment()) !!}</td>
                                </tr>
                            @endforeach
                        <tr>
                            <td colspan="5">
                                {!! Form::submit('Combine', ['class'=>'btn btn-success']) !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection