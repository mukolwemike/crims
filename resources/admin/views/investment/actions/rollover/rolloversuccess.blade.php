@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h1>Investment Rollover Successful</h1>
                <div class="detail-group">
                    <table class="table table-hover">
                        <p>The Investment was succesfully rolled over</p>
                        <thead>

                        </thead>
                        <tbody>
                            <tr><td><h4>Old Investment</h4></td><td></td></tr>

                            <tr><td>Investment ID</td><td>{!! $investment->id !!}</td></tr>
                            <tr><td>Client code</td><td>{!! $investment->client->client_code !!}</td></tr>
                            <tr><td>Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td></tr>
                            <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td></tr>
                            <tr><td>Total Interest</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getNetInterestForInvestment()) !!}</td></tr>
                            <tr><td>Total Withdrawals</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getWithdrawnAmount()) !!}</td></tr>
                            <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                            <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                            <tr><td>Interest rate</td><td>{!! $investment->interest_rate !!}%</tr>

                            <tr><td><h4>New Investment</h4></td><td></td></tr>

                            <tr><td>Amount Reinvested</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency( $rollover->client->amount ) !!}</td></tr>
                            <tr><td>Amount Withdrawn</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency( $rollover->amountWithdrawn ) !!}</td></tr>
                            <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($rollover->client->invested_date) !!}</td></tr>
                            <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($rollover->client->maturity_date )!!}</td></tr>
                            <tr><td>Interest rate</td><td>{!! $rollover->client->interest_rate !!}%</tr>
                            @if($rollover->client->interest_action_id)
                                <tr><td>Interest Action</td><td>{!! $rollover->client->interestAction->name !!}</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@stop
