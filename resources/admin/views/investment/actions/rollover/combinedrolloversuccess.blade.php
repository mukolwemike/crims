@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Investment Combined and Rolled Over Successfully</h3>
            <div class="detail-group">
                <h4>Old Investments</h4>

                <table  class="table table-hover">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <th>Value Date</th>
                        <th>Maturity Date</th>
                        <th>Invested Amount</th>
                    </tr>
                        @foreach($investments  as $investment)
                            <tr>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <h4>New Investment</h4>
                <table class="table table-hover">
                    <thead>
                    </thead>
                    <tbody>
                    <tr><td>Amount Reinvested</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency( $rollover->client->amount ) !!}</td></tr>
                    <tr><td>Amount Withdrawn</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency( $rollover->amountWithdrawn ) !!}</td></tr>
                    <tr><td>Investment Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($rollover->client->invested_date) !!}</td></tr>
                    <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($rollover->client->maturity_date )!!}</td></tr>
                    <tr><td>Interest rate</td><td>{!! $rollover->client->interest_rate !!}%</tr>
                    @if($rollover->client->interest_action_id)
                        <tr><td>Interest Action</td><td>{!! $rollover->client->interestAction->name !!}</td></tr>
                    @endif
                    <tr><td>Transaction Description</td><td>{!! $rollover->client->description !!}%</tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
