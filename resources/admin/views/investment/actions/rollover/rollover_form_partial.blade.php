<div class = "detail-group" ng-init="currentCommissionRate='{!! $investment->repo->getCommissionRate() !!}'; commission_recipient='{!! $investment->repo->getCommissionRecipientId() !!}'; product_id='{!! $investment->product_id !!}'">
    {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}
    <div class = "row">
        <div class = "col-md-6">
            <div class = "detail-group">
                <h4>Rollover Investment</h4>
                {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                {!! Form::hidden('client_instruction', NULL, ['init-model'=>'client_instruction', 'id'=>'client_instruction']) !!}

                <div class = "form-group margin-bottom-20">
                    {!! Form::radio('reinvest', 'reinvest', true) !!}
                    {!! Form::label('reivest', 'Enter amount to reinvest') !!}<br/>

                    {!! Form::radio('reinvest', 'withdraw', false, ['id'=>'reinvest_withdraw']) !!}
                    {!! Form::label('reinvest', 'Enter the amount to withdraw') !!}<br/><br/>
                </div>

                <div class="form-group">
                    <button type="button" ng-click="automaticRollover()" class="btn btn-primary">Automatic Rollover</button><br/><br/>
                </div>

                <div class = "form-group">
                    {!! Form::label('amount', 'Enter the amount') !!}

                    {!! Form::number('amount', NULL, ['class'=>'form-control', 'step'=>'0.01', 'init-model'=>'investedAmount', 'id'=>'amount']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}

                    {!! Form::number('interest_rate', NULL, ['class'=>'form-control','init-model'=>'interest_rate', 'step'=>'0.01', 'id'=>'rate']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('invested_date', 'Enter the date Investment resumes (Investment date)') !!}

                    {!! Form::text('invested_date', NULL, ['id'=>'invested_date', 'class'=>'form-control','datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened_1", 'ng-focus'=>'status.opened_1 = !status.opened_1', 'ng-required'=>"true"]) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('tenor', 'Tenor (months)') !!} &nbsp;&nbsp;<span ng-if="tenordays"><b>( Tenor Days : <% tenordays %> days )</b></span>
                    <input type="number" step="1" id="tenor" init-model="tenor" class="form-control" required />
                </div>

                <div class = "form-group">
                    {!! Form::label('maturity_date', 'Enter the new maturity date') !!}

                    {!! Form::text('maturity_date', NULL, ['class'=>'form-control','id'=>'maturity_date', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened_2", 'ng-focus'=>'status.opened_2 = !status.opened_2', 'ng-required'=>"true"]) !!}

                    {!! Form::checkbox('on_call', null, false, ['id'=>'on_call']) !!} {!! Form::label('on_call', 'On call') !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('description', 'Transaction Description') !!}

                    {!! Form::textarea('description', null, ['init-model'=>'description','class'=>'form-control', 'rows'=>2, 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                </div>
            </div>
        </div>
        <div class = "col-md-6">
            <div class = "detail-group">
                <h4>Commission</h4>

                <div class = "row">
                    <div class = "col-md-6">
                        <div class = "form-group">
                            {!! Form::label('commission_recepient', 'Commission paid to') !!}

                            {!! Form::select('commission_recepient', $commissionrecepients, $investment->repo->getCommissionRecipientId(),  ['id'=>'commission_recepient','class'=>'form-control', 'init-model'=>'commission_recepient', 'required','ng-disabled'=> '!checked']) !!}
                        </div>
                    </div>

                    <div class = "col-md-6">
                        <i class="fa fa-spinner fa-spin" ng-show="loading === true"></i>
                        <div class = "form-group" ng-show="zero_commission_rate === '0'">
                            {!! Form::label('commission_rate', 'Commission rate') !!}
                            <select id="commission_rate"
                                    ng-model="commission_rate"
                                    ng-options="commissionRate.name for commissionRate in rates"
                                    class="form-control"
                                    required
                                    ng-disabled="!checked"
                                    ng-change="getCommissionRate()"
                                    ng-if="zero_commission_rate === '0'">
                            </select>
                            {!! Form::hidden('commission_rate',null, ['id' => 'commission_rate_value', 'data-ng-value' => 'checkCommissionRateValue()']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                        </div>

                        <div class="form-group" ng-show="zero_commission_rate === '1'">
                            <p class="margin-top-20">This FA has Zero Commission Rate</p>
                            {!! Form::hidden('commission_rate', 0, ['id'=>'commission_rate','class'=>'form-control', 'required','ng-disabled'=> '!checked', 'ng-if'=>"zero_commission_rate === '1'"]) !!}
                        </div>
                    </div>

                    <div class = "col-md-6">
                        <div class = "form-group">
                            {!! Form::label('commission_start_date', 'Commission Start Date') !!}
                            {!! Form::text('commission_start_date', NULL, ['id'=>'commission_start_date' , 'class'=>'form-control', 'datepicker-popup init-model'=>"commission_start_date", 'is-open'=>"status.opened_cs", 'ng-focus'=>'status.opened_cs = ! status.opened_cs']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_start_date') !!}
                        </div>
                    </div>
                </div>
                <div class = "clearfix"></div>

                <div class = "col-md-4 row">
                    <div class = "form-group">
                        {!! Form::label('commissionRecipient', 'Edit Recipient' ) !!}
                        {!! Form::checkbox('commissionRecipient', NULL, false, ['ng-model'=>'checked', 'id'=>'commissionRecipient']) !!}
                    </div>
                </div>
                <div class = "clearfix"></div>
                <br/>
            </div>
            <div class = "detail-group">
                <h4>Interest payment</h4>

                <div class = "row">
                    <div class = "col-md-6">
                        <div class = "form-group">
                            {!! Form::label('interest_payment_interval', 'Interest payment interval') !!}

                            {!! Form::select('interest_payment_interval',  $interest_payment_intervals, $investment->interest_payment_interval, ['class'=>'form-control', 'id'=>'interest_payment_interval', 'init-model'=>'interest_payment_interval']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_interval') !!}
                        </div>
                    </div>
                    <div class = "col-md-6">
                        <div class = "form-group">
                            {!! Form::label('interest_payment_date', 'Preferred payment date(1 - 31)') !!}

                            {!! Form::number('interest_payment_date', $investment->interest_payment_date, ['class'=>'form-control', 'init-model'=>'interest_payment_date']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_date') !!}
                        </div>
                    </div>
                    <div class = "col-md-12">
                        <div class = "alert alert-info"><p>When date is entered as 31st it will automatically be converted to end of month</p>
                        </div>
                        <div class = "form-group" ng-show="interest_payment_interval != 0">
                            {!! Form::label('interest_action_id', 'Select Interest Action') !!}

                            {!! Form::select('interest_action_id', $interestactions, NULL, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}
                        </div>
                    </div>
                    <div ng-show="interest_action_id == 2" class = "col-md-12">
                        <div class = "alert alert-info"><p>When no reinvest tenor is entered the interest will be reivested upto the maturity date of the investment</p>
                        </div>
                        <div class = "form-group" ng-show="interest_action_id == 2">
                            {!! Form::label('interest_reinvest_tenor', 'Interest Reinvest Tenor (months)') !!}
                            {!! Form::number('interest_reinvest_tenor', null, ['class' => 'form-control' , 'init-model' => 'interest_reinvest_tenor', 'id' => 'interest_reinvest_tenor'] ) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_reinvest_tenor') !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class = "col-md-12">
            <div class = "detail-group">
                {!! Form::submit('Rollover', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>