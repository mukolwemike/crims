<script type = "text/ng-template" id = "rollover.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">
                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>

                    @if(isset($schedule_transaction))
                        <h4 class="modal-title">Schedule Investment Rollover</h4>
                    @else
                        <h4 class = "modal-title">Confirm Investment Rollover</h4>
                    @endif
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Are you sure you want to rollover this investment with the details below?</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr ng-if="automatic_rollover == true">
                                <td>Automatic Rollover</td>
                                <td><% automatic_rollover %></td>
                            </tr>
                            <tr>
                                <td>New investment date</td>
                                <td><% investedDate | date %></td>
                            </tr>
                            <tr>
                                <td>New maturity date</td>
                                <td><% maturityDate | date %></td>
                            </tr>
                            <tr>
                                <td>On call</td>
                                <td><span ng-if="on_call">Yes</span><span ng-if="!on_call">No</span></td>
                            </tr>
                            <tr class = "success">
                                <td>Reinvest</td>
                                <td><% reinvest_val | currency:"" %></td>
                            </tr>
                            <tr class="danger">
                                <td>Withdraw</td>
                                <td><% withdraw_val | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Interest Rate</td>
                                <td><% interestRate %></td>
                            </tr>
                            <tr>
                                <td>Commission paid to</td>
                                <td><% commissionRecipient %></td>
                            </tr>
                            <tr>
                                <td>Commission rate</td>
                                <td><% commissionRate %></td>
                            </tr>
                            <tr ng-if="commissionStartDate">
                                <td>Commission Start Date</td>
                                <td><% commissionStartDate %></td>
                            </tr>
                            <tr>
                                <td>Interest paid</td>
                                <td><% interest_payment %></td>
                            </tr>
                            <tr>
                                <td>Interest paid on date</td>
                                <td><% interest_payment_date %></td>
                            </tr>
                            <tr>
                                <td>Interest Action</td>
                                <td><% interestAction %></td>
                            </tr>
                            <tr ng-if="interest_reinvest_tenor">
                                <td>Interest Reinvest Tenor</td>
                                <td><% interest_reinvest_tenor %> months</td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td><% description %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>'rollover_path']) !!}
                            <div style = "display:none !important;">
                                @if(isset($schedule_transaction))
                                    {!! Form::hidden('schedule', true) !!}
                                @endif
                                {!! Form::hidden('rollover_form_id', null) !!}
                                {!! Form::hidden('client_instruction', NULL, ['init-model'=>'client_instruction']) !!}
                                {!! Form::text('investment', Crypt::encrypt($investment->id)) !!}
                                {!! Form::text('reinvest', NULL, ['init-model'=>'reinvest']) !!}
                                {!! Form::text('invested_date', NULL, ['init-model'=>'investedDate']) !!}
                                {!! Form::text('maturity_date', NULL, ['init-model'=>'maturityDate']) !!}
                                {!! Form::text('interest_rate', NULL, ['init-model'=>'interestRate']) !!}
                                {!! Form::text('amount', NULL, ['init-model'=>'investedAmount']) !!}
                                {!! Form::text('commission_recepient', NULL, ['init-model'=>'commissionRecipientId']) !!}
                                {!! Form::text('commission_rate', NULL, ['init-model'=>'commissionRateId']) !!}
                                {!! Form::text('commission_start_date', NULL, ['init-model'=>'commissionStartDate']) !!}
                                {!! Form::text('interest_payment_interval', null, ['init-model'=>'interest_payment_interval']) !!}
                                {!! Form::text('interest_payment_date', null, ['init-model'=>'interest_payment_date']) !!}
                                {!! Form::text('on_call', null, ['ng-model'=>'on_call']) !!}
                                {!! Form::text('description', null, ['ng-model'=>'description']) !!}
                                {!! Form::text('interest_action_id', null, ['ng-model'=>'interest_action_id', 'ng-if'=>'interest_action_id']) !!}
                                {!! Form::text('interest_reinvest_tenor', null, ['ng-model'=>'interest_reinvest_tenor', 'ng-if'=>'interest_reinvest_tenor']) !!}
                                {!! Form::text('automatic_rollover', null, ['ng-model'=>'automatic_rollover']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
