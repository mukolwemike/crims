@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            <div class="form-detail detail-group">
                <h4>Commission details</h4>

                <label for="">FA Name</label> {!! $commission->recipient->name !!}<br/>
                <label for="">Rate</label> {!! $commission->rate !!} %<br/>
                <label for="">Duration</label> {!! $investment->repo->getTenorInDays() !!} days<br/>

                @if($commission->written_off)
                    <div class="alert alert-info"><p>Commission was written off since client withdrew when non-compliant</p></div>
                @endif
            </div>

            @include('investment.partials.investmentdetail', ['inv_title'=>'Investment Details'])
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <h4>Commission payment schedule</h4>
                <a href="#add-commission-payment-schedule" class="btn btn-success" data-toggle="modal" role="button">Add Schedule</a>
                <table class="table table-responsive table-striped table-hover">
                    <thead>
                    <tr><td>Date</td><td>Description</td><td>Amount</td><td></td></tr>
                    </thead>
                    <tbody>
                    @foreach($schedules as $schedule)
                        <tr>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#edit-schedule-{!! $schedule->id !!}"><i class="fa fa-pencil-square-o"></i> </a>
                                <div class="modal fade" id="edit-schedule-{!! $schedule->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::model($schedule, ['route'=>['edit_commission_payment_schedule', $schedule->id]]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="myModalLabel">Edit Commission Payment Schedule</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('date', 'Scheduled Date') !!}
                                                    <div ng-controller="DatepickerCtrl">{!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="2">Total</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($schedules->sum('amount')) !!}</th>
                        <th></th>
                    </tr>
                    </tbody>
                </table>

                @if($commission->clawBacks->count() > 0)
                    <h4>Commission claw back</h4>

                    <table class="table table-responsive table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($unpaidClawbacks) > 0)
                            <tr>
                                <th colspan="3">Clawbacks for Commission ID {!! $commission->id !!}</th>
                                <th>
                                    <a href="#" data-toggle="modal"
                                       data-target="#stagger-multiclawback-{!! $commission->id !!}"><i
                                                class="fa fa-pencil-square-o"></i> </a>
                                    <div class="modal fade" id="stagger-multiclawback-{!! $commission->id !!}"
                                         tabindex="-1"
                                         role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {!! Form::open(['route'=>['stagger_commission_clawbacks', $commission->id]]) !!}
                                                <div class="modal-header">
                                                    <a href="#" type="button" class="close" data-dismiss="modal"
                                                       aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></a>
                                                    <h4 class="modal-title" id="myModalLabel">Stagger Unpaid Clawbacks</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        {!! Form::label('stagger_duration', 'Stagger Duration') !!}
                                                        {!! Form::number('stagger_duration', null, ['class'=>'form-control']) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('date', 'Start Date') !!}
                                                        <div ng-controller="DatepickerCtrl">
                                                            {!! Form::text('start_date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::hidden('commission_id', $commission->id) !!}
                                                    {!! Form::hidden('clawback_id', null) !!}
                                                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        @endif
                        @foreach($commission->clawBacks as $clawBack)
                            <tr>
                                <td>{!! $clawBack->narration !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($clawBack->amount) !!}</td>
                                <td>{!! $clawBack->date !!}</td>
                                <td>
                                    <a href="#" data-toggle="modal"
                                       data-target="#stagger-singleclawback-{!! $clawBack->id !!}"><i
                                                class="fa fa-pencil-square-o"></i> </a>
                                    <div class="modal fade" id="stagger-singleclawback-{!! $clawBack->id !!}"
                                         tabindex="-1"
                                         role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {!! Form::open(['route'=>['stagger_commission_clawbacks', $commission->id]]) !!}
                                                <div class="modal-header">
                                                    <a href="#" type="button" class="close" data-dismiss="modal"
                                                       aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></a>
                                                    <h4 class="modal-title" id="myModalLabel">Stagger Clawback</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        {!! Form::label('stagger_duration', 'Stagger Duration') !!}
                                                        {!! Form::number('stagger_duration', null, ['class'=>'form-control']) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('date', 'Start Date') !!}
                                                        <div ng-controller="DatepickerCtrl">
                                                            {!! Form::text('start_date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::hidden('commission_id', $commission->id) !!}
                                                    {!! Form::hidden('clawback_id', $clawBack->id) !!}
                                                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <th>Total</th>
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($commission->clawBacks->sum('amount')) !!}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>

        {{--start of add commission payment modal--}}
        <div class="modal fade" id="add-commission-payment-schedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route'=>['add_commission_payment_schedule', $commission->recipient_id], 'method'=>'POST']) !!}
                    <div class="modal-header">
                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">Add Commission Payment Schedule</h4>
                    </div>
                    <div class="modal-body row">
                        <div class="form-group">
                            <div class="col-md-12">
                                {!! Form::label('date', 'Scheduled Date') !!}
                                {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened', 'ng-required'=>"true", 'id'=>'date']) !!}
                            </div>
                            <div class="col-md-12">
                                {!! Form::label('amount', 'Amount') !!}
                                {!! Form::number('amount', null, ['class'=>'form-control', 'step'=>'0.01', 'init-model'=>'number', 'ng-required'=>'true']) !!}
                            </div>
                            <div class="col-md-12">
                                {!! Form::label('description', 'Description') !!}
                                {!! Form::text('description', null, ['class'=>'form-control', 'init-model'=>'description']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! Form::hidden('investment_id', $commission->investment_id) !!}
                        {!! Form::submit('Save Schedule', ['class'=>'btn btn-success']) !!}
                        <a data-dismiss="modal" class="btn btn-default">Close</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--end of add commission payment modal--}}
    </div>
@endsection