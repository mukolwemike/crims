<table st-pipe="getPortfolioTransactions" st-table="displayed" class = "table table-striped table-responsive">
    <thead>
    <tr>
        <th colspan="2"></th>
        <th colspan = "1">
            <select st-search="transaction_type" class="form-control" type="text">
                <option value="">Transaction Type</option>
                @foreach($portfolio_transaction_types as $key => $type)
                    <option value="{!! $key !!}">{!! $type !!}</option>
                @endforeach
            </select>
        </th>
        <th colspan="1">
            <select st-search="institution" class="form-control">
                <option value="">All Institutions</option>
                @foreach($institutions as $institution)
                    <option value="{!! $institution !!}">{!! $institution !!}</option>
                @endforeach
            </select>
        </th>
        <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
    </tr>
    <tr>
        <th>Institution</th>
        <th>Transaction Type</th>
        <th>Originator</th>
        <th>Approved By</th>
        <th>Date</th>
        <th>Details</th>
    </tr>
    </thead>
    <tbody ng-show="!isLoading">
        <tr ng-repeat = "row in displayed">
            <td><% row.institution_name %></td>
            <td><% row.type %></td>
            <td><% row.sent_by_name %></td>
            <td><% row.approved_by %></td>
            <td><% row.approved_on %></td>
            <td>
                <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/portfolio/approve/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
            </td>
        </tr>
    </tbody>
    <tbody ng-show="isLoading">
    <tr>
        <td colspan="100%">Loading...</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan = "100%" dmc-pagination></td>
    </tr>
    </tfoot>
</table>