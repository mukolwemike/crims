<table st-table = "payDisplayedCollection" st-safe-src = "payCollection" class = "table table-striped table-responsive">
    <thead>
    <tr>
        <th colspan = "3"></th>
        <th colspan = "1"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
    </tr>
    <tr>
        <th st-sort = "client_name">Client Name</th>
        <th st-sort = "investment.amount">Amount</th>
        <th st-sort= "created_at">Time</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr ng-repeat = "row in payDisplayedCollection">
        <td><% row.name %></td>
        <td><% row.amount | currency:"" %></td>
        <td><time am-time-ago="row.created_at"></time></td>
        <td>
            <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/investments/pay/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan = "1" class = "text-center">
            Items per page
        </td>
        <td colspan = "1" class = "text-center">
            <input type = "text" ng-model = "itemsByPage"/>
        </td>
        <td colspan = "2" class = "text-center">
            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
        </td>
    </tr>
    </tfoot>
</table>