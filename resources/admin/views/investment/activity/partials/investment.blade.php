<table st-table = "invDisplayedCollection" st-safe-src = "invCollection" class = "table table-striped table-responsive">
    <thead>
    <tr>
        <th colspan = "9"></th>
        <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
    </tr>
    <tr>
        <th st-sort = "client.client_code">Client Code</th>
        <th st-sort = "fullName">Name</th>
        <th st-sort = "amount">Amount</th>
        <th>Action</th>
        <th st-sort = "invested_date">Investment Date</th>
        <th st-sort = "maturity_date">Maturity Date</th>
        <th st-sort = "interest_rate">Interest rate</th>
        <th st-sort = "currency">Currency</th>
        <th st-sort="originator">Originator</th>
        <th st-sort="approver">Approval</th>
        <th st-sort="created_at">Time</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr ng-repeat = "row in invDisplayedCollection">
        <td><% row.client_code %></td>
        <td><% row.name %></td>
        <td><% row.amount | currency:"" %></td>
        <td><% row.type %></td>
        <td><% row.invested_date | date%></td>
        <td><% row.maturity_date | date%></td>
        <td><% row.interest_rate %>%</td>
        <td><% row.currency %></td>
        <td><% row.originator %></td>
        <td><% row.approver %></td>
        <td><time am-time-ago="row.created_at"></time></td>
        <td>
            <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/investments/clientinvestments/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan = "2" class = "text-center">
            Items per page
        </td>
        <td colspan = "2" class = "text-center">
            <input type = "text" ng-model = "itemsByPage"/>
        </td>
        <td colspan = "4" class = "text-center">
            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
        </td>
    </tr>
    </tfoot>
</table>