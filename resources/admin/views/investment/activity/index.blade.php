@extends('layouts.default')

@section('content')
    <div class="panel-dashboard" ng-controller="ActivityLogPageController">
        <div class="col-md-12">
            <div class="pull-right">
                {!! Form::open() !!}
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('from') !!}
                        {!! Form::text('start', $start, ['class'=>'form-control', 'init-model'=>'start', 'datepicker-popup is-open'=>"statusSecond.opened", 'ng-focus'=>'openSecond($event)']) !!}
                    </div>
                </div>

                <div class="col-md-4">
                    {!! Form::label('to') !!}
                    {!! Form::text('end', $end, ['class'=>'form-control', 'init-model'=>'end', 'datepicker-popup is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::submit('Filter', ['class'=>'btn btn-success margin-top-25']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
        {{--<div class="clearfix"></div>--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="detail-group">--}}
                {{--<table class="table table-responsive table-hover">--}}
                    {{--<tbody>--}}
                        {{--<tr><td>Investments</td><td><% invCollection.length %></td></tr>--}}
                        {{--<tr><td>Business Confirmations</td><td><% confCollection.length %></td></tr>--}}
                        {{--<tr><td>Clients Onboarded</td><td><% appCollection.length %></td></tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="detail-group">--}}
                {{--<table class="table table-responsive table-hover">--}}
                    {{--<tbody>--}}
                        {{--<tr><td>Bank Instructions</td><td><% instrCollection.length %></td></tr>--}}
                        {{--<tr><td>Payments</td><td><% payCollection.length %></td></tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-12">--}}
            {{--<div ng-if="loading" class="loading">--}}
                {{--<p class="loading-spinner"><i class="fa fa-circle-o-notch fa-spin"></i> Loading data</p>--}}
            {{--</div>--}}
            {{--<div ng-if="failed">--}}
                {{--<div ng-if="failed" class="alert alert-danger">--}}
                    {{--<p>Could not load data, check your network connection or contact admin</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="col-md-12" ng-if="!loading">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="#client_transactions" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Client Transactions</a></li>
                <li role="presentation" class=""><a href="#portfolio_transactions" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Portfolio Transactions</a></li>
                {{--<li role="presentation" class=""><a href="#investments" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Investments</a></li>--}}
                {{--<li role="presentation" class=""><a href="#confirmations" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Business Confirmations</a></li>--}}
                {{--<li role="presentation" class=""><a href="#applications" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Clients Onboarded</a></li>--}}
                {{--<li role="presentation" class=""><a href="#instructions" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Instructions</a></li>--}}
                {{--<li role="presentation" class=""><a href="#payments" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Payments</a></li>--}}
            </ul>

            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="client_transactions" aria-labelledby="home-tab" ng-controller="InvestmentActivityGridCtrl">
                    @include('investment.activity.partials.client_transactions')
                </div>
                <div role="tabpanel" class="tab-pane fade" id="portfolio_transactions" aria-labelledby="home-tab" ng-controller="InvestmentActivityGridCtrl">
                    @include('investment.activity.partials.portfolio_transactions')
                </div>
                {{--<div role="tabpanel" class="tab-pane fade" id="investments" aria-labelledby="home-tab">--}}
                    {{--@include('investment.activity.partials.investment')--}}
                {{--</div>--}}
                {{--<div role="tabpanel" class="tab-pane fade" id="confirmations" aria-labelledby="home-tab">--}}
                    {{--@include('investment.activity.partials.confirmations')--}}
                {{--</div>--}}
                {{--<div role="tabpanel" class="tab-pane fade" id="applications" aria-labelledby="home-tab">--}}
                    {{--@include('investment.activity.partials.applications')--}}
                {{--</div>--}}
                {{--<div role="tabpanel" class="tab-pane fade" id="instructions" aria-labelledby="home-tab">--}}
                    {{--@include('investment.activity.partials.instructions')--}}
                {{--</div>--}}
                {{--<div role="tabpanel" class="tab-pane fade" id="payments" aria-labelledby="home-tab">--}}
                    {{--@include('investment.activity.partials.payment')--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection