<?php $app = $application;?>
@extends('layouts.default')

@section('content')
    <div class = "col-md-12 panel-dashboard">
        <div class = "detail-group">
            <h4 class = "form-separator col-md-12">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($app->client_id) !!}</h4>

            @if($app->approval)
                @if($app->approval->approved)
                    <div class="alert alert-info">
                        <p>This application has been approved, editing it will resend it for approval to confirm the details</p>
                    </div>
                @endif
            @endif
        </div>
        {!! Form::open(['route'=>['edit_investment_application_path', $app->id]]) !!}
            <div class = "detail-group">
                <h4 class = "form-separator col-md-12">Application details</h4>

                <div class = "form-group">
                    <div class = "col-md-2">{!! Form::label('product_id', 'Product') !!}</div>

                    <div class = "col-md-4">{!! Form::select('product_id', $products , $app->product_id, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                    </div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-2">{!! Form::label('amount', 'Total Commitment') !!}</div>

                    <div class = "col-md-4">{!! Form::number('amount', $app->amount, ['class'=>'form-control', 'placeholder'=>'Amount invested', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-2">{!! Form::label('tenor', 'Investment Duration (Tenor in Months)') !!}</div>

                    <div class = "col-md-4">{!! Form::number('tenor', $app->tenor, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tenor') !!}</div>
                </div>
                <div class = "form-group">
                    <div class = "col-md-2">{!! Form::label('agreed_rate', 'Agreed rate (%)') !!}</div>

                    <div class = "col-md-4">{!! Form::number('agreed_rate', $app->agreed_rate, ['class'=>'form-control', 'placeholder'=>'Interest rate', 'step'=>'0.01', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'agreed_rate') !!}</div>
                </div>
            </div>

            <div class = "detail-group">
                <h4 class = "form-separator col-md-12">Source of funds</h4>
                <!-- Source of funds -->

                <div class = "form-group">
                    <div class = "col-md-2">{!! Form::label('funds_source_id', 'Source of funds') !!}</div>

                    <div class = "col-md-4">{!! Form::select('funds_source_id', $fundsources, null,  ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'funds_source_id') !!}</div>
                </div>

                <div class = "form-group">
                    <div class = "col-md-2">{!! Form::label('funds_source_other', 'Other source of funds') !!}</div>

                    <div class = "col-md-4">{!! Form::text('funds_source_other', $app->funds_source_other, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'funds_source_other') !!}</div>
                </div>

                <div class="form-group col-md-12">
                    <div class="col-md-3">{!! Form::checkbox('terms_accepted', true, ['class'=>'form-control']) !!}

                        {!! Form::label('terms_accepted', 'Terms accepted') !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'terms_accepted') !!}</div>
                    <div class="pull-right">All fields with  (<span class="required-field"></span>) are required and must be filled before submitting.</div>
                </div>

            </div>

            @if($app->clientType->name == 'individual')
                @if($app->new_client == 1 || $app->new_client == 2)
                    <div class="detail-group">
                        <h4 class="form-separator col-md-12">Personal Details</h4>

                        <!-- personal details -->
                        <div class="form-group">

                            <div class="col-md-2">
                                <span class="required-field">
                                    {!! Form::label('title', 'Full Name') !!}
                                </span>
                            </div>

                            <div class="col-md-2">
                                {!! Form::select('title', $client_titles , $app->title, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title') !!}

                                <div class="clearfix"></div>
                            </div>


                            <div class="col-md-2">{!! Form::text('lastname', $app->lastname, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'surname') !!}

                            </div>

                            <div class="col-md-2">

                                {!! Form::text('middlename', $app->middlename, ['class'=>'form-control', 'placeholder'=>'Middle name']) !!}

                            </div>

                            <div class="col-md-2 ">

                                {!! Form::text('firstname', $app->firstname, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}

                            </div>

                        </div>


                        <div class="clearfix"></div>

                        <div class="form-group">
                            <div class="col-md-2">

                                {!! Form::label('gender_id', 'Gender') !!}

                            </div>

                            <div class="col-md-4">

                                {!! Form::select('gender_id', $genders , $app->gender_id, ['class'=>'form-control']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'gender_id') !!}

                            </div>
                        </div>

                        <div class="form-group group">
                            <div class="col-md-2">

                                {!! Form::label('dob', 'Date of birth') !!}

                            </div>

                            <div class="col-md-4" ng-controller="DatepickerCtrl">
                                <div class="input-group">

                                    {!! Form::text('dob', $app->dob, [ 'class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}

                                    <span class="input-group-btn">

                                        <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>

                                    </span>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">

                                    {!! Form::label('pin_no', 'PIN No.') !!}

                                </span>
                            </div>

                            <div class="col-md-4">

                                {!! Form::text('pin_no', $app->pin_no, ['class'=>'form-control']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">

                                    {!! Form::label('taxable', 'Taxable') !!}

                                </span>
                            </div>

                            <div class="col-md-4">

                                {!! Form::select('taxable', [1=>'Yes', 0=>'No'], $app->taxable, ['class'=>'form-control']) !!}

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="col-md-2">

                                <span class="required-field">
                                    {!! Form::label('id_or_passport', 'ID/Passport no.') !!}
                                </span>
                            </div>

                            <div class="col-md-4">

                                {!! Form::text('id_or_passport', $app->id_or_passport, ['class'=>'form-control']) !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">
                                    {!! Form::label('email', 'Email') !!}
                                </span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('email', $app->email, ['class'=>'form-control', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">
                                    {!! Form::label('postal_address', 'Postal Address') !!}
                                </span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('postal_address', $app->postal_address, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">
                                    {!! Form::label('postal_code', 'Postal code') !!}
                                </span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('postal_code', $app->postal_address, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('street', 'Mailing Address') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('street', $app->street, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">
                                    {!! Form::label('town', 'Town') !!}
                                </span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('town', $app->town, ['class'=>'form-control', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('country_id', 'Country of Residence') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::select('country_id', $countries, $app->country_id, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <span class="required-field">
                                    {!! Form::label('phone', 'Cell Phone') !!}
                                </span>
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('phone', $app->phone, ['class'=>'form-control', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('telephone_office', 'Office Tel no.') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('telephone_office', $app->telephone_office, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">{!! Form::label('telephone_home', 'Home Tel no.') !!}</div>

                            <div class="col-md-4">
                                {!! Form::text('telephone_home', $app->telephone_home, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('residential_address', 'Residential Address') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('residential_address', $app->residential_address, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="detail-group">
                        <h4 class="form-separator col-md-12">Employment Details</h4>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('employment_id', 'Employed') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::select('employment_id', $employment_types, $app->employment_id, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('employment_other', 'Employment Other') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('employment_other', $app->employment_other, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('present_occupation', 'Present occupation') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('present_occupation', $application->present_occupation, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('business_sector', 'Business sector') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('business_sector', $app->business_sector, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('employer_name', 'Employer name') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('employer_name', $app->employer_name, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('employer_address', 'Employer address') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::text('employer_address', $app->employer_address, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                {!! Form::label('method_of_contact_id', 'Preferred Method of contact') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::select('method_of_contact_id', $contact_methods, $app->method_of_contact_id, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'method_of_contact_id') !!}
                            </div>
                        </div>
                    </div>
                @endif
            @elseif($app->clientType->name == 'individual')
            <div class="detail-group">

                <h4 class="form-separator col-md-12">Investor Details</h4>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('registered_name', 'Registered Name') !!}
                        </span>
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('registered_name', $app->registered_name, ['class'=>'form-control', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_name') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('trade_name', 'Trade name') !!}
                    </div>

                    <div class="col-md-4">
                        {!! Form::text('trade_name', $app->trade_name, ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="group"></div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('registered_address', 'Registered address') !!}
                        </span>
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('registered_address', $app->registered_address, ['class'=>'form-control', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_address') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">

                        {!! Form::label('registration_number', 'Registration number') !!}

                    </div>

                    <div class="col-md-4">

                        {!! Form::text('registration_number', $app->registration_number, ['class'=>'form-control']) !!}

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-2">

                        {!! Form::label('postal_address', 'Postal Address') !!}

                    </div>

                    <div class="col-md-4">

                        {!! Form::text('postal_address', $app->postal_address, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group">

                    <div class="col-md-2">
                        {!! Form::label('postal_code', 'Postal code') !!}
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('postal_code', $app->postal_code, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('street', 'Mailing address') !!}
                        </span>
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('street', $app->street, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('town', 'Town') !!}
                        </span></div>

                    <div class="col-md-4">

                        {!! Form::text('town', $app->town, ['class'=>'form-control', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('country_id', 'Country of Residence') !!}
                    </div>

                    <div class="col-md-4">
                        {!! Form::select('country_id', $countries, $app->country_id, ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('phone', 'Telephone Number') !!}
                        </span>
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('phone', $app->phone, ['class'=>'form-control', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('email', 'E-mail') !!}
                        </span>
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('email', $app->email, ['class'=>'form-control', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('residential_address', 'Physical Address') !!}
                    </div>

                    <div class="col-md-4">
                        {!! Form::text('residential_address', $app->residential_address, ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="group"></div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('pin_no', 'Company PIN Number') !!}
                        </span>
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('pin_no', $app->pin_no, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('taxable', 'Taxable') !!}
                        </span></div>

                    <div class="col-md-4">

                        {!! Form::select('taxable', [1=>'Yes', 0=>'No'], $app->taxable, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('method_of_contact_id', 'Preferred Method of contact') !!}
                    </div>

                    <div class="col-md-4">
                        {!! Form::select('method_of_contact_id', $contact_methods, $app->method_of_contact_id, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_method_id') !!}
                    </div>
                </div>
            </div>

            <div class="detail-group">
                <div class="col-md-12"><h4>Contact person</h4></div>


                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">
                            {!! Form::label('contact_person', 'Contact person') !!}
                        </span>
                    </div>

                    <div class="col-md-2">
                        {!! Form::select('contact_person_title', $client_titles , $app->contact_person_title, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}
                    </div>

                    <div class="col-md-4">

                        {!! Form::text('contact_person_lname', $app->contact_person_lname, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_lname') !!}

                    </div>


                    <div class="col-md-4">

                        {!! Form::text('contact_person_fname', $app->contact_person_fname, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_fname') !!}</div>

                    <div class="clearfix"></div>
                </div>
            </div>
            @endif

        <div class = "detail-group">
            {!! Form::submit('Edit', ['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop