
<div class="detail-group">
    <h4 class="col-md-12">New or Existing Client</h4>

    <div class="col-md-2">
        {!! Form::label('new_client', 'New or Existing Client') !!}
    </div>

    <div class="col-md-4">
        {!! Form::select('new_client', [1=>'New Client', 0=>'Existing Client', 2=>'Duplicate Client'], null, ['class'=>'form-control', 'init-model'=>'new_client']) !!}
    </div>


    <div class="col-md-2" ng-show="new_client == 0">
        {!! Form::label('client_name', 'Search Client') !!}
    </div>

    <div class="col-md-4" ng-show="new_client == 0">
        <input autocomplete="off" type="text" ng-model="selectedClient" placeholder="Enter the client name" typeahead="client as client.fullName for client in searchClients($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">

        <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
        <div ng-show="noResults">
            <i class="glyphicon glyphicon-remove"></i> No Results Found
        </div>
    </div>

    <div class="form-group" ng-show="new_client == 1 || new_client == 3">
        <div class="col-md-2"> <span class="required-field">{!! Form::label('complete', 'Client type') !!}</span></div>

        <div class="col-md-4">{!! Form::select('complete', [true=>'Complete', false=>'Incomplete'], true, ['class'=>'form-control', 'init-model'=>'complete']) !!}
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'complete') !!}
        </div>
    </div>

    <div class="col-md-4" ng-if="selectedClient">
        Name: <% selectedClient.fullName %>
    </div>

    <div class="col-md-4" ng-if="selectedClient">
        Email: <% selectedClient.email %>
    </div>

    <div class="col-md-4" ng-if="selectedClient">
        Client Code: <% selectedClient.client_code %>
    </div>

    <div class="hide">
        <input name="client_id" type="text" init-model="selectedClient.id">
    </div>

</div>