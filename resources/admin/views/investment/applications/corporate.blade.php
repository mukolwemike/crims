@extends('layouts.default')

@section('content')
    <div class="col-md-11" ng-controller="InvestmentApplicationCtrl">

        <div class="col-md-12 panel panel-dashboard form-grouping"><!-- nesting grid -->
            {!! Form::model($application, ['url'=>'/dashboard/investments/apply/corporate/'.$application->id, $application->id, 'id'=>'application_form']) !!}

            @include('investment.applications.partials.new_or_existing')

            @include('investment.partials.clientapplicationdetails')

            <div class="detail-group">
                <h4 class="form-separator col-md-12">Type of Investor</h4>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('corporate_investor_type', 'Type of Investor') !!}</div>


                    <div class="col-md-4">{!! Form::select('corporate_investor_type', $investor_types,null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_investor_type') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('corporate_investor_type_other', 'Other') !!}</div>


                        <div class="col-md-4">{!! Form::text('corporate_investor_type_other', null, ['class'=>'form-control']) !!}</div>
                    </div>
                </div>

            <div class="detail-group">

                <h4 class="form-separator col-md-12">Investor Details</h4>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('registered_name', 'Registered Name') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('registered_name', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_name') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('trade_name', 'Trade name') !!}</div>

                    <div class="col-md-4">{!! Form::text('trade_name', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="group"></div>
                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('registered_address', 'Registered address') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('registered_address', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_address') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('registration_number', 'Registration number') !!}</div>

                    <div class="col-md-4">{!! Form::text('registration_number', null, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('postal_address', 'Postal Address') !!}</div>

                    <div class="col-md-4">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('postal_code', 'Postal code') !!}</div>

                    <div class="col-md-4">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('street', 'Mailing address') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('street', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('town', 'Town') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('town', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('country_id', 'Country of Residence') !!}</div>

                    <div class="col-md-4">{!! Form::select('country_id', $countries, '114', ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('phone', 'Telephone Number') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('phone', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('email', 'E-mail') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('email', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('residential_address', 'Physical Address') !!}</div>

                    <div class="col-md-4">{!! Form::text('residential_address', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="group"></div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('pin_no', 'Company PIN Number') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('taxable', 'Taxable') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('taxable', [1=>'Yes', 0=>'No'], null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('method_of_contact_id', 'Preferred Method of contact') !!}</div>

                    <div class="col-md-4">{!! Form::select('method_of_contact_id', $contact_methods, null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_method_id') !!}</div>
                </div>
            </div>

            <div class="detail-group">
                <div class="col-md-12"><h4>Contact person</h4></div>


                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('contact_person', 'Contact person') !!}</span></div>

                        <div class="col-md-2">{!! Form::select('contact_person_title', $client_titles , null, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}</div>

                        <div class="col-md-4">{!! Form::text('contact_person_lname', null, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_lname') !!}</div>


                        <div class="col-md-4">{!! Form::text('contact_person_fname', null, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_fname') !!}</div>
                        <div class="clearfix"></div>
                </div>
            </div>


            @include('investment.partials.applicationformdetails')

            <div class="detail-group">
                <div class="col-md-12 margin-top-20">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
