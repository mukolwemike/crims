@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div ng-controller="InvestmentApplicationsController">
            <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan="3">
                        {{--<a href="/dashboard/investments/apply/individual" class="btn btn-success margin-bottom-20"><i class="fa fa-plus"></i> Individual</a>--}}
                        {{--<a href="/dashboard/investments/apply/corporate" class="btn btn-success margin-bottom-20"><i class="fa fa-plus"></i> Corporate</a>--}}
                    </th>
                    <th colspan="2"></th>
                    <th colspan="2">
                        <select st-search="status" class="form-control">
                            <option value="">All Applications</option>
                            <option value="invested">Invested</option>
                            <option value="not_invested">Not Invested</option>
                        </select>
                    </th>
                    <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                </tr>
                <tr>
                    <th>Application ID</th>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th>email</th>
                    <th>Email Indemnity</th>
                    <th>Amount</th>
                    <th>Product</th>
                    <th>Currency</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                <tr ng-repeat="row in displayed">
                    <td> <%row.id%></td>
                    <td><% row.client.data.client_code %></td>
                    <td><% row.client.data.fullName  %></td>
                    <td><% row.client.data.email  %></td>
                    <td><% row.email_indemnity | yesno  %></td>
                    <td><% row.amount | currency:""  %></td>
                    <td><% row.product.data.name %></td>
                    <td><% row.currency %></td>
                    <td><span to-html="row.status | applicationStatus"></span></td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/applications/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                        <a ng-show="row.status == 'not invested'" ng-controller = "PopoverCtrl" uib-popover = "Edit details" popover-trigger = "mouseenter" href = "/dashboard/investments/applications/edit/<%row.id%>"><i class = "fa fa-edit"></i></a>
                    </td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="10" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "4" class = "text-center">
                        Items per page
                    </td>
                    <td colspan = "2" class = "text-center">
                        <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                    <td colspan="1"></td>
                    {{--<td class="text-center" st-pagination="" st-items-by-page="10" colspan="6"></td>--}}
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop
