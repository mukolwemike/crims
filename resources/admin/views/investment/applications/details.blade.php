<?php
        $app = $application;

        $account = null;

        $branch = $app->branch;

try {
    $approved = $app->approval->approved;
} catch (Exception $e) {
    $approved = false;
}
?>

@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1" ng-controller = "ClientApplicationDetailCtrl">
        <div class = "col-md-12 panel-dashboard">
            <div class = "detail-group">
                <h4 class = "col-md-12">Application</h4>

                @if($app->product)
                    <p class = "col-md-4">Product: {!! $app->product->name  !!} </p>
                @endif

                <p class = "col-md-4">Total Commitment: {!!\Cytonn\Presenters\AmountPresenter::currency($app->amount)!!} </p>

                <p class = "col-md-4">Partnership Account Number: {!! $app->partnership_account_number !!}</p>

                <p class = "col-md-4">Agreed rate of return: {!! $app->agreed_rate.'%'!!} </p>

                <p class = "col-md-4">Investment Date: {!! Cytonn\Presenters\DatePresenter::formatDate($app->date) !!}</p>

                <p class = "col-md-4">Tenor In Months: {!! $app->tenor !!}</p>
            </div>


            @if($app->type_id == 1)
                @include('investment.partials.individualdetails', ['app'=>$app])
            @endif

            @if($app->type_id == 2)
                @include('investment.partials.corporatedetails', ['app'=>$app, ])
            @endif

            <div class = "detail-group">
                <h4 class = "col-md-12">Source of Funds</h4>

                @if($app->fundSource)
                <p class = "col-md-4">Source of Funds: {!! $app->fundSource->name !!}</p>
                @endif

                <p class = "col-md-6">Details: {!! $app->funds_source_other !!}</p>
            </div>

            @if($app->new_client == 1)
                <div class = "detail-group">
                    <h4 class = "col-md-12">Investor Bank information</h4>

                    <p class = "col-md-4">Account name: {!! $app->investor_account_name !!}</p>

                    <p class = "col-md-4">Account number: {!! $app->investor_account_number !!}</p>

                    @if($branch)
                        <p class = "col-md-4">Bank: {!! $branch->bank->name !!}</p>

                        <p class = "col-md-4">Branch: {!! $branch->name !!}</p>

                        <p class = "col-md-4">Clearing Code: {!! $branch->bank->clearing_code !!}</p>

                        <p class = "col-md-4">Swift code: {!! $branch->bank->swift_code !!}</p>
                    @endif

                    <p class = "col-md-4">Email Indemnity: {!! Cytonn\Presenters\BooleanPresenter::presentYesNo($app->email_indemnity) !!}</p>

                    <p class = "col-md-4">Terms
                        accepted: {!! Cytonn\Presenters\BooleanPresenter::presentIcon($app->terms_accepted) !!}</p>
                </div>
            @elseif($app->new_client == 0 && $client)
                <div class = "detail-group">
                    <h4 class = "col-md-12">Investor Bank information</h4>

                    <p class = "col-md-4">Account name: {!! $client->investor_account_name !!}</p>

                    <p class = "col-md-4">Account number: {!! $client->investor_account_number !!}</p>

                    @if($branch)
                        <p class = "col-md-4">Bank: {!! $branch->bank->name !!}</p>

                        <p class = "col-md-4">Branch: {!! $branch->name !!}</p>

                        <p class = "col-md-4">Clearing Code: {!! $branch->bank->clearing_code !!}</p>

                        <p class = "col-md-4">Swift code: {!! $branch->bank->swift_code !!}</p>
                    @endif

                    <p class = "col-md-4">Email Indemnity: {!! Cytonn\Presenters\BooleanPresenter::presentYesNo($app->email_indemnity) !!}</p>

                    <p class = "col-md-4">Terms
                        accepted: {!! Cytonn\Presenters\BooleanPresenter::presentIcon($app->terms_accepted) !!}</p>
                </div>
            @endif

            <div class = "detail-group">
                @if($jointHolders)
                <h4>Joint Holders</h4>
                    <table class = "table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                            <th>Pin Number</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jointHolders as $holder)
                            <tr>
                                <td><a href="/dashboard/clients/joint/{!! $holder->id !!}">{!! ucfirst($holder->firstname) !!} {!! ucfirst($holder->lastname) !!}</a></td>
                                <td>{!! $holder->email !!}</td>
                                <td>{!! $holder->telephone_cell !!}</td>
                                <td>{!! $holder->pin_no !!}</td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#edit-joint-holder-{!! $holder->id !!}" title="Edit Joint Holder"><i class = "fa fa-pencil-square-o"></i></a>
                                    <!-- Edit Modal -->
                                    <div class="modal fade" id="edit-joint-holder-{!! $holder->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {!! Form::open(['route'=>['edit_joint_holder', $app->id, $holder->id]]) !!}
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit Joint Holder</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form">
                                                        {!! Form::hidden('holder_id', $holder->id, null) !!}

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <span class="">{!! Form::label('title_id', 'Full name') !!}<br/>
                                                                        {!! Form::select('title_id', $clientTitles , $holder->title, ['class'=>'form-control', 'required']) !!}
                                                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title') !!}
                                                                        <div class="clearfix"></div>
                                                                    </span>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <span class="required-field">{!! Form::label('lastname', 'Last Name') !!}</span><br/>
                                                                    {!! Form::text('lastname', $holder->lastname, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}
                                                                </div>

                                                                <div class="col-md-4 ">
                                                                    <span class="required-field">{!! Form::label('firstname', 'First Name') !!}</span><br/>
                                                                    {!! Form::text('firstname', $holder->firstname, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <span class="">{!! Form::label('middlename', 'Middle Name') !!}</span><br/>
                                                                    {!! Form::text('middlename', $holder->middlename, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <span class="">{!! Form::label('gender_id', 'Gender') !!}</span><br/>
                                                                    {!! Form::select('gender_id', $gender , $holder->gender, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'gender') !!}
                                                                </div>

                                                                <div class="col-md-4" ng-controller="DatepickerCtrl">
                                                                    <span class="">{!! Form::label('dob', 'Date of Birth') !!}</span><br/>
                                                                    {!! Form::text('dob', $holder->dob, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'dob') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <span class="required-field">{!! Form::label('telephone_cell', 'Phone Number') !!}</span><br/>
                                                                    {!! Form::text('telephone_cell', $holder->telephone_cell, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_cell') !!}
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <span class="required-field">{!! Form::label('pin_no', 'Pin Number') !!}</span><br/>
                                                                    {!! Form::text('pin_no', $holder->pin_no, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}
                                                                </div>

                                                                <div class="col-md-4 ">
                                                                    <span class="required-field">{!! Form::label('id_or_passport', 'ID/ Passport') !!}</span><br/>
                                                                    {!! Form::text('id_or_passport', $holder->id_or_passport, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_or_passport') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <span class="required-field">{!! Form::label('email', 'Email Address') !!}</span><br/>
                                                                    {!! Form::text('email', $holder->email, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <span class="">{!! Form::label('postal_code', 'Postal Code') !!}</span><br/>
                                                                    {!! Form::text('postal_code', $holder->postal_code, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}
                                                                </div>

                                                                <div class="col-md-4 ">
                                                                    <span class="">{!! Form::label('postal_address', 'Postal Address') !!}</span><br/>
                                                                    {!! Form::text('postal_address', $holder->postal_address, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <span class="">{!! Form::label('residential_address', 'Residential Address') !!}</span><br/>
                                                                    {!! Form::text('residential_address', $holder->residential_address, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'residential_address') !!}
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <span class="">{!! Form::label('country_id', 'Country') !!}</span><br/>
                                                                    {!! Form::select('country_id', $countries , $holder->country_id, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country_id') !!}
                                                                </div>

                                                                <div class="col-md-4 ">
                                                                    <span class="">{!! Form::label('town', 'Town') !!}</span><br/>
                                                                    {!! Form::text('town', $holder->town, ['class'=>'form-control', 'required']) !!}
                                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                @endif
                @if(! $approved)
                    <a class = "btn btn-success" href = "/dashboard/investments/applications/{!! $app->id !!}/joint">Add Joint Holder</a>
                @endif
            </div>


            <div class="detail-group">
                <h4>Contact Information</h4>

                <h5>Additional Email Addresses</h5>

                @if($approved)
                    @if(count($clientEmails) > 0)
                        <ol>
                            @foreach($clientEmails as $email)
                                <li>{!! $email !!}</li>
                            @endforeach
                        </ol>
                    @else
                        <p>No additional email saved</p><br/>
                    @endif
                @endif

                @if(!$approved)
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#email-modal">
                        Add/Edit Email Addresses
                    </button>
                @endif

                <h5>Contact Persons</h5>

                @if($approved)
                    @if($app->client->contactPersons()->count() == 0)
                        <p>No contact persons added</p><br/>
                    @else
                        <table class="table table-responsive table-striped">
                            <thead>
                            <th>Name</th><th>Phone</th><th>Email</th><th>Address</th>@if(!$approved)<th></th>@endif
                            </thead>
                            <tbody>
                            @foreach($app->client->contactPersons as $contact)
                                <tr>
                                    <td>{!! $contact->name !!}</td>
                                    <td>{!! $contact->phone !!}</td>
                                    <td>{!! $contact->email !!}</td>
                                    <td>{!! $contact->address !!}</td>
                                    @if(!$approved)
                                        <td>
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#contact-modal-{!! $contact->id !!}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                @endif


                @if(!$approved)
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#contact-modal">
                        Add a contact person
                    </button>
                @endif

            </div>

            {{--application documents--}}

            <div class="detail-group">
                <h4>Application Documents</h4>

                @if(count($documents))
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr><th>Type</th><th>Uploaded</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $doc)
                            <tr>
                                <td>{!! $doc->complianceDocument ? $doc->complianceDocument->name : $doc->document->type->name !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($doc->created_at) !!}</td>
                                <td>
                                    <a target="_blank" href="/document/view-document/{{$doc->document_id}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p>No Application documents uploaded</p><br/>
                @endif
            </div>

            <!-- Invest application -->
            @if($approved)
                @if($app->repo->status() != 'invested')
                    @if(Auth::user()->isAbleTo('addinvestment'))
                        <div class = "detail-group">
                            <div class = "col-md-6">
                                <h4>Invest Application</h4>
                                <a href = "/dashboard/investments/applications/invest/{!! $app->id !!}"
                                   class = "btn btn-success">Invest application</a>
                            </div>
                        </div>
                    @else
                        <div class = "alert alert-warning"><p>This application has not been invested</p></div>
                    @endif
                @else
                    <div class = "alert alert-info"><p>This application has already been invested</p></div>
                @endif
            @else
                <div class="detail-group">
                    <div class = "alert alert-warning"><p>This application has not been approved, it has to be approved before you can invest it</p></div>

                    <a href = "/dashboard/investments/applications/{!! $app->id !!}/request"
                       class = "btn btn-success">Send Approval Request</a>
                </div>
            @endif

            @if($app->repo->status() != 'invested')
                <div class="detail-group">
                    <div class="col-md-12">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#remove-application">
                            Delete Application
                        </button>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop


<!-- Add/Edit Email -->
@if($approved)
    <div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['add_additional_email_path', $app->id ]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add an email</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('emails', 'Enter emails separated by commas') !!}

                        {!! Form::textarea('emails', implode(', ', $clientEmails), ['class'=>'form-control', 'rows'=>3]) !!}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endif

<!-- Add Contact person modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['add_contact_person_path', $app->id ]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a contact person</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}

                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}

                    {!! Form::text('email', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address', 'Address') !!}

                    {!! Form::text('address', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Phone Number') !!}

                    {!! Form::text('phone', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@if($approved)
    @foreach($app->client->contactPersons as $contact)
        <!-- Edit Contact person modal -->
        <div class="modal fade" id="contact-modal-{!! $contact->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route'=>['add_contact_person_path', $app->id ]]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add a contact person</h4>
                    </div>
                    <div class="modal-ody">
                        {!! Form::hidden('id', $contact->id) !!}
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}

                                {!! Form::text('name', $contact->name, ['class'=>'form-control']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}

                                {!! Form::text('email', $contact->email, ['class'=>'form-control']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('address', 'Address') !!}

                                {!! Form::text('address', $contact->address, ['class'=>'form-control']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('phone', 'Phone Number') !!}

                                {!! Form::text('phone', $contact->phone, ['class'=>'form-control']) !!}

                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endforeach
@endif

<!-- Remove application -->
<div class="modal fade" id="remove-application" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['delete_application_path', $app->id ]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Remove Investment Application</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        <p>Are you sure you want to remove the application?</p>
                    </div>

                    <div class="form-group">
                        {!! Form::label('reason', 'Enter the reason why you are deleting the application') !!}

                        {!! Form::text('reason', null, ['class'=>'form-control', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>