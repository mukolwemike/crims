@extends('layouts.default')

@section('content')
    <div class="col-md-11" ng-controller="InvestmentApplicationCtrl">

        <div class="col-md-12 panel panel-dashboard form-grouping"><!-- nesting grid -->
            {!! Form::model($application, ['url'=>'/dashboard/investments/apply/individual/'.$application->id, $application->id, 'id'=>'application_form']) !!}

            @include('investment.applications.partials.new_or_existing')

            @include('investment.partials.clientapplicationdetails')


            <div class="detail-group" ng-if="new_client == 1 || new_client == 2">
                <h4 class="form-separator col-md-12">Personal Details</h4>

                <!-- personal details -->
                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('title', 'Full name') !!}</span></div>

                    <div class="col-md-2">
                        {!! Form::select('title', $client_titles , null, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title') !!}
                        <div class="clearfix"></div>
                    </div>


                    <div class="col-md-2">{!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'surname') !!}</div>

                    <div class="col-md-2">{!! Form::text('middlename', null, ['class'=>'form-control', 'placeholder'=>'Middle name']) !!}</div>

                    <div class="col-md-2 ">{!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>

                    <div class="col-md-2 ">{!! Form::text('preferredname', null, ['class'=>'form-control', 'placeholder'=>'Preferred name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'preferredname') !!}</div>

                </div>


                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('gender_id', 'Gender') !!}</div>

                    <div class="col-md-4">{!! Form::select('gender_id', $genders , null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'gender_id') !!}</div>
                </div>

                <div class="form-group group">
                    <div class="col-md-2">{!! Form::label('dob', 'Date of birth') !!}</div>

                    <div class="col-md-4" ng-controller="DatepickerCtrl">
                        <div class="input-group">
                            {!! Form::text('dob', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('pin_no', 'PIN No.') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('taxable', 'Taxable') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('taxable', [1=>'Yes', 0=>'No'], null, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('id_or_passport', 'ID/Passport no.') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('id_or_passport', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('email', 'Email') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('email', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_address', 'Postal Address') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_code', 'Postal code') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('street', 'Mailing Address') !!}</div>

                    <div class="col-md-4">{!! Form::text('street', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('town', 'Town') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('town', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('country_id', 'Country of Residence') !!}</div>

                    <div class="col-md-4">{!! Form::select('country_id', $countries, '114', ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('phone', 'Cell Phone') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('phone', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('telephone_office', 'Office Tel no.') !!}</div>

                    <div class="col-md-4">{!! Form::text('telephone_office', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('telephone_home', 'Home Tel no.') !!}</div>

                    <div class="col-md-4">{!! Form::text('telephone_home', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('residential_address', 'Residential Address') !!}</div>

                    <div class="col-md-4">{!! Form::text('residential_address', null, ['class'=>'form-control']) !!}</div>
                </div>
            </div>

            <div class="detail-group" ng-show="new_client == 1 || new_client == 2">
                <h4 class="form-separator col-md-12">Employment Details</h4>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('employment_id', 'Employed') !!}</div>

                    <div class="col-md-4">{!! Form::select('employment_id', $employment_types, null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('employment_other', 'Employment Other') !!}</div>

                    <div class="col-md-4">{!! Form::text('employment_other', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('present_occupation', 'Present occupation') !!}</div>

                    <div class="col-md-4">{!! Form::text('present_occupation', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('business_sector', 'Business sector') !!}</div>

                    <div class="col-md-4">{!! Form::text('business_sector', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('employer_name', 'Employer name') !!}</div>

                    <div class="col-md-4">{!! Form::text('employer_name', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('employer_address', 'Employer address') !!}</div>

                    <div class="col-md-4">{!! Form::text('employer_address', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('method_of_contact_id', 'Preferred Method of contact') !!}</div>

                    <div class="col-md-4">{!! Form::select('method_of_contact_id', $contact_methods, null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'method_of_contact_id') !!}</div>
                </div>
            </div>



            @include('investment.partials.applicationformdetails')

            <div class="detail-group">
                <div class="col-md-12 margin-top-20">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop
