@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <h3>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</h3>
            <hr/>
            <div class="col-md-6">
                @include('investment.partials.investmentdetail')
            </div>
            <div class="hide">
                {!! Form::text('investment_bal',$investment->repo->getTotalValueOfAnInvestment(), ['init-model'=>'investment_bal'] ) !!}
                {!! Form::text('client_name', \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) , ['init-model'=>'client_name']) !!}
                {!! Form::text('client_code', $investment->client->client_code , ['init-model'=>'client_code']) !!}
            </div>
            <div class="col-md-6">
                <div class="detail-group">
                    <h4>Client Bank Account</h4>

                    @if(is_null($investment->bank_account_id))
                        <h5>Default Account</h5>

                        <table class="table table-responsive table-striped">
                            <thead>
                            <tr><th>Account Name</th><th>Account Number</th><th>Bank</th><th>Branch</th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{!! $investment->client->investor_account_name !!}</td>
                                    <td>{!! $investment->client->investor_account_number !!}</td>
                                    <td>{!! $investment->client->investor_bank !!}</td>
                                    <td>{!! $investment->client->investor_bank_branch !!}</td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <?php
                            $acc = $investment->bankAccount;
                        ?>

                        <table class="table table-responsive table-striped">
                            <thead>
                            <tr><th>Account Name</th><th>Account Number</th><th>Bank</th><th>Branch</th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{!! $acc->account_name !!}</td>
                                    <td>{!! $acc->account_number !!}</td>
                                    <td>{!! @$acc->bank->name !!}</td>
                                    <td>{!! @$acc->branch->name !!}</td>
                                </tr>
                            </tbody>
                        </table>
                    @endif

                    <?php
                        $acc_arr = $investment->client->bankAccounts;
                        $accs = [];

                    foreach ($acc_arr as $a) {
                        $name = 'Name: '.$a->account_name.' A/C Number: '.$a->account_number.' Bank: '.$a->bank->name.' Branch: '.$a->branch->name;
                        $accs = array_add($accs, $a->id, $name);
                    }

                        $accs = array_add($accs, 0, 'Client\'s Default Account Indicated In Application');
                    ?>

                    {!! Form::open(['route'=>['associate_bank_account_path', $investment->id]]) !!}

                    <div class="form-group">
                        {!! Form::label('Select an account to bind to investment') !!}

                        {!! Form::select('bank_acc', $accs, null, ['class'=>'form-control']) !!}
                    </div>

                    {!! Form::submit('Set as associated account', ['class'=>'btn btn-success']) !!}

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection