@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach(Product::all() as $product)
                    <li role="presentation" class=""><a href="#{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach(Product::all() as $product)
                    <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">

                        <div class="margin-top-20" ng-controller="DatepickerCtrl">
                            {!! Form::open() !!}
                            <div class="col-md-5">
                                {!! Form::text('start', $start, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                            </div>

                            <div class="col-md-5">
                                {!! Form::text('end', $end, ['class'=>'form-control', 'datepicker-popup init-model'=>"dateSecond", 'is-open'=>"statusSecond.opened", 'ng-focus'=>'openSecond($event)']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::submit('Filter', ['class'=>'btn btn-success btn-block no-margin-top']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>

                        <table class="table table-hover table-responsive table-striped margin-top-20">
                            <thead>
                                <tr><th>Date</th><th>Custody Fees</th><th>FA Commission</th></tr>
                            </thead>
                            <?php
                                $repo = new \Cytonn\Investment\CMSSummariesRepository();
                                $total_commission = 0;
                                $total_custodial = 0;
                            ?>
                            <tbody>
                                @foreach($dates as $date)
                                    <?php
                                        $commission = $repo->getTotalCommissionForProductForMonth($product, $date);
                                        $total_commission += $commission;

                                        $custodial = $repo->getTotalCustodyFeesForProductForMonth($product, $date);
                                        $total_custodial += $custodial;
                                    ?>
                                    <tr>
                                        <td>{!! $date->format('M Y') !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($commission) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($custodial) !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Totals</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total_commission) !!}</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total_custodial) !!}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection