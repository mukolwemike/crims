@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div>
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <?php $i = 1 ?>
                @foreach($products = Product::all() as $product)
                    <li role="presentation"><a href="#{!! $product->id !!}" id="{!!$product->id!!}-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach($products as $product)

                    <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">
                        <div ng-controller="ClientOngoingTaxController">
                            <div style="display: none !important;">
                                {!! Form::text($product->id, $product->id, ['init-model'=>'product_id']) !!}
                            </div>
                            <table st-pipe="callServer" st-table="displayed"  class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th colspan = "1"><a class="btn btn-success margin-bottom-20" href="/dashboard/investments/taxation/records">Tax Due</a></th>
                                    <th colspan="2"></th>
                                    <th >{!! Form::text('date', \Carbon\Carbon::today()->toDateString(), ['class'=>'form-control', 'st-search' =>"date", 'datepicker-popup init-model'=>"date_picked", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</th>
                                    <th colspan="2"><input st-search="client_code" placeholder="search by client code" class="form-control" type="text"/></th>
                                    <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                                </tr>
                                <tr><td colspan="8"><div class="alert alert-info"><p>Displaying the tax for the month within selected date</p></div></td></tr>
                                <tr>
                                    <th st-sort="client_code">Client Code</th>
                                    <th>Client Name</th>
                                    <th>PIN</th>
                                    <th>Amount</th>
                                    <th>Interest Rate</th>
                                    <th>Gross Interest</th>
                                    <th>Withholding Tax</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody  ng-show="!isLoading">
                                    <tr ng-repeat="row in displayed">
                                        <td><% row.client_code %></td>
                                        <td> <%row.name %></td>
                                        <td><% row.pin %></td>
                                        <td><% row.amount | currency:""  %></td>
                                        <td><% row.interest_rate | currency:"" %>%</td>
                                        <td><% row.gross_interest | currency:"" %></td>
                                        <td><% row.tax | currency:"" %></td>
                                        <td>
                                            <a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/taxation/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                                            <a ng-show="row.status == 'not invested'" ng-controller = "PopoverCtrl" uib-popover = "Edit details" popover-trigger = "mouseenter" href = "#/dashboard/investments/applications/edit/<%row.id%>"><i class = "fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody ng-show="isLoading">
                                    <tr>
                                        <td colspan=8 class="text-center">Loading ... </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td  colspan="2" class="text-center">
                                        Items per page
                                    </td>
                                    <td colspan="2" class="text-center">
                                        <input type="text"  ng-model="itemsByPage"/>
                                    </td>
                                    <td colspan="4" class="text-center">
                                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop