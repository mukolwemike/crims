@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">

        <div class="col-md-6">
            <a class="btn btn-success margin-top-25" href="/dashboard/investments/clientinvestments/maturity/analysis">Maturity Analysis</a>
            <a class="btn btn-success margin-top-25" href="#bulk-rollover-maturities" data-toggle="modal" role="button">Bulk Rollover Maturities</a>
            {{--<a class="btn btn-success margin-top-25" href="/dashboard/investments/clientinvestments/maturity/bulk_rollover">Bulk Rollover Maturities</a>--}}
        </div>
        <div class="col-md-4 pull-right" ng-controller = "DatepickerCtrl" >
            {!! Form::open() !!}
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('date', 'Enter Maturity Date') !!}

                    {!! Form::text('date', $date, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::submit('Submit', ['class'=>'btn btn-success margin-top-25']) !!}
            </div>

            <div class="col-md-3">
                {!! Form::close() !!}

                {!! Form::open(['route'=>'send_maturity_notifications_route']) !!}
                <div class="hide">
                    {!! Form::text('date', $date) !!}
                </div>
                {!! Form::submit('Send', ['class'=>'btn btn-warning margin-top-25']) !!}
            </div>
        </div>
        <div class="clearfix"></div>

        <table class="table table-responsive table-hover table-striped">
            <thead>
                <tr>
                    <th>Client code</th><th>Client Name</th><th>Amount Invested</th><th>Invested Date</th><th>Maturity Date</th>
                    <th>Interest Rate</th><th>Value</th><th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($investments as $investment)
                    <tr>
                        <td>{!! $investment->client->client_code !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getFinalTotalValueOfInvestment()) !!}</td>
                        <td><a href="/dashboard/investments/clientinvestments/maturity/{!! $investment->id !!}"><i class="fa fa-list-alt"></i></a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {!! $investments->links() !!}
    </div>
@stop


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send Maturity Notifications</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">
                    <p>Send notifications for investments maturing on {!! \Carbon\Carbon::today()->addWeek()->toDateString() !!}</p>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::open() !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-warning"></i> Send Notifications</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bulk-rollover-maturities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['bulk_rollover_maturities'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Bulk Rollover Maturities</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Select the maturity date for the investments to be bulk rolled over</p>
                    </div>

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Maturity Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>