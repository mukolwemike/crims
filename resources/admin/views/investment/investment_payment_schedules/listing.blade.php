@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="form-detail detail-group">
            <h3>Investment Payment Schedules

                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-success margin-bottom-20" data-toggle="modal" data-target="#payment-reminder-modal">
                        <i class="fa fa-envelope-o"></i> Payment Reminders</button>
                    <a class="btn btn-default" href="/dashboard/investments/clientinvestments">
                        <i class="fa fa-long-arrow-left"></i> Back
                    </a>
                </div>

            </h3>

            <all-investment-payment-schedules></all-investment-payment-schedules>
        </div>
    </div>

    <div class="modal fade" id="payment-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['investment-payment-schedule-details.send-reminders']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Send Payment Reminders for Bulk Approval</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'End Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Send for Approval', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
