@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @if($investment)
                    @include('investment.partials.investmentdetail', ['inv_title'=>'Parent Investment Details'])
                @endif
            </div>
            <div class="col-md-6">
                <div>
                    <div class="pull-right">
                        <a href="/dashboard/investments/investment_payment_schedules/{!! $investment->id !!}" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Schedules
                        </a>
                    </div>
                </div>
                <br>
                <hr>
                <div class="detail-group">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Topup Details</h4>
                        </div>
                        <div class="col-md-12">
                            <table class = "table table-hover">
                                <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id) !!}
                                        <a href="/dashboard/clients/details/{{ $investment->client_id }}" target="_blank"> <i class="fa fa-long-arrow-right"></i>Details</a></td>
                                </tr>
                                <tr>
                                    <td>Amount</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($paymentSchedule->amount) !!}</td>
                                </tr>
                                <tr>
                                    <td>Agreed Rate</td>
                                    <td>{!! $paymentSchedule->interest_rate !!}%</td>
                                </tr>
                                <tr>
                                    <td>Product</td>
                                    <td>{!! $investment->product->name !!}</td>
                                </tr>
                                <tr>
                                    <td>Invested Date</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($paymentSchedule->date) !!}</td>
                                </tr>
                                <tr>
                                    <td>Maturity Date</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                </tr>
                                <tr>
                                    <td>Franchise</td>
                                    <td>{!! Cytonn\Presenters\BooleanPresenter::presentIcon($investment->client->franchise) !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="btn-group">
                                @if($paymentSchedule->investment)
                                    <div class="alert alert-warning">
                                        <p>The payment schedule is already processed.</p>
                                    </div>

                                    <a href="/dashboard/investments/clientinvestments/{!! $paymentSchedule->investment->id !!}" class="btn btn-default">
                                        View Investment </a>
                                @else
                                     <a class="btn btn-success" href="#" data-toggle="modal" data-target="#confirm-topup">Process</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="modal fade" id="confirm-topup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['process_investment_payment_schedule', $paymentSchedule->id], 'method'=>'POST']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Process Payment Schedule</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('interest_rate', 'Interest Rate') !!}
                        {!! Form::text('interest_rate', $paymentSchedule->interest_rate, ['class'=>'form-control', 'required']) !!}
                    </div>
                    <div class="col-md-12" >
                        {!! Form::label('amount', 'Amount') !!}
                        {!! Form::text('amount', $paymentSchedule->amount, ['class'=>'form-control', 'required']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('invested_date', 'Date (Optional)') !!}
                        {!! Form::text('invested_date', '', ['class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::hidden("payment_schedule_id", $paymentSchedule->id) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
                {!! Form::submit('Yes, Continue', ['class'=>'btn btn-success pull-left']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>