@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="row">
            <div class="col-md-6">
                <div class="form-detail detail-group">
                    <h4>Payment Schedule Details</h4>

                    <label for="">Contribution Amount</label> {!! \Cytonn\Presenters\AmountPresenter::currency($paymentScheduleDetail->amount) !!}<br/>
                    <label for="">Contribution Interval</label> Every {!! $paymentScheduleDetail->payment_interval !!}  Month(s)<br/>
                    <label for="">Contribution Date</label> On day {!! $paymentScheduleDetail->payment_date !!}<br/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-detail detail-group">
                    <h4>Actions</h4>
                    @if($investment->withdrawn != 1 || $investment->rolled != 1)
                        <a href="#update-investment-payment-schedule-detail" class="btn btn-info margin-bottom-10"
                           data-toggle="modal" role="button">Edit Schedule Details</a>
                        <a href="#extend-investment-payment-plan" class="btn btn-info margin-bottom-10"
                           data-toggle="modal" role="button">Extend Investment Plan</a>

                        <a href="#terminate-investment-payment-plan" class="btn btn-info margin-bottom-10"
                        data-toggle="modal" role="button">Send Contract Termination</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-detail detail-group">
            <h4>Payment Schedules</h4>

            <investment-payment-schedules :investment-id="{!! $investment->id !!}"></investment-payment-schedules>
        </div>
    </div>

    {{--start investment export modals--}}
    <div class="modal fade" id="update-investment-payment-schedule-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['investment-payment-schedule-details.update', $paymentScheduleDetail->id]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Update Investment Payment Schedule Detail</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">

                        <div class="col-md-12">
                            {!! Form::label('amount', 'Contribution Amount') !!}
                            {!! Form::text('amount', $paymentScheduleDetail->amount, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('payment_interval', 'Contribution Interval') !!}
                            {!! Form::select('payment_interval', $paymentIntervals, $paymentScheduleDetail->payment_interval, ['class'=>'form-control']) !!}
                        </div>

                        <div class="col-md-12">
                            {!! Form::label('payment_date', 'Preferred Payment Date (1 - 31)') !!}
                            {!! Form::number('payment_date', $paymentScheduleDetail->payment_date, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of investment export modal--}}

    {{--start investment export modals--}}
    <div class="modal fade" id="extend-investment-payment-plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['investment-payment-schedule-details.extend-plan', $investment->id]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Extend Investment Payment Plan</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            <label for="">Current Maturity Date</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}<br/>
                        </div>


                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('maturity_date', 'New Maturity Date') !!}
                            {!! Form::text('maturity_date', Carbon\Carbon::parse($investment->maturity_date)->toDateString(), ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::hidden('investment_id', $investment->id) !!}
                    {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of investment export modal--}}

    {{--start investment export modals--}}
    <div class="modal fade" id="terminate-investment-payment-plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['investment-payment-schedule-details.send-contract-termination', $investment->id]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Send Contract Termination Notice</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-md-12" class="form-group">
                        <p>Are you sure you would like to send the contract termination notice to the client for approval?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::hidden('investment_id', $investment->id) !!}
                    {!! Form::submit('Send for Approval', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of investment export modal--}}
@endsection
