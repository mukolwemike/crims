@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard">
            <div class="row">
                <div class="col-md-6">
                    @include('investment.partials.investmentdetail', ['investment' => $schedule->investment])
                </div>

                <div class="col-md-6">
                    <div class="detail-group">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <td>Investment ID</td><td>{{ $schedule->investment->id }}</td>
                                </tr>
                                <tr>
                                    <td>Approval</td><td><a href="/dashboard/investments/approve/{{ $schedule->id }}">here</a></td>
                                </tr>
                                <tr>
                                    <td>Scheduled On</td><td> {{ \Cytonn\Presenters\DatePresenter::formatDate($schedule->approved_on) }}</td>
                                </tr>
                                <tr>
                                    <td>Action Date</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($schedule->action_date) }}</td>
                                </tr>
                                <tr>
                                    <td>Actioned On</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($schedule->run_date) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @if($schedule->transaction_type == 'withdrawal')
                        <h4>Withdrawal Schedule Details</h4>
                        @include('investment.partials.withdrawalscheduledetails', ['investment'=>$schedule->investment])

                    @elseif($schedule->transaction_type == 'rollover' || $schedule->transaction_type == 'rollover_investment')
                        <h4>Rollover Schedule Details</h4>
                        @include('investment.partials.rolloverscheduledetails', ['investment'=>$schedule->investment])
                    @else
                        <div class="alert alert-danger">
                            <p>Transaction not supported</p>
                        </div>
                    @endif
                </div>
            </div>


        </div>
    </div>
@stop