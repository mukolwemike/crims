@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "InterestPaymentGridCtrl">
            <h3 class="print-only">Interest Payments Schedule</h3>
            <div class="btn-group pull-right no-print">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#send_bulk_interest_payments"><i class="fa fa-check-square-o"></i> Bulk Payments</button>
                <a ng-click="download()" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Export to Excel</a>
                <a class="btn btn-success" ng-click="print()"><i class="fa fa-print"></i> Print</a>
            </div>
            <div class="modal fade" id="send_bulk_interest_payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {!! Form::open(['route'=>['send_bulk_interest_payments']]) !!}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Send Interest Payments for Bulk Approval</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                {!! Form::label('date', 'Select date for which to send interest payments for approval') !!}
                                {!! Form::text('date', null, ['class'=>'form-control margin-top-20', 'init-model'=>'date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!! Form::submit('Send for Approval', ['class'=>'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr class="no-print">
                    <th colspan = "5">Showing available interest on <b><% meta.interest_date | date %></b></th>
                    <th>{!! Form::select('search', $tenors, null, ['st-search'=>'tenor', 'class'=>'form-control']) !!}</th>
                    <th  class="no-print" colspan="2"><st-date-range predicate="date" before="query.before" after="query.after"></st-date-range></th>
                    <th  class="no-print" colspan = "2"><input st-search ="" class = "form-control" placeholder = "Search..." type = "text"/></th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th>Principal</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Value Date</th>
                    <th>Previous Date</th>
                    <th>Next Date</th>
                    <th>Available</th>
                    <th class="no-print">Action</th>
                </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed" class="no-page-break">
                        <td><% row.client_code %></td>
                        <td><% row.name %></td>
                        <td><% row.principal | currency:"" %></td>
                        <td><% row.description%></td>
                        <td><% row.type %></td>
                        <td><% row.value_date | date %></td>
                        <td><% row.last_payment_date | date %></td>
                        <td><% row.date | date %></td>
                        <td><% row.available | currency:"" %></td>
                        <td class="no-print">
                            <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/investments/pay/<% row.investment_id %>"><i class = "fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tfoot class="no-print">
                    <tr>
                        <td colspan = "100%" class = "text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>

                    </tr>
                </tfoot>
            </table>

        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop