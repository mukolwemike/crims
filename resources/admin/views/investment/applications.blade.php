@extends('layouts.default')

@section('content')
      <div class = "col-md-12">
            <div class = "panel-dashboard">
                  <ul id="myTabs" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class=""><a href="#investments" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Investments</a></li>
                        <li role="presentation" class=""><a href="#unitization" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Unitization</a></li>
                  </ul>
                  <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade" id="investments" aria-labelledby="home-tab">
                              @include('investment.applications.investment_index')
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="unitization" aria-labelledby="home-tab">
                              <unit-fund-applications></unit-fund-applications>
                        </div>
                  </div>
            </div>
      </div>
@endsection