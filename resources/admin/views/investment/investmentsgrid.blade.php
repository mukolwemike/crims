@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "InvestmentsGridController">
            <div class="btn-group" role="group" aria-label="...">
                {{--<a class="btn btn-success" href="/dashboard/investments/interest/schedule"><i class="fa fa-calendar-o"></i> Interest Schedule</a>--}}
                <a href="/dashboard/investments/clientinvestments/maturity" class="btn btn-success">Maturities</a>
                <a href="/dashboard/investments/interest" class="btn btn-success">Interest Payments</a>
                <a href="/dashboard/investments/clientinvestments/deductions" class="btn btn-success">Deductions</a>
                <a href="/dashboard/investments/clientinvestments/withdrawals" class="btn btn-success">Withdrawals</a>
                <a href="/dashboard/investments/clientinvestments/scheduled" class="btn btn-success">Scheduled</a>
                <a href="/dashboard/investments/clientinvestments/investment-payment-schedules/listing" class="btn btn-success">Payment Schedules</a>
            </div>
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan = "10"></th>
                    <th colspan="2"><input st-search="client_code" placeholder="search by client code" class="form-control" type="text"/></th>
                    <th colspan = "3"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th st-sort = "invested_date">Investment Date</th>
                    <th st-sort = "maturity_date">Maturity Date</th>
                    <th st-sort = "amount">Amount</th>
                    <th st-sort = "interest_rate">Interest rate</th>
                    <th>Gross Interest</th>
                    <th>Withholding Tax</th>
                    <th>Net Interest</th>
                    <th>Withdrawal</th>
                    <th>Total</th>
                    <th>Product</th>
                    <th>Currency</th>
                    <th st-sort = "withdrawal_date">Withdrawal Date</th>
                    <th st-sort = "withdrawn">Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                    <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td><% row.client_code %></td>
                            <td><% row.fullName %></td>
                            <td><% row.invested_date | date%></td>
                            <td><% row.maturity_date | date%></td>
                            <td><% row.amount | currency:"" %></td>
                            <td><% row.interest_rate %>%</td>
                            <td><% row.gross_interest | currency:"" %></td>
                            <td><% row.withholding_tax  | currency:""%></td>
                            <td><% row.net_interest | currency:"" %></td>
                            <td><% row.withdrawal | currency:"" %></td>
                            <td><% row.value | currency:"" %></td>
                            <td><% row.product %></td>
                            <td><% row.currency %></td>
                            <td><% row.withdrawal_date | date %></td>
                            <td><span to-html = "row | investmentStatus"></span></td>
                            <td>
                                <a ng-controller = "PopoverCtrl" uib-popover = "Details" popover-trigger = "mouseenter" href = "/dashboard/investments/clientinvestments/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr><td colspan="100%">Loading...</td></tr>
                    </tbody>
                <tfoot>
                    <tr>
                        <td colspan = "100%" dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop