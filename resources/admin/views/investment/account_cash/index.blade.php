@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="AccountsCashGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">

                <thead>
                    <tr>
                        <th colspan="4">
                            <div class="btn-group" role="group" aria-label="...">
                                <a class="btn btn-primary" href="/dashboard/investments/accounts-cash/create"><i class="fa fa-plus-square-o"></i> Add Inflow</a>
                                <a class="btn btn-success" href="/dashboard/investments/accounts-cash/daily-inflows">Export Daily Inflows</a>
                            </div>
                        </th>
                    </tr>
                    <tr >
                        <th  class="no-print" colspan="2"><st-date-range predicate="date" before="query.before" after="query.after"></st-date-range></th>
                        <th>
                            <select st-search="has_client" class="form-control" init-model="category">
                                <option value="">All</option>
                                <option value="1">With Client</option>
                                <option value="0">Without Client</option>
                            </select>
                        </th>
                        <th>
                            <select st-search="project" class="form-control">
                                <option value="">All Projects</option>
                                @foreach($projects as $project)
                                    <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="product" class="form-control">
                                <option value="">All Products</option>
                                @foreach($products as $product)
                                    <option value="{!! $product->id !!}">{!! $product->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="transferred" class="form-control">
                                <option value="">All Status</option>
                                <option value="1">Transferred</option>
                                <option value="0">UnTransferred</option>
                            </select>
                        </th>
                        <th>
                            <select st-search="shareEntity" class="form-control">
                                <option value="">All Share Entities</option>
                                @foreach($shareEntities as $shareEntity)
                                    <option value="{!! $shareEntity->id !!}">{!! $shareEntity->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="account" class="form-control">
                                <option value="">All Accounts</option>
                                @foreach($custodialaccount as $account)
                                    <option value="{!! $account->id !!}">{!! $account->account_name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                    </tr>
                    <tr>
                        <th st-sort="client_code">Client Code</th>
                        <th>Client Name</th>
                        <th>Account</th>
                        <th>Reference No.</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>FA</th>
                        <th>Transferred</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.client_code %></td>
                        <td><% row.full_name %></td>
                        <td><% row.account %></td>
                        <td><% row.ref_no %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><% row.description %></td>
                        <td><% row.date %></td>
                        <td><% row.fa %></td>
                        <td><% row.transferred %></td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="View Details" popover-trigger="mouseenter" href="/dashboard/investments/accounts-cash/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">Start: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">End: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop