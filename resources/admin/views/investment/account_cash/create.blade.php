@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard" ng-controller="AddAccountInflowController">
            <h3>Add Inflow to Account</h3>
            <br/>

            {!! Form::model($transaction, ['route'=>['add_cash_to_account', $transaction->id], 'files' => true]) !!}

            <div class="panel panel-default">
                <div class="panel-heading">Destination</div>

                <div class="panel-body">
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <p>Inflow to</p>
                        </div>
                        <div class="col-md-3">
                            {!! Form::radio('category', 'funds', true, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Unit Funds') !!}
                        </div>


                        <div class="col-md-3">
                            {!! Form::radio('category', 'shares', null, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Shares') !!}
                        </div>

                        <div class="col-md-3">
                            {!! Form::radio('category',  'project', null, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Project') !!}
                        </div>

                        <div class="col-md-3">
                            {!! Form::radio('category', 'product', true, ['init-model'=>'category']) !!}
                            {!! Form::label('category', 'Product') !!}
                        </div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'category') !!}<br/>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" ng-show="category == 'project'">
                            {!! Form::label('project_id', 'Projects') !!}
                            {!! Form::select('project_id', $projects, null, ['class'=>'form-control', 'init-model'=>'project_id', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                        </div>

                        <div class="form-group" ng-show="category == 'product'">
                            {!! Form::label('product_id', 'Products') !!}
                            {!! Form::select('product_id', $products, null, ['class'=>'form-control', 'init-model'=>'product_id', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                        </div>

                        <div class="form-group" ng-show="category == 'shares'">
                            {!! Form::label('entity_id', 'Share Entities') !!}
                            {!! Form::select('entity_id', $shareentities, null, ['class'=>'form-control', 'init-model'=>'entity_id', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'entity_id') !!}
                        </div>

                        <div class="form-group" ng-show="category == 'funds'">
                            {!! Form::label('unit_fund_id', 'Unit Funds') !!}
                            {!! Form::select('unit_fund_id', $funds, null, ['class'=>'form-control', 'init-model'=>'unit_fund_id', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'unit_fund_id') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('custodial_account_id', 'Bank Account') !!}

                            <select name="custodial_account_id" class="form-control">
                                <option ng-repeat="acc in custodial_accounts" value="<% acc.id %>"><% acc.account_name %></option>
                            </select>
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('exchange_rate', 'Exchange Rate (Account Currency to Product Currency)') !!}

                        {!! Form::text('exchange_rate', 1, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    Source Details
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        {!! Form::label('source', 'Source') !!}

                        {!! Form::select('source', ['deposit' => 'Direct deposit/transfer', 'cheque' => 'Cheque', 'mpesa' => 'Mpesa'], null, ['class'=>'form-control', 'init-model'=>'source']) !!}
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('bank_reference_no', 'Bank Reference No.') !!}
                            {!! Form::text('bank_reference_no', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_reference_no') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('amount', 'Amount') !!}
                            {!! Form::number('amount', null, ['class'=>'form-control', 'ng-required'=>'true', 'step'=>'0.01']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('description', 'Description') !!}
                            {!! Form::text('description', null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="source =='cheque'">
                        <div class = "form-group"  ng-controller = "DatepickerCtrl">
                            {!! Form::label('received_date', 'Date Received') !!}

                            {!! Form::text('received_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"received_date", 'is-open'=>"status.received_date", 'ng-focus'=>'status.received_date = !status.received_date', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'entry_date') !!}
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="source =='cheque'">
                        <div class = "form-group"  ng-controller = "DatepickerCtrl">
                            {!! Form::label('entry_date', 'Entry Date') !!}

                            {!! Form::text('entry_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"entry_date", 'is-open'=>"status.entry_date", 'ng-focus'=>'status.entry_date = !status.entry_date', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'entry_date') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class = "form-group"  ng-controller = "DatepickerCtrl">
                            {!! Form::label('date', 'Value Date') !!}

                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="source == 'cheque'">
                        {!! Form::label('cheque_number', 'Cheque number') !!}

                        {!! Form::text('cheque_number', null, ['class'=>'form-control']) !!}
                    </div>
                    <div ng-if="source == 'cheque'" class="clearfix"></div>
                    <div class="col-md-6" ng-if="source =='cheque'">
                        <div class = "form-group">
                            {!! Form::label('cheque_file', 'Upload scan of cheque') !!}

                            {!! Form::file('cheque_file', ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'cheque_file') !!}
                        </div>
                    </div>

                    <div class="col-md-6" ng-show="source == 'mpesa'">
                        {!! Form::label('mpesa_confirmation_code', 'Mpesa confirmation code') !!}

                        {!! form::text('mpesa_confirmation_code', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Client Details
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-6">
                                {!! Form::radio('new_client', false, null, ['init-model'=>'new_client']) !!}
                                {!! Form::label('new_client', 'Existing Client') !!}
                            </div>

                            <div class="col-md-4 clearfix">
                                {!! Form::radio('new_client', true, true, ['init-model'=>'new_client']) !!}
                                {!! Form::label('new_client', 'New Client') !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'new_client') !!}<br/>
                            </div>
                        </div>

                        {{--<div class="row" ng-controller="ClientUserCtrl" ng-show="new_client == false">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>Search Client by Name</label>--}}
                                    {{--<input type="text" ng-model="selectedClient" placeholder="Enter the Client name" typeahead="client as client.combinedName for client in searchClients($viewValue)" class="form-control"--}}
                                           {{--typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">--}}
                                    {{--<i ng-show="loading" class="fa fa-refresh fa-spin"></i>--}}
                                    {{--<div ng-show="noResults">--}}
                                        {{--<i class="glyphicon glyphicon-remove"></i> No Results Found--}}
                                    {{--</div>--}}

                                    {{--<div class="hide">--}}
                                        {{--<input name="client_id" type="text" ng-model="selectedClient.id" ng-required='true'>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="row" ng-show="new_client == false">
                            <div class="col-md-12">
                                <search-client name="client_id"></search-client>
                            </div>
                        </div>

                        <div class="row" ng-if="new_client == true">
                            <div class="col-md-12 form-group">
                                {!! Form::label('received_from', 'Received From') !!}
                                {!! Form::text('received_from', null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group clearfix">
                            <div class="col-md-6">
                                {!! Form::radio('recipient_known', true, true, ['init-model'=>'recipient_known']) !!}
                                {!! Form::label('recipient_known', 'FA Known') !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::radio('recipient_known', false, null, ['init-model'=>'recipient_known']) !!}
                                {!! Form::label('recipient_known', 'FA not known') !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'recipient_known') !!}<br/>
                            </div>
                        </div>
                        <div class="row" ng-show="recipient_known == true">
                            <div class="col-md-12">
                                <search-financial-advisor name="commission_recipient_id"></search-financial-advisor>
                            </div>
                        </div>
                        {{--<div class="row" ng-controller="ClientUserCtrl" ng-if="recipient_known == true">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>Search FA by Name</label>--}}
                                    {{--<input type="text" ng-model="selectedFA" placeholder="Enter the FA name" typeahead="fa as fa.fullname for fa in searchFas($viewValue)" class="form-control"--}}
                                           {{--typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">--}}
                                    {{--<i ng-show="loading" class="fa fa-refresh fa-spin"></i>--}}
                                    {{--<div ng-show="noResults">--}}
                                        {{--<i class="glyphicon glyphicon-remove"></i> No Results Found--}}
                                    {{--</div>--}}

                                    {{--<div class="hide">--}}
                                        {{--<input name="commission_recipient_id" type="text" ng-model="selectedFA.id" ng-required='true'>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::submit('Save Inflow', ['class'=>'btn btn-success']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection