@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Account Cash Details</h3>
            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Client Code</td>
                    @if($client->id)
                        <td>{!! $client->client_code !!}</td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <td>Client</td>
                    @if($client->id)
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    @else
                        <td>{!! $transaction->received_from !!}
                            <small>(Not on-boarded yet)</small>
                        </td>
                    @endif
                </tr>
                @if($payment)
                    <tr>
                        <td>Category</td>
                        @if($payment->product)
                            <td>Product : {!! $payment->product->name !!}</td>
                        @endif
                        @if($payment->project)
                            <td>Project : {!! $payment->project->name !!}</td>
                        @endif
                    </tr>
                    @if(isset($payment->product->custodialAccount))
                        <tr>
                            <td>Account</td>
                            <td>{!! $payment->product->custodialAccount->account_name !!}</td>
                        </tr>
                    @endif
                @endif
                <tr>
                    <td>Bank Reference No.</td>
                    <td>{!! $transaction->bank_reference_no !!}</td>
                </tr>
                <tr>
                    <td>Bank Transaction ID (unique id)</td>
                    <td> {{ $transaction->bank_transaction_id }}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{!! $transaction->transactionType->name !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>Now: {!! \Cytonn\Presenters\AmountPresenter::currency($transaction->amount) !!}
                        @if($transaction->value > 0 && $transaction->amount == 0)
                            After Value: {{ \Cytonn\Presenters\AmountPresenter::currency($transaction->value) }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{!! $transaction->description !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
                </tr>
                <tr>
                    <td>FA</td>
                    @if($recipient->id)
                        <td>{!! $recipient->name !!}</td>
                    @else
                        <td>No FA specified</td>
                    @endif
                </tr>
                </tbody>
            </table>


            <div class="btn-group">
                @if(!$transaction->client)
                    <button class="btn btn-default" ng-click="link = !link"><i class="fa fa-link"></i> Link to Client &
                        FA
                    </button>
                @endif
                <button class="btn btn-danger" data-toggle="modal" data-target="#reverseModal"><i
                            class="fa fa-remove"></i> Reverse
                </button>
                @if($transaction->repo->isCheque())
                    @if(!$transaction->repo->hasBeenValued())
                        <button class="btn btn-info" data-toggle="modal" data-target="#valueChequeModal"><i
                                    class="fa fa-check"></i> Value Cheque
                        </button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#bounceChequeModal"><i
                                    class="fa fa-angle-up"></i> Bounce Cheque
                        </button>
                    @endif
                @endif
                <a href="/dashboard/investments/accounts-cash/show/transfer-mismatch/{{$transaction->id}}"
                   class="btn btn-default">
                    <i class="fa fa-exchange"></i>&nbsp;Transfer
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-10 col-md-offset-1" ng-show="link">
        <div class="panel-dashboard">
            <h3>Link cash to Client/FA</h3>

            {!! Form::model($transaction, ['route'=>['link_cash_to_client_and_fa', $transaction->id]]) !!}

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <label>Search Client by Name</label>
                    </div>
                    <div class="col-md-12">
                        <search-client name="client_id"></search-client>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row" ng-controller="ClientUserCtrl">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Search FA by Name</label>
                            <input type="text" ng-model="selectedFA"
                                   ng-init="selectedFA.name = '{!! $recipient->name !!}'"
                                   placeholder="Enter the FA name"
                                   typeahead="fa as fa.name for fa in searchFas($viewValue)"
                                   class="form-control" typeahead-loading="loading" typeahead-show-hint="true"
                                   typeahead-min-length="1" typeahead-no-results="noResults">
                            <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                            <div ng-show="noResults">
                                <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>

                            <div class="hide">
                                <input name="recipient_id" type="text" ng-model="selectedFA.id"
                                       ng-init="selectedFA.id = {!! $recipient->id !!}">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-12">
                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
    </div>


@endsection


<!-- Modal -->
<div class="modal fade" id="reverseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['reverse_cash_inflow', $transaction->id], 'method'=>'delete']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Reverse Inflow</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Name: @if($client->id) {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}
                        @else
                            {!! $transaction->received_from !!}
                            <small>(Not on-boarded yet)</small>
                        @endif</p>
                    <p>Amount: {!! \Cytonn\Presenters\AmountPresenter::currency($transaction->amount) !!}</p>
                    <p>Date: {!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</p>
                </div>
                <div class="form-group">
                    {!! Form::label('reason', 'Why do you need to reverse?') !!}
                    {!! Form::text('reason', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Reverse Inflow', ['class'=>'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="bounceChequeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['bounce_client_cheque_inflow', $transaction->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Bounce Cheque</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" ng-controller="DatepickerCtrl">
                    {!! Form::label('date', 'Enter date the cheque bounced') !!}
                    {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.entry_date", 'ng-focus'=>'status.entry_date = !status.entry_date', 'ng-required'=>'true']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="valueChequeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['value_client_cheque_inflow', $transaction->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Value Cheque</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Are you sure this cheque has been paid?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Yes, Proceed', ['class'=>'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>