<div class="col-md-12">
        <div class="panel-dashboard">
            <ul id="myProducts" class="nav nav-tabs" role="tablist">
                @foreach($products as $key => $product)
                    <li role="presentation" @if ($key == 0) class="active" @endif><a href="#product-{!! $product->id !!}" id="product-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myProductContent" class="tab-content">
                @foreach($products as $key => $product)
                    <div role="tabpanel" @if ($key == 0) class="tab-pane fade active in" @else class="tab-pane fade" @endif   id="product-{!! $product->id !!}" aria-labelledby="product-tab" ng-controller="ClientSummaryController">
                        <div class="margin-top-20">
                            <div class="btn-group" role="group" aria-label="...">
                                <a href="#client-summary-export-{!! $product->id !!}" class="btn btn-success" data-toggle="modal" role="button">Export to excel</a>
                                @include('clients.partials.export-client-summary')

                                <a href="/dashboard/investments/summary/analytics" class="btn btn-success">Analytics</a>
                            </div>
                            <a class="btn btn-success pull-right margin-bottom-20 margin-right-5" href="/dashboard/investments/statements"><i class="fa fa-envelope"></i> Statements</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="hide">{!! Form::text('product_id', $product->id, ['init-model'=>'product_id']) !!}</div>

                        <div>
                            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="7">
                                        <th colspan="1"><input st-search="client_code" class="form-control" placeholder="Partner Code"/></th>
                                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                                    </tr>
                                    <tr>
                                        <th st-sort="client_code">Partner Code</th>
                                        <th>Partner Name</th>
                                        <th>Amount Invested</th>
                                        <th>Gross Interest</th>
                                        <th>Withholding Tax</th>
                                        <th>Net Interest</th>
                                        <th>Withdrawal</th>
                                        <th>Total Amount</th>
                                        <th>Custody Fees</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody  ng-show="!isLoading">
                                    <tr ng-repeat="row in displayed">
                                        <td><% row.client_code %></td>
                                        <td><% row.fullName %></td>
                                        <td><% row.amount | currency:"" %></td>
                                        <td><% row.gross_interest | currency:"" %></td>
                                        <td><% row.withholding_tax | currency:"" %></td>
                                        <td><% row.net_interest | currency:"" %></td>
                                        <td><% row.withdrawal | currency:"" %></td>
                                        <td><% row.total | currency:"" %></td>
                                        <td><% row.custody_fees | currency:"" %></td>
                                        <td>
                                            <a ng-controller="PopoverCtrl" uib-popover="View clients summary" popover-trigger="mouseenter" href="/dashboard/investments/summary/client/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                                            <a ng-controller="PopoverCtrl" uib-popover="View statement" popover-trigger="mouseenter" href="/report/statements/client/<% product_id %>/<% row.id %>"><i class="fa fa-file-pdf-o"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody ng-show="isLoading">
                                    <tr>
                                        <td colspan="10" class="text-center">Loading ... </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan = "2" class = "text-center">
                                            Items per page
                                        </td>
                                        <td colspan = "1" class = "text-center">
                                            <input type = "text" ng-model = "itemsByPage"/>
                                        </td>
                                        <td colspan = "7" class = "text-center">
                                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <table class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="50%">{!! $product->name !!} Total Partners: {!! $product->repo->countClients() !!}</th>
                                        <th colspan="50%">{!! $product->name !!} Active Partners: {!! $product->repo->countActiveClients() !!}</th>
                                    </tr>
                                    {{--<tr>--}}
                                        <th colspan="50%">{!! $fundManager->name !!} Total Partners: {!! $fundManager->repo->countClients() !!}</th>
                                        <th colspan="50%">{!! $fundManager->name !!} Active Partners: {!! $fundManager->repo->countActiveClients() !!}</th>
                                    {{--</tr>--}}
                                </thead>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
</div>
