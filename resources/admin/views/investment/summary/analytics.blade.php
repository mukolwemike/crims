@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="btn-group" role="group" aria-label="...">
            <a href="/dashboard/investments/summary" class="btn btn-success"><i class="fa fa-long-arrow-left"></i> Client Summary</a>
            {{--<a href="/dashboard/investments/summary/expenses" class="btn btn-success">Expenses Report</a>--}}
            <a href="/dashboard/portfolio/investments/maturity/analysis" class="btn btn-success">Maturity Profile</a>
            {{--<a href="/dashboard/investments/summary/history" class="btn btn-success">History</a>--}}
        </div>

        <investment-analytics
                date="{{ $date }}"
                from_date="{{ $from_date }}"
                :currencies="{{$currencies}}"
                :products="{{$products}}"
        >

        </investment-analytics>
    </div>
@endsection