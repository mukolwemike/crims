@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="detail-group">
                <h3>Client Details</h3>
                <table class="table table-responsive table-hover">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <td>Name</td><td>

                                {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}

                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#statement">
                                    Statements
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Client added on</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($client->created_at) !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach($products as $product)
                    @if($client->repo->hasProduct($product))
                        <li role="presentation" class=""><a href="#{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                    @endif
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content">
                @foreach($products as $product)
                    @if($client->repo->hasProduct($product))
                        <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">

                            <br/>
                            <div class="col-md-6">
                                @if(!is_null($last_stmt))
                                    @if($last_stmt->created_at->addMonthNoOverflow()->isPast())
                                        <div class="alert alert-warning">
                                    @else
                                        <div class="alert alert-success">
                                    @endif
                                        <p class="">Statement was last sent on {!! (new \Carbon\Carbon($last_stmt->created_at))->toRfc850String() !!} by {!! \Cytonn\Presenters\UserPresenter::presentFullNamesNoTitle($last_stmt->sent_by) !!}</p>
                                    </div>
                                @else
                                    <div class="alert alert-danger">
                                        <p class="">Statement has never been sent</p>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-8">
                                {!! Form::open(['route'=>['show_statement', $product->id, $client->id]]) !!}

                                <div class="col-md-2">
                                    {!! Form::select('format', ['pdf'=> 'PDF', 'excel'=>'Excel'], null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-xs-3" ng-controller="DatepickerCtrl">
                                    {!! Form::text('from', null, ['class'=>'form-control', 'placeholder'=>'Enter Effective Date', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                </div>
                                <div class="col-xs-3" ng-controller="DatepickerCtrl">
                                    {!! Form::text('date', null, ['class'=>'form-control', 'placeholder'=>'Enter Effective Date', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                </div>
                                <div class="col-xs-2">
                                    {!! Form::select('template', ['compact'=>'Normal', 'expanded'=>'Expanded (monthly)'], null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-xs-2">
                                    {!! Form::submit('Preview Statement', ['class'=>'btn btn-success']) !!}
                                </div>

                                {!! Form::close() !!}
                            </div>

                            <br/>
                            <hr/>

                            <?php
                                $partial_withdraw_id = App\Cytonn\Models\ClientInvestmentType::where('name', 'partial_withdraw')->first()->id;
                                $investments = App\Cytonn\Models\ClientInvestment::where('client_id', $client->id)->where('product_id', $product->id)->where('investment_type_id', '!=', $partial_withdraw_id)->orderBy('maturity_date', 'DESC')->paginate(5);
                            ?>

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr><th>Transaction Date</th><th>Maturity Date</th><th>Description</th><th>Invested Amount</th><th>Rate</th><th>Gross Interest</th><th>Net Interest</th><th>Running Bal</th></tr>
                                </thead>
                                <tbody>
                                <?php
                                    $presenter = new \Cytonn\Presenters\StatementPresenter();
                                    $total_principal = 0;
                                    $total_interest = 0;
                                    $total_value = 0;
                                ?>
                                    @foreach($investments as $investment)
                                        <?php
                                        $last_child = $investment;
                                        $last_inv = $investment;
                                        $value = $investment->amount;
                                        $payment = $investment->repo->getTotalPayments();
                                        ?>


                                        <tr>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                            <td>{!! ucfirst($investment->investmentType->name) !!}</td>
                                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                                            <td>{!! $investment->interest_rate !!}%</td>
                                            <td></td>
                                            <td></td>
                                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                                        </tr>


                                        @if($investment->descendant())
                                            <?php
                                                $presenter = new \Cytonn\Presenters\StatementPresenter();
                                                $data = $presenter->presentDescendants($investment->descendant());
                                                $payment = array_sum($data['interest']) + $investment->repo->getTotalPayments();
                                                $children = $data['children'];
                                                $last_inv = $data['last_inv'];
                                                $last_child = $investment;
                                            ?>

                                            @foreach($children as $child)
                                                <?php
                                                $last_child = $child;
                                                $value = $child->amount + (new \Cytonn\Presenters\StatementPresenter())->getTotalNetInterestForParents($child);
                                                ?>
                                                <tr>
                                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($child->invested_date) !!}</td>
                                                    <td></td>
                                                    <td>Redemption</td>
                                                    <td align="right">({!! \Cytonn\Presenters\AmountPresenter::currency($child->parent()->withdraw_amount) !!})</td>
                                                    <td></td>
                                                    <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($child->parent()->repo->getFinalGrossInterest()) !!}</td>
                                                    <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($child->parent()->repo->getFinalNetInterest() )!!}</td>
                                                    <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($value) !!}</td>
                                                </tr>

                                            @endforeach
                                        @endif
                                        @if($payment !== 0)
                                            <tr>
                                                <td></td><td></td><td>Interest</td><td></td><td></td><td></td>
                                                <td align="right">({!! \Cytonn\Presenters\AmountPresenter::currency($payment) !!})</td>
                                                <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($value - $payment) !!}</td>
                                            </tr>
                                        @endif


                                        <tr>
                                            @if($last_inv->withdrawn)
                                                <td>{!! (new \Carbon\Carbon($last_inv->withdrawal_date))->toFormattedDateString() !!}</td>
                                            @else
                                                <td>{!! \Carbon\Carbon::now()->toFormattedDateString() !!}</td>
                                            @endif
                                            <td></td>
                                            <td>Balance</td>
                                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->amount) !!}</td>
                                            <td>{!! $investment->interest_rate !!}%</td>
                                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->repo->getFinalGrossInterest()) !!}</td>
                                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->repo->getNetInterestForInvestmentAtDate($last_inv->withdrawal_date)) !!}</td>
                                            <?php $final = $last_inv->repo->getNetInterestForInvestmentAtDate($last_inv->withdrawal_date) + $value - $payment ?>
                                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($final) !!}</td>
                                        </tr>
                                        @if($last_inv->withdrawn)
                                            <tr>
                                                <td>{!! (new \Carbon\Carbon($last_inv->withdrawal_date))->toFormattedDateString() !!}</td><td></td>
                                                <td>Redemption</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="right">({!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->withdraw_amount) !!})</td>
                                            </tr>
                                            @if($last_inv->rolled)
                                                <tr>
                                                    <td>{!! (new \Carbon\Carbon($last_inv->withdrawal_date))->toFormattedDateString() !!}</td><td></td>
                                                    <td>Rollover</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td align="right">({!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->repo->getTotalValueOfAnInvestment()) !!})</td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <th align="left">{!! \Carbon\Carbon::now()->toFormattedDateString() !!}</th><td></td>
                                                <td></td>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency(0) !!}</p></th>
                                                <td></td>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency(0) !!}</p></th>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency(0) !!}</p></th>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency(0) !!}</p></th>
                                            </tr>
                                        @else
                                            <tr>
                                                <th align="left">{!! \Carbon\Carbon::now()->toFormattedDateString() !!}</th><td></td>
                                                <td></td>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->amount) !!}</p></th>
                                                <td></td>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency($last_inv->repo->getGrossInterestForInvestmentAtDate($last_inv->withdrawal_date) + $presenter->getTotalGrossInterestForParents($last_inv)) !!}</p></th>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency((new \Cytonn\Presenters\StatementPresenter())->getTotalNetInterestForParents($last_inv) + $last_inv->repo->getNetInterestForInvestmentAtDate($last_inv->withdrawal_date) - $payment) !!}</p></th>
                                                <th align="right"><p class="pull-right">{!! \Cytonn\Presenters\AmountPresenter::currency($final) !!}</p></th>
                                            </tr>
                                        @endif
                                        <tr><td colspan="8" style="height: 0px"></td></tr>
                                        <?php
                                        //if last investment is withdrawn, do not add it to totals
                                        if (!$last_inv->withdrawn) {
                                            $total_principal += $last_inv->amount;
                                            $total_interest += ((new \Cytonn\Presenters\StatementPresenter())->getTotalNetInterestForParents($last_inv) + $last_inv->repo->getNetInterestForInvestmentAtDate($last_inv->withdrawal_date) - $payment);
                                            $total_value += $final;
                                        }
                                        ?>
                                    @endforeach
                                </tbody>
                            </table>

                            {!! $investments->links() !!}

                            <h4>Totals</h4>
                            <div class="alert alert-info">
                                <p>These are totals for the active investments today and does not include investments that are withdrawn</p>
                            </div>
                            <table class="table table-responsive">
                                <thead></thead>
                                <tbody>
                                <tr><th>Total Investments</th><th>Total Custody Fees</th><th>Total Interest</th><th>Total Value of Investments</th></tr>
                                <tr>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($client->repo->getTodayInvestedAmountForProduct($product)) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($client->repo->getCustodyFeesForProduct($product)) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($client->repo->getTodayTotalInterestForProduct($product)) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($client->repo->getTodayTotalInvestmentsValueForProduct($product)) !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection

<!-- Statement Modal -->
<div class="modal fade" id="statement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Statements</h4>
            </div>
            <div class="modal-body">
                @if(count($open_campaigns_arr) > 0)
                    {!! Form::open(['route'=>'add_client_to_statement_campaign']) !!}
                        <div class="form-group">
                            {!! Form::label('campaign', 'Select campaign') !!}

                            {!! Form::select('campaign', $open_campaigns_arr, null, ['class'=>'form-control']) !!}
                        </div>

                        {!! Form::hidden('client', $client->id) !!}

                        {!! Form::submit('Add to campaign', ['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}
                @else
                    <div class="well well-lg">
                        <p>No open campaigns</p>
                    </div>
                @endif

                <div ng-controller="CollapseCtrl">
                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">More options</button>

                    <div uib-collapse="isCollapsed">
                        {!! Form::open(['route'=>['send_individual_statement', $client->id]]) !!}
                        {!! Form::submit('Send Current Statement to client now', ['class'=>'btn btn-success margin-top-20']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>



