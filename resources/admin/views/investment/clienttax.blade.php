@extends('layouts.default')

@section('content')
    <div class = "panel-dashboard">
        <ul id="myTabs" class="nav nav-tabs" role="tablist">
            <?php $i = 1 ?>
            @foreach($products = Product::all() as $product)
                <li role="presentation"><a href="#{!! $product->id !!}" id="{!!$product->id!!}-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
            @endforeach
        </ul>
        <div id="myTabContent" class="tab-content">
            @foreach($products as $product)

                <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab" ng-controller = "ClientTaxDueController">
                    <div>
                        <div style="display: none !important;">
                            {!! Form::text($product->id, $product->id, ['init-model'=>'product_id']) !!}
                        </div>

                        <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">


                            <thead>
                            <tr>
                                <th colspan = "2"></th>
                                <th colspan="2">
                                    <st-date-range predicate="date" before="query.before" after="query.after"></st-date-range>
                                </th>
                                <th colspan = "1"><input st-search = "client_code" placeholder = "search by client code" class = "form-control" type = "text"/></th>
                                <th colspan = "1"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/>
                                </th>
                            </tr>
                            <tr>
                                <th>Client Code</th>
                                <th>Name</th>
                                <th>PIN</th>
                                <th st-sort = "date">Date</th>
                                <th>Invested Amount</th>
                                <th>Gross Interest</th>
                                <th>Withholding Tax</th>
                            </tr>
                            </thead>
                                <tbody  ng-show="!isLoading">
                                    <tr ng-repeat = "row in displayed">
                                        <td><% row.client_code %></td>
                                        <td><% row.client_name %></td>
                                        <td><% row.pin %></td>
                                        <td><% row.date | date%></td>
                                        <td><% row.invested_amount | currency:"" %></td>
                                        <td><% row.gross_interest | currency:"" %></td>
                                        <td><% row.amount  | currency:""%></td>
                                    </tr>
                                </tbody>
                                <tbody ng-show="isLoading">
                                    <tr>
                                        <td colspan="7" class="text-center">Loading ... </td>
                                    </tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>Totals</th><td colspan="3"></td>
                                        <th><% totals.amount | currency:"" %></th>
                                        <th><% totals.gross_interest | currency:"" %></th>
                                        <th><% totals.tax | currency:""%></th>
                                    </tr>
                                    <tr>
                                        <td colspan = "2" class = "text-center">
                                            Items per page
                                        </td>
                                        <td colspan = "2" class = "text-center">
                                            <input type = "text" ng-model = "itemsByPage"/>
                                        </td>
                                        <td colspan = "4" class = "text-center">
                                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                        </td>
                                    </tr>
                                </tfoot>
                        </table>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop