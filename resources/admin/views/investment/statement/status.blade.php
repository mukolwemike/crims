@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="detail-group">
                <table class="table table-hover table-responsive">
                    <thead></thead>
                    <tbody>
                        <tr><td>{!! $batch->name !!}</td></tr>
                        <tr><td>Sent on {!! $batch->created_at->toRfc850String() !!}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="detail-group">
                <div ng-controller="statementStatusCtrl">
                    {!! Form::text('batch', $batch->id, ['init-model'=>'batch_id', 'style'=>'display: none !important']) !!}
                    <div class="alert alert-info" ng-hide="sent">
                        <p>The statements are currently being sent to clients</p>
                    </div>
                    <div class="alert alert-success" ng-show="sent">
                        <p>The statements have been sent successfully</p>
                    </div>
                    <uib-progressbar animate="true" value="progress" type="success"><b><% progress | currency:"" %>%</b></uib-progressbar>
                </div>
            </div>
        </div>
    </div>
@endsection