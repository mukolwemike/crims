<table class="table table-striped" style="font-size:12px;">
    <thead>
    <tr>
        <th>Description</th><th>Amount</th><th>Rate</th><th>Action Date</th><th>Gross Interest</th><th>Net Interest</th><th>Action</th><th>Total</th><th>Maturity Date</th>
    </tr>
    </thead>
    <?php $count = 1 ?>
    <tbody>
    @foreach($data['investments'] as $prepared)
            <tr><td colspan="9" style="padding-left: 10px"><b>{!! $count++ !!}.</b></td></tr>
            <tr>
                <td>Principal</td>
                <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($prepared->investment->amount, false, 0) !!}</td>
                <td align="center">{!! $prepared->investment->interest_rate !!}%</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($prepared->investment->invested_date) !!}</td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($prepared->investment->amount, false, 0) !!}</td>
                @if($prepared->investment->on_call)
                    <td>On call</td>
                @else
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($prepared->investment->maturity_date) !!}</td>
                @endif
            </tr>
            @foreach($prepared->actions as $action)
                <tr>
                    <td>{{ $action->description  }}</td>
                    <td></td><td></td>
                    <td>{{ \Cytonn\Presenters\DatePresenter::formatShortDate($action->date) }}</td>
                    <td align="right">{{ \Cytonn\Presenters\AmountPresenter::currency($action->gross_interest, false, 0) }}</td>
                    <td align="right">{{ \Cytonn\Presenters\AmountPresenter::currency($action->net_interest, false, 0) }}</td>
                    <td align="right">{{ \Cytonn\Presenters\AmountPresenter::accounting($action->amount, false, 0) }}</td>
                    <td align="right">{{ \Cytonn\Presenters\AmountPresenter::currency($action->total, false, 0) }}</td>
                    <td></td>
                </tr>
            @endforeach
            <tr>
                <th class="left">Total</th>
                <td class="right bold">
                    {!! \Cytonn\Presenters\AmountPresenter::currency($prepared->total->principal, false, 0) !!}
                </td>
                <th>
                    @if($prepared->total->principal > 0)
                        {!! $prepared->investment->interest_rate !!}%
                    @endif
                </th>
                <th class="left">{!! \Cytonn\Presenters\DatePresenter::formatShortDate($statementDate) !!}</th>
                <th class="right">
                    {!! \Cytonn\Presenters\AmountPresenter::currency($prepared->total->gross_interest_cumulative, false, 0) !!}
                </th>
                <th class="right">
                    {!! \Cytonn\Presenters\AmountPresenter::currency($prepared->total->net_interest_cumulative, false, 0) !!}
                </th>
                <th></th>
                <th class="right">
                    {!! \Cytonn\Presenters\AmountPresenter::currency($prepared->total->total, false, 0) !!}
                </th>
                <th class="left">
                    @if($prepared->investment->on_call && $prepared->total->principal <= 0)
                        On call
                    @elseif($prepared->total->principal <= 0)
                        {!! \Cytonn\Presenters\DatePresenter::formatShortDate($prepared->investment->maturity_date) !!}
                    @endif
                </th>
            </tr>
    @endforeach
    <tr><td colspan="9" height="14px"></td></tr>
    <tr class="right">
        <th class="left">Grand Total</th>
        <th>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['total_principal'], false, 0) !!}</th>
        <th colspan="3"></th>
        <th>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['total_interest'], false, 0) !!}</th>
        <th></th>
        <th>{!! $product->currency->symbol.' '.\Cytonn\Presenters\AmountPresenter::currency($data['totals']['total_client_value'], false, 0) !!}</th>
        <th></th>
    </tr>
    </tbody>
</table>