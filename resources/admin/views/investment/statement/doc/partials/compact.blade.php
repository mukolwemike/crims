@foreach($investment->statement->payments as $payment)
    <tr>
        <?php
        $balance = $payment->balance;
        $interest_date = $payment->interest_date;
        ?>
        <td>Interest withdrawal</td>
        <td></td>
        <td></td>
        <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($payment->date_paid) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($payment->gross_interest, false, 0) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($payment->net_interest, false, 0) !!}</td>
        <td align="right">({!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount, false, 0) !!})</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($balance = $balance - $payment->amount, false, 0) !!}</td>
        <td></td>
    </tr>
@endforeach