@foreach($investment->statement->monthly_breakdown as $month)
    <tr>
        <?php
            $balance = $month->balance;
        ?>

        <td>Interest: {!! $month->start->toFormattedDateString() !!} - {!! $month->end->toFormattedDateString() !!}</td>
        <td></td>
        <td></td>
        <td>{!! \Cytonn\Presenters\DatePresenter::formatShortDate($month->end) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($month->gross_interest, false, 0) !!}</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($month->net_interest, false, 0) !!}</td>
        <td align="right">({!! \Cytonn\Presenters\AmountPresenter::currency($month->withdrawal, false, 0) !!})</td>
        <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($balance, false, 0) !!}</td>
        <td></td>
    </tr>
@endforeach