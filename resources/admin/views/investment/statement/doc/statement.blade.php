@extends('reports.letterhead')

@section('content')

    @include('reports.partials.client_address')

    <p class="bold-underline">RE: STATEMENT OF ACCOUNT</p>

    <p>Kindly see below your {!! strtoupper($product->shortname) !!} statement showing account status as at {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}.<br/><br/></p>

    @include('investment.statement.doc.table')

    <br/>
    @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif
    <p>Thank you for investing with us.</p>
    <p>Kindly note that you can issue rollover instructions online from <a href="https://clients.cytonn.com/">clients.cytonn.com</a> for matured investment. Please note that for matured investment that we do not receive communication after 1 week we will automatically rollover for 3 months to ensure you don’t lose any interest income.</p>
    <p>To manage your investments online, kindly request for log in details by emailing us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a></p>
    <p>Yours Sincerely,</p>
    </br>
    <?php $fm = $product->fundManager; ?>

    <p class="bold"> For {!! $fm->fullname !!}<br/>
        <img src="{{ storage_path('resources/signatures/' . $fm->signature) }}" height="60px"/>
    </p>
    <p><strong style="text-decoration: underline;">{!! $fm->principal_partner ? $fm->principal_partner : $fm->fullname !!}</strong></p>
@stop