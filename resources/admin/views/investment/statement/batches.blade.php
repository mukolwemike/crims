@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div class="detail-group" ng-controller="statementCtrl">
                <a ng-hide ="stmtsent" class="btn btn-success margin-bottom-20" href="#" data-toggle="modal" data-target="#statementModal"><i class="fa fa-envelope"></i> Send all Statements</a>

                <div ng-show="stmtsent">
                    <div class="alert alert-success">
                        <p>The statements are currently being sent to clients</p>
                    </div>
                    <uib-progressbar animate="true" value="progress" type="success"><b><% progress %>%</b></uib-progressbar>
                </div>
            </div>

            <div class="detail-group">
                <table class="table table-hover table-responsive table-striped">
                    <thead>
                    <tr><th>Batch Name</th><th>Sent On</th><th>Sent by</th><th>Total jobs</th><th>Complete</th><th>Status</th></tr>
                    </thead>
                    <tbody>
                    @foreach($batches as $batch)
                        <tr>
                            <td>{!! $batch->name !!}</td>
                            <td>{!! $batch->created_at !!}</td>
                            <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($batch->sent_by) !!}</td>
                            <td>{!! $batch->total_jobs !!}</td>
                            <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($batch->sent) !!}</td>
                            <td><a href="/dashboard/investments/reports/statements/{!! $batch->id !!}" ><i class="fa fa-list-alt"></i> </a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $batches->links() !!}
            </div>
        </div>
    </div>
@endsection

    <!-- Modal -->
<div class="modal fade" id="statementModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" ng-controller="statementModalCtrl">
        <div class="modal-content">
            <div class="modal-header border-bottom-none">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Do you want to email all statements to clients?</h4>
            </div>
            <div class="modal-body">
                <h5>Statement batch</h5>

                {!! Form::open(['name'=>'stmtform','ng-submit'=>'submit($event)']) !!}

                {!! Form::select('name', $requests ,null, ['init-model'=>'stmt_name', 'class'=>'form-control', 'id'=>'stmt_name', 'required']) !!}


                <div class="alert alert-warning">
                    <p>Once this process is initiated it cannot be stopped. The statements will be generated and automatically sent to clients</p>
                </div>
                <div class="">
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" ng-click="submit($event)" data-dismiss="modal"><i class="fa fa-exclamation-triangle"></i> Send</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>