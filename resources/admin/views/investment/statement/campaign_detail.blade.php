@extends('layouts.default')

@section('content')
    <div class="">
        <div class="panel-dashboard">
            <div class="row">
                <div class="col-md-7">
                    <h3>{!! $campaign->name !!}</h3>
                </div>
                <div class="col-md-2">
                    <div class="margin-top-40 pull-right">
                        @if($campaign->closed)
                            <span class="label label-default">Closed</span>
                        @else
                            <span class="label label-success">Ongoing</span>
                        @endif
                        <span>{!! $sent_count !!}/{!! $total_count !!} sent</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="margin-top-40">
                        <a href="" class="btn btn-success margin-bottom-20" data-toggle="modal" data-target="#send">Send Statements</a>
                        <a href="" class="btn btn-danger margin-bottom-20" data-toggle="modal" data-target="#close">Close Campaign</a>
                    </div>
                </div>
            </div>

            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="#sending" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Sending To</a></li>
                <li role="presentation" class=""><a href="#missing" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Missing</a></li>
                <li role="presentation" class=""><a href="#accompaniments" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Accompaniments</a></li>
            </ul>

            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="sending" aria-labelledby="home-tab">
                    <div ng-controller="StatementCampaignController">
                        {!! Form::text('campaign', $campaign->id, ['class'=>'hide', 'init-model'=>'campaign_id']) !!}

                        <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th colspan="3">

                                </th>
                                <th colspan = "2"><input st-search = "" class = "form-control pull-right" placeholder = "Search..." type = "text"/></th>
                            </tr>
                            <tr>
                                <th>Client Code</th>
                                <th>Client Name</th>
                                <th>Added by</th>
                                <th>Added on</th>
                                <th></th>
                                <th>Sent</th>
                            </tr>
                            </thead>
                            <tbody ng-show="!isLoading">
                            <tr ng-repeat = "row in displayed">
                                <td><% row.client_code %></td>
                                <td><% row.client_name %></td>
                                <td><% row.added_by%></td>
                                <td><% row.added_on %></td>
                                <td>
                                    @if(! $campaign->closed)
                                        <a ng-controller="PopoverCtrl" uib-popover="Remove from campaign" popover-trigger="mouseenter" href="/dashboard/investments/statements/campaign/remove_client/<% row.id %>"><i
                                                    class="fa fa-trash"></i></a>
                                    @endif
                                    <a ng-controller="PopoverCtrl" uib-popover="View clients summary" popover-trigger="mouseenter" href="/dashboard/investments/summary/client/<% row.client_id %>"><i class="fa fa-bar-chart"></i></a>
                                </td>
                                <td to-html="row.sent | booleanIcon "></td>
                            </tr>
                            </tbody>

                            <tbody ng-show="isLoading">
                            <tr>
                                <td colspan="5" class="text-center">Loading ... </td>
                            </tr>
                            </tbody>

                            <tfoot>
                            <tr>
                                <td colspan = "2" class = "text-center">
                                    Items per page
                                </td>
                                <td colspan = "1" class = "text-center">
                                    <input type = "text" ng-model = "itemsByPage"/>
                                </td>
                                <td colspan = "2" class = "text-center">
                                    <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="missing" aria-labelledby="home-tab">
                    <div ng-controller="StatementCampaignMissingController">
                        {!! Form::text('campaign', $campaign->id, ['class'=>'hide', 'init-model'=>'campaign_id']) !!}
                        <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                            <thead>
                            <tr>
                                <th colspan="5">Statements not sent to <% count %> clients</th>
                                <th colspan="1"></th>
                                <th colspan="1"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                            </tr>
                            <tr>
                                <th st-sort="id">ID</th>
                                <th st-sort="client_code">Client Code</th>
                                <th>Name</th>
                                <th>Email Address</th>
                                <th>Phone Number</th>
                                <th>Client Type</th>
                                <th>Active</th>
                                <th>Client Summary</th>
                            </tr>
                            </thead>
                            <tbody  ng-show="!isLoading">
                            <tr ng-repeat="row in displayed">
                                <td><% row.id %></td>
                                <td><% row.client_code %></td>
                                <td><% row.fullName %></td>
                                <td><% row.email %></td>
                                <td><% row.phone %></td>
                                <td><% row.type %></td>
                                <td><span to-html="row.active | activeStatus"></span></td>
                                <td>
                                    <a ng-controller="PopoverCtrl" uib-popover="View clients summary" popover-trigger="mouseenter" href="/dashboard/investments/summary/client/<%row.id%>"><i class="fa fa-bar-chart"></i></a>
                                </td>
                            </tr>
                            </tbody>
                            <tbody ng-show="isLoading">
                            <tr>
                                <td colspan="8" class="text-center">Loading ... </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan = "2" class = "text-center">
                                    Items per page
                                </td>
                                <td colspan = "2" class = "text-center">
                                    <input type = "text" ng-model = "itemsByPage"/>
                                </td>
                                <td colspan = "4" class = "text-center">
                                    <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="accompaniments" aria-labelledby="home-tab">
                    <div>
                        <div class="col-md-12">
                            <div class="detail-group">
                                <h4>Campaign Message</h4>
                                <br />

                                @if($campaign->statement_message)
                                    {!! $campaign->statement_message !!}
                                    <br />
                                    <a class="btn btn-success" data-toggle="modal" data-target="#campaignMessage">Update Message</a>
                                @else
                                    <p>No message added.</p>
                                    <br />
                                    <a class="btn btn-success" data-toggle="modal" data-target="#campaignMessage">Add Message</a>
                                @endif
                            </div>
                        </div>
                        {{--<div class="col-md-12">--}}
                            {{--<div class="detail-group">--}}
                                {{--<h4>Campaign Documents</h4>--}}
                                {{--<br />--}}

                                {{--@if($campaign->client)--}}
                                    {{--<table class="table table-responsive table-striped">--}}
                                        {{--<thead>--}}
                                        {{--<th>Name</th><th>Actions</th>--}}
                                        {{--</thead>--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td>{!! $campaign->campaignDocument !!}</td>--}}
                                            {{--<td>{!! $institution->client->contact->email !!}</td>--}}
                                            {{--<td>--}}
                                                {{--<a class="fa fa-edit" href="/dashboard/clients/create/{!! $institution->client_id !!}"></a>--}}
                                                {{--<a class="fa fa-trash" href="/dashboard/clients/create/{!! $institution->client_id !!}"></a>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--@else--}}
                                    {{--<a href="/dashboard/portfolio/institutions/{!! $institution->id !!}/client/create" class="btn btn-success">Add Client</a>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- Modal -->
<div class="modal fade" id="close" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Close Campaign</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">
                    <p>A campaign should only be closed when no clients should be added, typically when statements have been sent to all clients</p>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::open(['route'=>['close_statement_campaign', $campaign->id]]) !!}
                {!! Form::submit('Close Campaign', ['class'=>'btn btn-danger']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="send" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send Campaign</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">
                    <p>This will send the statements in the campaign to the clients</p>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::open(['route'=>['send_statement_campaign', $campaign->id]]) !!}
                {!! Form::submit('Send Statements', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="campaignMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Campaign Message</h4>
            </div>
            {!! Form::open(['route'=>['store_campaign_message', $campaign->id]]) !!}
            <div class="modal-body">
                <div class="form-group" ng-controller="SummerNoteController">
                    {!! Form::label('campaign_message', 'Statement Message') !!}
                    {!! Form::textarea('statement_message', $campaign->statement_message, ['id' => 'tinymcetextcampign']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'campaign_message') !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Save Message', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>