@extends('layouts.default')

@section('content')
    <div class="panel-dashboard" ng-controller="ClientSummaryController">

        <div class="margin-top-20"></div>

        <a href="#" data-toggle="modal" data-target="#create-campaign" class="btn btn-success margin-bottom-20">Create a campaign</a>

        <table class="table table-responsive table-striped">
            <thead>
                <tr>
                    <th>Name</th><th>Sender</th><th>Type</th><th>Date</th><th colspan="2">Closed</th>
                </tr>
            </thead>
            <tbody>
                @foreach($campaigns as $statement)
                    <tr>
                        <td>{!! $statement->name !!}</td>
                        <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($statement->sender_id) !!}</td>
                        <td>{!! @$statement->type->name !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($statement->send_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($statement->closed) !!}</td>
                        <td><a href="/dashboard/investments/statements/campaign/detail/{!! $statement->id !!}"><i class="fa fa-list-alt"></i></a> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {!! $campaigns->links() !!}

    </div>
@endsection


<!-- Modal -->
<div class="modal fade" id="create-campaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>'create_statement_campaign']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create a statement campaign</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                    <div class="form-group">
                        {!! Form::label('name', 'Campaign name') !!}

                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('type_id', 'Campaign Type') !!}

                        {!! Form::select('type_id', $campaign_types, null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group" ng-controller="DatepickerCtrl">
                        {!! Form::label('send_date', 'Sending date') !!}

                        {!! Form::text('send_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save Campaign', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>