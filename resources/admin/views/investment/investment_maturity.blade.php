@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            @include('investment.partials.investmentdetail', ['inv_title'=>'Investment Details'])
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <h3>Maturity Notification</h3>

                <table class="table table-responsive table-striped">
                    <thead>
                    <tr><th>Principal</th><th>Interest rate</th><th>Duration</th><th>Start Date</th><th>Maturity Date</th><th>Net Interest Accrued</th><th>Withdrawals</th><th>Value at Maturity</th></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount, false, 0) !!}</td>
                        <td>{!! $investment->interest_rate !!}%</td>
                        <td>{!! $investment->repo->getTenorInDays() !!} days</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->netInterestBeforeDeductions($investment->maturity_date), false, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getWithdrawnAmount(), false, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date), false, 0) !!}</td>
                    </tr>
                    </tbody>
                </table>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#notificationModal">
                    Send Notification
                </button>

                <!-- Modal -->
                <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Send maturity notification</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to send this notification?</p>

                                <table class="table table-responsive table-striped">
                                    <thead>
                                    <tr><th>Principal</th><th>Interest rate</th><th>Duration</th><th>Start Date</th><th>Maturity Date</th><th>Net Interest Accrued</th><th>Withdrawals</th><th>Value at Maturity</th></tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount, false, 0) !!}</td>
                                            <td>{!! $investment->interest_rate !!}%</td>
                                            <td>{!! $investment->repo->getTenorInDays() !!} days</td>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->netInterestBeforeDeductions($investment->maturity_date), false, 0) !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getWithdrawnAmount(), false, 0) !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date), false, 0) !!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <a href="/dashboard/investments/clientinvestments/maturity/send/{!! $investment->id !!}" class="btn btn-success">Send Notification</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection