@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard" ng-controller="bankInstructionCtrl">
            <h3>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</h3>
            <hr/>
            <div class="col-md-6">
                @include('investment.partials.investmentdetail')
            </div>
            <div class="hide">
                {!! Form::text('investment_bal',$investment->repo->getTotalValueOfAnInvestment(), ['init-model'=>'investment_bal'] ) !!}
                {!! Form::text('client_name', \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) , ['init-model'=>'client_name']) !!}
                {!! Form::text('client_code', $investment->client->client_code , ['init-model'=>'client_code']) !!}
            </div>
            <div class="col-md-6">
                <div class="detail-group">
                    <h4>Select Signatories</h4>

                    {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}

                    <div class="alert alert-info">
                        <p>Generate Instructions for {!! $instruction->description !!}: {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount) !!}</p>
                    </div>

                    @if($instruction->viewed)
                        <div class="alert alert-danger">
                            <p>Instruction has been printed. Clicking Generate might send an email to signatories</p>
                        </div>
                    @endif

                    <div class="hide">

                    {!! Form::text('amount', $instruction->amount, ['class'=>'form-control', 'required'=>'required', 'step'=>'0.01', 'init-model'=>'amount', 'id'=>'amount']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('first_signatory', 'First Signatory') !!}

                    {!! Form::select('first_signatory', $signatories, null, ['class'=>'form-control', 'init-model'=>'first_signatory', 'id'=>'first_signatory']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory') !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('second_signatory', 'Second Signatory') !!}

                    {!! Form::select('second_signatory', $signatories, null, ['class'=>'form-control', 'init-model'=>'second_signatory', 'id'=>'second_signatory']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory') !!}
                    </div>

                    <div class="form-group">
                    {!! Form::submit('Generate Instructions', ['class'=>'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


<script type = "text/ng-template" id = "bankInstruction.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">

                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Bank Instructions</h4>
                </div>
                <div class = "modal-body">
                    <div>
                        <div class = "detail-group">
                            <p>Are you sure you want generate instructions for the transfer below?</p>
                            <table class = "table table-hover table-responsive table-striped">
                                <tbody>
                                    <tr>
                                        <td>Client Name</td>
                                        <td><% client_name %></td>
                                    </tr>
                                    <tr>
                                        <td>Client Code</td>
                                        <td><% client_code %></td>
                                    </tr>
                                    <tr>
                                        <td>Transfer amount</td>
                                        <td><% amount | currency:"" %></td>
                                    </tr>
                                    <tr>
                                        <td>First Signatory</td>
                                        <td><% signatory_one_name %></td>
                                    </tr>
                                    <tr>
                                        <td>Second Signatory</td>
                                        <td><% signatory_two_name %></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class = "detail-group">
                            <div class = "pull-right">
                                {!! Form::open([]) !!}
                                {!! Form::hidden('investment', Crypt::encrypt($investment->id)) !!}
                                <div style = "display: none !important;">
                                    {!! Form::text('amount', null, ['init-model'=>'amount']) !!}
                                    {!! Form::text('first_signatory', NULL, ['init-model'=>'first_signatory']) !!}
                                    {!! Form::text('second_signatory', NULL, ['init-model'=>'second_signatory']) !!}
                                </div>
                                <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                        data-dismiss = "modal">No
                                </button>
                                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>