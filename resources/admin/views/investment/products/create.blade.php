@extends('layouts.default')

@section('content')
    <div class = "col-md-6 col-md-offset-3">
        <div class = "panel-dashboard">
            <div class = "detail-group">
                {!! Form::model($product, ['route' => ['products.store', $product->id]]) !!}

                <div class = "form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', NULL, ['class'=>'form-control', 'required', 'placeholder' => 'Product Name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', NULL, ['class'=>'form-control', 'required', 'placeholder' => 'Product Description']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('shortname', 'Short Name') !!}
                    {!! Form::text('shortname', NULL, ['class'=>'form-control', 'required', 'placeholder' => 'Product Short Name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'shortname') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('longname', 'Long Name') !!}
                    {!! Form::text('longname', NULL, ['class'=>'form-control', 'required', 'placeholder' => 'Product Long Name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'longname') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('currency_id', 'Currency') !!}
                    {!! Form::select('currency_id', $currencies, null , ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'currency_id') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('custodial_account_id', 'Custodial Account') !!}
                    {!! Form::select('custodial_account_id', $custodialAccounts, null , ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                    {!! Form::select('fund_manager_id', $fundManagers, null , ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'fund_manager_id') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('type_id', 'Product Type') !!}
                    {!! Form::select('type_id', $productTypes, null , ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type_id') !!}
                </div>

                <div class = "form-group">
                    {!! Form::label('active', 'Status') !!}
                    {!! Form::select('active', ['1' => 'Active', '0' => 'Inactive'], null , ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'active') !!}
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
