@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "col-md-12 panel-dashboard">
            <div class="panel panel-default">
                <div class="panel-heading"><span>Product Details</span></div>
                <div class="panel-body">

                    <p class="col-md-4"><b>Name :</b>  {!! $product->name !!}</p>
                    <p class="col-md-8"><b>Description :</b> {!! $product->description !!} </p><div class="clearfix"></div>
                    <p class="col-md-4"><b>Short Name :</b>  {!! $product->shortname !!}</p>
                    <p class="col-md-8"><b>Long Name :</b>  {!! $product->longname !!}</p><div class="clearfix"></div>
                    <p class="col-md-4"><b>Currency :</b>  {!! $product->currency->code !!}</p>
                    <p class="col-md-8"><b>Custodial Account :</b>  {!! $product->custodialAccount->account_name !!}</p></p><div class="clearfix"></div>
                    <p class="col-md-4"><b>Fund Manager :</b>  {!! $product->fundManager->name !!}
                    <p class="col-md-4"><b>Product Type :</b>  {!! $product->type->name !!}</p>
                    <p class="col-md-4"><b>Status :</b>  {!! $product->present()->getStatus !!}</p><div class="clearfix"></div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Document Uploads</span>
                </div>
                <div class="panel-body">
                    @if($product->productDocuments()->count() <= 0)
                        <p>The are no uploaded documents</p>
                    @else
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>View</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product->productDocuments as $productDocument)
                                <tr>
                                    <td>{!! $productDocument->name !!}</td>
                                    <td><a class="fa fa-eye"
                                           target="_blank"
                                           href="/dashboard/documents/{!! $productDocument->document_id !!}">View
                                            file</a>
                                    </td>
                                    <td>
                                        <a href="#" class="fa fa-edit" data-toggle="modal"
                                                data-target="#productDocumentEditModal_{!! $productDocument->id !!}">
                                            Edit Document
                                        </a>
                                        {{--Document Upload Modal--}}
                                        <div class="modal fade" id="productDocumentEditModal_{!! $productDocument->id !!}" tabindex="-1" role="dialog" aria-labelledby="productDocumentUploadModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    {!! Form::open(['files'=>true, 'route'=>['products.upload_documents', $product->id, $productDocument->id]]) !!}
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Update Product Document</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            {!! Form::label('name', 'Document Name') !!}
                                                            {!! Form::text('name', $productDocument->name, ['class' => 'form-control', 'required', 'placeholder' => 'Document Name']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                                                        </div>

                                                        <div class="form-group">
                                                            {!! Form::label('file', 'File') !!}
                                                            {!! Form::file('file', ['class'=>'form-control', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {!! Form::hidden('product_id', $product->id) !!}
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                    <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#productDocumentUploadModal">
                        Upload Document
                    </button>
                </div>

            </div>
        </div>
    </div>

    @include('investment.products.partials.details_partial')
@stop