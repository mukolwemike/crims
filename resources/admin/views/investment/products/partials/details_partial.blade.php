{{--Document Upload Modal--}}
<div class="modal fade" id="productDocumentUploadModal" tabindex="-1" role="dialog" aria-labelledby="productDocumentUploadModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['files'=>true, 'route'=>['products.upload_documents', $product->id, null]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Product Document</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('name', 'Document Name') !!}
                    {!! Form::text('name', '', ['class' => 'form-control', 'required', 'placeholder' => 'Document Name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('file', 'File') !!}
                    {!! Form::file('file', ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::hidden('product_id', $product->id) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>