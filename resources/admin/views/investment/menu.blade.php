@extends('layouts.default')

@section('content')
    <bread-crumb title="">
        <a class="current" slot="sub_1" href="#">Investments</a>
    </bread-crumb>

    <dashboard-analysis-cards-section></dashboard-analysis-cards-section>
    <div class="admin_menus dashboard__account_summary ">
        <div class="summaries">
            <menu-card-details
                    title="Orders"
                    title_time="Today"
                    description="Orders Summary"
                    description_sub="Orders Summary"
                    footer="All Orders"
                    footer_sub="All Orders">

                <a href="/dashboard/investments/client-instructions" slot="description">
                    <span>View All</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>

                <a href="/dashboard/investments/orders" slot="footer">
                    <span>View All</span>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>

            </menu-card-details>

            <menu-card
                    title="Approve Transactions"
                    statistics_title="Pending"
                    :fetch="false"
                    statistics="{!! $gen->getClientTransactionsAwaitingApprovalCount() !!}"
                    link="/dashboard/investments/approve"
                    :first="true"
                    description="Manage Transaction Approvals">
                <i class="fa fa-check-square-o"></i>
            </menu-card>

            <menu-card
                    title="Business Confirmation"
                    link="/dashboard/investments/confirmation"
                    :first="false"
                    description="Manage Business Confirmations">
                <i class="fa fa-file-text-o"></i>
            </menu-card>

            <menu-card
                    title="Interest rates"
                    link="/dashboard/investments/interest-rates"
                    :first="false"
                    description="Manage Interest Rates">
                <i class="fa fa-bar-chart"></i>
            </menu-card>


            <menu-card
                    title="Commission"
                    link="/dashboard/investments/commission"
                    :first="false"
                    description="Manage FA Commissions">
                <i class="fa fa-line-chart"></i>
            </menu-card>

            <menu-card
                    title="Transaction Logs"
                    link="/dashboard/investments/activity"
                    :first="false"
                    description="View Transaction Logs">
                <i class="fa fa-feed"></i>
            </menu-card>


            <menu-card
                    title="Investment Reports"
                    link="/dashboard/investments/reports"
                    :first="false"
                    description="Export Investment Reports">
                <i class="fa fa-files-o"></i>
            </menu-card>

            <menu-card
                    title="Client Banks"
                    link="/dashboard/banks"
                    :first="false"
                    description="Manage Client Banks">
                <i class="fa fa-bank"></i>
            </menu-card>

            <menu-card
                    title="Client Summary & Analytics"
                    link="/dashboard/clients/summary"
                    :first="false"
                    description="View Client Summary & Analytics">
                <i class="fa fa-bar-chart"></i>
            </menu-card>

            @permission('products:view')
            <menu-card
                    title="Products"
                    link="/dashboard/products"
                    :first="false"
                    description="Manage Investment Products">
                <i class="fa fa-briefcase"></i>
            </menu-card>
            @endPermission()

            @permission('unittrust:view-funds')
            <menu-card
                    title="Unit Trusts"
                    link="/dashboard/unitization/unit-funds"
                    :first="true"
                    description="Manage Unit Trusts">
                <i class="fa fa-gg"></i>
            </menu-card>
            @endPermission

            @permission('upload_documents')
            <menu-card
                    title="Documents Management"
                    link="/dashboard/documents"
                    :first="false"
                    description="Manage Documents">
                <i class="fa fa-file-pdf-o"></i>
            </menu-card>
            @endPermission()

            @permission('unittrust:view-funds')
            <menu-card
                    title="USSD Applications"
                    link="/dashboard/unitization/ussd/ussd-applications"
                    :first="false"
                    description="View Applications">
                <i class="fa fa-hashtag"></i>
            </menu-card>
            @endPermission

        </div>
    </div>
@stop
