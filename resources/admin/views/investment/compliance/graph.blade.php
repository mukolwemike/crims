@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div class="app">
                {!! $chart->html() !!}
            </div>
            {!! \ConsoleTVs\Charts\Facades\Charts::scripts() !!}
            {!! $chart->script() !!}
        </div>
    </div>
@endsection