@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="detail-group">
                <h4>Client Details</h4>
                <table class="table table-striped table-responsive">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td>Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>E-Mail Address</td>
                        <td>{!! $client->contact->email !!}</td>
                    </tr>
                    <tr>
                        <td>Telephone No.</td>
                        <td>
                            {!! $client->phone !!}
                            @if($client->contact->phone)/ {!! $client->contact->phone !!} @endif
                            @if($client->telephone_office)/ {!! $client->telephone_office !!} @endif
                            @if($client->telephone_home)/ {!! $client->telephone_home !!} @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="detail-group">
                <client-details-validation :client="{{json_encode($client)}}"></client-details-validation>
            </div>

            <div class="detail-group">
                <h4>KYC Documents</h4>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Compliant: {!! \Cytonn\Presenters\BooleanPresenter::presentIcon($client->repo->compliant()) !!}
                            </p>
                        </div>

                        <div class="col-md-12">
                            <div class="detail-group">
                                <p><b>Required Kyc Documents</b></p>
                                <table class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Preview</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($checklist as $doc)
                                        <tr>
                                            <td>{!! $doc->name !!}</td>
                                            <td>
                                                @if($checked_items->contains($doc->id))
                                                    <span class="label label-success">Uploaded</span>
                                                @else
                                                    <span class="label label-warning">Missing</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($checked_items->contains($doc->id))
                                                    <?php
                                                    $kyc = App\Cytonn\Models\ClientUploadedKyc::where(['kyc_id' => $doc->id])->where(['client_id' => $client->id])->first();
                                                    ?>

                                                    @if($kyc->document)
                                                        <a target="_blank"
                                                           href="/dashboard/investments/client-instructions/filled-application-documents/{!! $kyc->document_id !!} ">
                                                            <i class="fa fa-file-pdf-o"> View</i>
                                                        </a>
                                                    @endif

                                                @endif </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if($client->repo->compliant())
                        @else
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="category"> Select category how to upload documents: </label>
                                    <select init-model="category" name="category" id="category" class="form-control"
                                            style="width: 20%">
                                        <option value="individual">Individual Upload</option>
                                        <option value="contribution">Combined Upload</option>
                                    </select>

                                </div>
                            </div>
                        @endif
                    </div>

                    <div>
                        <div class="col-md-12 detail-group" ng-show="category === 'contribution'">
                            {!! Form::open(['route' => ['upload_compliance_form', 'method' => 'POST'], 'files' => true]) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::label('Upload Form') !!}<br/><br/>
                                        {!! Form::file("doc", ['class'=>'form-control', 'required']) !!}
                                        {!! Form::hidden('client_id', Crypt::encrypt($client->id)) !!}
                                    </div>
                                    <div class="col-md-7 col-md-offset-1">
                                        {!! Form::label('Check the KYC documents included therein.') !!}<br/><br/>
                                        @foreach($checklist as $key => $kyc)
                                            {!! Form::checkbox("kycs[$kyc->id]", $kyc->id, $checked_items->contains($kyc->id)) !!}
                                            {!! Form::label($kyc->name) !!}<br/>
                                        @endforeach
                                        <br/>
                                        {!! Form::submit('Save Form', ['class'=>'btn btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                        <div class="col-md-12 detail-group" ng-show="category === 'individual'">
                            <div class="row">
                                <div class="col-md-3"><h4>Compliance Item</h4></div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-5" style="text-align: center;"><h4>Upload File</h4></div>
                                        <div class="col-md-2" style="text-align: center;"><h4>Uploaded</h4></div>
                                        {{--<div class="col-md-2" style="text-align: center;"><h4>Preview</h4></div>--}}
                                        <div class="col-md-3" style="text-align: center;"><h4>Validated By</h4></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            {!! Form::open(array('url' => '/dashboard/investments/compliance/'.$client->id, 'files' => true)) !!}
                            @foreach($checklist as $key => $item)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            {!! $key + 1 !!} . {!! Form::label($item->name) !!}
                                        </div>
                                        <div class="col-md-9">
                                            <div class="col-md-5" style="text-align: center;">
                                                {!! Form::hidden('client_id', Crypt::encrypt($client->id)) !!}
                                                {!! Form::file("doc[$item->id]", ['class'=>'form-control']) !!}
                                            </div>
                                            @if($checked_items->contains($item->id))
                                                <?php
                                                $kyc = App\Cytonn\Models\ClientUploadedKyc::where(['kyc_id' => $item->id])->where(['client_id' => $client->id])->first();
                                                ?>
                                                <div class="col-md-2" style="text-align: center;">
                                                    @if($kyc->document)
                                                        <span>
                                                        {!! \Cytonn\Presenters\BooleanPresenter::presentIcon(true) !!}
                                                    </span>
                                                    @else
                                                        <span>
                                                        {!! \Cytonn\Presenters\BooleanPresenter::presentIcon(false) !!}
                                                    </span>
                                                    @endif
                                                </div>
                                                {{--                                                <div class="col-md-2" style="text-align: center;">--}}
                                                {{--                                                    @if($kyc->document)--}}
                                                {{--                                                        <a target="_blank"--}}
                                                {{--                                                           href="/dashboard/investments/client-instructions/filled-application-documents/{!! $kyc->document_id !!} ">--}}
                                                {{--                                                            <i class="fa fa-file-pdf-o"> View</i>--}}
                                                {{--                                                        </a>--}}
                                                {{--                                                    @endif--}}
                                                {{--                                                </div>--}}
                                                <div class="col-md-3" style="text-align: center;">
                                                    @if($kyc->client_user_id)
                                                        {!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($kyc->client_user_id) !!}
                                                    @elseif($kyc->user_id)
                                                        {!! \Cytonn\Presenters\UserPresenter::presentFullNamesNoTitle($kyc->user_id) !!}
                                                    @else
                                                        {!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($kyc->client_id) !!}
                                                    @endif
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::submit('Upload Documents', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="detail-group">
                <h4>Application Documents</h4>

                @if(count($appDocuments))
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr><th>Type</th><th>Uploaded</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach($appDocuments as $doc)
                            <tr>
                                <td>{!! $doc->complianceDocument ? $doc->complianceDocument->name : $doc->document->type->name !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($doc->created_at) !!}</td>
                                <td>
                                    <a target="_blank" href="/document/view-document/{{$doc->document_id}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p>No Application documents uploaded</p><br/>
                @endif
            </div>

            <div class="detail-group">
                <h4>Other Documents</h4>
                <div class="col-md-12">
                    <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Compliance Doc</th>
                            <th>Category</th>
                            <th>Product/Project</th>
                            <th>Type</th>
                            <th>File</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $document)
                            <tr>
                                <td> {!! $document->title !!} </td>
                                <td> {!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($document->compliance_document) !!} </td>
                                <td> @if($document->project_id) Real Estate @elseif($document->product_id)
                                        Investments @endif </td>
                                @if($document->project_id)
                                    <td>{!! $document->project->name !!}</td>
                                @elseif($document->product_id)
                                    <td>{!! $document->product->name !!}</td>
                                @endif
                                <td> {!! $document->type->name !!} </td>
                                <td>
                                    <a href="/dashboard/documents/{!! $document->id !!}"><i
                                                class="fa fa-file-pdf-o"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary margin-bottom-20" data-toggle="modal" data-target="#upload-document">
                        <i class="fa fa-plus-square-o"></i> Upload
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="upload-document" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => ['upload_document'], 'files'=>true]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Other Document</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::hidden('client_id', $client->id, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('category', 'Document Category') !!}
                        {!! Form::select('category', [1=>'Investments', 2=>'Real Estate', 3=>'Portfolio Investors'], null, ['class'=>'form-control', 'init-model'=>'category', 'ng-required'=>'true']) !!}
                    </div>

                    <div class="form-group" ng-if="category == 1">
                        {!! Form::label('product_id', 'Product') !!}
                        {!! Form::select('product_id', $products, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                    </div>
                    <div class="form-group" ng-if="category == 2">
                        {!! Form::label('project_id', 'Project') !!}
                        {!! Form::select('project_id', $projects, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                    </div>
                    <div class="form-group" ng-if="category == 3">
                        {!! Form::label('portfolio_investor_id', 'Portfolio Investor') !!}
                        {!! Form::select('portfolio_investor_id', $portfolioInvestors, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('title', 'Title of Document') !!}
                        {!! Form::text('title', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('type', 'Document Type') !!}
                        {!! Form::select('type', $document_types, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}
                    </div>
                    <div class="form-group" ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('file', 'Select File') !!}
                        {!! Form::file('file', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group" ng-if="category == 2">
                        {!! Form::checkbox('compliance_document', 1) !!}
                        {!! Form::label('compliance_document', 'LOO Compliance Document?') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save Document', ['class'=>'btn btn-success pull-left']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection