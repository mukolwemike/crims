@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <a class="btn btn-default" href="/dashboard/investments/compliance/graph" target="_self"><i class="fa fa-line-chart"></i> View Non Compliance Graph</a>
            <div ng-controller="ComplianceController">
                <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            {{--<td colspan="4"><a href="/dashboard/investments/compliance/export" class="btn btn-success">Export</a></td>--}}
                            <td colspan="2"><a href="#export-comliance-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a></td>
                            <td colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></td>
                        </tr>
                        <tr>
                            <th>Client Code</th>
                            <th>Name</th>
                            <th st-sort-default="true">Compliance</th>
{{--                            <th st-sort-default="true">Kyc Validated</th>--}}
                            <th>Reason</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody  ng-show="!isLoading">
                        <tr ng-repeat="row in displayed">
                            <td><% row.client_code %></td>
                            <td><% row.fullName  %></td>
                            <td><span to-html="row.compliant | complianceStatus"></span></td>
{{--                            <td><span to-html="row.validated | validatedStatus"></span></td>--}}
                            <td><% row.compliance.comment | str_lim:50 %></td>
                            <td><span to-html="row.active | activeStatus"></span></td>
                            <td>
                                <a ng-controller="PopoverCtrl" uib-popover="Compliance details" popover-trigger="mouseenter" href="/dashboard/investments/compliance/<%row.id%>"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    </tbody>
                        <tbody ng-show="isLoading">
                            <tr>
                                <td colspan="100%" class="text-center">Loading ... </td>
                            </tr>
                        </tbody>
                    <tfoot>
                    <tr>
                        <td  colspan="100%" class="text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

<div class="modal fade" id="export-comliance-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_compliance']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Compliance</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)' ]) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)' ]) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>