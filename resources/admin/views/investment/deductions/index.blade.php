@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-6">
            @include('investment.partials.investmentdetail')
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <h4>Deductions</h4>
                <table class="table table-hover table-responsive">
                    <thead>
                        <tr><th>Date</th><th>Narrative</th><th>Amount</th></tr>
                    </thead>
                    <tbody>
                        @foreach($investment->deductions()->orderBy('date', 'ASC')->get() as $deduction)
                            <tr>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($deduction->date) !!}</td>
                                <td>{!! $deduction->narrative !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($deduction->amount) !!}</td>
                            </tr>
                        @endforeach
                        <tr><th colspan="2">Total</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total_deductions) !!}</th></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="detail-group" ng-controller="DeductionsController">
                <h4>Deduct from investment</h4>

                {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}

                <div class="form-group">
                    {!! Form::label('amount', 'Amount to Deduct') !!}

                    {!! Form::text('amount', null, ['class'=>'form-control', 'init-model'=>'amount', 'required']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('date', 'Record Date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'date'=>'date', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('narrative', 'Narrative') !!}

                    {!! Form::text('narrative', null, ['class'=>'form-control', 'init-model'=>'narrative', 'required']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                </div>

                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

<div ng-view></div>
<script type = "text/ng-template" id = "deductions.htm">
    <div class = "modal fade">
        <div class = "modal-dialog">
            <div class = "modal-content">

                <div class = "modal-header no-bottom-border">
                    <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                            data-dismiss = "modal" aria-hidden = "true">&times;</button>
                    <h4 class = "modal-title">Confirm Investment Deduction</h4>
                </div>
                <div class = "modal-body">

                    <div class = "detail-group">
                        <p>Do you want to make the deduction with the details below??</p>
                        <table class = "table table-hover table-responsive table-striped">
                            <tbody>
                            <tr>
                                <td>Client Name</td>
                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                            </tr>
                            <tr>
                                <td>Investment Amount</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            </tr>
                            <tr>
                                <td>Value date</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                            </tr>
                            <tr>
                                <td>Maturity date</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-center">Deduction</td>
                            </tr>
                            <tr>
                                <td>Deduction Amount</td>
                                <td><% amount | currency:"" %></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td><% date | date %></td>
                            </tr>
                            <tr>
                                <td>Narrative</td>
                                <td><% narrative %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "detail-group">
                        <div class = "pull-right">
                            {!! Form::open(['route'=>['deduct_investment', $investment->id]]) !!}
                            <div style = "display: none !important;">
                                {!! Form::text('narrative', NULL, ['ng-model'=>'narrative']) !!}
                                {!! Form::text('date', NULL, ['ng-model'=>'date']) !!}
                                {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                            </div>
                            <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                    data-dismiss = "modal">No
                            </button>
                            {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
