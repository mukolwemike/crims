@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div>
                <div class="">
                    <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                        <i class="fa fa-list-alt"></i>
                        Back to Instructions
                    </a>
                </div>
            </div>
            <br>
            <div class="">
                <create-purchase-units-instruction :funds="{{ json_encode($funds) }}"></create-purchase-units-instruction>
            </div>
        </div>
    </div>
@endsection