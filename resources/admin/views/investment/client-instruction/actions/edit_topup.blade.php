@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @if($investment)
                    @include('investment.partials.investmentdetail', ['inv_title'=>'Existing Investment Details'])
                @endif
            </div>
            <div class="col-md-6">
                <div>
                    <div class="pull-right">
                        <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <hr>
                <div class="">
                    <investment-topup-instruction-edit
                            :topup="{{ json_encode($topup) }}"
                            :investment="{{ json_encode($investment) }}">
                    </investment-topup-instruction-edit>
                </div>
            </div>
        </div>
    </div>
@endsection