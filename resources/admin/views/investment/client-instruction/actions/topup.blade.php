@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div>
                <div class="">
                    <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                        <i class="fa fa-list-alt"></i>
                        Back to Instructions
                    </a>
                </div>
            </div>
            <br>
            <div class="">
                <investment-topup-instruction></investment-topup-instruction>
            </div>
        </div>
    </div>
@endsection