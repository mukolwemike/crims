@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @if($investment)
                    @include('investment.partials.investmentdetail', ['inv_title'=>'Existing Investment Details'])
                @endif
            </div>
            <div class="col-md-6">
                <div>
                    <div class="pull-right">
                        <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <hr>
                <div class="detail-group">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Instruction Details</h4>
                        </div>
                        <div class="col-md-12">
                            @if($instruction->automatic_rollover)
                                <p class="col-md-12">Automatic Rollover: Yes</p>
                            @endif
                            <table class = "table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Type</td>
                                        <td>{!! ucfirst($instruction->type->name )!!}</td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>{!! $instruction->repo->showAmountSelect() !!}  ({!! $investment->product->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->repo->calculateAmountAffected()) !!})</td>
                                    </tr>
                                    <tr>
                                        <td>Agreed Rate</td>
                                        <td>{!! $instruction->agreed_rate !!}%</td>
                                    </tr>
                                    @if($instruction->tenor)
                                        <tr>
                                            <td>Tenor</td>
                                            <td>{!! $instruction->tenor !!} Months</td>
                                        </tr>
                                    @endif

                                    @if($instruction->withdrawal_stage)
                                        <tr>
                                            <td>Withdrawal Stage</td>
                                            <td>{!! ucfirst($instruction->withdrawal_stage) !!} on {!! \Cytonn\Presenters\DatePresenter::formatDate( $instruction->due_date)  !!}</td>
                                        </tr>
                                    @endif

                                    @if($instruction->inactive == 1)
                                        <tr>
                                            <td>Status</td>
                                            <td><span to-html = "{!! $instruction->inactive !!} | boolInactiveStatus"></span></td>
                                        </tr>
                                    @endif

                                    @if($instruction->type->name == 'withdraw')
                                        <tr>
                                            <td>Reasons for Withdrawal</td>
                                            <td>{!! $instruction->reason !!}</td>
                                        </tr>
                                    @endif

                                    @if($instruction->account)
                                        <tr>
                                            <th colspan="2">Selected Account</th>
                                        </tr>

                                        <tr>
                                            <td>Bank Name</td>
                                            <td>{{ $instruction->account->bank->name }}</td>
                                        </tr>

                                        <tr>
                                            <td>Branch Name</td>
                                            <td>{{ $instruction->account->branch->name }}</td>
                                        </tr>

                                        <tr>
                                            <td>Account Name</td>
                                            <td>{{ $instruction->account->account_name }}</td>
                                        </tr>

                                        <tr>
                                            <td>Account Number</td>
                                            <td>{{ $instruction->account->account_number }}</td>
                                        </tr>

                                    @endif
                                    {{--<tr>--}}
                                        {{--<td>Signing Mandate</td>--}}
                                        {{--<td>{{ $instruction->investment->client->present()->getSigningMandate }}</td>--}}
                                    {{--</tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            @if($instruction->repo->scheduledApproval())
                                <div class="alert alert-warning">
                                    There is an existing scheduled approval for the investment
                                </div>
                            @else
                                <div class="btn-group">

                                    @if(!$approved)
                                        <div class="alert alert-warning">
                                            <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate:</b></p>
                                            <br>
                                            <table class="table table-borderless">
                                                <thead>
                                                <tr>
                                                    <th>Client User</th>
                                                    <th>Approval Status</th>
                                                    <th>Reason</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($signatories['signatories'] as $signatory)
                                                    <tr>
                                                        <td>{{ $signatory['user'] }}</td>
                                                        <td>
                                                            @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                                            @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                                            @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                                                        </td>
                                                        <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif

                                    @if(! $kycValidated)
                                        <div class="alert alert-danger" role="alert">
                                            Please validate client's KYC Details to be able to process the instruction.
                                        </div>
                                        <br>
                                    @endif

                                    @if(!$approved)
                                        <a class="btn btn-success" href="#" data-toggle="modal" data-target="#confirm-rollover">Process</a>
                                    @else
                                        @if($kycValidated)
                                            <a class="btn btn-success"
                                               href="/dashboard/investments/client-instructions/rollover/{!! $instruction->id !!}/process">Process</a>
                                        @endif
                                    @endif
                                    <a class="btn btn-primary"
                                       href="/dashboard/investments/client-instructions/rollover/{!! $instruction->id !!}/pdf">View
                                        Pdf</a>
                                    @if($instruction->inactive == 1)
                                        <a class="btn btn-danger disabled" href="#">Cancel Instruction</a>
                                    @else
                                        <a class="btn btn-danger" href="#" data-toggle="modal"
                                           data-target="#cancel-instruction">Cancel Instruction</a>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="modal fade" id="confirm-rollover" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Are you sure to process this instruction?</h4>
            </div>
            {!! Form::open(['route'=>['process_rollover_instruction', $instruction->id], 'method'=>'POST']) !!}
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning">
                        <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate.</b></p>
                        <br>
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Client User</th>
                                <th>Approval Status</th>
                                <th>Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($signatories['signatories'] as $signatory)
                                <tr>
                                    <td>{{ $signatory['user'] }}</td>
                                    <td>
                                        @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                        @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                        @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                                    </td>
                                    <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <br>
                @if(!$approved)
                    <div class="form-group">
                        {!! Form::label('reason', 'Reason') !!}

                        {!! Form::textarea('reason', null, ['class'=>'form-control', 'init-model'=>'reason', 'required']) !!}
                    </div>
                @else
                @endif
                <br>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes, Continue', ['class'=>'btn btn-success pull-left']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="cancel-instruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['cancel_client_withdraw_rollover_instruction', $instruction->id], 'method'=>'POST']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Cancel {!! ucfirst($instruction->type->name) !!} Instruction</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center">Are you sure you want to cancel this client {!! $instruction->type->name !!} instruction?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
                {!! Form::submit('Yes, Continue', ['class'=>'btn btn-danger pull-left']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>