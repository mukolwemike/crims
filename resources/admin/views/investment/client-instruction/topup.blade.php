@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @if($investment)
                    @include('investment.partials.investmentdetail', ['inv_title'=>'Existing Investment Details'])
                @endif
            </div>
            <div class="col-md-6">
                <div>
                    <div class="pull-right">
                        <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <hr>
                <div class="detail-group">
                    <div class="row">
                        <div class="col-md-12">
                            @if($investment)
                                <h4>Topup Details</h4>
                            @else
                                <h4>Investment Details</h4>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <table class = "table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($topup->client_id) !!} <a href="/dashboard/clients/details/{{ $topup->client_id }}" target="_blank"> <i class="fa fa-long-arrow-right"></i>Details</a></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($topup->amount) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Agreed Rate</td>
                                        <td>{!! $topup->agreed_rate !!}%</td>
                                    </tr>
                                    <tr>
                                        <td>Product</td>
                                        <td>{!! $topup->product->name !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Tenor</td>
                                        <td>{!! $topup->tenor !!} Months</td>
                                    </tr>
                                    @if($topup->user)
                                    <tr>
                                        <td>Filled By</td>
                                        <td>{!! $topup->user->firstname.' '.$topup->user->lastname !!}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>Invested</td>
                                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo(!is_null($topup->investment)) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Mode of payment</td>
                                        <td>{!! $topup->mode_of_payment !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Interest payments</td>
                                        <td>@if($topup->interest_payment_interval > 0) Every {!! $topup->interest_payment_interval !!} months @else On Maturity @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Transfer Advise</td>
                                        <td>@if($topup->document_id) <a target="_blank" href="/dashboard/clients/kyc/document/{{ $topup->document_id }}">View document</a>  @else Not uploaded @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Franchise</td>
                                        <td>{!! Cytonn\Presenters\BooleanPresenter::presentIcon($topup->franchise) !!}</td>
                                    </tr>
                                    @if($topup->inactive == 1)
                                    <tr>
                                        <td>Status</td>
                                        <td><span to-html = "{!! $topup->inactive !!} | boolInactiveStatus"></span></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>Signing Mandate</td>
                                        <td><el-tag>{!! $topup->client->present()->getSigningMandate !!}</el-tag></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="btn-group">
                                @if(!$approved)
                                <div class="alert alert-warning">
                                    <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate.</b></p>
                                    <br>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th>Client User</th>
                                                <th>Approval Status</th>
                                                <th>Reason</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($signatories['signatories'] as $signatory)
                                            <tr>
                                                <td>{{ $signatory['user'] }}</td>
                                                <td>
                                                    @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                                    @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                                    @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                                                </td>
                                                <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif

                                @if($topup->approval)
                                    <a class="btn btn-success disabled" href="#">Process</a>
                                @elseif(!$approved)
                                    <a class="btn btn-success" href="#" data-toggle="modal" data-target="#confirm-topup">Process</a>
                                @else
                                    <a class="btn btn-success" href="/dashboard/investments/client-instructions/topup/{!! $topup->id !!}/process">Process</a>
                                @endif

                                <a class="btn btn-primary" href="/dashboard/investments/client-instructions/topup/{!! $topup->id !!}/pdf">View Pdf</a>

                                @if($topup->inactive == 1 || $topup->approval)
                                    <a class="btn btn-danger disabled" href="#">Cancel</a>
                                @else
                                    <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#cancel-topup">Cancel</a>
                                @endif

                                <br><br><br>
                                @if($topup->approval)
                                    <div class="alert alert-warning">This instruction has already been processed</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="modal fade" id="confirm-topup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Are you sure to process this instruction?</h4>
            </div>
            {!! Form::open(['route'=>['process_topup_instruction', $topup->id], 'method'=>'POST']) !!}
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning">
                        <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate.</b></p>
                        <br>
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Client User</th>
                                <th>Approval Status</th>
                                <th>Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($signatories['signatories'] as $signatory)
                                <tr>
                                    <td>{{ $signatory['user'] }}</td>
                                    <td>
                                        @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                        @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                        @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                                    </td>
                                    <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <br>
                @if(!$approved)
                <div class="form-group">
                    {!! Form::label('reason', 'Reason') !!}

                    {!! Form::textarea('reason', null, ['class'=>'form-control', 'init-model'=>'reason', 'required']) !!}
                </div>
                @else
                @endif
                <br>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes, Continue', ['class'=>'btn btn-success pull-left']) !!}
                {{--<a class="btn btn-success" href="/dashboard/investments/client-instructions/topup/{!! $topup->id !!}/process">Proceed</a>--}}
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="cancel-topup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['cancel_client_topup_instruction', $topup->id], 'method'=>'POST']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Cancel Top-Up Instruction</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center">Are you sure you want to cancel this client top-up instruction?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
                {!! Form::submit('Yes, Continue', ['class'=>'btn btn-danger pull-left']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>