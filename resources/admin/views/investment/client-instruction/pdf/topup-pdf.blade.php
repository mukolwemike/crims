@extends('reports.letterhead')

@section('content')
    <style>
        div{
            font-family: 'Avenir', 'Open Sans', sans-serif;
        }
        .bordered{
            padding-bottom: 20px;
        }
    </style>
    <div class="bordered">
        <div class="header">
            <h2>CYTONN HIGH YIELD SOLUTIONS LLP</h2>

            <h3>TOP-UP FORM</h3>
            <h4>Client Details</h4>
        </div>

        <div class="tab-left">
            <label>Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($topup->client_id) !!} <br/>
            <label for="">Phone Number</label> {!! $topup->client->contact->phone !!}<br/>
            <label for="">Client Code</label> {!! $topup->client->client_code !!} <br/>
        </div>

        <h4>Further Capital Contribution</h4>

        <div class="tab-left">
            <label for="">Amount</label> {!! $topup->product->currency->code !!}
            @if($topup->amount)
                {!! \Cytonn\Presenters\AmountPresenter::currency($topup->amount) !!}
            @else
                {!! \Cytonn\Presenters\AmountPresenter::currency($topup->repo->calculateAmountAffected()) !!}
            @endif
            <br>
            <label for="">Agreed rate</label> {!! $topup->agreed_rate !!}%<br/>
            <label for="">Period</label> {!! $topup->tenor !!} Months <br/>
        </div>
    </div>

    <div style="position: absolute; bottom: 40px; left: 0;">

        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($topup->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>

        <p class="left">REF: CRIMS/TOP-UP/{!! $topup->id !!}</p>
{{--        <p>Filled by: {!! $topup->user->firstname.' '.$topup->user->lastname !!}</p>--}}
    </div>

@endsection