@extends('reports.letterhead')

@section('content')
    <style>
        div{
            font-family: 'Avenir', 'Open Sans', sans-serif;
        }
        .bordered{
            padding-bottom: 20px;
        }
    </style>
    <div class="bordered">
        <div class="header">
            <h2>CYTONN HIGH YIELD SOLUTIONS LLP</h2>

            @if($rollover->isWithdrawal())
                <h3>WITHDRAWAL FORM</h3>
            @endif
            @if($rollover->isRollover())
                <h3>ROLLOVER FORM</h3>
            @endif
            <h4>Client Details</h4>
        </div>

        <div class="tab-left">
            <label>Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!} <br/>
            <label for="">Phone Number</label> {!! $client->contact->phone !!}<br/>
            <label for="">Account Number</label> {!! $client->client_code !!} <br/>
        </div>

        @if($rollover->isRollover())
            <h4>Rollover</h4>

            <div class="tab-left">
                <label for="">Amount</label>

                @if($rollover->amount_select != 'amount')
                    {!! $rollover->repo->showAmountSelect() .' = ' !!}
                @endif

                {!! $rollover->investment->product->currency->code !!}
                {!! \Cytonn\Presenters\AmountPresenter::currency($rollover->repo->calculateAmountAffected()) !!}
                <br>
                <label for="">Agreed rate</label> {!! $rollover->agreed_rate !!}%<br/>
                <label for="">Period</label> {!! $rollover->tenor !!} Months <br/>
            </div>
        @endif

        @if($rollover->isWithdrawal())
            <h4>Withdrawal</h4>

            <div class="tab-left">
                <label for="">Amount</label>

                @if($rollover->amount_select != 'amount')
                    {!! $rollover->repo->showAmountSelect() .' = ' !!}
                @endif

                {!! $rollover->investment->product->currency->code !!}
                {!! \Cytonn\Presenters\AmountPresenter::currency($rollover->repo->calculateAmountAffected()) !!}
                <br>
                @if($rollover->withdrawal_stage)
                    <label for="">Withdrawal date</label> {!! ucfirst($rollover->withdrawal_stage) !!} on {!! \Cytonn\Presenters\DatePresenter::formatDate( $rollover->due_date)  !!}
                @endif
                <br>

                @if($rollover->type->name == 'withdraw')
                    <label for="">Reason for withdrawal</label> {!! $rollover->reason !!}
                @endif

                <br/>
            </div>
        @endif

        <h4>Bank Details</h4>

        <div class="tab-left">
            @if($rollover->account)
                <label for="">Bank Name</label> {!! $rollover->account->bank->name !!}<br/>
                <label for="">Bank Branch</label> {!! $rollover->account->branch->name !!}<br/>
                <label for="">Bank Account Number</label> {!! $rollover->account->account_number !!}<br/>
                <label for="">Bank Account Name</label> {!! $rollover->account->account_name !!}<br/>
            @else
                <label for="">Bank Name</label> {!! $bank->bankName() !!}<br/>
                <label for="">Bank Account Name</label> {!! $bank->accountName() !!}<br/>
                <label for="">Bank Account Number</label> {!! $bank->accountNumber() !!}<br/>
                <label for="">Bank Branch</label> {!! $bank->branch() !!}<br/>
            @endif
        </div>
    </div>


    <div style="position: absolute; bottom: 40px; left: 0;">

        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($rollover->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>

        <p class="left">REF: CRIMS/{!! strtoupper($rollover->type->name) !!}/{!! $rollover->id !!}</p>
        @if($rollover->filledBy)
            <p>Filled by: {!! $rollover->filledBy->firstname.' '.$rollover->filledBy->lastname !!}</p>
        @endif
    </div>

@endsection