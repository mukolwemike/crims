@extends('reports.letterhead')

@section('content')
    <style>
        div {
            font-family: 'Avenir', 'Open Sans', sans-serif;
        }

        .bordered {
            padding-bottom: 20px;
        }
    </style>
    <div class="bordered">
        <div class="header">
            <h2>{{strtoupper($fund->name)}}</h2>

            <h3>{{strtoupper($app->type->name)}} FORM</h3>
            <h4>Client Details</h4>
        </div>

        <div class="tab-left">
            <label>Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!} <br/>
            <label for="">Phone Number</label> {!! $client->contact->phone !!}<br/>
            <label for="">Client Code</label> {!! $client->client_code !!} <br/>
        </div>

        <h4>Form Details</h4>

        <div class="tab-left">
            @if($app->type->slug == 'transfer')
                <label for="">From</label> {{$data['transfer_from']}} <br>
                <label for="">To</label> {{$data['transfer_to']}} <br>
                <label for="">Date</label> {{$data ['transfer_date']}} <br>
            @endif
            @if($app->type->slug == 'purchase')
                <label for="">Amount</label> {{$data['currency']}} {{$app->amount ? $app->amount : null}} <br>
            @else
                <label for="">Units</label> {{$app->number ? $app->number : null}} <br>
            @endif
            <label for="">Origin</label> {{strtoupper($data['origin'])}} <br>
            @if($data['bank'])
                <label for="">Bank</label> {{$data['bank']}}
            @endif


        </div>
    </div>

    <div style="position: absolute; bottom: 40px; left: 0;">

        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($app->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>

        <p class="left">REF: CRIMS/{{strtoupper($app->type->name)}}/{!! $app->id !!}</p>

        @if($app->user)
            <p>Filled by: {!! $app->user->firstname.' '.$app->user->lastname !!}</p>
        @endif
    </div>

@endsection
