@extends('reports.letterhead')

@section('content')
    <style>
        div{
            font-family: 'Avenir', 'Open Sans', sans-serif;
        }
        .bordered{
            padding-bottom: 20px;
        }
    </style>
    <div class="bordered">
        <div class="header">
            <h2>CYTONN HIGH YIELD SOLUTIONS LLP</h2>

            <h3>CASH TRANSFER FORM</h3>
            <h4>Client Details</h4>
        </div>

        <div class="tab-left">
            <label>Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!} <br/>
            <label for="">Phone Number</label> {!! $client->contact->phone !!}<br/>
            <label for="">Client Code</label> {!! $client->client_code !!} <br/>
        </div>

        <p class="bold-underline" style="padding-left: 30px;"> RE:
            {!! $transaction->custodialAccount->currency->code !!}
            {!! \Cytonn\Presenters\AmountPresenter::currency(abs($transaction->amount), false, 0) !!}
            ({!! strtoupper((new \Cytonn\Lib\Helper())->convertNumberToWords(abs($transaction->amount), false)) !!}) CASH TRANSFER.
        </p>
        <p style="padding-left: 30px;">Kindly transfer {!! $transaction->custodialAccount->currency->code !!}
            {!! \Cytonn\Presenters\AmountPresenter::currency(abs($transaction->amount),  false, 0) !!}
            ({!! ucfirst((new \Cytonn\Lib\Helper())->convertNumberToWords(abs($transaction->amount), false)) !!}) from our {!! $transaction->custodialAccount->alias !!} {!! $transaction->custodialAccount->account_no !!} in favour of
            {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}
            <br/><br/>

            Bank details are as shown below:<br/><br/>
            <span class="bold">Bank Name</span>: {!! $client->investor_bank !!}<br/>
            <span class="bold">Branch Name</span>: {!! $client->investor_bank_branch !!}<br/>
            <span class="bold">Account Name</span>: {!! $client->investor_account_name !!} <br/>
            <span class="bold">Account Number</span>: {!! $client->investor_account_name !!}<br/>
            <span class="bold">Account Number</span>: {!! $client->investor_account_name !!}<br/>
            <span class="bold">Swift Code</span>: {!! $client->investor_swift_code !!}<br/>
            <span class="bold">Clearing Code</span>: {!! $client->investor_clearing_code !!}<br/>
            <br/>

            In case of any query kindly get in touch with us.

            <br/><br/>
            Yours Faithfully,
        </p>

        </br>
        <div class="bold" style="padding-left: 30px;"> For Cytonn Investments Management Ltd</div>
        <br/><br/><br/>

        <span style="display: inline-block; width: 50%; padding-left: 30px;">
            {!! \Cytonn\Presenters\UserPresenter::presentSignOff($first_signatory->id) !!}
        </span>

        <span style="display: inline-block;  width: 50%;">
            {!! \Cytonn\Presenters\UserPresenter::presentSignOff($second_signatory->id) !!}
        </span>
    </div>

    <div style="position: absolute; bottom: 40px; left: 0;">
        <p style="text-align: right">{!! (new Picqer\Barcode\BarcodeGeneratorHTML())->getBarcode($transaction->id, Picqer\Barcode\BarcodeGeneratorHTML::TYPE_CODE_128); !!} </p>
        <p class="left">REF: CRIMS/TRANSFER/{!! $transaction->id !!}</p>
    </div>

@endsection