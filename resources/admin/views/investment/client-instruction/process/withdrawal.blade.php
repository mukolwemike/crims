@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <uib-accordion>
                <uib-accordion-group is-open="true" heading="Investment Details">
                    @include('investment.partials.investmentdetail')
                </uib-accordion-group>
            </uib-accordion>

            <div class="panel panel-default">
                <div class="panel-heading">Withdrawal Details</div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover">
                        <tbody>
                            <tr><td>Withdrawal Type</td><td>{{ ucfirst($type) }}</td></tr>
                            <tr><td>As At Date</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($instruction->due_date) }}</td></tr>
                            <tr><td>Amount</td><td>{{ $currency }} {{ \Cytonn\Presenters\AmountPresenter::currency($amount) }}</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="detail-group group">
                <div class="">
                    <h4> Withdraw Investment</h4>

                    {!! Form::open(['name'=>'form', 'route'=>['investment_withdraw_order', $instruction->id]]) !!}


                    <div class="alert alert-warning col-md-12">
                        <div class="form-group">
                            {!! Form::checkbox('callback', null, false, ['required', 'ng-model'=>'callback', 'id'=>'callback']) !!}

                            {!! Form::label('callback', 'I confirm that I have called the client and confirmed instructions to withdraw') !!}

                            <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'callback') !!}</div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">{!! Form::label('deduct_penalty', 'Deduct Penalty?') !!}</div>
                                    <div class="col-md-8">
                                        {!! Form::select('deduct_penalty', [0 => 'No', 1 => 'Yes'], true, ['init-model'=>'deduct_penalty',  'id'=>'deduct_penalty', 'class' => 'form-control']) !!}
                                    </div>
                                    <div class="col-md-12">{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'deduct_penalty') !!}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">{{ Form::label('description', 'Description (Goes to statement)') }}</div>

                                <div class="col-md-8">{{ Form::text('description', null, ['class'=>'form-control']) }}</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">{{ Form::label('narration', 'Detailed description') }}</div>
                            <div class="col-md-8">{{ Form::textarea('narration', null, ['class'=>'form-control', 'rows'=>2]) }}</div>
                        </div>

                        @if($reason)
                            <div class="row">
                                <div class="col-md-4">{{ Form::label('reason', 'Reason') }}</div>
                                <div class="col-md-8">{{ Form::textarea('reason', $reason, ['class'=>'form-control', 'rows'=>2]) }}</div>
                            </div>
                        @else
                        @endif
                    </div>

                    <!-- Button HTML (to Trigger Modal) -->
                    <a href="#withdrawModal" role="button" class="btn btn-large btn-primary" data-toggle="modal">Confirm</a>

                    <!-- Modal HTML -->
                    <div id="withdrawModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Confirm Withdrawal</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to withdraw this investment?</p>
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <td>Client Code</td>
                                            <td>{!! $investment->client->client_code !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Principal</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Interest at w/date</td>
                                            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($interest) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Value at w/date</td>
                                            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($value) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Withdrawal Type</td>
                                            <td>{{ ucfirst($type) }}</td>
                                        </tr>
                                        <tr>
                                            <td>As At Date</td>
                                            <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($instruction->due_date) }}</td>
                                        </tr>
                                        @if($investment->isSeip())
                                            <tr>
                                                <td>Investment Contributions</td>
                                                <td>{{ $currency }} {{ \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getSeipChildrenInvestedValueAsAtDate($instruction->due_date)) }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td>Amount</td>
                                            <td>{{ $currency }} {{ \Cytonn\Presenters\AmountPresenter::currency($amount) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Withdraw', ['class'=>'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
