@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard" ng-controller="clientTopupCtrl"
             ng-init="commission_recepient = '{!! $recipient_id !!}';
             client_id = '{!! $client->id !!}';
             isParent = '{!! $investmentPaymentSchedule ? 0 : 1 !!}';
             product_id = '{!! $product->id !!}';
             investment_type_id = '{!! $investment ? $investment->type->id : $investment_type_id !!}'">
            <h2>New Investment/Topup</h2>
            <uib-accordion>
                <uib-accordion-group is-open="true" heading="Client Details">
                    <table class="table table-responsive table-hover">
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{{ \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) }}</td>
                            </tr>
                            <tr>
                                <td>Client Code</td>
                                <td>{{ $client->client_code ? $client->client_code : "Not assigned" }}</td>
                            </tr>
                            <tr><td></td><td><a href="/dashboard/clients/details/{{ $client->id }}" target="_blank"><i class="fa fa-long-arrow-right"></i> See details</a></td></tr>
                        </tbody>
                    </table>
                </uib-accordion-group>
                <uib-accordion-group heading="Existing Investment">
                    @if($investment)
                        @include('investment.partials.investmentdetail')
                    @else
                        <p>Client has no existing investment.</p>
                    @endif
                </uib-accordion-group>
            </uib-accordion>

            <div class="panel panel-default">
                <div class="panel-heading">Investment Details</div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover">
                        <tbody>
                            <tr><td>Product</td><td>{{ $product->name }}</td></tr>
                            <tr><td>Amount </td><td> {{ \Cytonn\Presenters\AmountPresenter::currency($details['amount']) }}</td></tr>
                            <tr><td>Interest rate </td><td> {{ $details['interest_rate'] }}</td></tr>
                            <tr><td>Tenor</td><td> {{ $form->tenor }} Months <i class="fa fa-long-arrow-right"></i> {{ \Cytonn\Presenters\DatePresenter::formatDate($details['invested_date']) }} to {{ \Cytonn\Presenters\DatePresenter::formatDate($details['maturity_date']) }}</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>


            {!! Form::model((object)$details, ['name'=>'form', 'route' => ['investment_topup_order', $form->id]]) !!}

            @include('investment.client-instruction.process.partials.new_investment_form')

            <div class = "col-md-12">
                <div class = "detail-group">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#processTopupModal" ng-click="confirmModal($event)">
                        Confirm
                    </button>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="processTopupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Top up/Investment</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-responsive table-hover">
                                <tbody>
                                    <tr><td>Product</td><td>{{ $product->name }}</td></tr>
                                    <tr><td>Amount </td><td> {{ \Cytonn\Presenters\AmountPresenter::currency($details['amount']) }}</td></tr>
                                    <tr><td>Interest rate </td><td><% interest_rate %> %</td></tr>
                                    <tr><td>Tenor</td><td> {{ $form->tenor }} Months <i class="fa fa-long-arrow-right"></i> <% invested_date | date %> to {{ \Cytonn\Presenters\DatePresenter::formatDate($details['maturity_date']) }}</td></tr>
                                    <tr><td>Trans. Description</td><td> <% description %></td></tr>
                                    <tr><td>Financial Advisor</td><td> <% commissionRecipient %> </td></tr>
                                    <tr><td>Comm. Rate</td><td> <% commission_rate %>%</td></tr>
                                    <tr><td>Comm. Rate Name</td><td> <% commission_rate_name %></td></tr>
                                    <tr><td>Comm. Start Date</td><td><% commission_start_date | date %></td></tr>
                                    <tr ng-if="interest_payment"><td>Interest Payment</td><td><% interest_payment %> on date <% interest_payment_date %></td></tr>
                                    <tr ng-if="contribution_amount"><td>Monthly Contribution amount</td><td><% contribution_amount %></td></tr>
                                    <tr ng-if="monthlyContribution"><td>Monthly contribution interval</td><td><% monthlyContribution %> on date <% contribution_payment_date %></td></tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!! Form::submit('Topup', ['class'=>'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        </div>


    </div>
@stop