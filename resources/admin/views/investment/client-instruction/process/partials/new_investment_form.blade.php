<div class = "row">
    <div class = "col-md-6">
        <div class = "detail-group">
            <h4>Investment details</h4>

            <div class = "form-group">
                {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}
                {!! Form::number('interest_rate', NULL, ['class'=>'form-control', 'step'=>'0.01','init-model'=>'interest_rate', 'required']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
            </div>

            <div class = "form-group">
                {!! Form::label('invested_date', 'Value Date') !!}
                {!! Form::text('invested_date', NULL, ['id'=>'invested_date' , 'class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened_inv", 'ng-focus'=>'status.opened_inv = ! status.opened_inv']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
            </div>

            <div class = "form-group">
                {!! Form::label('maturity_date', 'Maturity Date') !!}
                {!! Form::text('maturity_date', NULL, ['id'=>'maturity_date' , 'class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened_mat", 'ng-focus'=>'status.opened_mat = ! status.opened_mat']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'maturity_date') !!}
            </div>

            <div class = "form-group">
                {!! Form::label('description', 'Transaction Description') !!}
                {!! Form::textarea('description', null, ['init-model'=>'description','class'=>'form-control', 'rows'=>2, 'required']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
            </div>

            @if(isset($details['reason']))
                <div class="form-group">
                    {!! Form::label('reason for processing before signatures are complete', 'Transction Reason') !!}

                    {!! Form::textarea('reason', null, ['init-model'=>'description','class'=>'form-control', 'rows'=>2, 'required']) !!}
                </div>
            @else
            @endif
        </div>

        @if($product->present()->isSeip && is_null($investmentPaymentSchedule))
            <div class="detail-group">
                <h4>Monthly Contribution</h4>
                <div class="col-md-12">
                    <div class = "form-group">
                        {!! Form::label('contribution_amount', 'Monthly Contribution Amount') !!}
                        {!! Form::text('contribution_amount', $investment && $investment->investmentPaymentScheduleDetail ? $investment->investmentPaymentScheduleDetail->amount : NULL, ['id'=>'contribution_amount' , 'class'=>'form-control', 'id'=>'contribution_amount', 'init-model'=>'contribution_amount']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contribution_amount') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('contribution_payment_interval', 'Monthly contribution interval') !!}
                        {!! Form::select('contribution_payment_interval', $interest_intervals, $investment && $investment->investmentPaymentScheduleDetail ? $investment->investmentPaymentScheduleDetail->payment_interval : 1, ['class'=>'form-control', 'id'=>'contribution_payment_interval', 'init-model'=>'contribution_payment_interval']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contribution_payment_interval') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('contribution_payment_date', 'Preferred payment date (1 - 31)') !!}
                        {!! Form::number('contribution_payment_date', $investment && $investment->investmentPaymentScheduleDetail ? $investment->investmentPaymentScheduleDetail->payment_date : 31, ['class'=>'form-control', 'init-model'=>'contribution_payment_date']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contribution_payment_date') !!}
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class = "col-md-6">
        <div class = "detail-group">
            <h4>Commission</h4>
            <div class = "row">
                <div class = "col-md-6">
                    <div class = "form-group">
                        {!! Form::label('commission_recepient', 'Commission paid to') !!}
                        {!! Form::select('commission_recepient', $recipients, $recipient_id,  ['id'=>'commission_recepient','class'=>'form-control', 'ui-select2', 'init-model'=>'commission_recepient', 'required']) !!}
                    </div>
                </div>

                <div class = "col-md-6">
                    <div class = "form-group">
                        {!! Form::label('commission_start_date', 'Commission Start Date') !!}
                        {!! Form::text('commission_start_date', NULL, ['id'=>'commission_start_date' , 'class'=>'form-control', 'datepicker-popup init-model'=>"commission_start_date", 'is-open'=>"status.opened_2", 'ng-focus'=>'status.opened_2 = ! status.opened_2']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_start_date') !!}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group alert alert-info">
                        {!! Form::label('commission_rate', 'Commission Rate') !!} :- <i class="fa fa-spinner fa-spin" ng-show="loadingRate === true"></i> {!! Form::label('commission_rate', '<% commission_rate_name %>') !!}
                    </div>
                </div>

                <div class = "col-md-6" ng-show="edit_rate">
                    <div class = "form-group">
                        <br />
                        {!! Form::label('commission_rate', 'Commission Rate') !!}
                        {!! Form::number('commission_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'data-ng-value' => 'commission_rate', 'required', 'ng-model' => 'commission_rate']) !!}
                        {!! Form::hidden('commission_rate_name', NULL, [ 'data-ng-value' => 'commission_rate_name', 'ng-model' => 'commission_rate_name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <br />
                        <br />
                        {!! Form::checkbox('edit', true, true, ['ng-model'=>'edit_rate']) !!} {!! Form::label('edit', "Edit Commission Rate") !!}
                    </div>
                </div>
            </div>
        </div>

        <div class = "detail-group">
            <h4>Interest payment</h4>
            <div class = "col-md-6">
                <div class = "form-group">
                    {!! Form::label('interest_payment_interval', 'Interest payment interval') !!}

                    {!! Form::select('interest_payment_interval', $interest_intervals, $investment && ! $product->present()->isSeip ? $investment->interest_payment_interval : null, ['class'=>'form-control', 'id'=>'interest_payment_interval', 'init-model'=>'interest_payment_interval']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_interval') !!}
                </div>
            </div>
            <div class = "col-md-6">
                <div class = "form-group">
                    {!! Form::label('interest_payment_date', 'Preferred payment date (1 - 31)') !!}

                    {!! Form::number('interest_payment_date', $investment ? $investment->interest_payment_date : 31, ['class'=>'form-control', 'init-model'=>'interest_payment_date']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_payment_date') !!}
                </div>
            </div>
            <div class = "col-md-12">
                <div class = "alert alert-info"><p>When date is entered as 31st it will automatically be converted to end of month</p>
                </div>

                <div class = "form-group" ng-show="interest_payment_interval != 0">
                    {!! Form::label('interest_action_id', 'Select Interest Action') !!}
                    {!! Form::select('interest_action_id', $interestActions, NULL, ['class'=>'form-control', 'id'=>'interest_action_id', 'init-model'=>'interest_action_id']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_action_id') !!}
                </div>
            </div>
            <div ng-show="interest_action_id == 2" class = "col-md-12">
                <div class = "alert alert-info"><p>When no reinvest tenor is entered the interest will be reivested upto the maturity date of the investment</p>
                </div>
                <div class = "form-group" ng-show="interest_action_id == 2">
                    {!! Form::label('interest_reinvest_tenor', 'Interest Reinvest Tenor (months)') !!}
                    {!! Form::number('interest_reinvest_tenor', null, ['class' => 'form-control' , 'init-model' => 'interest_reinvest_tenor', 'id' => 'interest_reinvest_tenor'] ) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_reinvest_tenor') !!}
                </div>
            </div>
        </div>

    </div>
</div>