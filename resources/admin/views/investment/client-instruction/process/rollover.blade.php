@extends('layouts.default')

@section('content')
<div class="col-md-12">
    <div class="panel-dashboard" ng-controller="clientTopupCtrl"
         ng-init="setClientId('{!! $investment->client->id !!}');
             commission_recipient='{!! $recipient_id !!}';
             client_id = '{!! $client->id !!}';
             product_id='{!! $investment->product_id !!}';
             isParent = '{!! $investmentPaymentSchedule ? 0 : 1 !!}';
             investment_type_id = '{!! $investment_type_id !!}'">
        <uib-accordion>
            <uib-accordion-group heading="Investment Details">
                @include('investment.partials.investmentdetail')
            </uib-accordion-group>
        </uib-accordion>

        <div class="panel panel-default">
            <div class="panel-heading">Rollover Details</div>
            <div class="panel-body">
                <table class="table table-responsive table-hover">
                    <thead>
                        <tr><th>Principal</th><th>Rate</th>
                            <th>Value Date</th><th>Maturity Date</th>
                            <th>Rollover Date</th>
                            <th>Net Interest</th><th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($investments as $inv)
                        <tr>
                            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($inv->amount) }}</td>
                            <td>{{ $inv->interest_rate }}%</td>
                            <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($inv->invested_date) }}</td>
                            <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($inv->maturity_date) }}</td>
                            <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($instruction->due_date) }}</td>
                            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($inv->net_interest) }}</td>
                            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($inv->total_value) }}</td>
                        </tr>
                    @endforeach
                    @if($instruction->topup_amount > 0)
                        <tr>
                            <td>Topup</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($instruction->topup_amount) }}</td>
                        </tr>
                    @endif
                    <?php $seipChildrenTotal = 0 ?>
                    @if($investment->repo->isSeipParent())
                        <?php $seipChildrenTotal = $investment->repo->getSeipChildrenInvestedValueAsAtDate($instruction->due_date) ?>
                        <tr>
                            <td>Investment Contributions</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($seipChildrenTotal) }}</td>
                        </tr>
                    @endif
                    <tr>
                        <th>Total</th><td colspan="5"></td><th>{{ \Cytonn\Presenters\AmountPresenter::currency($total = $instruction->topup_amount + $investments->sum('total_value') + $seipChildrenTotal) }}</th>
                    </tr>
                    <tr>
                        <td>Rollover</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::accounting($r = -1 * $amount) }}</td>
                    </tr>
                    <tr>
                        <td>Withdraw</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::accounting(-1 * ($total + $r)) }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Investment Details</div>
            <div class="panel-body">
                <table class="table table-responsive table-hover">
                    <tbody>
                    <tr><td>Product</td><td>{{ $investment->product->name }}</td></tr>
                    <tr><td>Amount </td><td> {{ \Cytonn\Presenters\AmountPresenter::currency(-$r) }}</td></tr>
                    <tr><td>Interest rate </td><td> {{ $instruction->agreed_rate }}%</td></tr>
                    <tr><td>Tenor</td><td> {{ $instruction->tenor }} Months <i class="fa fa-long-arrow-right"></i> {{ \Cytonn\Presenters\DatePresenter::formatDate($invested_date) }} to {{ \Cytonn\Presenters\DatePresenter::formatDate($maturity_date) }}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="detail-group group">
            <div class="">
                <h4>Rollover</h4>

                {{ Form::model((object)$details, ['route' => ['investment_rollover_order', $instruction->id]]) }}

                    @include('investment.client-instruction.process.partials.new_investment_form')

                    <!-- Button HTML (to Trigger Modal) -->
                    <a href="#rolloverModal" role="button" class="btn btn-large btn-primary" data-toggle="modal" data-ng-click="confirmModal()">Confirm Rollover</a>

                    <!-- Modal HTML -->
                    <div id="rolloverModal" class="modal fade">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Rollover Investment</h4>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-responsive table-hover">
                                        <tbody>
                                        <tr><td>Product</td><td>{{ $investment->product->name }}</td></tr>
                                        <tr><td>Total</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($total) }}</td></tr>
                                        <tr><td>Rollover</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency(-$r) }}</td></tr>
                                        <tr><td>Withdraw</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($total + $r) }}</td></tr>
                                        <tr><td>Interest rate </td><td><% interest_rate %> %</td></tr>
                                        <tr><td>Tenor</td><td> {{ $instruction->tenor }} Months <i class="fa fa-long-arrow-right"></i> <% invested_date | date %> to {{ \Cytonn\Presenters\DatePresenter::formatDate($maturity_date) }}</td></tr>
                                        <tr><td>Trans. Description</td><td> <% description %></td></tr>
                                        <tr><td>Financial Advisor</td><td> <% commissionRecipient %> </td></tr>
                                        <tr><td>Comm. Rate</td><td> <% commission_rate %>%</td></tr>
                                        <tr><td>Comm. Rate Name</td><td> <% commission_rate_name %></td></tr>
                                        <tr><td>Comm. Start Date</td><td><% commission_start_date | date %></td></tr>
                                        <tr ng-if="interest_payment"><td>Interest Payment</td><td><% interest_payment %> on date <% interest_payment_date %></td></tr>
                                        <tr ng-if="contribution_amount"><td>Monthly Contribution amount</td><td><% contribution_amount %></td></tr>
                                        <tr ng-if="monthlyContribution"><td>Monthly contribution interval</td><td><% monthlyContribution %> on date <% contribution_payment_date %></td></tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Rollover', ['class'=>'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop
