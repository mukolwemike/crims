@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">

        <ul id="myTabs" class="nav nav-tabs" role="tablist">
            <li role="presentation" class=""><a href="#application" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Applications</a></li>
            <li role="presentation" class=""><a href="#topup" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Investments & Topups</a></li>
            <li role="presentation" class=""><a href="#unitFund" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Unit Fund Investments</a></li>
            <li role="presentation" class=""><a href="#rollover" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Rollovers & Withdrawals</a></li>
            <li role="presentation" class=""><a href="#billing" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Utility Payments</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="application" aria-labelledby="home-tab">
                @include('investment.client-instruction.partials.application_tab')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="topup" aria-labelledby="home-tab">
                @include('investment.client-instruction.partials.topup_tab')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="unitFund" aria-labelledby="home-tab">
                @include('investment.client-instruction.partials.unit_fund_applications')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="rollover" aria-labelledby="home-tab">
                @include('investment.client-instruction.partials.rollover_tab')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="billing" aria-labelledby="home-tab">
                @include('investment.client-instruction.partials.utility_payment_tab')
            </div>
        </div>
    </div>
@endsection