@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div class="col-md-6">
                @if($client)
                    @include('clients.clientdetails')
                @endif
            </div>
            <div class="col-md-6">
                <div>
                    <div class="pull-right">
                        <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <hr>
                <div class="row">
                    <invest-fund :instruction="{{ json_encode($instruction) }}" :client="{{ json_encode($client) }}"></invest-fund>
                </div>
            </div>
        </div>
    </div>
@endsection