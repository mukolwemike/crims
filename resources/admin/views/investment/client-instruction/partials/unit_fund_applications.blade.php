<div ng-controller="UnitFundInvestmentInstruction">
    <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-hover">
        <thead>
            <tr>
                <th colspan = "3">
                    <a class="btn btn-success margin-bottom-20" href="/dashboard/investment/client-instruction/purchase-units"><i class="fa fa-plus"></i> Create Purchase Instruction</a>
                </th>
                <th colspan="2">
                    <select st-search="origin" class="form-control">
                        <option value="">Platform Originated</option>
                        <option value="admin">Admin</option>
                        <option value="mpesa">Mpesa</option>
                        <option value="web">Web</option>
                        <option value="mobile">Mobile</option>
                        <option value="ussd">USSD</option>
                        <option value="client">CLIENT</option>
                    </select>
                </th>
                <th colspan="2">
                    <select st-search="types" class="form-control">
                        <option value="">All Types</option>
                        <option value="1">Purchase</option>
                        <option value="2">Sale</option>
                        <option value="3">Transfer</option>
                    </select>
                </th>
                <th colspan="2">
                    <select st-search="invested" class="form-control">
                        <option value="">All Forms</option>
                        <option value="1">Not Invested</option>
                        <option value="2">Invested</option>
                        <option value="3">Cancelled</option>
                    </select>
                </th>
                <th colspan="2">
                    <select st-search="unit_fund" class="form-control">
                        <option value="">All Unit Funds</option>
                        @foreach($unitFunds as $fund)
                            <option value="{!! $fund->id !!}">{!! $fund->name !!}</option>
                        @endforeach
                    </select>
                </th>
                <th colspan = "2"><input st-search="" class = "form-control" placeholder = "Search..." type = "text"/></th>
            </tr>
            <tr>
                <th>Client Code</th>
                <th colspan="3">Client Name</th>
                <th>Amount</th>
                <th>Number</th>
                <th colspan="2">Unit Fund</th>
                <th>Platform</th>
                <th>Instruction Type</th>
                <th>Sent On</th>
                <th>Invested</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody ng-show="!isLoading">
            <tr ng-repeat="row in displayed">
                <td><% row.client_code %></td>
                <td colspan="3"><% row.client_name %></td>
                <td><% row.currency %> <% row.amount | currency:"" %></td>
                <td><% row.number%>&nbsp;units</td>
                <td colspan="2"><% row.unit_fund %></td>
                <td><span to-html = "row.origin | instructionPlatformOrigin"></span></td>
                <td colspan=""><el-tag><% row.type %></el-tag></td>
                <td><time am-time-ago="row.created_at"></time></td>
                <td><span to-html = "row.processed | boolUnitFundInvestedStatus"></span></td>
                <td>
                    <a uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/unit_fund_instruction/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                    <a ng-show="row.processesed == false" uib-popover="Edit details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/unit_fund_instruction/<% row.id %>/edit"><i class="fa fa-edit"></i></a>
                </td>
            </tr>
        </tbody>
        <tbody ng-show="isLoading">
            <tr>
                <td colspan="11" class="text-center">Loading ... </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="text-center" st-pagination="" st-items-by-page="10" colspan="11"></td>
            </tr>
        </tfoot>
    </table>
</div>
