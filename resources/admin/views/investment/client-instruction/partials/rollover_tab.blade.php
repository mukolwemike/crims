<div ng-controller="ClientInstructionRolloverCtrl">
    <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-hover">
        <thead>
        <tr>
            <th colspan = "4">
                <a href="/dashboard/investments/clientinvestments" class="btn btn-success margin-bottom-20"><i class="fa fa-plus"></i> WITHDRAW/ ROLLOVER</a>
            </th>
            <th colspan="2">
                <select st-search="invested" class="form-control">
                    <option value="">Executed/Not Executed</option>
                    <option value="1">Executed</option>
                    <option value="0">Not Executed</option>
                </select>
            </th>
            <th colspan="2">
                <select st-search="cancelled" class="form-control">
                    <option value="">Cancelled/Not Cancelled</option>
                    <option value="1">Cancelled</option>
                    <option value="0">Not Cancelled</option>
                </select>
            </th>
            <th colspan="2">
                <select st-search="origin" class="form-control">
                    <option value="">Platform Originated</option>
                    <option value="admin">Admin</option>
                    <option value="mpesa">Mpesa</option>
                    <option value="web">Web</option>
                    <option value="mobile">Mobile</option>
                    <option value="ussd">USSD</option>
                    <option value="client">CLIENT</option>
                </select>
            </th>
            <th colspan = "3"><input st-search="" class = "form-control" placeholder = "Search..." type = "text"/></th>
        </tr>
        <tr>
            <th>ID</th>
            <th>Code</th>
            <th>Client Name</th>
            <th>Investment Amount</th>
            <th>Maturity Date</th>
            <th>Effective Date</th>
            <th>Type</th>
            <th colspan="2">Amount Affected</th>
            <th st-sort-default="reverse" st-sort="created_at">Sent On</th>
            <th>Origin</th>
            <th colspan="2">Status</th>
            <th colspan="2">Action</th>
        </tr>
        </thead>
        <tbody ng-show="!isLoading">
        <tr ng-repeat="row in displayed">
            <td><% row.id  %></td>
            <td><% row.client_code %></td>
            <td><% row.client_name %></td>
            <td><% row.currency %> <% row.investment_amount | currency:"" %></td>
            <td><% row.maturity_date | date %></td>
            <td><% row.due_date | date %></td>
            <td><% row.instruction_type %></td>
            <td><% row.amount_select %></td>
            <td><% row.currency %> <% row.amount | currency:""%></td>
            <td><% row.created_at | date:'medium' %></td>
            <td><span to-html = "row.origin | instructionPlatformOrigin"></span></td>
            <td><span to-html = "row.invested | asLabel:'Executed':'Not Executed'"></span></td>
            <td><span to-html = "row.inactive | boolInactiveStatus"></span></td>
            <td>
                <a uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/rollover/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                <a uib-popover="Edit details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/rollover-withdraw/<% row.instruction_type %>/<% row.id %>/edit"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
        </tbody>
        <tbody ng-show="isLoading">
        <tr>
            <td colspan="6" class="text-center">Loading ... </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            {{--<div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>--}}
            <td class="text-center" st-pagination="" st-items-by-page="10" colspan="10"></td>
        </tr>
        </tfoot>
    </table>
</div>
