    <div ng-controller="ClientFilledInvestmentApplicationCtrl">
    <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
        <thead>
        <tr>
            <th colspan="3">
                {{--<a href="/dashboard/investment/client-instruction/apply" class="btn btn-success"><i class="fa fa-plus"></i> APPLY</a>--}}
                <a href="/dashboard/investment/client/apply" class="btn btn-success"> New application </a>
            </th>
            <th colspan="3"></th>
            <th>
                <select st-search="used" class="form-control">
                    <option selected value="">All Applications</option>
                    <option value="1">Used</option>
                    <option value="0">Not Used</option>
                </select>
            </th>
            <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
        </tr>
        <tr>
            <th>Name</th>
            {{--<th>email</th>--}}
            <th>Email Indemnity</th>
            <th>Currency</th>
            <th>Amount</th>
            <th>Product</th>
            <th st-sort="created_at">Created</th>
            <th st-sort-default="reverse" st-sort="updated_at">Updated</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody  ng-show="!isLoading">
        <tr ng-repeat="row in displayed">
            <td><% row.name  %></td>
            {{--<td><% row.email  %></td>--}}
            <td><% row.email_indemnity | yesno %></td>
            <td><% row.currency_name %></td>
            <td><% row.amount | currency:""  %></td>
            <td><% row.product_name %></td>
            <td><% row.created_at | date:'medium' %></td>
            <td><% row.updated_at | date:'medium' %></td>
            <td><span to-html = "row.used | clientApplicationStatus"></span></td>
            <td>
                <a ng-show="row.used == false" ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/application/<%row.id%>"><i class="fa fa-list-alt"></i></a>

                <a ng-show="row.used == true" ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/applications/<%row.id%>"><i class="fa fa-list-alt"></i></a>

                {{--<a ng-show="row.used == false" ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/application/<%row.id%>/edit"><i class="fa fa-edit"></i></a>--}}
                <a ng-show="row.used == false" ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investment/client/application/<%row.id%>/edit"><i class="fa fa-edit"></i></a>

                <a ng-show="row.used == true && row.approved == false" ng-controller = "PopoverCtrl" uib-popover = "Edit details" popover-trigger = "mouseenter" href = "/dashboard/investments/applications/edit/<%row.id%>"><i class = "fa fa-edit"></i></a>
            </td>
        </tr>
        </tbody>
        <tbody ng-show="isLoading">
        <tr>
            <td colspan="10" class="text-center">Loading ... </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan = "2" class = "text-center">
                Items per page
            </td>
            <td colspan = "4" class = "text-center">
                <input type = "text" ng-model = "itemsByPage"/>
            </td>
            <td colspan = "4" class = "text-center">
                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
            </td>
            <td colspan="1"></td>
            {{--<td class="text-center" st-pagination="" st-items-by-page="10" colspan="6"></td>--}}
        </tr>
        </tfoot>
    </table>
</div>