<div ng-controller="ClientInstructionTopupCtrl">
    <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-hover">
        <thead>
        <tr>
            <th colspan = "3">
                <a href="/dashboard/investment/client-instruction/top-up" class="btn btn-success margin-bottom-20"><i class="fa fa-plus"></i> TOP UP</a>
            </th>
            <th colspan="2">
                <select st-search="invested" class="form-control">
                    <option value="">All Forms</option>
                    <option value="1">Invested</option>
                    <option value="0">Not Invested</option>
                </select>
            </th>
            <th colspan="2">
                <select st-search="cancelled" class="form-control">
                    <option value="">All Forms</option>
                    <option value="1">Cancelled</option>
                    <option value="0">Not Cancelled</option>
                </select>
            </th>
            <th colspan="2">
                <select st-search="origin" class="form-control">
                    <option value="">Platform Originated</option>
                    <option value="admin">Admin</option>
                    <option value="mpesa">Mpesa</option>
                    <option value="web">Web</option>
                    <option value="mobile">Mobile</option>
                    <option value="ussd">USSD</option>
                    <option value="client">Client</option>
                </select>
            </th>
            <th colspan = "2"><input st-search="" class = "form-control" placeholder = "Search..." type = "text"/></th>
        </tr>
        <tr>
            <th>ID</th><th>Client Code</th><th>Client Name</th><th>Investment Amount</th><th>Product</th>
            <th>Agreed Rate</th><th>Tenor</th><th>Sent On</th><th>Platform</th><th>Client Approval</th><th>Invested</th><th>Cancelled</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody ng-show="!isLoading">
        <tr ng-repeat="row in displayed">
            <td><% row.id %></td>
            <td><% row.client_code %></td>
            <td><% row.client_name %></td>
            <td><% row.currency %> <% row.amount | currency:"" %></td>
            <td><% row.product_name %></td>
            <td><% row.agreed_rate | currency:"" %>%</td>
            <td><% row.tenor %> Months</td>
            <td><time am-time-ago="row.created_at"></time></td>
            <td><span to-html = "row.origin | instructionPlatformOrigin"></span></td>
            <td><span to-html = "row.approved | boolClientApprovalStatus"></span></td>
            <td><span to-html = "row.invested | boolInvestedStatus"></span></td>
            <td><span to-html = "row.inactive | boolInactiveStatus"></span></td>
            <td>
                <a uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/topup/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                <a uib-popover="Edit details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/topup/<% row.id %>/edit"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
        </tbody>
        <tbody ng-show="isLoading">
        <tr>
            <td colspan="11" class="text-center">Loading ... </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            {{--<div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>--}}
            <td class="text-center" st-pagination="" st-items-by-page="10" colspan="11"></td>
        </tr>
        </tfoot>
    </table>
</div>
