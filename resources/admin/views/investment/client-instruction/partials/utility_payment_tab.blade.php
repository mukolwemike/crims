<div ng-controller="UtilityBillingInstructions">
    <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-hover">
        <thead>
            <tr>
                <th colspan = "6">
                    <button type="button" class="btn btn-success margin-bottom-20" data-toggle="modal" data-target="#exportModal">
                        <i class="fa fa-save"></i> Export
                    </button>
                </th>
                <th colspan="2">
                    <select st-search="types" class="form-control">
                        <option value="">All Types</option>
                        <option value="1">Bill</option>
                        <option value="2">Airtime</option>
                    </select>
                </th>
                <th colspan="2">
                    <select st-search="status" class="form-control">
                        <option value="">All Status</option>
                        <option value="1">Pending</option>
                        <option value="2">Submitted</option>
                        <option value="3">Completed</option>
                        <option value="4">Cancelled</option>
                    </select>
                </th>
                <th colspan="2">
                    <select st-search="origin" class="form-control">
                        <option value="">Platform Originated</option>
                        <option value="admin">Admin</option>
                        <option value="mpesa">Mpesa</option>
                        <option value="web">Web</option>
                        <option value="mobile">Mobile</option>
                        <option value="ussd">USSD</option>
                        <option value="client">Client</option>
                    </select>
                </th>
{{--                <th colspan="2">--}}
{{--                    <select st-search="unit_fund" class="form-control">--}}
{{--                        <option value="">All Unit Funds</option>--}}
{{--                        @foreach($unitFunds as $fund)--}}
{{--                            <option value="{!! $fund->id !!}">{!! $fund->name !!}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </th>--}}
                <th colspan = "2">
                    <input st-search="" class = "form-control" placeholder = "Search..." type = "text"/>
                </th>
            </tr>
            <tr>
                <th>Client Code</th>
                <th colspan="3">Client Name</th>
                <th>Amount</th>
                <th>Number</th>
{{--                <th colspan="2">Source Fund</th>--}}
                <th>To</th>
                <th>Platform</th>
                <th>Utility Type</th>
                <th>Date</th>
                <th>Sent On</th>
                <th>Origin</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody ng-show="!isLoading">
            <tr ng-repeat="row in displayed">
                <td><% row.client_code %></td>
                <td colspan="3"><% row.client_name %></td>
                <td><% row.currency %> <% row.amount | currency:"" %></td>
                <td><% row.number | currency:""%></td>
{{--                <td colspan="2"><% row.unit_fund %></td>--}}
                <td><% row.to%></td>
                <td><span to-html = "row.origin | instructionPlatformOrigin"></span></td>
                <td colspan=""><el-tag><% row.type %></el-tag></td>
                <td><% row.date%></td>
                <td><time am-time-ago="row.created_at"></time></td>
                <td><span to-html = "row.processed | boolUtilityBillingStatus"></span></td>
                <td>
                    <a uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/client-instructions/utility_billing_instruction/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                </td>
            </tr>
        </tbody>
        <tbody ng-show="isLoading">
            <tr>
                <td colspan="11" class="text-center">Loading ... </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="text-center" st-pagination="" st-items-by-page="10" colspan="11"></td>
            </tr>
        </tfoot>
    </table>


    <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_utility_instructions_summary_report']]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Utility Instructions Summary</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Choose the start date and the end date for the report you wish to export</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
