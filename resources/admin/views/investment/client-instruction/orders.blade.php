@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div>
            <small>Orders for the last 30 days shown</small>
            <a href="/dashboard/investments/client-instructions" class="btn btn-info pull-right">All Orders <i class="fa fa-long-arrow-right"></i></a>
        </div>
        <br>
        <hr>
        <div>
            <investment-orders></investment-orders>
        </div>
    </div>
@endsection