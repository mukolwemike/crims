@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Client Payment Details</h3>
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        @if($client->id)
                            <td>{!! $client->client_code !!}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Client</td>
                        @if($client->id)
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                        @else
                            <td>{!! $payment->custodialTransaction->received_from !!} <small>(Not on-boarded yet)</small></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Category</td>
                        @if($payment->product_id)
                            <td>Product :
                                {!! $payment->product->name !!}
                            </td>
                        @elseif($payment->project_id)
                            <td>Project :
                                {!! $payment->project->name !!}
                            </td>
                        @endif
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
                    </tr>
                    <tr style="background-color: yellow; color: red;">
                        <th>Balance</th>
                        @if($payment->product_id)
                            <th>Product :
                                {!! \Cytonn\Presenters\AmountPresenter::currency($payment->balance($client, $payment->product)) !!}
                            </th>
                        @elseif($payment->project_id)
                            <th>Project :
                                {!! \Cytonn\Presenters\AmountPresenter::currency($payment->balance($client, null, $payment->project)) !!}
                            </th>
                        @endif
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $payment->description !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
                    </tr>
                    @if($transaction)
                        <tr>
                            <td>Bank Account</td> <td>{{ $transaction->custodialAccount->account_name }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>FA</td>
                        @if($payment->recipient)
                            <td>{!! $payment->recipient->name !!}</td>
                        @else
                            <td>No FA specified</td>
                        @endif
                    </tr>
                </tbody>
            </table>

            @if(isset($payment->type->slug))
                @if($payment->type->slug == 'TO')
                <div class="btn-group">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#reverseModal"><i
                                class="fa fa-remove"></i> Reverse
                    </button>
                </div>
                @endif
            @endif

            @if($transfers->count())
                <h4>Client Payment Transfers</h4>
                <table class="table table-hover">
                    <thead>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Instruction</th>
                    </thead>
                    <tbody>
                    @foreach($transfers as $transfer)
                        <tr>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($transfer->amount)) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transfer->date) !!}</td>
                            <td>{!! $transfer->description !!}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#generate-instruction-{!! $transfer->id !!}"><i class="fa fa-download"></i></a>
                                <div class="modal fade" id="generate-instruction-{!! $transfer->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::model($transfer, ['route'=>['generate_instruction', $transfer->id]]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="myModalLabel">Generate Instruction</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-4">{!! Form::label('first_signatory_id', 'First Signatory') !!}</div>
                                                        <div class="col-md-8">{!! Form::select('first_signatory_id', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-4">{!! Form::label('second_signatory_id', 'Second Signatory') !!}</div>
                                                        <div class="col-md-8">{!! Form::select('second_signatory_id', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::submit('Generate Instruction', ['class'=>'btn btn-success']) !!}
                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif

            {{--<div class="col-md-12">--}}
                {{--<div class="btn-group">--}}
                    {{--@if($client->id)--}}
                        {{--<button class="btn btn-default" ng-click="transfer = !transfer">Transfer Cash to Client</button>--}}
                    {{--@else--}}
                        {{--<button class="btn btn-default" disabled>Transfer Cash to Client</button>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

    <div class="modal fade" id="reverseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['reverse_cash_transfer', $payment->id], 'method'=>'delete']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Reverse Cash Transfer</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>Description: {!! $payment->description !!}</p>
                        <p>Amount: {!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount)) !!}</p>
                        <p>Date: {!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('reason', 'Why do you need to reverse?') !!}
                        {!! Form::text('reason', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Reverse Transfer', ['class'=>'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--<div class="col-md-10 col-md-offset-1" ng-show="transfer">--}}
        {{--<div class="panel-dashboard">--}}
            {{--<h4>Transfer Cash to Client</h4>--}}
            {{--{!! Form::open(['route'=>['transfer_cash_to_client', $client->id, $payment->id, $payment->custodial_transaction_id]]) !!}--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-4">{!! Form::label('amount', 'Amount to Transfer') !!}</div>--}}
                        {{--@if($payment->project_id)--}}
                            {{--<div class="col-md-8">{!! Form::number('amount', $payment->balance($client, null, $payment->project), ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>--}}
                        {{--@elseif($payment->product_id)--}}
                            {{--<div class="col-md-8">{!! Form::number('amount', $payment->balance($client, $payment->product, null), ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class = "form-group"  ng-controller = "DatepickerCtrl">--}}
                        {{--<div class="col-md-4">{!! Form::label('date', 'Date') !!}</div>--}}
                        {{--<div class="col-md-8">{!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-4">{!! Form::label('first_signatory_id', 'First Signatory') !!}</div>--}}
                        {{--<div class="col-md-8">{!! Form::select('first_signatory_id', User::all()->lists('full_name', 'id'), null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-4">{!! Form::label('second_signatory_id', 'Second Signatory') !!}</div>--}}
                        {{--<div class="col-md-8">{!! Form::select('second_signatory_id', User::all()->lists('full_name', 'id'), null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="form-group">--}}
                        {{--{!! Form::submit('Transfer Cash', ['class'=>'btn btn-success']) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--{!! Form::close() !!}--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
