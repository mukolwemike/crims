<div class="col-md-12">
    <div class="panel-dashboard">
        <!-- Client Details -->
        @include('investment.payments.partials.client_details')

                <!-- Balance Details -->
        @include('investment.payments.partials.balance_details')
    </div>
</div>

<div class="col-md-12" ng-controller="AddAccountInflowController">
    <div class="panel-dashboard">
        <h4>Transfer Cash to Client</h4>
        {!! Form::open(['route'=>['transfer_cash_to_another_client', $client->id]]) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                Source &amp; Cash Details
            </div>
            <div class="panel-body">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('balance_type_id', 'Product/Project/Share Entity/Unit Fund') !!}
                        {!! Form::select('balance_type_id', $balances->lists('name', 'type_id'), null, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('amount', 'Amount') !!}
                        {!! Form::number('amount', null, ['class'=>'form-control', 'step'=>'0.01', 'init-model'=>'amount', 'ng-required'=>'true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <div class = "form-group"  ng-controller = "DatepickerCtrl">
                        {!! Form::label('date', 'Value Date') !!}

                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Destination Account</div>
            <div class="panel-body">
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <p>Send to</p>
                    </div>
                    <div class="col-md-3">
                        {!! Form::radio('category',  'project', null, ['init-model'=>'category']) !!}
                        {!! Form::label('category', 'Project') !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::radio('category', 'shares', null, ['init-model'=>'category']) !!}
                        {!! Form::label('category', 'Shares') !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::radio('category', 'product', true, ['init-model'=>'category']) !!}
                        {!! Form::label('category', 'Product') !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::radio('category', 'funds', true, ['init-model'=>'category']) !!}
                        {!! Form::label('category', 'Unit Fund') !!}
                    </div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'category') !!}<br/>
                </div>

                <div class="col-md-4">
                    <div class="form-group" ng-show="category == 'project'">
                        {!! Form::label('project_id', 'Projects') !!}
                        {!! Form::select('project_id', $projects, null, ['class'=>'form-control', 'init-model'=>'project_id', 'ng-required'=>'true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                    </div>

                    <div class="form-group" ng-show="category == 'product'">
                        {!! Form::label('product_id', 'Products') !!}
                        {!! Form::select('product_id', $products, null, ['class'=>'form-control', 'init-model'=>'product_id', 'ng-required'=>'true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                    </div>

                    <div class="form-group" ng-show="category == 'shares'">
                        {!! Form::label('entity_id', 'Share Entities') !!}
                        {!! Form::select('entity_id', $shareentities, null, ['class'=>'form-control', 'init-model'=>'entity_id', 'ng-required'=>'true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                    </div>

                    <div class="form-group" ng-show="category == 'funds'">
                        {!! Form::label('unit_fund_id', 'Unit Funds') !!}
                        {!! Form::select('unit_fund_id', $funds, null, ['class'=>'form-control', 'init-model'=>'unit_fund_id', 'ng-required'=>'true']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'unit_fund_id') !!}
                    </div>
                </div>

                <div class="col-md-4">
                    {!! Form::label('exchange_rate', 'Exchange Rate (Account Currency to Product Currency)') !!}
                    <span><a ng-click="reverse = !reverse">Reverse Exchange Calculation</a></span>
                    {!! Form::number('exchange_rate', 1, ['class'=>'form-control', 'step'=>'0.0001', 'ng-model'=>'exchange_rate',]) !!}
                    {!! Form::hidden('effective_rate', null, ['data-ng-value' => 'effective_rate']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'exchange_rate') !!}
                </div>

                <div class="col-md-4">
                    {!! Form::label('converted', 'Converted Amount') !!}
                    {!! Form::text('converted', null, ['class'=>'form-control', 'init-model'=>'converted', 'ng-disabled'=>'true']) !!}
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Client Details
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="row" ng-controller="ClientUserCtrl">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Search Client by Name</label>

                                <input type="text" ng-model="selectedClient" placeholder="Enter the client name" typeahead="client as client.fullName for client in searchClients($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_id') !!}
                                <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                                <div ng-show="noResults">
                                    <i class="glyphicon glyphicon-remove"></i> No Results Found
                                </div>

                                <div class="hide">
                                    <input name="client_id" type="text" ng-model="selectedClient.id" ng-required='true'>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" ng-if="selectedClient.id">
                                <h4>Selected client</h4>
                                <table class="table table-responsive">
                                    <thead></thead>
                                    <tbody>
                                    <tr>
                                        <td>Client Name</td><td><% selectedClient.fullName %></td>
                                    </tr>
                                    <tr>
                                        <td>Client code</td><td><% selectedClient.client_code %></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td><td><% selectedClient.contact.email %></td>
                                    </tr>
                                    <tr>
                                        <td>Client Type</td><td><% selectedClient.type %></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group clearfix">
                        <div class="col-md-6">
                            {!! Form::radio('recipient_known', true, true, ['init-model'=>'recipient_known']) !!}
                            {!! Form::label('recipient_known', 'FA Known') !!}
                        </div>

                        <div class="col-md-4">
                            {!! Form::radio('recipient_known', false, null, ['init-model'=>'recipient_known']) !!}
                            {!! Form::label('recipient_known', 'FA not known') !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'recipient_known') !!}<br/>
                        </div>
                    </div>

                    <div class="row" ng-controller="ClientUserCtrl" ng-if="recipient_known == true">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Search FA by Name</label>
                                <input type="text" ng-model="selectedFA" placeholder="Enter the FA name" typeahead="fa as fa.fullname for fa in searchFas($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                                <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                                <div ng-show="noResults">
                                    <i class="glyphicon glyphicon-remove"></i> No Results Found
                                </div>

                                <div class="hide">
                                    <input name="commission_recipient_id" type="text" ng-model="selectedFA.id">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                {!! Form::submit('Transfer Cash', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>