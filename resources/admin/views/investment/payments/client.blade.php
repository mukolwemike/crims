@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">

        <!-- Client Details -->
        @include('investment.payments.partials.client_details')

        <!-- Balance Details -->
        @include('investment.payments.partials.balance_details', ['users'=>$users])

        <!-- Client Transactions -->
        <div ng-controller="ClientTransactionsGridController" ng-init="setClientId({!! $client->id !!})">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="2">
                            <a href="{!! route('client_payments_transact', $client->id) !!}" class="btn btn-primary margin-bottom-20">Transact</a>
                        </th>
                        <th colspan="2"></th>
                        <th>
                            <select st-search="project" class="form-control">
                                <option value="">All Projects</option>
                                @foreach($projects as $project)
                                    <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="product" class="form-control">
                                <option value="">All Products</option>
                                @foreach($products as $product)
                                    <option value="{!! $product->id !!}">{!! $product->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="entity" class="form-control">
                                <option value="">All Share Entities</option>
                                @foreach($shareentities as $entity)
                                    <option value="{!! $entity->id !!}">{!! $entity->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                    </tr>
                    <tr>
                        <th>Trans ID</th>
                        <th>Client Code</th>
                        <th>Client</th>
                        <th>Type</th>
                        <th>Payment For</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>FA</th>
                        <th>Balance</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.client_code %></td>
                        <td><% row.full_name %></td>
                        <td><% row.type %></td>
                        <td><% row.payment_for %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><% row.description %></td>
                        <td><% row.date %></td>
                        <td><% row.fa %></td>
                        <td><% row.balance | currency:"" %></td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="View Details" popover-trigger="mouseenter" href="/dashboard/investments/client-payments/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                            <a ng-if="row.type_slug == 'W' || row.type_slug == 'TO'" href="#" ng-controller="PopoverCtrl" uib-popover="Generate Instruction" popover-trigger="mouseenter" data-toggle="modal" data-target="#generate-instruction-<% row.id %>"><i class="fa fa-download"></i></a>
                            <a ng-if="row.type_slug == 'FO' || row.type_slug == 'TO'" ng-controller="PopoverCtrl" uib-popover="View Instruction" popover-trigger="mouseenter" href="/dashboard/investments/client-payments/pdf/<% row.id %>"><i class="fa fa-eye"></i></a>
                            <el-button size="mini" ng-if="row.type_slug == 'W' || row.type_slug == 'TO'" href="#" ng-controller="PopoverCtrl" uib-popover="Exempt Instruction" popover-trigger="mouseenter" data-toggle="modal" data-target="#exempt-instruction-<% row.id %>"><i class="fa fa-arrow-left"></i></el-button>

                            <div class="modal fade" id="generate-instruction-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open(['route'=>['transfer_cash_to_client', $client->id]]) !!}
                                        <div class="modal-header">
                                            <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                            <h4 class="modal-title" id="myModalLabel">Generate Instruction</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                {!! Form::text('payment_id', null, ['ng-model'=>'row.id', 'style'=>'display:none;']) !!}
                                                <div class="col-md-4">{!! Form::label('first_signatory', 'First Signatory') !!}</div>
                                                <div class="col-md-8">{!! Form::select('first_signatory', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory') !!}
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-4">{!! Form::label('second_signatory', 'Second Signatory') !!}</div>
                                                <div class="col-md-8">{!! Form::select('second_signatory', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory') !!}
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-4">{!! Form::label('client_bank_account_id', 'Client Bank Account') !!}</div>
                                                <div class="col-md-8">{!! Form::select('client_bank_account_id', $banks, null, ['class'=>'form-control']) !!}</div>
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_bank_account_id') !!}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                                            <a data-dismiss="modal" class="btn btn-default">Close</a>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="exempt-instruction-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open(['route'=>['exempt_client_payment', $client->id]]) !!}
                                        <div class="modal-header">
                                            <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                            <h4 class="modal-title" id="myModalLabel">Exempt Instruction</h4>
                                        </div>
                                        <div class="modal-body row">
                                            <div class="form-group">
                                                <div class="col-md-12"  ng-controller="DatepickerCtrl">
                                                    {!! Form::text('payment_id', null, ['ng-model'=>'row.id', 'style'=>'display:none;']) !!}
                                                    {!! Form::label('date', 'Choose date to generate instruction') !!}
                                                    {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>'true']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Exempt', ['class'=>'btn btn-success']) !!}
                                            <a data-dismiss="modal" class="btn btn-default">Close</a>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop