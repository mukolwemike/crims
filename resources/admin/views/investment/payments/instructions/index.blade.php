@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="PaymentInstructionsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <td colspan="4">
                        <!-- Button trigger modal -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportInstructions">Download Summary</button>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#bank-instructions-modal">Export Instructions</button>
                        </div>
                    </td>
                    <th colspan="2"><st-date-range predicate="date" before="query.before" after="query.after"></st-date-range></th>
                    <th>
                        <select st-search="project" class="form-control">
                            <option value="">All Projects</option>
                            @foreach($projects as $project)
                                <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                            @endforeach
                        </select>
                    </th>
                    <th>
                        <select st-search="product" class="form-control">
                            <option value="">All Products</option>
                            @foreach($projects as $product)
                                <option value="{!! $product->id !!}">{!! $product->name !!}</option>
                            @endforeach
                        </select>
                    </th>
                    <th>
                        <select st-search="fund" class="form-control">
                            <option value="">All Funds</option>
                            @foreach($funds as $fund)
                                <option value="{!! $fund->id !!}">{!! $fund->name !!}</option>
                            @endforeach
                        </select>
                    </th>
                    <th><input st-search="client_code" class="form-control" placeholder="Search client code..." type="text"/></th>
                    <th><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                    <th></th>
                </tr>
                <tr>
                    <th st-sort="client_code">Client Code</th>
                    <th>Client Name</th>
                    <th>Product/Project</th>
                    <th>Custodial Account</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>First Signatory</th>
                    <th>Second Signatory</th>
                    <th></th>
                </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                <tr ng-repeat="row in displayed">
                    <td><% row.client_code %></td>
                    <td><% row.full_name %></td>
                    <td><% row.project_product %></td>
                    <td><% row.account_name %></td>
                    <td><% row.amount | currency:"" %></td>
                    <td><% row.date | date %></td>
                    <td><% row.description %></td>
                    <td><% row.first_signatory %></td>
                    <td><% row.second_signatory %></td>
                    <td>
                        <a ng-controller="PopoverCtrl" uib-popover="View Details" popover-trigger="mouseenter" href="/dashboard/investments/payment-instructions/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                        <a ng-controller="PopoverCtrl" uib-popover="View Instruction PDF" popover-trigger="mouseenter" href="/dashboard/investments/client-payments/pdf/<% row.payment_id %>"><i class="fa fa-eye"></i></a>
                    </td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="100%" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exportInstructions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>'export_bank_instructions']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Export Instructions</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('start', 'From date') !!}

                        {!! Form::date('start', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('end', 'To date') !!}
                        {!! Form::date('end', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status1.opened", 'ng-focus'=>'status1.opened = !status1.opened']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Download', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--start of bank instructions modal--}}
    <div class="modal fade" id="bank-instructions-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['export_bank_instructions_path'], 'method'=>'POST']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Bank Instructions</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'required', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>

                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end_date', 'End Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'required', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of bank instructions modal--}}
    <script type = "text/ng-template" id = "stDateRange.htm">
        <div class="row">
            <span class="col-md-6">From: <input is-open="isAfterOpen" ng-change="rangeChanged()" datepicker-popup="yyyy/MM/dd" ng-focus="openAfter($event)" ng-model="after" type="text" class="form-control margin-bottom-20"/></span>
            <span class="col-md-6">To: <input is-open="isBeforeOpen" ng-change="rangeChanged()" ng-model="before" datepicker-popup="yyyy/MM/dd" ng-focus="openBefore($event)" type="text" class="form-control margin-bottom-20"/></span>
        </div>
    </script>
@stop