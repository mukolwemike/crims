@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Payment Instruction</h3>
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td colspan="2">{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client Name</td>
                        <td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>

                    <tr>
                        <td>Product</td>
                        <td colspan="2">{!! $payment->present()->paymentFor !!}</td>
                    </tr>

                    <tr>
                        <td>Custodial Account</td>
                        <td colspan="2">{!! $transaction->custodialAccount->name !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td colspan="2">{!! $instruction->description !!}</td>
                    </tr>
                    <tr>
                        <td>First Signatory</td>
                        <td colspan="2">{!! @$instruction->firstSignatory->full_name !!}</td>
                    </tr>
                    <tr>
                        <td>Second Signatory</td>
                        <td colspan="2">{!! @$instruction->secondSignatory->full_name !!}</td>
                    </tr>
                    <tr>
                        <th>Bank Details</th>
                        <td>Account Name</td>
                        <td>{!! @$account->accountName !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Account Number</th>
                        <td>{!! @$account->accountNumber !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Bank</th>
                        <td>{!! @$account->bankName !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Branch</th>
                        <td>{!! @$account->branch !!}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td></td>--}}
                        {{--<th>Clearing Code</th>--}}
                        {{--<td>{!! $instruction->clientBankAccount->bank->clearing_code !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td></td>--}}
                        {{--<th>Swift Code</th>--}}
                        {{--<td>{!! $instruction->clientBankAccount->swift_code !!}</td>--}}
                    {{--</tr>--}}

                </tbody>
            </table>
            <div class="btn-group">
                <a class="btn btn-default" data-toggle="modal" data-target="#edit-instruction"><i class="fa fa-pencil-square-o"></i> Edit Instruction</a>
                <a class="btn btn-danger" data-toggle="modal" data-target="#reverse-instruction"><i class="fa fa-undo"></i> Reverse Instruction</a>
            </div>
        </div>
    </div>
@endsection

<!-- Reverse Instruction Modal -->
<div class="modal fade" id="reverse-instruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Reverse Payment Instruction</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to reverse this instructions?</p>
            </div>
            <div class="modal-footer">
                {!! Form::open(['route'=>['reverse_payment_instruction', $instruction->id]]) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                {!! Form::button('<i class="fa fa-undo"></i> Reverse', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- Edit Instruction Modal -->
<div class="modal fade" id="edit-instruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::model($instruction, ['route'=>['update_payment_instruction', $instruction->id]]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Upload a Letter of Offer</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-md-4">{!! Form::label('first_signatory_id', 'First Signatory') !!}</div>
                    <div class="col-md-8">{!! Form::select('first_signatory_id', $users, null, ['class'=>'form-control']) !!}</div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory_id') !!}
                </div>

                <div class="form-group">
                    <div class="col-md-4">{!! Form::label('second_signatory_id', 'Second Signatory') !!}</div>
                    <div class="col-md-8">{!! Form::select('second_signatory_id', $users, null, ['class'=>'form-control']) !!}</div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory_id') !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Upload', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>