@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="ClientPaymentsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="4">
                            <a href="/dashboard/investments/payment-instructions" class="btn btn-primary margin-bottom-20">Payment Instructions</a>
                            <a href="{!! route('export_clients_with_balances') !!}" class="btn btn-primary margin-bottom-20">Export Balances</a>
                        </th>
                        <th>
                            <select st-search="project" class="form-control">
                                <option value="">All Projects</option>
                                @foreach($projects as $project)
                                    <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="product" class="form-control">
                                <option value="">All Products</option>
                                @foreach($products as $product)
                                    <option value="{!! $product->id !!}">{!! $product->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select st-search="fund" class="form-control">
                                <option value="">All Funds</option>
                                @foreach($funds as $fund)
                                    <option value="{!! $fund->id !!}">{!! $fund->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input st-search="client_code" class="form-control" placeholder="Search client code..." type="text"/></th>
                        <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                    </tr>
                    <tr>
                        <th st-sort="client_code">Client Code</th>
                        <th>Client Name</th>
                        <th>Payment For</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>FA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.client_code %></td>
                        <td><% row.full_name %></td>
                        <td><% row.payment_for %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><% row.type %></td>
                        <td><% row.description %></td>
                        <td><% row.date %></td>
                        <td><% row.fa %></td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="View Details" popover-trigger="mouseenter" href="/dashboard/investments/client-payments/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                            <a ng-if="row.client_id" ng-controller="PopoverCtrl" uib-popover="View Client Payments" popover-trigger="mouseenter" href="/dashboard/investments/client-payments/client/<% row.client_id %>"><i class="fa fa-user"></i></a>
                        </td>
                    </tr>
                    {{--<tr style="background-color: yellow; color: red;">--}}
                        {{--<th colspan="3" class="text-center">BALANCE</th>--}}
                        {{--<th colspan="7" class="text-center"><% balance | currency:"" %></th>--}}
                    {{--</tr>--}}
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop