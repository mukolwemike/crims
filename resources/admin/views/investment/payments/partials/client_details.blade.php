<div class="col-md-6">
    <div class="detail-group">
        <h4>Client Details</h4>
        <table class = "table table-hover">
            <tbody>
            <tr>
                <td>Client Code</td>
                <td>{!! $client->client_code !!}</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
            </tr>
            <tr>
                <td>E-Mail Address</td>
                <td>{!! $client->contact->email !!}</td>
            </tr>
            <tr>
                <td>Telephone</td>
                <td>{!! $client->contact->phone !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>