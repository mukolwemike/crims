<div class="col-md-6">
    <div class="detail-group">
        <h4>Balances</h4>
        <table class="table table-responsive table-hover">
            <thead>
            <tr>
                <th>Product/Project/Unit Fund</th>
                <th>Amount</th>
                <th>Instruction</th>
            </tr>
            </thead>
            <tbody>
            @foreach($balances as $balance)
                <tr>
                    <td>{!! $balance->name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($balance->balance) !!}</td>
                    <th>
                        <a ng-controller="PopoverCtrl" uib-popover="Generate Instruction" popover-trigger="mouseenter" href="#" data-toggle="modal" data-target="#generate-instruction-{!! $balance->type !!}-{!! $balance->id !!}"><i class="fa fa-download"></i></a>
                        <div class="modal fade" id="generate-instruction-{!! $balance->type !!}-{!! $balance->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>['transfer_cash_to_client', $client->id]]) !!}
                                    <div class="modal-header">
                                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                        <h4 class="modal-title" id="myModalLabel">Generate Instruction</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            {!! Form::hidden('balance_id', $balance->id) !!}
                                            {!! Form::hidden('balance_type', $balance->type) !!}
                                            <div class="col-md-4">{!! Form::label('first_signatory', 'First Signatory') !!}</div>
                                            <div class="col-md-8">{!! Form::select('first_signatory', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'first_signatory') !!}
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4">{!! Form::label('second_signatory', 'Second Signatory') !!}</div>
                                            <div class="col-md-8">{!! Form::select('second_signatory', $users, null, ['class'=>'form-control', 'ng-required'=>'true']) !!}</div>
                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'second_signatory') !!}
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4">{!! Form::label('client_bank_account_id', 'Client Bank Account') !!}</div>
                                            <div class="col-md-8">{!! Form::select('client_bank_account_id', $banks, null, ['class'=>'form-control']) !!}</div>
                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_bank_account_id') !!}
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4">{!! Form::label('amount', 'Amount to Withdraw') !!}</div>
                                            <div class="col-md-8">{!! Form::number('amount', null, ['class'=>'form-control', 'step'=>'0.01', 'ng-required'=>'true']) !!}</div>
                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                                        <a data-dismiss="modal" class="btn btn-default">Close</a>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{--@if($show_link)--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="btn-group">--}}
                    {{--@if($client->id)--}}
                        {{--<a class="btn btn-default" href="/dashboard/investments/client-payments/transfer/{!! $client->id !!}">Transfer Cash to Client</a>--}}
                    {{--@else--}}
                        {{--<button class="btn btn-default" disabled>Transfer Cash to Client</button>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
    </div>
</div>