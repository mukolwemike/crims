<!-- Interest Rate Updates Modal -->
<div class="modal fade" id="interest-rate-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>'save_interest_rate_update']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Interest Rate Update</h4>
            </div>
            <div class="modal-body"  ng-controller="InterestRateController">
                <div class="form-group">
                    <div class="col-md-3">{!! Form::label('product_id', 'Product') !!}</div>

                    <div class="col-md-9">{!! Form::select('product_id', $productslists, null, ['class'=>'form-control', 'required']) !!}</div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                </div>

                <div class="form-group">
                    <div class="col-md-3">{!! Form::label('date', 'Effective Date') !!}</div>

                    <div ng-controller="DatepickerCtrl" class="col-md-9">{!! Form::text('date', null, ['required', 'class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <div class="form-group">
                    <div class="col-md-3">{!! Form::label('description', 'Description') !!}</div>

                    <div class="col-md-9">{!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>3, 'required']) !!}</div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                </div>

                <div class="col-md-12">
                    <h4 class="center">Rates</h4>
                </div>

                <div class="form-group" data-ng-repeat="rate in rates track by $index">

                    <div class="panel">
                        <div class="col-md-2">
                            <br><br>
                            <% $index + 1 %>
                        </div>

                        <div class="col-md-5">
                            {!! Form::label('tenor') !!}
                            {!! Form::number('tenor[]', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tenor') !!}
                        </div>

                        <div class="col-md-5">
                            {!! Form::label('rate') !!}
                            {!! Form::number('rate[]', null, ['class'=>'form-control', 'id'=>'rate','step'=>'0.01', ]) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'rate') !!}
                        </div>

                        <div class="col-md-2">
                            <br><br>
                        </div>

                        <div class="col-md-5">
                            {!! Form::label('minimum amount') !!}
                            {!! Form::number('min_amount[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'rate') !!}
                        </div>

                        <div class="col-md-5">
                            {!! Form::label('Hidden Rate') !!}
                            {!! Form::select('hidden[]', [0 => 'Non Hidden', 1 => 'Hidden'], 0, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'rate') !!}
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-1">
                        <br><br>
                        <a type="button" class="btn btn-success" ng-show="$last" ng-click="addRate()"><i class="fa fa-plus"></i></a>

                    </div>
                    <div class="col-md-1">
                        <br><br>
                        <a type="button" class="btn btn-danger" ng-show="$last" ng-click="removeRate()"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                {!! Form::submit('Save Rate Update', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>