@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Product Interest Rate</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Product</td>
                        <td>{!! $update->product->name !!}</td>
                    </tr>
                    <tr>
                        <td>Effective Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($update->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $update->description !!}</td>
                    </tr>
                </tbody>
            </table>
            {{--<button class="btn btn-default" data-toggle="modal" data-target="#interest-rate-update"><i class="fa fa-pencil-square-o"></i> Edit Update</button>--}}
            {{--@if($rates->count())--}}
                {{--<a class="btn btn-primary" href="/dashboard/investments/interest-rates/rates/{!! $update->id !!}"><i class="fa fa-list-alt"></i> Edit Rates</a>--}}
            {{--@else--}}
                {{--<a class="btn btn-primary" href="/dashboard/investments/interest-rates/rates/{!! $update->id !!}"><i class="fa fa-list-alt"></i> Add Rates</a>--}}
            {{--@endif--}}
        </div>

        <div class = "panel-dashboard">
            <h4>Rates</h4>
            @if($rates->count())
                <table class = "table table-hover">
                    <thead>
                    <tr>
                        <td>Tenor</td>
                        <td>Amount</td>
                        <td>Rate</td>
                        <td>Hidden</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rates as $rate)
                        <tr>
                            <td>{!! $rate->tenor !!} months</td>
                            <td>{!! $rate->min_amount !!}</td>
                            <td>{!! $rate->rate !!}%</td>
                            <td>
                                @if($rate->hidden == 1)
                                    &nbsp;Yes
                                @else
                                    &nbsp;No
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>There are no rates for this interest rate update of yet.</p>
            @endif
        </div>
    </div>
@endsection

@include('investment.interest_rate_updates.partials.form', ['update'=>$update])