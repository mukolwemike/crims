@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="InterestRateUpdatesGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="3">
                            <button class="btn btn-primary margin-bottom-20" data-toggle="modal" data-target="#interest-rate-update"><i class="fa fa-plus-square-o"></i> Create Interest Update</button>
                        </th>
                        <th>
                            <select st-search="product" class="form-control">
                                <option value="">All Products</option>
                                @foreach($products as $product)
                                    <option value="{!! $product->id !!}">{!! $product->name !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>ID</th>
                        <th st-sort="date">Date</th>
                        <th>Product</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.date | date %></td>
                        <td><% row.product %></td>
                        <td><% row.description %></td>
                        <td>
                            <a href="/dashboard/investments/interest-rates/show/<% row.id %>" target="_self"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop

@include('investment.interest_rate_updates.partials.form')