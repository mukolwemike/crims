@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard" ng-controller="InterestRateController">

            {!! Form::open(['route'=>['save_interest_rates_for_update', $update->id]]) !!}

            <div class="form-group">
                <div class="col-md-12 bold margin-bottom-20">
                    <div class="col-md-12"><h4>Add Interest Rates</h4></div>
                    <div class="col-md-3">Tenor (months)</div>
                    <div class="col-md-3">Rate (%)</div>
                </div>

                <div class="col-md-12" data-ng-repeat="rate in rates track by $index">
                    <div class="col-md-3 form-group">
                        {!! Form::number('tenor[]', null, ['class'=>'form-control', 'ng-required'=>"true", 'ng-model'=>'rate.tenor']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tenor') !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::number('rate[]', null, ['class'=>'form-control', 'id'=>'rate', 'ng-model'=>'rate.rate']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'rate') !!}
                    </div>

                    <div class="col-md-1"><button type="button" class="btn btn-danger" ng-show="$last" ng-click="removeRate()"><i class="fa fa-remove"></i></button></div>
                </div>

                <div class="col-md-7">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" ng-click="addNewRate()"><i class="fa fa-plus-circle"></i> Add Rate</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirm-interest-rates"><i class="fa fa-save"></i> Save Rate(s)</button>
                    </div>
                </div>

                <!-- Confirm Interest Rates Modal -->
                <div class="modal fade" id="confirm-interest-rates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class = "modal-title">Confirm Interest Rate(s)</h4>
                            </div>
                            <div class="modal-body">
                                <div class = "detail-group">
                                    <p>Are you sure you want to save the interest rate(s) below?</p>
                                    <table class = "table table-hover table-responsive table-striped">
                                        <thead>
                                        <tr>
                                            <th>Tenor</th>
                                            <th>Rate</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="rate in rates">
                                            <td><% rate.tenor %> months</td>
                                            <td><% rate.rate %>%</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <div class="form-group">
                                    {!! Form::submit('Save Rate(s)', ['class'=>'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}


        </div>
    </div>
@endsection