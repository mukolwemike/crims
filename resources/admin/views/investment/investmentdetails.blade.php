@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
       <div class="col-md-6">
           @include('investment.partials.investmentdetail', ['inv_title'=>'Investment Details'])
       </div>

        <div class="col-md-6">
            <div class="detail-group">
                <h4>Actions</h4>
                @unless($investment->withdrawn == 1 || $investment->rolled == 1)
                    @if($schedule)
                        <a class="btn btn-danger margin-bottom-10 disabled" href="#">Withdraw</a>
                        <a class="btn btn-warning margin-bottom-10 disabled" href="#">Rollover</a>
                        <a class="btn btn-primary margin-bottom-10 disabled" href="#">Edit</a>
                        <a class="btn btn-info margin-bottom-10 disabled" href="#">Interest payment</a>
                    @else
                        @if(! $investment->repo->isSeipChild())
                                <a class="btn btn-danger margin-bottom-10"
                                   href="/dashboard/investments/withdraw/{!!$investment->id!!}">Withdraw</a>
                            <a class="btn btn-warning margin-bottom-10"
                               href="/dashboard/investments/roll-over/{!!$investment->id !!}">Rollover</a>
                        @endif
                        <a class="btn btn-primary margin-bottom-10" href="/dashboard/investments/edit/{!!$investment->id!!}">Edit</a>
                        <a class="btn btn-info margin-bottom-10" href="/dashboard/investments/pay/{!!$investment->id!!}">Interest payment</a>
                    @endif
                @endunless
                <a class="btn btn-primary margin-bottom-10" href="/dashboard/investments/commission/{!! $investment->id !!}">Commission</a>
                @module('cms')
                    {{--<a class="btn btn-success margin-bottom-10" href="/dashboard/investments/topup/{!!$investment->id!!}">Topup</a>--}}
                @endModule
                <a class="btn btn-primary margin-bottom-10" href="/dashboard/investments/instructions/{!! $investment->id !!}">Bank Instructions</a>
                <a class="btn btn-success margin-bottom-10" href="/dashboard/investments/clientinvestments/maturity/{!! $investment->id !!}">Maturity notification</a>
                @if($investment->investmentPaymentScheduleDetail)
                    <a class="btn btn-info margin-bottom-10" href="/dashboard/investments/investment_payment_schedules/{!!$investment->id!!}">Payment Schedules</a>
                @endif

                <div ng-controller="CollapseCtrl">
                    <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">More Actions</button>
                    <hr>
                    <div uib-collapse="isCollapsed">
                        <a class="btn btn-primary margin-bottom-10" href="/dashboard/investments/edit/{!!$investment->id!!}">Edit</a>
                        {{--<a class="btn btn-warning margin-bottom-10" href="/dashboard/investments/deductions/{!! $investment->id !!}">Deductions</a>--}}
                        @if($investment->withdrawn)
                            @if($investment->rolled)
                                <a class="btn btn-danger margin-bottom-10" href="#" data-toggle="modal" data-target="#rollback-modal">Rollback Investment</a>
                            @else
                                {{--<a class="btn btn-danger margin-bottom-10" href="#" data-toggle="modal" data-target="#reverse-withdraw-modal">Reverse Withdrawal</a>--}}
                            @endif
                        @else
                            <a class="btn btn-danger margin-bottom-10" href="#" data-toggle="modal" data-target="#rollback-modal">Rollback Investment</a>
                            <a class="btn btn-danger margin-bottom-10" href="/dashboard/investments/transfer/{!! $investment->id !!}">Transfer</a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="detail-group">
                <h3>Withdrawals</h3>
                <table class="table table-striped">
                    <tbody>
                        @foreach($investment->withdrawals as $withdrawal)
                            <tr>
                                <td>{{ $withdrawal->description }}</td>
                                <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($withdrawal->date) }}</td>
                                <td>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($withdrawal->amount)) }}</td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#reverse-withdraw-modal{{ $withdrawal->id }}" class="btn btn-danger btn-xs"><i class="fa fa-undo"></i> Reverse</a>
                                    <!-- Reverse Withdrawal Modal -->
                                    <div class="modal fade" id="reverse-withdraw-modal{{ $withdrawal->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {!! Form::open(['route'=>['reverse_withdraw', $withdrawal->id ]]) !!}
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Are you sure you want to reverse this withdrawal</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>The withdrawal will be undone</p>

                                                    <div class="form-group">
                                                        {!! Form::label('reason', 'Please provide a reason') !!}

                                                        {!! Form::text('reason', null, ['class'=>'form-control', 'placeholder'=>'Reason', 'required']) !!}

                                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <button type="submit" class="btn btn-danger">Proceed to Rollback</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop


<!--Rollback Investment Modal -->
<div class="modal fade" id="rollback-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['rollback_path', $investment->id ]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Are you sure you want to Rollback Investment?</h4>
                </div>
                <div class="modal-body">
                    <p>The investment will be removed and all affected records undone.</p>

                    <div class="form-group">
                        {!! Form::label('reason', 'Please provide a reason') !!}

                        {!! Form::text('reason', null, ['class'=>'form-control', 'placeholder'=>'Reason', 'required']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger">Proceed to Rollback</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
