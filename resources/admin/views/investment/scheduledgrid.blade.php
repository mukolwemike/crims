@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "ScheduledGridController">
            <div class="btn-group" role="group" aria-label="...">
                <a href="/dashboard/investments/clientinvestments/maturity" class="btn btn-success">Maturities</a>
                <a href="/dashboard/investments/interest" class="btn btn-success">Interest Payments</a>
                <a href="/dashboard/investments/clientinvestments/deductions" class="btn btn-success">Deductions</a>
                <a href="/dashboard/investments/clientinvestments/withdrawals" class="btn btn-success">Withdrawals</a>
            </div>
            <div class="clearfix"></div>

            <table class="table table-responsive table-hover table-striped">
                <thead>
                <tr>
                    <th>Client code</th>
                    <th>Name</th>
                    <th>Investment Date</th><th>Maturity Date</th><th>Amount</th>
                    <th>Interest Rate</th><th>Net Interest</th><th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($investments as $investment)
                    <tr>
                        <td>{!! $investment->client->client_code !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getNetInterestForInvestment()) !!}</td>
                        <td>
                            @if(isset($investment->schedule->action))
                                {!! $investment->schedule->action !!}
                            @endif
                        </td>
                        <td><a href="/dashboard/investments/clientinvestments/schedule/{!! $investment->id !!}"><i class="fa fa-list-alt"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan = "100%">{!! $investments->links() !!}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop
