@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                @foreach($products as $product)
                    <li role="presentation" class=""><a href="#{!! $product->id !!}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">{!! $product->name !!}</a></li>
                @endforeach
            </ul>
            <div id="myTabContent" class="tab-content" ng-controller="depositAnalysisCtrl">
                @foreach($products as $product)
                    <div role="tabpanel" class="tab-pane fade" id="{!! $product->id !!}" aria-labelledby="home-tab">

                        {!! Form::open() !!}
                        <div class="col-md-6 pull-right"  ng-controller = "DatepickerCtrl">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('start', 'Start Date') !!}

                                    {!! Form::text('start', $start, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('end', 'End Date') !!}

                                    {!! Form::text('end', $end, ['class'=>'form-control', 'datepicker-popup init-model'=>"dateSecond", 'is-open'=>"statusSecond.opened", 'ng-focus'=>'openSecond($event)']) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                {!! Form::submit('Submit', ['class'=>'btn btn-success margin-top-25']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>

                        <div class="col-md-6">
                            <h4>Weekly Analysis</h4>
                        </div>
                        <div class="col-md-6 pull-right">
                            <button type="button" class="btn btn-default pull-right" ng-click="weeklyCollapsed = !weeklyCollapsed">Toggle collapse</button>
                        </div>

                        <div class="clearfix"></div>


                        <hr>
                        <div uib-collapse="weeklyCollapsed">

                            <?php $weeklies = (new \Cytonn\Investment\MaturityRepository())->getWeeklyMaturityAnalysisFor($start, $end, $product) ?>
                            <table class="table table-responsive table-striped table-hover">
                                <thead>
                                    <tr><th>From</th><th>To</th><th>Amount</th></tr>
                                </thead>
                                <tbody>
                                    @foreach($weeklies as $weekly)
                                        <tr>
                                            <td>{!! $weekly['start']->toFormattedDateString() !!}</td>
                                            <td>{!! $weekly['end']->toFormattedDateString() !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($weekly['amount']) !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <h4>Monthly Analysis</h4>
                        </div>
                        <div class="col-md-6 pull-right">
                            <button type="button" class="btn btn-default pull-right" ng-click="monthlyCollapsed = !monthlyCollapsed">Toggle collapse</button>
                        </div>



                        <div class="clearfix"></div>
                        <hr>
                        <div uib-collapse="monthlyCollapsed">
                            <?php $monthlies = (new \Cytonn\Investment\MaturityRepository())->getMonthlyMaturityAnalysisFor($start, $end, $product)?>
                            <table class="table table-responsive table-striped table-hover">
                                <thead>
                                <tr><th>From</th><th>To</th><th>Amount</th></tr>
                                </thead>
                                <tbody>
                                @foreach($monthlies as $monthly)
                                    <tr>
                                        <td>{!! $monthly['start']->toFormattedDateString() !!}</td>
                                        <td>{!! $monthly['end']->toFormattedDateString() !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($monthly['amount']) !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection