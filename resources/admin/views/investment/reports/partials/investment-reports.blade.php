<div class="panel">
    <div class="panel-heading">
        <h3>Investment Reports</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-hover table-responsive">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {{--<tr>--}}
                    {{--<td>IFAs Report</td>--}}
                    {{--<td><a class="btn btn-success" href="/dashboard/investments/reports/export-client-contacts">Export</a></td>--}}
                    {{--<td><a href="/dashboard/investments/client-payments-report" class="btn btn-success" role="button">Test</a></td>--}}
                {{--</tr>--}}
                <tr>
                    <td>IFAs Report</td>
                    {{--<td><a class="btn btn-success" href="/dashboard/investments/reports/export-client-contacts">Export</a></td>--}}
                    <td><a href="/dashboard/investments/ifs-clients-report" class="btn btn-success" role="button">Export</a></td>
                </tr>
                <tr>
                    <td>Client Contacts</td>
                    {{--<td><a class="btn btn-success" href="/dashboard/investments/reports/export-client-contacts">Export</a></td>--}}
                    <td><a href="#add-event-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a></td>
                </tr>
                <tr>
                    <td>Client Profiling Report</td>
                    <td><a href="#client-profiling-report" class="btn btn-success" data-toggle="modal" role="button">Export</a></td>
                </tr>
                <tr>
                    <td>Client Summary</td>
                    <td>
                        <a href="#client_summery_modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Daily Business Confirmations</td>
                    <td>
                        <a href="#daily_business_confirmations" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Untaxed Clients Report</td>
                    <td>
                        {!! Form::open(['route'=>['export_untaxed_clients_report'], 'method'=>'POST']) !!}
                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                <tr>
                    <td>Inactive Clients Report</td>
                    <td><a href="#inactive_clients_summary_modal" class="btn btn-success" data-toggle="modal" role="button">Export</a></td>
                </tr>
                <tr>
                    <td>Active Clients Report</td>
                    <td><a href="#active_clients_summary_modal" class="btn btn-success" data-toggle="modal" role="button">Export</a></td>
                </tr>
                <tr>
                    <td>Investment Exports</td>
                    <td>
                        <a href="#export-client-investment-holdings" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                {{--<tr>--}}
                    {{--<td>Closing Balances for Partners</td>--}}
                    {{--<td>--}}
                        {{--<a href="#closing-balances-for-partners-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                <tr>
                    <td>Interest Expense</td>
                    <td>
                        <a href="#interest-expense-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Residual Income</td>
                    <td>
                        <a href="#residual-income-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Commission Summary for Fund Manager</td>
                    <td>
                        <a href="#commission-summary-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Commission Summary for FAs</td>
                    <td>
                        <a href="#commission-summary-for-fas-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Coop Clients Report</td>
                    <td>
                        {!! Form::open(['route'=>['export_coop_clients_path'], 'method'=>'POST']) !!}
                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                <tr>
                    <td>Clients Churn Report</td>
                    <td>
                        {!! Form::open(['route'=>['export_client_investments_churn_path'], 'method'=>'POST']) !!}
                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                <tr>
                    <td>FA Clients</td>
                    <td>
                        <a href="#fa-clients-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Distribution Structure</td>
                    <td>
                        {!! Form::open(['route'=>['export_distribution_structure_path'], 'method'=>'POST']) !!}
                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                <tr>
                    <td>Penalty Deductions</td>
                    <td>
                        <a href="#penalty-deductions-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Asset Liability Reconciliation</td>
                    <td>
                        <a href="#asset-liability-reconciliation-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Production Report</td>
                    <td>
                        <a href="#production-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Monthly Inflow Report</td>
                    <td>
                        <a href="#monthly-inflow-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>FA Commissions Projections</td>
                    <td>
                        <a href="#fa-commissions-projections" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Unaccounted/Unassigned Funds Report</td>
                    <td>
                        {!! Form::open(['route'=>['export_unaccounted_funds'], 'method'=>'POST']) !!}
                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                <tr>
                    <td>Withdrawals Report</td>
                    <td>
                        <a href="#investment-withdrawals-export-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Inflows Report</td>
                    <td>
                        <a href="#investment-inflows-export-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Client Statements Export</td>
                    <td>
                        <a href="#investment-client-statements-export-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Year Production</td>
                    <td>
                        <a href="#investment-fa-year-production-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Fundmanager Monthly Netflow</td>
                    <td>
                        <a href="#investment-fundmanager-monthly-netflow" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Currency Report</td>
                    <td>
                        <a href="#currency-report" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Client Movement Report</td>
                    <td>
                        <a href="#client-movement-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Client Tenor Report</td>
                    <td>
                        <a href="#client-tenor-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Daily Withdrawal Report</td>
                    <td>
                        <a href="#daily-withdrawal-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Clawbacks Report</td>
                    <td>
                        <a href="#clawbacks-summary-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Client Custodial Transfer Report</td>
                    <td>
                        <a href="#inter-client-custodial-transfer" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Client Commission Payments Report</td>
                    <td>
                        <a href="#client-commission-payments" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>FA Commission History Report</td>
                    <td>
                        <a href="#fa-commission-history-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Backdated Transactions Report</td>
                    <td>
                        <a href="#backdated_transactions_report" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Client Investments Withholding Tax</td>
                    <td>
                        <a href="#investments-withholding-tax-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Withholding Tax Status Report</td>
                    <td>
                        <a href="#withholding-tax-report" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Withholding Tax ITAX Report</td>
                    <td>
                        <a href="#withholding-tax-itax-report" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
                <tr>
                    <td>Fund Maturity Summary</td>
                    <td>
                        <a href="#fund-maturity-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Client Maturity Report</td>
                    <td>
                        <a href="#client-maturity-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>SAP Funds Inflow</td>
                    <td>
                        <a href="#export-fund-inflows" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>SAP Funds Outflow</td>
                    <td>
                        <a href="#export-fund-outflows" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>SAP Cash Transfers Report</td>
                    <td>
                        <a href="#cash-transfers" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>SAP Interest Expense Report</td>
                    <td>
                        <a href="#sap-interest-expense" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Payment Transactions Summary</td>
                    <td>
                        <a href="#payment-transactions-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Digital Transactions Summary</td>
                    <td>
                        <a href="#digital-transactions-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Intra Payments Summary</td>
                    <td>
                        <a href="#intra-payments-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>

                <tr>
                    <td>Automatic Withdrawal Summary</td>
                    <td>
                        <a href="#automatic-withdrawal-summary" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

{{--start investment export modals--}}
<div class="modal fade" id="investment-export-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_investment_path']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Investment Exports</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the investments you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of investment export modal--}}

{{--start closing balances for partners modals--}}
<div class="modal fade" id="closing-balances-for-partners-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_closing_balances_for_partners_path']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Closing Balances for Partners</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the closing balances for partners report you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="modal fade" id="client_summery_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['url'=>'/dashboard/investments/reports/inv/clients-summaries']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Summary Test</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the closing balances for partners report you wish to export</p>
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('fundmanager', 'Select Fund Manager') !!}
                        {!! Form::select('fundmanager', $fundManagers->toArray(), null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--start of daily business confirmations export modal--}}
<div class="modal fade" id="daily_business_confirmations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['daily_business_confirmations']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Daily Business Confirmations</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the date for which to the export the daily confirmations</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of daily business confirmations export--}}

{{--end of closing balances for partners modal--}}
{{--start interest expense modals--}}
<div class="modal fade" id="interest-expense-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_interest_expense_path']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Interest Expense</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the interest expense report you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of interest expense modal--}}
{{--the start of the modal window--}}
<div class="modal fade" id="add-event-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['get_reports_path']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Clients Contacts</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group" ng-init="category='investment'">
                    <div class="col-md-12">
                        {!! Form::label('category', 'Select category') !!}
                        {!! Form::select('category', $categories, null, ['id'=>'category', 'class'=>'form-control', 'required', 'ng-model' => 'category']) !!}
                    </div>
                    <div class="col-md-12">
                        {!! Form::label('active', 'Active Status') !!}
                        {!! Form::select('active', $activeStatuses, null, ['id'=>'active', 'class'=>'form-control', 'required']) !!}
                    </div>
                    <div ng-show="category != 'realestate'" class="col-md-12">
                        {!! Form::label('product_ids', 'Select Products') !!}
                        {!! Form::select('product_ids[]', $products, null, ['v-select'=>'', 'multiple', 'class'=>'form-control', 'style'=>'width: 100%;']) !!}
                    </div>

                    <div ng-show="category != 'investment'" class="col-md-12">
                        {!! Form::label('project_ids', 'Select Projects') !!}
                        {!! Form::select('project_ids[]', $projects, null, ['v-select'=>'', 'multiple', 'class'=>'form-control', 'style'=>'width: 100%;']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of modal--}}

{{--the start of the modal window--}}
<div class="modal fade" id="client-profiling-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_profiling_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Clients Profiling Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group" ng-init="category='investment'">

                    <div class="col-md-12">
                        {!! Form::label('category', 'Select category') !!}
                        {!! Form::select('category', array_except($categories, ['combined']), null, ['id'=>'category', 'class'=>'form-control', 'required', 'ng-model' => 'category']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('gender', 'Gender') !!}
                        {!! Form::select('gender', $genders, null, ['id'=>'gender', 'class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('account_type', 'Account Type') !!}
                        {!! Form::select('account_type', $accountTypes, null, ['id'=>'account_type', 'class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('min_age', 'Minimum Age') !!}
                        {!! Form::number('min_age', NULL, ['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('max_age', 'Maximum Age') !!}
                        {!! Form::number('max_age', NULL, ['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('min_investment', 'Minimum Investment') !!}
                        {!! Form::number('min_investment', NULL, ['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('max_investment', 'Maximum Investment') !!}
                        {!! Form::number('max_investment', NULL, ['class'=>'form-control']) !!}
                    </div>
                    <div ng-show="category != 'investment'" class="col-md-6">
                        {!! Form::label('re_option', 'Real Estate Option') !!}
                        {!! Form::select('re_option', ['payments' => 'Real Estate Payments', 'sales' => 'Real Estate Sales'], null, ['id'=>'active', 'class'=>'form-control', 'required']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('active', 'Active Status') !!}
                        {!! Form::select('active', $activeStatuses, null, ['id'=>'active', 'class'=>'form-control', 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of modal--}}
{{--start of residual income modal--}}
<div class="modal fade" id="residual-income-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_residual_income_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Residual Income</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of residual income modal--}}
{{--start of commission summary modal--}}
<div class="modal fade" id="commission-summary-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_commission_summary_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Commission Summary</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the fund manager and the date</p>
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                        {!! Form::select('fund_manager_id', \App\Cytonn\Models\FundManager::all()->lists('name','id'), NULL, ['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of commission summary modal--}}
{{--start of commission summary for fas modal--}}
<div class="modal fade" id="commission-summary-for-fas-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_commission_summary_for_fas_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Commission Summary for FAs</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of commission summary modal--}}
{{--start of investments withholding tax modal--}}
<div class="modal fade" id="investments-withholding-tax-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_investments_withholding_tax_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Investments Withholding Tax</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('product_id', 'Select product') !!}
                        {!! Form::select('product_id', ['' => 'All Products'] + $products->toArray(), null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of investments withholding tax modal--}}

<div class="modal fade" id="withholding-tax-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_withholding_report'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Investments Withholding Tax</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('product_id', 'Select product') !!}
                        {!! Form::select('product_id', ['' => 'All Products'] + $products->toArray(), null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('unit_fund_id', 'Select fund') !!}
                        {!! Form::select('unit_fund_id', ['' => 'Select Fund'] + $unitFunds->toArray(), null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="withholding-tax-itax-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_withholding_itax_report'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Investments Withholding Tax ITAX Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('product_id', 'Select product') !!}
                        {!! Form::select('product_id', ['' => 'All Products'] + $products->toArray(), null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('unit_fund_id', 'Select fund') !!}
                        {!! Form::select('unit_fund_id', ['' => 'Select Fund'] + $unitFunds->toArray(), null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--start of fa clients modal--}}
<div class="modal fade" id="fa-clients-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fa_clients_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">FA Clients</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-6"  ng-controller="DatepickerCtrl">
                    {!! Form::label('start_date', 'Start Date') !!}
                    {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                </div>
                <div class="col-md-6"  ng-controller="DatepickerCtrl">
                    {!! Form::label('end_date', 'Date') !!}
                    {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <p>Select the FA whose clients you wish to export</p>
                        {!! Form::select('fa_ids', $fas, null, ['class'=>'form-control', 'style'=>'width: 100%;']) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    {!! Form::label('fa_type', 'Select FA Type for whose FAs you wish to export their clients (Optional)') !!}
                    {!! Form::select('fa_type', [ '' => 'Select Fa Type'] + $faTypes->toArray(), null, ['class'=>'form-control']) !!}
                </div>

                <div class="col-md-6">
                    {!! Form::label('fa_rank', 'Select FA Rank for whose FAs you wish to export their clients (Optional)') !!}
                    {!! Form::select('fa_rank', [ '' => 'Select Fa Rank'] + $faRanks->toArray(), null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of investments withholding tax modal--}}
{{--start of bank instructions modal--}}
<div class="modal fade" id="bank-instructions-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_bank_instructions_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Bank Instructions</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of bank instructions modal--}}
{{--start of penalty deductions modal--}}
<div class="modal fade" id="penalty-deductions-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_penalty_deductions_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Penalty Deductions Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of penalty deductions modal--}}

{{--start of asset liability reconciliation modal--}}
<div class="modal fade" id="asset-liability-reconciliation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_asset_liability_reconciliation_path'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Asset Liability Reconciliation Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Select Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('currency_id', 'Select Currency') !!}
                        {!! Form::select('currency_id', \App\Cytonn\Models\Currency::get()->lists('code', 'id'), NULL, ['class'=>'form-control', 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of asset liability reconciliation modal--}}

{{--start production report--}}

<div class="modal fade" id="production-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['production_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Production Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start date, end date and level of detail for the production report</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('granularity', 'Level of detail') }}

                            {{ Form::select('granularity', ['daily' => 'Daily', 'monthly' => 'Monthly'], null, ['class'=>'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end production report--}}

{{--start monthly inflows report--}}

<div class="modal fade" id="monthly-inflow-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['monthly_inflows_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Monthly Inflows Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start date and end date for the monthly inflows report</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('fundmanager', 'Select Fund Manager') !!}
                        {!! Form::select('fundmanager', ['' => 'All'] + $fundManagers->toArray(), null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end monthly inflows report--}}

{{-- FA COMMISSIONS PROJECTIONS --}}
<div class="modal fade" id="fa-commissions-projections" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['fa_commissions_projections']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">FA Commissions Projections</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Select the FAs whose clients you wish to export</p>
                        {!! Form::select('fa_ids[]', $fas, null, ['v-select'=>'', 'multiple', 'class'=>'form-control', 'style'=>'width: 100%;']) !!}
                    </div>

                </div>
                <br/>
                <br/>
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end dates</p>
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--WITHDRAWALS REPORT--}}
<div class="modal fade" id="investment-withdrawals-export-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_withdrawals_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Withdrawals Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the withdrawals you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--INFLOWS REPORT--}}
<div class="modal fade" id="investment-inflows-export-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_inflows_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Inflows Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the investment inflows you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--CLIENT STATEMENTS EXPORTS--}}
<div class="modal fade" id="investment-client-statements-export-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_statements_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Statements Export</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12">
                        <p>Choose the start and the end date for the client statements you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('product_id', 'Select product') !!}
                        {!! Form::select('product_id', $products, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--FA YEAR PRODUCTION--}}
<div class="modal fade" id="investment-fa-year-production-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fa_year_production_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">FA Year Production Export</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12">
                        <p>Enter the year and select fa type</p>
                    </div>

                    <div class="col-md-6" >
                        {!! Form::label('year', 'Year') !!}
                        {!! Form::text('year', NULL, ['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('fa_type', 'Select FA Type') !!}
                        {!! Form::select('fa_type', $faTypes, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--FUNDMANAGER MONTHLY NETFLOW--}}
<div class="modal fade" id="investment-fundmanager-monthly-netflow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fundmanager_monthly_netflow']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">FundManager Monthly Netfow Export</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12">
                        <p>Enter the year and select the fund manager</p>
                    </div>

                    <div class="col-md-6" >
                        {!! Form::label('year', 'Year') !!}
                        {!! Form::text('year', NULL, ['class'=>'form-control', 'placeholder' => '2018']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('fundmanager', 'Select Fund Manager') !!}
                        {!! Form::select('fundmanager', $fundManagers, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


{{--CURRENCY REPORT--}}
<div class="modal fade" id="currency-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_currency_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Currency Report Export</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12">
                        <p>Enter the dates and desired custodial account</p>
                    </div>

                    <div class="col-md-6" ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date (Optional)') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6" ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date (Optional)') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('account_id', 'Select Custodial Account) (Optional)') !!}
                        {!! Form::select('account_id', ['' => 'All'] + $accounts, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


{{--CLIENT MOVEMENT REPORT--}}
<div class="modal fade" id="client-movement-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_movement_audit'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Movement Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Select the fund manager and the start and end dates</p>
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                        {!! Form::select('fund_manager_id', $fundManagers, NULL, ['class'=>'form-control', 'required']) !!}
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--CLIENT TENOR REPORT--}}
<div class="modal fade" id="client-tenor-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_tenor_report'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Tenor Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Select the fund manager and the start and end dates</p>
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('fund_manager_id', 'Fund Manager') !!}
                        {!! Form::select('fund_manager_id', $fundManagers, NULL, ['class'=>'form-control', 'required']) !!}
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--daily withdrawl export--}}
<div class="modal fade" id="daily-withdrawal-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_daily_withdrawal_report'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Daily Withdrawal Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required' => 'true']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required' => 'true']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


{{--the start of the modal window--}}
<div class="modal fade" id="inactive_clients_summary_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_inactive_cms_clients_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Inactive Clients Summary</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group" ng-init="category='investment'">
                    <div class="col-md-12">
                        {!! Form::label('category', 'Select category') !!}
                        {!! Form::select('category', $categories + ['unit_funds' => 'Unit Funds'], null, ['id'=>'category', 'class'=>'form-control', 'required', 'ng-model' => 'category']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of modal--}}

{{--the start of the modal window--}}
<div class="modal fade" id="active_clients_summary_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_active_clients_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Active Clients</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group" ng-init="category='combined'">
                    <div class="col-md-12">
                        {!! Form::label('type', 'Select category') !!}
                        {!! Form::select('type', $categories + ['unit_funds' => 'Unit Funds'], null, ['id'=>'category', 'class'=>'form-control', 'required', 'ng-model' => 'category']) !!}
                    </div>

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required' => 'true']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of modal--}}

{{--start investment export modals--}}
<div class="modal fade" id="clawbacks-summary-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_clawbacks_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Clawbacks Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the clawbacks you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--end of investment export modal--}}

{{--Inter Client Custodial Transfer--}}
<div class="modal fade" id="inter-client-custodial-transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_custodial_transfer']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title text-center" id="myModalLabel">Inter Client Custodial Transfer Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the Report you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--Inter Client Custodial Transfer--}}

{{--Client Commission Payments Report--}}
<div class="modal fade" id="client-commission-payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_commission_payments']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title text-center" id="myModalLabel">Client Commission Payments Report Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p class="text-center">Search for Client by Client Name or Client Code</p>
                    </div>

                    <div class="panel-body" style="padding:50px;">
                        <div class="row">
                           <search-client name="client_id"></search-client>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Client Commission Payments Report--}}
<div class="modal fade" id="backdated_transactions_report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_backdated_transaction_report']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Backdated Transaction Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and the end date for the backdated transaction report you wish to export</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="export-client-investment-holdings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_unit_fund_client_investments'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Unit Fund Client Investments</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Select the unit fund</p>
                    </div>

                    <div class="col-md-12">
                        {!! Form::label('unit_fund_id', 'Unit Fund') !!}
                        {!! Form::select('unit_fund_id', \App\Cytonn\Models\Unitization\UnitFund::all()->lists('name', 'id'), NULL, ['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--start of Fund Maturity Report modal--}}

<div class="modal fade" id="fund-maturity-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fund_maturity_report'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Fund Maturity Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    {{--<div class="col-md-6">--}}
                        {{--{!! Form::label('product_id', 'Select product') !!}--}}
                        {{--{!! Form::select('product_id', ['' => 'All Products'] + $products->toArray(), null, ['class'=>'form-control']) !!}--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="client-maturity-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_maturity_report'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Maturity Report</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the start and end dates</p>
                    </div>

                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start_date', 'Start Date') !!}
                        {!! Form::text('start_date', NULL, ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-6"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'Date') !!}
                        {!! Form::text('end_date', NULL, ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--end of investments withholding tax modal--}}

<div class="modal fade" id="export-fund-inflows" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fund_inflows'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Fund Inflows For SAP</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="export-fund-outflows" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fund_outflows'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Fund Outflows For SAP</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="cash-transfers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_cash_transfers'], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Cash Transfers For SAP</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('category', 'Select transfer type') !!}
                        {!! Form::select('type', ['inter-client' => 'Inter Client Transfers', 'inter-custodial' => 'Inter Custodial Transfers'], null, ['id'=>'category', 'class'=>'form-control', 'required', 'ng-model' => 'category']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="sap-interest-expense" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fund_interest_expense'], 'method'=>'POST']) !!}

            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Interest Expense For SAP</h4>
            </div>

            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="payment-transactions-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_payment_transactions_summary'], 'method'=>'POST']) !!}

            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Payment Transactions Summary</h4>
            </div>

            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="fa-commission-history-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_fa_commission_history'], 'method'=>'POST']) !!}

            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export FA Commission History</h4>
            </div>

            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="digital-transactions-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_digital_transaction_summary'], 'method'=>'POST']) !!}

            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Digital Transactions Summary</h4>
            </div>

            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="modal fade" id="intra-payments-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_intra_payments_summary'], 'method'=>'POST']) !!}

            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Intra Payments Transactions</h4>
            </div>

            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="automatic-withdrawal-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_automatic_withdrawal_summary'], 'method'=>'POST']) !!}

            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Export Automatic Withdrawal Transactions</h4>
            </div>

            <div class="modal-body row">
                <div class="form-group">

                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('start', 'Start Date') !!}
                        {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end', 'End Date') !!}
                        {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>





