@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="panel">
                <div class="panel-heading">
                    <h3> Please Select The Columns You Need Exported </h3>
                </div>
                <div class="panel-body">
                    {!! Form::open() !!}
                    <div class = "row">
                        <div class = "col-md-12">
                            <div class = "detail-group">
                                <h4>Client Details</h4>

                                    <span class="col-md-12 summaries-div" >
                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[client_code]', null, false, ['id'=>'client_code']) !!} {!! Form::label('client_code', 'Client Code') !!}
                                        </div>

                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[firstname]', null, false, ['id'=>'firstname']) !!} {!! Form::label('firstname', 'First name') !!}

                                        </div>

                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[full_name]', null, false, ['id'=>'name']) !!} {!! Form::label('name', 'Client Names') !!}

                                        </div>

                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[email]', null, false, ['id'=>'email']) !!} {!! Form::label('email', 'Main Email') !!}

                                        </div>

                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[emails]', null, false, ['id'=>'emails']) !!} {!! Form::label('email', 'Other Emails') !!}

                                        </div>
                                    </span>

                                    <span class="col-md-12 summaries-div" >
                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[phone]', null, false, ['id'=>'phone']) !!} {!! Form::label('phone', 'Client Phone') !!}
                                        </div>

                                        <div class = "form-group col-md-4 pull-left" >

                                            {!! Form::checkbox('clients[country]', null, false, ['id'=>'country']) !!} {!! Form::label('country', 'Country') !!}
                                        </div>
                                    </span>

                                </div>
                                <div class = "detail-group">
                                    <h4>FA Details</h4>
                                    <div class="col-md-12">
                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('fa[name]', null, false, ['id'=>'fa']) !!} {!! Form::label('fa', 'Client FA') !!}
                                        </div>

                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('fa[type]', null, false, ['id'=>'fa']) !!} {!! Form::label('fa', 'FA Type (Staff, IFA)') !!}
                                        </div>
                                    </div>


                                </div>
                                <div class = "detail-group col-md-12">
                                    <h4>Investment Details</h4>

                                    <div class="col-md-12 summaries-div" >

                                        <div class = "form-group form-group col-md-4" >

                                            {!! Form::checkbox('investment[amount]', null, false, ['id'=>'principal']) !!} {!! Form::label('principal', 'Principal Amount') !!}

                                        </div>

                                        <div class = "form-group form-group col-md-4" >

                                            {!! Form::checkbox('investment[gross_interest]', null, false, ['id'=>'gross_interest']) !!} {!! Form::label('gross_interest', 'Gross Interest') !!}
                                        </div>
                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('investment[net_interest]', null, false, ['id'=>'net_interest']) !!} {!! Form::label('net_interest', 'Net Interest') !!}

                                        </div>
                                    </div>

                                    <div class="col-md-12 summaries-div">

                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('investment[interest_paid]', null, false, ['id'=>'interest_paid']) !!} {!! Form::label('interest_paid', 'Amount Paid') !!}
                                        </div>

                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('investment[interest_balance]', null, false, ['id'=>'interest_balance']) !!} {!! Form::label('interest_balance', 'Interest Balance ') !!}

                                        </div>
                                        <div class = "form-group  col-md-4" >

                                            {!! Form::checkbox('investment[tenor]', null, false, ['id'=>'tenor']) !!} {!! Form::label('tenor', 'Tenor') !!}
                                        </div>
                                    </div>

                                    <div class="col-md-12 summaries-div">

                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('investment[total]', null, false, ['id'=>'total']) !!} {!! Form::label('total', 'Total') !!}

                                        </div>

                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('investment[start_date]', null, false, ['id'=>'start_date']) !!} {!! Form::label('start_date', 'Start Date') !!}
                                        </div>

                                        <div class = "form-group col-md-4" >

                                            {!! Form::checkbox('investment[end_date]', null, false, ['id'=>'end_date']) !!} {!! Form::label('end_date', 'End Date ') !!}

                                        </div>
                                    </div>

                                </div>

                            <div class="detail-group col-md-12">
                                <h4>Optional Settings</h4>
                                <div class="form-group"  ng-controller="DatepickerCtrl">
                                    {!! Form::label('end_date', 'End Date (If unspecified, it will return all results up to current date.)') !!}
                                    {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                </div>
                            </div>


                        <div class = " detail-group col-md-12">
                            <span class=" col-md-12 margin-top-15 wide-button">
                                    {!! Form::submit('Download', ['class'=>'btn btn-success form-control']) !!}
                            </span>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    </div>
@stop