@extends('layouts.default')
@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="#investment-reports" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">Investment</a></li>
                <li role="presentation" class=""><a href="#unitization-reports" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">Unitization</a></li>
                <li role="presentation" class=""><a href="#custom-reports" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">Custom Reports</a></li>
            </ul>
            <div id="myTabContent" class="tab-content margin-top-25">
                <div role="tabpanel" class="tab-pane fade" id="investment-reports" aria-labelledby="section-tab">
                    @include('investment.reports.partials.investment-reports')
                </div>
                <div role="tabpanel" class="tab-pane fade" id="unitization-reports" aria-labelledby="section-tab">
                    <unit-fund-reports></unit-fund-reports>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="custom-reports" aria-labelledby="section-tab">
                    @include('investment.reports.custom.index')
                </div>
            </div>
        </div>
    </div>
@stop