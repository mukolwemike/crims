<div class="panel">
    <div class="panel-heading">
        <new-custom-report></new-custom-report>
    </div>

    <div class="panel-body">
        <custom-report-table></custom-report-table>
    </div>
</div>

{{--Form for new custom report--}}