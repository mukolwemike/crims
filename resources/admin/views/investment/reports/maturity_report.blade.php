<table class="table table-responsive" style="font-size: 13px">
    <thead>
    <tr>
        <th>Client Code</th><th>Client Name</th><th>Value Date</th><th>Maturity Date</th><th>Invested Amount</th><th>Value at Maturity</th><th>FA Name</th><th>FA Position</th><th>FA Branch</th><th>FA Department</th>
    </tr>
    </thead>
    <tbody>
    @foreach($investments as $investment)
        <tr>
            <td>{!! $investment->client->client_code !!}</td>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date->subDay(), true)) !!}</td>
            <td>@if($investment->client->getLatestFa('investment')){!! $investment->client->getLatestFa('investment')->name !!}@endif</td>
            <td>@if($investment->client->getLatestFa('investment')){!! $investment->client->getLatestFa('investment')->type->name !!}@endif</td>
            <td>@if($investment->client->getLatestFa('investment')){!! $investment->client->getLatestFa('investment')->present()->getBranch() !!}@endif</td>
            <td>@if($investment->client->getLatestFa('investment')){!! $investment->client->getLatestFa('investment')->present()->getDepartmentUnit() !!}@endif</td>
        </tr>
    @endforeach
    </tbody>
</table>