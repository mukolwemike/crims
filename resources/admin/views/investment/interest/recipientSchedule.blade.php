@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <h3>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client)  !!}</h3>

            <hr/>
            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#present" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">This Month</a></li>
                <li role="presentation"><a href="#past" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Past Payments</a></li>
                <li role="presentation"><a href="#future" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Future Payments</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="present" aria-labelledby="present-tab">
                    <div ng-controller="clientPaymentGridCtrl">
                        <div style="display: none !important;">
                            {!! Form::text('client_id', $client, ['init-model'=>'client_id']) !!}
                            {!! Form::text('period', 'present', ['init-model'=>'period']) !!}
                        </div>
                        @include('investment.interest.partials.schedule_table')
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="past" aria-labelledby="past-tab">
                    <div ng-controller="clientPaymentGridCtrl">
                        <div style="display: none !important;">
                            {!! Form::text('client_id', $client, ['init-model'=>'client_id']) !!}
                            {!! Form::text('period', 'past', ['init-model'=>'period']) !!}
                        </div>
                        @include('investment.interest.partials.schedule_table')
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="future" aria-labelledby="future-tab">
                    <div ng-controller="clientPaymentGridCtrl">
                        <div style="display: none !important;">
                            {!! Form::text('client_id', $client, ['init-model'=>'client_id']) !!}
                            {!! Form::text('period', 'future', ['init-model'=>'period']) !!}
                        </div>
                        @include('investment.interest.partials.schedule_table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection