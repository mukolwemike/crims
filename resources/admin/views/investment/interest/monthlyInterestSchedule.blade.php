@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <ul id = "myTabs" class = "nav nav-tabs" role = "tablist">
                @foreach(Product::all() as $product)
                    <li role = "presentation" class = "">
                        <a href = "#{!! $product->id !!}" id = "home-tab" role = "tab" data-toggle = "tab" aria-controls = "home" aria-expanded = "false">{!! $product->name !!}</a>
                    </li>
                @endforeach
            </ul>

            <div id = "myTabContent" class = "tab-content">
                @foreach(Product::all() as $product)
                    <div role = "tabpanel" class = "tab-pane fade" id = "{!! $product->id !!}" aria-labelledby = "home-tab" ng-controller = "DatepickerCtrl">
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr><td><a href="/dashboard/investments/clientinvestments/interest/fullschedule" class="btn btn-success"><i class="fa fa-calendar-o"></i> View full schedule</a></td></tr>
                            <tr><td colspan="4"><div class="alert alert-info"><p>Displaying upcoming interest payments for the month</p></div></td></tr>
                            <tr><th>Client Name</th><th>Date</th><th>Amount</th><th>details</th></tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                                @if($client->repo->hasProduct($product))
                                    <?php
                                        $schedules = $repo->getClientsSchedulesForProductThisMonth($client, $product);
                                    ?>
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                                        @if($schedules->count() > 0)
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedules->first()->date) !!}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($repo->getTotalInterestForClientForProductThisMonth($client, $product)) !!}</td>
                                        <td><a ng-controller="PopoverCtrl" uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/interest/schedule/{!! $client->id !!}"><i class="fa fa-list-alt"></i></a></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection