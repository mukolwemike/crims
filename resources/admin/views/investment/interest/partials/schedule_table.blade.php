<table st-table="displayedCollection" st-safe-src="rowCollection" class="table table-striped table-responsive">
    <thead>
    <tr>
        <th colspan="4"></th>
        <th colspan="1"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
    </tr>
    <tr>
        <th st-sort="client_name">Client Name</th>
        <th st-sort="investment.amount">Invested Amount</th>
        <th st-sort="date">Due Date</th>
        <th st-sort="description">Description</th>
        <th st-sort="amount">Interest Amount</th>
    </tr>
    </thead>
    <tbody>
    <tr ng-repeat="row in displayedCollection">
        <td><% row.client_name %></td>
        <td><% row.investment | currency:"" %></td>
        <td><% row.date | date %></td>
        <td><% row.description %></td>
        <td><% row.amount %></td>
    </tr>
    </tbody>
    <tfoot>
    <tr><td><p class="bold margin-top-20">Total</p></td><td colspan="3"></td><td><p class="bold margin-top-20"><% totalAmount | currency:"" %></p></td></tr>
    <tr>
        <td  colspan="2" class="text-center">
            Items per page
        </td>
        <td colspan="2" class="text-center">
            <input type="text"  ng-model="itemsByPage"/>
        </td>
        <td colspan="4" class="text-center">
            <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="10"></div>
        </td>
    </tr>
    </tfoot>
</table>