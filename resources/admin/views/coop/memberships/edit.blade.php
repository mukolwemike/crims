@extends('layouts.default')

@section('content')
    <div class="col-md-11" ng-controller="CoopController">

        <div class="col-md-12 panel panel-dashboard form-grouping">
            {!! Form::model($form, ['route'=>['update_coop_membership', $form->id], 'method'=>'PUT']) !!}

                    <!-- PERSONAL DETAILs -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12"><ol type="A"><li>Full Names of Applicant</li></ol> </h4>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('title', 'Full name') !!}</span></div>

                    <div class="col-md-1">{!! Form::select('title_id', App\Cytonn\Models\Title::pluck('name', 'id') , null, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}</div>

                    <div class="col-md-3">{!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>

                    <div class="col-md-3">{!! Form::text('middlename', null, ['class'=>'form-control', 'placeholder'=>'Middle name']) !!}</div>

                    <div class="col-md-3">{!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('dob', 'Date of birth') !!}</div>

                    <div class="col-md-4" ng-controller="DatepickerCtrl">
                        <div class="input-group">
                            {!! Form::text('dob', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group group">
                    <div class="col-md-2">{!! Form::label('id_or_passport', 'ID/Passport no.') !!}</div>

                    <div class="col-md-4">{!! Form::text('id_or_passport', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_address', 'Postal Address') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_code', 'Postal code') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('town', 'Town') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('town', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('street', 'Physical Address / Street') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('street', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('email', 'Email') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('email', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('phone', 'Mobile/Telephone') !!}</div>

                    <div class="col-md-4">{!! Form::text('phone', null, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('pin_no', 'PIN No.') !!}</div>

                    <div class="col-md-4">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('employment_id', 'Employment Status') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('employment_id', App\Cytonn\Models\Employment::pluck('name', 'id'), null, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('country_id', 'Country of Residence') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('country_id', App\Cytonn\Models\Country::pluck('name', 'id'), '114', ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('client_code', 'Membership No.') !!}</div>

                    <div class="col-md-4">{!! Form::text('client_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}</div>
                </div>

                <div class="clearfix"></div>
                <hr>

                <h4 class="form-separator col-md-12">Referee</h4>

                <div class="row" ng-controller="ClientUserCtrl">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Search Referee by Name</label>
                            <input type="text" ng-model="selectedClient" placeholder="Enter the referee name" typeahead="client as client.fullName for client in searchClients($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                            <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                            <div ng-show="noResults">
                                <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>

                            <div class="hide">
                                <input name="referee_id" type="text" ng-model="selectedClient.id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5>Referee Details</h5>
                            <table class="table table-responsive">
                                <thead></thead>
                                <tbody>
                                <tr>
                                    <td>Full Name</td><td><% selectedClient.fullName %></td>
                                </tr>
                                <tr>
                                    <td>Email</td><td><% selectedClient.contact.email %></td>
                                </tr>
                                <tr>
                                    <td>Member Number</td><td><% selectedClient.client_code %></td>
                                </tr>
                                <tr>
                                    <td>ID Number</td><td><% selectedClient.id_or_passport %></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PERSONAL DETAILS -->


            <!-- MEMBERSHIP DETAILS -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12">
                    <ol type="A" start="2">
                        <li>Membership Fee &amp; Share Purchase</li>
                    </ol>
                </h4>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('membership_fee', 'Membership Fee') !!}</div>

                    <div class="col-md-4">{!! Form::text('membership_fee', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('share_purchase', 'Share Purchase') !!}</div>

                    <div class="col-md-4">{!! Form::text('share_purchase', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('mode_of_payment', 'Mode of Payment') !!}</div>

                    <div class="col-md-4">{!! Form::select('mode_of_payment', ['Direct Bank Deposit'=>'Direct Bank Deposit', 'M-Pesa Paybill'=>'M-Pesa Paybill', 'Human Resource Remittance'=>'HR Remittance (Cytonn Staff)'], null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('payment_options', 'Payment Options') !!}</div>

                    <div class="col-md-4">{!! Form::select('payment_options', ['1'=>'Monthly', '3'=>'Quarterly', '6'=>'Semi-Annaully', '12'=>'Annually'], null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('amount', 'Amount') !!}</div>

                    <div class="col-md-4">{!! Form::text('amount', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('date', 'Payment Date') !!}</div>

                    <div class="col-md-4" ng-controller="DatepickerCtrl">
                        <div class="input-group">
                            {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('narrative', 'Narrative') !!}</div>

                    <div class="col-md-4">{!! Form::text('narrative', null, ['class'=>'form-control']) !!}</div>
                </div>
            </div>
            <!-- END MEMBERSHIP DETAILS -->

            <!-- NOMINEES -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12">
                    <ol type="A" start="4">
                        <li>Nominees</li>
                    </ol>
                </h4>

                <div class="form-group">
                    <div class="col-md-1"><label>No</label></div>
                    <div class="col-md-3"><label>Name</label></div>
                    <div class="col-md-3"><label>ID Number</label></div>
                    <div class="col-md-3"><label>Relationship</label></div>
                    <div class="col-md-1"><label>% Share</label></div>
                    <div class="col-md-1"></div>
                </div>

                <div class="clearfix"></div>
                <hr>

                <div class="form-group">
                    <div class="col-md-12" data-ng-repeat="nominee in nominees">
                        <div class="col-md-1"><% nominee.id %>.</div>
                        <div class="col-md-3">{!! Form::text('client_contact_person_name[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_contact_person_name') !!}</div>
                        <div class="col-md-3">{!! Form::text('id_number[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_number') !!}</div>
                        <div class="col-md-3">{!! Form::select('relationship_id[]', $relationships, null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'relationship_id') !!}</div>
                        <div class="col-md-1">
                            {!! Form::text('percentage_share[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'percentage_share') !!}
                        </div>
                        <div class="col-md-1"><button type="button" class="btn btn-danger" ng-show="$last" ng-click="removeNominee()"><i class="fa fa-remove"></i></button></div>
                    </div>
                    <button type="button" class="btn btn-primary pull-right" ng-click="addNewNominee()"><i class="fa fa-plus-circle"> Add Nominee</i></button>
                </div>
            </div>
            <!-- END NOMINEES -->


            <div class="detail-group">
                <div class="col-md-12 margin-top-20">
                    {!! Form::submit('Update Membership Form', ['class'=>'btn btn-success']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
