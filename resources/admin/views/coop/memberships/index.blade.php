@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="CoopMembershipFormsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="5"></th>
                        <th colspan="3"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                    </tr>
                    <tr>
                        <th st-sort="client_code">Client Code</th>
                        <th>Name</th>
                        <th>Date of Birth</th>
                        <th>ID/Passport</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Approval</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.client_code %></td>
                        <td><% row.fullName %></td>
                        <td><% row.dob %></td>
                        <td><% row.id_or_passport %></td>
                        <td><% row.email %></td>
                        <td><% row.phone %></td>
                        <td class="text-center">
                            <i ng-show="row.approval == true" class="fa fa-check-circle-o" style="color:green;"></i>
                            <i ng-show="row.approval == false" style="color:red;">Pending</i>
                        </td>
                        <td><a ng-show="row.approval == false" href="/dashboard/coop/memberships/edit/<% row.id%>" class="margin-bottom-20"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="8" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "4" class = "text-center">
                        Items per page <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop