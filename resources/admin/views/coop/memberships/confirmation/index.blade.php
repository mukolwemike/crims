@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "CoopMembershipConfirmationsController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan = "6">
                        <div class="btn-group">
                            <a type="button" href="#" class="btn btn-success">Investment</a>
                            <a href="/dashboard/coop/membership/confirmations" class="btn btn-info">Memberships</a>
                            <a type="button" href="/dashboard/shares/confirmations" class="btn btn-primary active">Shares</a>
                        </div>
                    </th>
                    <th colspan="2">
                            <select st-search="confirmation" class="form-control">
                                <option value="">All</option>
                                <option value="sent">Sent</option>
                                <option value="not_sent">Not Sent</option>
                            </select>
                    </th>
                    <th><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                </tr>
                <tr>
                    <th>Membership No.</th>
                    <th>Client Name</th>
                    <th>Total Payment</th>
                    <th>Shares</th>
                    <th>Products</th>
                    <th>Membership Fees</th>
                    <th>Date</th>
                    <th>Confirmation</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody ng-show="!isLoading">
                <tr ng-repeat = "row in displayed">
                    <td><% row.client_code %></td>
                    <td><% row.full_name %></td>
                    <td><% row.membership_confirmation.data.total_payment | currency:"" %></td>
                    <td><% row.membership_confirmation.data.shares | currency:"" %></td>
                    <td><% row.membership_confirmation.data.products | currency:"" %></td>
                    <td><% row.membership_confirmation.data.membership_fees | currency:"" %></td>
                    <td><% row.date | date %></td>
                    <td><span to-html = "row.membership_confirmation.data.confirmation | confirmationStatus"></span></td>
                    <td>
                        <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/coop/membership/confirmations/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                    </td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="10" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr >
                    <td colspan = "100%" class = "text-center">
                        <dmc-pagination></dmc-pagination>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop