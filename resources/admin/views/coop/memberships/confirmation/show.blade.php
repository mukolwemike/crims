@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Send Shares Business Confirmation</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Membership No.</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Member from</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
                    </tr>
                </tbody>
            </table>

            <div class = "panel panel-success">
                <div class = "panel-heading">
                    <h4>Membership confirmation</h4>
                </div>
                <div class = "panel-body">
                    {!! Form::open() !!}

                        <div class="alert alert-info">
                            <p>Select the payments to include in the confirmation</p>
                        </div>
                        <table class = "table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Amount</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $p)
                                    <tr>
                                        <td>{!! Form::checkbox('payment_id[]', $p->id) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($p->amount) !!}</td>
                                        <td>{!! $p->description !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if($is_bc_sent)
                            <input type="radio" name="send" value="true" data-toggle="modal" data-target="#confirmBCResend" />
                            {!! Form::label('Send Business Confirmation') !!} ( Sent : {!! \Cytonn\Presenters\BooleanPresenter::presentIcon($is_bc_sent) !!} )

                            <div class="form-group">
                                {!! Form::radio('send', false, true) !!}

                                {!! Form::label('Preview') !!}
                            </div>
                        @else
                            <div class="form-group">
                                {!! Form::radio('send', true, false) !!}

                                {!! Form::label('Send Business Confirmation') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::radio('send', false, true) !!}

                                {!! Form::label('Preview') !!}
                            </div>
                        @endif

                        <br>
                        <br>
                        <div class="form-group">
                            {!! Form::submit('Go', ['class'=>'btn btn-success']) !!}
                        </div>

                    {!! Form::close() !!}
                </div>

                <div class = "panel-heading">
                    <h4>Previous Business confirmations</h4>
                </div>
                <div class = "panel-body">
                    <table class = "table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>Sent On</th>
                                <th>Sent By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($previous as $confirmation)
                                <tr>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDateTime($confirmation->created_at) !!}</td>
                                    <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNamesNoTitle($confirmation->sent_by) !!}</td>
                                    <td>
                                      <a ng-controller = "PopoverCtrl" uib-popover = "View PDF" popover-trigger = "mouseenter" href = "/dashboard/coop/membership/confirmations/replay/{!! $confirmation->id !!}"><i class = "fa fa-list-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="confirmBCResend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Send Business Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>This business confirmation has already been sent. Are you sure that you want to resend it?</p>
                    {!! Form::hidden('send', true) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {!! Form::submit('Resend', ['class'=>'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

