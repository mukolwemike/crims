@extends('reports.letterhead')

@section('content')
    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Member No. – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <!-- RE -->
    <p class="bold-underline">MEMBERSHIP CONFIRMATION - MEMBER NO. {!! strtoupper($client->client_code) !!}</p>

    <!-- Body -->
    <p>Cytonn Investment Co-operative Society Limited takes this opportunity to thank you for investing with us.</p>
    <p>This is to confirm that we are in receipt of your KES {!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}. The Transaction is summarized as follows:</p>

    <table class="table table-responsive">
        <tbody>
            @foreach($payments as $payment)
                <tr>
                    <td>{!! $payment->description !!}</td><td>KES {!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount)) !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>
        In case of any question, comment or assistance, we are at your service. Our relationship team will be at hand to assist you with all your investment needs. Please do not hesitate to contact us on +254 709 101 000 or email us at
        <a href="mailto:coop@cytonn.com">coop@cytonn.com</a>
    </p>
    <p>
        Once again, we thank you for choosing us to serve your investment needs and look forward to delivering to our investment promise.
    </p>
    <p>&nbsp;</p>

    <p>Yours sincerely,</p>
    <p><strong>For: {!! $client->fundManager->fullname !!}</strong></p>

    <p><strong>{!! $email_sender !!}</strong></p>
@stop