@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="CoopClientsGridController">
            <a href="/dashboard/coop/payments" class="btn btn-primary margin-bottom-20"><i class="fa fa-list-alt"> View All Payments</i></a>
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th st-sort="id">ID</th>
                    <th st-sort="client_code">Client Code</th>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Phone Number</th>
                    <th>Client Type</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.client_code %></td>
                        <td><a href="/dashboard/coop/clients/<% row.id %>" style="text-decoration:none;"><% row.fullName %></a></td>
                        <td><% row.email %></td>
                        <td><% row.phone %></td>
                        <td><% row.type %></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success" ng-controller="PopoverCtrl" uib-popover="Make Payment" popover-trigger="mouseenter" data-toggle="modal" data-target="#addPayment<% row.id %>"><i class="fa">Payment</i></button>
                                <button type="button" class="btn btn-primary" ng-controller="PopoverCtrl" uib-popover="Invest In Product" popover-trigger="mouseenter" data-toggle="modal" data-target="#investInProduct<% row.id %>"><i class="fa">Product</i></button>
                                <button type="button" class="btn btn-default" ng-controller="PopoverCtrl" uib-popover="Buy Shares" popover-trigger="mouseenter" data-toggle="modal" data-target="#buyShares<% row.id %>"><i class="fa">Shares</i></button>
                            </div>

                            <!-- Payment Modal -->
                            <div class="modal fade" id="addPayment<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open(['route'=>['make_coop_payment']]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Make Coop Payment</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="hidden" name="client_id" value="<% row.id %>">
                                                {!! Form::label('date', 'Date of Payment') !!}

                                                {!! Form::text('date', null, ['class'=>'form-control', 'init-model'=>'date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('amount', 'Amount Paid') !!}

                                                {!! Form::text('amount', null, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('narrative', 'Narrative') !!}

                                                {!! Form::text('narrative', null, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            {!! Form::submit('Submit Payment', ['class'=>'btn btn-danger']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="8" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "4" class = "text-center">
                        Items per page <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop