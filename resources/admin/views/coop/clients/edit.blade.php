@extends('layouts.default')

@section('content')
    <div class="col-md-11" ng-controller="CoopController">

        <div class="col-md-12 panel panel-dashboard form-grouping">
            {!! Form::model($client, ['url'=>'/dashboard/coop/clients/'.$client->id, 'method'=>'PUT']) !!}

            <!-- PERSONAL DETAILs -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12"><ol type="A"><li>Full Names of Applicant</li></ol> </h4>
                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('title', 'Full name') !!}</span></div>

                    <div class="col-md-1">{!! Form::select('title_id', $titles , $client->contact, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}</div>

                    <div class="col-md-3">{!! Form::text('middlename', $client->contact->middlename, ['class'=>'form-control', 'placeholder'=>'Surname']) !!}</div>

                    <div class="col-md-3">{!! Form::text('firstname', $client->contact->firstname, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>

                    <div class="col-md-3">{!! Form::text('lastname', $client->contact->lastname, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('dob', 'Date of birth') !!}</div>

                    <div class="col-md-4" ng-controller="DatepickerCtrl">
                        <div class="input-group">
                            {!! Form::text('dob', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group group">
                    <div class="col-md-2">{!! Form::label('id_or_passport', 'ID/Passport no.') !!}</div>

                    <div class="col-md-4">{!! Form::text('id_or_passport', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_address', 'Postal Address') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_code', 'Postal code') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('email', 'Email') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('email', $client->contact->email, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('phone', 'Mobile/Telephone') !!}</div>

                    <div class="col-md-4">{!! Form::text('phone', $client->contact->phone, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('pin_no', 'PIN No.') !!}</div>

                    <div class="col-md-4">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('employment_status', 'Employment Status') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('employment_status', ['employed'=>'Employed', 'self_employed'=>'Self Employed'], null, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('country_id', 'Country of Residence') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('country_id', $countries, null, ['class'=>'form-control']) !!}</div>
                </div>

            </div>
            <!-- END PERSONAL DETAILS -->


            <div class="detail-group">
                <div class="col-md-12 margin-top-20">
                    {!! Form::submit('Update Coop Client', ['class'=>'btn btn-success']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
