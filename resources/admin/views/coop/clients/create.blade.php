@extends('layouts.default')

@section('content')
    <div class="col-md-11" ng-controller="CoopController">

        <div class="col-md-12 panel panel-dashboard form-grouping">
            {!! Form::open(['route'=>'store_coop_client']) !!}

            <div class="detail-group">
                <h4 class="col-md-12">Client Type</h4>
                <div class="clearfix"></div>
                <hr>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">{!! Form::label('client_type_id', 'Select Client type') !!}</div>
                        <div class="col-md-8">{!! Form::select('client_type_id', $client_types, null, ['class'=>'form-control', 'init-model'=>'client_type']) !!}
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4"> <span class="required-field">{!! Form::label('complete', 'Client type') !!}</span></div>

                        <div class="col-md-8">{!! Form::select('complete', [true=>'Complete', false=>'Incomplete'], true, ['class'=>'form-control', 'init-model'=>'complete']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'complete') !!}
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4"> <span class="required-field">{!! Form::label('client_status', 'Client Status') !!}</span></div>

                        <div class="col-md-8">{!! Form::select('client_status', [1 => 'New Client', 2 => 'Duplicate Client'], 1, ['class'=>'form-control', 'init-model'=>'client_status']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_status') !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="detail-group">

            <!--Individual details -->
            <div class="col-md-12">
                <div class="form-group" ng-if="client_type == '1'">
                    <h4 class="col-md-12">A. Details of Individual Client Applicant</h4>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('title', 'Full name') !!}</span></div>

                    <div class="col-md-1">{!! Form::select('title_id', $titles , null, ['class'=>'form-control', 'placeholder'=>'Title', 'required']) !!}</div>

                    <div class="col-md-3">{!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Last name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>

                    <div class="col-md-3">{!! Form::text('middlename', null, ['class'=>'form-control', 'placeholder'=>'Middle name']) !!}</div>

                    <div class="col-md-3">{!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'First name', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2">{!! Form::label('dob', 'Date of birth') !!}</div>

                    <div class="col-md-4" ng-controller="DatepickerCtrl">
                        <div class="input-group">
                            {!! Form::text('dob', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'dob') !!}
                        </div>
                    </div>
                </div>

                <div class="form-group group" ng-if="client_type == '1'">
                    <div class="col-md-2">{!! Form::label('id_or_passport', 'ID/Passport no.') !!}</div>

                    <div class="col-md-4">{!! Form::text('id_or_passport', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_address', 'Postal Address') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_code', 'Postal code') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('town', 'Town') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('town', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('street', 'Physical Address / Street') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('street', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"> <span class="required-field">{!! Form::label('email', 'Email') !!}</span></div>

                    <div class="col-md-4">{!! Form::text('email', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2">{!! Form::label('phone', 'Mobile/Telephone') !!}</div>

                    <div class="col-md-4">{!! Form::text('phone', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}</div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2">{!! Form::label('pin_no', 'PIN No.') !!}</div>

                    <div class="col-md-4">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>
                </div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('employment_id', 'Employment Status') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('employment_id', $employments, null, ['class'=>'form-control']) !!}</div>
                </div>
                <div class="clearfix"></div>

                <div class="form-group" ng-if="client_type == '1'">
                    <div class="col-md-2"><span class="required-field">{!! Form::label('country_id', 'Country of Residence') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('country_id', $countries, '114', ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('client_code', 'Membership No.') !!}</div>

                    <div class="col-md-4">{!! Form::text('client_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}</div>
                </div>
            </div>
            <!-- End of individual details -->

            <!-- Start of corporate details -->

            <div class="col-md-12" ng-if="client_type == '2'">

                <h4 class="col-md-12">A. Details of Corporate Client Applicant</h4>
                <div class="clearfix"></div>
                <hr>

                <div class="form-group">

                    <div class="col-md-12" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('corporate_investor_type', 'Entity tpe') !!}
                                {!! Form::select('corporate_investor_type', $corporate_entities, null , ['class'=>'form-control']) !!}
                            </div>

                            <div class="col-md-6">
                               {!! Form::label('corporate_investor_type_other', 'Other (specify)') !!}
                                {!! Form::text('corporate_investor_type_other', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_type') !!}
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4"><span class="required-field">{!! Form::label('registered_name', 'Registered name') !!}</span></div>

                            <div class="col-md-8">{!! Form::text('registered_name', null, ['class'=>'form-control'], 'required') !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_registered_name') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4"><span class="required-field">{!! Form::label('phone', 'Telephone') !!}</span></div>

                            <div class="col-md-8">{!! Form::text('phone', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4">{!! Form::label('trade_name', 'Trade name') !!}</div>

                            <div class="col-md-8">{!! Form::text('trade_name', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_trade_name') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4"><span class="required-field">{!! Form::label('email', 'Email') !!}</span></div>

                            <div class="col-md-8">{!! Form::text('email', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4"><span class="required-field">{!! Form::label('registered_address', 'Registered address') !!}</span></div>

                            <div class="col-md-8">{!! Form::text('registered_address', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_address') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" nng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4">{!! Form::label('street', 'Physical Address') !!}</div>

                            <div class="col-md-8">{!! Form::text('street', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'physical_address') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4">{!! Form::label('registration_number', 'Registration number') !!}</div>

                            <div class="col-md-8">{!! Form::text('registration_number', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registration_number') !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" ng-if="client_type == '2'">
                        <div class="row">
                            <div class="col-md-4">{!! Form::label('pin_no', 'company pin number') !!}</div>

                            <div class="col-md-8">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group" ng-if="client_type == '2'">
                        <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_address', 'Postal Address') !!}</span></div>

                        <div class="col-md-4">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                    </div>

                    <div class="form-group" ng-if="client_type == '2'">
                        <div class="col-md-2"> <span class="required-field">{!! Form::label('postal_code', 'Postal code') !!}</span></div>

                        <div class="col-md-4">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                    </div>

                    <div class="form-group" ng-if="client_type == '2'">
                        <div class="col-md-2"> <span class="required-field">{!! Form::label('town', 'Town') !!}</span></div>

                        <div class="col-md-4">{!! Form::text('town', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>
                    </div>

                    <div class="form-group" ng-if="client_type == '2'">
                        <div class="col-md-2"><span class="required-field">{!! Form::label('country_id', 'Country of Residence') !!}</span></div>

                        <div class="col-md-4">{!! Form::select('country_id', $countries, '114', ['class'=>'form-control']) !!}</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('method_of_contact_id', 'Preferred Method of contact') !!}</div>

                        <div class="col-md-4">{!! Form::select('method_of_contact_id', $contactMethods, null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'method_of_contact_id') !!}</div>
                    </div>

                </div>
            </div>

            <div class="form-group" ng-if="client_type == '2'">
                <div class="col-md-2"> <span class="required-field">{!! Form::label('contact_person', 'Contact person') !!}</span></div>

                <div class="col-md-2">{!! Form::select('contact_person_title', $titles , null, ['class'=>'form-control', 'placeholder'=>'Title']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_title') !!}
                </div>

                <div class="col-md-4">{!! Form::text('contact_person_lname', null, ['class'=>'form-control', 'placeholder'=>'Last name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_lname') !!}</div>


                <div class="col-md-4">{!! Form::text('contact_person_fname', null, ['class'=>'form-control', 'placeholder'=>'First name']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_fname') !!}</div>
                <div class="clearfix"></div>
            </div>
            <!-- End of corporate details -->

            <div class="clearfix"></div>
            <hr>

            <h4 class="col-md-12">Referee</h4>
            <div class="clearfix"></div>
            <hr>

            <div class="row" ng-controller="ClientUserCtrl">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Search Referee by Name</label>
                        <input type="text" ng-model="selectedClient" placeholder="Enter the referee name" typeahead="client as client.fullName for client in searchClients($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                        <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                        <div ng-show="noResults">
                            <i class="glyphicon glyphicon-remove"></i> No Results Found
                        </div>

                        <div class="hide">
                            <input name="referee_id" type="text" ng-model="selectedClient.id">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h5>Referee Details</h5>
                        <table class="table table-responsive">
                            <thead></thead>
                            <tbody>
                                <tr>
                                    <td>Full Name</td><td><% selectedClient.fullName %></td>
                                </tr>
                                <tr>
                                    <td>Email</td><td><% selectedClient.contact.email %></td>
                                </tr>
                                <tr>
                                    <td>Member Number</td><td><% selectedClient.client_code %></td>
                                </tr>
                                <tr>
                                    <td>ID Number</td><td><% selectedClient.id_or_passport %></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PERSONAL DETAILS -->


            <!-- MEMBERSHIP DETAILS -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12">
                    <ol type="A" start="2">
                        <li>Membership Fee &amp; Share Purchase</li>
                    </ol>
                </h4>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('membership_fee', 'Membership Fee') !!}</div>

                    <div class="col-md-4">{!! Form::text('membership_fee', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('share_purchase', 'Amount For Share Purchase') !!}</div>

                    <div class="col-md-4">{!! Form::text('share_purchase', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('entity_id', 'Shares Entity') !!}</div>

                    <div class="col-md-4">{!! Form::select('entity_id', $share_entities, null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('category_id', 'Shares Category') !!}</div>

                    <div class="col-md-4">{!! Form::select('category_id', $share_categories, null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('mode_of_payment', 'Mode of Payment') !!}</div>

                    <div class="col-md-4">{!! Form::select('mode_of_payment', ['Direct Bank Deposit'=>'Direct Bank Deposit', 'M-Pesa Paybill'=>'M-Pesa Paybill', 'Human Resource Remittance'=>'HR Remittance (Cytonn Staff)'], null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('payment_options', 'Payment Options') !!}</div>

                    <div class="col-md-4">{!! Form::select('payment_options', ['1'=>'Monthly', '3'=>'Quarterly', '6'=>'Semi-Annaully', '12'=>'Annually'], null, ['class'=>'form-control']) !!}</div>
                </div>
            </div>
            <!-- END MEMBERSHIP DETAILS -->


            <!-- PRODUCT PLANS -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12">
                    <ol type="A" start="3">
                        <li>Product Plans</li>
                    </ol>
                </h4>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('shares', 'Shares per Month') !!}</div>

                    <div class="col-md-4">{!! Form::text('shares', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('product_id', 'Select Product') !!}</div>

                    <div class="col-md-4">{!! Form::select('product_id', [null=>null]+$coop_products, null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('duration', 'Investment Duration (years)') !!}</div>

                    <div class="col-md-4">{!! Form::text('duration', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('amount_paid_for_product', 'Amount Paid (monthly)') !!}</div>

                    <div class="col-md-4">{!! Form::text('amount_paid_for_product', null, ['class'=>'form-control']) !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('payment_date', 'Payment Date') !!}</div>

                    <div class="col-md-4" ng-controller="DatepickerCtrl">
                        <div class="input-group">
                            {!! Form::text('payment_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"payment_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PRODUCT PLANS -->


            <!-- NOMINEES -->
            <div class="detail-group">
                <h4 class="form-separator col-md-12">
                    <ol type="A" start="4">
                        <li>Nominees</li>
                    </ol>
                </h4>

                <div class="form-group">
                    <div class="col-md-1"><label>No</label></div>
                    <div class="col-md-3"><label>Name</label></div>
                    <div class="col-md-3"><label>ID Number</label></div>
                    <div class="col-md-3"><label>Relationship</label></div>
                    <div class="col-md-1"><label>% Share</label></div>
                    <div class="col-md-1"></div>
                </div>

                <div class="clearfix"></div>
                <hr>

                <div class="form-group">
                    <div class="col-md-12" data-ng-repeat="nominee in nominees">
                        <div class="col-md-1"><% nominee.id %>.</div>
                        <div class="col-md-3">{!! Form::text('client_contact_person_name[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_contact_person_name') !!}</div>
                        <div class="col-md-3">{!! Form::text('id_number[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_number') !!}</div>
                        <div class="col-md-3">{!! Form::select('relationship_id[]', $relationships, null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'relationship_id') !!}</div>
                        <div class="col-md-1">
                            {!! Form::text('percentage_share[]', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'percentage_share') !!}
                        </div>
                        <div class="col-md-1"><button type="button" class="btn btn-danger" ng-show="$last" ng-click="removeNominee()"><i class="fa fa-remove"></i></button></div>
                    </div>
                    <button type="button" class="btn btn-primary pull-right" ng-click="addNewNominee()"><i class="fa fa-plus-circle"> Add Nominee</i></button>
                </div>
            </div>
            <!-- END NOMINEES -->


            <div class="detail-group">
                <div class="col-md-12 margin-top-20">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
