<?php
is_array($client->emails) ? : $client->emails = [];
?>

@extends('layouts.default')
<style>
    .bold {font-weight: bold;}
</style>
@section('content')
    <div class="col-md-10 col-md-offset-1" ng-controller="CoopClientPaymentsGridController">
        <div class="col-md-12 panel-dashboard">
            <a class="btn btn-primary" target="_self" href="/dashboard/clients/details/<% client_id %>"><i class="fa fa-arrow-left"> Back to Client</i></a>
            <h4 class="col-md-12 bold">Coop Payments</h4>
            <table  st-pipe="callServer" st-table="displayed" class="table table-hover table-responsive table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Narrative</th>
                        <th>Receipt</th>
                        <th>Payment</th>
                    </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.date %></td>
                        <td><% row.narrative %></td>
                        <td><span ng-if="row.amount > 0"><% row.amount | currency:"" %></span></td>
                        <td><span ng-if="row.amount <= 0"><% row.amount | abs | currency:"" %></span></td>
                    </tr>
                    <tr style="color:red; background:yellow;">
                        <th colspan="2" class="text-center">BALANCE</th>
                        <th colspan="2" class="text-center">{!! \Cytonn\Presenters\AmountPresenter::currency($client->coopPaymentsBalance()) !!}</th>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="4" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan = "2" class = "text-center">
                            Items per page <input type = "text" ng-model = "itemsByPage"/>
                        </td>
                        <td colspan = "2" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop