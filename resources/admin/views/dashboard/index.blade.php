@extends('layouts.default')

@section('content')
   <div class="col-md-12">
       <div class="dashboard">

           <div class="row">
               <div class="col-md-12">
                   @if(allowed('investments:view-analytics'))
                       <div class="dashboard-item">
                           <div class="panel-body">
                               <div class="row">
                                   <div class="col-xs-4"><i class="fa fa-users fa-3x pull-left"></i></div>
                                   <div class="col-xs-8">
                                       <div class="item-content pull-right">
                                           <div class="huge">
                                               {!! $gen->getActiveClientCount() !!}
                                           </div>
                                           <p>Active Clients</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <a href="/dashboard/investments/summary">
                               <div class="panel-footer">
                                   <p class="pull-left">See Summary</p>
                                   <i class="fa fa-arrow-circle-o-right pull-right"></i>
                                   <div class="clearfix"></div>
                               </div>
                           </a>
                       </div>
                   @endif
                   @if(allowed('investments:view-analytics'))
                       <div class="dashboard-item">
                           <div class="panel-body table-responsive">
                               <div class="row">
                                   <div class="col-xs-4">
                                       <i class="fa fa-table fa-3x pull-left"></i>
                                   </div>
                                   <div class="col-xs-8">
                                       <div class="item-content pull-right">
                                           <div class="huge">
                                               {!! $gen->getActiveClientInvestmentCount() !!}
                                           </div>
                                           <p>Client Investments</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <a href="/dashboard/investments/clientinvestments">
                               <div class="panel-footer">
                                   <p class="pull-left">See Investments</p>
                                   <i class="fa fa-arrow-circle-o-right pull-right"></i>
                                   <div class="clearfix"></div>
                               </div>
                           </a>
                       </div>
                   @endif

                   @if(allowed('portfolio:view-analytics'))
                       <div class="dashboard-item">
                           <div class="panel-body">
                               <div class="row">
                                   <div class="col-xs-4">
                                       <i class="fa fa-institution fa-3x pull-left"></i>
                                   </div>
                                   <div class="col-xs-8">
                                       <div class="item-content pull-right">
                                           <div class="huge">
                                               {!! $gen->getActivePortfolioInvestmentCount() !!}
                                           </div>
                                           <p>Portfolio Investments</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <a href="/dashboard/portfolio/deposit-holdings">
                               <div class="panel-footer">
                                   <p class="pull-left">See Investments</p>
                                   <i class="fa fa-arrow-circle-o-right pull-right"></i>
                                   <div class="clearfix"></div>
                               </div>
                           </a>
                       </div>
                   @endif

                   @if(allowed('client-approvals:view'))
                       <div class="dashboard-item">
                           <div class="panel-body">
                               <div class="row">
                                   <div class="col-xs-4">
                                       <i class="fa fa-check-square-o fa-3x pull-left"></i>
                                   </div>
                                   <div class="col-xs-8">
                                       <div class="item-content pull-right">
                                           <div class="huge">
                                               {!! $gen->getClientTransactionsAwaitingApprovalCount() !!}
                                           </div>
                                           <p>Client Transactions</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <a href="/dashboard/investments/approve">
                               <div class="panel-footer">
                                   <p class="pull-left">See Transactions</p>
                                   <i class="fa fa-arrow-circle-o-right pull-right"></i>
                                   <div class="clearfix"></div>
                               </div>
                           </a>
                       </div>
                   @endif

                   @if(allowed('viewinvestments'))
                       <div class="dashboard-item">
                           <div class="panel-body">
                               <div class="row">
                                   <div class="col-xs-4">
                                       <i class="fa fa-check-square-o fa-3x pull-left"></i>
                                   </div>
                                   <div class="col-xs-8">
                                       <div class="item-content pull-right">
                                           <div class="huge">
                                               {!! $gen->getPortfolioTransactionsAwaitingApprovalCount() !!}
                                           </div>
                                           <p>Portfolio Transactions</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <a href="/dashboard/portfolio/approve">
                               <div class="panel-footer">
                                   <p class="pull-left">See Transactions</p>
                                   <i class="fa fa-arrow-circle-o-right pull-right"></i>
                                   <div class="clearfix"></div>
                               </div>
                           </a>
                       </div>
                   @endif
               </div>
           </div>

           <div class="row">
               <div class="col-md-12">
                   <div class="dashboard-item-lg" ng-controller = "NotificationController">
                       <div class="panel panel-success">
                           <div class="panel-heading dashboard-panel-title">
                               <h5>Notifications</h5>
                           </div>


                           <table st-pipe="callServer" st-table="displayed" class = "table table-responsive table-hover">
                               <thead>
                               </thead>
                               <tbody  ng-show="!isLoading">
                               <tr ng-repeat = "row in displayed">
                                   <td>

                                       <div  class="notification-row">
                                           <a ng-href="<% row.location %>">
                                               <div class="pull-left">
                                               <span ng-show="row.read != 1">
                                                    <strong><% row.content %></strong>
                                               </span>
                                                   <span ng-hide="row.read != 1">
                                                    <% row.content %>
                                               </span>
                                               </div>
                                               <div class="pull-right">
                                                   <time am-time-ago="row.time"></time>
                                               </div>
                                               <span class="clearfix"></span>
                                           </a>
                                       </div>
                                   </td>
                               </tr>
                               <tr>
                                   <td  class="notification-footer">
                                       <div class="pull-right" st-pagination = "" st-items-by-page = "itemsByPage"></div>
                                   </td>
                               </tr>
                               </tbody>
                               <tbody ng-show="isLoading">
                               <tr>
                                   <td colspan="6" class="text-center">Loading ... </td>
                               </tr>
                               </tbody>

                           </table>
                       </div>
                   </div>

                   @if(allowed('investments:view'))
                       <div class="dashboard-item-lg" ng-controller = "ActivityLogController">
                           <div class="panel panel-success">
                               <div class="panel-heading dashboard-panel-title">
                                   <h5><a href="/dashboard/investments/activity">Activity log</a></h5>
                               </div>


                               <table st-pipe="callServer" st-table="displayed" class = "table table-responsive table-hover">
                                   <thead>
                                   </thead>
                                   <tbody ng-show="!isLoading">
                                   <tr ng-repeat = "row in displayed">
                                       <td>

                                           <div class="notification-row">
                                               <a ng-href="<% row.target %>">
                                                   <div class="pull-left">
                                                       <% row.text %>
                                                   </div>
                                                   <div class="pull-right">
                                                       <time am-time-ago="row.time"></time>
                                                   </div>
                                                   <span class="clearfix"></span>
                                               </a>
                                           </div>

                                       </td>
                                   </tr>
                                   <tr>
                                       <td class="notification-footer">
                                           <div class="pull-right" st-pagination = "" st-items-by-page = "itemsByPage"></div>
                                       </td>
                                   </tr>
                                   </tbody>
                                   <tbody ng-show="isLoading">
                                   <tr>
                                       <td class="text-center">Loading ... </td>
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                       </div>
                   @endif
               </div>
           </div>

           <div class="clearfix"></div>
       </div>
   </div>
@stop


