@extends('layouts.default')

@section('content')
    <div class="crims_home_page">

        <div class="dashboard__salutation">
            <salutation :user="{{ json_encode(Auth::user()) }}"></salutation>
        </div>
        @if(allowed('investments:view-analytics'))

        <div class="dashboard__investment_analytics">
                <dashboard-investment-analytics> </dashboard-investment-analytics>
        </div>

        <div class="dashboard__account_summary">
            <account-summary></account-summary>
        </div>

        <div class="dashboard__charts">
            <div class="crims_dashboard_charts">
                <div class="crims_chart">
                    <div class="chart_area">
                        <div class="chart__head">
                        <span class="head__left">
                            Fund Valuation
                        </span>
                            <span class="head__right"></span>
                        </div>

                        <div class="chart__body">
                            <fund-valuation></fund-valuation>
                        </div>
                    </div>
                </div>

                <div class="crims_chart">
                    <div class="chart_area">
                        <div class="chart__head">
                        <span class="head__left">
                            Real Estate
                        </span>
                            <span class="head__right"></span>
                        </div>

                        <div class="chart__body">
                            <re_chart></re_chart>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @endif

    </div>
@stop


