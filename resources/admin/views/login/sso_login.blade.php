@extends('layouts.default')
@section('content')
    <div class="col-md-4  col-md-offset-4 login-form">
        <div class="panel panel-login">
            <div class="login-icon">
                <i class="fa fa-user fa-4x"></i>
                <p>Please login to continue</p>
            </div>

            <div class="form-group">
                <a class="btn btn-success btn-block form-control" href="/sso/authorize">Login via SSO</a>
            </div>

            <div>
                <p>Having trouble logging in? <a href="/password/remind">Click Here</a></p>
            </div>
        </div>
    </div>
@stop