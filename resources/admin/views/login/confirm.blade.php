@extends('layouts.default')
@section('content')
    <div class="col-md-4  col-md-offset-4 login-form">
        <div class="panel panel-login">
            <div class="login-icon">
                    <i class="fa fa-sign-in fa-4x"></i>
                <p>
                    Enter the six digit login token you received in your email to confirm your login.
                </p>
            </div>

            {!! Form::open(['url'=>'login/confirm/'.$username]) !!}
            <div class="form-group">
                {!! Form::text('one_time_key', null, ['class'=>'form-control', 'placeholder'=>'Login PIN', 'autocomplete'=>'off']) !!}
            </div>

            <div class="form-group">
                {!! Form::button('Submit', ['name'=>'submit', 'type'=>"submit", 'class'=>"btn btn-success btn-block"]) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop