@extends('layouts.default')
@section('content')
        <div class="col-md-4  col-md-offset-4 login-form">
            <div class="panel panel-login">
                <div class="login-icon">
                    <i class="fa fa-user fa-4x"></i>
                    <p>Please login to continue</p>
                </div>

                    {!! Form::open() !!}

                    <div class="form-group" ng-controller="FundManagerSelectCtrl">
                        <?php $fm = App::make(\Cytonn\Investment\FundManager\FundManagerScope::class); ?>
                        {!! Form::select('fund_manager', $fm->getFundManagersArr(), $fm->getSelectedFundManagerId(), ['class'=>'form-control', 'init-model'=>'selected_fm_id']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Username', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'username') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'password') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::button('login', ['type'=>'submit', 'name'=>'login', 'class'=>'form-control btn btn-success btn-block']) !!}
                    </div>
                    {!! Form::close() !!}

                <div>
                    <p>Having trouble logging in? <a href="/password/remind">Click Here</a></p>
                </div>
            </div>
        </div>
@stop