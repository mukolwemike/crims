<div>
    <p class="bold-underline">CONFIRMATION OF INVESTMENT {{ strtoupper($unitFund->name) }}</p>


    <p>On behalf of Cytonn Asset Managers, we take this opportunity to thank you for taking up investments in our {{ $unitFund->name }} and choosing us to deliver to your investment promise </p>

    <p>As per your application, your fund have been invested as per below;</p>

    <table class="table table-responsive">
        <thead>
            <tr>
                <th>Net Amount Invested</th>
                <th>Unit Price</th>
                <th>Number of Units</th>
                <th>Value Date</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td> {{ $unitFund->currency->code }}. {{ \Cytonn\Presenters\AmountPresenter::currency(($purchase->number * $purchase->unit_price) + $purchase->fees_incurred) }} </td>
                <td> {{ $unitFund->currency->code }}. {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->unit_price) }} </td>
                <td> {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number, true, 0) }} </td>
                <td> {{ Carbon\Carbon::parse($purchase->date)->toFormattedDateString() }} </td>
            </tr>
        </tbody>
    </table>

    <p>
        Following investment of your funds, you shall receive a statement via your email address at the end of the month confirming your units and the unit price.
    </p>

    <p>
        At Cytonn we are committed to delivering innovative and differentiated financial solutions that speaks to you as a client.
    </p>

    <p>
        If you have any questions, comments or need any assistance, we are at your service.
        Our relationship team will be at hand to assist you with all your investment needs.
        Please do not hesitate to contact us on +254 709 101 000 or email us at <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>
    </p>

    <p>
        Once again, we thank you for choosing us to serve your investment needs and look forward to delivering to our investment promise.
    </p>

</div>