<div class="">

    <p class="bold-underline">REF: CONFIRMATION OF INVESTMENT </p>

    <p>
        On behalf of {{ $unitFund->manager->fullname }},
        we would like to confirm having
        invested {{ $unitFund->currency->name }} {{ \Cytonn\Presenters\AmountPresenter::convertToWords($purchase->number * $purchase->price) }}
        (<b>{{ $unitFund->currency->code }}
            . {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number  * $purchase->price, true, 0) }}</b>)
        value date
        {{ Carbon\Carbon::parse($purchase->date)->toFormattedDateString() }} in our Unit Trust product offering,
        specifically the <b>{{ $unitFund->name }}</b>.
    </p>
    <p>
        You shall receive a statement via your email address at the end of the month confirming your investment and the
        interest accrued to keep you updated on your investment growth.
    </p>


    <p>If you have any questions, comments or need any assistance, we are at your service.
        Our relationship team will be at hand to assist you with all your investment needs.
        Please do not hesitate to contact us on +254 719101000 or email us at <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>
    </p>

    <p>We thank you for your investment with {{ $unitFund->manager->fullname }} and look forward to working closely with
        you in realizing your financial and investment goals.</p>
</div>