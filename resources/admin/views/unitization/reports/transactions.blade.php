<table>
    <thead>
    <tr>
        <th>Report Start Date</th>
        <th colspan="2">{!! $startDate !!}</th>
    </tr>
    <tr>
        <th>Report End Date</th>
        <th colspan="2">{!! $endDate !!}</th>
    </tr>
    <tr>
        <th>Unit Fund</th>
        <th colspan="2">{!! $unitFund->name !!}</th>
    </tr>
    <tr>
        <th>Report Title</th>
        <th colspan="2">Transaction Report</th>
    </tr>
    <tr>
        <th></th>
    </tr>
    <tr>
        <th></th>
    </tr>
    <tr>
        <th>Code</th>
        <th>Security Name</th>
        <th>Transaction</th>
        <th>Description</th>
        <th>Transaction Date</th>
        <th>Set Date</th>
        <th>Currency</th>
        <th>Maturity Date</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @if(count($data['deposits']) > 0)
        <tr>
            <td colspan="3"><strong>Deposits</strong></td>
        </tr>
    @endif
    @foreach($data['deposits'] as $securityDeposits)
        <tr>
            <td colspan="3"><strong>{!! $securityDeposits['security_details']['name'] !!}</strong></td>
        </tr>
        <tr>
            <td>{!! $securityDeposits['starting_balance']['code'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['name'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['description'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['transaction'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['trade_date'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['set_date'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['currency'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['maturity_date'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['amount'] !!}</td>
        </tr>
        @foreach($securityDeposits['investments'] as $deposit)
            <tr>
                <td>{!! $deposit['code'] !!}</td>
                <td>{!! $deposit['name'] !!}</td>
                <td>{!! $deposit['description'] !!}</td>
                <td>{!! $deposit['transaction'] !!}</td>
                <td>{!! $deposit['trade_date'] !!}</td>
                <td>{!! $deposit['set_date'] !!}</td>
                <td>{!! $deposit['currency'] !!}</td>
                <td>{!! $deposit['maturity_date'] !!}</td>
                <td>{!! $deposit['amount'] !!}</td>
            </tr>
        @endforeach
        <tr>
            <td>{!! $securityDeposits['closing_balance']['code'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['name'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['description'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['transaction'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['trade_date'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['set_date'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['currency'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['maturity_date'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['amount'] !!}</td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
    @endforeach
    @if(count($data['equity_holdings']) > 0)
        <tr>
            <td colspan="3"><strong>Equities</strong></td>
        </tr>
    @endif
    @foreach($data['equity_holdings'] as $securityDeposits)
        <tr>
            <td colspan="3"><strong>{!! $securityDeposits['security_details']['name'] !!}</strong></td>
        </tr>
        <tr>
            <td>{!! $securityDeposits['starting_balance']['code'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['name'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['description'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['transaction'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['trade_date'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['set_date'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['currency'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['maturity_date'] !!}</td>
            <td>{!! $securityDeposits['starting_balance']['amount'] !!}</td>
        </tr>
        @foreach($securityDeposits['investments'] as $deposit)
            <tr>
                <td>{!! $deposit['code'] !!}</td>
                <td>{!! $deposit['name'] !!}</td>
                <td>{!! $deposit['description'] !!}</td>
                <td>{!! $deposit['transaction'] !!}</td>
                <td>{!! $deposit['trade_date'] !!}</td>
                <td>{!! $deposit['set_date'] !!}</td>
                <td>{!! $deposit['currency'] !!}</td>
                <td>{!! $deposit['maturity_date'] !!}</td>
                <td>{!! $deposit['amount'] !!}</td>
            </tr>
        @endforeach
        <tr>
            <td>{!! $securityDeposits['closing_balance']['code'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['name'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['description'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['transaction'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['trade_date'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['set_date'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['currency'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['maturity_date'] !!}</td>
            <td>{!! $securityDeposits['closing_balance']['amount'] !!}</td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
    @endforeach
    </tbody>
</table>