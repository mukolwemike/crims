@extends('reports.letterhead')

@section('content')
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    @if(isset($client->client_code))
        (Client Code – {!! $client->client_code !!})<br/>
    @endif
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">REF: SALE OF UNITS </p>

    <table class="table table-responsive">
        <tbody>
        <tr>
            <td>
                <b>Fund:</b> {{ $unitFund->name }}<br><br>

                <b>Value Date:</b> {{Carbon\Carbon::parse($sale->date)->toFormattedDateString()}} <br><br>

                <b>Account No:</b> {{ $bank->account_number }}<br>
            </td>
        </tr>
        <tr style="">
            <td>
                <b>Amount Withdrawn:</b> {{ $unitFund->currency->code }}
                {{ \Cytonn\Presenters\AmountPresenter::currency($sale->number, true, 0) }}<br><br>
                <b>Units Withdrawn:</b> {{ \Cytonn\Presenters\AmountPresenter::currency($sale->number, true, 0) }}<br>
            </td>
        </tr>
        <tr>
            <td>
                <b>Amount in
                    words:</b> {{ $unitFund->currency->name }} {{\Cytonn\Presenters\AmountPresenter::convertToWords($sale->number)}}
            </td>
        </tr>
        </tbody>
    </table>




    {{--    <p>--}}
    {{--        You shall receive a statement via your email address at the end of the month confirming your investment and the--}}
    {{--        interest accrued to keep you updated on your investment growth.--}}
    {{--    </p>--}}


    {{--    <p>If you have any questions, comments or need any assistance, we are at your service.--}}
    {{--        Our relationship team will be at hand to assist you with all your investment needs.--}}
    {{--        Please do not hesitate to contact us on 0708 758 969 or email us at <a--}}
    {{--                href="mailto:operations@serianiasset.com">operations@serianiasset.com</a>--}}
    {{--    </p>--}}

    <p>We thank you for your investment with {{ $unitFund->manager->fullname }} Limited and look forward to working
        closely with you in realizing your financial and investment goals.</p>

    <p>Yours sincerely,</p>
    <p><strong>For: {!! $unitFund->manager->fullname !!}</strong></p>

    @if(!isset($companyAddress))
        <p><strong>{!! $emailSender !!}</strong></p>
    @else
        @include('emails.caml_client_service_signature')
{{--        <p><strong>Pauline Ayoki</strong></p>--}}
{{--        <p><strong>Administrator</strong></p>--}}
{{--        <p><strong>Mobile : 0714143588</strong></p>--}}
{{--        <p><strong>Email</strong> : <a href="payoki@serianiasset.com">payoki@serianiasset.com</a></p>--}}
    @endif
@stop