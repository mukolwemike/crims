<table>
    <tr>
        <th>Report Start Date</th>
        <th colspan="2">{!! $values['heading']['start'] !!}</th>
    </tr>
    <tr>
        <th>Report End Date</th>
        <th colspan="2">{!! $values['heading']['end'] !!}</th>
    </tr>
    <tr>
        <th>FundManager</th>
        <th colspan="2">{!! $values['heading']['fundmanager'] !!}</th>
    </tr>
    @if($values['heading']['fund'] )
    <tr>
        <th>Fund</th>
        <th colspan="2">{!! $values['heading']['fund']  !!}</th>
    </tr>
    @endif
    <tr>
        <th></th>
    </tr>

    @foreach($values['months'] as $monthKey => $monthData)
        <tr>
            <th>Month</th>
            <th>{!! $monthKey !!}</th>
        </tr>
        <tr>
            <th></th>
            @foreach($monthData['funds'] as $fundData)
                <th colspan="4">{!! $fundData['details']['name'] !!}</th>
            @endforeach
        </tr>
        <tr>
            <th>Asset Class Movement and Return</th>
            @foreach($monthData['funds'] as $fundData)
                <th>Equity</th>
                <th>Fixed Income</th>
                <th>Offshore</th>
                <th>Total Value</th>
            @endforeach
        </tr>
        <tr>
            <td>Beginning Value</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['starting_value']['equities'] !!}</td>
                <td>{!! $fundData['totals']['starting_value']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['starting_value']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['starting_value']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td>Cash Flows</td>
            @foreach($monthData['funds'] as $fundData)
                <td></td>
                <td></td>
                <td></td>
            @endforeach
        </tr>
        @foreach($monthData['days'] as $dayData)
            <tr>
                <td>{!! $dayData !!}</td>
                @foreach($monthData['funds'] as $fundData)
                    <td>{!! isset($fundData['data']['equities'][$dayData]) ? $fundData['data']['equities'][$dayData] : '' !!}</td>
                    <td>{!! isset($fundData['data']['deposits'][$dayData]) ? $fundData['data']['deposits'][$dayData] : '' !!}</td>
                    <td>{!! isset($fundData['data']['offshore'][$dayData]) ? $fundData['data']['offshore'][$dayData] : '' !!}</td>
                    <td>{!! isset($fundData['data']['total_value'][$dayData]) ? $fundData['data']['total_value'][$dayData] : '' !!}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td>Ending Value</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['ending_value']['equities'] !!}</td>
                <td>{!! $fundData['totals']['ending_value']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['ending_value']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['ending_value']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Net Cash Flow during Period</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['net_cash_flow']['equities'] !!}</td>
                <td>{!! $fundData['totals']['net_cash_flow']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['net_cash_flow']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['net_cash_flow']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td>Weighted Cash Flow during Period</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['weighted_cash_flow']['equities'] !!}</td>
                <td>{!! $fundData['totals']['weighted_cash_flow']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['weighted_cash_flow']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['weighted_cash_flow']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td>Beginning Value plus Weighted Cash Flows</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['beginning_plus_weighted']['equities'] !!}</td>
                <td>{!! $fundData['totals']['beginning_plus_weighted']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['beginning_plus_weighted']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['beginning_plus_weighted']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Beginning Weighted Portfolio Return Calculation</td>
        </tr>
        <tr>
            <td>Period Return (Modified Deitz)</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['period_return']['equities'] !!}</td>
                <td>{!! $fundData['totals']['period_return']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['period_return']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['period_return']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td>% of Value at Beginning of Period + Wgt CF</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['percentage_value_at_beginning']['equities'] !!}</td>
                <td>{!! $fundData['totals']['percentage_value_at_beginning']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['percentage_value_at_beginning']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['percentage_value_at_beginning']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td>Avg Weighted Total Portfolio Return</td>
            @foreach($monthData['funds'] as $fundData)
                <td>{!! $fundData['totals']['avg_weighted_total']['equities'] !!}</td>
                <td>{!! $fundData['totals']['avg_weighted_total']['deposits'] !!}</td>
                <td>{!! $fundData['totals']['avg_weighted_total']['offshore'] !!}</td>
                <td>{!! $fundData['totals']['avg_weighted_total']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Composite Beginning Value plus Weighted CFs</td>
            @foreach($monthData['funds'] as $fundData)
                <td></td>
                <td></td>
                <td></td>
                <td>{!! isset($fundData['totals']['composite_beginning_value']['total_value']) ? $fundData['totals']['composite_beginning_value']['total_value'] : '' !!}</td>
            @endforeach
        </tr>
        <tr>
            <td>% of Value at Beginning of Period + Wgt CF</td>
            @foreach($monthData['funds'] as $fundData)
                <td></td>
                <td></td>
                <td></td>
                <td>{!! $fundData['totals']['composite_percentage_value_at_beginning']['total_value'] !!}</td>
            @endforeach
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Composite Average Weighted Total Portfolio Returns</td>
            @foreach($monthData['funds'] as $fundData)
                <td></td>
                <td></td>
                <td></td>
                <td>{!! isset($fundData['totals']['composite_avg_weighted_total']['total_value']) ? $fundData['totals']['composite_avg_weighted_total']['total_value'] : '' !!}</td>
            @endforeach
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
    @endforeach
</table>