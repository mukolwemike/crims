<table>
    <thead>
    <tr>
        <th>Report Date</th>
        <th colspan="2">{!! $date !!}</th>
    </tr>
    <tr>
        <th>Unit Fund</th>
        <th colspan="2">{!! $unitFund->name !!}</th>
    </tr>
    <tr>
        <th>Report Title</th>
        <th colspan="2">Holdings Report</th>
    </tr>
    <tr>
        <th></th>
    </tr>
    <tr>
        <th></th>
    </tr>
    <tr>
        <th style="background-color: #006666; color: #ffffff">Code</th>
        <th style="background-color: #006666; color: #ffffff">Security Name</th>
        <th style="background-color: #006666; color: #ffffff">ISIN Code</th>
        <th style="background-color: #006666; color: #ffffff">Nominal/Number</th>
        <th style="background-color: #006666; color: #ffffff">Historical Cost</th>
        <th style="background-color: #006666; color: #ffffff">Trade Date</th>
        <th style="background-color: #006666; color: #ffffff">Maturity Date</th>
        <th style="background-color: #006666; color: #ffffff">Currency</th>
        <th style="background-color: #006666; color: #ffffff">Market Price</th>
        <th style="background-color: #006666; color: #ffffff">Market Value</th>
        <th style="background-color: #006666; color: #ffffff">Percentage Exposure</th>
    </tr>
    </thead>
    @foreach($data['deposits'] as $assetClass => $deposits)
    @if(count($deposits) > 0)
        <tr>
        <td colspan="2" style="background-color: #95B3D7; font-weight: 700">
        <strong>{!! $assetClass !!}</strong></td>
        </tr>

        <tbody>
        @foreach($deposits as $securityDeposits)
            {{--<tr>--}}
                {{--<td colspan="2" style="background-color: #95B3D7; font-weight: 700">--}}
                    {{--<strong>{!! $securityDeposits['security_details']['name'] !!}</strong></td>--}}
            {{--</tr>--}}
            @foreach($securityDeposits['investments'] as $key => $deposit)
                @if($key === 'total')
                    {{--<tr>--}}
                        {{--<td>{!! $deposit['code'] !!}</td>--}}
                        {{--<td>{!! $deposit['name'] !!}</td>--}}
                        {{--<td>{!! $deposit['isin_code'] !!}</td>--}}
                        {{--<td style="background-color: #FFFF00; font-weight: 700">{!! $deposit['nominal_number'] !!}--}}
                        {{--</td>--}}
                        {{--<td style="background-color: #FFFF00; font-weight: 700">{!! $deposit['historical_cost'] !!}--}}
                        {{--</td>--}}
                        {{--<td>{!! $deposit['trade_date'] !!}</td>--}}
                        {{--<td>{!! $deposit['maturity_date'] !!}</td>--}}
                        {{--<td style="background-color: #FFFF00; font-weight: 700">{!! $deposit['currency'] !!}</td>--}}
                        {{--<td style="background-color: #FFFF00; font-weight: 700">{!! $deposit['amount'] !!}</td>--}}
                        {{--<td style="background-color: #FFFF00; font-weight: 700">{!! percentage($deposit['amount'], $data['deposit_totals']['amount'], 3) !!}--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                @else
                    <tr>
                        <td>{!! $deposit['code'] !!}</td>
                        <td>{!! $deposit['name'] !!}</td>
                        <td>{!! $deposit['isin_code'] !!}</td>
                        <td>{!! $deposit['nominal_number'] !!}</td>
                        <td>{!! $deposit['historical_cost'] !!}</td>
                        <td>{!! $deposit['trade_date'] !!}</td>
                        <td>{!! $deposit['maturity_date'] !!}</td>
                        <td>{!! $deposit['currency'] !!}</td>
                        <td>{{ $deposit['price'] }}</td>
                        <td>{!! $deposit['amount'] !!}</td>
                        <td>{!! percentage($deposit['amount'], $data['deposit_totals']['amount'], 3) !!}</td>
                    </tr>
                @endif
            @endforeach
            {{--<tr>--}}
                {{--<td colspan="10"></td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td colspan="10"></td>--}}
            {{--</tr>--}}
        @endforeach
        {{--<tr>--}}
            {{--<td colspan="10"></td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td colspan="10"></td>--}}
        {{--</tr>--}}
    @endif
    @endforeach
        <tr>
            <td colspan="11"></td>
        </tr>
        <tr>
            <td></td>
            <td>Totals</td>
            <td></td>
            <td style="background-color: #FFFF00; font-weight: 700">{!! $data['deposit_totals']['nominal_number'] !!}</td>
            <td style="background-color: #FFFF00; font-weight: 700">{!! $data['deposit_totals']['historical_cost'] !!}</td>
            <td></td>
            <td></td>
            <td></td>
            <td style="background-color: #FFFF00; font-weight: 700">{!! $data['deposit_totals']['amount'] !!}</td>
            <td></td>
        </tr>
    </tbody>
</table>