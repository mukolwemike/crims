@extends('reports.plain_letterhead_space')

@section('content')
    Dear, {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    <div class="">

        <p>
            On behalf of {{ $unitFund->manager->fullname }},
            we would like to inform you that we have processed a purchase of <b>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number), false, 2 }}</b>
            units for <b>{{ $unitFund->currency->code }} {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number * $purchase->price) }}</b>
            in our Unit Trust product offering, specifically the <b>{{ $unitFund->name }}</b> and was charged
            {{ $unitFund->currency->code }} {{ \Cytonn\Presenters\AmountPresenter::currency($fees->sum('amount')) }} on fees.
        </p>

        <br>

    </div>

    <p>Kind regards,</p>
@stop