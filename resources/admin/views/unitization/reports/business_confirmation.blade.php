@extends('reports.letterhead')

@section('content')
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    @if(isset($client->client_code))
        @if($unitFund->category->slug == 'pension-fund')
            (Member Number – {!! $client->client_code !!})<br/>
        @else
            (Client Code – {!! $client->client_code !!})<br/>
        @endif
    @endif
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    @if($unitFund->category->slug == 'money-market-fund')
        @include('unitization.reports.bc_templates.mmf')
    @elseif($unitFund->category->slug == 'pension-fund')
        @include('unitization.reports.bc_templates.pension-fund')
    @else
        @include('unitization.reports.bc_templates.cfsf')
    @endif

    <p>Yours sincerely,</p>

    <img src="{{ storage_path('resources/signatures/'.$signature) }}" height="60px"/>
    <p><strong style="text-decoration: underline;">{!! $emailSender !!}</strong></p>
@stop