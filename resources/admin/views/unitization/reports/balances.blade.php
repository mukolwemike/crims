<table>
    <thead>
    <tr>
        <th>Report Date</th>
        <th colspan="2">{!! $date !!}</th>
    </tr>
    <tr>
        <th>Unit Fund</th>
        <th colspan="2">{!! $unitFund->name !!}</th>
    </tr>
    <tr>
        <th>Report Title</th>
        <th colspan="2">Holdings Report</th>
    </tr>
    <tr>
        <th></th>
    </tr>
    <tr>
        <th></th>
    </tr>
    <tr>
        <th>Code</th>
        <th>Security Name</th>
        <th>Currency</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @if(count($data['deposits']) > 1)
        <tr>
            <td colspan="3"><strong>Deposits</strong></td>
        </tr>
        @foreach($data['deposits'] as $deposit)
            <tr>
                <td>{!! $deposit['code'] !!}</td>
                <td>{!! $deposit['name'] !!}</td>
                <td>{!! $deposit['currency'] !!}</td>
                <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::accounting($deposit['amount'], false, 0) !!}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="4"></td>
        </tr>
    @endif
    @if(count($data['equity_holdings']) > 1)
        <tr>
            <td colspan="3"><strong>Equities</strong></td>
        </tr>
        @foreach($data['equity_holdings'] as $deposit)
            <tr>
                <td>{!! $deposit['code'] !!}</td>
                <td>{!! $deposit['name'] !!}</td>
                <td>{!! $deposit['currency'] !!}</td>
                <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::accounting($deposit['amount'], false, 0) !!}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>