@extends('reports.letterhead')

@section('content')
    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    @if($fund->category->slug == 'pension-fund')
        <br>
        @include('unitization.reports.statements.partials.client_details')
    @else
        {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}
        @if($client->account_name)
            - {!! $client->account_name !!}
        @endif<br/>

        @if($client->clientType->name == 'corporate')
            C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
        @endif
        (Client code – {!! $client->client_code !!})<br/>
        {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    @endif

    <!-- RE -->
    <p><strong style="border-bottom: 2px solid #006B5A;">CONSOLIDATED {{ strtoupper($fund->name) }} STATEMENT AS AT {!! strtoupper(\Cytonn\Presenters\DatePresenter::formatDate($date)) !!}</strong></p>

    <!-- Body -->
    @if($fund->category->slug == 'pension-fund')
        <p>{!! $fund->fundmanager->fullname !!} wants to thank you for choosing to partner with us, as you save for your retirement
            through the {!! $fund->name !!}.</p>

        <p>Below is a summary of your contributions so far.</p>
    @else
        <p>{!! $fund->name !!} takes this opportunity to thank you for investing with us.</p>
        <p>Below is a summary of your investments with the {{ $fund->name }} to date.</p>
    @endif

    @include('unitization.reports.statements.'.$calculation)

    @if($campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif

    @if($fund->category->slug == 'pension-fund')
        <p>Once again, we thank you for saving for your retirement with {!! $fund->fundmanager->fullname !!}.</p>
    @else
        <p>Once again, we thank you for choosing us to serve your investment needs and look forward to delivering to our investment promise.</p>
    @endif

    <p>Yours sincerely,</p>
    <img src="{{ storage_path('resources/signatures/'.$signature) }}" height="60px"/>
    <p><strong style="text-decoration: underline;">{!! $emailSender !!}</strong></p>
    @if($fund->category->slug == 'pension-fund')
        <br><br>
        <strong>Notes</strong><br>
        * As per the regulations, interest is distributed once every year after the scheme audit. <br>
        AVC = Additional Voluntary Contribution
    @endif
@stop
