<table class="table table-responsive" style="font-size: 11px; text-align: center;">
    <?php
    $hasEmployer = $fund->short_name == 'CURBS';
    ?>

    <thead>
    <tr style="color: #FFF; background-color: #006B5A;">
        @if($hasEmployer)
            <th colspan="8">Tax Exempt Contributions</th>
            <th colspan="8">Non Tax Exempt Contributions</th>
        @else
            <th colspan="7">Tax Exempt Contributions</th>
            <th colspan="7">Non Tax Exempt Contributions</th>
        @endif
    </tr>
    <tr style="color: #FFF; background-color: #006B5A;">
        <th>Contribution Date</th>
        @if($hasEmployer)
            <th>Employee Contribution</th>
            <th>Employer Contribution</th>
        @else
            <th>Contribution</th>
        @endif
        <th>AVC</th>
        <th>Transfer In</th>
        <th>Interest *</th>
        <th>Withdrawal</th>
        <th>Total</th>
        <th>Contribution Date</th>
        @if($hasEmployer)
            <th>Employee Contribution</th>
            <th>Employer Contribution</th>
        @else
            <th>Contribution</th>
        @endif
        <th>AVC</th>
        <th>Transfer In</th>
        <th>Interest *</th>
        <th>Withdrawal</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $firstAction = $actions->first();

    $firstTaxAction = $firstAction ? $firstAction->tax_exempt_action : null;
    $taxOpening = $firstTaxAction ? $firstTaxAction->opening_tax_exempt_balance : 0;

    $firstNonTaxAction = $firstAction ? $firstAction->non_tax_exempt_action : null;
    $nonTaxOpening = $firstNonTaxAction ? $firstNonTaxAction->opening_non_tax_exempt_balance : 0;
    ?>
    @if($taxOpening != 0 || $nonTaxOpening != 0)
        <tr>
            <td>Opening Bal : {!! \Cytonn\Presenters\DatePresenter::formatDate($start_date) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($taxOpening) !!}</td>
            @if($hasEmployer)
                <td></td>
            @endif
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($taxOpening) !!}</td>
            <td>Opening Bal : {!! \Cytonn\Presenters\DatePresenter::formatDate($start_date) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($nonTaxOpening) !!}</td>
            @if($hasEmployer)
                <td></td>
            @endif
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($nonTaxOpening) !!}</td>
        </tr>
    @endif

    <?php
    $lastTaxAction = $firstTaxAction;
    $lastNonTaxAction = $firstNonTaxAction;
    ?>

    @foreach($actions as $action)
        <tr>
            <?php
            $taxAction = $action->tax_exempt_action;
            $lastTaxAction = $taxAction ? $taxAction : $lastTaxAction;
            $nonTaxAction = $action->non_tax_exempt_action;
            $lastNonTaxAction = $nonTaxAction ? $nonTaxAction : $lastNonTaxAction;
            ?>
            @if($taxAction)
                <td>{!! $taxAction->date->toFormattedDateString() !!}</td>
                <td>
                    @if($taxAction->transaction_type == 'contribution' && $taxAction->contribution_type == 'employee')
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->purchase_amount) !!}
                    @endif
                </td>
                @if($hasEmployer)
                    <td>
                        @if($taxAction->transaction_type == 'contribution' && $taxAction->contribution_type == 'employer')
                            {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->purchase_amount) !!}
                        @endif
                    </td>
                @endif
                <td>
                    @if($taxAction->transaction_type == 'contribution' && ($taxAction->contribution_type == 'employer_avc' || $taxAction->contribution_type == 'employee_avc') )
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->purchase_amount) !!}
                    @endif
                </td>
                <td>
                    @if($taxAction->transaction_type == 'transfer_in')
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->purchase_amount) !!}
                    @endif
                </td>
                <td>
                    @if($taxAction->transaction_type == 'interest')
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->net_interest) !!}
                    @endif
                </td>
                <td>
                    @if($taxAction->transaction_type == 'withdrawal')
                        ({!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->sale_amount) !!})
                    @endif
                </td>
                <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($taxAction->closing_tax_exempt_total) !!}</td>
            @else
                <td></td>
                <td></td>
                @if($hasEmployer)
                    <td></td>
                @endif
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
            @if($nonTaxAction)
                <td>{!! $nonTaxAction->date->toFormattedDateString() !!}</td>
                <td>
                    @if($nonTaxAction->transaction_type == 'contribution' && $nonTaxAction->contribution_type == 'employee')
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->purchase_amount) !!}
                    @endif
                </td>
                @if($hasEmployer)
                    <td>
                        @if($nonTaxAction->transaction_type == 'contribution' && $nonTaxAction->contribution_type == 'employer')
                            {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->purchase_amount) !!}
                        @endif
                    </td>
                @endif
                <td>
                    @if($nonTaxAction->transaction_type == 'contribution' && ($nonTaxAction->contribution_type == 'employer_avc' || $nonTaxAction->contribution_type == 'employee_avc') )
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->purchase_amount) !!}
                    @endif
                </td>
                <td>
                    @if($nonTaxAction->transaction_type == 'transfer_in')
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->purchase_amount) !!}
                    @endif
                </td>
                <td>
                    @if($nonTaxAction->transaction_type == 'interest')
                        {!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->net_interest) !!}
                    @endif
                </td>
                <td>
                    @if($nonTaxAction->transaction_type == 'withdrawal')
                        ({!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->sale_amount) !!})
                    @endif
                </td>
                <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($nonTaxAction->closing_non_tax_exempt_total) !!}</td>
            @else
                <td></td>
                <td></td>
                @if($hasEmployer)
                    <td></td>
                @endif
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        </tr>
    @endforeach

    <tr>
        @if($hasEmployer)
            <td colspan="8"></td>
            <td colspan="8"></td>
        @else
            <td colspan="7"></td>
            <td colspan="7"></td>
        @endif
    </tr>
    <tr>
        <th>Total</th>
        @if($lastTaxAction)
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->employee_contribution) !!}</th>
            @if($hasEmployer)
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->employer_contribution) !!}</th>
            @endif
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->avc) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->transfer_in) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->net_tax_exempt_interest_after) !!}</th>
            <th>({!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->withdrawal) !!})</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastTaxAction->closing_tax_exempt_total) !!}</th>
        @else
            <th></th>
            <th></th>
            @if($hasEmployer)
                <th></th>
            @endif
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        @endif
        <th>Total</th>
        @if($lastNonTaxAction)
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->employee_contribution) !!}</th>
            @if($hasEmployer)
                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->employer_contribution) !!}</th>
            @endif
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->avc) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->transfer_in) !!}</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->net_non_tax_exempt_interest_after) !!}</th>
            <th>({!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->withdrawal) !!})</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($lastNonTaxAction->closing_non_tax_exempt_total) !!}</th>
        @else
            <th></th>
            <th></th>
            @if($hasEmployer)
                <th></th>
            @endif
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        @endif
    </tr>
    </tbody>
</table>