<table class="table table-responsive" style="font-size: 13px; text-align: center;">
    <thead>
    <tr style="color: #FFF; background-color: #006B5A;">
        <th>Transactional Date</th>
        <th>Inflow</th>
        <th>Withdrawal</th>
        <th>Gross Interest</th>
        <th>Tax</th>
        <th>Running Balance</th>
    </tr>
    </thead>
    <tbody>
    @if($opening_balance != 0)
        <tr>
            <td>Opening Bal : {{ \Cytonn\Presenters\DatePresenter::formatDate($start_date) }}</td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($opening_balance) }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($opening_balance) }}</td>
        </tr>
    @endif
    @foreach($actions as $action)
        <?php $balance = $action->opening_balance; ?>
        @foreach($action->purchases as $purchase)
            <tr>
                <td>{{ $action->date->toFormattedDateString() }}</td>
                <td> {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    {{ \Cytonn\Presenters\AmountPresenter::currency($balance += $purchase->number) }}
                </td>
            </tr>
        @endforeach
        @if($action->gross_interest > 0 || $action->sale_amount > 0)
            <tr>
                <td>{{ $action->date->toFormattedDateString() }}</td>
                <td></td>
                <td>{{  \Cytonn\Presenters\AmountPresenter::currency($action->sale_amount)}}</td>
                <td>{{  \Cytonn\Presenters\AmountPresenter::currency($action->gross_interest) }}</td>
                <td>{{  \Cytonn\Presenters\AmountPresenter::currency($action->withholding_tax)}}</td>
                <td>{{  \Cytonn\Presenters\AmountPresenter::currency($balance += $action->gross_interest - $action->sale_amount - $action->withholding_tax) }}</td>
            </tr>
        @endif

{{--        displays sale same as above code block--}}
{{--        @foreach($action->sales as $sale)--}}
{{--            <tr>--}}
{{--                <td>{{ $action->date->toFormattedDateString() }}</td>--}}
{{--                <td></td>--}}
{{--                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($sale->number) }}</td>--}}
{{--                <td></td>--}}
{{--                <td></td>--}}
{{--                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($balance -= $sale->number) }}</td>--}}
{{--            </tr>--}}
{{--        @endforeach--}}
    @endforeach
    <tr>
        <td colspan="6" height="14px"></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <?php $grossTotal = $actions->last() ? $actions->last()->total : 0; ?>
        <th>Total</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($actions->sum->purchase_amount + $opening_balance) }}</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($actions->sum->sale_amount) }}</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($actions->sum->gross_interest) }}</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($actions->sum->withholding_tax) }}</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency(abs($grossTotal > 1 ? $grossTotal : 0)) }}</th>
    </tr>
    </tfoot>
</table>