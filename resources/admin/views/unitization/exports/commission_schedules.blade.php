<h2>Commission Schedules</h2>
<table>
    <thead>
    <tr>
        <th>Client Name</th>
        <th>Client Code</th>
        <th>Number</th>
        <th>Price</th>
        <th>Rate</th>
        <th>Value Date</th>
        <th>Entity</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($schedules as $cs)
        <tr>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($cs->purchase->client_id) !!}</td>
            <td>{!! $cs->purchase->client->client_code !!}</td>
            <td>{!! number_format($cs->purchase->number) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->purchase->price) !!}</td>
            <td>{!! $cs->unitFundCommission->rate !!}%</td>
            <td>{!! $cs->purchase->date !!}</td>
            <td>{!! $cs->purchase->unitFund->name !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->amount) !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@if($override != 0)
    <h2>Overrides</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Override</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <td>{!! $report->name !!}</td>
                <td>{!! $report->rank->name !!}</td>
                <td>{!! $report->commission !!}</td>
                <td>{!! $report->rate !!}</td>
                <td>{!! $report->override !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif