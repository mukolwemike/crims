@extends('layouts.default')

@section('content')

    <div class="col-md-12 col-md-offset-0">
        <div class = "panel-dashboard">
            <div class="row">
                <unit-fund-summaries></unit-fund-summaries>
            </div>
        </div>
    </div>
@endsection
