<div>
      <div class="panel panel-success">
            <div class="panel-heading">
                  <h4>Investor  Details</h4>
            </div>
            <div class="panel-body">
                  <table class="table table-responsive table-striped">
                        <tbody>
                              <tr>
                                    <th>Registered Name:</th>
                                    <td>{!! $data['registered_name'] !!}</td>

                                    <th>Trade Name:</th>
                                    <td>{!! $data['trade_name'] !!}</td>

                                    <th>Registered Address:</th>
                                    <td>{!! $data['registered_address'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Telephone Number:</th>
                                    <td>{!! $data['phone'] !!}</td>

                                    <th>Email:</th>
                                    <td>{!! $data['email'] !!}</td>

                                    <th>Pin Number:</th>
                                    <td>{!! $data['company_pin'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Physical Address:</th>
                                    <td>{!! $data['physical_address'] !!}</td>

                                    <th>Preferred method of contact:</th>
                                    <td>{!! $data['method_of_contact'] !!}</td>

                                    <th>Contact Person:</th>
                                    <td>{!! $data['contact_person'] !!}</td>
                              </tr>
                        </tbody>
                  </table>
            </div>
      </div>
</div>
