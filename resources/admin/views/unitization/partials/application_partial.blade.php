<div class="panel panel-success">
      <div class="panel-heading">
            <h4>Investment Section</h4>
      </div>
      <div class="panel-body">
            <table class="table table-responsive table-striped">
                  <tbody>
                  <tr>
                        <th width="30%">Unit Fund:</th>
                        <td>{!! $data['unitFund'] !!}</td>
                  </tr>
                  <tr>
                        <th>Amount:</th>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                  </tr>
                  <tr>
                        <th>Payment Method:</th>
                        <td>{!! $data['paymentMethod'] !!}</td>
                  </tr>
                  </tbody>
            </table>
      </div>
</div>
@if($data['type'] == 1)
      @include('unitization.partials.individualdetails', ['data'=>$data])
@endif

@if($data['type'] == 2)

      @include('unitization.partials.corporatedetails', ['data'=>$data, 'corporateinvestortype'=>$data['type_of_investor']])

@endif
<div class="panel panel-success">
      <div class="panel-heading">
            <h4>Source of Funds</h4>
      </div>
      <div class="panel-body">
            <table class="table table-responsive table-striped">
                  <tbody>
                        <tr>
                              <th width="30%">Source of Fund:</th>
                              <td>{!! $data['source_of_fund']  !!}</td>
                        </tr>
                  </tbody>
            </table>
      </div>
</div>
<div class="panel panel-success">
      <div class="panel-heading">
            <h4>Investor Bank information</h4>
      </div>
      <div class="panel-body">
            <table class="table table-responsive table-striped">
                  <tbody>
                        <tr>
                              <th width="30%">Investor Account Name:</th>
                              <td>{!! $data['investor_account_name']  !!}</td>
                              <th width="30%">Investor Account Number:</th>
                              <td>{!! $data['investor_account_number']  !!}</td>
                        </tr>
                  </tbody>
            </table>
      </div>
</div>
@if($data['bank'])

@endif

@if($holders != null)
<div class="panel panel-success">
      <div class="panel-heading">
            <h4>Joint Holders</h4>
      </div>
      @foreach($holders as $holder)
            <div class="panel-body form-detail">

                  <div class="col-md-6">
                        <label for="">Name</label> {!! $holder->title . ' ' . $holder->firstname . ' ' !!}
                        @if(isset($holder->middlename))
                              {!! $holder->middlename !!}
                        @endif
                        {!!  ' ' . $holder->lastname !!}
                  </div>

                  @if(isset($holder->dob))
                        <div class="col-md-6">
                              <label for="">Date of
                                    Birth</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($holder->dob) !!}
                        </div>
                  @endif

                  <div class="col-md-6">
                        <label for="">Email</label> {!! $holder->email !!}
                  </div>

                  <div class="col-md-6">
                        <label for="">Phone</label> {!! $holder->telephone_cell !!}
                  </div>

                  <div class="col-md-6">
                        <label for="">Pin Number</label> {!! $holder->pin_no !!}
                  </div>

                  <div class="col-md-6">
                        <label for="">ID/Passport Number</label> {!! $holder->id_or_passport !!}
                  </div>

                  @if(isset($holder->postal_code))
                        <div class="col-md-6">
                              <label for="">Postal Code</label> {!! $holder->postal_code !!}
                        </div>
                  @endif

                  @if(isset($holder->postal_address))
                        <div class="col-md-6">
                              <label for="">Postal Address</label> {!! $holder->postal_address !!}
                        </div>
                  @endif

                  @if(isset($holder->country))
                        <div class="col-md-6">
                              <label for="">Country</label> {!! $holder->country !!}
                        </div>
                  @endif

                  <div class="clearfix"></div>
            </div>
      @endforeach
</div>
@endif
