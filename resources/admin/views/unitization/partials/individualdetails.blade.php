<div>
      <div class="panel panel-success">
            <div class="panel-heading">
                  <h4>Investor Details</h4>
            </div>
            <div class="panel-body">
                  <table class="table table-responsive table-striped">
                        <tbody>
                              <tr>
                                    <th>Name:</th>
                                    <td>{!! $data['name'] !!}</td>
                                    <th>Email:</th>
                                    <td>{!! $data['email'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Gender:</th>
                                    <td>{!! $data['gender'] !!}</td>
                                    <th>Date of Birth:</th>
                                    <td>{!! $data['dob'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Pin Number:</th>
                                    <td>{!! $data['pin_no'] !!}</td>
                                    <th>Id/Passport Number:</th>
                                    <td>{!! $data['id_or_passport'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Postal Address:</th>
                                    <td>{!! $data['postal_address'] !!}</td>
                                    <th>Postal Code:</th>
                                    <td>{!! $data['postal_code'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Country:</th>
                                    <td>{!! $data['country'] !!}</td>
                                    <th>Phone:</th>
                                    <td>{!! $data['phone'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Home Tel Number:</th>
                                    <td>{!! $data['telephone_home'] !!}</td>
                                    <th>Office Tel Number:</th>
                                    <td>{!! $data['telephone_office'] !!}</td>
                              </tr>
                              <tr>
                                    <th>Residential Address:</th>
                                    <td>{!! $data['residential_address'] !!}</td>
                                    <th>Town:</th>
                                    <td>{!! $data['town'] !!}</td>
                              </tr>
                        </tbody>
                  </table>
            </div>
      </div>
</div>
