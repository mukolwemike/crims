@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <h3>Application Feedback</h3>

            <div class="detail-group">
                {!! Form::open(['route'=>['save_application_feedback'], 'method'=>'POST', 'files'=>true]) !!}

                <div class="form-group">
                    {!! Form::text('subject', null, ['class'=>'form-control', 'placeholder'=>'Subject...', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'subject') !!}
                </div>

                <div class="form-group" ng-controller="SummerNoteController">
                    <summernote  config="options" ng-model="summernote" ng-init="summernote=''" placeholder="Description..." on-paste="onPaste(evt)"></summernote>
                    {!! Form::text('description', null, ['ng-model'=>'summernote', 'style'=>'display: none;']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                </div>

                <div class="form-group">
                    {!! Form::file('file', ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Save Feedback', ['class'=>'btn btn-block btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop