@extends('layouts.default')

@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "RealEstateClientsGridCtrl">

            <a href="/dashboard/realestate/statements" class="btn btn-success margin-bottom-20">Statements</a>

            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <td colspan="2"></td><td colspan="2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></td>
                    </tr>
                    <tr>
                        <th>Client Code</th><th>ClientName</th><th>Number of Units</th><th>Details</th>
                    </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed">
                        <td><% row.code %></td>
                        <td><% row.fullName %></td>
                        <td><% row.units %></td>
                        <td>
                            <a ng-controller = "PopoverCtrl" uib-popover = "View more details" popover-trigger = "mouseenter" href = "/dashboard/realestate/clients/show/<% row.id %>"><i class = "fa fa-eye"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr><td colspan="100%">Loading real-estate clients...</td></tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td class = "text-center">
                            Items per page
                        </td>
                        <td class = "text-center">
                            <input type = "text" ng-model = "itemsByPage"/>
                        </td>
                        <td colspan = "2" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@endsection