@extends('layouts.default')

@section('content')
    <div class="panel-dashboard" ng-controller="RealEstateClientsController">
        <div class="detail-group">
            <h3>Client Details</h3>

            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Client Name</td>
                    <td>
                        {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}
                        <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#statement">Statements</button>
                    </td>
                </tr>
                <tr>
                    <td>Client added on</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($client->created_at) !!}</td>
                </tr>
                </tbody>
                <tr><td>Client Code</td><td>{!! $client->client_code !!}</td></tr>
                <tr><td>E-Mail</td><td>{!! $client->contact->email !!}</td></tr>
                <tr><td>Phone</td>
                    <td>
                        {!! $client->contact->phone !!}
                        <a class="btn btn-success pull-right" href="/dashboard/clients/details/{!! $client->id !!}">View Client Details</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <h3>Payment Plan</h3>

        <a class="btn btn-success" href="{!! route('preview_payment_plan', $client->id) !!}">Preview Payment Plan</a>
        <a ng-click="sendPaymentPlan('{!! route('mail_payment_plan', $client->id) !!}')" class='btn btn-primary' href="#">Send Payment plan</a>


        <h3>Statements</h3>

        <div>

            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th>Project Name</th>
                    <th colspan="2">Date</th>
                    {{--<th>Action</th>--}}
                </tr>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                        <tr>
                                <td>{!! $project->name !!}</td>
                                <td colspan="2">
                                    {!! Form::open(['route'=>['view_realestate_statement', $client->id, $project->id], 'method'=>'GET', 'class'=>'form-inline']) !!}
                                        <input type="text" name="date"  class="form-control" ng-model="date" uib-datepicker-popup="yyyy-MM-dd" is-open="status.isopen" ng-focus="status.isopen = true">
                                        {!! Form::submit('View statement', ['class'=>'btn btn-success inline margin-bottom-20']) !!}
                                    {!! Form::close() !!}
                                </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <h3>Unit Holdings</h3>

        <div ng-controller="RealEstateClientUnitsController" ng-init="client_id = {!! $client->id !!}">

            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Unit Number</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Amount Paid</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed">
                        <td><% row.project %></td>
                        <td><% row.number %></td>
                        <td><% row.type_name %></td>
                        <td><% row.size_name %></td>
                        <td><% row.amount_paid %></td>
                        <td><% row.balance %></td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr><td colspan="100%">Loading client unit holdings...</td></tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan = "2" class = "text-center">
                            Items per page <input type = "text" ng-model = "itemsByPage"/>
                        </td>
                        <td colspan = "2" class = "text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>

    <!-- Statement Modal -->
    <div class="modal fade" id="statement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Statements</h4>
                </div>
                <div class="modal-body">
                    @if(count($open_campaigns_arr) > 0)
                        {!! Form::open(['route'=>'add_client_to_realestate_statement_campaign']) !!}
                        <div class="form-group">
                            {!! Form::label('campaign', 'Select campaign') !!}

                            {!! Form::select('campaign', $open_campaigns_arr, null, ['class'=>'form-control']) !!}
                        </div>

                        {!! Form::hidden('client', $client->id) !!}

                        {!! Form::submit('Add to campaign', ['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    @else
                        <div class="well well-lg">
                            <p>No open campaigns</p>
                        </div>
                    @endif

                    <div ng-controller="CollapseCtrl" class="padding-top-50">
                        <button type="button" class="btn btn-default" ng-click="isCollapsed = !isCollapsed">More options</button>

                        <div uib-collapse="isCollapsed" class="padding-top-50">
                            {!! Form::open(['route'=>['mail_realestate_statement', $client->id, $project->id]]) !!}
                                <input type="text" name="date"  class="form-control" ng-model="date" uib-datepicker-popup="yyyy-MM-dd" is-open="status.isopen" ng-focus="status.isopen = true">
                                @foreach($projects as $project)
                                    {!! Form::hidden('projects[]', $project->id) !!}
                                @endforeach
                                {!! Form::submit('Send Statement', ['class'=>'btn btn-success']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

@endsection


