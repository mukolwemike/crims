@extends('reports.plain')

@section('content')
    <style>
        .document-title_one {
            font-family: Tahoma;
            font-size: 28px;
            font-weight: 900;
            text-align: center !important;
            margin-top: 20%;
            line-height: 60px;
        }

        .document-title_two {
            font-family: Tahoma;
            font-size: 24px;
            font-weight: 900;
            text-align: center;
            margin-top: 10%;
            line-height: 40px;
        }

        .document-body {
            margin-top: 5%;
            font-size: 16px;
            font-family: Tahoma,Verdana,Segoe,sans-serif;
        }

        .spaced-text {
            line-height: 30px;
        }
    </style>

    <div class="">

        <h1 class="document-title_one">VARIATION OF CONTRACT

            <br>

            BETWEEN

            <br>

            CYTONN INTERGRATED PROJECT, LLP

            <br>

            AND

            <br>
            _________________

            <br>
            <br>
        </h1>

        <h1 class="document-title_two">

            DRAWN BY

            <br>

            CYTONN REAL ESTATE, LLP

            <br>

            P.O. BOX 20695-00200

            <br>

            NAIROBI

        </h1>

    </div>

    <br>
    <br>
    <br>
    <br>

    <div class="document-body">
        <p>This Variation of Contract is made on this {!! $day_of !!} day of {!! $month_of !!} {!! $year_of !!} between</p>
        <ol>
            <li><strong>CYTONN INTERGRATED PROJECT, LLP</strong> a limited liability partnership registered under the laws of the Republic of Kenya and having a postal address of P. O. Box 20695 - 00200, Nairobi (hereinafter referred to as &lsquo;the '<strong>Vendor</strong>&rsquo; which expression shall where the context so admits include its successors in title and assigns);
                <p style="text-align: center;"><strong>And</strong></p>
            </li>
            <li><strong><span>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</span></strong> of P. O. Box <span>{!! \Cytonn\Presenters\ClientPresenter::presentAddressInline($client->id) !!} </span>
                <br> ( hereinafter referred to as &lsquo;the Purchaser '&rsquo; which expression shall where the context so admits include her/his personal representatives and assigns );</li>
        </ol>
        <p>Collectively the &ldquo;<strong>Parties</strong>&rdquo;.</p>
        <p><strong>WHEREAS:</strong></p>
        <ol>
            <li>The Parties vide the Letter of Offer dated <span>{!! Carbon\Carbon::parse($variation->loo_sent_on)->toFormattedDateString() !!}</span> entered into a contract <em>interpartes</em> for the purchase of a <span>{!! $variation->unit_size !!}</span> unit in {!! $holding->project->name !!} known as Unit No. <span>{!! $variation->unit_number !!}</span>. The Unit&rsquo;s Purchase Price is the sum of Kenya Shillings {!! $prev_price_in_words !!} ({!! \Cytonn\Presenters\AmountPresenter::currency((int) $prev_price) !!}).</li>
            <li>The Purchaser signified acceptance of the Offer by paying deposit equivalent to {!! \Cytonn\Presenters\AmountPresenter::currency($deposit_percentage, true, 0) !!} per cent (Figure) of the Purchase Price in the sum of Kenya Shillings  {!! $deposit_in_words !!} ({!! \Cytonn\Presenters\AmountPresenter::currency((int)$net_deposit) !!}) and signing the Letter of Offer and returning a copy thereof to the Vendor;</li>
            <li>The Purchaser has now approached the Vendor with a view to vary the terms of the letter of Offer so constituted which request the Vendor has agreed to.</li>
        </ol>
        <p><strong>NOW THIS VARIATION OF CONTRACT WITNESSETH AS FOLLOWS:</strong></p>
        <p>The clause depicting &ldquo;<strong>Property</strong>&rdquo; is varied by deleting the existing clause and substituting it with the following clause;</p>
        <p><em>&ldquo;A <span>{!! $newHolding->unit->size->name !!}</span> Apartment <span>{!! $newHolding->unit->number !!}</span> to be situate in that residential Development to be known as &lsquo;{!! $newHolding->project->name !!}&rdquo; </em></p>
        <p>The Parties agree that save for the terms and conditions stated above, the terms of the Letter of Offer, remain as contained in the said Letter of Offer and the same is subject to execution of the Agreement for Sale, which upon execution of the Agreement of Sale, which upon execution shall supersede the contract constituted by said Offer and this variation together with any subsequent variations.</p>
        <p><strong>IN WITNESS WHEREOF </strong>the Vendor and Purchaser have duly executed this Agreement.</p>
        <p><strong>SIGNED &amp; SEALED </strong>by the Vendor</p>
    </div>


    <div class="document-body">
        <p><strong>CYTONN INTERGRATED PROJECT, LLP</strong></p>
        <p><strong>Authorized Representative&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong>)&nbsp; &nbsp; &nbsp;---------------------------------------------------<u></u></p>
        <p><strong>Authorized Representative&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong>)&nbsp; &nbsp; &nbsp; --------------------------------------------------<u></u></p>
        <p>In the presence of:</p>
        <p><strong>ADVOCATE</strong></p>
        <p class="spaced-text">I certify that I was present and saw ________________________________________________and ___________________________________________ being the Authorized Representatives of the Vendor duly sign this Contract of Variation.</p>
        <p style="text-align: center;">_________________________</p>
        <p style="text-align: center;"><strong>Advocate</strong></p>
        <p>&nbsp;</p>
        <p><strong>SIGNED </strong>by the Purchaser&nbsp;<strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong>)&nbsp; &nbsp; &nbsp;--------------------------------------------------</p>
        <p>&nbsp;</p>
        <p>In the presence of:&nbsp;</p>
        <p><strong>ADVOCATE</strong></p>
        <p>I certify that I was present and saw ____________________________ being the Purchaser duly sign this Contract of Variation.</p>
        <p style="text-align: center;">_________________________</p>
        <p style="text-align: center;"><strong>Advocate</strong></p>
    </div>
@stop