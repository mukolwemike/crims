@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Edit a real estate unit size</h3>

            {!! Form::model($unitSize, ['route'=>['unitsizes.store', $unitSize->id]]) !!}

            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('realestate_land_size_id', 'Land Size (Optional)') !!}
                {!! Form::select('realestate_land_size_id', $landSizes, null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'realestate_land_size_id') !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection