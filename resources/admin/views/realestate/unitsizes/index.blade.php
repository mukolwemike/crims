@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <div ng-controller = "realestateUnitSizeController">
                <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                    <thead>
                    <tr>
                        <td colspan="1"></td>
                        <td colspan="1">
                            <button type="button" class="pull-right btn btn-success" data-toggle="modal" data-target="#addUnitSize"><i class="fa fa-plus"></i> Add unit size</button>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="1">Unit Size</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed" class="no-page-break">
                        <td colspan="1"><% row.completename %></td>
                        <td>
                            <a  ng-controller="PopoverCtrl" uib-popover="Edit details" popover-trigger="mouseenter" href="/dashboard/realestate/unitsizes/create/<% row.id %>" ><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="2" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot class="no-print">
                    <tr>
                        <td colspan = "100%" class = "text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>
                    </tr>
                    </tfoot>
                </table>

                <!-- Modal -->
                <div class="modal fade" id="addUnitSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open(['route'=>['unitsizes.store']]) !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add a real estate unit size</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-detail">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::label('name', 'Name') !!}
                                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::label('realestate_land_size_id', 'Land size') !!}
                                            {!! Form::select('realestate_land_size_id', $landSizes, null, ['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                {!! Form::submit('Add', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop