@extends('layouts.default')

@section('content')

    <bread-crumb title="">
        <a class="current" slot="sub_1" href="#">Real Estate</a>
    </bread-crumb>

    <div class="analysis_cards__sections">
        <summary-card
                title="Clients"
                statistics="{!! $total_clients !!}"
                statistics_desc="Active Clients"
                link="/dashboard/realestate/clients">
            <a href="/dashboard/realestate/clients">
                <span>View All</span>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </summary-card>

        {{--<summary-card--}}
        {{--title="Reserved Units"--}}
        {{--statistics="{!! $reserved_units !!} / {!! $total_units !!}"--}}
        {{--statistics_desc="Reserved Units">--}}
        {{--<a href="#">--}}
        {{--<span>View All</span>--}}
        {{--<i class="fa fa-angle-right" aria-hidden="true"></i>--}}
        {{--</a>--}}
        {{--</summary-card>--}}

        <summary-card
                title="Payments"
                statistics="{!! \Cytonn\Presenters\AmountPresenter::currency($cash_paid) !!}"
                statistics_desc="Cash Paid"
                link="/dashboard/realestate/payments">
            <a href="/dashboard/realestate/payments">
                <span>View All</span>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </summary-card>

        <summary-card
                title="Upcoming Payments"
                statistics="{!! \Cytonn\Presenters\AmountPresenter::currency($cash_expected_this_month) !!}"
                statistics_desc="Cash Expected ({!! \Carbon\Carbon::today()->format('F') !!})"
                link="/dashboard/realestate/payments-schedules">
            <a href="/dashboard/realestate/payments-schedules">
                <span>View All</span>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </summary-card>

        <summary-card
                title="Real Estate Instructions"
                statistics="{!! $total_re_instructions !!} "
                statistics_desc="Pending Instructions"
                link="/dashboard/realestate/client-instructions">
            <a href="/dashboard/realestate/client-instructions">
                <span>View All</span>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </summary-card>

    </div>

    <!-- MENU -->
    <div class="admin_menus dashboard__account_summary">
        <div class="summaries">
            <menu-card
                    title="Projects"
                    link="/dashboard/realestate/projects"
                    :first="true"
                    description="Manage Real Estate Projects">
                <i class="fa fa-tags"></i>
            </menu-card>

            <menu-card
                    title="LOOs"
                    link="/dashboard/realestate/loos"
                    :first="false"
                    description="Manage Letters of Offer">
                <i class="fa fa-file-pdf-o"></i>
            </menu-card>

            <menu-card
                    title="Sales Agreements"
                    link="/dashboard/realestate/sales-agreements"
                    :first="false"
                    description="Manage Sales Agreements">
                <i class="fa fa-file-pdf-o"></i>
            </menu-card>

            <menu-card
                    title="Commissions"
                    link="/dashboard/realestate/commissions">
                <i class="fa fa-money"></i>
            </menu-card>

            <menu-card
                    title="Advocates"
                    link="/dashboard/realestate/advocates"
                    :first="false"
                    description="Manage Real Estate Advocates">
                <i class="fa fa-gavel"></i>
            </menu-card>

            <menu-card
                    title="Reports"
                    link="/dashboard/realestate/reports"
                    :first="false"
                    description="Export Real Estate Reports">
                <i class="fa fa-files-o"></i>
            </menu-card>

            <menu-card
                    title="Documents"
                    link="/dashboard/realestate/documents"
                    :first="false"
                    description="Manage Real Estate Documents">
                <i class="fa fa-file-pdf-o"></i>
            </menu-card>

            <menu-card
                    title="Forfeiture Notices"
                    link="/dashboard/realestate/forfeiture_notices"
                    :first="false"
                    description="View Forfeiture Notices">
                <i class="fa fa-bell-o "></i>
            </menu-card>

        </div>
    </div>
@endsection