@extends('layouts.default')
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <div ng-controller = "realEstateUnitGroupsController">
                <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                    <thead>
                    <tr class="no-print">
                        <th  class="no-print" colspan="2"></th>
                        <th>
                            {!! Form::label('project_id', 'Project') !!}
                            {!! Form::select('project_id', $projects, null, ['ng-model'=>'project_id', 'class'=>'form-control', 'ng-change' => 'callServer(tableState)']) !!}
                        </th>
                        <th>
                            <a class="margin-bottom-20 pull-right btn btn-success" href="/dashboard/realestate/unit_groups/create"> <i class="fa fa-plus"></i> Add new</a>
                        </th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Project</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed" class="no-page-break">
                        <td><% row.name %></td>
                        <td><% row.project %></td>
                        <td><a href="/dashboard/realestate/unit_groups/create/<% row.id %>" class="fa fa-edit"></a> </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="4" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot class="no-print">
                    <tr>
                        <td colspan = "100%" class = "text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@stop