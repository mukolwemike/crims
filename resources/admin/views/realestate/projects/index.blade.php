@extends('layouts.default')

@section('content')
    <div class = "panel-dashboard">
        <div class="btn-group" role="group" aria-label="...">
            <a class="btn btn-default" href="/dashboard/realestate/projects/create">Create Project</a>
            <a class="btn btn-default" href="/dashboard/realestate/unitsizes">Unit Sizes</a>
            <a class="btn btn-default" href="/dashboard/realestate/payments">Payments</a>
            <a href="/dashboard/realestate/loos" class="btn btn-default">LOOs</a>
            <a href="/dashboard/realestate/sales-agreements" class="btn btn-default">Sales Agreements</a>
            <a class="btn btn-default" href="/dashboard/realestate/clients">Clients</a>
            <a class="btn btn-default" href="/dashboard/realestate/commissions">Commissions</a>
        </div>
        <div ng-controller = "RealEstateProjectsGridCtrl">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <th>Name</th><th>Code</th><th>Reserved</th><th>Total</th><th>Total Payments</th><th>Total Refunds</th><th>Remaining</th><th></th>
                    </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed">
                        <td><% row.name %></td>
                        <td><% row.code %></td>
                        <td><% row.reserved_count %></td>
                        <td><% row.total_unit_count %></td>
                        <td><% row.total_payments | currency:"" %></td>
                        <td><% row.total_refunds | currency:"" %></td>
                        <td><% row.remaining | currency:"" %></td>
                        <td>
                            <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                            <a ng-controller = "PopoverCtrl" uib-popover = "Setup project" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/setup/<% row.id %>"><i class = "fa fa-cog"></i></a>
                            <a ng-controller = "PopoverCtrl" uib-popover = "Edit" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/create/<% row.id %>"><i class = "fa fa-edit"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr><td colspan="100%">Loading...</td></tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "2" class = "text-center">
                        Items per page
                    </td>
                    <td colspan = "2" class = "text-center">
                        <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@endsection