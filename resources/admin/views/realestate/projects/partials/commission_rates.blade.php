<table class="table table-striped table-responsive">
    <thead>
    <tr>
        <th>Date</th><th>Project</th><th>Recipient Type</th><th>Type</th><th>Amount</th><th>Details</th>
    </tr>
    </thead>
    <tbody>
    @foreach($commission_rates as $rate)
        <tr>
            <td>{!! $rate->date !!}</td>
            <td>{!! $rate->project->name !!}</td>
            <td>{!! $rate->recipientType->name !!}</td>
            <td>{!! $rate->type !!}</td>
            <td>{!! $rate->amount !!}</td>
            <td>
                <a href="#" data-toggle="modal" data-target="#update-rate-{!!$rate->id!!}" title="Edit Rate"><i class = "fa fa-pencil-square-o"></i></a>
                <!-- Edit Modal -->
                <div class="modal fade" id="update-rate-{!!$rate->id!!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open(['route'=>['update_realestate_commission_rate', $rate->id]]) !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Commission Rate</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form">
                                    {!! Form::hidden('project_id', $project->id, null) !!}

                                    <div class="form-group"  ng-controller="DatepickerCtrl">
                                        {!! Form::label('date', 'Effective Date') !!}
                                        {!! Form::text('date', $rate->date, ['class'=>'form-control', 'datepicker-popup init-model'=>"effective_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('recipient_type_id', 'Recipient Type') !!}
                                        {!! Form::select('recipient_type_id', $recipient_types, $rate->recipient_type_id, ['class'=>'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('type', 'Type') !!}
                                        {!! Form::select('type', ['amount'=>'Amount', 'percentage'=>'Percentage'], $rate->type, ['class'=>'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('amount', 'Amount') !!}
                                        {!! Form::text('amount', $rate->amount, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                {!! Form::submit('Update Rate', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                {{--<a href="#" data-toggle="modal" data-target="#delete-rate-{!!$rate->id!!}" class="padding-left-20" style="color:#FF4500" title="Delete Rate"><i class = "fa fa-remove"></i></a>--}}
            </td>


            {{--<!-- Delete Modal -->--}}
            {{--<div class="modal fade" id="delete-rate-{!!$rate->id!!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
                {{--<div class="modal-dialog" role="document">--}}
                    {{--<div class="modal-content">--}}
                        {{--{!! Form::model($rate, ['route'=>['delete_realestate_commission_rate', $rate->id], 'method' => 'delete']) !!}--}}
                        {{--<div class="modal-body">--}}
                            {{--<div class="form">--}}
                                {{--<p>Are you sure you want to delete this commission rate?</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                            {{--{!! Form::submit('Delete Rate', ['class'=>'btn btn-success']) !!}--}}
                        {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </tr>
    @endforeach
    </tbody>
</table>

<button href="#" data-toggle="modal" data-target="#create-rate" class="btn btn-success margin-bottom-20">Create New</button>

<!-- Modal -->
<div class="modal fade" id="create-rate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>'create_realestate_commission_rate']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create a Commission Rate</h4>
            </div>
            <div class="modal-body">
                <div class="form">

                    {!! Form::hidden('project_id', $project->id, null) !!}

                    <div class="form-group"  ng-controller="DatepickerCtrl">
                        {!! Form::label('date', 'Effective Date') !!}
                        {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"effective_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('recipient_type_id', 'Recipient Type') !!}

                        {!! Form::select('recipient_type_id', $recipient_types, null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('type', 'Type') !!}

                        {!! Form::select('type', ['amount'=>'Amount', 'percentage'=>'Percentage'], null, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('amount', 'Amount') !!}

                        {!! Form::text('amount', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save Rate', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>