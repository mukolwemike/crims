<!-- Add Modal -->
<div class="modal fade" id="trancheModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['add_realestate_tranche', $project->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a tranche</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}

                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Availability date') !!}

                            {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>'date', 'ng-focus'=>'open($event)', 'is-open'=>'status.opened']) !!}
                        </div>
                    </div>
                </div>
                <hr/>
                <h5>Prices</h5>

                @foreach(array_keys($available_size_arr) as $size)
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('size_id[]', 'Unit size') !!}
                                {!! Form::select('size_id[]', $available_size_arr, $size, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('number[]', 'No of Units') !!}

                                {!! Form::text('number[]', null, ['class'=>'form-control']) !!}
                            </div>
                            {!! Form::hidden('ident[]', $size) !!}
                        </div>
                        @if($floors)
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('floor_id[]', 'Floor') !!}
                                    {!! Form::select('floor_id[]', $floors, null, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                        @endif
                        @if($real_estate_types)
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('type_id[]', 'Type') !!}
                                    {!! Form::select('type_id[]', $real_estate_types, null, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                        @endif
                        @foreach(array_keys($payment_plans) as $payment_plan)
                            <div class="col-md-12">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label("payment_plan_id[$size][]", 'Payment Plan') !!}

                                        {!! Form::select("payment_plan_id[$size][]", $payment_plans, $payment_plan, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label("price[$size][]", 'Price') !!}

                                        {!! Form::text("price[$size][]", null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label("value[$size][]", 'Value') !!}

                                        {!! Form::text("value[$size][]", null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>