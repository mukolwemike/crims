
<!-- Modal -->
<div class="modal fade" id="viewTrancheModal{!! $tranche->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! $tranche->name !!}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Size</th>
                                <th>Floor</th>
                                <th>Type</th>
                                <th>Number Available</th>
                                <th>Payment Plan</th>
                                <th>Price</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tranche->sizes  as $tranche_sizing)
                                <tr>
                                    <td>
                                        {!! $tranche_sizing->size->name !!}
                                    </td>
                                    <td>{!! $tranche_sizing->projectFloor ? $tranche_sizing->projectFloor->floor->floor : '' !!}</td>
                                    <td>{!! $tranche_sizing->realEstateType ? $tranche_sizing->realEstateType->type->name : '' !!}</td>
                                    <td>
                                        @if(isset($tranche_sizing['number']))
                                            {!! $tranche_sizing['number'] !!}
                                        @endif
                                    </td>
                                    <td colspan="3"></td>
                                </tr>
                                @foreach($tranche_sizing->prices as $pricing)
                                    <tr>
                                        <td></td><td></td><td></td><td></td>
                                        <td>{!! $pricing->paymentPlan->name !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($pricing->price) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($pricing->value) !!}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>