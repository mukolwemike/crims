@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">

            @include('realestate.projects.units.unit_detail_partial')

            <ul class="media-list hover">
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img data-name="R" class="avatar_img"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Reservation</h4>

                        @if(!$holding)
                            This unit has not been reserved.
                            <div class="margin-top-20">
                                <a class="btn btn-success"
                                   href="/dashboard/realestate/projects/{!! $project->id !!}/units/reserve/{!! $unit->id !!}">Reserve</a>
                            </div>
                        @else
                            The unit was reserved by <a
                                    href="/dashboard/clients/details/{!! $holding->client_id !!}">{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id) !!}</a>
                            .

                            @if($holding->tranche)
                                The unit is priced
                                at {!! \Cytonn\Presenters\AmountPresenter::currency($holding->price(true)) !!}, a
                                discount of {!! $holding->discount !!}% on

                                @if($holding->negotiated_price)
                                    a negotiated price.
                                @else
                                    the {!! $holding->tranche->name !!}.
                                @endif
                                The payment plan is {!! $holding->paymentPlan->name !!}.
                            @endif
                            <div class="margin-top-20">
                                <a class="btn btn-success"
                                   href="/dashboard/realestate/reservations/show/{!! $holding->reservation->id !!}">Reservation
                                    form</a>
                                @if($holding->payments->count() == 0)
                                    <a class="btn btn-success"
                                       href="/dashboard/realestate/payments/create/{!! $holding->id !!}/reservation_fee">Add
                                        reservation fee</a>
                                    <a href="/dashboard/realestate/payments-schedules/show/{!! $holding->id !!}"
                                       class="btn btn-primary">Payment Schedules</a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete
                                        Holding</a>
                                @else
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#forfeitModal">Forfeit
                                        unit</a>
                                @endif
                                <a class="btn btn-primary"
                                   href="/dashboard/realestate/projects/{!! $project->id !!}/units/transfer/{!! $unit->id !!}">Transfer
                                    unit</a>
                                @if(! $holding->repo->currentActiveFeitureNotice())
                                    <a class="btn btn-info" data-toggle="modal" data-target="#confirmSendForfeitModal">Send
                                        Forfeiture Notice</a>
                                @endif
                            </div>
                        @endif
                    </div>
                </li>

                @if($paid)
                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="A" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Actions</h4>
                            <div class="actions">
                                @if($holding->payments->count() > 0)
                                    <a class="btn btn-success"
                                       href="/dashboard/realestate/payments/create/{!! $holding->id !!}">Add a
                                        payment</a>
                                @endif
                                <a href="/dashboard/realestate/payments-schedules/show/{!! $holding->id !!}"
                                   class="btn btn-primary">Payment Schedules</a>
                                <button class="btn btn-primary" data-toggle="modal"
                                        data-target="#editPaymentInformation">Edit Pricing Information
                                </button>
                                    @if($holding->receive_campaign_statement)
                                        <a href="/dashboard/realestate/projects/holdings/{!! $holding->id !!}/deactivate" class="btn btn-danger">Deactivate Monthly Statements</a>
                                    @else
                                        <a href="/dashboard/realestate/projects/holdings/{!! $holding->id !!}/activate" class="btn btn-info">Activate Monthly Statements</a>
                                    @endif
                            </div>
                        </div>
                    </li>
                @endif

                @if($paid)
                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="$" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Payment</h4>

                            <div class="actions">
                                <p class="inline-block">KES {!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}
                                    has been paid, <a href="" ng-click="show_payment_details = !show_payment_details">view
                                        details</a></p>

                                <a class="close" href="" ng-show="show_payment_details"
                                   ng-click="show_payment_details = !show_payment_details" class="right"> &times;</a>
                            </div>

                            <div ng-show="show_payment_details">
                                <hr class="thick"/>
                                @include('realestate.projects.units.partials.payments')
                            </div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="$" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Refunds</h4>

                            <div class="actions">
                                <p class="inline-block">
                                    KES {!! \Cytonn\Presenters\AmountPresenter::currency($refunds->sum('amount')) !!}
                                    has been refunded, <a href="" ng-click="show_refund_details = !show_refund_details">view
                                        refunds</a></p>

                                <a class="close" href="" ng-show="show_payment_details"
                                   ng-click="show_refund_details = !show_refund_details" class="right"> &times;</a>
                            </div>

                            @if($balance > 0)
                                <div class="">
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#refundModal">Refund</a>
                                </div>
                            @endif

                            <div ng-show="show_refund_details">
                                <hr class="thick"/>
                                @include('realestate.projects.units.partials.refunds')
                            </div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="C" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Commission</h4>

                            <div class="actions">
                                <p class="inline-block">
                                    @if($holding->commission)
                                        <a class="btn btn-info"
                                           href="{!! route('commission_for_unit', $holding->id) !!}">View commission
                                            details</a>
                                    @else
                                        Commission details are not yet available
                                    @endif
                                </p>
                            </div>
                        </div>
                    </li>

                    @if($loo)
                        <li class="media">
                            <div class="media-left">
                                <a href="#"><img data-name="L" class="avatar_img"/></a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Letter of offer</h4>

                                <div class="actions">
                                    @if($loo->document_id)
                                        <p class="inline-block">A letter of offer
                                            dated {!! \Cytonn\Presenters\DatePresenter::formatDate($loo->date) !!} has
                                            been uploaded.
                                            <a href="#" ng-click="show_loo_details = !show_loo_details">view details</a>
                                        </p>
                                    @else
                                        <p class="inline-block">A Letter of offer should be prepared
                                            by {!! \Cytonn\Presenters\DatePresenter::formatDate($loo->due_date) !!}. <a
                                                    href="#" ng-click="show_loo_details = !show_loo_details">Upload
                                                Loo</a></p>
                                    @endif

                                    <a class="close right" href="#" ng-show="show_loo_details"
                                       ng-click="show_loo_details = !show_loo_details"> &times;</a>
                                </div>

                                <div ng-show="show_loo_details">
                                    <hr class="thick"/>
                                    @include('realestate.projects.units.partials.loo', ['uploaded'=> (bool)$loo->document_id])
                                </div>
                            </div>
                        </li>
                    @endif

                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="S" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Sales agreement</h4>

                            <div class="actions">
                                @if(!$sales_agreement)
                                    <p class="inline-block">The sales agreement has not been uploaded
                                        <a href="#" ng-click="show_sa_details = !show_sa_details">view details</a>
                                    </p>
                                @else
                                    <p class="inline-block">A sales agreement
                                        dated {!! \Cytonn\Presenters\DatePresenter::formatDate($sales_agreement->date) !!}
                                        is available
                                        <a href="#" ng-click="show_sa_details = !show_sa_details">view details</a>
                                    </p>
                                @endif
                                <a class="close" href="#" ng-show="show_sa_details"
                                   ng-click="show_sa_details = !show_sa_details" class="right"> &times;</a>
                            </div>

                            <div ng-show="show_sa_details">
                                <hr class="thick"/>
                                @include('realestate.projects.units.partials.sales_agreement', ['uploaded'=> (bool)$sales_agreement->document_id])
                            </div>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($holding)
        @include('realestate.projects.units.partials.forfeit_modal')
        @include('realestate.projects.units.partials.send_forfeit_modal')
        @include('realestate.projects.units.partials.delete_modal')
        @include('realestate.projects.units.partials.edit_pricing_information')
        @include('realestate.projects.units.partials.refund_modal')
        @include('realestate.projects.units.partials.cancel_forfeit_notice_modal')
    @endif
@endsection
