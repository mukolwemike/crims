@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">

            @include('realestate.projects.units.unit_detail_partial')

            <ul class="media-list hover">
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img data-name="R" class="avatar_img"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Reservation</h4>

                        @if(!$holding)
                            This unit has not been reserved.
                            <div class="margin-top-20">
                                <a class="btn btn-success" href="/dashboard/realestate/projects/{!! $project->id !!}/units/reserve/{!! $unit->id !!}">Reserve</a>
                            </div>
                        @else
                            The unit had been previously reserved by <a href="/dashboard/clients/details/{!! $holding->client_id !!}">{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id) !!}</a>.

                            @if($holding->tranche)
                                The unit was priced at {!! \Cytonn\Presenters\AmountPresenter::currency($holding->price(true)) !!}, a discount of {!! $holding->discount !!}% on

                                @if($holding->negotiated_price)
                                    a negotiated price.
                                @else
                                    the {!! $holding->tranche->name !!}.
                                @endif
                                The payment plan was {!! $holding->paymentPlan->name !!}.
                            @endif
                            The unit was forfeited on {!! \Cytonn\Presenters\DatePresenter::formatDate($holding->forfeit_date) !!}. <br/><br/>
                            Reason for forfeiture:  @if($holding->forfeit_reason)
                                                        {!! $holding->forfeit_reason !!}
                                                    @else
                                                        No reason has been provided.
                                                    @endif
                            <br/><br/>
                            Refunded Amount: KES @if($holding->refunds)
                                                    {!! \Cytonn\Presenters\AmountPresenter::currency($holding->refundedAmount()) !!}
                                                @else
                                                    No refunds have been made.
                                                @endif
                            <div class="margin-top-20 btn-group">
                                <a class="btn btn-success" href="/dashboard/realestate/reservations/show/{!! $holding->reservation->id !!}">Reservation form</a>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#reverse-forfeiture"><i class="fa fa-reply"></i> Reverse Forfeiture</button>
                                <div class="modal fade" id="reverse-forfeiture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Reverse Forfeiture</h4>
                                            </div>
                                            <div class="modal-body">
                                                @if($isCurrentlyReservedByAnotherClient)
                                                    You cannot reverse this forfeiture because another client has already reserved it.
                                                @else
                                                    {!! Form::open(['route'=>['real_estate_reverse_forfeiture', $holding->id]]) !!}
                                                    <p>
                                                        Are you sure you want to reverse this unit forfeiture?
                                                    </p>
                                                    <div class="form-group">
                                                        {!! Form::submit('Reverse', ['class'=>'btn btn-danger']) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                @endif
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </li>

                @if($paid)
                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="A" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Actions</h4>
                            <div class="actions">
                                @if($holding->payments()->count() > 0)
                                    <a class="btn btn-success disabled" href="#">Add a payment</a>
                                @endif
                                <a href="/dashboard/realestate/payments-schedules/show/{!! $holding->id !!}" class="btn btn-primary">Payment Schedules</a>
                            </div>
                        </div>
                    </li>
                @endif

                @if($paid)
                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="$" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Payment</h4>

                            <div class="actions">
                                <p class="inline-block">KES {!! \Cytonn\Presenters\AmountPresenter::currency($total) !!} has been paid, <a href="#" ng-click="show_payment_details = !show_payment_details">view details</a></p>

                                <a class="close" href="#" ng-show="show_payment_details" ng-click="show_payment_details = !show_payment_details" class="right"> &times;</a>
                            </div>

                            <div ng-show="show_payment_details">
                                <hr class="thick"/>
                                @include('realestate.projects.units.partials.payments')
                            </div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="C" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Commission</h4>

                            <div class="actions">
                                <p class="inline-block">
                                @if($holding->commission)
                                    KES ----- was paid to -------.
                                @else
                                    Commission details are not yet available
                                @endif
                                    <a href="#">view details</a>
                                </p>
                            </div>
                        </div>
                    </li>

                    @if($loo)
                        <li class="media">
                            <div class="media-left">
                                <a href="#"><img data-name="L" class="avatar_img"/></a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Letter of offer</h4>

                                <div class="actions">
                                    @if($loo->document_id)
                                        <p class="inline-block">A letter of offer dated {!! \Cytonn\Presenters\DatePresenter::formatDate($loo->date) !!} was uploaded.
                                            <a href="#" ng-click="show_loo_details = !show_loo_details">view details</a>
                                        </p>
                                    @else
                                        <p class="inline-block">A Letter of offer should be prepared by {!! \Cytonn\Presenters\DatePresenter::formatDate($loo->due_date) !!}. <a href="#" ng-click="show_loo_details = !show_loo_details">Upload Loo</a></p>
                                    @endif

                                    <a class="close right" href="#" ng-show="show_loo_details" ng-click="show_loo_details = !show_loo_details"> &times;</a>
                                </div>

                                <div ng-show="show_loo_details">
                                    <hr class="thick"/>
                                    @include('realestate.projects.units.partials.loo', ['uploaded'=> (bool)$loo->document_id])
                                </div>
                            </div>
                        </li>
                    @endif

                    <li class="media">
                        <div class="media-left">
                            <a href="#"><img data-name="S" class="avatar_img"/></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Sales agreement</h4>

                            <div class="actions">
                                @if(!$sales_agreement)
                                    <p class="inline-block">The sales agreement was not uploaded
                                        <a href="#" ng-click="show_sa_details = !show_sa_details">view details</a>
                                    </p>
                                @else
                                    <p class="inline-block">A sales agreement dated {!! \Cytonn\Presenters\DatePresenter::formatDate($sales_agreement->date) !!} was uploaded.
                                        <a href="#" ng-click="show_sa_details = !show_sa_details">view details</a>
                                    </p>
                                @endif
                                    <a class="close" href="#" ng-show="show_sa_details" ng-click="show_sa_details = !show_sa_details" class="right"> &times;</a>
                            </div>

                            <div ng-show="show_sa_details">
                                <hr class="thick"/>
                                @if((bool)$sales_agreement->document_id)
                                    <div class="col-md-6">
                                        <div class="btn-group margin-bottom-20">
                                            <a class="btn btn-success" href="/dashboard/documents/{!! $sales_agreement->document_id !!}">View Sales Agreement</a>
                                            <a class="btn btn-info" href="/dashboard/realestate/sales-agreements/show/{!! $sales_agreement->id !!}">Sales Agreement Details</a>
                                        </div>
                                        @if($previous_sas)
                                            @if($previous_sas->count() > 0)
                                                <p>The following previous sales agreement have been uploaded</p>
                                            @endif
                                        @endif

                                        <ol>
                                            @if($previous_sas)
                                                @foreach($previous_sas as $d)
                                                    <li>[ {!! $d->title !!} ] <a href="{!! route('show_document', [$d->id]) !!}">{!! $d->name !!}</a></li>
                                                @endforeach
                                            @endif
                                        </ol>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($holding)
        @include('realestate.projects.units.partials.forfeit_modal')
        @include('realestate.projects.units.partials.delete_modal')
        @include('realestate.projects.units.partials.edit_pricing_information')
    @endif
@endsection