@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <div>
                <h4>Unit Details</h4>
                <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                    <tbody>
                        <tr><td>Unit Number</td><td>{!! $unit->number !!}</td></tr>
                        <tr><td>Unit Type</td><td>{!! $unit->size->name !!} {!! $unit->type->name !!}</td></tr>
                        <tr><td>Current Holder (Client)</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($holding->client_id) !!}</td></tr>
                        <tr><td>Client Code</td><td>{!! $holding->client->client_code !!}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class = "panel-dashboard">
            <h4>Transfer unit</h4>
                <div class="row" ng-controller="ClientUserCtrl">
                    <div class="col-md-6">
                        {!! Form::open(['route'=>['unit_transfer', $holding->id]]) !!}
                        <div class="form-group">
                            <label>Select Client</label>
                            <input type="text" ng-model="selectedClient" placeholder="Enter name of client" typeahead="client as client.fullName for client in searchClients($viewValue)" class="form-control" typeahead-loading="loading" typeahead-show-hint="true" typeahead-min-length="1" typeahead-no-results="noResults">
                            <i ng-show="loading" class="fa fa-refresh fa-spin"></i>
                            <div ng-show="noResults">
                                <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>
                            <div class="hide">
                                <input name="client_id" type="text" ng-model="selectedClient.id">
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Transfer', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <h5>Client Details</h5>
                            <table class="table table-responsive">
                                <thead></thead>
                                <tbody>
                                <tr>
                                    <td>Full Name</td><td><% selectedClient.fullName %></td>
                                </tr>
                                <tr>
                                    <td>Email</td><td><% selectedClient.contact.email %></td>
                                </tr>
                                <tr>
                                    <td>Member Number</td><td><% selectedClient.client_code %></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection