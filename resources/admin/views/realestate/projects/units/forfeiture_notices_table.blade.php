@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="ForfeitureNoticeController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>
                        <select st-search="project" class="form-control">
                            <option value="">All Projects</option>
                            @foreach($projects as $project)
                                <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                            @endforeach
                        </select>
                    </th>
                    <th colspan="2">
                        <select st-search="sent_status" class="form-control">
                            <option value="">Sent Status</option>
                            <option value="0">Pending</option>
                            <option value="1">Sent</option>
                            <option value="2">Canceled</option>
                        </select>
                    </th>
                    <th colspan="2"><input st-search="client_code" class="form-control" placeholder="Search client code..." type="text"/></th>
                    <th></th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Client Name</th>
                    <th>Project Name</th>
                    <th>Unit Number</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th></th>
                </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                <tr ng-repeat="row in displayed">
                    <td><% row.client_code %></td>
                    <td><% row.client_name %></td>
                    <td><% row.project_name %></td>
                    <td><% row.unit_id %></td>
                    <td ng-if="row.status == 0" >
                        <i class="fa fa-check-circle-o" style="color:blue;"> </i> Pending Approval
                    </td>
                    <td ng-if="row.status == 1">
                        <i class="fa fa-check-circle-o" style="color:green;"> </i> Sent
                    </td>
                    <td ng-if="row.status == 2">
                        <i class="fa fa-warning" style="color:orange;"></i> Canceled
                    </td>
                    <td>
                        <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/realestate/forfeiture_notices/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                    </td>
                </tr>

                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="100%" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop