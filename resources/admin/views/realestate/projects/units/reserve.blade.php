@extends('layouts.default')

@section('content')
    {{--<div class="panel-dashboard">--}}
        {{--@if(isset($form))--}}
            {{--{!! Form::model($form, array('url' => 'Dashboard'.$form->id, 'method' => 'put')) !!}--}}
        {{--@else--}}
            {{--{!! Form::open() !!}--}}
        {{--@endif--}}

        {{--<div class="detail-group">--}}
            {{--<h4 class="col-md-12">Client Details</h4>--}}

            {{--<div class="col-md-12">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-2">{!! Form::label('client_type_id', 'Client type') !!}</div>--}}
                    {{--<div class="col-md-4">{!! Form::select('client_type_id', $clientTypes , null, ['class'=>'form-control', 'init-model'=>'client_type']) !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-12" ng-show="client_type == '1'">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-2">--}}
                        {{--{!! Form::label('name', 'Full Name') !!}--}}
                    {{--</div>--}}

                    {{--<div class="col-md-2">--}}
                        {{--{!! Form::select('title_id',$titles, null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}--}}
                    {{--</div>--}}

                    {{--<div class="col-md-3">{!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'First Name'])  !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>--}}

                    {{--<div class="col-md-2">{!! Form::text('middlename', null, ['class'=>'form-control', 'placeholder'=>'Middle Name']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}</div>--}}

                    {{--<div class="col-md-3">{!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Last Name']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6" ng-hide="client_type == '1'">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('corporate_registered_name', 'Registered name') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('corporate_registered_name', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_registered_name') !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6" ng-hide="client_type == '1'">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('corporate_trade_name', 'Trade name') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('corporate_trade_name', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_trade_name') !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6" ng-hide="client_type == '1'">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('registered_address', 'Registered address') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('registered_address', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_address') !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6" ng-hide="client_type == '1'">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('registration_number', 'Registration number') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('registration_number', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registration_number') !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">--}}
                        {{--{!! Form::label('email', 'Email') !!}--}}
                    {{--</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('email', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('postal_address', 'Postal Address') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('postal_code', 'Postal code') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('town', 'Town/City') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('town', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('street', 'Physical Address') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('street', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('country_id', 'Country') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::select('country_id', $countries, 114, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country_id') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('nationality_id', 'Nationality') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::select('nationality_id', $countries, 114, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'nationality_id') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label("phone", 'Phone Number') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('phone', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_home') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('telephone_office', 'Work Telephone Number') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('telephone_office', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_office') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('telephone_home', 'Home phone number') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('telephone_home', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_cell') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('id_or_passport', 'ID or Passport Number') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('id_or_passport', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_or_passport') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('pin_no', 'PIN Number') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('pin_no', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="detail-group" ng-hide="client_type == '1'">--}}
            {{--<h4 class="col-md-12">Company contact person</h4>--}}

            {{--<div class="col-md-12">--}}
                {{--<div class="col-md-2"> <span class="required-field">{!! Form::label('contact_person', 'Contact person') !!}</span></div>--}}

                {{--<div class="col-md-2">{!! Form::select('contact_person_title', $titles , null, ['class'=>'form-control', 'placeholder'=>'Title']) !!}</div>--}}

                {{--<div class="col-md-4">{!! Form::text('contact_person_lname', null, ['class'=>'form-control', 'placeholder'=>'Last name']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'surname') !!}</div>--}}


                {{--<div class="col-md-4">{!! Form::text('contact_person_fname', null, ['class'=>'form-control', 'placeholder'=>'First name']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>--}}
                {{--<div class="clearfix"></div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="detail-group">--}}
            {{--@include('investment.partials.re_bankdetails')--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('investor_clearing_code', 'Clearing code') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('investor_clearing_code', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_clearing_code') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-6">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">{!! Form::label('investor_swift_code', 'Swift code') !!}</div>--}}

                    {{--<div class="col-md-8">{!! Form::text('investor_swift_code', null, ['class'=>'form-control']) !!}--}}
                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_swift_code') !!}</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div ng-controller="ClientUserCtrl">--}}
            {{--<div class="detail-group">--}}
                {{--<h4 class="col-md-12">Link to existing client</h4>--}}
                {{--<br/><br/>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-4">--}}
                            {{--{!! Form::label('new_client', 'New or Existing Client') !!}--}}
                        {{--</div>--}}

                        {{--<div class="col-md-8">--}}
                            {{--{!! Form::select('new_client', [1 => 'New Client', 0 => 'Existing Client', 2 => 'Duplicate Client'], null, ['class'=>'form-control',  'ng-model' => 'existing_client']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 margin-bottom-20">--}}
                        {{--<div class="form-group">--}}

                        {{--{!! Form::radio('new_client', true, true, ['init-model'=>'existing_client']) !!}--}}

                        {{--{!! Form::label('new_client', 'Existing Client') !!}--}}
                        {{--</div>--}}

                        {{--</div>--}}

                        {{--<div class="col-md-4 margin-bottom-20 clearfix">--}}
                        {{--<div class="form-group">--}}
                        {{--{!! Form::radio('new_client', false, null, ['init-model'=>'existing_client']) !!}--}}

                        {{--{!! Form::label('new_client', 'New Client') !!}--}}

                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'new_client') !!}<br/>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-8">--}}
                            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-4">--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::checkbox('penalty_exempt', 1) !!}--}}
                            {{--{!! Form::label('penalty_exempt', 'Penalty Exemption') !!}--}}
                            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'penalty_exempt') !!}<br/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="detail-group" ng-show="existing_client == '0'">--}}
                {{--<h4 class="col-md-12">Client Search</h4>--}}
                {{--<br/><br/>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Search Client by Name</label>--}}

                        {{--<search-client name="client_id"></search-client>--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6"></div>--}}
            {{--</div>--}}

            {{--<div class="detail-group" ng-show="existing_client == '2'">--}}
                {{--<h4 class="col-md-12">Duplicate Client</h4>--}}
                {{--<br/><br/>--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-2"><span class="required-field">{!! Form::label('duplicate_reason', 'Duplicate Reason') !!}</span></div>--}}
                        {{--<div class="col-md-10">--}}
                            {{--{!! Form::textarea('duplicate_reason', null, ['id' => 'duplicate_reason', 'rows' => 3, 'cols'=> 55, 'class'=>'form-control']) !!}--}}
                            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'duplicate_reason') !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="detail-group">--}}
                {{--<div class="col-md-12">--}}
                    {{--{!! Form::submit('Save Reservation', ['class'=>'btn btn-success']) !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--{!! Form::close() !!}--}}
    {{--</div>--}}

    <div class = "col-md-12">
        <client-application :edit="{{ json_encode($edit) }}" :type="{{ json_encode($type) }}" :unit="{{ json_encode($unit) }}"  :application="{{ json_encode($applicationId) }}"></client-application>
    </div>

@endsection