<table class="table table-hover">
    <tbody>
    <tr>
        <td>Project</td>
        <td>{!! $unit->project->name !!}</td>
    </tr>
    <tr>
        <td>Unit</td>
        <td>{!! $unit->number !!}</td>
    </tr>
    <tr>
        <td>Size</td>
        <td>{!! $unit->size->name !!}</td>
    </tr>
    @if($unit->realEstateType)
        <tr>
            <td>Type</td>
            <td>{!! $unit->realEstateType->type->name !!}</td>
        </tr>
    @endif
    @if($unit->projectFloor)
        <tr>
            <td>Floor</td>
            <td>{!! $unit->projectFloor->floor->floor !!}</td>
        </tr>
    @endif
    @if($holding)
        @if($holding->paymentPlan)
            <tr>
                <td>Payment plan</td>
                <td>{!! $holding->paymentPlan->name !!}</td>
            </tr>
        @endif
        <tr>
            <td>Penalty Exemption</td>
            <td>{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($holding->penalty_excempt) !!}</td>
        </tr>
        <tr>
            <td>Forfeiture Notice Status</td>
            <td>
                @if(!$forfeiture==null)
                    @if($forfeiture->status == 0)
                        <i class="fa fa-check-circle-o" style="color:blue;"> </i> Pending Approval
                    @elseif($forfeiture->status == 1)
                        <i class="fa fa-check-circle-o" style="color:green;"> </i> Sent
                    @elseif($forfeiture->status == 2)
                        <i class="fa fa-warning" style="color:orange;"></i> Canceled
                    @endif
                @endif
            </td>
        </tr>
        @if(!$forfeiture==null && $forfeiture->status == 2)
            <tr>
                <td>Cancelation Reason</td>
                <td>
                    {!! $forfeiture->review_reason !!}
                </td>
            </tr>
        @endif
        <tr>
            <td>Forfeiture Notice Requested By</td>
            <td>
                @if(!$forfeiture==null)
                    {!! Cytonn\Presenters\UserPresenter::presentFullNames($forfeiture->sent_by) !!}
                @endif
            </td>
        </tr>
        <tr>
            <td>Forfeiture Notice Reviewed By</td>
            <td>
                @if(!$forfeiture==null)
                    {!! Cytonn\Presenters\UserPresenter::presentFullNames($forfeiture->reviewed_by) !!}
                @endif
            </td>
        </tr>

    @endif
    </tbody>
</table>