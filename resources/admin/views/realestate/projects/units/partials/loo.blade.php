@if(!$uploaded)
    <div class="btn-group btn-group-sm">
        <a class="btn btn-info" href="/dashboard/realestate/loos/show/{!! $loo->id !!}">View LOO Details</a>
        <a href="{!! route('generate_view_loo', $holding->id) !!}" class="btn btn-primary">Generate and preview</a>
    </div>
@else
    <div class="col-md-6">
        <div class="btn-group">
            <a class="btn btn-success" href="/dashboard/documents/{!! $loo->document_id !!}">View the Loo</a>
            <a class="btn btn-info" href="/dashboard/realestate/loos/show/{!! $loo->id !!}">LOO Details</a>
        </div>
    </div>
@endif