<!-- Modal -->
<div class="modal fade" id="confirmSendForfeitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Send Forfeiture Notice For Approval</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div class="form-group">
                        <p>Are you sure you want to send a forfeiture notice request for approval?</p>
                    </div>

                    <div class="margin-top-20">
                        <a class="btn btn-primary"
                           href="/dashboard/realestate/projects/units/forfeiture_notices/approval/{!! $holding->id !!}/">Send
                            Forfeiture Notice</a>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Go back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>