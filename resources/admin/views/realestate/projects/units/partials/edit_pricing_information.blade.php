<!-- Modal -->
<div class="modal fade" id="editPaymentInformation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" data-ng-controller="EditRealEstatePaymentController"
             data-ng-init="project_id = '{!! $holding->unit->project_id !!}';
             commission_rate = '{!! @$holding->commission->commission_rate !!}';
             commissionRateName='{!! $commissionRateName !!}';
             client_id = '{!! $holding->client_id !!}';
             reservationDate='{!! $holding->reservation_date !!}';">
            {!! Form::model($holding, ['route'=>['update_realestate_pricing_information', $holding->id], 'method'=>'PUT']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Pricing Information</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('tranche_id', 'Tranche') !!}
                    {!! Form::select('tranche_id', $tranches, $holding->tranche_id, ['class'=>'form-control', 'init-model'=>'tranche_id', 'id'=>'tranche_id']) !!}
                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tranche_id') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('payment_plan_id', 'Payment Plan') !!}
                    {!! Form::select('payment_plan_id', $payment_plans, null, ['class'=>'form-control', 'init-model'=>'payment_plan_id', 'id'=>'payment_plan_id']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('recipient_id', 'Select FA') !!}
                    {!! Form::select('recipient_id', $recipients, @$holding->commission->recipient_id, ['required', 'class'=>'form-control', 'ui-select2', 'init-model'=>'recipient_id', 'data-placeholder'=>"Select the FA", 'id'=>'recipients_id']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('awarded', 'Award Commission?') !!}

                    {!! Form::select('awarded', [0=>'No', 1=>'Yes'], @$holding->commission->awarded, ['class'=>'form-control', 'init-model'=>'awarded', 'id'=>'awarded']) !!}
                </div>

                <div class="form-group alert alert-info" ng-if="awarded == 1">
                    {!! Form::label('rate', 'Commission Rate') !!} :- <i class="fa fa-spinner fa-spin" ng-show="loadingRate === true"></i> {!! Form::label('rate', '<% commissionRateName %>') !!}
                    <br />
                </div>

                <div class = "form-group" ng-show="edit_rate">
                    <br />
                    {!! Form::label('rate', 'Commission Rate') !!}
                    {!! Form::number('rate', @$holding->commission->commission_rate, ['class'=>'form-control', 'step'=>'0.01', 'data-ng-value' => 'commission_rate', 'required', 'ng-model' => 'commission_rate']) !!}
                    {!! Form::hidden('commission_rate_name', NULL, [ 'data-ng-value' => 'commissionRateName', 'ng-model' => 'commissionRateName']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                </div>

                <div class="form-group" ng-show="edit_rate">
                    {!! Form::label('reason', 'Rate Reason') !!}
                    {!! Form::textarea('reason', @$holding->commission->reason, ['class'=>'form-control', 'rows' => '3', 'init-model'=>'reason']) !!}
                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                </div>

                <div class="form-group" ng-show="awarded == 1">
                    <br />
                    {!! Form::checkbox('edit', true, true, ['ng-model'=>'edit_rate']) !!} {!! Form::label('edit', "Edit Commission Rate") !!}
                    <br />
                </div>

                <a class="btn btn-success" ng-click="add_negotiated_price = !add_negotiated_price">Add a negotiated price</a>
                <br><br>

                <div class="form-group" ng-show="add_negotiated_price">
                    {!! Form::label('negotiated_price', 'Negotiated price') !!}
                    {!! Form::text('negotiated_price', $holding->negotiated_price, ['class'=>'form-control', 'init-model'=>'negotiated_price']) !!}
                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'negotiated_price') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('discount', 'Discount %') !!}

                    {!! Form::text('discount', $holding->discount, ['class'=>'form-control', 'init-model'=>'discount']) !!}
                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'discount') !!}
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Update Pricing Info', ['class'=>'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>