<table class="table table-responsive table-striped table-hover">

    <thead>
        <tr>
            <th>Date</th>
            <th>Description</th>
            <th>Amount</th>
        </tr>
    </thead>

    <tbody>
        @foreach($refunds as $refund)
            <tr>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($refund->date) !!}</td>
                <td>{!! $refund->narration !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($refund->amount) !!}</td>
            </tr>
        @endforeach
        <tr>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate('today') !!}</td>
            <th>Total</th>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($refunds->sum('amount')) !!}</th>
        </tr>
    </tbody>
</table>