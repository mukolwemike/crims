@if($uploaded)
    <div class="col-md-6">
        <div class="btn-group margin-bottom-20">
            <a class="btn btn-success" href="/dashboard/documents/{!! $sales_agreement->document_id !!}">View Sales Agreement</a>
            <a class="btn btn-info" href="/dashboard/realestate/sales-agreements/show/{!! $sales_agreement->id !!}">Sales Agreement Details</a>
        </div>
        @if($previous_sas->count() > 0)
            <p>The following previous sales agreement have been uploaded</p>
        @endif

        <ol>
            @foreach($previous_sas as $d)
                <li>[ {!! $d->title !!} ] <a href="{!! route('show_document', [$d->id]) !!}">{!! $d->name !!}</a></li>
            @endforeach
        </ol>
    </div>
@endif

<div class="col-md-6">
    <h3>Upload a sales agreement</h3>

    @include('realestate.sales_agreements.partials.upload_form', ['holding'=>$sales_agreement->holding])
</div>
