<table class="table table-responsive table-striped table-hover">
    <thead>
        <tr><th>Date</th><th>Description</th><th>Payment Type</th><th>Amount</th><th></th></tr>
    </thead>
    <tbody>
        @foreach($payments as $payment)
            <tr>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
                <td>{!! $payment->description !!}</td>
                <td>
                    @if(is_null($payment->schedule))
                    @else
                        {!! $payment->type->name !!}
                    @endif
                </td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
                <td><a href="/dashboard/realestate/payments/show/{!! $payment->id !!}"><i class="fa fa-list-alt"></i></a></td>
            </tr>
        @endforeach
        <tr>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate('today') !!}</td>
            <th>Total</th>
            <td></td>
            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->repo->netUnitTotalPaid()) !!}</th>
            <td></td>
        </tr>
    </tbody>
</table>