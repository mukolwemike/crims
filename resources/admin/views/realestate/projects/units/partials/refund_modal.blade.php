<!-- Modal -->
<div class="modal fade" id="refundModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['re_refund_payment', $holding->id]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Refund Payment</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('date', 'Date of Refund') !!}

                        {!! Form::text('date', null, ['class'=>'form-control', 'init-model'=>'date', 'datepicker-popup'=>'yyyy-MM-dd', 'is-open'=>'dt.open', 'ng-focus'=>'dt.open = !dt.open', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('amount', 'Amount that can be refunded: KES '.number_format($balance)) !!}

                        {!! Form::text('amount', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('narration', 'Narration') !!}

                        {!! Form::text('narration', null, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narration') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Go back</button>
                    {!! Form::submit('Refund payment', ['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>