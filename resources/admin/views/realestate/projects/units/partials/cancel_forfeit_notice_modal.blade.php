<!-- Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Cancel Forfeiture Notice</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('cancel_notice')}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" typeof="number" value="{{$holding->id}}" name="unit_id">
                    <div class="form-group">
                        <label>Cancel Reason:</label>
                        <textarea cols="5" type="text" class="form-control" name="review_reason"
                                  placeholder="Reason"></textarea>
                    </div>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Cancel Forfeiture Notice</button>
                </form>
            </div>

        </div>
    </div>
</div>