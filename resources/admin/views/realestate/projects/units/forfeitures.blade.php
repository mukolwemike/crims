@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <div>
                <h4>Unit Details</h4>
                    @include('realestate.projects.units.unit_detail_partial')
            </div>
            <div class="margin-top-20">
                @if(!$holding->forfeiture_letter_sent_on)
                    {!! Form::open(['url'=>'/dashboard/realestate/projects/units/forfeitures/'.$holding->id]) !!}

                        <div class="form-group">
                            {!! Form::submit('Send Forfeiture', ['class'=>'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                @else
                    <div class="alert alert-info">
                        <p>Forfeiture letter was sent</p>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection