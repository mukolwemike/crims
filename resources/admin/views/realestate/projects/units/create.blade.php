 @extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            {!! Form::model($unit, ['route'=>['store_realestate_unit', $project->id, $unit->id]]) !!}

            <div class="form-group">
                {!! Form::label('number', 'Unit Number') !!}

                {!! Form::text('number', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('type_id', 'Unit type') !!}

                {!! Form::select('type_id',  $unitTypes, null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('size_id', 'Unit size') !!}

                {!! Form::select('size_id', $project->availableSizes()->lists('name', 'id'), null, ['class'=>'form-control']) !!}
            </div>

            @if($groups)
                <div class="form-group">
                    {!! Form::label('group_id', 'Group unit belongs to') !!}

                    {{ Form::select('group_id', [''=> 'None'] + $groups, null, ['class' => 'form-control']) }}
                </div>
            @endif

            @if($floors)
                <div class="form-group">
                    {!! Form::label('project_floor_id', 'Floor unit belongs to') !!}

                    {{ Form::select('project_floor_id', [''=> 'None'] + $floors, null, ['class' => 'form-control']) }}
                </div>
            @endif

            @if($types)
                <div class="form-group">
                    {!! Form::label('real_estate_type_id', 'Type unit belongs to') !!}

                    {{ Form::select('real_estate_type_id', [''=> 'None'] + $types, null, ['class' => 'form-control']) }}
                </div>
            @endif

            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection