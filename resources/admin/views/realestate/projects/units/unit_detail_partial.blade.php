<table class="table table-hover">
    <tbody>
    <tr><td>Project</td><td>{!! $unit->project->name !!}</td></tr>
    <tr><td>Unit</td><td>{!! $unit->number !!}</td></tr>
    <tr><td>Size</td><td>{!! $unit->size->name !!}</td></tr>
    @if($unit->realEstateType)
        <tr>
            <td>Type</td>
            <td>{!! $unit->realEstateType->type->name !!}</td>
        </tr>
    @endif
    @if($unit->projectFloor)
        <tr>
            <td>Floor</td>
            <td>{!! $unit->projectFloor->floor->floor !!}</td>
        </tr>
    @endif
    @if($holding)
        @if($holding->paymentPlan)
            <tr>
                <td>Payment plan</td><td>{!! $holding->paymentPlan->name !!}</td>
            </tr>
        @endif
        <tr>
            <td>Penalty Exemption</td>
            <td>{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($holding->penalty_excempt) !!}</td>
        </tr>
    @endif
    </tbody>
</table>