@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            @include('realestate.projects.units.partials.unit_forfeiture_partial')

            <ul class="media-list hover">
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img data-name="R" class="avatar_img"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Reservation</h4>

                        The unit was reserved by <a
                                href="/dashboard/clients/details/{!! $holding->client_id !!}">{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id) !!}</a>
                        .

                        @if($holding->tranche)
                            The unit is priced
                            at {!! \Cytonn\Presenters\AmountPresenter::currency($holding->price(true)) !!}, a
                            discount of {!! $holding->discount !!}% on

                            @if($holding->negotiated_price)
                                a negotiated price.
                            @else
                                the {!! $holding->tranche->name !!}.
                            @endif
                            The payment plan is {!! $holding->paymentPlan->name !!}.
                        @endif
                        <div class="margin-top-20">
                            @if($forfeiture->status == 0)
                                <a class="btn btn-success"
                                   href="/dashboard/realestate/projects/units/forfeiture_notices/approve/{!! $holding->id !!}/">Approve
                                    Forfeiture Notice</a>

                                <button type="button" class="btn btn-default" data-toggle="modal"
                                        data-target="#cancelModal">
                                    Cancel Forfeiture Notice
                                </button>
                            @else
                                <div class="alert alert-info">
                                    <p>The forfeiture notice approval request has already been reviewed</p>
                                </div>
                            @endif
                        </div>

                    </div>
                </li>

            </ul>
        </div>
    </div>
    @if($holding)
        @include('realestate.projects.units.partials.cancel_forfeit_notice_modal')
    @endif
@endsection