@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h4>Project Type</h4>

            <div class="form-detail hover">
                <p class="bold">Apartment Blocks</p>
            </div>

            <hr/>
            {{--Unit Groups--}}
            <h4>Unit Groups</h4>

                <table class="table table-hover table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($project->unitGroups as $unitGroup)
                        <tr>
                            <td>{!! $unitGroup->name !!}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#editUnitGroup_{!! $unitGroup->id !!}"><i
                                            class="fa fa-edit"></i> </a>
                                <!-- Modal -->
                                <div class="modal fade" id="editUnitGroup_{!! $unitGroup->id !!}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['realestate.setup_unit_groups', $unitGroup->id]]) !!}
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Edit a Unit Group</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-detail">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            {!! Form::hidden('project_id', $project->id) !!}
                                                            {!! Form::hidden('id', $unitGroup->id) !!}
                                                            {!! Form::label('name', 'Name') !!}
                                                            {!! Form::text('name', $unitGroup->name, ['class'=>'form-control']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                                </button>
                                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- End of edit modal -->
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>


            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUnitGroup"><i class="fa fa-plus"></i> Add a unit group</button>

            <!-- Modal -->
            <div class="modal fade" id="addUnitGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {!! Form::open(['route'=>['realestate.setup_unit_groups', null]]) !!}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add a Unit Group</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-detail">
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! Form::hidden('project_id', $project->id) !!}
                                        {!! Form::hidden('id', null) !!}
                                        {!! Form::label('name', 'Name') !!}
                                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!! Form::submit('Add', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            {{--Number of Units & Sizes--}}
            <hr/>
            <h4>Number of Units & Sizes</h4>

            <table class="table table-hover table-responsive table-striped">
                <thead>
                    <tr><th>Type</th><th>Number</th><th></th></tr>
                </thead>
                <tbody>
                    @foreach($available_sizes as $available)
                        <tr>
                            <td>{!! $available->size->present()->getName !!}</td>
                            <td>{!! $available->number !!}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#myModal{!! $available->id !!}"><i class="fa fa-edit"></i> </a>
                                <!-- Modal -->
                                <div class="modal fade" id="myModal{!! $available->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['setup_unit_numbers', $project->id]]) !!}
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Edit Number of Units & Sizes</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-6">
                                                    {!! Form::hidden('id', $available->id) !!}
                                                    {!! Form::label('size_id', 'Unit size') !!}
                                                    {!! Form::select('size_id', $sizes, $available->size_id, ['class'=>'form-control']) !!}
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('number', 'Total number of units') !!}
                                                    {!! Form::text('number', $available->number, ['class'=>'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                {!! Form::submit('Add', ['class'=>'btn btn-success']) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- End of edit modal -->
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>


            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addSize"><i class="fa fa-plus"></i> Add a size</button>

            <!-- Modal -->
            <div class="modal fade" id="addSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {!! Form::open(['route'=>['setup_unit_numbers', $project->id]]) !!}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add the size and total number of units</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-detail">
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::label('size_id', 'Unit size') !!}

                                        {!! Form::select('size_id', $sizes, null, ['class'=>'form-control']) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('number', 'Total number of units') !!}

                                        {!! Form::text('number', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!! Form::submit('Add', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <hr/>

            <h4>Unit Tranches</h4>

            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Total Units</th>
                        <th>Remaining Units</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tranches as $tranche)
                        <tr>
                            <td>{!! $tranche->name !!}</td>
                            <td>{!! $tranche->sizes->sum('number') !!}</td>
                            <td>{!! $tranche->sizes->sum('number') - $tranche->holdings()->active()->count() !!}</td>
                            <td>{!! $tranche->date !!}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#trancheModalEdit{!! $tranche->id !!}"><i class="fa fa-edit"></i></a>
                                <a href="#" data-toggle="modal" data-target="#viewTrancheModal{!! $tranche->id !!}"><i class="fa fa-list-alt"></i></a>
                                @include('realestate.projects.partials.edit_tranche')
                                @include('realestate.projects.partials.view_tranche')
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#trancheModalAdd">Add a tranche</button>

            @include('realestate.projects.partials.tranche_setup')

            <hr>
            <h4>Commission rates</h4>

            @include('realestate.projects.partials.commission_rates')

            <hr>
            <h4>LOO Details</h4>
            <table class="table table-hover table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Long Name</td><td>{!! $project->long_name !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor</td><td>{!! $project->vendor !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor Address</td><td>{!! $project->vendor_address !!}</td>
                    </tr>
                    <tr>
                        <td>Completion Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($project->completion_date) !!}</td>
                    </tr>
                    <tr>
                        <td>County Government</td><td>{!! $project->county_government !!}</td>
                    </tr>
                    <tr>
                        <td>Development Description</td><td>{!! $project->development_description !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Name</td><td>{!! $project->bank_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Branch Name</td><td>{!! $project->bank_branch_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Name</td><td>{!! $project->bank_account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Number</td><td>{!! $project->bank_account_number !!}</td>
                    </tr>
                    <tr>
                        <td>Swift Code</td><td>{!! $project->bank_swift_code !!}</td>
                    </tr>
                    <tr>
                        <td>Clearing Code</td><td>{!! $project->bank_clearing_code !!}</td>
                    </tr>
                </tbody>
            </table>

            <a href="/dashboard/realestate/projects/setup-loo-details/{!! $project->id !!}" class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Edit LOO Details</a>

        </div>
    </div>
@endsection