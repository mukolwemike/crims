@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Create a project</h3>

            {!! Form::model($project, ['route'=>['create_realestate_project', $project->id]]) !!}

                <div class="form-group">
                    {!! Form::label('code', 'Code') !!}

                    {!! Form::text('code', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'code') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}

                    {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('reservation_fee', 'Reservation Fee') !!}

                    {!! Form::text('reservation_fee', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reservation_fee') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('type_id', 'Project type') !!}

                    {!! Form::select('type_id', $types, null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'type_id') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('custodial_account_id', 'Custodial Account') !!}

                    {!! Form::select('custodial_account_id', $accounts, null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('currency_id', 'Currency') !!}

                    {!! Form::select('currency_id', $currencies, null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'currency_id') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('commission_calculator', 'Commission calculator') !!}

                    {!! Form::select('commission_calculator', $commissionCalculators, null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_calculator') !!}
                </div>

            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection