@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <h4>Project Details</h4>

        <table class="table table-hover">
            <tbody>
                <tr><td>Project Name</td><td>{!! $project->name !!}</td></tr>
                <tr><td>Project Code</td><td>{!! $project->code !!}</td></tr>
            </tbody>
        </table>


        <a href="/dashboard/realestate/projects/{!! $project->id !!}/units/create" class="btn btn-success">Create Unit</a>
        <a href="/dashboard/realestate/projects/cancelled/{!! $project->id !!}" class="btn btn-primary">Cancelled Units</a>
        {{--<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#cashflows">Cash Flows</button>--}}

        {{--{!! Form::open(['route'=>['view_project_clients', $project->id], 'class'=>'pull-right margin-right-10']) !!}--}}
            {{--{!! Form::submit('Clients\' Report', ['class'=>'btn btn-success inline']) !!}--}}
        {{--{!! Form::close() !!}--}}

        <br/><br/>
        <div class="row">
            <div class="col-md-12">
                <div ng-controller="RealEstateUnitsController" ng-init="project_id = {!! $project->id !!}" class="detail-group">

                    <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                        <thead>
                        <tr>
                            <td colspan="5"><h3>All Units</h3></td><td colspan="2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></td>
                        </tr>
                        <tr>
                            <th>Unit Number</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Client Code</th>
                            <th>Client Name</th>
                            <th>Penalty Exemption</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td><% row.number %></td>
                            <td><% row.type_name %></td>
                            <td><% row.size_name %></td>
                            <td><% row.client_code %></td>
                            <td><% row.client_name %></td>
                            <td><% row.penalty_exempt | yesno %></td>
                            <td><span to-html = "row.status | unitStatus"></span></td>
                            <td>
                                <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/{!! $project->id !!}/units/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                                <a ng-controller = "PopoverCtrl" uib-popover = "Edit" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/{!! $project->id !!}/units/create/<% row.id %>"><i class = "fa fa-edit"></i></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                        <tr><td colspan="100%">Loading...</td></tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan = "4" class = "text-center">
                                Items per page <input type = "text" ng-model = "itemsByPage"/>
                            </td>
                            <td colspan = "4" class = "text-center">
                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Statement Modal -->
    <div class="modal fade" id="cashflows" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{!! $project->name !!} Cash Flows</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route'=>['view_project_cash_flows', $project->id]]) !!}
                        <div class="form-group">
                            <div class="col-md-4">{!! Form::label('startDate', 'Start Date') !!}</div>
                            <div ng-controller="DatepickerCtrl" class="col-md-8">{!! Form::text('startDate', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"startDate", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}</div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">{!! Form::label('endDate', 'End Date') !!}</div>
                            <div ng-controller="DatepickerCtrl" class="col-md-8">{!! Form::text('endDate', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"endDate", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Download Cash Flows', ['class'=>'btn btn-success']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
@endsection