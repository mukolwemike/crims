@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Edit LOO Details for a Project</h3>

            {!! Form::model($project, ['route'=>['update_loo_details', $project->id], 'method'=>'PUT']) !!}

            <div class="form-group">
                {!! Form::label('long_name', 'Long Name') !!}
                {!! Form::text('long_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'long_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('vendor', 'Vendor') !!}
                {!! Form::text('vendor', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'vendor') !!}
            </div>

            <div class="form-group" ng-controller="SummerNoteController">
                {!! Form::label('vendor_address', 'Vendor Address') !!}
                <summernote  config="options" ng-model="summernote" ng-init="summernote='{!! $project->vendor_address !!}'" on-paste="onPaste(evt)"></summernote>
                {!! Form::text('vendor_address', null, ['ng-model'=>'summernote', 'style'=>'display: none;']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'vendor_address') !!}
            </div>

            <div class="form-group"  ng-controller="DatepickerCtrl">
                {!! Form::label('completion_date', 'Completion Date') !!}
                {!! Form::text('completion_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"completion_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'completion_date') !!}
            </div>

            <div class="form-group">
                {!! Form::label('county_government', 'County Government') !!}
                {!! Form::text('county_government', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'county_government') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_name', 'Bank Name') !!}
                {!! Form::text('bank_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_branch_name', 'Bank Branch Name') !!}
                {!! Form::text('bank_branch_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_branch_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_account_name', 'Bank Account Name') !!}
                {!! Form::text('bank_account_name', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_account_name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_account_number', 'Bank Account Number') !!}
                {!! Form::text('bank_account_number', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_account_number') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_swift_code', 'Bank Swift Code') !!}
                {!! Form::text('bank_swift_code', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_swift_code') !!}
            </div>

            <div class="form-group">
                {!! Form::label('bank_clearing_code', 'Bank Clearing Code') !!}
                {!! Form::text('bank_clearing_code', null, ['class'=>'form-control']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'bank_clearing_code') !!}
            </div>

            <div class="form-group" ng-controller="SummerNoteController">
                {!! Form::label('development_description', 'Development Description') !!}
                <summernote  config="options" ng-model="summernote" ng-init="summernote='{!! $project->development_description !!}'" on-paste="onPaste(evt)"></summernote>
                {!! Form::text('development_description', null, ['ng-model'=>'summernote', 'style'=>'display: none;']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'development_description') !!}
            </div>

            <div class="clearfix"></div>

            <div class="form-group">
                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection