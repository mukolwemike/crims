@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <h4>Project Details</h4>

        <table class="table table-hover">
            <tbody>
                <tr><td>Project Name</td><td>{!! $project->name !!}</td></tr>
                <tr><td>Project Code</td><td>{!! $project->code !!}</td></tr>
            </tbody>
        </table>

        <a href="/dashboard/realestate/projects/show/{!! $project->id !!}" class="btn btn-success"><i class="fa fa-long-arrow-left"></i> All Units</a>

        <br/><br/>
        <div class="row">
            <div class="col-md-12">
                <div ng-controller="RealEstateCancelledUnitsController" ng-init="project_id = {!! $project->id !!}" class="detail-group">

                    <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                        <thead>
                        <tr>
                            <td colspan="9"><h3>Cancelled Units</h3></td><td colspan="2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></td>
                        </tr>
                        <tr>
                            <th>Unit Number</th>
                            <th>Client</th>
                            <th>Client Code</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Paid</th>
                            <th>Refunded</th>
                            <th>Gained</th>
                            <th>Reason</th>
                            <th>Unit Taken</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody ng-show="!isLoading">
                        <tr ng-repeat = "row in displayed">
                            <td><% row.number %></td>
                            <td><% row.client_name %></td>
                            <td><% row.client_code %></td>
                            <td><% row.type_name %></td>
                            <td><% row.size_name %></td>
                            <td><% row.paid | currency:"" %></td>
                            <td><% row.refunded | currency:"" %></td>
                            <td><% row.gains | currency:"" %></td>
                            <td><% row.forfeit_reason %></td>
                            <td><% row.taken | yesno %></td>
                            <td>
                                <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/{!! $project->id !!}/units/cancelled/<% row.id %>/<% row.client_id %>"><i class = "fa fa-list-alt"></i></a>
                                <a ng-controller = "PopoverCtrl" uib-popover = "Edit" popover-trigger = "mouseenter" href = "/dashboard/realestate/projects/{!! $project->id !!}/units/create/<% row.id %>"><i class = "fa fa-edit"></i></a>
                            </td>
                        </tr>
                        </tbody>
                        <tbody ng-show="isLoading">
                        <tr><td colspan="100%">Loading...</td></tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan = "1" class = "text-center">
                                Items per page <input type = "text" ng-model = "itemsByPage"/>
                            </td>
                            <td colspan = "4" class = "text-center">
                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection