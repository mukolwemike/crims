@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="SalesAgreementsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th colspan="2">
                        <select st-search="project" class="form-control">
                            <option value="">All Projects</option>
                            @foreach($projects as $project)
                                <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                            @endforeach
                        </select>
                    </th>
                    <th colspan="4"></th>
                    <th colspan="2"><input st-search="client_code" class="form-control" placeholder="Search client code..." type="text"/></th>
                    <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                </tr>
                <tr>
                    <th st-sort="client_code">Client Code</th>
                    <th>Client Name</th>
                    <th>Project</th>
                    <th>Vendor</th>
                    <th>Unit Number</th>
                    <th>Size</th>
                    <th>Signed On</th>
                    <th>Uploaded</th>
                    <th>Title</th>
                    <th></th>
                </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                <tr ng-repeat="row in displayed">
                    <td><% row.client_code %></td>
                    <td><% row.fullName %></td>
                    <td><% row.project %></td>
                    <td><% row.vendor %></td>
                    <td><% row.unit_number %></td>
                    <td><% row.size %></td>
                    <td><% row.signed_on | date %></td>
                    <td><% row.document_id > 0 | yesno  %></td>
                    <td class="text-center">
                        <i ng-show="row.status == true" class="fa fa-check-circle-o" style="color:green;"></i>
                        <i ng-show="row.status == false" style="color:red;">Not Sent</i>
                    </td>
                    <td><% row.title %></td>
                    <td>
                        <a ng-if="row.document_id" href="/dashboard/documents/<% row.document_id %>"><i class="fa fa-eye"></i></a>
                        <a class="pull-right" href="/dashboard/realestate/sales-agreements/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                    </td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="100%" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop