{!! Form::model($sales_agreement, ['route'=>['upload_sales_agreement', $sales_agreement->holding->id], 'files'=>true, 'method'=>'POST', 'style'=>'min-height: 350px;']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title') !!}

        {!! Form::text('title', null, ['class'=>'form-control', 'init-model'=>"title", 'ng-required'=>"true"]) !!}
        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title') !!}
    </div>

    @if(!$sales_agreement->date_signed)
        <div class="form-group">
            {!! Form::label('date_signed', 'Date signed') !!}

            <div ng-controller="DatepickerCtrl">{!! Form::text('date_signed', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date_sent_to_lawyer", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}</div>
            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date_signed') !!}
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('file', 'Attach the Sales Agreement') !!}

        {!! Form::file('file', ['class'=>'form-control']) !!}
        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Upload', ['class'=>'btn btn-success']) !!}
    </div>

{!! Form::close() !!}