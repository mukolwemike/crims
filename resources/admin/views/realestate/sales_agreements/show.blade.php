@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Real Estate Sales Agreement</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Project</td>
                        <td>{!! $project->name !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor</td>
                        <td>{!! $project->vendor !!}
                    </tr>
                    <tr>
                        <td>Unit Number</td>
                        <td>{!! $unit->number !!}</td>
                    </tr>
                    <tr>
                        <td>Size</td>
                        <td>{!! $unit->size->name !!}</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>{!! $unit->type->name !!}</td>
                    </tr>
                    @if($sales_agreement->document)
                        <tr>
                            <td>Title</td>
                            <td>{!! $sales_agreement->document->title !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Signed On</td>
                        <td> @if($sales_agreement->date_signed) {!! \Cytonn\Presenters\DatePresenter::formatDate($sales_agreement->date_signed) !!} @else Not Signed @endif </td>
                    </tr>
                    <tr>
                        <td>Uploaded</td>
                        <td> {!! \Cytonn\Presenters\BooleanPresenter::presentIcon((bool)$sales_agreement->document_id) !!}</td>
                    </tr>
                </tbody>
            </table>

            <h4>Sales Agreement</h4>
            <hr>

            @if($sales_agreement->document_id)
                <a href = "/dashboard/documents/{!! $sales_agreement->document_id !!}" class = "btn btn-primary"><i class = "fa fa-file-pdf-o"></i> Preview Sales Agreement</a>
            @endif

            <hr>
            <div class="col-md-6">
                <h4>Upload a sales agreement</h4>

                @if($previous_sales_agreements)
                    <p>The following previous sales agreements have been uploaded</p>
                    <ol>
                        @foreach($previous_sales_agreements as $d)
                            <li><a href="{!! route('show_document', [$d->id]) !!}">{!! $d->name !!}</a></li>
                        @endforeach
                    </ol>
                @endif

            </div>
            <div class="col-md-6">
                <button class="btn btn-danger" data-toggle="modal" data-target="#uploadSalesAgreementVersion"><i class="fa fa-envelope"></i> Upload Sales Agreement</button>
            </div>
        </div>
    </div>

    <!-- Upload Sales Agreement Version -->
    <div class="modal fade" id="uploadSalesAgreementVersion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    @include('realestate.sales_agreements.partials.upload_form')
                </div>
            </div>
        </div>
    </div>
@endsection