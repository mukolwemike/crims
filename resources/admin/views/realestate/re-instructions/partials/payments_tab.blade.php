<div ng-controller="RealEstateInstructionsCtrl">
    <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
        <thead>
        <tr>
            <th colspan="3">
            </th>
            <th colspan="3"></th>
            <th>
                <select st-search="used" class="form-control">
                    <option value="">All Applications</option>
                    <option value="1">Used</option>
                    <option selected value="0">Not Used</option>
                </select>
            </th>
            <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Project</th>
            <th>Holding</th>
            <th>Amount</th>
            <th st-sort="created_at">Created</th>
            <th st-sort-default="reverse" st-sort="updated_at">Updated</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody  ng-show="!isLoading">
        <tr ng-repeat="row in displayed">
            <td><% row.client  %></td>
            <td><% row.project %></td>
            <td><% row.unit %></td>
            <td><% row.amount %></td>
            <td><% row.created %></td>
            <td><% row.updated %></td>
            <td><span to-html = "row.status | clientApplicationStatus"></span></td>
            <td>
                <a href="/dashboard/realestate/client-instructions/payment/<%row.id%>"><i class="fa fa-list-alt"></i></a>
            </td>
        </tr>
        </tbody>
        <tbody ng-show="isLoading">
        <tr>
            <td colspan="10" class="text-center">Loading ... </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan = "2" class = "text-center">
                Items per page
            </td>
            <td colspan = "4" class = "text-center">
                <input type = "text" ng-model = "itemsByPage"/>
            </td>
            <td colspan = "4" class = "text-center">
                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
            </td>
            <td colspan="1"></td>
        </tr>
        </tfoot>
    </table>
</div>