@if($paymentSchedules)
    <table class="table table-responsive table-striped table-hover">
        <thead>
        <tr>
            <th>Remove</th>
            <th>Date</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Charge Penalty</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($paymentSchedules->sortBy('date') as $schedule)
            <tr>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                <td>{!! $schedule->type->name !!}</td>
                <td>{!! $schedule->description !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount)  !!}</td>
                <td>{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($schedule->penalty) !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif