<table class="table table-responsive table-striped">
    <tbody>
    <tr>
        <th>Payment Date</th>
        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
    </tr>

    <tr>
        <th>Amount</th>
        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
    </tr>

    {{--<tr>--}}
        {{--<th>Schedule</th>--}}
        {{--<td>{!! $schedule->description !!}</td>--}}
    {{--</tr>--}}

    <tr>
        <th>Narration</th>
        <td>{!! $payment->description !!}</td>
    </tr>

    <tr>
        <th>Mode of Payment</th>
        <td>{!! $payment->mode_of_payment !!}</td>
    </tr>

    @if(isset($payment->description))
        <tr>
            <th>Narration</th>
            <td>{!! $payment->description !!}</td>
        </tr>
    @endif

    <tr>
        <th>Proof of payment</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($instruction->document_id) !!}</td>
    </tr>

    @if($instruction->document_id)
        <tr>
            <th>Payment File</th>
            <th>
                <a target="_blank"
                   href="/dashboard/investments/client-instructions/filled-application-documents/{!! $instruction->document_id !!}">
                    <i class="fa fa-file-pdf-o"></i> View File
                </a>
            </th>
        </tr>
    @endif

    </tbody>
</table>