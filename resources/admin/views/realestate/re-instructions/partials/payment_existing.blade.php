<table class="table table-hover table-responsive table-striped">
    <thead>
    <tr>
        <th>Amount (KES)</th>
        <th>Date</th>
        <th>Narrative</th>
    </tr>
    </thead>
    <tbody>
    @foreach($paymentsExisting as $payment)
        <tr>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
            <td>{!! $payment->description !!}</td>
        </tr>
    @endforeach
    <tr style="background: yellow; color: red;">
        <th>Total</th>
        <th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($paymentsExisting->sum('amount')) !!}</th>
    </tr>
    <tr style="background: yellow; color: red;">
        <th>Remaining</th>
        <th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price() - $paymentsExisting->sum('amount')) !!}</th>
    </tr>
    </tbody>
</table>