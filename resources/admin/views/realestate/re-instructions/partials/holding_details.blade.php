<table class="table table-responsive table-striped">
    <tbody>
    <tr>
        <th>Project Name</th>
        <td>{!!$holding->project->name !!}</td>
    </tr>

    <tr>
        <th>Unit Name</th>
        <td>{!! $holding->unit->number !!}</td>
    </tr>

    <tr>
        <th>Unit Size</th>
        <td>{!! $holding->unit->size->name !!}</td>
    </tr>

    <tr>
        <th>Unit Price</th>
        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($unit_price) !!}</td>
    </tr>

    <tr>
        <th>FA</th>
        <td>{!! $holding->commission->recipient->name !!}</td>
    </tr>
    <tr>
        <th>Current FA Position</th>
        <td>{!! $holding->commission->recipient->type->name !!}</td>
    </tr>
    <tr>
        <th>Commission Rate Percentage</th>
        <td>{!! $holding->commission->commission_rate !!}%</td>
    </tr>
    </tbody>
</table>