@extends('layouts.default')

@section('content')
    <div class="col-md-12" ng-controller="">
        <div class="panel-dashboard">
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="detail-group">
                        <br>
                        <h5>HOLDING DETAILS</h5>
                        <hr>
                        @include('realestate.re-instructions.partials.holding_details')
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="detail-group">
                        <br>
                        <h5>PAYMENT HISTORY</h5>
                        <hr>
                        @include('realestate.re-instructions.partials.payment_existing')
                    </div>
                </div>

                @if($paymentSchedules)
                    <div class="col-md-12">
                        <div class="detail-group">
                            <br>
                            <h5>PAYMENT SCHEDULES</h5>
                            <hr>
                            @include('realestate.re-instructions.partials.payment_schedules')
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-md-6">
                <div>
                    <div class="pull-right">

                        <a href="/dashboard/realestate/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="detail-group">
                            <br>
                            <h5>PAYMENT DETAILS</h5>
                            <hr>
                            @include('realestate.re-instructions.partials.payment_details')
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="detail-group">
                            <br>
                            @if(is_null($instruction->approval_id))
                                <a href="/dashboard/realestate/client-instructions/payment/{!! $instruction->id !!}/process"
                                   class="btn btn-success">Process Payment</a>
                            @else
                                <div class="alert alert-warning"><p>This payment has already been processed</p></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop