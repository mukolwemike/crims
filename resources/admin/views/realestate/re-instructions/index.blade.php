@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">

        <ul id="myTabs" class="nav nav-tabs" role="tablist">
            <li role="presentation" class=""><a href="#payments" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Payments</a></li>
            {{--<li role="presentation" class=""><a href="#payments" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Unit Payments</a></li>--}}
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="payments" aria-labelledby="home-tab">
                @include('realestate.re-instructions.partials.payments_tab')
            </div>

            {{--<div role="tabpanel" class="tab-pane fade" id="payments" aria-labelledby="home-tab">--}}
                {{--@include('realestate.re-instructions.partials.payment_tab')--}}
            {{--</div>--}}
        </div>
    </div>
@endsection