@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <div style="display: flex;align-items: center;">

                <h3 style="flex: 1;">Real Estate Letter of Offer</h3>

                    <div style="padding-right: 50px;">

                        @if($prev_id)
                            <a href="/dashboard/realestate/loos/show/{!! $prev_id !!}"
                               style="margin-left:10px;" data-tooltip tabindex="1" title="PREVIOUS UNAPPROVED LOO"
                               class="previous"><span
                                        class="glyphicon glyphicon-chevron-left"></span>&nbsp;PREV&nbsp;</a>
                        @endif
                        @if($next_id)
                            <a href="/dashboard/realestate/loos/show/{!! $next_id !!}" data-tooltip tabindex="1"
                               title="NEXT UNAPPROVED LOO" class="next">NEXT&nbsp;<span
                                        class="glyphicon glyphicon-chevron-right"></span></a>
                        @endif

                    </div>

            </div>
            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Client Code</td>
                    <td>{!! $client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Client Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                </tr>
                <tr>
                    <td>Project</td>
                    <td>{!! $project->name !!}</td>
                </tr>
                <tr>
                    <td>Vendor</td>
                    <td>{!! $project->vendor !!}
                </tr>
                <tr>
                    <td>Unit Number</td>
                    <td>{!! $unit->number !!}</td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td>{!! $unit->size->name !!}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{!! $unit->type->name !!}</td>
                </tr>
                <tr>
                    <td>Advocate</td>
                    <td> @if($loo->advocate) {!! $loo->advocate->name !!} @else Not Set @endif </td>
                </tr>
                <tr>
                    <td>PM Approval</td>
                    <td> {!! \Cytonn\Presenters\BooleanPresenter::presentIcon((bool)$loo->pm_approved_on) !!} @if(! is_null($loo->pm_approval_id))
                            Approved
                            by {!! \Cytonn\Presenters\UserPresenter::presentFullNames($loo->pm_approval_id) !!} @endif</td>
                </tr>
                <tr>
                    <td>LOO Authorization Form</td>
                    <td>
                        @if((bool)$loo->authorization_sent_on)
                            <i class="fa fa-check-circle-o" style="color:green;"> </i> Sent
                        @else
                            <i class="fa fa-warning" style="color:orange;"></i> Not Sent
                        @endif
                    </td>
                <tr>
                    <td>LOO Status</td>
                    <td>
                        @if((bool)$loo->sent_on)
                            <i class="fa fa-check-circle-o" style="color:green;"> </i> Sent
                        @else
                            <i class="fa fa-warning" style="color:orange;"></i> Not Sent
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>LOO signed on</td>
                    @if($loo->date_signed)
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($loo->date_signed) !!}</td>
                    @else
                        <td>NOT SIGNED</td>
                    @endif
                </tr>
                @if(!$loo->pm_approved_on and $loo->rejects->count() > 0)
                    <tr>
                        <td>PM Rejection Status</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon(false) !!} Rejected</td>
                    </tr>
                    <tr>
                        <td>PM Rejection Reason</td>
                        <td>{!! $loo->rejects->first()->reason !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <h4>Letter of offer authorization</h4>
            <hr>

            @if((bool)$loo->authorization_sent_on)
                <a href="{!! route('preview_loo_authorization_form', $loo->id) !!}" class="btn btn-primary"><i
                            class="fa fa-file-pdf-o"></i> Preview Form</a>
                <button class="btn btn-danger" data-toggle="modal" data-target="#confirmLOOAuthorizationFormResend"><i
                            class="fa fa-envelope"></i> Resend Form
                </button>
            @else
                {!! Form::open(['route'=>['send_loo_authorization_form', $loo->id]]) !!}
                <a href="{!! route('preview_loo_authorization_form', $loo->id) !!}" class="btn btn-primary"><i
                            class="fa fa-file-pdf-o"></i> Preview Form</a>
                {!! Form::button('<i class="fa fa-envelope"></i> Send Form', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                {!! Form::close() !!}
            @endif


            @if($is_loo_sent)
            <hr>

            <h4>Contract Variation</h4>

                @if(!$contract_variation)
                    {{--<a href="{!! route('generate_contract_variation', $loo->id) !!}" class="btn btn-primary">--}}
                        {{--<i class="fa fa-file-pdf-o"></i> Generate--}}
                    {{--</a>--}}
                    <div class="btn-group">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#generateContractVariation">
                            <i class="fa fa-file-pdf-o"></i> Generate
                        </button>
                    </div>
                @else

                    @if($contract_variation->rejected())
                        <div class="alert alert-warning">
                            <p>This contract variation was rejected</p>
                            <p><b>Reason:</b> {{ $contract_variation->reason }}</p>
                        </div>

                    @else
                        <div class="">
                            <div class="btn-group">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#generateContractVariation">
                                    <i class="fa fa-file-pdf-o"></i> Re-generate
                                </button>

                                <a href="{!! route('preview_contract_variation', $contract_variation->id) !!}" class="btn btn-default">
                                    <i class="fa fa-file-pdf-o"></i> Preview
                                </a>
                            </div>

                            @if($contract_variation->approved())
                                <div class="btn-group">
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#confirmContractVariationSend">
                                        <i class="fa fa-envelope"></i> Send
                                    </button>
                                </div>
                            @else
                                <div class="btn-group">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#approveContractVariation">
                                        <i class="fa fa-thumbs-up"></i> Approve
                                    </button>

                                    <button class="btn btn-danger" data-toggle="modal" data-target="#rejectContractVariation">
                                        <i class="fa fa-thumbs-down"></i> Reject
                                    </button>
                                </div>
                            @endif
                        </div>
                    @endif
                @endif
            @else
            @endif

            <hr>

            <h4>Letter of offer</h4>
            <hr>

            @if(!$loo->pm_approved_on)
                {!! Form::open(['route'=>['pm_approve_loo', $loo->id]]) !!}
                {!! Form::button('<i class="fa fa-check-square-o"></i> Approve LOO', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#reject-loo"><i
                            class="fa fa-thumbs-o-down"></i> Reject LOO</a>
                {!! Form::close() !!}

            <!-- Reject LOO Modal -->
                <div class="modal fade" id="reject-loo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Reject Letter of Offer</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['route'=>['pm_reject_loo', $loo->id]]) !!}

                                <div class="form-group">
                                    {!! Form::label('reason', 'Reason for Rejection') !!}
                                    {!! Form::textarea('reason', null, ['class'=>'form-control', 'placeholder'=>'Give reason for rejecting LOO', 'rows'=>2, 'ng-required'=>"true"]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Reject', ['class'=>'btn btn-danger']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>

                        </div>
                    </div>
                </div>
            @else
                <p>The LOO was approved
                    by {!! \Cytonn\Presenters\UserPresenter::presentFullNamesNoTitle($loo->pm_approval_id) !!}
                    on {!! \Cytonn\Presenters\DatePresenter::formatDateTime($loo->pm_approved_on) !!}</p>
            @endif

            <hr>


            @if($loo->document_id)
                @if($is_loo_sent)

                    <a href="/dashboard/documents/{!! $loo->document_id !!}" class="btn btn-primary">
                        <i class="fa fa-file-pdf-o"></i> View Latest LOO
                    </a>

                    <button class="btn btn-danger" data-toggle="modal" data-target="#confirmLOOResend">
                        <i class="fa fa-envelope"></i> Resend LOO
                    </button>
                @else
                    {!! Form::open(['route'=>['send_loo_to_client', $loo->id]]) !!}
                    <a href="/dashboard/documents/{!! $loo->document_id !!}" class="btn btn-primary"><i
                                class="fa fa-file-pdf-o"></i> Preview LOO</a>

                    @if($loo->pm_approval_id)
                        {!! Form::button('<i class="fa fa-envelope"></i> Send LOO', array('class'=>'btn btn-success', 'type'=>'submit')) !!}
                    @endif
                    {!! Form::close() !!}
                @endif
            @else
                <a href="{!! route('generate_view_loo', $loo->holding->id) !!}" class="btn btn-primary">Generate and
                    preview LOO</a>
                <a href="{!! route('generate_upload_loo', $loo->holding->id) !!}" class="btn btn-success">Generate and
                    Upload LOO</a>
            @endif

            @if($loo->document_id)
                <div class="pull-right">
                    {!! Form::open(['route'=>['regenerate_loo_and_update', $loo->id], 'method'=>'PUT']) !!}
                    {!! Form::submit('Regenerate the Loo', ['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </div>
            @endif

            <hr>

            <h4>Upload a letter of offer</h4>

            @if($previous_loos)
                <p>The following previous loos have been uploaded</p>
                <ol>
                    <li><a href="/dashboard/documents/{!! $loo->document_id !!}"><i class="fa fa-file-pdf-o"></i> Latest
                            LOO</a></li>
                    @foreach($previous_loos as $d)
                        <li><a href="{!! route('show_document', [$d->id]) !!}">{!! $d->name !!}</a></li>
                    @endforeach
                </ol>
            @endif

            <br>
            <button class="btn btn-danger" data-toggle="modal" data-target="#uploadLooVersion"><i
                        class="fa fa-envelope"></i> Upload LOO
            </button>
        </div>
    </div>

    <!-- LOO Resend Modal -->
    <div class="modal fade" id="confirmLOOResend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Send LOO</h4>
                </div>
                <div class="modal-body">
                    <p>This Letter of Offer has already been sent to the client. Are you sure that you want to resend
                        it?</p>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>['send_loo_to_client', $loo->id]]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {!! Form::button('<i class="fa fa-envelope"></i> Resend LOO', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <!-- LOO Authorization Form Resend Modal -->
    <div class="modal fade" id="confirmLOOAuthorizationFormResend" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Send LOO Authorization Form</h4>
                </div>
                <div class="modal-body">
                    <p>This LOO Authorization Form has already been sent. Are you sure that you want to resend it?</p>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>['send_loo_authorization_form', $loo->id]]) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {!! Form::button('<i class="fa fa-envelope"></i> Resend Form', array('class'=>'btn btn-danger pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    <!-- Upload LOO Version -->
    <div class="modal fade" id="uploadLooVersion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::model($loo, ['route'=>['upload_loo', $loo->id], 'files'=>true]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Upload a Letter of Offer</h4>
                </div>
                <div class="modal-body">
                    @if(!$loo->sent_on)
                        <div class="form-group">
                            <div class="col-md-4">{!! Form::label('sent_on', 'Date LOO was sent to client') !!}</div>

                            <div ng-controller="DatepickerCtrl"
                                 class="col-md-8">{!! Form::text('sent_on', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date_sent_to_client", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}</div>
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'sent_on') !!}
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('date_signed', 'Date LOO was signed') !!}</div>

                        <div ng-controller="DatepickerCtrl"
                             class="col-md-8">{!! Form::text('date_signed', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"date_returned_by_client", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date_signed') !!}
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('advocate_id', 'Select Advocate') !!}</div>

                        <div class="col-md-8">{!! Form::select('advocate_id', $advocate, null, ['class'=>'form-control']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date_signed') !!}
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">{!! Form::label('file', 'Attach the LOO') !!}</div>

                        <div class="col-md-8">{!! Form::file('file', ['class'=>'form-control']) !!}</div>
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Upload', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    {{--Contract variation send--}}


    <div class="modal fade" id="generateContractVariation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['generate_contract_variation', $loo->id ]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Generate Contract Variation</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p class="alert alert-warning">There are no changes made in this holding, would wish to generate contract variation from a forfeited holding?</p>
                        {!! Form::label('reason', 'Select one of the forfeited holdings') !!}
                        {!! Form::select('prev_holding_id', $client_holdings, null, ['class'=>'form-control', 'placeholder'=>'Select one of the holdings', 'rows'=>2, 'ng-required'=>"true"]) !!}
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>

                    {!! Form::button('<i class="fa fa-envelope"></i> Generate', array('class'=>'btn btn-success pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="approveContractVariation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['approve_contract_variation', $contract_variation ? $contract_variation->id : null ]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Approve Contract Variation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to approve the contract variation?</p>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>

                    {!! Form::button('<i class="fa fa-envelope"></i> Approve', array('class'=>'btn btn-success pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="rejectContractVariation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['reject_contract_variation', $contract_variation ? $contract_variation->id : null ]]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Reject Contract Variation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to reject the contract variation?</p>
                    <div class="form-group">
                        {!! Form::label('reason', 'Reason for Rejection') !!}
                        {!! Form::textarea('reason', null, ['class'=>'form-control', 'placeholder'=>'Give reason for rejecting the contract variation', 'rows'=>2, 'ng-required'=>"true"]) !!}
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>

                    {!! Form::button('<i class="fa fa-envelope"></i> Reject', array('class'=>'btn btn-success pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmContractVariationSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Send Contract Variation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to send the contract variation?</p>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route'=>['send_contract_variation', $contract_variation ? $contract_variation->id : null]]) !!}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    {!! Form::button('<i class="fa fa-envelope"></i> Send Contract Variation', array('class'=>'btn btn-success pull-left', 'type'=>'submit')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection