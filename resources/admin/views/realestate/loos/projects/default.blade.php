@extends('realestate.loos.projects.parent')

@section('loo')

    <!-- RE -->
    <p style="text-align: center;"><strong><u>SUBJECT TO CONTRACT</u></strong></p>
    <div style="text-align: left; font-weight: 800; height: auto; padding-top:0;">
        <div style="display: inline-block; width: 25px; padding-top:0; margin-top:0; vertical-align: top;">
            RE:
        </div>
        <div style="display: inline-block;  margin-bottom: 0">
            LETTER OF OFFER FOR THE SALE OF A {!! strtoupper($unit->size->name.' '.$unit->type->name) !!}
            @if($unit->number) ({!! strtoupper($unit->number) !!}) @endif
            @if($unit->group) IN {{ strtoupper($unit->group->name) }} @endif
            @if($unit->size->realestate_land_size_id && $project->loo_indicate_land_size) ON A {{ strtoupper($unit->size->landSize->name) }} PARCEL OF LAND @endif
            IN THE PROPOSED {{ strtoupper($project->type->name) }} TO BE KNOWN AS {!! strtoupper($project->long_name) !!}
        </div>
        <hr style="margin-top: 0; font-weight: 500; border: 0; height: 1px; background: #333;">
    </div>

    <!-- Body -->
    <p>
        On behalf of our client, {!! $project->vendor !!}, we have the pleasure of offering you the above-captioned
        @if($unit->size->realestate_land_size_id) residential unit inclusive of the space on which it is built @else Unit @endif for purchase,
        subject to contract by way of a formal sale agreement to be prepared by the Vendor&rsquo;s Advocates (hereinafter prescribed) and based
        on the following terms and conditions:
    </p>

    <!-- Vendor -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Vendor : </h5>
        </div>
        <div class="right-sec left-align">
            <p>
                {!! $project->vendor_address !!}
            </p>
        </div>
    </div>

    <div id="restart"></div>

    <!-- Purchaser -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Purchaser : </h5>
        </div>
        <div class="right-sec left-align">
            {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!} <br>
            {!! \Cytonn\Presenters\ClientPresenter::presentAddressInline($client->id) !!}
            Tel: {!! $client->present()->getAnyPhone !!} <br>
            <a class="underline">{!! $client->contact->email !!}</a>
        </div>
    </div>

    <div id="restart"></div>

    <br><br>
    <!-- Vendor's Advocates -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Vendor's Advocates : </h5>
        </div>
        <div class="right-sec left-align">
            {!! $loo->advocate->address !!}
        </div>
    </div>
    <div id="restart"></div>

    <!-- Purchaser's Advocates -->
    @if(!empty(trim($purchasers_advocate)))
        <div class="container">
            <div class="left-sec">
                <h5 class="bold">Purchaser's Advocates : </h5>
            </div>
            <div class="right-sec left-align">
                {!! $purchasers_advocate !!}
            </div>
        </div>
        <div id="restart"></div>
    @endif

    <div style="page-break-before: always;"></div>

    <!-- Development -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Proposed Development : </h5>
        </div>
        <div class="right-sec">
            <p>
                {!! $project->development_description !!}
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Property -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Property : </h5>
        </div>
        <div class="right-sec">
            <p>
                A {!! $unit->size->name !!} {!! $unit->type->name !!} ({!! $unit->number !!}) to be situate in that residential Development to be known as ‘{!! $project->name !!}’.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    @if($project->property_additional_information)
        <!-- Property -->
        <div class="container">
            <div class="left-sec">
                <h5 class="bold">Phased Development : </h5>
            </div>
            <div class="right-sec">
                <p>
                    {!! $project->property_additional_information !!}
                </p>
            </div>
        </div>
        <div id="restart"></div>
    @endif

    <!-- Purchase Price -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Purchase Price : </h5>
        </div>
        <div class="right-sec">
            <p>
                The Property is offered for sale by way of Lease for the term of ninety-nine (99) years at the one off
                premium (the Purchase Price) of Kenya Shillings {!! $price_in_words !!} (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($unitHolding->price()) !!} /=)
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Terms of Payment -->
    @include('realestate.loos.projects.partials.terms_of_payment')

    <div id="restart"></div>

    <!-- Forfeiture -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Forfeiture : </h5>
        </div>
        <div class="right-sec">
            <p>
                In the event that the Purchaser defaults in the performance of the Purchaser's obligations herein after
                signing this Offer and payment of the Deposit, constituting this Letter of Offer a contract
                inter-parties, with no fault on the part of the Vendor, the Purchaser will forfeit an amount equivalent
                to ten percent (10%) of the Purchase Price to the Vendor and the Vendor shall refund any money that is
                over and above the forfeited amount upon re-sale of the unit, and receipt of the full purchase price
                from the New Purchaser.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Assignment -->

    @if($project->is_loo_assignment)
        <div class="container">
            <div class="left-sec">
                <h5 class="bold">Assignment : </h5>
            </div>
            <div class="right-sec">
                <p>
                    The Purchaser shall only  be entitled to assign or otherwise novate its rights, interests and/or
                    obligations accruing from this Offer, the Offer having been constituted as binding as hereinafter
                    provided, upon the Purchaser’s payment of 50% of the Purchase Price.  In the event the Purchaser,
                    having paid the 50% Purchase Price, opts to exercise its rights as afore-said, the same shall be
                    subject to the Vendor’s consent, first hand and obtained in writing.
                </p>
            </div>
        </div>
    @else
        <div class="container">
            <div class="left-sec">
                <h5 class="bold">No Assignment : </h5>
            </div>
            <div class="right-sec">
                <p>
                    The Purchaser shall not be entitled to assign or otherwise novate this Offer except upon receipt of the Vendor’s written consent
                    which shall be subject to the Purchaser having paid at least 50% of the Purchase Price and such other matters as the Vendor
                    may consider reasonable. <br><br>The Vendor shall not be responsible for any tax implications occasioned by such novation or assignment and the Purchaser shall
                    indemnify the Vendor against any losses or costs or expenses incurred by the Vendor by reason of any cost or tax implications
                    occasioned by virtue of this Clause.
                </p>
            </div>
        </div>
    @endif
    <div id="restart"></div>

    <!-- Governance -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Governance : </h5>
        </div>
        <div class="right-sec">
            <p>
                Each residential unit owner shall be entitled to one vote at the meetings of the Management Company.
                The votes allocated in respect of the non-residential parts will be proportional to the relative
                internal floor areas of the residential and non-residential parts of the building, excluding the common
                parts. This is calculated by taking the total votes allocated to the residential parts and multiplying
                that number by the formula A/B, where A is the total floor area of the non-residential parts, and B is
                the total area of the residential parts (the areas are to be calculated in square meters – fractions of
                less than half a square meter are ignored).
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Management -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Management : </h5>
        </div>
        <div class="right-sec">
            <p>
                The Purchaser acknowledges that {!! $project->name !!} is being developed as a homogeneous mixed
                use development where certain amenities will be used in common with other unit purchasers.
                The Purchaser acknowledges that an Association will therefore be formed for the proper and
                convenient management of {!! $project->name !!}. The Purchaser further acknowledges that in
                consideration of the fact the Purchaser is purchasing the Apartment not only for its physical
                amenities but also as (i) a real estate investment and (ii) for the lifestyle that the Estate shall
                offer the powers and functions of the Association may be delegated to the Vendor or its
                nominee in its capacity as the first manager in terms of the first management agreement
                which shall be attached to the Association’s Constitution for an initial term of twenty(20)
                years automatically renewable for two(2) further terms of fifteen(15) years each.
            </p>
            {{--<p>--}}
                {{--The Purchaser acknowledges that {!! $project->name !!} is being developed as a homogeneous mixed use development where certain amenities--}}
                {{--will be used in common with other unit purchasers in {!! $project->name !!}. The Purchaser acknowledges that an Association will therefore--}}
                {{--be formed for the proper and convenient management of {!! $project->name !!}, where the Purchaser shall be a member, and that since the--}}
                {{--Purchaser is purchasing the Apartment not only for its physical amenities but also as (i) a real estate investment and (ii) for the lifestyle--}}
                {{--that the Estate shall offer the Purchaser acknowledges that the powers and functions of the Association may be delegated to the Vendor or its--}}
                {{--nominee in its capacity as the first manager in terms of the first management agreement which shall be attached to the Association’s--}}
                {{--Constitution  for an initial term of twenty(20) years automatically renewable for two(2) further terms of  fifteen(15) years each.--}}
            {{--</p>--}}
        </div>
    </div>
    <div id="restart"></div>

    <!-- Apportioned Costs -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Apportioned Costs : </h5>
        </div>
        <div class="right-sec">
            <p>
                Upon Completion, the Purchaser shall in addition to the Purchase Price, pay such costs and charges as are incidental to the
                Purchase Price being inclusive of the cost of purchase of one (1) share in the Management Company, the Purchaser's apportioned share
                in the cost of the purchase of the Reversionary Interest in the Mother Title, the cost of transfer and the stamp duty
                payable thereof, the cost of incorporation of the Manager and a three (3) months Service Charge together with the equivalent
                thereto being Deposit on account of the Service Charge, as shall be particularly provided for in the Agreement for Sale to
                be executed by the Purchaser and Vendor, as hereinafter provided.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    {{--<!-- Termination by Purchaser -->--}}
    {{--<div class="container">--}}
        {{--<div class="left-sec">--}}
            {{--<h5 class="bold">Termination by Purchaser : </h5>--}}
        {{--</div>--}}
        {{--<div class="right-sec">--}}
            {{--<p>In the event the Purchaser after execution of this Letter of Offer and payment of the Deposit, is unable to fulfill--}}
                {{--the Purchaser's obligations as contained herein and therefore opts to withdraw from this transaction, with no fault--}}
                {{--on the part of the Vendor, the Purchaser will forfeit an amount equivalent to ten per cent (10%) of the Purchase Price--}}
                {{--being the agreed liquidated damages for breach of the Purchaser's obligations.  The Vendor shall thereafter refund any--}}
                {{--money that is over and above the forfeited amount less all costs, fees and expenses incurred by the Vendor in pursuance--}}
                {{--of this Letter of Offer within twent-yone (21) days of notification from the Purchaser or the Advocates of the Purchaser.--}}
            {{--</p>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div id="restart"></div>--}}

    {{--<div style="page-break-before: always;"></div>--}}

    <!-- Sale Agreement -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Sale Agreement : </h5>
        </div>
        <div class="right-sec">
            <p>The parties will enter into a formal Agreement for Sale and Lease which will incorporate the Law Society of Kenya Conditions
                of Sale (1989 Edition) in so far as the same are applicable and not inconsistent with this offer. The said Agreement and
                Lease shall be prepared by the Vendor’s Advocates in standard form in respect of all Units in the Development. Once the
                Agreement for Sale is executed, it shall supersede this Letter of Offer. Until then, the terms of this Offer are binding upon
                execution by both parties and the Vendor’s receipt of the Agreed Deposit. <br><br>The Agreement for Sale shall be executed by the Purchaser within fourteen (14) days of the Purchaser receiving
                the Agreement for Sale, from the Vendor’s Advocates
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Legal Costs -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Legal Costs : </h5>
        </div>
        <div class="right-sec">
            <p>The Purchaser shall be responsible to the Vendor’s Advocates for the legal fees on account of preparing
                the instrument of Lease herein together with the disbursements incidental to the registration of the
                instrument of Lease in the Purchaser’s favor, being inclusive but not limited to the stamp duty payable
                on the instrument of Lease and the registration fees payable at the Lands office for the purposes of
                the Lease. The breakdown in respect of the Legal Costs is as contained in the Appendix hereto.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Completion Date -->
    <div class="container"  style="page-break-inside: avoid;">
        <div class="left-sec">
            <h5 class="bold">Completion Date : </h5>
        </div>
        <div class="right-sec">
            <p>
                The Completion Date shall be twenty-one (21) days from the date of issue of the issuance of the
                residential unit’s Occupation Certificate by the {!! $project->county_government !!}. <br><br>
                Completion shall take place at the offices of the Vendor’s Advocates.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Service Charge Payment -->
    <div class="container"  style="page-break-inside: avoid;">
        <div class="left-sec">
            <h5 class="bold">Service Charge Payment: </h5>
        </div>
        <div class="right-sec">
            <p>The Service Charge Deposit and Provisional Service Charge (as defined in the Appendix) shall be due for
                payment on or before Completion date. Any late payments shall attract interest at 4% above the base
                rate from time to time of the Central Bank of Kenya, or, if that base rate is no longer published, a
                comparable rate reasonably determined by the Vendor.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Possession -->
    <div class="container"  style="page-break-inside: avoid;">
        <div class="left-sec">
            <h5 class="bold">Possession : </h5>
        </div>
        <div class="right-sec">
            <p>The Purchaser shall be entitled to vacant possession of the Property on the Completion Date but subject to full
                payment of the Purchase Price and all other costs and charges herein.
            </p>
        </div>
    </div>
    <div id="restart"></div>


    <!-- Confidentially -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Confidentiality : </h5>
        </div>
        <div class="right-sec">
            <p>The terms of this transaction will remain confidential unless both parties agree to non-confidentiality in writing.</p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Validity -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Validity : </h5>
        </div>
        <div class="right-sec">
            <p>This Letter of Offer will be valid for thirty (30) working days from the date hereof, after which the Offer will be
                deemed to have lapsed and automatically become null and void (except where 30 days are yet to lapse from the Purchaser's payment
                of the Reservation Deposit) and the Vendor shall be at liberty to offer the Unit to a third party.</p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Confirmation -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Confirmation : </h5>
        </div>
        <div class="right-sec">
            <p>Kindly signify your acceptance by signing and returning one (1) copy of this Letter of Offer, together with a deposit slip
                confirming payment of the Deposit, within the stipulated timelines.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <p>The Purchaser hereby accepts and acknowledges that this Letter of Offer shall immediately upon execution by both parties
        and the Purchaser's payment of the Deposit constitute a binding Contract between the parties pending the execution of the
        Agreement for Sale by both parties, upon which the Agreement for Sale shall supersede the Letter of Offer.
    </p>
    <p>&nbsp;</p>

    <p>Yours faithfully,</p>
    <p>
        <strong>For: {!! $unitHolding->project->fundManager->re_fullname !!}</strong><br/>

        <strong>{!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($pm_id) !!}</strong>
    </p>
    <hr/>
    <div style="page-break-inside: avoid;">
        <p>Acceptance by the Purchaser</p>
        <p>I/We, the undersigned, hereby confirm acceptance of the above terms and conditions.</p>
        <p>Purchaser</p>
        <div class="container">
            <div class="left-sec">Date </div><div class="right-sec">: </div>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Signature </div><div class="right-sec">: </div>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Witness Date </div><div class="right-sec">: </div><br>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Name of Witness </div><div class="right-sec">: </div><br>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Signature </div><div class="right-sec">: </div><br>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">c.c. </div><div class="right-sec">: {!! $unitHolding->loo->advocate->name !!}</div>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">c.c. </div><div class="right-sec">: {!! $fa->name !!} (<a href="mailto:{!! $fa->email !!}">{!! $fa->email !!}</a>)</div>
        </div>
        <div id="restart"></div>
    </div>

    <div style="page-break-before: always;"></div>

    @include('realestate.loos.projects.partials.sales_agreement_extract')
@stop