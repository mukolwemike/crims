@extends('realestate.loos.projects.parent')

@section('loo')

    <!-- RE -->
    <p style="text-align: center;"><strong><u>SUBJECT TO CONTRACT</u></strong></p>
    <div style="text-align: left; font-weight: 800; height: auto; padding-top:0;">
        <div style="display: inline-block; width: 25px; padding-top:0; margin-top:0; vertical-align: top;">
            RE:
        </div>
        <div style="display: inline-block;  margin-bottom: 0">
            LETTER OF OFFER FOR THE SALE OF A PARCEL OF LAND MEASURING {{ strtoupper($unit->size->name )}}  IN THE PROPOSED MASTER PLANNED DEVELOPMENT TO BE KNOWN AS ‘NEWTOWN’
        </div>
        <hr style="margin-top: 0; font-weight: 500; border: 0; height: 1px; background: #333;">
    </div>

    <!-- Body -->
    <p>
        We, on behalf of our clients , John Kioko Mutua and Serah Nzembi Nzyoka, the nominees and trustees of the registered proprietors of the parcels of land known as Land Reference Numbers 13208/2, 28055 and 28056 containing by measurement an aggregate of 383.03 hectares located in Athi River in Machakos County (hereinafter together "the Land")have the pleasure of offering to you for purchase, the above-captioned Parcel of Land in Newtown an integrated mixed use commercial, residential, leisure and retail master planned community, subject to contract by way of a formal sale agreement to be prepared by the Vendor’s Advocates (hereinafter prescribed) and based on the following terms and conditions:

    </p>

    <!-- Vendor -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Vendor : </h5>
        </div>
        <div class="right-sec left-align">
            <p>
                {!! $project->vendor_address !!}
            </p>
        </div>
    </div>

    <div id="restart"></div>

    <!-- Purchaser -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Purchaser : </h5>
        </div>
        <div class="right-sec left-align">
            {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!} <br>
            {!! \Cytonn\Presenters\ClientPresenter::presentAddressInline($client->id) !!}
            Tel: {!! $client->present()->getAnyPhone !!} <br>
            <a class="underline">{!! $client->contact->email !!}</a>
        </div>
    </div>

    <div id="restart"></div>

    <br><br>
    <!-- Vendor's Advocates -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Vendor's Advocates : </h5>
        </div>
        <div class="right-sec left-align">
            {!! $loo->advocate->address !!}
        </div>
    </div>
    <div id="restart"></div>

    <!-- Purchaser's Advocates -->
    @if(!empty(trim($purchasers_advocate)))
        <div class="container">
            <div class="left-sec">
                <h5 class="bold">Purchaser's Advocates : </h5>
            </div>
            <div class="right-sec left-align">
                {!! $purchasers_advocate !!}
            </div>
        </div>
        <div id="restart"></div>
    @endif

    <div style="page-break-before: always;"></div>

    <!-- Development -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Proposed Development : </h5>
        </div>
        <div class="right-sec">
            <p>
                {!! $project->development_description !!}
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Property -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Property : </h5>
        </div>
        <div class="right-sec">
            <p>
                That single piece of parcel of land measuring {{ $unit->size->name}} being Land that shall be registered as a Single Title and shall not be divided into Units, as identified in the Newtown Master Plan, being the overall site plan for Newtown to be registered with the relevant statutory bodies, a preliminary copy of which is attached hereto.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    @if($project->property_additional_information)
        <!-- Property -->
        <div class="container">
            <div class="left-sec">
                <h5 class="bold">Phased Development : </h5>
            </div>
            <div class="right-sec">
                <p>
                    {!! $project->property_additional_information !!}
                </p>
            </div>
        </div>
        <div id="restart"></div>
    @endif

    <!-- Purchase Price -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Purchase Price : </h5>
        </div>
        <div class="right-sec">
            <p>
                The Property is offered for sale by way of Lease for the remainder of the  term, as granted by the leasehold Titles over the Property less the last seven(7) days thereof at the one off premium (the Purchase Price) of Kenya Shillings {!! $price_in_words !!} (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($unitHolding->price()) !!} /=)
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Terms of Payment -->
    @include('realestate.loos.projects.partials.terms_of_payment')
    
    <div id="restart"></div>

    <!-- Interest -->

    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Interest: </h5>
        </div>
        <div class="right-sec">
            <p>
                Any late payment shall attract interest at the rate of eighteen (18%) per annum to be calculated on daily balances and shall accrue from the due date until payment in full. The Vendor shall not be obliged to sign a Sub-Lease in favour of the Purchaser prior to payment of such interest and any outstanding balance of the Purchase Price.
            </p>
        </div>
    </div>

    <div id="restart"></div>

    <!-- Forfeiture -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Forfeiture: </h5>
        </div>
        <div class="right-sec">
            <p>
                In the event that the Purchaser defaults in the performance of the Purchaser's obligations herein after
                signing this Offer and payment of the Deposit, constituting this Letter of Offer a contract
                inter-parties, with no fault on the part of the Vendor, the Purchaser will forfeit an amount equivalent
                to ten percent (10%) of the Purchase Price to the Vendor and the Vendor shall refund any money that is
                over and above the forfeited amount upon re-sale of the unit, and receipt of the full purchase price
                from the New Purchaser.
            </p>
        </div>
    </div>

    <div class="restart"></div>

    <!-- Security Deposit -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Security Deposit:</h5>
        </div>
        <div class="right-sec">
            <p>
                On or before the Completion Date the Purchaser shall lodge with the Master Developer a Security Deposit in an amount as determined by the Master Developer from time to time as security for the Purchaser’s obligations to pay Community Fees and other Fees.

                The Security Deposit shall be held by the Master Developer as a continuing covering Security and the Master Developer may apply the Security Deposit in whole or in part towards the Purchaser’s payment obligations.

                The Master Developer may invest or deposit the Security Deposit in an income or interest bearing account and shall be entitled to retain any interest or other income earned from the Security Deposit.
            </p>
        </div>
    </div>

    <div id="restart"></div>

    <!-- Management -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Management : </h5>
        </div>
        <div class="right-sec">
            <p>
                The Purchaser acknowledges that the Property is part and parcel of a master planned community and therefore that the maintenance,
                preservation, enhancement and protection of the property values and assets of Newtown is essential. The Purchaser therefore agrees
                that the Vendor shall nominate a  Managing Agent to manage the Property on behalf of the Purchaser and the Precinct Property Owners
                Association, to which the Purchaser shall belong,  and such Service Charge shall be payable to the appointed Managing Agent.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Apportioned Costs -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Apportioned Costs : </h5>
        </div>
        <div class="right-sec">
            <p>
                Upon Completion, the Purchaser shall in addition to the Purchase Price, pay such costs and charges as are incidental to the
                Purchase Price being inclusive of the cost of incorporation of the Property Owners Association, issuance of the share certificate
                and a three (3) months Service Charge together with the equivalent thereto being Deposit on account of the Service Charge, as shall be
                particularly provided for in the Agreement for Sale to be executed by the Purchaser and Vendor, as hereinafter provided.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    {{--<div style="page-break-before: always;"></div>--}}

    <!-- Sale Agreement -->
    <br/>
    <br/>
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Sale Agreement : </h5>
        </div>
        <div class="right-sec">
            <p>
                The parties will enter into a formal Agreement for Sale and Lease which will incorporate the Law Society of Kenya Conditions
                of Sale (1989) in so far as the same are applicable and not inconsistent with this offer. The said Agreement and
                Lease shall be prepared by the Vendor’s Advocates in standard form in respect of all Units in the Development.
            </p>
            <p>
                The Purchaser shall execute the Agreement for Sale within fourteen (14) days of the Purchaser receiving
                the Agreement for Sale, from the Vendor’s Advocates

            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Legal Costs -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Legal Costs : </h5>
        </div>
        <div class="right-sec">
            <p>The Purchaser shall be responsible to the Vendor’s Advocates for the legal fees on account of preparing the instrument
                of Lease herein together with the Disbursements incidental to the registration of the instrument of Lease in favour of
                the Purchaser being inclusive but not limited to the Stamp duty payable on the instrument of Lease and two (2)
                counterparts thereof together with the registration fees payable at the Lands office for the purposes of the Lease.
                The breakdown in respect of the Legal Costs shall be contained in the Agreement for Sale as well the mode of payment.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Completion Date -->
    <div class="container"  style="page-break-inside: avoid;">
        <div class="left-sec">
            <h5 class="bold">Completion Date : </h5>
        </div>
        <div class="right-sec">
            <p>
                {{--The Completion Date shall be fourteen (14) days from the date of issue of the Architect’s Certificate of Practical--}}
                {{--Completion or the issuance of the Unit’s Occupation Certificate by the {!! $project->county_government !!},--}}
                {{--whichever is later.--}}
                The Completion Date shall be the date seven (7) days from the date the Purchaser clears the Purchase Price.
            </p>
            <p>Completion shall take place at the offices of the Vendor’s Advocates.</p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Possession -->
    <div class="container"  style="page-break-inside: avoid;">
        <div class="left-sec">
            <h5 class="bold">Possession : </h5>
        </div>
        <div class="right-sec">
            <p>The Purchaser shall be entitled to vacant possession of the Property on the Completion Date but subject
                to full payment of the Purchase Price and all other costs and charges herein.
            </p>
        </div>
    </div>
    <div id="restart"></div>


    <!-- Confidentially -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Confidentiality : </h5>
        </div>
        <div class="right-sec">
            <p>The terms of this transaction will remain confidential unless both parties agree to non-confidentiality in writing.</p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Validity -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Validity : </h5>
        </div>
        <div class="right-sec">
            <p>This Letter of Offer will be valid for a period of Fourteen (14) days after the date hereof, provided the
                same shall be automatically extended where  the Reservation Period, being the thirty (30)-day period from
                the date of the Purchaser's payment of the reservation deposit, shall not have lapsed,
                after which it shall become null and void  and the Vendor shall be at liberty to offer the Property to
                another party of choice.</p>
        </div>
    </div>
    <div id="restart"></div>

    <!-- Confirmation -->
    <div class="container">
        <div class="left-sec">
            <h5 class="bold">Confirmation : </h5>
        </div>
        <div class="right-sec">
            <p>Kindly signify your acceptance by signing and returning one (1) copy of this Letter of Offer, together
                with the said proof of payment of the Deposit mentioned above, within the stipulated timelines.
            </p>
        </div>
    </div>
    <div id="restart"></div>

    <p>The Purchaser hereby accepts and acknowledges that this Letter of Offer shall immediately upon execution by both parties
        and the Purchaser's payment of the Deposit constitute a binding Contract between the parties pending the execution of the
        Agreement for Sale by both parties, upon which the Agreement for Sale shall supersede the Letter of Offer.
    </p>
    <p>&nbsp;</p>

    <p>Yours faithfully,</p>
    <p>
        <strong>For: {!! $unitHolding->project->fundManager->re_fullname !!}</strong><br/>

        <strong>{!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($pm_id) !!}</strong>
    </p>
    <hr/>
    <div style="page-break-inside: avoid;">
        <p>Acceptance by the Purchaser</p>
        <p>I/We, the undersigned, hereby confirm acceptance of the above terms and conditions.</p>
        <p>Purchaser</p>
        <div class="container">
            <div class="left-sec">Date </div><div class="right-sec">: </div>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Signature </div><div class="right-sec">: </div>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Witness Date </div><div class="right-sec">: </div><br>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Name of Witness </div><div class="right-sec">: </div><br>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">Signature </div><div class="right-sec">: </div><br>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">c.c. </div><div class="right-sec">: {!! $unitHolding->loo->advocate->name !!}</div>
        </div>
        <div id="restart"></div>

        <div class="container">
            <div class="left-sec">c.c. </div><div class="right-sec">: {!! $fa->name !!} (<a href="mailto:{!! $fa->email !!}">{!! $fa->email !!}</a>)</div>
        </div>
        <div id="restart"></div>
    </div>
@stop