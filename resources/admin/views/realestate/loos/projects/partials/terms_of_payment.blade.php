<div class="container">
    <div class="left-sec">
        <h5 class="bold">Terms of Payment : </h5>
    </div>
    <div class="right-sec">
        <p>The Purchaser shall pay the Purchase Price as follows:</p>
        <ol type="1">
            @if($reservation_fee > 0)
                <li><span>The Purchaser has paid the sum of Kenya Shillings {!! ucwords(\Cytonn\Core\DataStructures\Number::toWords($reservation_fee)) !!}
                        (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($reservation_fee) !!}/=) on account of the reservation deposit,
                    receipt whereof the Vendor acknowledges by causing the execution of this Letter of Offer</span>
                </li>
            @endif
            @if($holding->paymentPlan->slug == 'installment')
                @if($net_deposit > 0)
                    <li>
                            <span>The sum of Kenya Shillings {!! $deposit_in_words !!} (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($net_deposit) !!}/=)
                            shall be paid upon the Purchaser’s execution of this Letter of Offer and together with the Reservation deposit shall constitute the agreed Deposit in respect of the Purchase Price;</span>
                    </li>
                @endif
                <li>
                    <span>The balance of the Purchase Price in the sum of Kenya Shillings {!! $balance_after_deposit_in_words !!}
                        (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($balance_after_deposit, false, 0) !!}/=) shall be
                    paid in installments in the following manner that is to say:</span>
                    <ol type="a" class="inner-ol">
                        <?php $key = 0 ?>
                        @foreach($installments as  $schedule)
                            <?php
                            $amount = $schedule->amount;

                            if (($overDeposit > 0) && ($amount >= $overDeposit)) {
                                $amount = $amount - $overDeposit;
                                $overDeposit = 0;
                            } elseif ($overDeposit > 0) {
                                $overDeposit = $overDeposit - $amount;
                                $amount = 0;
                            }
                            ?>
                            @if($amount > 0)
                                <?php  ++$key; ?>
                                <li class="inner">
                                    <span>The
                                        @if($key == count($installments))
                                            Final
                                        @else
                                            {!! ucwords($ordinaliseWords($key)) !!}
                                        @endif
                                        Installment of Kenya Shillings {!! $toWords($amount) !!}
                                        (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($amount) !!}) shall be payable on or
                                        before
                                        @if(($key == count($installments)))
                                            Completion Date;
                                        @else
                                            {!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}.
                                        @endif
                                    </span>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                    <span>The said installments shall be payable by the fifth day following the due date thereof (due date shall be
                    the dates indicated at this Clause 3 hereof) failing which any outstanding amounts shall attract interest at the rate of
                    eighteen percent per annum (18% p.a.) to be calculated on daily balances from the due date until payment in full both days inclusive.</span>
                </li>
            @elseif($holding->paymentPlan->slug == 'cash')
                @if($net_deposit > 0)
                    <li>
                        <span>The sum of Kenya Shillings {!! $deposit_in_words !!} (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($net_deposit) !!}/=)
                        shall be paid upon the Purchaser’s execution of this Letter of Offer and together with the Reservation deposit shall constitute the agreed Deposit in respect of the Purchase Price;
                        </span>
                    </li>
                @endif
                @if($balance_after_deposit)
                    <li>
                        <span>The balance of the Purchase Price in the sum of Kenya Shillings {!! $balance_after_deposit_in_words !!}
                            (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($balance_after_deposit, false, 0) !!}/=) shall be paid
                        within {!! Number::toWords($cash_payment_days) !!} ({!! $cash_payment_days !!}) days of the Vendor’s receipt of the Deposit</span>
                    </li>
                @endif
            @elseif($holding->paymentPlan->slug == 'mortgage')
                @if($net_deposit > 0)
                    <li>
                    <span>The sum of Kenya Shillings {!! $deposit_in_words !!} (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($net_deposit) !!}/=)
                    shall be paid upon the Purchaser’s execution of this Letter of Offer and together with the Reservation deposit shall constitute the agreed Deposit in respect of the Purchase Price;</span>
                    </li>
                @endif
                    @if($project->name == 'Applewood')
                        <li>
                            <span>The balance of the Purchase Price shall be financed and shall be secured an appropriate letter of professional undertaking
                                from the Purchaser’s Financier to be issued in a form acceptable to the Vendor’s Advocates and shall be payable within
                                fourteen (14) days of the registration of the Lease over the Property in the Purchaser’s favour and a Charge in favour
                                of the Purchaser’s financiers.</span>
                        </li>
                        <li>
                            <span>Any delay by the Purchaser in paying the outstanding amounts as agreed in this Clause 8 shall attract interest at the
                                rate of eighteen percent per annum (18% p.a.) to be calculated on daily balances from the due date until payment in
                                full both days inclusive.</span>
                        </li>
                        <li>
                            <span>The Purchaser is aware and agrees that the Purchase Price or any part thereof shall not be held on stakeholder terms
                                and shall be utilized by the Vendor in the development of the Property.</span>
                        </li>
                    @else
                        <li>
                            <span>The Purchaser shall within fourteen (14) days of the Purchaser’s or the Purchaser’s Advocates’ receipt of a notification
                            from the Vendor’s Advocates that the Vendor is in receipt of the Architect’s Certificate of Practical Completion over the
                            Property secure a professional Undertaking from a reputable Financier, in the form as shall be prescribed by the Vendor’s Advocates,
                            to the pay the balance of the Purchase Price in the sum of Kenya Shillings {!! $balance_after_deposit_in_words !!}
                                (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($balance_after_deposit) !!}) upon successful registration of the instrument of sub-lease in the Purchaser’s favour and
                            the instrument of Charge in the Financier’s favour</span>
                        </li>
                    @endif
            @elseif($holding->paymentPlan->slug == 'zero_deposit_mortgage' || $holding->paymentPlan->slug == 'zero_deposit_installment')
                <li>
                    <span>The sum of Kenya Shillings {!! $deposit_in_words !!} (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($net_deposit) !!}/=)
                    shall be paid upon the Purchaser’s execution of this Letter of Offer and together with the Reservation Deposit shall constitute the agreed
                        Deposit in respect of the Purchase Price and therefore the agreed Deposit
                    shall be paid in six installments in the following manner;</span>
                    <ol type="a" class="inner-ol">
                        <?php $key = 0; ?>
                        @foreach($deposits as $deposit_installment)
                            <?php
                            $amt = $deposit_installment->amount - $paid;
                            $paid = $paid - $deposit_installment->amount;

                            if ($paid < 0) {
                                $paid = 0;
                            }
                            ?>
                            @if($amt > 0)
                                <li class="inner">
                                        <span>The {!! ucwords($ordinaliseWords(++$key)) !!} Installment of Kenya Shillings

                                            {!! ucwords(\Cytonn\Core\DataStructures\Number::toWords($amt)) !!}
                                            (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($amt) !!})

                                        shall be payable on or before {!! \Cytonn\Presenters\DatePresenter::formatDate($deposit_installment->date) !!};</span>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                </li>
                @if($holding->paymentPlan->slug == 'zero_deposit_mortgage')
                    <li>
                        <span>The Purchaser shall within fourteen (14) days of the Purchaser’s or the Purchaser’s Advocates’ receipt of a notification
                        from the Vendor’s Advocates that the Vendor is in receipt of the Architect’s Certificate of Practical Completion over the
                        Property secure a professional Undertaking from a reputable Financier, in the form as shall be prescribed by the Vendor’s Advocates,
                        to the pay the balance of the Purchase Price in the sum of Kenya Shillings {!! $balance_after_deposit_in_words !!}
                            (KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($balance_after_deposit) !!}) upon successful registration of the instrument of
                        sub-lease in the Purchaser’s favour and the instrument of Charge in the Financier’s favour</span>
                    </li>
                @else
                    <li>
                        <span>The balance of the Purchase Price in the sum of Kenya Shillings {!! $balance_after_deposit_in_words !!}
                            ({!! \Cytonn\Presenters\AmountPresenter::currency($balance_after_deposit, false, 0) !!}/=) shall be paid in installments
                        in the following manner that is to say:</span>
                        <ol type="a" class="inner-ol">
                            <?php $key = 0 ?>
                            @foreach($installments as  $schedule)
                                <li class="inner"><span>
                                        <?php  ++$key; ?>
                                        The @if($key == count($installments)) Final @else {!! ucwords($ordinaliseWords($key)) !!} @endif Installment of {!! $schedule->amount_in_words !!}
                                        KShs. {!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!} shall be payable on or
                                        before Completion Date
                                        @if($project->practical_completion && ($key == count($installments)))
                                                ; but subject to the issuance of the Unit's Certificate of Practical Completion by the Architect
                                        @else
                                            .
                                        @endif
                                    </span></li>
                            @endforeach
                        </ol>
                        <span>The said installments shall be payable by the fifth day following the due date thereof (due date shall be
                        the dates indicated at Clause 3 hereof) failing which any outstanding amounts shall attract interest at the rate of
                        eighteen percent per annum (18% p.a.) to be calculated on daily balances from the due date until payment in full both days inclusive.</span>
                    </li>
                @endif
            @else
                <?php $plan = $holding->paymentPlan->slug;
                throw new InvalidArgumentException("Payment plan $plan is not support"); ?>
            @endif
            <li>
                    <span>All payments shall be made in cleared funds, without deduction, set-off or counterclaim and to the Vendor’s
                    Bank account, whose details are as follows:</span>
                <br/><br/>

                <div class="bold" style="page-break-inside: avoid;">
                    <span class="">Account Name : </span><span>{!! (strlen($project->bank_account_name) > 2) ? $project->bank_account_name: $project->vendor !!}</span><br/>
                    <span class="">Account No : </span><span class="">{!! $project->bank_account_number !!}</span><br/>
                    <span class="">Bank : </span><span class="">{!! $project->bank_name !!}</span><br/>
                    <span class="">Branch : </span><span class="">{!! $project->bank_branch_name !!}</span><br/>
                    <span class="">Swift Code : </span><span class="">{!! $project->bank_swift_code !!}</span><br/>
                </div>
            </li>
        </ol>
    </div>
</div>
