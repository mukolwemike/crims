<p><strong><u>SALE AGREEMENT EXTRACT</u></strong></p>
<p><strong>SECOND SCHEDULE</strong></p>
<p><strong>Legal Fees and Associated Costs</strong></p>
<p>The legal fees payable by the Purchaser shall be broken down as hereunder:</p>
<table class="table table-bordered" width="100%">
    <tr>
        <th>ITEM</th>
        <th>AMOUNT (KSHS)</th>
        <th>PAYABLE TO</th>
    </tr>
    <tr>
        <td>Legal Fees 1% of purchase price</td>
        <td>Purchase price (X)</td>
        <td></td>
    </tr>
    <tr>
        <td>
            V.A.T. (at the rate of 16% or such other rate
            as shall be applicable as at the time of payment
            of this amount)
        </td>
        <td>16% of X</td>
        <td></td>
    </tr>
    <tr>
        <td>Disbursements</td>
        <td>5,000/-</td>
        <td></td>
    </tr>
    <tr>
        <th>TOTAL</th>
        <th>Sum of above</th>
        <th>Vendor's advocate</th>
    </tr>
</table>
<br>
<p>The associated costs payable by the Purchaser shall be as follows:</p>
<table class="table table-bordered" width="100%">
    <tr>
        <th>ITEM</th>
        <th>AMOUNT (KSHS)</th>
        <th>PAYABLE TO</th>
    </tr>
    <tr>
        <td>Stamp Duty (4% of the Purchase Price subject to the provisions of
            the law and assessment of the Premises by the Government Valuer)
        </td>
        <td>4% of X</td>
        <td>Vendor’s advocates</td>
    </tr>
    <tr>
        <td>
            Proportionate Cost of Incorporation of the Management Company and Purchase of Share in the same
        </td>
        <td>2,000/-</td>
        <td>Vendor</td>
    </tr>
    <tr>
        <td>Proportionate Cost for the Transfer of the Reversion from the Vendor to the Management Company</td>
        <td>13,000/-</td>
        <td>Vendor</td>
    </tr>


    @if($project->id == 2)
        {{--Alma--}}
        <tr>
            <td>Service Charge: <br>
                1 Bedroom - 3,000/= <br>
                2 Bedroom - 3,500/= <br>
                3 Bedroom - 4,000/= <br>
                Initial Provisional Service Charge deposit (3 months)
             </td>
            <td>i.e. 1 Bedroom 9,000 @ 3,000* a month </td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Service Charge deposit equivalent to Three (3) months provisional service charge</td>
            <td>i.e. 1 Bedroom 9,000/=</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Registration Fees on the Lease and Official Search</td>
            <td>5,000/-</td>
            <td>Vendor’s Advocates</td>
        </tr>
        <tr>
            <td>Installation of Water Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Installation of Electricity Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
    @elseif($project->id == 3)
        {{--Situ--}}
        <tr>
            <td>Service Charge: <br>
                Initial Provisional Service Charge deposit (3 months)
            </td>
            <td>i.e. 150,000 @ 50,000* a month</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Service Charge deposit equivalent to Three (3) months provisional service charge</td>
            <td>i.e. 150,000/=</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Registration Fees on the Lease and Official Search</td>
            <td>5,000/-</td>
            <td>Vendor’s Advocates</td>
        </tr>
        <tr>
            <td>Installation of Water Meter</td>
            <td>10,000/-</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Installation of Electricity Meter</td>
            <td>10,000/-</td>
            <td>Vendor</td>
        </tr>
    @elseif($project->id == 4)
        {{--Taraji--}}
        <tr>
            <td>Service Charge: <br>
                2 Bedroom - 5,000/= <br>
                3 Bedroom - 6,000/= <br>
                3 Bedroom + DSQ - 6,000/= <br>
                Initial Provisional Service Charge deposit (3 months)
            </td>
            <td>i.e. 2 Bedroom 15,000 @ 5,000* a month </td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Service Charge deposit equivalent to Three (3) months provisional service charge</td>
            <td>i.e. 2 Bedroom 15,000/=</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Registration Fees on the Lease and Official Search</td>
            <td>5,000/-</td>
            <td>Vendor’s Advocates</td>
        </tr>
        <tr>
            <td>Installation of Water Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Installation of Electricity Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
    @elseif($project->id == 5)
        {{--Ridge--}}
        <tr>
            <td>Service Charge: <br>
                1 Bedroom - 4,500/= <br>
                2 Bedroom - 5,000/= <br>
                3 Bedroom - 5,500/= <br>
                3 Bedroom + DSQ - 6,000/= <br>
                Penthouse - 6,500/= <br>
                Initial Provisional Service Charge deposit (3 months)
            </td>
            <td>i.e. 1 Bedroom 13,500 @ 4,500* a month </td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Service Charge deposit equivalent to Three (3) months provisional service charge</td>
            <td>i.e. 1 Bedroom 13,500/=</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Registration Fees on the Lease and Official Search</td>
            <td>5,000/-</td>
            <td>Vendor’s Advocates</td>
        </tr>
        <tr>
            <td>Installation of Water Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Installation of Electricity Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
    @elseif($project->id == 6)
        {{--Riverrun--}}
        <tr>
            <td>Service Charge: <br>
                1/4 Villas - 6,520/= <br>
                1/8 Townhouses - 6,520/= <br>
                1/16 Semi-detached - 6,520/= <br>
                Apartments - 4,300/= <br>
                Initial Provisional Service Charge deposit (3 months)
            </td>
            <td>i.e. 1/4 Villas 19,560 @ 6,520* a month </td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Service Charge deposit equivalent to Three (3) months provisional service charge</td>
            <td>i.e. 1/4 Villas 19,560/=</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Registration Fees on the Lease and Official Search</td>
            <td>5,000/-</td>
            <td>Vendor’s Advocates</td>
        </tr>
        <tr>
            <td>Installation of Water Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
        <tr>
            <td>Installation of Electricity Meter</td>
            <td>5,000/-</td>
            <td>Vendor</td>
        </tr>
    @else
    @endif
</table>