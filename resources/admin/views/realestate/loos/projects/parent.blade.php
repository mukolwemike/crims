@extends('reports.letterhead')

@section('content')
    <style>
        #restart:before, .restart:before {
            content: ' '; display: block; clear: right
        }
        .container{
            width: 100%; height: auto;
            page-break-inside: avoid;
        }
        .left-sec {
            float: left;
            width: 20%;
            position: relative;
            height: 100%;
        }
        .right-sec {
            overflow: hidden;
            text-align: justify;
            position: relative;
        }

        .right-sec p {
            margin-left: 20%;
        }

        .right-sec ol{ margin: 0; list-style-position: outside; padding: 0 }
        .right-sec ol li{
            text-align: justify;
            margin-left: 20%;
            padding-left: 2em;
            margin-bottom: 5px;
        }
        .right-sec ol.inner-ol{margin: 0; list-style-position: outside; padding: 0}
        .right-sec ol li.inner{
            text-align: justify;
            margin-left: 27%;
            padding-left: -20%;
            margin-bottom: 5px;
        }
        .bold { font-weight: bold; }
        .left-align{text-align: left}
    </style>

    <!-- Address -->
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    @yield('loo')

@endsection