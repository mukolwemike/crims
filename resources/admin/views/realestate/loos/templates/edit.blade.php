@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h4>Letter Of Offer Templates</h4>

            <br>
            <h5>Project Details</h5>

            <div>
                <p>
                    Project Name: {{ $project->name }}
                    <br><br>

                    <a class="btn btn-info" ng-click="show_details = !show_details"> Show project details</a>
                </p>

                <table ng-show="show_details" class="table table-hover table-responsive table-striped">
                    <tbody>
                    <tr>
                        <td>Long Name</td><td>{!! $project->long_name !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor</td><td>{!! $project->vendor !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor Address</td><td>{!! $project->vendor_address !!}</td>
                    </tr>
                    <tr>
                        <td>Completion Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($project->completion_date) !!}</td>
                    </tr>
                    <tr>
                        <td>County Government</td><td>{!! $project->county_government !!}</td>
                    </tr>
                    <tr>
                        <td>Development Description</td><td>{!! $project->development_description !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Name</td><td>{!! $project->bank_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Branch Name</td><td>{!! $project->bank_branch_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Name</td><td>{!! $project->bank_account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Number</td><td>{!! $project->bank_account_number !!}</td>
                    </tr>
                    <tr>
                        <td>Swift Code</td><td>{!! $project->bank_swift_code !!}</td>
                    </tr>
                    <tr>
                        <td>Clearing Code</td><td>{!! $project->bank_clearing_code !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div>
                {{ Form::model($template, ['route'=>['setup_project_loo_template', $project->id]]) }}
                    <div class="form-group">
                        {{ Form::label('contents', 'Template content') }}

                        {{ Form::textarea('contents', $template->contents, ['class'=>'form-control',  'required']) }}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contents')  !!}
                    </div>

                    <div class="form-group">
                        {{ Form::submit('Save', ['class'=>'btn btn-success']) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection