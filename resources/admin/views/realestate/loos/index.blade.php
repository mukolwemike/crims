@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="LOOsGridController">
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>
                        <select st-search="project" class="form-control">
                            <option value="">All Projects</option>
                            @foreach($projects as $project)
                                <option value="{!! $project->id !!}">{!! $project->name !!}</option>
                            @endforeach
                        </select>
                    </th>
                    <th colspan="2">
                        <select st-search="sent_status" class="form-control">
                            <option value="">Sent Status</option>
                            <option value="1">Sent</option>
                            <option value="2">Not sent</option>
                        </select>
                    </th>
                    <th colspan="2">
                        <select st-search="approval_status" class="form-control">
                            <option value="">Approval Status</option>
                            <option value="1">Approved</option>
                            <option value="2">Not approved</option>
                        </select>
                    </th>
                    <th colspan="2"><input st-search="client_code" class="form-control" placeholder="Search client code..." type="text"/></th>
                    <th colspan="2">
                        <button type="button" class="btn btn-success margin-bottom-20" ng-click="exportLOOs()">
                            <i class="fa fa-file-excel-o"></i> Export <i class="fa fa-spinner fa-spin" ng-if="loading"></i>
                        </button>
                    </th>
                    <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Client Name</th>
                    <th>Project</th>
                    <th>Vendor</th>
                    <th>Unit Number</th>
                    <th>Size</th>
                    <th>Advocate</th>
                    <th>Uploaded</th>
                    <th>Sent</th>
                    <th>Approved</th>
                    <th></th>
                </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                <tr ng-repeat="row in displayed">
                    <td><% row.client_code %></td>
                    <td><% row.fullName %></td>
                    <td><% row.project %></td>
                    <td><% row.vendor %></td>
                    <td><% row.unit_number %></td>
                    <td><% row.size %></td>
                    <td><% row.advocate %></td>
                    <td><% row.document_id > 0 | yesno  %></td>
                    <td class="text-center">
                        <i ng-show="row.status == true" class="fa fa-check-circle-o" style="color:green;"></i>
                        <i ng-show="row.status == false" style="color:red;">Not Sent</i>
                    </td>
                    <td class="text-center">
                        <span ng-hide="row.pm_approved_on">
                            <i ng-show="row.rejected == true" class="fa fa-thumbs-o-down" style="color:red;"></i>
                        </span>
                        <i ng-show="row.pm_approved_on" class="fa fa-thumbs-o-up" style="color:green;"></i>
                    </td>
                    <td>
                        <a ng-if="row.document_id" href="/dashboard/documents/<% row.document_id %>"><i class="fa fa-eye"></i></a>
                        <a class="pull-right" href="/dashboard/realestate/loos/show/<% row.id %>"><i class="fa fa-list-alt"></i></a>
                    </td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr>
                    <td colspan="100%" class="text-center">Loading ... </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop