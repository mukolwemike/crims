@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <h3>Additional Purchaser</h3>

            <div class="detail-group">
                {!! Form::model($joint, ['route'=>['save_joint_purchaser', $form->id, $joint->id]]) !!}

                {!! Form::hidden('client_id', $client->id) !!}

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field">{!! Form::label('title', 'Full Name') !!}</span>
                    </div>
                    <div class="col-md-2">
                        {!! Form::select('title_id', $titles, null, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::text('lastname', null, ['class'=>'form-control', 'placeholder'=>'Last Name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::text('middlename', null, ['class'=>'form-control', 'placeholder'=>'Middle Name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::text('firstname', null, ['class'=>'form-control', 'placeholder'=>'First Name']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('gender_id', 'Gender') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('gender_id', $gender, null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'gender_id') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('dob', 'Date of Birth') !!}
                    </div>
                    <div class="col-md-4"  ng-controller="DatepickerCtrl">
                        {!! Form::text('dob', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'dob') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('pin_no', 'Pin No') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('pin_no', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}
                    </div>

                    <div class="col-md-2">
                        <span class="required-field">{!! Form::label('id_or_passport', 'ID/Passport No') !!}</span>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('id_or_passport', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_or_passport') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field"> {!! Form::label('email', 'Email') !!}</span>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('email', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('postal_code', 'Postal Code') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('postal_code', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('postal_address', 'Postal Address') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('postal_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('country_id', 'Country') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('country_id',  $countries, 114, ['class'=>'form-control'])!!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country_id') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('telephone_office', 'Office Telephone Number') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('telephone_office', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_office') !!}
                    </div>

                    <div class="col-md-2">
                        <span class="required-field">{!! Form::label('telephone_home', 'Home Tel. No.') !!}</span>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('telephone_home', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_home') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('telephone_cell', 'Cell No.') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('telephone_cell', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_cell') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('residential_address', 'Residential Address') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('residential_address', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'residential_address') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                        <span class="required-field"> {!! Form::label('town', 'Town') !!}</span>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('town', null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('method_of_contact_id', 'Preferred Method of Contact') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('method_of_contact_id', $contactMethods, null, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'method_of_contact_id') !!}
                    </div>
                </div>

                <div class="col-md-12">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
                <div class="pull-right">All fields with  (<span class="required-field"></span>) are required and must be filled before submitting.</div>
            </div>
        </div>
    </div>
@endsection