@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h4>Reservation form</h4>

            <hr/>

            <div class="detail-group form-detail">
                <h4 class="col-md-12">Client Details</h4>

                <div class="col-md-4"><label>Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</div>

                <div class="col-md-4"><label>Postal Address </label>{!! $form->postal_address !!}</div>

                <div class="col-md-4"><label>Postal Code</label> {!! $form->postal_code !!}</div>

                <div class="col-md-4"><label>Town/City</label> {!! $form->town !!}</div>

                <div class="col-md-4"><label>Physical Address</label> {!! $form->street !!}</div>

                <div class="col-md-4"><label>Country</label> {!! $form->country ? $form->country->name: '' !!}</div>

                <div class="col-md-4"><label>Nationality</label> {!! $form->nationality ? $form->nationality->name: '' !!}</div>

                <div class="col-md-4"><label>ID or Passport No</label> {!! $form->id_or_passport !!}</div>

                <div class="col-md-4"><label>PIN Number</label> {!! $form->pin_no !!}</div>

                <div class="col-md-4"><label>Home Telephone Number</label> {!! $form->telephone_home !!}</div>

                <div class="col-md-4"><label>Work Telephone Number</label> {!! $form->telephone_office !!}</div>

                <div class="col-md-4"><label>Mobile Number</label> {!! $form->phone !!}</div>

                <div class="col-md-4"><label>Email</label> {!! $form->email !!}</div>

                <div class="col-md-12"><label>Contact person</label> {!! @$titles($form->contact_person_title)->name.' '.$form->contact_person_fname.' '.$form->contact_person_lname !!}</div>
            </div>

            <div class="detail-group">
                <h4 class="col-md-12">Additional Purchasers</h4>

                <div class="col-md-12">
                    @if($joints->count() > 0)
                        <table class = "table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email Address</th>
                                <th>Phone Number</th>
                                <th>Pin Number</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($joints as $holder)
                                <tr>
                                    <td><a href="/dashboard/clients/joint/{!! $holder->id !!}">{!!  ucfirst($holder->firstname) !!} {!! ucfirst($holder->lastname) !!}</a></td>
                                    <td>{!! $holder->email !!}</td>
                                    <td>{!! $holder->telephone_cell !!}</td>
                                    <td>{!! $holder->pin_no !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                    {{--@if(! $approved)--}}
                    <a class = "btn btn-success" href = "/dashboard/realestate/reservations/joint/{!! $form->id !!}">Add additional purchaser</a>
                    {{--@endif--}}
                </div>
            </div>

            <div class="detail-group form-detail">
                <h4 class="col-md-12">Banking Details</h4>

                <div class="col-md-4"><label>Account Name</label>{!! $form->investor_account_name !!}</div>

                <div class="col-md-4"><label>Account Number</label>{!! $form->investor_account_number !!}</div>

                <div class="col-md-4"><label>Bank Name</label>{!! $form->investor_bank !!}</div>

                <div class="col-md-4"><label>Branch</label> {!! $form->investor_bank_branch !!}</div>

                <div class="col-md-4"><label>Swift code</label>{!! $form->investor_swift_code !!}</div>

                <div class="col-md-4"><label>Clearing code</label>{!! $form->investor_clearing_code !!}</div>
            </div>
        </div>
    </div>
@endsection