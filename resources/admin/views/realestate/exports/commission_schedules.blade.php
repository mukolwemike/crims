<h2>Commission Schedules</h2>
<table>
    <thead>
    <tr>
        <th>Client Code</th>
        <th>Client Name</th>
        <th>Project</th>
        <th>Unit</th>
        <th>Description</th>
        <th>Reservation Date</th>
        <th>LOO Date</th>
        <th>Sales Ag. Date</th>
        <th>Commission Date</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($schedules as $cs)
        <?php $holding = $cs->commission->holding ?>
        <tr>
            <td>{!! $holding->client->client_code !!}</td>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($holding->client_id) !!}</td>
            <td>{!! @$holding->project->name !!}</td>
            <td>{!! @$holding->unit->number !!}</td>
            <td>{!! $cs->description !!}</td>
            <td>{!! $holding->reservation_date !!}</td>
            <td>{!! @$holding->loo->date_received !!}</td>
            <td>{!! @$holding->salesAgreement->date_received !!}</td>
            <td>{!! $cs->date !!}</td>
            <td>{!! $cs->amount !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@if($override != 0)
    <h2>Overrides</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Override</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <td>{!! $report->name !!}</td>
                <td>{!! $report->rank->name !!}</td>
                <td>{!! $report->commission !!}</td>
                <td>{!! $report->rate !!}</td>
                <td>{!! $report->override !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif