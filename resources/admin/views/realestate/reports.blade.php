@extends('layouts.default')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel-dashboard">
            <div class="panel">
                <div class="panel-heading">
                    <h3>Real Estate Reports</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Client Contacts</td>
                                <td><a href="#add-event-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a></td>
                            </tr>
                            <tr>
                                <td>Client Summary</td>
                                <td>
                                    <a href = "/dashboard/realestate/reports/clients-summaries" class="btn btn-success">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Clients Tranche Report</td>
                                <td>
                                    <a href="#clients-tranche-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Reservation Summary Report</td>
                                <td>
                                    <a href="#reservation-summary-export-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>

                            <tr>
                                <td>Deposit Payment Expiry (60+ days) Report</td>
                                <td>
                                    {!! Form::open(['route'=>['deposit_payment_expiry_notifications_report_path']]) !!}
                                        {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td>All LOOs Sent for a Project</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Daily Report of LOOs Sent to Clients</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Weekly Report of LOOs Sent</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Weekly Report of LOOs Whose Sale Agreements Have Not Been Signed</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Clients Tranche Report</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Tranche Reports for Each Project</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Clients With Investments and/or RE</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}

                            <tr>
                                <td>Combined Clients with overdue payments</td>
                                <td>
                                    <a href = "/dashboard/realestate/reports/clients-overdue" class="btn btn-success">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Client Overdue Payments</td>
                                <td>
                                    <a href="#clients-overdue-payments" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cash Flow</td>
                                <td>
                                    <button type="button" class="btn btn-success" ng-controller="PopoverCtrl" popover-trigger="mouseenter" data-toggle="modal" data-target="#download-cash-flow">Download</button>
                                    <div class="modal fade" id="download-cash-flow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Download Cash Flow</h4>
                                                </div>
                                                {!! Form::open(['url'=>'/dashboard/realestate/reports/export-project-cash-flows']) !!}
                                                <div class="modal-body">

                                                    <div class = "row">
                                                        <div class = "col-md-12">
                                                            <div class = "">
                                                                <div class="col-md-6">
                                                                    <div class = "form-group">

                                                                        {!! Form::label('project_id', 'Select project') !!}

                                                                        {!! Form::select('project_id', $projects, null, ['class'=>'form-control']) !!}
                                                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6"  ng-controller="DatepickerCtrl">
                                                                    {!! Form::label('date', 'Effective Date') !!}
                                                                    {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    {!! Form::submit('Download', ['class'=>'btn btn-danger']) !!}
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            {{--<tr>--}}
                                {{--<td>Client Summary</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            <tr>
                                <td>Payment Summary</td>
                                <td>
                                    <button type="button" class="btn btn-success" ng-controller="PopoverCtrl" popover-trigger="mouseenter" data-toggle="modal" data-target="#choose-project">Export</button>

                                    <!-- Choose project to export real estate payments summaries -->

                                    <div class="modal fade" id="choose-project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Real Estate Project</h4>
                                                </div>
                                                {!! Form::open(['route'=>'realestate_payments_summary_path']) !!}
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        {!! Form::label('project_id', 'Select Project') !!}
                                                        {!! Form::select('project_id', $projects, null, ['id'=>'category', 'class'=>'form-control']) !!}
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>

                                    <!-- end of modal for the real estate payments summaries -->
                                </td>
                            </tr>

                            <tr>
                                <td>Project Payment Summary</td>
                                <td>
                                    <button type="button" class="btn btn-success" ng-controller="PopoverCtrl" popover-trigger="mouseenter" data-toggle="modal" data-target="#choose-project-payment">Export</button>

                                    <!-- Choose project to export real estate payments summaries -->

                                    <div class="modal fade" id="choose-project-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Real Estate Project</h4>
                                                </div>
                                                {!! Form::open(['route'=>'realestate_project_payments_summary_path']) !!}
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            {!! Form::label('project_id', 'Select Project') !!}
                                                            {!! Form::select('project_id', $projects, null, ['id'=>'category', 'class'=>'form-control']) !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <p>Select start and end dates</p>
                                                        </div>

                                                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                                                            {!! Form::label('start', 'Start Date') !!}
                                                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                                        </div>
                                                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                                                            {!! Form::label('end', 'End Date') !!}
                                                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                                                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>

                                    <!-- end of modal for the real estate payments summaries -->
                                </td>
                            </tr>

                            {{--<tr>--}}
                                {{--<td>Payment Reminders for Upcoming Payments</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Units Summary Daily Report</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Units Summary Weekly Report</td>--}}
                                {{--<td></td>--}}
                            {{--</tr>--}}
                            <tr>
                                <td>Forfeitures</td>
                                <td><a href = "/dashboard/realestate/reports/forfeited-units" class="btn btn-success">Export</a></td>
                            </tr>
                            <tr>
                                <td>Real Estate Inflows Report</td>
                                <td>
                                    <a href="#real-estate-inflows-report" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Real Estate Commissions</td>
                                <td>
                                    <a href="#real-estate-commissions-report" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Real Estate Holdings Summary</td>
                                <td>
                                    <a href="#re-holdings-summary-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Real Estate Monthly Holdings Summary</td>
                                <td>
                                    <a href="#real-estate-monthly-holdings-report-modal" class="btn btn-success" data-toggle="modal" role="button">Export</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--the start of the modal window--}}


    <div class="modal fade" id="add-event-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>'realestate_report_path']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Export Clients Contacts</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group" ng-init="category='realestate'">
                        <div class="col-md-12">
                            {!! Form::label('category', 'Select category') !!}
                            {!! Form::select('category', $categories, null, ['id'=>'category', 'class'=>'form-control', 'required', 'ng-model' => 'category']) !!}
                        </div>
                        <div class="col-md-12">
                            {!! Form::label('active', 'Active Status') !!}
                            {!! Form::select('active', $activeStatuses, null, ['id'=>'active', 'class'=>'form-control', 'required']) !!}
                        </div>
                        <div ng-show="category != 'realestate'" class="col-md-12">
                            {!! Form::label('product_ids', 'Select Products') !!}
                            {!! Form::select('product_ids[]', $products, null, ['v-select'=>'', 'multiple', 'class'=>'form-control', 'style'=>'width: 100%;']) !!}
                        </div>

                        <div ng-show="category != 'investment'" class="col-md-12">
                            {!! Form::label('project_ids', 'Select Projects') !!}
                            {!! Form::select('project_ids[]', $clientProjects, null, ['v-select'=>'', 'multiple', 'class'=>'form-control', 'style'=>'width: 100%;']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Generate', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--end of modal--}}
    {{--the start of the clients tranche report modal window--}}
    <div class="modal fade" id="clients-tranche-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>'clients_tranche_report_path']) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Clients Tranche Report</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('project_id', 'Project') !!}
                        {!! Form::select('project_id', \App\Cytonn\Models\Project::all()->lists('name', 'id'), null, ['id'=>'project_id', 'class'=>'form-control', 'required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--start reservation summary export modals--}}
    <div class="modal fade" id="reservation-summary-export-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['reservation_summary_report_path']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Reservation Summary Export</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Select start and end dates</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--end of clients tranche report modal--}}

    {{--start deposit payment expiry notifications export modals--}}
    {{--<div class="modal fade" id="deposit-payment-expiry-notifications-export-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--{!! Form::open(['route'=>['deposit_payment_expiry_notifications_report_path']]) !!}--}}
                {{--<div class="modal-header">--}}
                    {{--<a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>--}}
                    {{--<h4 class="modal-title" id="myModalLabel">Deposit Payment Expiry Notifications Export</h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body row">--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<p>Select start and end dates</p>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6"  ng-controller="DatepickerCtrl">--}}
                            {{--{!! Form::label('start', 'Start Date') !!}--}}
                            {{--{!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6"  ng-controller="DatepickerCtrl">--}}
                            {{--{!! Form::label('end', 'End Date') !!}--}}
                            {{--{!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--{!! Form::submit('Export', ['class'=>'btn btn-success']) !!}--}}
                    {{--<a data-dismiss="modal" class="btn btn-default">Close</a>--}}
                {{--</div>--}}
                {{--{!! Form::close() !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--end of deposit payment expiry notifications export modal--}}

    {{--REAL ESTATE INFLOWS --}}
    <div class="modal fade" id="real-estate-inflows-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['real_estate_inflows_export']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Real Estate Inflows Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Choose the start and the end date for the real estate inflows you wish to export</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--CLIENT WITH OVERDUE PAYMENTS--}}
    <div class="modal fade" id="clients-overdue-payments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['real_estate_client_with_overdue_payments']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Clients With Overdue Payments Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Choose the start and the end date for the real estate inflows you wish to export</p>
                        </div>

                        <div class="col-md-6" >
                            {!! Form::label('project_id', 'Select project') !!}
                            {!! Form::select('project_id', $projects, null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Effective Date') !!}
                            {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--REAL ESTATE INFLOWS --}}
    <div class="modal fade" id="real-estate-commissions-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['real_estate_commisisons_export']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Real Estate Commissions Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <p>Choose the start, end date and project for the real estate commissions you wish to export</p>
                        </div>

                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>

                        <div class="col-md-6" >
                            {!! Form::label('project_id', 'Select project') !!}
                            {!! Form::select('project_id', $projects, null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

{{--    REAL ESTATE HOLDINGS SUMMARY--}}
    <div class="modal fade" id="re-holdings-summary-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['real_estate_holdings_summary_export']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Real Estate Holdings Summary</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-12"  ng-controller="DatepickerCtrl">
                            {!! Form::label('date', 'Date') !!}
                            {!! Form::text('date', NULL, ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>

                        <div class="col-md-12" >
                            {!! Form::label('project_id', 'Select project') !!}
                            {!! Form::select('project_id', array_except($projects, ['']), null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--REAL ESTATE INFLOWS --}}
    <div class="modal fade" id="real-estate-monthly-holdings-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['real_estate_monthly_holdings_summary_export']]) !!}
                <div class="modal-header">
                    <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">Real Estate Monthly Holdings Report</h4>
                </div>
                <div class="modal-body row">
                    <div class="form-group">
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('start', 'Start Date (Optional)') !!}
                            {!! Form::text('start', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>
                        <div class="col-md-6"  ng-controller="DatepickerCtrl">
                            {!! Form::label('end', 'End Date') !!}
                            {!! Form::text('end', NULL, ['required', 'class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                        </div>

                        <div class="col-md-6" >
                            {!! Form::label('project_id', 'Select project') !!}
                            {!! Form::select('project_id', array_except($projects, ['']), null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'project_id') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                    <a data-dismiss="modal" class="btn btn-default">Close</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop