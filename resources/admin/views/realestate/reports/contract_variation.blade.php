@extends('reports.letterhead')

@section('content')
    {{--@include('reports.partials.client_address')--}}

    <p class="bold-underline">REAL ESTATE CONTRACT VARIATION</p>

    <div class="body">
        <p>
            Please see below the Real Estate Letter of Offer details.
        </p>
        <?php $total = 0; ?>
        <table class="table table-striped table-responsive">
            <tbody>
                <tr class=bold>
                    <td colspan="2">Client Code</td>
                    <td>{!! $client->client_code !!}</td>
                </tr>
                <tr class=bold>
                    <td colspan="2">Client Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                </tr>
                <?php $unit = $holding->unit; ?>
                <tr class=bold>
                    <td colspan="2">Project</td>
                    <td>{!! $unit->project->name !!}</td>
                </tr>
                <tr class=bold>
                    <td colspan="2">Price</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td>
                </tr>
                <tr class=bold>
                    <td colspan="2">Payment Plan</td>
                    <td>{!! $holding->paymentPlan->name !!}</td>
                </tr>
                <tr class="bold">
                    <td colspan="2">Unit</td>
                    <td>Unit {!! $holding->unit->number !!} - {!! $unit->size->name !!} {!! $unit->type->name !!}</td>
                </tr>
                    @foreach($schedules = $holding->paymentSchedules()->inPricing()->oldest('date')->get() as $schedule)
                        <tr>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount, false, 0)!!}</td>
                        </tr>
                    @endforeach
                <tr>
                    <td colspan="2">Total</td>
                    <td align="right">{!!  \Cytonn\Presenters\AmountPresenter::currency($schedules->sum('amount'), false, 0) !!}</td>
                </tr>
            </tbody>
        </table>

        <p>
            <span class="bold">Project Manager Approval:</span><br /><br />
            Date ......................................................................<br /><br />
            Signature .................................................................<br /><br />
        </p>

        <p>
            <span class="bold">Financial Advisor Approval:</span><br /><br />
            Date ......................................................................<br /><br />
            Signature .................................................................<br /><br />
        </p>
    </div>
@endsection