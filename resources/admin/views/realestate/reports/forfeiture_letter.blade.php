@extends('reports.letterhead', ['logo'=>'cre_logo_md.png'])

@section('content')
    <style>
        .cytonn-green{
            background: #006666;
            color: #FFF;
        }
    </style>
    {!! \Carbon\Carbon::today()->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Client Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">RE: NOTICE OF EXPIRY OF RESERVATION      </p>

    <p>The above subject-matter where we act for our Client {!! $project->vendor !!} refers.</p>

    <p>Please note that a period of thirty(30) days has lapsed since you signed the Reservation Form and paid
        the Reservation Deposit on account of the {!! $unit->size->name !!} Unit @if($unit->number) No @endif {!! $unit->number !!}
        in our mixed use residential development known as “{!! $project->name !!}” situate in {!! $project->location !!}.
    </p>

    <p>
        We therefore write to notify you that our Client has graciously agreed to extend the Reservation period
        by a further period of fourteen (14) days from the date hereof to enable you make the Deposit equivalent
        to ten (10%) of the Purchase Price and sign the Letter of Offer, failure to which the said Reservation shall
        automatically stand null and void by operation of the terms thereof without any further notice to yourself.
    </p>
    <p>
        Thank you choosing to partner with Cytonn Real Estate.
    </p>

    <p>Yours Sincerely,</p>
    <p class="bold"> For Cytonn Real Estate
        <br/>
        {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender->id) !!}
    </p>
@stop
