@extends('reports.letterhead')

@section('content')
    <style>
        .real-estate-statement, .real-estate-statement tr, .real-estate-statement th, .real-estate-statement td{
            border: 1px solid #000;
        }
        .real-estate-statement tr th {
            font-weight: bold;
            text-align: left;
        }
        .cytonn-green{
            background: #006666;
            color: #FFF;
        }
    </style>
    {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Client Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">RE: STATEMENT OF ACCOUNT</p>

    Cytonn Real Estate takes this opportunity to thank you for investing in our Real Estate product, {!! ucwords($project->name) !!}.
    <br><br>

    Kindly see below your investment statement showing the account status as at {!! $statementDate->format('jS F Y')  !!}. <br>
    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive real-estate-statement">
                <tbody>
                <tr class="cytonn-green">
                    <th>Project</th>
                    @foreach($clusters as $cluster)
                        <th class="bold">{!! $cluster->project->name !!}</th>
                    @endforeach
                    @if($clusters->count() > 1)
                        <th>Totals</th>
                    @endif
                </tr>
                <tr>
                    <th>Unit Size</th>
                    @foreach($clusters as $cluster)
                        <td>{!! $cluster->size->name !!} {!! $cluster->type->name !!}</td>
                    @endforeach
                    @if($clusters->count() > 1)
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <th>Unit Numbers</th>
                    @foreach($clusters as $cluster)
                        <td>{!! $cluster->unitNumbers !!}</td>
                    @endforeach
                    @if($clusters->count() > 1)
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <th>No. of Units</th>
                    @foreach($clusters as $cluster)
                        <td>{!! $cluster->holdings->count() !!} {!! str_plural($cluster->type->name, $cluster->holdings->count()) !!}</td>
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! $totals->no_of_units !!} {!! str_plural('Unit', $totals->no_of_units) !!}</td>
                    @endif
                </tr>
                <tr>
                    <th>Unit Price (KES)</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->price) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <th>Total Purchase Price</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_price) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_price) !!}</td>
                    @endif
                </tr>
                @if($totals->interest_accrued > 0)
                    <tr>
                        <th>Interest on overdue payments (KES)</th>
                        @foreach($clusters as $cluster)
                            @if($cluster->price != 0)
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->interest_accrued) !!}*</td>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                        @if($clusters->count() > 1)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('interest_accrued')) !!}</td>
                        @endif
                    </tr>
                @endif
                <tr>
                    <th>Booking Fees</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->reservation_fees) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->reservation_fees) !!}</td>
                    @endif
                </tr>
                <tr>
                    <th>Deposit</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->deposit) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->deposit) !!}</td>
                    @endif
                </tr>
                <tr>
                    <th>Installments</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_installments) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_installments) !!}</td>
                    @endif
                </tr>
                <tr>
                    <th>Total Payment Received (KES)</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_payments_received) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_payments_received) !!}</td>
                    @endif
                </tr>
                <tr>
                    <th>Payment Pending (KES)</th>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_pending) !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    @if($clusters->count() > 1)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->payment_pending) !!}</td>
                    @endif
                </tr>

                @if($has_payments_due)
                    <tr>
                        <td height="1em" colspan="{!! $clusters->count() > 1 ? 2 + $clusters->count() : 1 + $clusters->count() !!}"></td>
                    </tr>
                    <tr class="cytonn-green">
                        <th>Payments Overdue (KES)</th>
                        @foreach($clusters as $cluster)
                            @if($cluster->price != 0)
                                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_due) !!}</th>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                        @if($clusters->count() > 1)
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('payment_due')) !!}</th>
                        @endif
                    </tr>
                @endif

                @if($has_next_payments)
                    <tr>
                        <td height="1em" colspan="{!! $clusters->count() > 1 ? 2 + $clusters->count() : 1 + $clusters->count() !!}"></td>
                    </tr>
                    <tr>
                        <th>Next Payment (KES)</th>
                        @foreach($clusters as $cluster)
                            @if($cluster->price != 0)
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->next_payment_amount) !!}</td>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                        @if($clusters->count() > 1)
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('next_payment_amount')) !!}</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Next Payment Date</th>
                        @foreach($clusters as $cluster)
                            @if($cluster->price != 0)
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($cluster->next_payment_date) !!}</td>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                        @if($clusters->count() > 1)
                            <td></td>
                        @endif
                    </tr>
                @endif
                </tbody>
            </table>
            @if($totals->interest_accrued > 0)
                <p>Below is the breakdown of the interest accrued </p>
                <table class="table table-responsive real-estate-statement">
                    <thead>
                    <tr class="cytonn-green">
                        <th>Principal</th><th>Paid</th><th>Balance</th><th>Rate</th><th>Due Date</th><th>Tenor</th><th>Interest</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            @foreach($cluster->holdings as $holding)
                                @foreach($holding->overdue_schedules as $schedule)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->principal) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->paid) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->balance) !!}</td>
                                        <td>{!! \Cytonn\Realestate\Payments\Interest::INTEREST_RATE !!}%</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->due_date) !!}</td>
                                        <td>{!! $schedule->tenor !!} days</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->interest_accrued) !!}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif

    Thank you for investing with us. <br><br>

    <p>Yours Sincerely,</p>
    <p class="bold"> For Cytonn Real Estate
        <br/>
        {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender->id) !!}
    </p>
@stop

{{--@section('quote')--}}
{{--<p class="footer-quote">--}}
{{--@if(!is_null($quote))--}}
{{--{!! $quote->quote !!}--}}
{{--@endif--}}
{{--</p>--}}
{{--@stop--}}