@extends('reports.letterhead')

@section('content')
    @include('reports.partials.client_address')

    <p class="bold-underline">REAL ESTATE PAYMENT PLAN</p>

    <div class="body">
        <p>
            Please see below your Real Estate Payment plan details.
        </p>
        <?php $total = 0; ?>
        <table class="table table-striped table-responsive">
            <thead>
                <tr>
                    <th>Description</th><th>Date</th><th>Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach($client->unitHoldings()->active()->get() as $holding)
                    <?php $unit = $holding->unit; ?>
                    <tr class=bold><td colspan="3">{!! $unit->project->name !!}</td></tr>
                    <tr class="bold"><td colspan="3">Unit {!! $holding->unit->number !!} - {!! $unit->size->name !!} {!! $unit->type->name !!}</td></tr>
                    @foreach($schedules = $holding->paymentSchedules()->inPricing()->oldest('date')->get() as $schedule)
                        <tr>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            <td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount, false, 0)!!}</td>
                        </tr>
                    @endforeach
                    <tr><td>Sub Total</td><td></td><td align="right">{!!  \Cytonn\Presenters\AmountPresenter::currency($schedules->sum('amount'), false, 0) !!}</td></tr>
                    <?php $total += $schedules->sum('amount') ?>
                    <tr><td></td><td></td><td></td></tr>
                @endforeach
                <tr class="bold"><td>Total</td><td></td><td align="right">{!! \Cytonn\Presenters\AmountPresenter::currency($total, false, 0) !!}</td></tr>
            </tbody>
        </table>

        Thank you for investing with us. <br><br>

        <p>Yours Sincerely,</p>
        </br>
        <p class="bold"> For Cytonn Real Estate
            <br/>
            {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender->id) !!}
        </p>
    </div>
@endsection