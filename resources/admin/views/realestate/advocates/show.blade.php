@extends('layouts.default')

@section('content')
    <div class = "col-md-10 col-md-offset-1">
        <div class = "panel-dashboard">
            <h3>Advocate</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $advocate->name !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td>{!! $advocate->postal_code !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Address</td>
                        <td>{!! $advocate->postal_address !!}</td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td>{!! $advocate->street !!}
                    </tr>
                    <tr>
                        <td>Town</td>
                        <td>{!! $advocate->town !!}</td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>
                            @if($advocate->country)
                                {!! $advocate->country->name !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Building</td>
                        <td>{!! $advocate->building !!}</td>
                    </tr>
                    <tr>
                        <td>Telephone Office</td>
                        <td>{!! $advocate->telephone_office !!}</td>
                    </tr>
                    <tr>
                        <td>Telephone Cell</td>
                        <td> {!! $advocate->telephone_cell !!}</td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>{!! $advocate->email !!}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>{!! $advocate->address !!}</td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>
                            {!! \Cytonn\Presenters\BooleanPresenter::presentIcon($advocate->active) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Projects</td>
                        <td>{!! $projects !!}</td>
                    </tr>
                    <tr>
                        <td>Actions</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default" href="/dashboard/realestate/advocates/{!! $advocate->id !!}/edit"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                @if($advocate->active)
                                    <a class="btn btn-danger" href="/dashboard/realestate/advocates/{!! $advocate->id !!}/status"><i class="fa fa-close"></i> Deactivate</a>
                                @else
                                <a class="btn btn-success" href="/dashboard/realestate/advocates/{!! $advocate->id !!}/status"><i class="fa fa-check-circle-o"></i> Activate</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection