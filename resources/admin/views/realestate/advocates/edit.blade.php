@extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Create Advocate</h3>

            {!! Form::model($advocate, ['route'=>['update_advocate', $advocate->id], 'method'=>'put']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}

                    {!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('postal_code', 'Postal Code') !!}--}}

                    {{--{!! Form::text('postal_code', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('postal_address', 'Postal Address') !!}--}}

                    {{--{!! Form::text('postal_address', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('street', 'Street') !!}--}}

                    {{--{!! Form::text('street', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('town', 'Town') !!}--}}

                    {{--{!! Form::text('town', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('country', 'Country') !!}--}}

                    {{--{!! Form::select('country_id', $countries, null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('building', 'Building') !!}--}}

                    {{--{!! Form::text('building', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'building') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('telephone_office', 'Telephone Office') !!}--}}

                    {{--{!! Form::text('telephone_office', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_office') !!}--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('telephone_cell', 'Telephone Cell') !!}--}}

                    {{--{!! Form::text('telephone_cell', null, ['class'=>'form-control']) !!}--}}
                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_cell') !!}--}}
                {{--</div>--}}

                <div class="form-group">
                    {!! Form::label('email', 'E-mail Address') !!}

                    {!! Form::email('email', null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>

                <div class="form-group" ng-controller="SummerNoteController">
                    {!! Form::label('address', 'Address') !!}
                    <summernote  config="options" ng-model="summernote" ng-init="summernote='{!! $advocate->address !!}'"
                                 on-paste="onPaste(evt)"></summernote>
                    {!! Form::text('address', null, ['ng-model'=>'summernote', 'style'=>'display: none;']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('project_ids', 'Select Projects to Assign') !!}

                    {!! Form::select('project_ids[]', $projects, null, ['v-select'=>'', 'multiple', 'class'=>'form-control', 'style'=>'width: 100%;']) !!}
                </div>

            <div class="form-group">
                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection