@extends('layouts.default')
@section('content')
    <div class="panel-dashboard">
        <div ng-controller="AdvocatesGridController">
            <a class="btn btn-default pull-right margin-bottom-20 margin-left-20" href="/dashboard/realestate/advocates/create"><i class="fa fa-plus-square-o"></i> Create Advocate</a>
            <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th colspan="2">
                            <select st-search="project" class="form-control">
                                <option value="">All Projects</option>
                                @foreach($projects as $key => $project)
                                    <option value="{!! $key !!}">{!! $project !!}</option>
                                @endforeach
                            </select>
                        </th>
                        <th colspan="2"></th>
                        <th>
                            <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                        </th>
                    </tr>
                    <tr>
                        <th st-sort="id">ID</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th class="text-center">Active</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.name %></td>
                        <td><% row.email %></td>
                        <td class="text-center" ng-switch="row.active">
                            <i ng-switch-when="true" class="fa fa-check-circle-o" style="color:green;"></i>
                            <i ng-switch-default class="fa fa-close" style="color:red;"></i>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default btn-xs" href="/dashboard/realestate/advocates/<% row.id %>"><i class="fa fa-list-alt"></i> View</a>
                                <a ng-if="row.active" class="btn btn-danger btn-xs" href="/dashboard/realestate/advocates/<% row.id %>/status"><i class="fa fa-close"></i> Deactivate</a>
                                <a ng-if="!row.active" class="btn btn-success btn-xs" href="/dashboard/realestate/advocates/<% row.id %>/status"><i class="fa fa-check-circle-o"></i> Activate</a>
                            </div>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop