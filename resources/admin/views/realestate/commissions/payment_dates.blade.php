@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div ng-controller="RealEstateCommissionPaymentDatesGridController">
                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th colspan="4">
                                <button class="btn btn-success margin-bottom-20" data-toggle="modal" data-target="#add-new-payment-date"><i class="fa fa-plus-square-o"></i> Add New</button>
                            </th>
                        </tr>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody  ng-show="!isLoading">
                        <tr ng-repeat="row in displayed">
                            <td><% row.start_date %></td>
                            <td><% row.end_date %></td>
                            <td><% row.description %></td>
                            <td>
                                <a ng-controller="PopoverCtrl" uib-popover="More Actions" popover-trigger="mouseenter" href="/dashboard/investments/commission/payment-dates/show/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                        <tr>
                            <td colspan="4" class="text-center">Loading ... </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td  colspan="1" class="text-center">
                                Items per page
                            </td>
                            <td colspan="1" class="text-center">
                                <input type="text"  ng-model="itemsByPage"/>
                            </td>
                            <td colspan="2" class="text-center">
                                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="add-new-payment-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Commission Payment Date</h4>
                </div>
                {!! Form::open(['route'=>'save_realestate_commission_payment_date']) !!}
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('start', 'Start date') !!}

                        {!! Form::text('start', null, ['class'=>'form-control', 'datepicker-popup init-model'=>'start', 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('end', 'End date') !!}

                        {!! Form::text('end', null, ['class'=>'form-control', 'datepicker-popup init-model'=>'end', 'is-open'=>"status.opened_", 'ng-focus'=>'status.opened_ = !status.opened_']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Description') !!}

                        {!! Form::text('description', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection