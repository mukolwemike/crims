@extends('layouts.default')

@section('content')

    <div class="panel-dashboard">
        <div class="row">
            <div class="col-md-6">
                <div class="detail-group">
                    <h4>FA Details</h4>

                    <table class="table table-hover table-responsive">
                        <thead>
                        </thead>
                        <tbody>
                        <tr><td>Name</td><td>{!! $recipient->name !!}</td></tr>
                        </tbody>
                    </table>
                </div>

                <div class="detail-group">
                    <h4>Commission Calculation ({!! $start->toFormattedDateString() !!} to {!! $end->toFormattedDateString() !!})</h4>

                    <table class="table table-hover table-responsive">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-12">
                <div class="detail-group">
                    <h4>Commission schedules</h4>

                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Client Code</th><th>Name</th><th>Project</th><th>Unit</th><th>Type</th><th>Description</th><th>LOO</th><th>SA</th><th>Date</th><th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td>{!! $schedule->client_code !!}</td>
                                <td>{!! $schedule->client_name !!}</td>
                                <td>{!! $schedule->project_name !!}</td>
                                <td><a href="{!! route('commission_for_unit', [$schedule->holding->id]) !!}">@if( $schedule->unit_number){!! $schedule->unit_number !!} @else [No number] @endif</a></td>
                                <td>{!! $schedule->type->name !!}</td>
                                <td>{!! $schedule->description !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->holding->loo->date_received) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->holding->salesAgreement->date_received) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th>Total</th>
                            <td colspan="8"></td>
                            <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection