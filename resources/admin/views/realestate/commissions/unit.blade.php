@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="col-md-12">
            <a class="btn btn-info margin-bottom-20" href="/dashboard/realestate/projects/{!! $holding->project->id !!}/units/show/{!! $holding->unit_id !!}"><i class="fa fa-long-arrow-left"></i> Unit Details</a>
            <a href="#awardAdditionalCommissionModal" class="btn btn-success pull-right" data-toggle="modal" role="button">Award Additional Commission</a>
        </div>
        <div class="col-md-6">
            <div class="detail-group">
                <table class="table table-responsive table-hover">
                    <tbody>
                    <tr><td>Project</td><td>{!! $holding->project->name !!}</td></tr>
                    <tr><td>Unit</td><td>{!! $unit->number !!} ({!! $unit->size->name !!}) {!! $unit->type->name !!}</td></tr>
                    <tr><td>Client</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id) !!} {!! $holding->client->client_code !!}</td></tr>
                    <tr><td>Price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            <div class="detail-group">
                <table class="table table-responsive table-hover">
                    <tbody>
                    <tr><td>Financial Advisor</td><td>{!! $commission->recipient->name !!}</td></tr>
                    <tr><td>Rate</td><td>{!! $commission->present()->getRate. " ({$commission->present()->getType})" !!}</td></tr>
                    <tr><td>Total Commission</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($commission->amount) !!}</td></tr>
                    </tbody>
                </table>

                <div class="margin-bottom-10">
                    <a href="#award-real-estate-holding-commission" class="btn btn-success" data-toggle="modal" role="button">Award Commission</a>
                </div>

                {{--start of award commission modal--}}
                <div class="modal fade" id="award-real-estate-holding-commission" tabindex="-1" role="dialog" aria-labelledby="award-real-estate-holding-commission-label">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open(['route'=>['award_real_estate_holding_commission', $holding->id], 'method'=>'POST']) !!}
                            <div class="modal-header">
                                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                <h4 class="modal-title" id="myModalLabel">Award Unit Holding Commission</h4>
                            </div>
                            <div class="modal-body row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <p>Are you sure you would like to award commission for this unit?</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                {!! Form::hidden('holding_id', $holding->id) !!}
                                {!! Form::submit('Award', ['class'=>'btn btn-success']) !!}
                                <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                {{--end of award commission modal--}}
            </div>
        </div>

        <div class="col-md-12">
            <h4>Schedules</h4>
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>Type</th><th>Description</th><th>LOO</th><th>SA</th><th>Date</th><th>Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schedules as $schedule)
                    <tr>
                        <td>{!! $schedule->type->name !!}</td>
                        <td>{!! $schedule->description !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($holding->loo->date_received) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($holding->salesAgreement->date_received) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                        <td><!-- Button trigger modal -->
                            <a href="#" data-toggle="modal" data-target="#editModal{!! $schedule->id !!}">
                                <i class="fa fa-edit"></i>
                            </a>

                            <!-- Modal -->
                            <div class="modal fade" id="editModal{!! $schedule->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">

                                        {!! Form::model($schedule, ['route'=>['update_for_schedule', $schedule->id]]) !!}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update Payment Schedule</h4>
                                        </div>
                                        <div class="modal-body">
                                            <label for="">Name: </label>{!! $schedule->type->name !!} <br>
                                            <label for="">Description: </label>{!! $schedule->description !!}<br>
                                            <label for="">Date: </label>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}
                                            <br>
                                            <label for="">Amount: </label>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}
                                            <br> <br>

                                            <div class="form-group">
                                                {!! Form::label('date', 'New payment date') !!}

                                                {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'status.opened = !status.opened']) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('reason', 'Reason for change') !!}

                                                {!! Form::text('reason', null, ['class'=>'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th>Total</th>
                    <td colspan="4"></td>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($schedules->sum('amount')) !!}</th>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-12">
            <h4>Advance Commission Schedules</h4>
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>Type</th><th>Description</th><th>Date</th><th>Amount</th><th>Rapayable</th><th>Paid</th><th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($holding->advanceCommissions as $advanceCommission)
                    <tr>
                        <td>{!! $advanceCommission->type->name !!}</td>
                        <td>{!! $advanceCommission->description !!}</td>
                        <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($advanceCommission->date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($advanceCommission->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($advanceCommission->repayable) !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($advanceCommission->paid) !!}</td>
                        <td>
                            @if($advanceCommission->present()->canEdit)
                                <a href="#" data-toggle="modal"
                                   data-target="#editAdditionalCommission{!! $advanceCommission->id !!}">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <div class="modal fade" id="editAdditionalCommission{!! $advanceCommission->id !!}"
                                     tabindex="-1" role="dialog"
                                     aria-labelledby="editAdditionalCommissionLabel{!! $advanceCommission->id !!}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['store_additional_commission', $advanceCommission->id]]) !!}
                                            <div class="modal-header">
                                                <a href="#" type="button" class="close" data-dismiss="modal"
                                                   aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title" id="myModalLabel">Update Additional Commission</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <p>Please provide the following details to update additional
                                                        commission to the FA</p>
                                                </div>

                                                <div class="form-group" ng-controller="DatepickerCtrl">
                                                    {!! Form::label('date', 'Date') !!}
                                                    {!! Form::text('date', $advanceCommission->date, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('amount', 'Amount') !!}
                                                    {!! Form::text('amount', $advanceCommission->amount, ['class'=>'form-control', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('type_id', 'Additional Commission Type') !!}
                                                    {!! Form::select('type_id', $advanceCommissionTypes, $advanceCommission->type_id, ['class'=>'form-control', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('repayable', 'Repayable') !!}
                                                    {!! Form::select('repayable', ['0' => 'Non Repayable', '1' => 'To Be Repayed'], $advanceCommission->repayable, ['class'=>'form-control', 'required']) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('description', 'Description') !!}
                                                    {!! Form::text('description', $advanceCommission->description, ['class'=>'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::hidden('holding_id', $holding->id) !!}
                                                {!! Form::hidden('recipient_id', $commission->recipient_id) !!}
                                                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                                                <a data-dismiss="modal" class="btn btn-default">Close</a>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

{{--Start Additional Commission Modal--}}
<div class="modal fade" id="awardAdditionalCommissionModal" tabindex="-1" role="dialog" aria-labelledby="awardAdditionalCommissionModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['store_additional_commission']]) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Award Additional Commission</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>Please provide the following details to award additional commission to the FA</p>
                </div>

                <div class="form-group" ng-controller="DatepickerCtrl">
                    {!! Form::label('date', 'Date') !!}
                    {!! Form::text('date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('amount', 'Amount') !!}
                    {!! Form::text('amount', NULL, ['class'=>'form-control', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('type_id', 'Additional Commission Type') !!}
                    {!! Form::select('type_id', $advanceCommissionTypes, 1, ['class'=>'form-control', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('repayable', 'Repayable') !!}
                    {!! Form::select('repayable', ['0' => 'Non Repayable', '1' => 'To Be Repayed'], 1, ['class'=>'form-control', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', null, ['class'=>'form-control', 'required']) !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::hidden('holding_id', $holding->id) !!}
                {!! Form::hidden('recipient_id', $commission->recipient_id) !!}
                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
{{--End Additional Commission Modal--}}
