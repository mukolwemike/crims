@extends('layouts.default')

@section('content')
    <div class = "panel-dashboard">

        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>Recipient</th><th>Project</th><th>Client Code</th><th>Client Name</th><th>Unit</th><th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($commissions as $commission)
                <tr>
                    <td>{!! $commission->recipient->name !!}</td>
                    <td>{!! $commission->holding->project->name !!}</td>
                    <td>{!! $commission->holding->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($commission->holding->client_id) !!}</td>
                    <td>{!! $commission->holding->unit->number !!}</td>
                    <td>{!! $commission->amount !!}</td>
                    {{--<td>--}}
                        {{--<a href="#"><i class = "fa fa-pencil-square-o"></i></a>--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection