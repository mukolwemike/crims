@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">

            <div ng-controller="RealEstateCommissionGridController">
                <table st-pipe="callServer" st-table="displayed" class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th colspan="3">
                            <a href="/dashboard/investments/commission/payment-dates/list" class="btn btn-success margin-bottom-20"><i class="fa fa-calendar-check-o"></i> Payment Dates</a>
                        </th>
                        <th colspan="1">
                            <input st-search="month" placeholder="date" type="date" class="form-control" init-model="date">
                        </th>
                        <th colspan="1">
                            <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                        </th>
                    </tr>
                    <tr>
                        <td class="alert alert-info" colspan="100%"> Commission as from <% meta.start | date %> to <% meta.end | date %></td>
                    </tr>
                    <tr>
                        <th st-sort="id">FA ID</th>
                        <th>Name</th>
                        <th st-sort="email">Email</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.name %></td>
                        <td><% row.email %></td>
                        <td><% row.amount | currency:"" %></td>
                        <td>
                            <a ng-controller="PopoverCtrl" uib-popover="Payment Details" popover-trigger="mouseenter" href="/dashboard/realestate/commissions/payment/<%row.id%>?date=<% date | date:'yyyy-M-dd' %>"><i class="fa fa-money"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="7" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td  colspan="2" class="text-center">
                            Items per page
                        </td>
                        <td colspan="2" class="text-center">
                            <input type="text"  ng-model="itemsByPage"/>
                        </td>
                        <td colspan="4" class="text-center">
                            <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@endsection