@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div class="row" ng-controller="RealEstatePaymentDetailController">
            <div class="col-md-12">
                @include('realestate.projects.units.unit_detail_partial', ['unit'=>$holding->unit])
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Payment Details</div>
                    <table class="table table-responsive table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Date</th><th>Description</th><th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{!! $payment->date->toFormattedDateString() !!}</td>
                            <td>{!! $payment->description !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm" ng-controller="PopoverCtrl" popover-trigger="mouseenter" data-toggle="modal" data-target="#editPayment"><i class="fa fa-edit"></i>Edit</button>
                                    <a class="btn btn-danger btn-sm" href="#" ng-click="delete({!! $payment->id!!}, {!! $payment->amount!!})"><i class="fa fa-trash"></i> Delete</a>
                                </div>
                                <div class="modal fade" id="editPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::model($payment,['route'=>['update_realestate_payment', $payment->id], 'method'=>'PUT']) !!}
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Edit Real Estate Payment</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{--<div class="form-group">--}}
                                                    {{--<div class = "form-group" ng-controller = "DatepickerCtrl">--}}
                                                        {{--{!! Form::label('date', 'Payment Date') !!}--}}

                                                        {{--{!! Form::text('date', NULL, ['id'=>'date','class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                <div class="form-group">
                                                    {!! Form::label('payment_type_id', 'Payment Types') !!}

                                                    {!! Form::select('payment_type_id', $realestatePaymentType, null, ['class'=>'form-control', 'id'=>'payment_type_id']) !!}

                                                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'payment_type_id') !!}
                                                </div>

                                                {{--<div class="form-group">--}}
                                                    {{--{!! Form::label('amount', 'Amount') !!}--}}

                                                    {{--{!! Form::number('amount', null, ['class'=>'form-control', 'required']) !!}--}}

                                                    {{--{!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}--}}
                                                {{--</div>--}}

                                                <div class="form-group">
                                                    {!! Form::label('description', 'Narration') !!}

                                                    {!! Form::text('description', null, ['class'=>'form-control', 'required']) !!}

                                                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('schedule_id', 'Payment') !!}

                                                    {!! Form::select('schedule_id', $schedules, null, ['class'=>'form-control', 'id'=>'schedules']) !!}

                                                    {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'payment_type_id') !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                {!! Form::submit('Update Payment', ['class'=>'btn btn-success']) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                       <span>Business Confirmation</span>
                        @if($bc_status)
                            <span class="label label-success pull-right">Sent</span>
                        @else
                            <span class="label label-danger pull-right">Not sent</span>
                        @endif
                    </div>

                    <table class="table table-bordered table-responsive">
                        <tbody>
                            <tr><td>Project</td><td>{!! $project->name !!}</td></tr>
                            <tr><td>Unit price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td></tr>
                            <tr><td>Unit number</td><td>{!! $unit->number !!}</td></tr>
                            @foreach($payments as $p)
                                <tr><td>{!! $p->present()->scheduleName() !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($p->amount) !!}</td></tr>
                            @endforeach
                            <tr><td>Total Amount Paid</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($payments->sum('amount')) !!}</td></tr>
                            <tr><td>Payment Pending</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price() - $inPricingPayments) !!}</td></tr>
                        </tbody>
                    </table>

                    <div class="panel-footer">
                        @if($bc_status)
                            <button class="btn btn-danger" data-toggle="modal" data-target="#confirmBCResend"><i class="fa fa-envelope"></i> Resend Business Confirmation</button>
                            <!-- Modal -->
                            <div class="modal fade" id="confirmBCResend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Send Business Confirmation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>This business confirmation has already been sent. Are you sure that you want to resend it?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <a href="/dashboard/realestate/payments/send-bc/{!! $payment->id !!}" class="btn btn-success"><i class="fa fa-envelope-o"></i> Resend Business Confirmation</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <a href="/dashboard/realestate/payments/send-bc/{!! $payment->id !!}" class="btn btn-success"><i class="fa fa-envelope-o"></i> Send Business Confirmation</a>
                        @endif
                        <a href="/dashboard/realestate/payments/preview-bc/{!! $payment->id !!}" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o"></i> Preview</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Payments</div>
                    <div class="panel-body">Here are the payments made so far...</div>
                    <table class="table table-responsive table-striped table-hover">
                        <thead>
                        <tr><th>Date</th><th>Description</th><th>Payment Type</th><th>Amount</th></tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $payment)
                            <tr>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
                                <td>{!! $payment->description !!}</td>
                                <td>
                                    @if(is_null($payment->schedule))
                                    @else
                                        {!! $payment->type->name !!}
                                    @endif
                                </td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
                            </tr>
                        @endforeach
                            <tr><td>{!! \Cytonn\Presenters\DatePresenter::formatDate('today') !!}</td><th>Total</th><td></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($payments->sum('amount')) !!}</th></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
