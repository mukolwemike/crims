@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div ng-controller = "RealEstatePaymentGridController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                    <tr>
                        <th colspan="4">
                            <a href="/dashboard/realestate/payments-schedules" class="btn btn-success margin-bottom-20">Upcoming Payments</a>
                            <a href="{!! route('get_overdue_payment_schedules') !!}" class="btn btn-success margin-bottom-20">Overdue Payments</a>
                        </th>
                        <th colspan="2"><input st-search="client_code" placeholder="search by client code" class="form-control" type="text"/></th>
                        <th colspan = "2"><input ng-model="search" st-search="" class = "form-control" placeholder = "Search..." type = "text"/></th>
                    </tr>
                    <tr>
                        <th>Client Code</th>
                        <th>Name</th>
                        <th >Project</th>
                        <th>Unit</th>
                        <th st-sort = "amount">Amount</th>
                        <th st-sort = "date">Date</th>
                        <th>Business Confirmation</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody ng-show="!isLoading">
                    <tr ng-repeat = "row in displayed | filter:search">
                        <td><% row.client_code %></td>
                        <td><% row.client_name %></td>
                        <td><% row.project_name | date%></td>
                        <td><% row.unit_number | date%></td>
                        <td><% row.amount | currency:"" %></td>
                        <td><% row.date | date %></td>
                        <td><span to-html="row.confirmation_sent | confirmationStatus"></span></td>
                        <td>
                            <a ng-controller = "PopoverCtrl" uib-popover = "View details" popover-trigger = "mouseenter" href = "/dashboard/realestate/payments/show/<% row.id %>"><i class = "fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="isLoading">
                    <tr><td colspan="100%">Loading...</td></tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="100%" class="text-center">
                            <dmc-pagination></dmc-pagination>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
@endsection