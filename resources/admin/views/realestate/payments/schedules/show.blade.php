@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard" ng-controller="RealEstateScheduledPaymentController">

            @include('realestate.projects.units.unit_detail_partial', ['unit'=>$holding->unit])

            <table class="table table-responsive table-striped table-hover">
                <thead>
                <tr>
                    <th>Remove</th>
                    <th>Date</th>
                    <th>Payment Type</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Charge Penalty</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($schedules->sortBy('date') as $schedule)
                    <tr>
                        <td>{!! Form::checkbox('items[]', null, false, ['ng-model'=>'items['.$schedule->id.']']) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                        <td>{!! $schedule->type->name !!}</td>
                        <td>{!! $schedule->description !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount)  !!}</td>
                        <td>{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($schedule->penalty) !!}</td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                    data-target="#deleteModal{!! $schedule->id !!}">
                                <i class="fa fa-remove"></i>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal{!! $schedule->id !!}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        {!! Form::open(['route'=>['remove_realestate_payment_schedule', $schedule->id], 'method'=>'DELETE']) !!}
                                        <div class="modal-body">
                                            Are you sure you want to delete this payment schedule?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#editModal{!! $schedule->id !!}">
                                <i class="fa fa-pencil-square-o"></i>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editModal{!! $schedule->id !!}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header"><h4>Edit Payment Schedule</h4></div>
                                        {!! Form::model($schedule,['route'=>['update_realestate_payment_schedule', $schedule->id], 'method'=>'PUT']) !!}
                                        <div class="modal-body" data-ng-controller="RealEstateScheduleEditController"
                                             ng-init="setPenaltyChargeStatus({!! $schedule->penalty !!})">
                                            <div class="form-group">
                                                {!! Form::hidden('schedule_id', $schedule->id) !!}
                                                {!! Form::label('description', 'Description') !!}
                                                {!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>'2', 'ng-required'=>"true"]) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('payment_type_id', 'Payment Type') !!}
                                                {!! Form::select('payment_type_id', $paymentTypes->lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'payment_type_id']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'payment_type_id') !!}
                                            </div>
                                            <div class="form-group" ng-controller="DatepickerCtrl">
                                                {!! Form::label('date', 'Date') !!}
                                                {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('amount', 'Amount') !!}
                                                {!! Form::number('amount', null, ['class'=>'form-control', 'ng-required'=>"true"]) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::checkbox('penalty', true, null, ['ng-model' => 'penaltyCharge', 'ng-change' => 'changePenaltyChargeStatus()']) !!}
                                                {!! Form::label('penalty', 'Charge penalty') !!}
                                                {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'penalty') !!}
                                            </div>

                                            <div class="form-group" ng-controller="DatepickerCtrl"
                                                 ng-show="! penaltyCharge">
                                                {!! Form::label('interest_date', 'Interest Start Date') !!}
                                                {!! Form::text('interest_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"interest_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_date') !!}
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success">Update</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th></th>
                    <th>Total</th>
                    <td></td>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="7">
                        {!! Form::open(['route'=>['bulk_delete_payment_schedules', $holding->id], 'method'=>'DELETE']) !!}
                        <div id="delete-items"></div>
                        {!! Form::submit('Delete selected', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn-group">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#scheduleCollapse"
                        aria-expanded="false" aria-controls="scheduleCollapse">
                    Add Payment Schedule
                </button>
                <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#typesCollapse"
                        aria-expanded="false" aria-controls="typesCollapse">
                    Payment Types
                </button>
            </div>

            <div class="collapse" id="scheduleCollapse">
                <h4>Add items to the schedule</h4>
                {!! Form::open(['route'=>['save_realestate_payment_schedule', $holding->id]]) !!}

                <div>
                    <div class="form-group">
                        <div class="col-md-12 margin-bottom-20">
                            <div class="col-md-6 checkbox">
                                <label><input type="checkbox" value="" ng-model="recurring"> Recurring</label>
                            </div>
                            <div class="col-md-6" ng-hide="recurring">
                                @if(is_null($holding->paymentPlan))
                                    <p>The payment plan for this holding has not been defined. Please add the payment
                                        plan to be able to generate the payment schedules.</p>
                                @else
                                    <div class="form-group form-inline">
                                        @if($holding->paymentPlan->slug == 'installment' or $holding->paymentPlan->slug == 'zero_deposit_installment')
                                            <label class="margin-top-25"><span>Spacing <small>(in months)</small></span>:
                                                <input type="number" class="form-control" ng-model="spacing"></label>
                                        @endif
                                        <a href="#" class="btn btn-primary pull-right margin-top-25"
                                           ng-click="generate({!! $holding->id!!})">Generate</a>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12" ng-show="recurring">
                                <div class="col-md-3" ng-controller="DatepickerCtrl">
                                    <label>Start Date</label>
                                    <input type="text" class="form-control" id="start_date" datepicker-popup=""
                                           ng-model="custom.start_date" is-open="status.opened"
                                           ng-focus="open($event)"/>
                                </div>
                                <div class="col-md-3" ng-controller="DatepickerCtrl">
                                    <label>End Date</label>
                                    <input type="text" class="form-control" id="end_date" datepicker-popup=""
                                           ng-model="custom.end_date" is-open="status.opened" ng-focus="open($event)"/>
                                </div>
                                <div class="col-md-3">
                                    <label>Date</label>
                                    <input type="number" class="form-control" id="date_of_month"
                                           ng-model="custom.date_of_month"/>
                                </div>
                                <div class="col-md-3">
                                    <label>Amount</label>
                                    <input type="number" class="form-control" id="amount" ng-model="custom.amount"/>
                                </div>
                                <div class="col-md-3 margin-top-25">
                                    <a href="#" class="btn btn-primary" ng-click="generateSchedules()">Generate</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 bold margin-bottom-20">
                            <div class="col-md-3">Description</div>
                            <div class="col-md-3">Payment Type</div>
                            <div class="col-md-3">Date</div>
                            <div class="col-md-3">Amount</div>
                        </div>
                        <div class="col-md-12" data-ng-repeat="schedule in schedules">
                            <div class="col-md-3">{!! Form::textarea('description[]', null, ['class'=>'form-control', 'rows'=>'2', 'ng-required'=>"true", 'ng-model'=>'schedule.description']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}</div>
                            <div class="col-md-3">{!! Form::select('payment_type_id[]', $paymentTypes->lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'payment_type_id', 'ng-model'=>'schedule.payment_type_id']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'payment_type_id') !!}</div>
                            <div class="col-md-2"
                                 ng-controller="DatepickerCtrl">{!! Form::text('date[]', null, ['class'=>'form-control', 'datepicker-popup ng-model'=>"schedule.date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}</div>
                            <div class="col-md-3">{!! Form::text('amount[]', null, ['class'=>'form-control', 'ng-required'=>"true", 'ng-model'=>'schedule.amount']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-danger" ng-show="$last"
                                        ng-click="removeSchedule()"><i
                                            class="fa fa-remove"></i></button>
                            </div>

                        </div>
                        <button type="button" class="btn btn-primary pull-right" ng-click="addNewSchedule()"><i
                                    class="fa fa-plus-circle"> Add Schedule</i></button>
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-success" data-toggle="modal"
                                data-target="#payment-schedule-confirmation-modal">Save
                        </button>

                        <div class="modal fade" id="payment-schedule-confirmation-modal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Confirm Payment Schedule(s)</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="detail-group">
                                            <p>Are you sure you want to save the payment schedule(s) below?</p>
                                            <table class="table table-hover table-responsive table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Payment Type</th>
                                                    <th>Date</th>
                                                    <th>Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="schedule in schedules">
                                                    <td><% schedule.description %></td>
                                                    <td><% getPaymentType(schedule.payment_type_id) %></td>
                                                    <td><% schedule.date | date %></td>
                                                    <td><% schedule.amount | currency:"" %></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <div class="form-group">
                                            {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <hr style="border: 1px solid #066;">
            </div>
            <br>
            <div class="collapse" id="typesCollapse">
                <button type="button" class="btn btn-info pull-right margin-top-25"
                        style="border-radius: 0" data-toggle="modal"
                        data-target="#add-payment-type-modal"><i class="fa fa-plus"></i> Payment
                    Type
                </button>

                <table class="table table-responsive table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Accrues Interest?</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paymentTypes as $type)
                        <tr>
                            <td>{{$type->name}}</td>
                            <td>{{$type->slug}}</td>
                            <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($type->accrues_interest) !!}</td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#deleteTypeModal{!! $type->id !!}">
                                    <i class="fa fa-remove"></i>
                                </button>

                                <!-- Delete Modal -->
                                <div class="modal fade" id="deleteTypeModal{!! $type->id !!}" tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route'=>['remove_realestate_payment_type', $holding->id, $type->id], 'method'=>'DELETE']) !!}
                                            <div class="modal-body">
                                                Are you sure you want to delete this payment type?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editTypeModal{!! $type->id !!}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </button>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="editTypeModal{!! $type->id !!}" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header"><h4>Edit Payment Type</h4></div>
                                            {!! Form::model($type,['route'=>['update_realestate_payment_type', $holding->id, $type->id], 'method'=>'PUT']) !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('name', 'Name') !!}
                                                    {!! Form::text('name', null, ['class'=>'form-control','ng-required'=>"true"]) !!}
                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('slug', 'Slug') !!}
                                                    {!! Form::text('slug', null, ['class'=>'form-control','ng-required'=>"true"]) !!}
                                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'slug') !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::hidden('accrues_interest', 0) !!}
                                                    {!! Form::checkbox('accrues_interest', 1 , null) !!} {!! Form::label('accrues_interest', 'Accrues Interest') !!}
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-success">Update</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                {{--add payment type modal--}}
                <div class="modal fade" id="add-payment-type-modal" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add Payment Type</h4>
                            </div>
                            {!! Form::open(['route'=>['add_realestate_payment_type', $holding->id]]) !!}
                            <div class="modal-body">
                                <div class="form-group">
                                    {!! Form::label('name', 'Payment Name') !!}
                                    {!! Form::text('name', null, ['class'=>'form-control','ng-required'=>"true"]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('accrues_interest', 0) !!}
                                    {!! Form::checkbox('accrues_interest', 1, null) !!} {!! Form::label('accrues_interest', 'Accrues Interest') !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
