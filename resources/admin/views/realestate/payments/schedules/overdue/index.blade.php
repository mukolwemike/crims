@extends('layouts.default')

@section('content')
    <div class="panel-dashboard">
        <div id="exportable" ng-controller="RealEstateOverduePaymentsController">
            <table st-pipe="callServer" st-table="displayed" class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan="3">
                        <div class="btn-group">
                            <a href="/dashboard/realestate/payments" class="btn btn-default margin-bottom-20"><i class="fa fa-arrow-left"></i> Back to Payments</a>
                            <button type="button" class="btn btn-primary margin-bottom-20" data-toggle="modal" data-target="#overdue_payments_bulk_reminders"><i class="fa fa-envelope-o"></i> Payment Reminders</button>
                        </div>
                    </th>
                    <th colspan="1">
{{--                        {!! Form::select('project_id', [null=>'All projects'] + Project::lists('name', 'id'), null, ['class'=>'form-control', 'st-search'=>'project_id']) !!}--}}
                    </th>
                    <th colspan="2"></th>
                    <th colspan="2">
                        <input st-search placeholder="Search..." class="form-control" type="search" />
                    </th>
                    <th>
                        {!! Form::open(['route'=>'export_overdue_payments']) !!}
                        {!! Form::button('<i class="fa fa-file-excel-o"></i> Export Overdues', ['class'=>'btn btn-success margin-bottom-10', 'type'=>'submit']) !!}
                        {!! Form::close() !!}
                    </th>
                </tr>
                <tr>
                    <th>Client Code</th>
                    <th>Name</th>
                    <th >Project</th>
                    <th>Unit</th>
                    <th st-sort = "amount">Amount Overdue</th>
                    <th st-sort = "amount">Interest</th>
                    <th st-sort = "date">Date</th>
                    <th>Description</th>
                    <th>Payment Type</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody ng-show="!isLoading">
                <tr ng-repeat = "row in displayed | filter:client_name">
                    <td><% row.client_code %></td>
                    <td><% row.client_name %></td>
                    <td><% row.project_name | date%></td>
                    <td><% row.unit_number | date%></td>
                    <td><% row.amount_overdue | currency:"" %></td>
                    <td><% row.interest_overdue | currency:"" %></td>
                    <td><% row.date | date %></td>
                    <td><% row.description | date %></td>
                    <td><% row.paymentType | date %></td>
                    <td to-html="row.paid | asLabel:'Paid':'Not Paid'"></td>
                </tr>
                </tbody>
                <tbody ng-show="isLoading">
                <tr><td colspan="100%">Loading...</td></tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="100%" class="text-center">
                        <dmc-pagination></dmc-pagination>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@endsection

<div class="modal fade" id="overdue_payments_bulk_reminders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['send_bulk_overdue_payment_reminders']]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send Upcoming Payments for Bulk Approval</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class = "form-group clearfix"  ng-controller = "DatepickerCtrl">
                            {!! Form::label('start_date', 'Start Date') !!}
                            {!! Form::text('start_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"start_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'start_date') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class = "form-group clearfix"  ng-controller = "DatepickerCtrl">
                            {!! Form::label('end_date', 'End Date') !!}
                            {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'end_date') !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        {!! Form::label('project_id', 'Select Projects') !!}

                        {!! Form::select('project_id',  $projects, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Send for Approval', ['class'=>'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>