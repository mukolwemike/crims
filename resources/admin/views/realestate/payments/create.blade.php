@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            @include('realestate.projects.units.unit_detail_partial')


            <div class="" ng-controller="RealEstatePaymentController" data-ng-init="currentRecipient = '{!! $currentRecipient !!}'; client_id = '{!! $holding->client_id !!}'; project_id = '{!! $holding->unit->project_id !!}'">
                {!! Form::open(['name'=>'form', 'ng-submit'=>'submit($event)']) !!}
                <div class="col-md-6">
                    <h5>Payment details</h5>
                    <hr>
                    <div class="form-group">
                        {!! Form::label('date', 'Payment Date') !!}

                        {!! Form::text('date', null, ['class'=>'form-control', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>"entered_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true", 'id'=>'entered_date']) !!}

                        {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('amount', 'Amount') !!} <small>(Current payments balance : {!! \Cytonn\Presenters\AmountPresenter::currency($payments_balance) !!})</small>

                        {!! Form::text('amount', null, ['class'=>'form-control', 'init-model'=>'amount', 'required']) !!}

                        {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Narration') !!}

                        {!! Form::text('description', null, ['class'=>'form-control', 'init-model'=>'description', 'required']) !!}

                        {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'description') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('schedule_id', 'Payment') !!}

                        {!! Form::select('schedule_id', $schedules, null, ['class'=>'form-control', 'id'=>'schedules', 'init-model'=>'schedule_id']) !!}

                        {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'schedule_id') !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <h5>Purchase Information</h5>
                    <hr/>
                    @if(!$holding->client->client_code)
                        <div class="form-group">
                            {!! Form::label('client_code', 'Client code') !!}

                            {!! Form::text('client_code', null, ['class'=>'form-control', 'init-model'=>'client_code']) !!}
                        </div>
                    @endif

                    @if($schedule)
                        <div ng-controller="TranchePricingController">
                            <div class="form-group">
                                <label>Payment Plan</label>
                                <select ng-model="tranche.payment_plan_id"
                                        ng-options="payment_plan.id as payment_plan.name for payment_plan in payment_plans track by payment_plan.id"
                                        ng-change="getTranchePricingsForSelectedPaymentPlan()"
                                        class="form-control"
                                        ng-required="true"
                                        id="payment_plan_id"
                                        name="payment_plan_id">
                                    <option value="">--- Select Payment Plan ---</option>
                                    <%  %>
                                </select>
                                {{--{!! Form::select('payment_plan_id', $payment_plans, null, ['class'=>'form-control', 'init-model'=>'payment_plan_id', 'id'=>'payment_plan_id']) !!}--}}
                            </div>
                            <div class="form-group">
                                <label>Tranche</label>
                                <select ng-model="tranche.id"
                                        ng-options="selected.tranche_id as selected.name_and_price for selected in tranche_pricing_for_selected_payment_plan track by selected.tranche_id"
                                        class="form-control"
                                        ng-required="true"
                                        id="tranche_id"
                                        name="tranche_id">
                                    <option value="">-- Select Tranche --</option>
                                </select>

                                {{--{!! Form::select('tranche_id', $tranches, null, ['class'=>'form-control', 'init-model'=>'tranche_id', 'id'=>'tranche_id']) !!}--}}
                                {{--{!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'tranche_id') !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('recipient_id', 'Select FA') !!}

                            {!! Form::select('recipient_id', $recipients, $currentRecipient, ['required','class'=>'form-control', 'ui-select2', 'init-model'=>'recipient_id', 'data-placeholder'=>"Select the FA", 'id'=>'recipients_id'] ) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('awarded', 'Award Commission?') !!}
                            {!! Form::select('awarded', [1=>'Yes', 0=>'No'], null, ['class'=>'form-control', 'init-model'=>'awarded', 'id'=>'awarded']) !!}
                        </div>

                        @if((bool) $holding->payments()->count() == 0)
                            <div class="form-group alert alert-info" >
                                {!! Form::label('commission_rate', 'Commission Rate') !!} :- {!! Form::label('commission_rate', '<% commissionRateName %>') !!}
                                <br />
                            </div>

                            <div class = "form-group" ng-show="edit_rate && awarded == 1">
                                <br />
                                {!! Form::label('commission_rate', 'Commission Rate') !!}
                                {!! Form::number('commission_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'data-ng-value' => 'commission_rate', 'required', 'ng-model' => 'commission_rate']) !!}
                                {!! Form::hidden('commission_rate_name', NULL, [ 'data-ng-value' => 'commissionRateName', 'ng-model' => 'commissionRateName']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                            </div>

                            <div class="form-group" ng-show="edit_rate && awarded == 1">
                                {!! Form::label('reason', 'Rate Reason') !!}
                                {!! Form::textarea('reason', NULL, ['class'=>'form-control', 'rows' => '3',  'ng-model' => 'reason']) !!}
                                {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                            </div>

                            <div class="form-group" ng-show="awarded == 1">
                                <br />
                                {!! Form::checkbox('edit', true, true, ['ng-model'=>'edit_rate']) !!} {!! Form::label('edit', "Edit Commission Rate") !!}
                                <br />
                            </div>


                            <div class="form-group">
                                {!! Form::label('discount', 'Discount %') !!}
                                {!! Form::number('discount', 0, ['class'=>'form-control', 'init-model'=>'discount', 'step'=>'0.01']) !!}
                            </div>
                        @endif

                        <a class="btn btn-success" ng-click="add_negotiated_price = !add_negotiated_price">Add a negotiated price</a>
                        <br><br>

                        <div class="form-group" ng-show="add_negotiated_price">
                            {!! Form::label('negotiated_price', 'Negotiated price') !!}

                            {!! Form::text('negotiated_price', null, ['class'=>'form-control', 'init-model'=>'negotiated_price']) !!}
                            {!! Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'negotiated_price') !!}
                        </div>
                    @else
                        <div class="alert">
                            <p>All required information has been entered with previous payments</p>
                        </div>
                    @endif
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div ng-view></div>
    <script type = "text/ng-template" id = "re_payment.htm">
        <div class = "modal fade">
            <div class = "modal-dialog">
                <div class = "modal-content">

                    <div class = "modal-header no-bottom-border">
                        <button type = "button" class = "close margin-bottom-20" ng-click = "close(false)"
                                data-dismiss = "modal" aria-hidden = "true">&times;</button>
                        <h4 class = "modal-title">Confirm Payment</h4>
                    </div>
                    <div class = "modal-body">

                        <div class = "detail-group">
                            <p>Are you sure you want to make the payment below?</p>
                            <table class = "table table-hover table-responsive table-striped">
                                <tbody>
                                <tr>
                                    <td>Date</td><td><% entered_date | date %></td>
                                </tr>
                                <tr>
                                    <td>Amount</td><td><% amount | currency:""%></td>
                                </tr>
                                <tr>
                                    <td>Description</td><td><% description %></td>
                                </tr>
                                <tr>
                                    <td>Payment</td><td><% schedule %></td>
                                </tr>
                                <tr ng-if="client_code">
                                    <td>Client code</td><td><% client_code %></td>
                                </tr>

                                <tr ng-if="discount > 0">
                                    <td>Discount</td><td><% discount %>%</td>
                                </tr>
                                @if($schedule)
                                    <tr>
                                        <td>Tranche</td><td><% selected_tranche %></td>
                                    </tr>
                                    <tr>
                                        <td>Payment Plan</td><td><% selected_payment_plan %></td>
                                    </tr>
                                    <tr>
                                        <td>FA</td><td><% selected_recipient %></td>
                                    </tr>
                                    <tr>
                                        <td>Award Commission?</td><td><% selected_awarded %></td>
                                    </tr>
                                    <tr>
                                        <td>Commission rate</td><td><% c_rate %></td>
                                    </tr>
                                    <tr>
                                        <td>Reason</td><td><% reason %></td>
                                    </tr>
                                    <tr ng-if="negotiated_price">
                                        <td>Negotiated price</td><td><% negotiated_price %></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class = "detail-group">
                            <div class = "pull-right">
                                {!! Form::open(['route'=>['save_realestate_payment', $holding->id]]) !!}
                                <div style = "display: none !important;">
                                    {!! Form::text('date', NULL, ['ng-model'=>'entered_date']) !!}
                                    {!! Form::text('amount', NULL, ['ng-model'=>'amount']) !!}
                                    {!! Form::text('description', NULL, ['ng-model'=>'description']) !!}
                                    {!! Form::text('schedule_id', NULL, ['ng-model'=>'schedule_id']) !!}
                                    @if($schedule)
                                        {!! Form::text('tranche_id', NULL, ['ng-model'=>'tranche_id']) !!}
                                        {!! Form::text('negotiated_price', NULL, ['ng-model'=>'negotiated_price']) !!}
                                        {!! Form::text('payment_plan_id', NULL, ['ng-model'=>'payment_plan_id']) !!}
                                        {!! Form::text('recipient_id', NULL, ['ng-model'=>'recipient_id']) !!}
                                        {!! Form::text('awarded', NULL, ['ng-model'=>'awarded']) !!}
                                        {!! Form::text('discount', NULL, ['ng-model'=>'discount']) !!}
                                        {!! Form::text('rate', NULL, ['ng-model'=>'commission_rate']) !!}
                                        {!! Form::text('commission_rate_name', NULL, ['ng-model'=>'commissionRateName']) !!}
                                        {!! Form::text('reason', NULL, ['ng-model'=>'reason']) !!}
                                        {!! Form::text('client_code', NULL, ['ng-model'=>'client_code']) !!}
                                    @endif
                                </div>
                                <button type = "button" ng-click = "close(false)" class = "btn btn-default"
                                        data-dismiss = "modal">No
                                </button>
                                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
@endsection