<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('DEFAULT_FILESYSTEM', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path(''),
        ],
        'root_local' => [
            'driver' => 'local',
            'root' => storage_path(''),
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'fortrabbit' => [
            'driver' => 's3',
            'key' => env('AWS_KEY', fortrabbitSecret('OBJECT_STORAGE.KEY')),
            'secret' => env('AWS_SECRET', fortrabbitSecret('OBJECT_STORAGE.SECRET')),
            'region' => env('AWS_REGION', fortrabbitSecret('OBJECT_STORAGE.REGION')),
            'bucket' => env('AWS_BUCKET', fortrabbitSecret('OBJECT_STORAGE.BUCKET')),
            'version' => 'latest',
            'endpoint' => 'https://objects.us1.frbit.com'
        ],
        'backup' => [
            'driver' => 's3',
            'key'    => 'AKIAI5BQERQVFVHBFLYQ',
            'secret' => fortrabbitSecret('CUSTOM.AWS_S3_SECRET'),
            'region' => 'us-west-2',
            'bucket' => 'cytonn-backups',
            'version' => 'latest',
            'root'   => 'crims',
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY', fortrabbitSecret('CUSTOM.STATEMENT_STORAGE_KEY')),
            'secret' => env('AWS_SECRET', fortrabbitSecret('CUSTOM.STATEMENT_STORAGE_SECRET')),
            'region' => 'us-west-2',
            'bucket' => 'cytonn-crims',
            'version' => 'latest',
        ],
        's3_local' => [
            'driver' => 'local',
            'root' => storage_path('statement'),
        ],

    ],

];
