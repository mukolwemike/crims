<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'username' => 'jgnasser@gmail.com',
        'secret' => fortrabbitSecret('CUSTOM.MANDRILL_KEY'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model'  => 'User',
        'secret' => '',
    ],

    'rollbar' => [
        'access_token' => env('ROLLBAR_TOKEN', ''),//@cytonn 070d0474effa4a4d8c70800547a7a4fe
        'environment' => env('APP_ENV'),
        'root' => base_path()
    ],

    'currencylayer' => [
        'key' => 'eb59d63a9832a10f8f1c1b06d780e7f3'
    ],

    'authy' => [
        'key' => env('AUTHY_KEY')
    ],

    'cytonn_payments' => [
        'url' => env('CYTONN_PAYMENTS_URL', ''),
        'submit_payments' => 'api/payments/submit',
        'client_id' => env('CYTONN_PAYMENTS_CLIENT_ID', ''),
        'client_secret' => env('CYTONN_PAYMENTS_CLIENT_SECRET', '')
    ],
    'iprs' => [
        'wsdl' => 'http://10.1.1.5:9004/IPRSServerwcf?wsdl',
        'username' => 'CYTONN',
        'password' => env('IPRS_PASSWORD', '')
    ]

];
