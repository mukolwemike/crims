<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Logging Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use for logging connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */


    'logging' => env('LOG_CONNECTION', env('DB_CONNECTION')),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => __DIR__.'/../../database/production.sqlite',
            'prefix'   => '',
        ],

        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', fortrabbitSecret('MYSQL.HOST')),
            'database'  => env('DB_DATABASE', fortrabbitSecret('MYSQL.DATABASE')),
            'username'  => env('DB_USERNAME', fortrabbitSecret('MYSQL.USER')),
            'password'  => env('DB_PASSWORD', fortrabbitSecret('MYSQL.PASSWORD')),
            'port'      => env('DB_PORT', '3306'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',


            // Here is the time zone setting
//            'options'   => [
//                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET time_zone = \'+03:00\''
//            ]
        ],
        'logging' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', fortrabbitSecret('MYSQL.HOST')),
            'database'  => env('DB_DATABASE_LOG', fortrabbitSecret('MYSQL.DATABASE')),
            'username'  => env('DB_USERNAME', fortrabbitSecret('MYSQL.USER')),
            'password'  => env('DB_PASSWORD', fortrabbitSecret('MYSQL.PASSWORD')),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',


            // Here is the time zone setting
//            'options'   => [
//                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET time_zone = \'+03:00\''
//            ]
        ],
        'cassandra' => [
            'driver' => 'cassandra',
            'host' => env('CASSANDRA_HOST', 'localhost'),
            'port' => env('CASSANDRA_PORT', 9042),
            'keyspace' => env('CASSANDRA_DATABASE', 'cassandra_db'),
            'username' => env('CASSANDRA_USERNAME', ''),
            'password' => env('CASSANDRA_PASSWORD', ''),
            'consistency'     => 1,
        ],

        'pgsql' => [
            'driver'   => 'pgsql',
            'host'     => 'localhost',
            'database' => 'forge',
            'username' => 'forge',
            'password' => '',
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public',
        ],

        'sqlsrv' => [
            'driver'   => 'sqlsrv',
            'host'     => 'localhost',
            'database' => 'database',
            'username' => 'root',
            'password' => '',
            'prefix'   => '',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [
        'client' => 'predis',

        'cluster' => false,

        'default' => [
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => 6379,
            'database' => env('REDIS_DATABASE', "0"),
        ],

    ],

];
