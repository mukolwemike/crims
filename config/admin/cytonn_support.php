<?php
/**
 * Created by PhpStorm.
 * User: donyino
 * Date: 20/06/2018
 * Time: 3:25 PM
 */

return [
    /**
     * |
     * |--------------------------------
     * |Configuration for sending SMS
     * |--------------------------------
     * |
     */
    'sms' => [
        //supported: nanodigital, africastalking, log
        'default' => env('SMS_DRIVER', 'log'),

//            'nanodigital' => [
//                'api_key' => env('NANO_DIGITAL_API_KEY', '')
//            ],

        'africastalking' => [
            'username' => env('AFRICASTALKING_USERNAME', ''),
            'api_key' => env('AFRICASTALKING_API_KEY'),
            'sender_id' => env('AFRICASTALKING_SENDER_ID', 'CYTONN')
        ]
    ]


];