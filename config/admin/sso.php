<?php
/**
 * Date: 27/08/2017
 * Time: 02:15
 * @author Timothy Kimathi <timsnky@gmail.com>
 * Project: sso
 */

return [
    /*
    |--------------------------------------------------------------------------
    | SSO Configuration
    |--------------------------------------------------------------------------
    |
    | This file is for storing the configuration for use by sso manager
    |
    */

    'sso' => [
        'client_id' => getenv('SSO_CLIENT_ID'),
        'client_secret' => getenv('SSO_CLIENT_SECRET'),
        'redirect' => getenv('SSO_REDIRECT_URL'),
        'authorize' => getenv('SSO_DOMAIN') . '/oauth/authorize',
        'token' => getenv('SSO_DOMAIN') . '/oauth/token',
        'user' => getenv('SSO_DOMAIN') . '/api/user',
        'logout' => getenv('SSO_DOMAIN') . '/api/logout',
        'grant_access' => getenv('SSO_DOMAIN') . '/api/grant_access',
        'revoke_access' => getenv('SSO_DOMAIN') . '/api/revoke_access',
        'sync_users' => getenv('SSO_DOMAIN') . '/api/sync_users',
    ],
];