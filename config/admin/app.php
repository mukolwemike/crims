<?php

use Illuminate\Hashing\BcryptHasher;

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */

    'name' => 'CRIMS',

    'env' => env('APP_ENV', 'production'),


    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('DOMAIN', 'http://crims-admin.cytonn.net'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Africa/Nairobi',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'base64:o0QoK3hLizL+LDzML62zvo+vF71wuQfK5j9FRPZUbL4='),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        'Illuminate\Auth\AuthServiceProvider',
        'Illuminate\Cache\CacheServiceProvider',
        'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
        'Illuminate\Cookie\CookieServiceProvider',
        'Illuminate\Database\DatabaseServiceProvider',
        'Illuminate\Encryption\EncryptionServiceProvider',
        'Illuminate\Filesystem\FilesystemServiceProvider',
        'Illuminate\Hashing\HashServiceProvider',
        'Illuminate\Mail\MailServiceProvider',
        Illuminate\Notifications\NotificationServiceProvider::class,
        'Illuminate\Pagination\PaginationServiceProvider',
        'Illuminate\Queue\QueueServiceProvider',
        'Illuminate\Redis\RedisServiceProvider',
        'Illuminate\Session\SessionServiceProvider',
        Laravel\Tinker\TinkerServiceProvider::class,
        'Illuminate\Translation\TranslationServiceProvider',
        'Illuminate\Validation\ValidationServiceProvider',
        'Illuminate\View\ViewServiceProvider',
        Way\Generators\GeneratorsServiceProvider::class,
        Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class,
//        'Laracasts\Commander\CommanderServiceProvider',
        'Cytonn\Providers\EventServiceProvider',
        'Cytonn\Providers\NotificationServiceProvider',
        'Cytonn\Providers\LogServiceProvider',
        \Cytonn\Providers\PdfServiceProvider::class,
        Schuppo\PasswordStrength\PasswordStrengthServiceProvider::class,
        Cytonn\Providers\StorageProvider::class,
        BackupManager\Laravel\Laravel5ServiceProvider::class,
        \Cytonn\Providers\FundManagerServiceProvider::class,
        \Cytonn\Providers\BladeServiceProvider::class,
        \Cytonn\Providers\Bootstrap::class,
        \Crims\Providers\CrimsServiceProvider::class,
        'Illuminate\Bus\BusServiceProvider',
        'Illuminate\Foundation\Providers\FoundationServiceProvider',
        'Illuminate\Pipeline\PipelineServiceProvider',
        'Illuminate\Auth\Passwords\PasswordResetServiceProvider',
        'Collective\Html\HtmlServiceProvider',
        App\Providers\AppServiceProvider::class,
        App\Providers\BroadcastServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,
        Fedeisas\LaravelMailCssInliner\LaravelMailCssInlinerServiceProvider::class,
        \Bschmitt\Amqp\AmqpServiceProvider::class,
        \CytonnSB\ServiceBusProvider::class,
        \App\Providers\DataProtectionServiceProvider::class,
        ConsoleTVs\Charts\ChartsServiceProvider::class,
        \SSOManager\SSOManagerProvider::class,
        \Fideloper\Proxy\TrustedProxyServiceProvider::class,
        \Cytonn\Support\ServiceProvider::class,
        Dionera\BeanstalkdUI\BeanstalkdUIServiceProvider::class,
        Laravel\Passport\PassportServiceProvider::class,
        LaravelFCM\FCMServiceProvider::class,
        \Cytonn\Cassandra\CassandraServiceProvider::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Service Provider Manifest
    |--------------------------------------------------------------------------
    |
    | The service provider manifest is used by Laravel to lazy load service
    | providers which are not needed for each request, as well to keep a
    | list of all of the services. Here, you may set its storage spot.
    |
    */

    'manifest' => storage_path().'/meta',

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'             => 'Illuminate\Support\Facades\App',
        'Artisan'         => 'Illuminate\Support\Facades\Artisan',
        'Auth'            => 'Illuminate\Support\Facades\Auth',
        'Blade'           => 'Illuminate\Support\Facades\Blade',
        'Broadcast'       => Illuminate\Support\Facades\Broadcast::class,
        'Bus'             => Illuminate\Support\Facades\Bus::class,
        'Cache'           => 'Illuminate\Support\Facades\Cache',
        'Config'          => 'Illuminate\Support\Facades\Config',
        'Cookie'          => 'Illuminate\Support\Facades\Cookie',
        'Crypt'           => 'Illuminate\Support\Facades\Crypt',
        'DB'              => 'Illuminate\Support\Facades\DB',
        'Eloquent'        => 'Illuminate\Database\Eloquent\Model',
        'Event'           => 'Illuminate\Support\Facades\Event',
        'File'            => 'Illuminate\Support\Facades\File',
        'Hash'            => 'Illuminate\Support\Facades\Hash',
        'Lang'            => 'Illuminate\Support\Facades\Lang',
        'Log'             => 'Illuminate\Support\Facades\Log',
        'Mail'            => 'Illuminate\Support\Facades\Mail',
        'Notification'    => Illuminate\Support\Facades\Notification::class,
        'Password'        => 'Illuminate\Support\Facades\Password',
        'Queue'           => 'Illuminate\Support\Facades\Queue',
        'Redirect'        => 'Illuminate\Support\Facades\Redirect',
        'Redis'           => 'Illuminate\Support\Facades\Redis',
        'Request'         => 'Illuminate\Support\Facades\Request',
        'Response'        => 'Illuminate\Support\Facades\Response',
        'Route'           => 'Illuminate\Support\Facades\Route',
        'Schema'          => 'Illuminate\Support\Facades\Schema',
        'Session'         => 'Illuminate\Support\Facades\Session',
        'URL'             => 'Illuminate\Support\Facades\URL',
        'Validator'       => 'Illuminate\Support\Facades\Validator',
        'View'            => 'Illuminate\Support\Facades\View',
        'Flash'           => 'Cytonn\Notifier\FlashNotifier',
        'App\Http\Controllers\Flash' => 'Cytonn\Notifier\FlashNotifier',
        'Model'           => 'Way\Database\Model',
        'PDF'             => 'Barryvdh\DomPDF\Facade',
        'Excel'           => 'Maatwebsite\Excel\Facades\Excel',
        'Number'          => \Cytonn\Core\DataStructures\Number::class,
        'Storage'         => 'Illuminate\Support\Facades\Storage',
        'Form'            => 'Collective\Html\FormFacade',
        'Html'            => 'Collective\Html\HtmlFacade',
        'FORM'            => 'Collective\Html\FormFacade',
        'HTML'            => 'Collective\Html\HtmlFacade',
        'Amqp'            => 'Bschmitt\Amqp\Facades\Amqp',
        'Charts'          => ConsoleTVs\Charts\Facades\Charts::class,
        'SSO'             => SSOManager\Facades\SSO::class,
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        'FCM'      => LaravelFCM\Facades\FCM::class,
        'FCMGroup' => LaravelFCM\Facades\FCMGroup::class,
        'SMS' => Cytonn\Support\Notifiers\SMS\SMS::class
    ],

];
