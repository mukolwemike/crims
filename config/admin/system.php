<?php
/**
 * Date: 28/04/2017
 * Time: 09:21
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

return [
    'administrators' => [
        'emukiri@cytonn.com',
        'molukaka@cytonn.com',
        'ikipngeno@cytonn.com',
    ],
    'emails' => [
        'super_admins' => [
            'emukiri@cytonn.com',
            'molukaka@cytonn.com',
            'ikipngeno@cytonn.com',
        ],
        'administrators' => [
            'emukiri@cytonn.com',
            'molukaka@cytonn.com',
            'ikipngeno@cytonn.com',
        ],
        'operations' => [
            'jmwanjama@cytonn.com',
            'sodindo@cytonn.com',
            'kduncan@cytonn.com',
            'lalicia@cytonn.com',
            'jkkamau@cytonn.com',
            'gadenge@cytonn.com'
        ],
        'operations_admins' => [
            'jmwanjama@cytonn.com'
        ],
        'operations_mgt' => [
            'gweru@cytonn.com',
            'jmwanjama@cytonn.com'
        ],
        'compliance' => [
            'kmuchiri@cytonn.com',
            'dkingoo@cytonn.com'
        ],
        'statements' => [
            'statements@cytonn.com'
        ],
        'fintech' => [
            'fintechalerts@cytonn.com'
        ],
        'operations_group' => 'operations@cytonn.com',
        'risk_group' => 'risk@cytonn.com'
    ],
    'base_currency' => 'KES'
];
