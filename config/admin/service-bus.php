<?php
/**
 * Date: 07/06/2017
 * Time: 11:15
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: cytonn-sb
 * Cytonn Technologies
 */

return [

    'source' => config('app.name'),

    'query-queue' => 'crims-admin-query',
    'command-queue' => 'crims-admin-command',
    'event-queue' => 'crims-admin-events',

    'routes_files' => [
        base_path('routes/service-bus/all.php')
    ],

    'exception-handler' => \App\Exceptions\Handler::class . '@report',

    'targets' => [
        'system-name' => [
            'event' => 'event-queue-name',
            'query' => 'query-queue-name',
            'command' => 'command-queue-name'
        ],
        'cytonn_payments' => [
            'command' => 'cytonn_payments-command',
            'query' => 'cytonn_payments-query',
            'event' => 'cytonn_payments-events'
        ],
        'crims-admin' => [
            'command' => 'crims-admin-command',
            'query' => 'crims-admin-query',
            'event' => 'crims-admin-events'
        ],
    ],
];

