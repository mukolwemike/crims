<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | The Laravel queue API supports a variety of back-ends via an unified
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "sync", "beanstalkd", "sqs", "iron", "redis"
    |
    */

    'default' => env('QUEUE_DRIVER', 'sync'),

    'priority' => [
        'default' => env('QUEUE_NAME', 'crims-admin'),
        'high' => env('QUEUE_NAME_HIGH'),
        'payments' => env('QUEUE_NAME_PAYMENTS', 'crimsAdminCytonn_Payments'),
        'low' => env('QUEUE_NAME_LOW')
    ],

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],
        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'retry_after' => 90,
        ],
        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host'   => env('BEANSTALKD_HOST', '127.0.0.1'),
            'queue'  => env('QUEUE_NAME', 'crims-admin'),
            'retry_after'    => 1830,
        ],
        'sqs' => [
            'driver' => 'sqs',
            'key' => 'AKIAI5BQERQVFVHBFLYQ',
            'secret' => fortrabbitSecret('CUSTOM.AWS_S3_SECRET'),
            'prefix' => 'https://sqs.us-west-2.amazonaws.com/437702958374',
            'queue' => 'crims-admin',
            'region' => 'us-west-2'
        ],
        'iron' => [
            'driver'  => 'iron',
            'host'    => 'mq-aws-eu-west-1-1.iron.io',
            'token'   => fortrabbitSecret('CUSTOM.IRON_IO_TOKEN'),
            'project' => fortrabbitSecret('CUSTOM.IRON_IO_PROJECT_ID'),
            'queue'   => 'default',
            'encrypt' => true,
        ],
        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
            'queue' => 'default',
            'retry_after' => 1830,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'failed_jobs',
    ],

];
