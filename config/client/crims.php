<?php
/**
 * Date: 05/02/2017
 * Time: 15:46
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;

return [
    /*
     * Map model interfaces in crims-base to concrete implementation in the app
     */
    'model_maps'=>[
        //Clients
        Crims\Base\Clients\Client::class => Client::class,

        //Investments
        \Crims\Investments\Structured\Models\Investment::class => ClientInvestment::class,
        \Crims\Investments\Commission\Models\CommissionClawback::class => CommissionClawback::class,
        \Crims\Investments\Commission\Models\CommissionPaymentSchedule::class => CommissionPaymentSchedule::class,
        \Crims\Investments\Commission\Models\Payment::class => CommissionPayment::class,

        //Commission
        \Crims\Investments\Commission\Models\Recipient::class => CommissionRecepient::class,
        \Crims\Investments\Commission\Models\OverrideStructure::class => CommissionOverrideStructure::class,


        //Real Estate
        Crims\Realestate\Commission\Models\RealEstateCommissionPaymentSchedule::class => RealEstateCommissionPaymentSchedule::class,

        //Shares
        Crims\Shares\Commission\Models\ShareCommissionPaymentSchedule::class => SharesCommissionPaymentSchedule::class,

        //Unitization
        Crims\Unitization\Commission\Models\UnitFundCommissionPaymentSchedule::class => UnitFundCommissionPaymentSchedule::class,
        Crims\Unitization\Commission\Models\UnitFundCommissionSchedule::class => UnitFundCommissionSchedule::class,
    ]
];
