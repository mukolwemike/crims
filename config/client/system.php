<?php
/**
 * Date: 28/04/2017
 * Time: 09:21
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

return [
    'administrators'=>[
        'molukaka@cytonn.com',
        'emukiri@cytonn.com',
        'ikipngeno@cytonn.com'
    ],
    'emails' => [
        'super_admins' => [
            'molukaka@cytonn.com',
            'emukiri@cytonn.com',
            'ikipngeno@cytonn.com'
        ],
        'administrators' => [
            'molukaka@cytonn.com',
            'emukiri@cytonn.com',
            'ikipngeno@cytonn.com'
        ],
        'operations' => [
            'lmugo@cytonn.com',
            'rnjuguna@cytonn.com',
            'jmwanjama@cytonn.com',
            'sodindo@cytonn.com',
            'kduncan@cytonn.com',
            'lalicia@cytonn.com',
            'jkkamau@cytonn.com'
        ],
        'operations_admins' => [
            'lmugo@cytonn.com',
            'rnjuguna@cytonn.com'
        ],
        'operations_mgt' => [
            'gweru@cytonn.com',
            'lmugo@cytonn.com'
        ],
        'statements' => [
            'statements@cytonn.com'
        ],
        'operations_group' => 'operations@cytonn.com',
    ],
    'base_currency' => 'KES'
];
