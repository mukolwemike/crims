<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'username' => 'jgnasser@gmail.com',
        'secret' => fortrabbitSecret('CUSTOM.MANDRILL_KEY'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model'  => \App\Cytonn\Models\ClientUser::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'pm' => [
        'url' => env('PM_URL', 'https://pm.cytonn.com'),
        'project_id' => env('PM_PROJECT_ID', '6')
    ],

    'rollbar' => [
//        'access_token' => 'dbd845e1b6844b07bf37bd62a99d7f97',  //mwaruwac+rollbar_1@gmail
//      'access_token' => '070d0474effa4a4d8c70800547a7a4fe', //@cytonn
//      'access_token' => '5d964ba4a4974b32a75b55d9a40c7c12', //@gmail
        'access_token' => env('ROLLBAR_ACCESS_TOKEN', '070d0474effa4a4d8c70800547a7a4fe'),
        'front_end_access_token' => '0256f38303e94f8f86f11f3f5fcfa415',
        'environment' => env('APP_ENV'),
        'root' => base_path()
    ],
    'authy' => [
        'key' => env('AUTHY_KEY')
    ],
    'ussd' => [
        'key' => env('USSD_PASSWORD', 'effa4a4d8c7080054')
    ],
    'cytonn_payments' => [
        'url' => env('CYTONN_PAYMENTS_URL', ''),
        'submit_payments' => 'api/payments/submit',
        'client_id' => env('CYTONN_PAYMENTS_CLIENT_ID', ''),
        'client_secret' => env('CYTONN_PAYMENTS_CLIENT_SECRET', '')
    ],
    'iprs' => [
        'wsdl' => 'http://10.1.1.5:9004/IPRSServerwcf?wsdl',
        'username' => 'CYTONN',
        'password' => env('IPRS_PASSWORD', '')
    ]

];
