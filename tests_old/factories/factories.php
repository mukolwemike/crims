<?php

$factory(FundManager::class, [
    'name'              =>  $faker->word,
    'logo'              =>  'cms_logo_md.png',
    're_logo'           =>  'cre_logo_md.png',
    'fullname'          =>  $faker->words,
    're_fullname'       =>  $faker->words
]);


$factory(ShareHolderType::class, [
    'name'              =>  $faker->word,
    'slug'              =>  $faker->word
]);


$factory(SharesEntity::class, [
    'fund_manager_id'   =>  1,
    'default_for'       =>  null,
    'max_holding'       =>  $faker->numberBetween(20000,20000000),
    'name'              =>  $faker->name
]);


$factory(SharesPriceTrail::class, [
    'entity_id'             =>  1,
    'date'                  =>  $faker->dateTimeBetween('2015-01-01', 'now'),
    'price'                 =>  $faker->numberBetween(15, 150),
    'category_id'           =>  1,
]);


$factory(SharesCategory::class, [
    'name'              =>  $faker->word
]);


$factory(ShareHolding::class, [
    'number'            =>  $faker->numberBetween(100, 2000),
    'purchase_price'    =>  $faker->numberBetween(15, 70),
    'date'              =>  $faker->dateTimeBetween('2016-12-12', '2017-12-12'),
    'entity_id'         =>  1,
    'client_id'         =>  1,
    'category_id'       =>  1,
    'shareholder_type_id'       =>  1,
]);


$factory(SharesBusinessConfirmation::class, [
    'holding_id'        =>  1,
    'sent_by'           =>  1,
//    'payload'           =>  [
//        'number'                =>  $faker->randomNumber(50, 2000),
//        'purchase_price'        => $faker->randomNumber(15, 75),
//        'date'                  => '2016-10-10',
//        'entity_id'             => 1,
//        'client_id'             => 1,
//        'category_id'           => 1,
//        'shareholder_type_id'   => 1,
//        'payment_id'            => 1
//    ]
]);


$factory(Client::class, [
    'client_code'       =>  $faker->randomNumber(6, true),
    'dob'               =>  $faker->dateTimeBetween('1980-12-12', '2000-12-12'),
    'residence'         =>  $faker->streetName,
    'pin_no'            => $faker->randomNumber(6, true),
    'taxable'           => 1,
    'id_or_passport'    => $faker->randomNumber(6, true),
    'postal_code'       => $faker->postcode,
    'postal_address'    => $faker->address,
    'street'            => $faker->streetName,
    'town'              => $faker->city,
    'phone'             => $faker->phoneNumber,
    'telephone_office'  => $faker->phoneNumber,
    'telephone_home'    => $faker->phoneNumber,
]);


$factory(ClientType::class, [
    'name'              =>  'individual'
]);


$factory(ContactEntity::class, [
    'name'              =>  'individual'
]);


$factory(\App\Cytonn\Models\Title::class, [
    'name'              =>  $faker->title
]);


$factory(Country::class, [
    'iso_abbr'          =>  $faker->countryCode,
    'name'              =>  $faker->country
]);


$factory(Contact::class, [
    'entity_type_id'    =>  1,
    'firstname'         =>  $faker->firstName,
    'lastname'          =>  $faker->lastName,
    'middlename'        =>  $faker->lastName,
    'title_id'          =>  1,
    'email'             =>  $faker->email,
    'phone'             =>  $faker->phoneNumber,
    'country_id'        =>  1
]);


$factory(User::class, [
    'contact_id'        =>  1,
    'username'          =>  $faker->userName,
    'entity_type_id'    =>  1,
    'phone'             =>  $faker->phoneNumber,
    'title_id'          =>  1,
    'email'             =>  $faker->email,
    'jobtitle'          =>  $faker->jobTitle,
    'firstname'         =>  $faker->firstName,
    'middlename'        =>  $faker->lastName,
    'lastname'          =>  $faker->lastName,
    'qualification'     =>  $faker->jobTitle,
    'country_id'        =>  1
]);


$factory(ShareHolderEntity::class, [
    'name'              =>  $faker->word,
    'type'              =>  $faker->word
]);


$factory(ShareHolderEntityMember::class, [
    'shareholder_entity_id' =>  1,
    'client_id'             =>  1
]);


$factory(ShareHolderGroup::class, [
    'name'              =>  $faker->word,
]);


$factory(ShareHolderGroupMember::class, [
    'shareholder_group_id'  =>  1,
    'client_id'             =>  1
]);



$factory(CustodialTransactionType::class, [
    'name'                  =>  'FI',
    'description'           =>  $faker->sentence,
    'editable'              =>  $faker->boolean
]);


$factory(TransactionOwner::class, [
    'fund_investor_id'      =>  null,
    'fund_manager_id'       =>  1,
    'client_id'             =>  1
]);


$factory(ClientTransactionApproval::class, [
    'transaction_type'          =>  'deposit_funds_for_share_purchase',
    'client_id'                 =>  1,
    'payload'                   =>  null,
    'sent_by'                   =>  1,
    'approved'                  =>  1,
    'approved_by'               =>  1,
    'approved_on'               =>  $faker->dateTimeBetween('2015-12-12', '2016-12-12'),
]);


$factory(CustodialAccount::class, [
    'account_name'              =>  $faker->word,
    'bank_name'                 =>  $faker->company,
    'currency_id'               =>  1,
    'account_no'                =>  $faker->bankAccountNumber,
    'alias'                     =>  $faker->company,
    'fund_manager_id'           =>  1
]);


$factory(Currency::class, [
    'code'                      =>  $faker->currencyCode,
    'name'                      =>  $faker->currencyCode,
    'symbol'                    =>  $faker->currencyCode,
]);


$factory(CustodialTransaction::class, [
    'type'                      =>  1,
    'approval_id'               =>  1,
    'custodial_account_id'      =>  1,
    'transaction_owners_id'     =>  1,
    'amount'                    =>  $faker->numberBetween(12000, 200000),
    'description'               =>  $faker->sentence,
    'date'                      =>  $faker->dateTimeBetween('2015-12-12', '2016-12-12')
]);