<?php

use Laracasts\TestDummy\Factory;


function createShareHolderType($attributes = [])
{
    return Factory::create(ShareHolderType::class, $attributes);
}


function createSharesEntity($attributes = [])
{
    return Factory::create(SharesEntity::class, $attributes);
}


function createSharesCategory($attributes = [])
{
    return Factory::create(SharesCategory::class, $attributes);
}


function createFundManager($attributes = [])
{
    return Factory::create(FundManager::class, $attributes);
}


function createSharesPriceTrail($attributes = [])
{
    return Factory::create(SharesPriceTrail::class, $attributes);
}


function createClient($attributes = [])
{
    $contact = createContact();
    $attributes += [
        'contact_id'        =>  $contact->id,
        'client_type_id'    =>  createClientType()->id,
        'country_id'        =>  $contact->country_id
    ];
    return Factory::create(Client::class, $attributes);
}


function createClientType($attributes = [])
{
    return Factory::create(ClientType::class, $attributes);
}


function createTitle($attributes = [])
{
    return Factory::create(Title::class, $attributes);
}


function createCountry($attributes = [])
{
    return Factory::create(Country::class, $attributes);
}


function createContactEntityType($attributes = [])
{
    return Factory::create(ContactEntity::class, $attributes);
}

function createContact($attributes = [])
{
    $attributes += [
        'entity_type_id'        =>  createContactEntityType()->id,
        'title_id'              =>  createTitle()->id,
        'country_id'            =>  createCountry()->id
    ];
    return Factory::create(Contact::class, $attributes);
}


function createSharesHolding($attributes = [])
{
    return Factory::create(ShareHolding::class, $attributes);
}


function createSharesBusinessConfirmation($attributes = [])
{
    return Factory::create(SharesBusinessConfirmation::class, $attributes);
}


function createUser($attributes = [])
{
    return Factory::create(User::class, $attributes);
}


function createShareHolderEntity($attributes = [])
{
    return Factory::create(ShareHolderEntity::class, $attributes);
}


function createShareHolderEntityMember($attributes = [])
{
    return Factory::create(ShareHolderEntityMember::class, $attributes);
}


function createShareHolderGroup($attributes = [])
{
    return Factory::create(ShareHolderGroup::class, $attributes);
}


function createShareHolderGroupMember($attributes = [])
{
    return Factory::create(ShareHolderGroupMember::class, $attributes);
}



function createCustodialTransactionType($attributes = [])
{
    return Factory::create(CustodialTransactionType::class, $attributes);
}


function createClientTransactionApproval($attributes = [])
{
    return Factory::create(ClientTransactionApproval::class, $attributes);
}


function createCustodialAccount($attributes = [])
{
    return Factory::create(CustodialAccount::class, $attributes);
}


function createCustodialTransaction($attributes = [])
{
    return Factory::create(CustodialTransaction::class, $attributes);
}



function createTransactionOwner($attributes = [])
{
    return Factory::create(TransactionOwner::class, $attributes);
}


function createCurrency($attributes = [])
{
    return Factory::create(Currency::class, $attributes);
}