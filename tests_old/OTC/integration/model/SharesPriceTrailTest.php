<?php

namespace App\Tests\OTC\Integration\Model;


use Laracasts\TestDummy\DbTestCase;

class SharesPriceTrailTest extends DbTestCase
{

    protected $trail;

    protected $entity;

    protected $category;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->category = createSharesCategory(['name' => 'General']);

        $fund_manager = createFundManager(['name' => 'Coop']);

        $this->entity = createSharesEntity(['fund_manager_id' => $fund_manager->id]);

        $this->trail = createSharesPriceTrail(['category_id' => $this->category->id, 'entity_id' => $this->entity->id, 'price' => 35.5]);
    }

    /** @test */
    public function a_trail_has_a_category()
    {
        $this->assertEquals($this->category->name, $this->trail->category->name);
    }

    /** @test */
    public function a_trail_has_an_entity()
    {
        $this->assertEquals($this->entity->name, $this->trail->entity->name);
    }

    /** @test */
    public function a_trail_can_get_the_purchase_price_of_an_entity_at_a_certain_date()
    {
        $this->assertEquals($this->trail->price, $this->trail->getPrice($this->entity->id, $this->trail->date));
    }
}
