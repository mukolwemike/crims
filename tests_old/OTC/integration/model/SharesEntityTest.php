<?php

namespace App\Tests\OTC\Integration\Model;


use Laracasts\TestDummy\DbTestCase;

class SharesEntityTest extends DbTestCase
{

    protected $entity;

    protected $fund_manager;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->fund_manager = createFundManager(['name' => 'Coop']);

        $this->entity = createSharesEntity(['fund_manager_id' => $this->fund_manager->id]);
    }

    /** @test */
    public function an_entity_has_a_fund_manager()
    {
        $this->assertEquals($this->fund_manager->name, $this->entity->fundManager->name);
    }

    /** @test */
    public function an_entity_has_price_trails()
    {
        $category = createSharesCategory();

        createSharesPriceTrail(['category_id' => $category->id, 'entity_id' => $this->entity->id]);

        createSharesPriceTrail(['category_id' => $category->id, 'entity_id' => $this->entity->id]);

        $this->assertEquals(2, $this->entity->priceTrails()->count());
    }
}
