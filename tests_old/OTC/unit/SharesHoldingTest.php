<?php

namespace App\Tests\OTC\Unit;

class SharesHoldingTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $holding
     */
    protected $holding;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->holding = new \ShareHolding([
            'number'                =>  152,
            'purchase_price'        =>  20.0,
            'date'                  =>  '2016-10-10',
            'entity_id'             =>  1,
            'category_id'           =>  1,
            'client_id'             =>  1,
            'payment_id'            =>  1,
            'approval_id'           =>  1,
        ]);
    }

    /** @test */
    function a_holding_has_a_number()
    {
        $this->assertEquals(152, $this->holding->number);
    }

    /** @test */
    function a_holding_has_a_purchase_price()
    {
        $this->assertEquals(20.0, $this->holding->purchase_price);
    }

    /** @test */
    function a_holding_has_a_date()
    {
        $this->assertEquals('2016-10-10', $this->holding->date);
    }

    /** @test */
    function a_holding_has_an_entity_id()
    {
        $this->assertEquals(1, $this->holding->entity_id);
    }

    /** @test */
    function a_holding_has_a_client_id()
    {
        $this->assertEquals(1, $this->holding->client_id);
    }

    /** @test */
    function a_holding_has_a_category_id()
    {
        $this->assertEquals(1, $this->holding->category_id);
    }

    /** @test */
    function a_holding_has_a_payment_id()
    {
        $this->assertEquals(1, $this->holding->payment_id);
    }

    /** @test */
    function a_holding_has_a_approval_id()
    {
        $this->assertEquals(1, $this->holding->approval_id);
    }


}
