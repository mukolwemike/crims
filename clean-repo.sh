#!/usr/bin/env bash
java -jar ~/Downloads/bfg.jar --delete-folders "{node_modules,debugbar,logs}" --strip-blobs-bigger-than 1M --protect-blobs-from master
git reflog expire --expire=now --all
git gc --prune=now --aggressive
