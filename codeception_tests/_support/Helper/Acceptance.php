<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{

    /*
    * Mock logging in, avoid the token issue
    */
    public function login($userId)
    {
        $user = \User::find($userId);

        \Auth::login($user);
    }
}
