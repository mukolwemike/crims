<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{
     /*
      * Mock logging in, avoid the token issue
      */
    public function login($userId)
    {
        $user = \User::find($userId);

        \Auth::login($user);
    }

    /**
     *This helps get the response property of the REST module
     */
    public function seeResponseIsHtml()
    {
        $response = $this->getModule('REST')->response;
        $this->assertRegExp('~^<!DOCTYPE HTML(.*?)<html>.*?<\/html>~m', $response);
    }

    /**
     * @param array $attributes
     * Check to see that a response contains multiple attributes
     */
    public function seeResponseContainsAttributes(array $attributes)
    {
        $I = $this->getModule('REST');

        foreach ($attributes as $attribute) {
            $I->seeResponseContains($attribute);
        }
    }

    public function sendApiGet($url)
    {
        return $this->getModule('PhpBrowser')->_request('GET', $url);
    }
}
