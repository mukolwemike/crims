<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Faker\Factory;
use Helper\Resources\Base;

/**
 * Class Functional
 * @package Helper
 */
class Functional extends Base
{
    /*
    * Mock logging in, avoid the token issue
    */
    public function login()
    {
        $I = $this->getModule('Laravel4');

        $faker = Factory::create();

        $user_id = $I->haveRecord('users', ['id'=>rand(1, 10000000), 'username'=>$faker->userName, 'email'=>$faker->email, 'password'=>\Hash::make('secret')]);
        $current_user = \User::find($user_id);

        $I->amOnPage('/login');
        $I->fillField('username', $current_user->username);
        $I->fillField('password', 'secret');
        $I->click('login');
        $I->seeAuthentication();

        return $current_user;
    }
}
