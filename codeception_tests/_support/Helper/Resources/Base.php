<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 10/4/16
 * Time: 4:16 PM
 */

namespace Helper\Resources;

use Faker\Factory as Faker;

class Base extends \Codeception\Module
{

    /**
     * Have many database records for the same model
     *
     * @param $table
     * @param array $records
     */
    public function haveManyRecords($table, array $records)
    {
        $laravel =  $this->getModule('Laravel4');

        foreach ($records as $record) {
            $laravel->haveRecord($table, $record);
        }
    }

    /**
     * Get latest Transaction
     *
     * @param $type
     * @return mixed
     */
    public function getLatestTransaction($type)
    {
        return \ClientTransactionApproval::where('transaction_type', $type)->latest()->first()->id;
    }

    /**
     * Approve a Transaction
     *
     * @param $id
     */
    public function approveTransaction($id)
    {
        $I = $this->getModule('Laravel4');
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->click('Approve');
    }

    /**
     * Setup a Client
     *
     * @throws \Exception
     */
    protected function setupClient()
    {
        try {
            $this->haveManyRecords('client_type', [
                ['id'=>1, 'name'=>'individual'],
                ['id'=>2,'name'=>'corporate']
            ]);
        } catch (\Exception $e) {
            if (!\ClientType::find(1)) {
                throw $e;
            }
        }

        try {
            $this->haveManyRecords('contact_entity_types', [
                ['id'=>1, 'name'=>'individual'],
                ['id'=>2,'name'=>'corporate']
            ]);
        } catch (\Exception $e) {
            if (!\ContactEntity::find(1)) {
                throw $e;
            }
        }

        try {
            $this->haveManyRecords('titles', [
                [ 'id'=>1, 'name'=>'Prof'],
                [ 'id'=>2, 'name'=>'Sir']
            ]);
        } catch (\Exception $e) {
            if (!\Title::find(1)) {
                throw $e;
            }
        }

        $laravel = $this->getModule('Laravel4');

        try {
            //Client Gender
            $clientGender = [
                'id'=>1,
                'abbr'=>'M'
            ];
            $laravel->haveRecord('gender', $clientGender);
        } catch (\Exception $e) {
            if (!\Gender::find(1)) {
                throw $e;
            }
        }

        try {
            $country = [
                'id'=>114,
                'iso_abbr'=>'KE',
                'name'=>'Kenya'
            ];
            $laravel->haveRecord('countries', $country);
        } catch (\Exception $e) {
            if (!\Country::find(114)) {
                throw $e;
            }
        }

        try {
            //Methods of Contact
            $method_of_contact = [
                'id'=>1,
                'description'=>'email'
            ];
            $laravel->haveRecord('contact_method', $method_of_contact);
        } catch (\Exception $e) {
            if (!\ContactMethod::find(1)) {
                throw $e;
            }
        }

        try {
            //Employment Status
            $employment = [
                'id'=>1,
                'name'=>'Employed'
            ];
            $laravel->haveRecord('employment', $employment);
        } catch (\Exception $e) {
            if (!\Employment::find(1)) {
                throw $e;
            }
        }

        try {
            //Fund Sources
            $fund_source = [
                'id'=>1,
                'name'=>'Profits'
            ];
            $laravel->haveRecord('funds_sources', $fund_source);
        } catch (\Exception $e) {
            if (!\FundSource::find(1)) {
                throw $e;
            }
        }

        try {
            $client_contact_person_relationships = [
                'id'=>1,
                'relationship'=>'Brother'
            ];
            $laravel->haveRecord('client_contact_person_relationships', $client_contact_person_relationships);
        } catch (\Exception $e) {
            if (!\ClientContactPersonRelationship::find(1)) {
                throw $e;
            }
        }
    }

    /**
     * Setup Investments
     *
     * @throws \Exception
     */
    protected function setupInvestments()
    {

        $this->haveManyRecords('custodial_transaction_types', [
            ['name'=>'FI'], ['name'=>'FO']
        ]);

        try {
            //Settings
            $this->haveManyRecords('settings', [
                [
                    'id'=>1,
                    'key'=>'withholding_tax_rate',
                    'value'=>15
                ],
                [
                    'id'=>2,
                    'key'=>'custody_fee_rate',
                    'value'=>0.145
                ],
                [
                    'id'=>3,
                    'key'=>'commission_first_installment_rate',
                    'value'=>0.5
                ],
                [
                    'id'=>4,
                    'key'=>'withholding_tax_rate',
                    'value'=>15
                ],
                [
                    'id'=>5,
                    'key'=>'dividend_tax_rate',
                    'value'=>15
                ]
            ]);
        } catch (\Exception $e) {
            if (!\Setting::find(2)) {
                throw $e;
            }
        }

        try {
            //Investment Types
            $this->haveManyRecords('client_investment_types', [
                [
                    'id'=>4,
                    'name'=>'partial_withdraw'
                ],[
                    'id'=>3,
                    'name'=>'rollover'
                ],[
                    'id'=>2,
                    'name'=>'topup'
                ],
                [
                    'id'=>1,
                    'name'=>'investment'
                ]
            ]);
        } catch (\Exception $e) {
            if (!\ClientInvestmentType::find(4)) {
                throw $e;
            }
        }

        try {
            //Currencies
            $this->haveManyRecords('currencies', [
                [
                    'id'=>1,
                    'code'=>'KES',
                    'name'=>'Kenya Shillings',
                    'symbol'=>'KES'
                ],
                [
                    'id'=>2,
                    'code'=>'USD',
                    'name'=>'US Dollars',
                    'symbol'=>'$'
                ],
                [
                    'id'=>3,
                    'code'=>'GBP',
                    'name'=>'Kenya Great Britain Pound',
                    'symbol'=>'Â£'
                ],
                [
                    'id'=>4,
                    'code'=>'EUR',
                    'name'=>'Euro',
                    'symbol'=>'â¬'
                ]
            ]);
        } catch (\Exception $e) {
            if (!\Currency::find(1)) {
                throw $e;
            }
        }

        try {
            //Corporate Investor Types
            $this->haveManyRecords('corporate_investor_type', [
                ['id'=>1, 'name'=>'Company' ],
                ['id'=>2,'name'=>'Trust'],
                ['id'=>3,'name'=>'Fund'],
                ['id'=>4,'name'=>'Other']
            ]);
        } catch (\Exception $e) {
            if (!\CorporateInvestorType::find(1)) {
                throw $e;
            }
        }

        try {
            $this->haveManyRecords('custodial_accounts', [['account_name'=>'Custodial', 'account_no'=>'328928923', 'bank_name'=>'SCB', 'alias'=>'Investments A/C', 'currency_id'=>1]]);
        } catch (\Exception $e) {
            if (!\CustodialAccount::find(1)) {
                throw $e;
            }
        }
    }

    /**
     * Setup Portfolio Investments
     *
     * @throws \Exception
     */
    public function setupPortfolioInvestments()
    {
        try {
            //fund Types
            $this->haveManyRecords('fund_types', [
                ['id'=>1,'name'=>'Deposit'],
                ['id'=>2,'name'=>'Commercial Paper'],
                ['id'=>3,'name'=>'T-Bill'],
                ['id'=>4,'name'=>'T-Bond'],
                ['id'=>5,'name'=>'Others']
            ]);
        } catch (\Exception $e) {
            if (!\FundType::find(1)) {
                throw $e;
            }
        }

        try {
            //Custodial Transaction Types
            $this->haveManyRecords('custodial_transaction_types', [
                ['name'=>'FI'],
                ['name'=>'FO'],
                ['name'=>'I'],
                ['name'=>'R'],
                ['name'=>'ECF'],
                ['name'=>'EMC'],
                ['name'=>'EFC']
            ]);
        } catch (\Exception $e) {
            if (!\CustodialTransactionType::where('name', 'FI')->first()) {
                throw  $e;
            }
        }

        try {
            //Fund Managers
            $this->haveManyRecords('fund_managers', [
            ['id'=>1, 'name'=>'CIM', 'fullname'=>'Cytonn Investments Cooperative Society'],
            ['id'=>2,'name'=>'Coop', 'fullname'=>'Cytonn Investments Cooperative Society']
            ]);
        } catch (\Exception $e) {
            if (!\FundManager::find(1)) {
                throw $e;
            }
        }
        try {
            //Currencies
            $this->haveManyRecords('currencies', [
                [
                    'id'=>1,
                    'code'=>'KES',
                    'name'=>'Kenya Shillings',
                    'symbol'=>'KES'
                ],
                [
                    'id'=>2,
                    'code'=>'USD',
                    'name'=>'US Dollars',
                    'symbol'=>'$'
                ],
                [
                    'id'=>3,
                    'code'=>'GBP',
                    'name'=>'Kenya Great Britain Pound',
                    'symbol'=>'Â£'
                ],
                [
                    'id'=>4,
                    'code'=>'EUR',
                    'name'=>'Euro',
                    'symbol'=>'â¬'
                ]
            ]);
        } catch (\Exception $e) {
            if (!\Currency::find(1)) {
                throw $e;
            }
        }

        try {
            //Custodial Accounts
            $this->haveManyRecords('custodial_accounts', [
                ['id'=>1, 'account_name'=>'ssmmsms', 'bank_name'=>'Equity', 'currency_id'=>\Currency::find(1)->id, 'alias'=>'ALIAS', 'fund_manager_id'=>\FundManager::find(1)->id]
            ]);
        } catch (\Exception $e) {
            if (!\CustodialAccount::find(1)) {
                throw $e;
            }
        }


        try {
            //Portfolio Investors
            $this->haveManyRecords('portfolio_investors', [
                ['id'=>1, 'name'=>'Test Insert portfolio Investor'],
                ['id'=>2, 'name'=>'Test Success portfolio Investor']
            ]);
        } catch (\Exception $e) {
            if (!\PortfolioInvestor::find(1)) {
                throw $e;
            }
        }
        try {
            //Portfolio Investment Types
            $this->haveManyRecords('portfolio_investment_types', [
                ['id'=>1, 'name'=>'partial'],
                ['id'=>2, 'name'=>'complete']
            ]);
        } catch (\Exception $e) {
            if (!\PortfolioInvestmentType::find(1)) {
                throw $e;
            }
        }
        try {
            //Settings
            $this->haveManyRecords('settings', [
                [
                    'id'=>1,
                    'key'=>'withholding_tax_rate',
                    'value'=>15
                ],
                [
                    'id'=>2,
                    'key'=>'custody_fee_rate',
                    'value'=>0.145
                ],
                [
                    'id'=>3,
                    'key'=>'commission_first_installment_rate',
                    'value'=>0.5
                ],
                [
                    'id'=>4,
                    'key'=>'withholding_tax_rate',
                    'value'=>15
                ],
                [
                    'id'=>5,
                    'key'=>'dividend_tax_rate',
                    'value'=>15
                ]
            ]);
        } catch (\Exception $e) {
            if (!\Setting::find(2)) {
                throw $e;
            }
        }
    }

    /**
     *Setup Real Estate Projects
     */
    public function setupProject()
    {
        $faker = Faker::create();

        try {
            //Fund Managers
            $this->haveManyRecords('fund_managers', [
                ['id'=>1, 'name'=>'CIM', 'fullname'=>'Cytonn Investments Cooperative Society'],
                ['id'=>2,'name'=>'Coop', 'fullname'=>'Cytonn Investments Cooperative Society']
            ]);
        } catch (\Exception $e) {
            if (!\FundManager::find(1)) {
                throw $e;
            }
        }

        try {
            //Project Types
            $this->haveManyRecords('project_types', [
                ['id'=>1, 'name'=>'Apartment Blocks', 'slug'=>'apartments'],
                ['id'=>2, 'name'=>'Exclusive gated community', 'slug'=>'gated_community'],
                ['id'=>3, 'name'=>'Sharplands', 'slug'=>'sharplands'],
            ]);
        } catch (\Exception $e) {
            if (!\ProjectType::find(1)) {
                return $e;
            }
        }

        try {
            //Unit Sizes
            $this->haveManyRecords('realestate_unit_sizes', [
                ['id'=>1, 'name'=>'1 Bedroom'],
                ['id'=>2, 'name'=>'2 Bedroom'],
                ['id'=>3, 'name'=>'3 Bedroom'],
                ['id'=>4, 'name'=>'4 Bedroom'],
                ['id'=>5, 'name'=>'5 Bedroom'],
            ]);
        } catch (\Exception $e) {
            if (!\RealestateUnitSize::latest()->first()) {
                throw $e;
            }
        }

        try {
            //Unit Types
            $this->haveManyRecords('realestate_unit_types', [
                [ 'id'=>rand(1, 10000), 'name'=>'Apartment']
            ]);
        } catch (\Exception $e) {
            if (!\RealestateUnitType::latest()->first()) {
                throw $e;
            }
        }

        try {
            //Payment Types
            $this->haveManyRecords('realestate_payment_types', [
                ['slug'=>'reservation_fee', 'name'=>'Reservation fees']
            ]);
        } catch (\Exception $e) {
            if (!\RealEstatePaymentType::latest()->first()) {
                throw $e;
            }
        }
    }
}
