<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 10/11/16
 * Time: 10:04 AM
 */

namespace Helper\Resources;

use Laracasts\TestDummy\Factory as TestDummy;
use Cytonn\Core\DataStructures\Carbon;

class PortfolioInvestments extends Base
{
    public function have($model, $overrides = [])
    {
        return TestDummy::create($model, $overrides);
    }

    public function haveCustodialTransaction(array $params = [])
    {
        $acc = isset($params['custodial_account_id']) ? \CustodialAccount::find($params['custodial_account_id']) : \CustodialAccount::find($this->haveCustodialAccount());

        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('custodial_transactions', [
            'custodial_account_id'=>$acc->id,
            'type'=>$this->haveCustodialTransactionType(),
            'amount'=>3245678654321345,
            'description'=>"Just Needed this for testing",
            'date'=>Carbon::today()->toDateString()
        ]);
    }

    public function haveCustodialAccount()
    {
        $this->setupPortfolioInvestments();

        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('custodial_accounts', [
            'account_name'=>'Custodial Account',
            'bank_name'=>'SC',
            'currency_id'=>\Currency::find(1)->id,
            'account_no'=>25523,
            'fund_manager_id'=>\FundManager::all()->random()->id
        ]);
    }

    public function havePortfolioInstitution()
    {
        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('portfolio_investors', ['name'=>'Test Insert portfolio Investor']);
    }

    public function haveFundTypes()
    {
        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('fund_types', ['name'=>'Deposit']);
    }
    public function haveBank()
    {
        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('banks', ['name'=>'Equity']);
    }

    public function haveCustodialTransactionType()
    {

        $this->haveManyRecords('custodial_transaction_types', [
            ['id'=>1, 'name'=>'FI', 'description'=>'Client funds in'],
            ['id'=>2, 'name'=>'FO', 'description'=>'Client funds out'],
            ['id'=>3, 'name'=>'I', 'description'=>'Fund Investment'],
            ['id'=>4, 'name'=>'R', 'description'=>'Fund Redemption'],
            ['id'=>5, 'name'=>'ECF', 'description'=>'Custodial fees'],
            ['id'=>6, 'name'=>'EMF', 'description'=>'Management fees'],
            ['id'=>7, 'name'=>'EFC', 'description'=>'FA Commission']
        ]);

        return \CustodialTransactionType::find(1)->id;
    }

    public function havePortfolioInvestment()
    {
        $laravel = $this->getModule('Laravel4');

        $this->setupPortfolioInvestments();

        $formData = [
            'custodial_account_id'=>1,
            'portfolio_investor_id'=>1,
            'taxable'=>1,
            'fund_manager_id'=>1,
            'fund_type_id'=>1,
            'type_id'=>1,
            'invested_date'=>Carbon::today()->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(5)->toDateString(),
            'amount'=>45464749000,
            'withdrawn'=>0,
            'rolled'=>0,
            'withdrawal_date'=>null,
            'withdrawn_by'=>null,
            'on_call'=>false,
            'custodial_invest_transaction_id'=>null,
            'custodial_withdraw_transaction_id'=>null,
            'invest_transaction_approval_id'=>null,
            'withdraw_transaction_approval_id'=>null,
            'rollover_to'=>null,
            'rollover_from'=>null
        ];
        return $laravel->haveRecord('portfolio_investments', $formData);
    }
}
