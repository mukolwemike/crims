<?php

namespace Helper\Resources;

use Cytonn\Clients\ClientRepository;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Investment\BaseInvestment;
use Cytonn\Portfolio\PortfolioInvestment;
use Helper\Functional;
use Illuminate\Database\QueryException;
use Laracasts\TestDummy\Factory;
use Faker\Factory as Faker;

/**
 * Date: 27/09/2016
 * Time: 23:12
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class Investments extends Base
{

    public function haveFundManager($id = null)
    {
        !is_null($id) ? : $id = 1;

        $laravel = $this->getModule('Laravel4');

        if (\FundManager::find($id)) {
            return $id;
        }

        $laravel->haveRecord('fund_managers', ['id'=>$id, 'name'=>'CIM']);

        return $id;
    }

    public function haveProduct()
    {
        $fm = $this->haveFundManager(1);
        $this->setupInvestments();

        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('products', ['fund_manager_id'=>$fm, 'name'=>'CMS', 'currency_id'=>\Currency::first()->id, 'custodial_account_id'=>\CustodialAccount::first()->id]);
    }

    public function haveProductPlan()
    {
        $laravel = $this->getModule('Laravel4');
        $product_plan = [
            'id'=>rand(1, 10000),
            'client_id'=>$this->haveClient(),
            'product_id'=>$this->haveProduct(),
            'mode_of_payment'=>'Direct Bank Deposit',
            'shares'=>500,
            'duration'=>5.00,
            'amount'=>1000000,
            'start_date'=>Carbon::today()->toDateString(),
            'active'=>1
        ];

        $laravel->haveRecord('product_plans', $product_plan);

        return $product_plan['id'];
    }

    public function haveUser()
    {
        $faker = Faker::create();

        $I = $this->getModule('Laravel4');

        if (!\ContactEntity::where('name', 'individual')->first()) {
            $I->haveRecord('contact_entity_types', ['id'=>1, 'name'=>'individual']);
        }

        $contacts = [
            'id'=>rand(1, 1000),
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
            'email'=>$faker->email,
            'entity_type_id'=>1
        ];
        $I->haveRecord('contacts', $contacts);


        $user_id = $faker->numberBetween(1, 1000);
        $users = [
            'id'=>$user_id,
            'email'=>$faker->email,
            'username'=>$faker->userName,
            'password'=>\Hash::make('secret'),
            'contact_id'=>$contacts['id']
        ];
        $I->haveRecord('users', $users);

        //Staff
        $staff = [
            'id'=>rand(1, 10000),
            'user_id'=>$user_id,
            'contact_id'=>$contacts['id'],
            'title'=>$faker->title,
            'qualification'=>'CFA'
        ];

        $I->haveRecord('staff', $staff);

        return $user_id;
    }

    public function haveClient(array $params = [])
    {
        $this->setupClient();

        $fund_manager_id = isset($params['fund_manager_id']) ? $params['fund_manager_id'] : $fund_manager_id = 1;
        $fund_investor_id = isset($params['fund_investor_id']) ? $params['fund_investor_id'] : $fund_investor_id = 1;


        $this->haveFundManager($fund_manager_id);

        $laravel = $this->getModule('Laravel4');

        $faker = Faker::create();

        //Client Contact
        $contact = $laravel->haveRecord('contacts', [
            'title_id'=>2,
            'firstname'=> $faker->firstName,
            'middlename'=>$faker->name,
            'lastname'=> $faker->lastName,
            'email' => $faker->email,
            'entity_type_id'=>1,
            'phone'=>$faker->phoneNumber,
            'gender_id'=>1
        ]);

        $client_code = (new ClientRepository())->suggestClientCode();

        //Create Client
        $this->haveManyRecords(
            'clients',
            [
                [
                    'contact_id' => $contact,
                    'taxable'=>true,
                    'client_code'=>$client_code,
                    'client_type_id' => 1,
                    'phone'=>070654533453,
                    'town'=>'Nairobi',
                    'fund_manager_id'=>$fund_manager_id,
                    'employment_id'=>1,
                    'employer_address'=>$faker->address,
                    'investor_bank_branch'=>$faker->city,
                    'investor_bank'=>$faker->name,
                    'investor_account_name'=>$faker->bankAccountNumber,
                    'created_at'=>Carbon::today()->toDateString()
                ],
                [
                    'contact_id' => $contact,
                    'taxable'=>true,
                    'client_code'=>rand(1, 1000000),
                    'client_type_id' => 1,
                    'town'=>'Test-Nairobi',
                    'fund_manager_id'=>$fund_manager_id,
                    'employment_id'=>1,
                    'employer_address'=>$faker->address,
                    'investor_bank_branch'=>$faker->city,
                    'investor_bank'=>$faker->name,
                    'investor_account_name'=>$faker->bankAccountNumber,
                    'created_at'=>Carbon::today()->toDateString()
                ]
            ]
        );

        $client_id = \Client::where('town', 'Nairobi')->first()->id;

        return $client_id;
    }


    /**
     * @return mixed
     */
    public function haveClientInvestment(array $params = [])
    {
        $client = isset($params['client_id']) ? \Client::find($params['client_id']) : \Client::find($this->haveClient());

        $product_id = isset($params['product_id']) ? $params['product_id'] : $this->haveProduct();

        $this->setupInvestments();

        $laravel = $this->getModule('Laravel4');

        $trans = $laravel->haveRecord('client_transaction_approval', ['transaction_type'=>'investment']);

        $inv = [
            'client_id'=>$client->id,
            'approval_id'=>$trans,
            'investment_type_id'=>1,
            'amount'=>10000,
            'interest_rate'=>18,
            'invested_date'=>Carbon::today()->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(3)->toDateString(),
            'product_id'=> $this->haveProduct(),
            'product_plan_id'=>$this->haveProductPlan(),
            'withdrawn'=>0,
            'rolled'=>0,
            'withdraw_amount'=>0,
            'withdrawal_date'=>null,
            'withdrawal_by'=>null,
            'custodial_invest_transaction_id'=>$this->haveCustodialTransaction($client->id),
            'custodial_withdraw_transaction_id'=>null,
            'withdraw_approval_id'=>null,
            'interest_payment_interval'=>0,
            'interest_payment_date'=>0,
            'on_call'=>1,
            'partial_withdraw'=>0,
            'bank_account_id'=>$this->haveBankAccount(),
            'withdrawal_instruction_id'=>1,
            'payment_id'=>$this->haveCoopPayment()
        ];

        $this->haveManyRecords('client_investments', [$inv, $inv]);
        $inv['id'] = \ClientInvestment::first()->id;

        $laravel->haveRecord('commission_rates', ['name'=>'Staff - KES', 'rate'=>0.5, 'currency_id'=>\Product::find($product_id)->currency_id]);

        $laravel->haveRecord('commissions', $c = ['recipient_id'=>$this->haveCommissionRecepient(), 'investment_id'=>$inv['id'], 'compliant'=>true, 'compliance_date'=>$inv['invested_date'], 'rate'=>0.5]);

        return $inv['id'];
    }

    public function haveCommission()
    {

        $laravel = $this->getModule('Laravel4');

        $commission = [
            'id'=>rand(1, 100000),
            'recipient_id'=>$this->haveCommissionRecepient(),
            'investment_id'=>$this->haveClientInvestment(),
            'compliant'=>1,
            'compliance_date'=>Carbon::today()->toDateString(),
        ];
        return $laravel->haveRecord('commissions', $commission);
    }

    public function haveCoopPayment()
    {
        $laravel = $this->getModule('Laravel4');

        $client_id = $this->haveClient();

        //coop payments
        $coop_payments = [

            'client_id'=>$this->haveClient(),
            'custodial_transaction_id'=>$this->haveCustodialTransaction($client_id),
            'type'=>'payments',
            'amount'=>5454,
            'date'=>Carbon::today()->toDateString(),
            'narrative'=>'Its justs a coop payment'
        ];

        return $laravel->haveRecord('coop_payments', $coop_payments);
    }

    public function haveBankAccount()
    {
        $client_bank_accounts = [
            'client_id'=>$this->haveClient(),
            'account_name'=>'My Bank Account',
            'account_number'=>2423242,
            'bank'=>'My Second Bank',
            'branch'=>'Westlands',
            'clearing_code'=>123,
            'swift_code'=>254366
        ];

        return $this->getModule('Laravel4')->haveRecord('client_bank_accounts', $client_bank_accounts);
    }

    public function haveShareEntities($fund_manager_id = null)
    {
        !is_null($fund_manager_id) ? : $fund_manager_id = 1;
        $this->haveFundManager($fund_manager_id);

        $laravel = $this->getModule('Laravel4');

        $share_entities = [
            'fund_manager_id'=>$fund_manager_id,
            'max_holding'=>20,
            'name'=>'Coop'
        ];

        return $laravel->haveRecord('shares_entities', $share_entities);
    }

    /**
     * @return int
     */
    public function haveShareCategries()
    {
        $laravel = $this->getModule('Laravel4');
        $share_categories = [
            'id'=>1,
            'name'=>'General'
        ];
        $laravel->haveRecord('share_categories', $share_categories);
        return $share_categories['id'];
    }

    public function haveApplication()
    {
        $laravel = $this->getModule('Laravel4');
        $faker = Faker::create();
        $client_id = $this->haveClient();
        $product_id  = $this->haveProduct();
        $client = \Client::where('id', $client_id)->first();

        //individual application
        $individualApplication = [
            'id' =>rand(1, 1000),
            'type_id'=>$client->client_type->id,
            'joint'=>0,
            'application_type_id'=>1,
            'parent_application_id'=>34,
            'client_id'=>$client->id,
            'amount' =>12300000,
            'partnership_account_number'=> 120,
            'agreed_rate'=>15,
            'tenor'=>18,
            'title'=> \Title::first()->id,
            'lastname'=>$faker->lastName,
            'middlename'=>$faker->name,
            'firstname'=>$faker->firstName,
            'gender_id'=>1,
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>121212,
            'id_or_passport'=>424244224,
            'email'=>'email@example.com',
            'postal_code'=>345234,
            'postal_address'=>22222,
            'country_id'=>114,
            'telephone_office'=>07052626262,
            'phone'=>07052626262,
            'residential_address'=>'Cketch',
            'town'=>'Nairobi',
            'method_of_contact_id'=>1,
            'employment_id'=>1,
            'present_occupation'=>'Investor',
            'employer_name'=>'Cytonn Management',
            'funds_source_id'=>1,
            'funds_source_other'=>'Gampler',
            'investor_account_name'=>'MyInvestment',
            'investor_account_number'=>232323,
            'investor_bank'=>'Bank',
            'investor_bank_branch'=>'Branch',
            'investor_clearing_code'=>232,
            'investor_swift_code'=>234,
            'terms_accepted'=>1,
            'product_id'=>$product_id,
            'approval_id'=>null,
            'taxable'=>1,
            'complete'=>1

        ];
        $laravel->haveRecord('client_investment_application', $individualApplication);

        return $individualApplication['id'];
    }


    public function haveCustodialAccount()
    {
        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('custodial_accounts', [
            'account_name'=>'Custodial Account',
            'bank_name'=>'Standard Chattered',
            'currency_id'=>\Currency::find(1)->id,
            'account_no'=>25523,
            'fund_manager_id'=>\FundManager::find(1)->id
        ]);
    }

    public function haveTransactionOwner(\Client $client = null, \PortfolioInvestor $fundInvestor = null)
    {
        return \TransactionOwner::getOwner($client, $fundInvestor);
    }

    public function haveCustodialTransaction($client_id)
    {
        $this->setupInvestments();
        $laravel = $this->getModule('Laravel4');
        $custodial_transactions_types = \CustodialTransactionType::first();


        //Custodial Transactions
        $custodial_transactions = [
            'type'=>$custodial_transactions_types['id'],
            'custodial_account_id'=>$this->haveCustodialAccount(),
            'amount'=>23000,
            'description'=>'Custodial Transaction',
            'date'=>Carbon::today()->toDateString(),
            'transaction_owners_id'=> $this->haveTransactionOwner(\Client::find($client_id))->id
        ];


        return $laravel->haveRecord('custodial_transactions', $custodial_transactions);
    }


    public function haveCommissionRecepient()
    {
        $laravel = $this->getModule('Laravel4');

        //Commission Rates
        $commission_rates = [
            'id'=> rand(1, 2563),
            'name'=>'Staff - KES',
            'rate'=>0.5,
            'currency_id'=>$laravel->haveRecord('currencies', ['code'=>'KES', 'name'=>'Kenya Shillings', 'symbol'=>'KES'])
        ];

        $laravel->haveRecord('commission_rates', $commission_rates);

        //Commision Recipient Types
        $commission_recipient_type = [
            'id'=>rand(1, 365673),
            'name'=>'Staff',
            'rate_id'=>$commission_rates['id']
        ];
        $laravel->haveRecord('commission_recepient_type', $commission_recipient_type);


        //Commission Recipients
        $commission_recipient = [
            'recipient_type_id'=>$commission_recipient_type['id'],
            'staff_id'=>null,
            'email'=>'email@example.com',
            'name'=>'Name'
        ];

        return $laravel->haveRecord('commission_recepients', $commission_recipient);
    }


    public function haveClientTransaction()
    {
        $laravel = $this->getModule('Laravel4');

        $f_id = $this->haveFundManager();
        $formData = [
            'id'=> rand(1, 10000),
            'client_id'=>$this->haveClient(),
            'transaction_type'=>'application',
            'payload'=>['application_id'=>$this->haveApplication()],
            'sent_by'=>$this->haveUser(),
            'approved'=>false,
            'approved_by'=>false,
            'approved_on'=>false,
            'scheduled'=>0,
            'schedule_id'=>null,
            'fund_manager_id'=>$f_id
        ];

        return $laravel->haveRecord('client_transaction_approval', $formData);
    }

    public function haveCampaignType()
    {
        $this->haveManyRecords('statement_campaign_types', [
            ['name'=>'manual', 'slug'=>'manual'],
            ['name'=>'automated', 'slug'=>'automated']
        ]);

        return \StatementCampaignType::where('name', 'automated')->first()->id;
    }

    public function haveStatementCampaign()
    {
        $laravel = $this->getModule('Laravel4');
        $formData = [
            'name'=>'Automatic Campaign Tester',
            'sender_id'=>$this->haveUser(),
            'sent'=>null,
            'send_date'=>Carbon::today()->addMonthsNoOverflow(4)->toDateString(),
            'closed'=>null,
            'type_id'=>$this->haveCampaignType(),
            'closed_on'=>null,
            'closed_by'=>null
        ];

        return $laravel->haveRecord('statement_campaigns', $formData);
    }

    public function haveTransactionApproval()
    {
        //Created manually since creating automatically cannot carry a create_on date
        $laravel = $this->getModule('Laravel4');

        $app_id = $this->haveApplication();

        $app = \ClientInvestmentApplication::find($app_id);

        $formData = [
            'client_id'=>$app->client_id,
            'transaction_type'=> 'application',
            'payload'=>'a:1:{s:14:"application_id";i:32;}',
            'sent_by'=>$this->haveUser(),
            'approved'=>null,
            'approved_by'=>null,
            'approved_on'=>null,
            'scheduled'=>0,
            'schedule_id'=>null,
            'fm_id'=>$this->haveFundManager(),
            'created_at'=>Carbon::today()->toDateString()
        ];

        return $laravel->haveRecord('client_transaction_approval', $formData);
    }
}
