<?php

namespace Helper\Resources;

use Carbon\Carbon;
use Faker\Factory as Faker;
use Cytonn\Clients\ClientRepository;
use RealEstate\PaymentSchedules;

class Realestate extends Base
{
    protected $unit_size;

    /**
     * Have a Real Estate Project
     *
     * @return static
     * @throws \Codeception\Exception\ModuleException
     */
    public function haveProject()
    {
        $laravel = $this->getModule('Laravel4');

        $faker = Faker::create();

        $this->setupProject();

        $formData = [
            'id'=>rand(1, 1000000),
            'name'=>'The Alma',
            'long_name'=>'The Alma',
            'project_manager'=>$faker->name,
            'location'=>$faker->streetAddress,
            'vendor'=>'Cytonn LLp',
            'code'=>$faker->randomLetter.$faker->randomDigit,
            'development_description'=>$faker->paragraph,
            'type_id'=>\ProjectType::first()->id,
            'reservation_fee'=>null,
            'fund_manager_id'=>\FundManager::find(1)->id,
            'commission_calculator'=>'payment_plan_based',
            'bank_name'=>$faker->name,
            'bank_branch_name'=>$faker->word,
            'bank_account_name'=>$faker->name,
            'bank_account_number'=>$faker->bankAccountNumber,
            'bank_swift_code'=>$faker->postcode,
            'bank_clearing_code'=>$faker->postcode
        ];

        return $laravel->haveRecord('projects', $formData);
    }


    /**
     * Prepare test data for RE Units
     *
     * @return array
     * @throws \Codeception\Exception\ModuleException
     */
    public function getUnitTestData()
    {
        $faker = Faker::create();

        return [
            'project_id'                =>  $this->haveProject(),
            'number'                    =>  $faker->word . $faker->numberBetween(100, 999),
            'size_id'                   =>  \RealestateUnitSize::latest()->first()->id,
            'type_id'                   =>  \RealestateUnitType::latest()->first()->id
        ];
    }

    /**
     * Have RE Units
     *
     * @return mixed
     */
    public function haveUnit()
    {
        $laravel = $this->getModule('Laravel4');

        $unit_id = $laravel->haveRecord('realestate_units', $this->getUnitTestData());
        $unit = \RealestateUnit::find($unit_id);

        return $unit;
    }

    /**
     * Have a payment plan
     *
     * @return mixed
     */
    public function havePaymentPlan()
    {
        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('realestate_payment_plans', ['name' => 'Cash', 'slug'=>'cash']);
    }

    /**
     * Have RE Tranches
     *
     * @return mixed
     */
    public function haveTranche()
    {
        $laravel = $this->getModule('Laravel4');

        $formData = [
            'project_id'=>$this->haveProject(),
            'payment_plan_id'=>$this->havePaymentPlan(),
            'name'=>'Early Bird Offer'
        ];

        return $laravel->haveRecord('realestate_unit_tranches', $formData);
    }

    /**
     * Have Tranche Sizing
     *
     * @return mixed
     */
    public function haveTrancheSizing(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $tranche = isset($params['tranche_id']) ? \RealEstateUnitTranche::find($params['tranche_id']) : \RealEstateUnitTranche::find($this->haveTranche());

        return $laravel->haveRecord('realestate_unit_tranche_sizing', $ruts = [
            'tranche_id'        =>  $tranche->id,
            'number'            =>  rand(15, 500),
            'size_id'           =>  \RealestateUnitSize::latest()->first()->id
        ]);
    }

    /**
     * Have Tranche Pricing
     *
     * @return mixed
     */
    public function haveTranchePricing(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $sizing = isset($params['sizing_id']) ? \RealEstateUnitTrancheSizing::find($params['sizing_id']) : \RealEstateUnitTrancheSizing::find($this->haveTrancheSizing());
        $plan = isset($params['payment_plan_id']) ? \RealEstatePaymentPlan::find($params['payment_plan_id']) : \RealEstatePaymentPlan::find($this->havePaymentPlan());

        return $laravel->haveRecord('realestate_unit_tranche_pricing', [
            'payment_plan_id'   =>  $plan->id,
            'sizing_id'         =>  $sizing->id,
            'price'             =>  rand(800000, 1200000)
        ]);
    }


    /**
     * Have Fund Manager
     *
     * @param null $id
     * @return int|null
     */
    public function haveFundManager($id = null)
    {
        !is_null($id) ? : $id = 1;

        $laravel = $this->getModule('Laravel4');

        if (\FundManager::find($id)) {
            return $id;
        }

        $laravel->haveRecord('fund_managers', ['id'=>$id, 'name'=>'CIM']);

        return $id;
    }

    /**
     * Have a client
     *
     * Returns client id
     */
    public function haveClient(array $params = [])
    {
        $this->setupClient();

        $fund_manager_id = isset($params['fund_manager_id']) ? $params['fund_manager_id'] : $fund_manager_id = 1;

        $laravel = $this->getModule('Laravel4');

        $faker = Faker::create();

        $this->haveFundManager($fund_manager_id);

        //Client Contact
        $contact = $laravel->haveRecord('contacts', [
            'title_id'=>2,
            'firstname'=> $faker->firstName,
            'middlename'=>$faker->name,
            'lastname'=> $faker->lastName,
            'email' => $faker->email,
            'entity_type_id'=>1,
            'phone'=>$faker->phoneNumber,
            'gender_id'=>1
        ]);

        $client_code = (new ClientRepository())->suggestClientCode();

        //Create Client
        return $laravel->haveRecord('clients', [
                    'contact_id' => $contact,
                    'taxable'=>true,
                    'client_code'=>$client_code,
                    'client_type_id' => 1,
                    'phone'=>070654533453,
                    'town'=>'Nairobi',
                    'fund_manager_id'=>\FundManager::find($fund_manager_id)->id,
                    'employment_id'=>1,
                    'employer_address'=>$faker->address,
                    'investor_bank_branch'=>$faker->city,
                    'investor_bank'=>$faker->name,
                    'investor_account_name'=>$faker->bankAccountNumber,
                    'created_at'=>Carbon::today()->toDateString()
                ]);
    }


    /**
     * Have RE Unit Holding
     *
     * @return mixed
     */
    public function haveUnitHolding()
    {
        $laravel = $this->getModule('Laravel4');

        $unit = $this->haveUnit();

        $formData = [
            'unit_id'=> $unit->id,
            'client_id'=>$this->haveClient(),
            'approval_id'=>null,
            'active'=>1,
            'payment_plan_id'=> $this->havePaymentPlan(),
            'tranche_id'=>$this->haveTranche()

        ];
        return $laravel->haveRecord('unit_holdings', $formData);
    }

    /**
     * Have unit size numbers
     *
     * @return mixed
     */
    public function haveSizeNumbers()
    {
        $laravel = $this->getModule('Laravel4');

        return $laravel->haveRecord('realestate_size_numbers', [
                'size_id'           =>  \RealestateUnitSize::latest()->first()->id,
                'project_id'        =>  $this->haveProject(),
                'number'            => rand(50, 10000)
        ]);
    }


    /**
     * Have RE payment schedule
     */

    public function haveSchedule(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $holding = isset($params['unit_holding_id']) ? \UnitHolding::find($params['unit_holding_id']) : \UnitHolding::find($this->haveUnitHolding());

        return $laravel->haveRecord('realestate_payments_schedules', [
            'unit_holding_id'=>$this->haveUnitHolding(),
            'amount'=>2342545,
            'description'=>'Reservation fee',
            'fully_paid'=>null,
            'date'=>Carbon::today()->toDateString(),
            'payment_type_id'=>\RealEstatePaymentType::latest()->first()->id
        ]);
    }


    /**
     * Have RE Payments
     */
    public function haveREPayment(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $faker = Faker::create();

        $holding = isset($params['holding_id']) ? \UnitHolding::find($params['holding_id']) : \UnitHolding::find($this->haveUnitHolding());

        $schedule = isset($params['schedule_id']) ? \RealEstatePaymentSchedule::find($params['schedule_id']) : \RealEstatePaymentSchedule::find($this->haveSchedule());

        $formData = [
            'date'=>Carbon::today()->toDateString(),
            'holding_id'=>$holding->id,
            'description'=>$faker->paragraph(),
            'amount'=>rand(465780, 10000787878000),
            'payment_type_id'=>\RealEstatePaymentType::latest()->first()->id,
            'approval_id'=>null,
            'schedule_id'=>$schedule->id
        ];

        return $laravel->haveRecord('realestate_payments', $formData);
    }


    /**
     * Have Commission Recipients
     *
     * @return mixed
     */
    public function haveCommissionRecepient()
    {
        $laravel = $this->getModule('Laravel4');

        //Commission Rates
        $commission_rates = [
            'id'=> rand(1, 2563),
            'name'=>'Staff - KES',
            'rate'=>0.5,
            'currency_id'=>$laravel->haveRecord('currencies', ['code'=>'KES', 'name'=>'Kenya Shillings', 'symbol'=>'KES'])
        ];

        $laravel->haveRecord('commission_rates', $commission_rates);

        //Commision Recipient Types
        $commission_recipient_type = [
            'id'=>rand(1, 365673),
            'name'=>'Staff',
            'rate_id'=>$commission_rates['id']
        ];
        $laravel->haveRecord('commission_recepient_type', $commission_recipient_type);


        //Commission Recipients
        $commission_recipient = [
            'recipient_type_id'=>$commission_recipient_type['id'],
            'staff_id'=>null,
            'email'=>'email@example.com',
            'name'=>'Name'
        ];

        return $laravel->haveRecord('commission_recepients', $commission_recipient);
    }

    /**
     * Have RE reservation form
     *
     * @param array $params
     */
    public function haveReservationForm(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $holding = isset($params['holding_id']) ? \UnitHolding::find($params['holding_id']) : \UnitHolding::find($this->haveUnitHolding());

        $client = isset($params['client_id']) ? \Client::find($params['client_id']) : \Client::find($this->haveClient());

        $formData = array_except($this->getClientTestData(), [
            'dob',
            'gender_id',
            'residential_address',
            'telephone_cell',
            'method_of_contact_id',
            'new_client'
        ]);

        $formData['holding_id'] = $holding->id;
        $formData['client_id'] = $client->id;

        $laravel->haveRecord('reservation_forms', $formData);
    }

    /**
     * @return array
     */
    public function getProjectTestData()
    {
        $I = $this->getModule('Laravel4');
        $I->haveRecord('project_types', $pt = ['id'=>rand(1, 200), 'name'=>'Apartments']);
        $I->haveRecord('fund_managers', $fm = ['id'=>rand(1, 1000), 'name'=>'CIM']);
        $faker = Faker::create();
        return [
            'code'                      => $faker->numberBetween(100, 100000),
            'name'                      => $faker->firstName,
            'long_name'                 => $faker->firstName,
            'project_manager'           => $faker->name,
            'location'                  => $faker->streetName . ', ' . $faker->city,
            'vendor'                    => $faker->company,
            'development_description'   => $faker->realText($maxNbChars = 200, $indexSize = 2),
            'reservation_fee'           => $faker->numberBetween($min = 800000, $max = 1200000),
            'type_id'                   => $pt['id'],
            'fund_manager_id'           => $fm['id'],
            'commission_calculator'     => 'payment_plan_based',
            'bank_name'                 => $faker->company,
            'bank_branch_name'          => strtoupper($faker->streetName),
            'bank_account_name'         => $faker->company,
            'bank_account_number'       => $faker->creditCardNumber,
            'bank_swift_code'           => $faker->word,
            'bank_clearing_code'        => $faker->numberBetween(100, 100000),
        ];
    }

    /**
     * Have RE Commissions
     */
    public function haveRECommissions(array $params = [])
    {

        $laravel = $this->getModule('Laravel4');

        $holding = isset($params['holding_id']) ? \UnitHolding::find($params['holding_id']) : \UnitHolding::find($this->haveUnitHolding());

        $recipient = isset($params['recipient_id']) ? \CommissionRecepient::find($params['recipient_id']) : \CommissionRecepient::find($this->haveCommissionRecepient());

        $formData = [
            'recipient_id'=> $recipient->id,
            'amount'=>rand(1000, 1000000000),
            'holding_id'=>$holding->id,
            'awarded'=>0,
        ];

        return $laravel->haveRecord('realestate_commissions', $formData);
    }

    /**
     * @return array
     * @throws \Codeception\Exception\ModuleException
     */
    public function getClientTestData()
    {
        $faker = Faker::create();
        $I = $this->getModule('Laravel4');
        $I->haveRecord('titles', $title = ['id'=>rand(1, 100000), 'name'=>'Mr']);
        $I->haveRecord('contact_method', $cm = ['id'=>rand(1, 1000000), 'description'=>'email']);
        $I->haveRecord('countries', $country = [
            'id'                        =>  rand(1, 1000000),
            'iso_abbr'                  =>  $faker->countryCode,
            'name'                      =>  $faker->country
        ]);
        $I->haveRecord('client_type', $ct_individual = ['id'=>rand(1, 1000000), 'name'=>'individual']);
        $I->haveRecord('client_type', $ct_corporate = ['id'=>rand(101, 200000), 'name'=>'corporate']);
        $I->haveRecord('contact_entity_types', ['id'=>$ct_individual['id'], 'name'=>'individual']);
        $I->haveRecord('contact_entity_types', ['id'=>$ct_corporate['id'], 'name'=>'corporate']);
        return [
            'client_type_id'            =>  $ct_individual['id'],
            'title_id'                  =>  $title['id'],
            'firstname'                 =>  $faker->firstName,
            'lastname'                  =>  $faker->lastName,
            'middlename'                =>  $faker->lastName,
            'dob'                       =>  $faker->dateTimeThisCentury->format('Y-m-d'),
            'gender_id'                 =>  rand(1, 2),
            'corporate_registered_name' =>  $faker->company,
            'corporate_trade_name'      =>  $faker->company,
            'registered_address'        =>  $faker->address,
            'registration_number'       =>  $faker->creditCardNumber,
            'email'                     =>  $faker->email,
            'postal_address'            =>  $faker->address,
            'postal_code'               =>  $faker->postcode,
            'town'                      =>  $faker->city,
            'street'                    =>  $faker->streetName,
            'country_id'                =>  $country['id'],
            'nationality_id'            =>  $country['id'],
            'residential_address'       =>  $faker->postcode . ' ' . $faker->city,
            'phone'                     =>  $faker->phoneNumber,
            'telephone_office'          =>  $faker->phoneNumber,
            'telephone_home'            =>  $faker->phoneNumber,
            'telephone_cell'            =>  $faker->phoneNumber,
            'id_or_passport'            =>  $faker->creditCardNumber,
            'pin_no'                    =>  $faker->creditCardNumber,
            'contact_person_title'      =>  $title['id'],
            'contact_person_lname'      =>  $faker->lastName,
            'contact_person_fname'      =>  $faker->firstName,
            'investor_account_name'     =>  $faker->firstName . ' ' . $faker->lastName,
            'investor_account_number'   =>  $faker->bankAccountNumber,
            'investor_bank'             =>  $faker->company,
            'investor_bank_branch'      => $faker->city,
            'investor_clearing_code'    => $faker->postcode,
            'investor_swift_code'       => $faker->postcode,
            'method_of_contact_id'      =>  $cm['id'],
            'new_client'                => '',
            'client_id'                 =>  ''
        ];
    }

    /**
     * @return array
     */
    public function getAdditionalPurchaserRequiredFields()
    {
        return [
            'title_id',                 'lastname',                 'middlename',
            'firstname',                'gender_id',                'dob',
            'pin_no',                   'id_or_passport',           'email',
            'postal_code',              'postal_address',           'country_id',
            'telephone_office',         'telephone_home',           'telephone_cell',
            'residential_address',      'town',                     'method_of_contact_id'
        ];
    }
}
