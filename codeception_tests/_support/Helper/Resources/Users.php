<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 10/23/16
 * Time: 2:08 PM
 */

namespace Helper\Resources;

use Cytonn\Authorization\Permission;
use Faker\Factory as Faker;


use Cytonn\Authorization\Role;

class Users extends Base
{

    /**
     * Have Users
     *
     * @return mixed
     */
    public function haveUser()
    {
        $faker = Faker::create();

        $I = $this->getModule('Laravel4');

        if (!\ContactEntity::where('name', 'individual')->first()) {
            $I->haveRecord('contact_entity_types', ['id'=>1, 'name'=>'individual']);
        }

        $contacts = [
            'id'=>rand(1, 1000),
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
            'email'=>$faker->email,
            'entity_type_id'=>1
        ];
        $I->haveRecord('contacts', $contacts);


        $user_id = $faker->numberBetween(1, 1000);
        $users = [
            'id'=>$user_id,
            'email'=>$faker->email,
            'username'=>$faker->userName,
            'password'=>\Hash::make('secret'),
            'contact_id'=>$contacts['id']
        ];
        $I->haveRecord('users', $users);

        //Staff
        $staff = [
            'id'=>rand(1, 10000),
            'user_id'=>$user_id,
            'contact_id'=>$contacts['id'],
            'title'=>$faker->title,
            'qualification'=>'CFA'
        ];

        $I->haveRecord('staff', $staff);

        return $user_id;
    }

    /**
     * Have User Roles: authoritaire roles
     */
    public function haveUserRoles()
    {
        $this->haveManyRecords('authoritaire_roles', [
            ['id'=>1, 'name'=>'superadmin', 'description'=>'Super Administrator'],
            ['id'=>2, 'name'=>'admin', 'description'=>'System Administrator'],
            ['id'=>3, 'name'=>'staff', 'description'=>'Staff Member'],
            ['id'=>4, 'name'=>'client', 'description'=>'Client'],
        ]);

        return Role::find(1)->id;
    }

    /**
     * Have User Permissions: authoritaire permissions
     */
    public function haveUserPermissions()
    {
        $this->haveManyRecords('authoritaire_permissions', [
            ['id'=>1, 'name'=>'addinvestment', 'description'=>'Add an investment to the system, withdraw, rollover or topup'],
            ['id'=>2, 'name'=>'approveclientinvestment', 'description'=>'Approve a client investment transaction'],
            ['id'=>3, 'name'=>'sendstatements', 'description'=>'Send client statements'],
            ['id'=>4, 'name'=>'approvestatements', 'description'=>'Approve statements to be sent'],
        ]);

        return Permission::find(1)->id;
    }

    /**
     * Have Assigned Permissions
     */
    public function haveAssignedPermissions(array $params = [])
    {

        $laravel = $this->getModule('Laravel4');

        $permission = isset($params['permission_id']) ? Permission::find($params['permission_id']) : Permission::find($this->haveUserPermissions());

        $user = isset($params['user_id']) ? \User::find($params['user_id']) : \User::find($this->haveUser());

        $laravel->haveRecord('authoritaire_user_permissions', [
            'permission_id'=>$permission->id,
            'user_id'=>$user->id
        ]);
    }



    /**
     * Have Client User
     */
    public function haveClientUser()
    {
        $laravel = $this->getModule('Laravel4');

        $faker = Faker::create();

        $formData = [
            'username'=>$faker->userName,
            'email'=>$faker->email,
            'phone_country_code'=>+254,
            'phone'=>$faker->phoneNumber,
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
        ];

        return $laravel->haveRecord('client_users', $formData);
    }

    /**
     * Have role permissions
     */
    public function haveRolePermissions(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $role = isset($params['role_id']) ? Role::find($params['role_id']) : Role::find($this->haveUserRoles());

        $formData = [
            'permission_id'=>$this->haveUserPermissions(),
            'role_id'=>$role->id
        ];

        return $laravel->haveRecord('authoritaire_role_permissions', $formData);
    }

    /**
     * Have FA users
     */
    public function haveFaUsers(array $params = [])
    {
        $laravel = $this->getModule('Laravel4');

        $recipient =  \CommissionRecepient::find($params['fa_id']);

        $user = \ClientUser::find($params['user_id']);

        return $laravel->haveRecord('fa_users', ['fa_id'=>$recipient->id, 'user_id'=>$user->id ]);
    }
}
