<?php


class ClientSummaryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test view client investment summary on Clients Summary Grid
     */
    public function testClientSummary()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can be able to view the client summary grid');

        //Login and variables
        $I->haveClient();
        $I->haveProduct();
        $inv = ClientInvestment::find($I->haveClientInvestment());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/investments/summary');
        $I->see('Client Summary');
        $I->see(Product::find($inv->product_id)->name);

        //Grid
        $client = Client::find($inv->client_id);
        $I->sendAjaxGetRequest('/api/clients/summary/'.$inv->product_id);
        $I->seeResponseCodeIs(200);
        $I->see($client->client_code);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id));
    }
}
