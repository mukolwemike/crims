<?php
use Illuminate\Support\Facades\Hash;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/26/16
 * Time: 12:00 PM
 */
class ClientsTest extends \Codeception\TestCase\Test
{
    use \Cytonn\Core\Validation\ValidatorTrait;

    /**
     * @var \FunctionalTester
     */
    protected $tester;


    /**
     *Test view clients on grid
     */
    public function testClientGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test API data received in the All Clients page');

        $client_id = $I->haveClient();
        $client = Client::find($client_id);
        $I->assertInstanceOf(Client::class, $client);

        //page
        $I->amOnPage('/dashboard/clients');
        $I->see('Clients');
        $I->login();
        $I->seeAuthentication();
        $I->seeResponseCodeIs(200);

        //grid
        $I->sendAjaxGetRequest('/api/clients/all');
        $I->seeResponseCodeIs(200);
        $I->see($client->client_type_id);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($client_id));
        $I->see($client->id);
    }


    /**
     *Test view details for individual clients
     */
    public function testClientDetails()
    {
        $I = $this->tester;
        $I->wantTo('Test API data received in the Client-Details page');

        $client_id = $I->haveClient();
        $I->amOnPage("/dashboard/clients/details/".$client_id);
        $I->see('Client Details');
        $I->seeResponseCodeIs(200);
        $client = Client::find($client_id);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames(Client::find($client_id)));
        $I->see($client->contact->phone);
        $I->see($client->contact->email);
        $I->see($client->contact->gender_id);

        //Employment Details
        $I->see('Employment Details');
        $I->see($client->employment_id);
        $I->see($client->employer_address);

        //bank details
        $I->see('Bank Information');
        $I->see($client->investor_bank_branch);
        $I->see($client->investor_account_name);
    }

    /**
     *Test add a bank account associated to a given client
     */
    public function testAddBankAccount()
    {
        $I = $this->tester;

        $client_id = $I->haveClient();
        $I->wantTo('Add Bank Account');
        $user = $I->login();
        $user_id = $user->id;
        $I->amOnPage('/dashboard/clients/details/'.$client_id);

        //Add bank account
        $I->wantTo('Add additional bank account for a client');
        $I->see('Add a bank account');
        $formData = [
            'account_name'=>'DanielInvestor',
            'account_number'=>22332,
            'bank'=>'Equity',
            'branch'=>'Valley Road',
            'clearing_code'=>5678,
            'swift_code'=>344
        ];
        $I->sendAjaxPostRequest(route('bank_details_add_path', [$client_id]), $formData);

        $I->seeRecord('client_transaction_approval', ['client_id'=>$client_id, 'sent_by'=>$user_id]);
        $id = ClientTransactionApproval::where('client_id', $client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->see('Awaiting Approval');

        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_bank_accounts', $formData);
    }

    /**
     *Test edit a bank account associated to a specified client
     */
    public function testEditBankAccount()
    {
        $I = $this->tester;

        $client_id =$I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->amOnPage("/dashboard/clients/details/".$client_id);

        $I->wantTo('Edit existing client bank account');
        $formData = [
            'account_name'=>'DanielInvestor',
            'account_number'=>22332,
            'bank'=>'Equity',
            'branch'=>'Valley Road',
            'clearing_code'=>4343,
            'swift_code'=>3444
        ];
        $I->sendAjaxPostRequest(route('bank_details_add_path', [$client_id]), $formData);
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_transaction_approval', ['client_id'=>$client_id, 'sent_by'=>$user_id]);
        $id = ClientTransactionApproval::where('client_id', $client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->see('Awaiting Approval');

        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_bank_accounts', $formData);
    }

    /**
     *Test make Coop payments for Coop clients
     */
    public function testMakeCoopPayments()
    {
        $I = $this->tester;

        $I->haveClient();
        $inv_id =$I->haveClientInvestment();
        $inv  = ClientInvestment::find($inv_id);
        $user = $I->login();
        $user_id = $user->id;
        $I->amOnPage("/dashboard/clients/details/".$inv->client_id);

        $I->wantTo('Make Coop payments');
        $acc_id = $I->haveCustodialAccount();
        $I->haveRecord('custodial_transaction_types', ['name'=>'FI']);
        $formData = [
            'client_id'=>$inv->client_id,
            'account_id'=>$acc_id,
            'date'=> Carbon::today()->addMonthsNoOverflow(1)->toDateString(),
            'amount'=>11,
            'narrative'=>'I need the test to pass and pass Again'
        ];
        $I->sendAjaxPostRequest(route('make_coop_payment', [$inv->client_id]), $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Coop payment have been saved for approval');
        $I->seeRecord('client_transaction_approval', ['client_id'=>$inv->client_id, 'sent_by'=>$user_id]);

        //Proceed to approve
        $id = ClientTransactionApproval::where('client_id', $inv->client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
    }


    /**
     *Test view Coop payments on grid
     */
    public function testPayments()
    {
        $I = $this->tester;

        $client_id = $I->haveClient();
        $I->amOnPage("/dashboard/coop/clients/payments/".$client_id);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames(Client::find($client_id)));
        $I->see('Coop Payments');
    }

    /**
     *Test investment on Coop Products
     */
    public function testInvestInProduct()
    {
        $I = $this->tester;
        $I->wantTo('Test investment in Product');

        $inv_id =$I->haveClientInvestment();
        $inv  = ClientInvestment::find($inv_id);
        $user = $I->login();
        $user_id = $user->id;
        $I->amOnPage("/dashboard/clients/details/".$inv->client_id);

        $I->haveRecord('client_investment_types', ['id'=>rand(100, 200), 'name'=>'investment']);
        $I->haveRecord('custodial_transaction_types', ['name'=>'FI']);
        $I->haveRecord('custodial_transaction_types', ['name'=>'FO']);
        $I->haveRecord('product_plans', ['client_id'=>$inv->client_id, 'product_id'=>$I->haveProduct(), 'active'=>1]);
        $I->haveRecord('commission_recepients', ['name'=> 'dmc']);
        $I->haveRecord('coop_payments', ['client_id'=>$inv->client_id,  'date'=>Carbon::today(), 'amount'=>200001]);
        $formData =[
            'approval_id'=>$inv->approval_id,
            'client_id'=>$inv->client_id,
            'product_plan_id'=>ProductPlan::first()->id,
            'investment'=>'investment',
            'deduct'=>'yes',
            'invested_date'=>Carbon::today()->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(3)->toDateString(),
            'amount'=>200001,
            'interest_rate'=>3,
            'narrative'=>'The test must pass',
            'commission_recipient'=>CommissionRecepient::first()->id,
            'commission_rate'=>1
        ];
        $I->sendAjaxPostRequest(route('invest_in_coop_product', [$inv->client_id]), $formData);
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_transaction_approval', ['client_id'=>$inv->client_id, 'sent_by'=>$user_id]);

        //Proceed to approve
        $id = ClientTransactionApproval::where('client_id', $inv->client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
    }


    /**
     *Test topup a Coop investment
     */
    public function testTopUpInvestment()
    {
        $I = $this->tester;


        $inv_id =$I->haveClientInvestment();
        $inv  = ClientInvestment::find($inv_id);
        $user = $I->login();
        $user_id = $user->id;
        $I->amOnPage("/dashboard/clients/details/".$inv->client_id);

        $I->wantTo('Test Top Up Investment');
        $I->haveRecord('client_investment_types', ['id'=>rand(100, 200), 'name'=>'topup']);
        $I->haveRecord('custodial_transaction_types', ['name'=>'FI']);
        $I->haveRecord('custodial_transaction_types', ['name'=>'FO']);
        $I->haveRecord('product_plans', ['client_id'=>$inv->client_id, 'product_id'=>$I->haveProduct(), 'active'=>1]);
        $I->haveRecord('commission_recepients', ['name'=> 'dmc']);
        $I->haveRecord('coop_payments', ['client_id'=>$inv->client_id,  'date'=>Carbon::today(), 'amount'=>200001]);
        $formData = [
            'approval_id'=>$inv->approval_id,
            'client_id'=>$inv->client_id,
            'product_plan_id'=>ProductPlan::first()->id,
            'investment'=>'topup',
            'deduct'=>'yes',
            'invested_date'=>Carbon::today()->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(3)->toDateString(),
            'amount'=>20000,
            'interest_rate'=>3,
            'narrative'=>'The test must pass',
            'commission_recipient'=>CommissionRecepient::first()->id,
            'commission_rate'=>4
        ];
        $I->sendAjaxPostRequest(route('top_up_coop_investment', [$inv->client_id]), $formData);
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_transaction_approval', ['client_id'=>$inv->client_id, 'sent_by'=>$user_id]);

        //Proceed to approval
        $id = ClientTransactionApproval::where('client_id', $inv->client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
    }

    /**
     *Test buy shares for Coop clients
     */
    public function testBuyShares()
    {
        $I = $this->tester;

        $client_id = $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->seeAuthentication();
        $I->amOnPage("/dashboard/clients/details/".$client_id);

        $I->wantTo('Test Buy Shares');
        $formData = [
            'client_id'=>$client_id,
            'deduct'=>'yes',
            'custodial_account_id'=>4,
            'date'=>'26/10/2016',
            'entity_id'=> 1,
            'category_id'=>1,
            'amount'=>20222,
            'narrative'=>'The test must pass',
            'purchase_price'=>20
        ];
        $I->sendAjaxPostRequest(route('buy_coop_shares', [$client_id]), $formData);
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_transaction_approval', ['client_id'=>$client_id, 'sent_by'=>$user_id]);
    }

    /**
     *Test view investments summary for Coop products
     */
    public function testClientsInvestmentsSummary()
    {
        $I = $this->tester;
        $I->wantTo('Test whether user can view clients\' investments summary');

        //Client CMS Summary
        $client_id = $I->haveClient();
        $I->login();
        $I->seeAuthentication();
        $I->amOnPage("/dashboard/investments/summary/client/".$client_id);
        $I->see('Client Details');
        $I->seeResponseCodeIs(200);
        $client = Client::where('id', $client_id)->first();
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames(Client::find($client->id)));
        $I->see(\Cytonn\Presenters\DatePresenter::formatDate($client->created_at));
    }

    /**
     *Test add a Coop Client
     */
    public function testAddEditClient()
    {
        $I = $this->tester;
        $I->wantTo('Test whether user can Add/Edit Client');

        //Add/Edit Client
        $client_id = $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->seeAuthentication();
        $I->amOnPage('/dashboard/clients/create/'.$client_id);
        $I->see('Add/Edit Client');
        $formData = Client::find($client_id)->toArray();
        unset($formData['updated_at']);
        unset($formData['created_at']);

        $I->sendAjaxPostRequest('/dashboard/clients/create/'.$client_id, $formData);
        $I->seeResponseCodeIs('200');
        $I->seeRecord('client_transaction_approval', ['client_id'=>$client_id, 'sent_by'=>$user_id]);

        //Proceed to Approval
        $id = ClientTransactionApproval::where('client_id', $client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/'.$id);
        $I->see('Awaiting Approval');

        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('clients', ['uuid'=>$formData['uuid']]);
        $I->see('The client has been edited');
    }

    /**
     *Test view bank instructions for all clients on grid
     */
    public function testBankInstructions()
    {
        $I = $this->tester;
        $I->wantTo('Test whether users can view the Bank Instructions page for all clients');

        //Focus on an exist
        $client_id = $I->haveClient();
        $client = Client::find($client_id);
        $I->assertInstanceOf(Client::class, $client);

        //page
        $I->amOnPage('/dashboard/clients/bank-instructions');
        $I->see('Bank Instructions');
        $I->login();
        $I->seeAuthentication();
        $I->seeResponseCodeIs(200);

        //grid
        $I->sendAjaxGetRequest('/api/clients/bank-instruction');
        $I->seeResponseCodeIs(200);
    }

    /**
     *Test view ban instruction details for individual clients
     */
    public function testIndividualBankIntructions()
    {
        $I = $this->tester;
        $I->wantTo('Test whether users can view the Bank Instructions page for individual clients');

        $I->haveClient();

        $inv_id = $I->haveClientInvestment();

        $I->haveRecord('bank_instructions', $instr = ['id'=>rand(1, 9999), 'investment_id'=>$inv_id, 'amount'=>200]);

        //Page
        $I->amOnPage('/dashboard/clients/bank-instructions/show/'.$instr['id']);
        $I->seeResponseCodeIs(200);
    }
}
