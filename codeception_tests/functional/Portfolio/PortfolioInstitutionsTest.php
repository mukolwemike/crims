<?php


class PortfolioInstitutionsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    // tests
    /**
     *Test add a portfolio institution
     */
    public function testAddPortfolioInstitution()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can add a portfolio Institution');

        //Login and variables
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/institutions/add');
        $I->see('Add or Edit an Institution');

        //Add Portfolio Institution
        $I->fillField('Name', 'Test Add Portfolio Institution');
        $I->click('Save');

        //See Institution Added
        $I->seeRecord('portfolio_investors', ['name'=>'Test Add Portfolio Institution']);
        $inst = PortfolioInvestor::where('name', 'Test Add Portfolio Institution')->first();
        $I->see('Institution saved succesfully');
        $I->seeInCurrentUrl('/dashboard/portfolio/institutions/add/'.$inst->id);

        //Edit Portfolio Institution
        $I->fillField('Name', 'Test Edit Portfolio Institution');
        $I->click('Save');
        $I->seeRecord('portfolio_investors', ['name'=>'Test Edit Portfolio Institution']);
        $I->see('Institution saved succesfully');
    }

    /**
     *Test view an existing portfolio institution
     */
    public function testViewPortfolioInstitution()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can see Portfolio Investments on Grid');

        //Login and variables
        $I->haveClient();
        $inst = PortfolioInvestor::find($I->havePortfolioInstitution());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/institutions');
        $I->see('Portfolio institutions');

        $I->sendAjaxGetRequest('/api/portfolio/institutions');
        $I->seeResponseCodeIs(200);

        $I->see($inst->id);
        $I->see($inst->name);
    }

    /**
     *Test view details of an individual portfolio institution
     */
    public function testViewIndividualPortfolioInstitution()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can see details for Individual Portfolio Investments');

        //Login and variables
        $I->haveClient();
        $inst = PortfolioInvestor::find($I->havePortfolioInstitution());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/institutions/details/'.$inst->id);
        $I->see('Institution details');
        $I->see($inst->name);
    }
}
