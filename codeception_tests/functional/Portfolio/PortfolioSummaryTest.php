<?php

use App\Cytonn\Models\Portfolio\DepositHolding;
use Cytonn\Portfolio\DepositHoldingRepository;
use Cytonn\Portfolio\PortfolioRepository;

class PortfolioSummaryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }


    /**
     *Tet portfolio summary
     */
    public function testPortfolioSummaryGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can be able view the portfolio investments summary grid');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/summary');
        $I->see('Portfolio Summary');

        $I->sendAjaxGetRequest('/api/portfolio/summary/'.$acc->currency->id);
        $I->seeResponseCodeIs(200);

        //See Grid
        $I->see($inv->portfolioInvestor->name);
    }

    /**
     *Test Portfolio deposit analysis
     */
    public function testPortfolioDepositAnalysis()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can perform portfolio deposit analysis');

        //Login and variables
        $inv = DepositHolding::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/summary');
        $I->see('Portfolio Summary');

        //Go to analysis
        $I->click('Deposit Analysis');
        $I->seeInCurrentUrl('/dashboard/portfolio/analysis');
        $I->see('Portfolio Analysis');

        //Analysis Details
        $I->see('Deposit Analysis: '.$acc->currency->name);
        $cash = (new DepositHoldingRepository())->getPortfolioCashBalanceForCurrency($acc->currency->id);
        $deposit = (new DepositHoldingRepository())->getTotalPortfolioForCurrencyForCategory($acc->currency, $inv->fundType);
        $I->see(\Cytonn\Presenters\AmountPresenter::currency($cash));
        $I->see(\Cytonn\Presenters\AmountPresenter::currency($deposit));
    }

    /**
     *Test export and download portfolio analysis details
     */
    public function testExportPortfolioSummary()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can export portfolio analysis details to excel');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/summary');
        $I->see('Portfolio Summary');

        //TODO cannot download excel sheets: use acceptance tests
    }
}
