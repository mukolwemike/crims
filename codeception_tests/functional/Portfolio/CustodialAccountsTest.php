<?php
use Cytonn\Core\DataStructures\Carbon;

class CustodialAccountsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test create and view the created custodial accounts
     */
    public function testCreateViewCustodialAccount()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can create a custodial account');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/custodials');
        $I->see('Custodial accounts');

        //Create Account
        $I->click('Add new');
        $I->seeInCurrentUrl('/dashboard/portfolio/custodials/add/');
        $I->see('Add/Edit a custodial account');

        $formData = [
            'account_name'=> 'New Custodial Account',
            'bank_id'=>$I->haveBank(),
            'account_no'=>28390383,
            'currency_id'=>1
        ];
        $I->sendAjaxPostRequest('/dashboard/portfolio/custodial/add/', $formData);
        $I->seeResponseCodeIs(200);

        //See Account Created
        $I->seeRecord('custodial_accounts', [
            'account_name'=> 'New Custodial Account',
            'account_no'=>28390383,
            'currency_id'=>1
        ]);
        $I->see('Custodial account succesfully saved');

        //See account on grid
        $acc = CustodialAccount::where('account_name', 'New Custodial Account')->where('account_no', 28390383)->first();
        $I->seeInCurrentUrl('/dashboard/portfolio/custodials');
        $I->see('Custodial accounts');
        $I->see($acc->account_name);
        $I->see($acc->id);
        $I->see($acc->account_no);
    }

    /**
     *Test adding custodial accounts
     */
    public function testEditCustodialAccount()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can edit an existing custodial account');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/custodials/add/'.$acc->id);
        $I->see('Add/Edit a custodial account');

        $formData = [
            'account_name'=> 'Edited Custodial Account',
            'bank_id'=>$I->haveBank(),
            'account_no'=>45678876,
            'currency_id'=>1
        ];
        $I->sendAjaxPostRequest('/dashboard/portfolio/custodial/add/'.$acc->id, $formData);
        $I->seeResponseCodeIs(200);

        //See Account edited
        $I->seeRecord('custodial_accounts', [
            'account_name'=> 'Edited Custodial Account',
            'account_no'=>45678876,
            'currency_id'=>1
        ]);
        $I->see('Custodial account succesfully saved');

        //See edited account on grid
        $acc = CustodialAccount::where('account_name', 'Edited Custodial Account')->where('account_no', 45678876)->first();
        $I->seeInCurrentUrl('/dashboard/portfolio/custodials');
        $I->see('Custodial accounts');
        $I->see($acc->account_name);
        $I->see($acc->id);
        $I->see($acc->account_no);
    }

    /**
     *Test Custodial Transactions Grid
     */
    public function testCustodialTransactions()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view the Custodial Transactions Grid');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $trans = CustodialTransaction::find($I->haveCustodialTransaction(['custodial_account_id'=>$acc->id]));
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/custodials/details/'.$acc->id);
        $I->see('Custodial Accounts');

        //Account Details
        $I->see($acc->account_name);
        $I->see($acc->id);
        $I->see($acc->currency->name);

        //Transaction Details
        $I->see($trans->description);
        $I->see(\Cytonn\Presenters\DatePresenter::formatDate($trans->date));
        $I->see(CustodialTransactionType::find($trans->type)->name);
    }

    /**
     *Test custodial transactions withdraw
     */
    public function testCustodialTransactionsWithdraw()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can perform a Custodial Transactions Withdraw');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $type = $I->haveRecord('custodial_withdraw_type', ['code'=>'ECF', 'name'=>'Custodial Fees']);
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/custodials/details/' . $acc->id);
        $I->see('Custodial Accounts');
        $I->click('Transact');
        $I->seeInCurrentUrl('/dashboard/portfolio/custodials/transact/'.$acc->id);
        $I->see('Withdraw from Cutodial Account');

        //See Account Details
        $I->see($acc->account_name);
        $I->see($acc->currency->name);

        $formData = [
            'transaction'=>$type,
            'amount'=>200000,
            'date'=>Carbon::today()->addMonthsNoOverflow(3)->toDateString(),
            'narrative'=>'Time for a custodial withdraw'
        ];
        $I->sendAjaxPostRequest(route('custodial_withdraw', [$acc->id]), $formData);
        $I->seeResponseCodeIs(200);

        //Approval Requested
        $I->see('Transaction saved for approval');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'transaction_type'=>'custodial_payment']);

        //Approve
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('transaction_type', 'custodial_payment')->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->click('Approve');
        $I->dontSeeInCurrentUrl('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Payment has been made');
    }

    /**
     *Test custodial transactions transfer
     */
    public function testCustodialTransactionsTransfer()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can perform a Custodial Transactions Transfer');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/custodials/details/' . $acc->id);
        $I->see('Custodial Accounts');
        $I->click('Transact');
        $I->seeInCurrentUrl('/dashboard/portfolio/custodials/transact/'.$acc->id);
        $I->see('Withdraw from Cutodial Account');

        //See Account Details
        $I->see($acc->account_name);
        $I->see($acc->currency->name);

        $formData = [
            'destination_id'=>$acc->id,
            'effective_rate'=>20,
            'amount'=>30000,
            'exchange'=>23,
            'converted'=>true,
            'transfer_date'=>Carbon::today()->addMonthsNoOverflow(3)->toDateString(),
            'narrative'=>'Time for a Custodial Transfer'
        ];
        $I->sendAjaxPostRequest(route('custodial_transfer', [$acc->id]), $formData);
        $I->seeResponseCodeIs(200);

        //Approval Requested
        $I->see('The transfer has been saved for approval');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'transaction_type'=>'transfer']);

        //Approve
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('transaction_type', 'transfer')->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->click('Approve');
        $I->dontSeeInCurrentUrl('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('The transfer has been successfully executed');
    }

    /**
     *Test export and download custodial transaction details
     */
    public function testExportToExcel()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can export details of a custodial transaction to excel sheets');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $acc = CustodialAccount::find($inv->custodial_account_id);
        $type = $I->haveRecord('custodial_withdraw_type', ['code'=>'ECF', 'name'=>'Custodial Fees']);
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/custodials/details/' . $acc->id);
        $I->see('Custodial Accounts');

        //TODO cannot test file downloads: use acceptance test
        //Export
//        $I->sendAjaxPostRequest(route('export_details_to_excel_path'), [
//            'id'=>$acc->id,
//            'from'=>Carbon::today()->toDateString(),
//            'to'=>Carbon::today()->addMonthsNoOverflow(5)->toDateString()
//        ]);
//        $I->seeResponseCodeIs(200);
//        $I->seeInCurrentUrl('/dashboard/portfolio/custodials/details/' . $acc->id);
    }
}
