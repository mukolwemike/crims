<?php

use Cytonn\Core\DataStructures\Carbon;

class PortfolioInvestmentsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test add a portfolio investment
     */
    public function testAddPortfolioInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can add a portfolio Investment');

        //Login and variables
        $inst = PortfolioInvestor::find($I->havePortfolioInstitution());
        $I->haveCustodialTransactionType();
        $acc = $I->haveCustodialAccount();
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments/add');
        $I->see('Add a portfolio investment');

        //Fill FormData
        $formData = [
            'portfolio_investor_id'=>$inst->id,
            'fund_type_id'=>$I->haveFundTypes(),
            'custodial_account_id'=>$acc,
            'invested_date'=>Carbon::today()->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(4)->toDateString(),
            'on_call'=>true,
            'interest_rate'=>24,
            'taxable'=>true,
            'amount'=>2537839000
        ];
        $I->sendAjaxPostRequest('/dashboard/portfolio/investments/add', $formData);
        $I->seeResponseCodeIs(200);

        //See Approval Request
        $I->see('Portfolio Investment has been added for approval');
        $I->seeRecord('portfolio_transaction_approval', ['institution_id'=>$inst->id, 'sent_by'=>$user->id]);

        //Approve
        $approval = PortfolioTransactionApproval::where('institution_id', $inst->id)->where('sent_by', $user->id)->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->see($inst->name);
        $I->click('Approve');
        $I->see('Portfolio Investment has been approved and invested');
        $I->see('New Investment Successful');
    }

    /**
     *Test view existing portfolio investments
     */
    public function testViewPortfolioInvestments()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view existing portfolio Investments on Grid');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $I->haveClient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments');
        $I->see('Portfolio Investments');

        //Grid
        $I->sendAjaxGetRequest('/api/portfolio/investments/'.$inv->custodialAccount->currency_id);
        $I->seeResponseCodeIs(200);
        $I->see($inv->id);
        $I->see(PortfolioInvestor::find($inv->portfolio_investor_id)->name);
    }

    /**
     *Test redeem portfolio investments
     */
    public function testRedeemPortfolioInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can redeem Portfolio Investments');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments/redeem/'.$inv->id);
        $I->see('Redeem Investment');

        //Redeem
        $formData = [
            'investment_id'=>Crypt::encrypt($inv->id),
            'premature'=>true,
            'end_date'=>Carbon::today()->addMonthsNoOverflow(2)->toDateString(),
            'partial'=>false,
            'amount'=>false
        ];
        $I->sendAjaxPostRequest(route('portfolio_withdraw'), $formData);
        $I->seeResponseCodeIs(200);

        //Approval Requested
        $I->see('The withdrawal has been saved for confirmation');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'institution_id'=>$inv->portfolio_investor_id]);

        //Approve
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('institution_id', $inv->portfolio_investor_id)->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->see(PortfolioInvestor::find($inv->portfolio_investor_id)->name);
        $I->click('Approve');
        $I->see('The withdrawal has been successful');

        //Begin Reverse Withdraw//Page
        $I->amOnPage('/dashboard/portfolio/investments/details/'.$inv->id);
        $I->see('Investment Details');

        $I->sendAjaxPostRequest(route('portfolio_rollback_withdraw', [$inv->id]), ['reason'=>'I have extra money for saving']);
        $I->seeResponseCodeIs(200);
        $I->see('The investment withdrawal rollback has been saved for approval');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'institution_id'=>$inv->portfolio_investor_id]);

        //Approve
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('institution_id', $inv->portfolio_investor_id)->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->see(PortfolioInvestor::find($inv->portfolio_investor_id)->name);
        $I->click('Approve');
        $I->dontSeeInCurrentUrl('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('The investment withdrawal rollback has been done');
    }


    /**
     *Test rollback portfolio investments
     */
    public function testRollbackPortfolioInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can redeem Portfolio Investments');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments/details/'.$inv->id);
        $I->see('Investment Details');

        $I->sendAjaxPostRequest(route('portfolio_reverse', [$inv->id]), ['reason'=>'I need my money back']);
        $I->seeResponseCodeIs(200);

        //Approval Requested
        $I->see('The investment reversal has been saved for approval');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'institution_id'=>$inv->portfolio_investor_id]);

        //Approve
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('institution_id', $inv->portfolio_investor_id)->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->see(PortfolioInvestor::find($inv->portfolio_investor_id)->name);
        $I->click('Approve');
        $I->dontSeeInCurrentUrl('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('The investment reversal has been successful');
    }

    /**
     *Test rollover portfolio investments
     */
    public function testRolloverPortfolioInvestments()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can rollover Portfolio Investments');

        //Login and variables
        $I->havePortfolioInvestment();

        $formData = [
            'custodial_account_id'=>1,
            'portfolio_investor_id'=>1,
            'taxable'=>1,
            'fund_manager_id'=>1,
            'fund_type_id'=>1,
            'type_id'=>1,
            'invested_date'=>Carbon::today()->toDateString(),
            'maturity_date'=>Carbon::today()->toDateString(),
            'amount'=>45464749000,
            'withdrawn'=>0,
            'rolled'=>0,
            'withdrawal_date'=>null,
            'withdrawn_by'=>null,
            'on_call'=>false,
            'custodial_invest_transaction_id'=>null,
            'custodial_withdraw_transaction_id'=>null,
            'invest_transaction_approval_id'=>null,
            'withdraw_transaction_approval_id'=>null,
            'rollover_to'=>null,
            'rollover_from'=>null
        ];
        $inv_id = $I->haveRecord('portfolio_investments', $formData);
        $inv = PortfolioInvestment::find($inv_id);
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments/rollover/'.$inv->id);
        $I->seeResponseCodeIs(200);
        $I->see('Rollover Investment');

        $formData = [
            'investment'=>Crypt::encrypt($inv->id),
            'invested_date'=>Carbon::today()->addMonthsNoOverflow(5)->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(6)->toDateString(),
            'amount'=>2999999,
            'interest_rate'=>23,
            'taxable'=>true
        ];
        $I->sendAjaxPostRequest(route('portfolio_rollover'), $formData);
        $I->seeResponseCodeIs(200);

        //Approval Requested
        $I->see('The rollover has been saved for confirmation');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'institution_id'=>$inv->portfolio_investor_id]);

        //Approve
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('institution_id', $inv->portfolio_investor_id)->latest()->first();
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->see(PortfolioInvestor::find($inv->portfolio_investor_id)->name);
        $I->click('Approve');
        $I->dontSeeInCurrentUrl('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('The rollover has been approved');
    }

    /**
     *Test view maturity analysis for specified periods
     */
    public function testMaturityAnalysis()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can perform portfolio funds maturity analysis');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $curr = Currency::find($inv->custodialAccount->currency_id);
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments/maturity/analysis');
        $I->see('Funds Maturity Analysis');

        $formData = [
            'start'=>Carbon::today()->toDateString(),
            'end'=>Carbon::today()->addMonthsNoOverflow(6)->toDateString()
        ];
        $I->sendAjaxPostRequest('/api/portfolio/maturityprofiledata/weekly/'.$curr->id, $formData);
        $I->seeResponseCodeIs(200);

        //TODO cannot view graph: Use acceptance test
    }
}
