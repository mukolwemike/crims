<?php


class PortfolioApprovalsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test create, View, and Approve portfolio transactions
     */
    public function testPortfolioApprovals()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view pending portfolio approvals on the approvals grid');

        //Login and variables
        $inv = PortfolioInvestment::find($I->havePortfolioInvestment());
        $I->haveClient();
        $user = $I->login();

        //Page
        $I->amOnPage('/dashboard/portfolio/investments/details/'.$inv->id);
        $I->see('Investment Details');

        $I->sendAjaxPostRequest(route('portfolio_reverse', [$inv->id]), ['reason'=>'I need my money back']);
        $I->seeResponseCodeIs(200);

        //Approval Requested
        $I->see('The investment reversal has been saved for approval');
        $I->seeInCurrentUrl('/dashboard/portfolio');
        $I->seeRecord('portfolio_transaction_approval', ['sent_by'=>$user->id, 'institution_id'=>$inv->portfolio_investor_id]);

        //Approvals Grid
        $approval = PortfolioTransactionApproval::where('sent_by', $user->id)->where('institution_id', $inv->portfolio_investor_id)->latest()->first();
        $inst = PortfolioInvestor::find($approval->institution_id);
        $I->amOnPage('/dashboard/portfolio/approve');
        $I->see('Approve Portfolio Transactions');
        $I->sendAjaxGetRequest('/api/portfolio/approvals');
        $I->seeResponseCodeIs(200);
        $I->see($inst->name);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($approval->sent_by));

        //Approve
        $I->amOnPage('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('Approve Portfolio Transaction');
        $I->see(PortfolioInvestor::find($inv->portfolio_investor_id)->name);
        $I->click('Approve');
        $I->dontSeeInCurrentUrl('/dashboard/portfolio/approve/'.$approval->id);
        $I->see('The investment reversal has been successful');
    }
}
