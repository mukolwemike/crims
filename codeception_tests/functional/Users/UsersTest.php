<?php
namespace Users;

use Cytonn\Authorization\Permission;
use Cytonn\Authorization\Role;
use Cytonn\Presenters\UserPresenter;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Crypt;

class UsersTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    public function testAddViewNewUser()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can add new user');

        //Login and Variables
        $I->haveClientInvestment();
        $I->haveUser();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users');
        $I->see('Users');
        $I->click('Add new');
        $I->seeInCurrentUrl('/dashboard/users/add');
        $I->see('Add a new user');

        //Test Add user
        $formData = [
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
            'username'=>$faker->name,
            'email'=>$faker->email
        ];
        $I->sendAjaxPostRequest('/dashboard/users/add', $formData);
        $I->seeResponseCodeIs(200);

        //See User added
        $I->see('User succesfully registered, an email has been sent to user to activate account');
        $I->seeRecord('users', ['email'=>$formData['email'], 'username'=>$formData['username']]);

        //See user on Users Grid
        $I->seeInCurrentUrl('/dashboard/users');
        $I->sendAjaxGetRequest('/api/users');
        $user = \User::where('email', $formData['email'])->where('username', $formData['username'])->first();
        $I->see($user->username);
        $I->see($user->contact->email);
    }

    /**
     * Test View Existing Users
     */
    public function testViewUserDetails()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can view details of an existing user');

        //Login and Variables
        $I->haveClientInvestment();
        $user = \User::find($I->haveUser());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/details/'.$user->id);
        $I->see('User Details');

        //User Details
        $I->see($user->contact->email);
        $I->see(UserPresenter::presentFullNamesNoTitle($user->id));
    }


    /**
     * Test edit Existing Users
     */
    public function testEditExistingUser()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can edit registration details of an existing user');

        //Login and Variables
        $I->haveClientInvestment();
        $user = \User::find($I->haveUser());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/add/'.$user->id);
        $I->see('Add a new user');

        //Test Edit user
        $formData = [
            'firstname'=>'EditFirst',
            'lastname'=>'EditLast',
            'username'=>'EditUser',
            'email'=>'edit@email.com'
        ];
        $I->sendAjaxPostRequest('/dashboard/users/add', $formData);
        $I->seeResponseCodeIs(200);

        //See User edited
        $I->see('User succesfully registered, an email has been sent to user to activate account');
        $I->seeRecord('users', ['email'=>$formData['email'], 'username'=>$formData['username']]);

        //See edited user on Users Grid
        $I->seeInCurrentUrl('/dashboard/users');
        $I->sendAjaxGetRequest('/api/users');
        $user = \User::where('email', $formData['email'])->where('username', $formData['username'])->first();
        $I->see($user->username);
        $I->see($user->contact->email);
    }


    /**
     * Test Deactivate Existing Users
     */
    public function testDeactivateReactivateUsers()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can deactivate the account an existing user');

        //Login and Variables
        $I->haveClientInvestment();
        $user = \User::find($I->haveUser());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/details/'.$user->id);
        $I->see('User Details');

        //User Details
        $I->see($user->contact->email);
        $I->see(UserPresenter::presentFullNamesNoTitle($user->id));

        //Deactivate Account
        $I->see('Actions');
        $I->click('Reactivate account');
        $I->seeRecord('users', ['active'=>1]);

        //Reactivate Account
        $I->click('Deactivate account');
        $I->seeRecord('users', ['active'=>0]);
    }


    /**
     * Test Assign Roles to Users
     */
    public function testAssignUserRoles()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can assign roles to an existing user');

        //Login and Variables
        $I->haveClientInvestment();
        $user = \User::find($I->haveUser());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/details/'.$user->id);
        $I->see('User Details');

        //User Details
        $I->see($user->contact->email);
        $I->see(UserPresenter::presentFullNamesNoTitle($user->id));

        //Assign Roles
        $I->see('Access Control');
        $role = Role::find($I->haveUserRoles());
        $formData = [
            'user_id'=>Crypt::encrypt($user->id),
            'role'=> $role->id
        ];
        $I->sendAjaxPostRequest(route('assign_user_role'), $formData);
        $I->seeResponseCodeIs(200);

        //See Role Added
        $I->see('The user now has the role: '.$role->description);
        $I->see('Assigned roles');
        $I->see($role->description);
    }


    /**
     * Test Assign Roles to Users
     */
    public function testAssignUserPermissions()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can permissions to an existing user');

        //Login and Variables
        $I->haveClientInvestment();
        $user = \User::find($I->haveUser());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/details/'.$user->id);
        $I->see('User Details');

        //User Details
        $I->see($user->contact->email);
        $I->see(UserPresenter::presentFullNamesNoTitle($user->id));

        //Assign Permissions
        $I->see('Select Role to Assign user');
        $permission = Permission::find($I->haveUserPermissions());
        $formData = [
            'user_id'=>Crypt::encrypt($user->id),
            'permission'=> $permission->id
        ];
        $I->sendAjaxPostRequest(route('assign_user_permission'), $formData);
        $I->seeResponseCodeIs(200);

        //See Role Added
        $I->see('The use has now been allowed the permission: '.$permission->description);
        $I->see('The user has been assigned the following permissions:');
        $I->see($permission->description);
    }
}
