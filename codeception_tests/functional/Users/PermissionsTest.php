<?php
namespace Users;

use Cytonn\Authorization\Permission;
use Cytonn\Authorization\UserPermission;

class PermissionsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
        $I = $this->tester;
        $I->haveClientInvestment();
        $I->login();
    }

    /**
     * Test view permissions on grid
     */
    public function testViewPermissionsGrid()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can view system permissions on grid');

        //Login and Variables
        $permission = Permission::find($I->haveUserPermissions());

        //Page
        $I->amOnPage('/dashboard/users/permissions');
        $I->see('System Permissions');

        //View Permissions on Grid
        $I->sendAjaxGetRequest('/api/permissions');
        $I->seeResponseCodeIs(200);
        $I->see($permission->id);
        $I->see($permission->name);
        $I->see($permission->description);
    }

    /**
     * Test view individual permissions details
     */
    public function testViewIndividualPermissionsDetails()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can view details for individual system permissions');

        //Login and Variables
        $user = \User::find($I->haveUser());
        $permission = Permission::find($I->haveUserPermissions());
        $I->haveAssignedPermissions(['permission_id'=>$permission->id, 'user_id'=>$user->id]);
        $assigned = UserPermission::where('permission_id', $permission->id)->where('user_id', $user->id)->first();

        //Page
        $I->amOnPage('/dashboard/users/permissions/details/'.$permission->id);
        $I->see('Permission Detail');

        //Permission Details
        $I->see($permission->id);
        $I->see($permission->name);
        $I->see($permission->description);

        //Permission Assignment
        $I->see($assigned->user_id);
        $I->see(\Cytonn\Presenters\UserPresenter::presentNames($assigned->user_id));
    }
}
