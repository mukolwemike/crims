<?php
namespace Users;

use Cytonn\Authorization\Role;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Crypt;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Authorization\Permission;

class RolesTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     * Test View Roles on grid
     */
    public function testViewRolesGrid()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can view the roles grid');

        //Login and Variables
        $I->haveClientInvestment();
        $role = Role::find($I->haveUserRoles());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/roles');
        $I->see('User Roles');

        //View Roles on Grid
        $I->sendAjaxGetRequest('/api/roles');
        $I->seeResponseCodeIs(200);
        $I->see($role->id);
        $I->see($role->description);
    }

    /**
     * Test Add User Roles
     */
    public function testAddEditUserRoles()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can new User Roles');

        //Login and Variables
        $faker = Faker::create();
        $I->haveClientInvestment();
        $role = Role::find($I->haveUserRoles());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/roles');
        $I->see('User Roles');
        $I->click('Add new');
        $I->seeInCurrentUrl('/dashboard/users/roles/add');
        $I->see('Add a new role');

        //Add Role
        $formData = [
            'name'=>$name=$faker->name,
            'description'=>$desc= $faker->paragraph
        ];
        $I->sendAjaxPostRequest('/dashboard/users/roles/add/', $formData);

        //See Record Added
        $I->seeRecord('authoritaire_roles', ['name'=>$name, 'description'=>$desc]);

        //Edit Role
        $I->amOnPage('/dashboard/users/roles/add/'.$role->id);
        $formData = [
            'name'=>$name=$faker->name,
            'description'=>$desc= $faker->paragraph
        ];
        $I->sendAjaxPostRequest('/dashboard/users/roles/add/'.$role->id, $formData);

        //See Record Added
        $I->seeRecord('authoritaire_roles', ['name'=>$name, 'description'=>$desc]);
    }


    /**
     * Test View details for individual roles
     */
    public function testViewRoleDetails()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can view details for individual roles');

        //Login and Variables
        $I->haveClientInvestment();
        $role = Role::find($I->haveUserRoles());
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/roles/details/'.$role->id);
        $I->see('Role Details');

        //Role details
        $I->see($role->id);
        $I->see($role->name);
        $I->see($role->description);
    }

    /**
     * Test Revoke user from role
     */
    public function testRevokeUserRoles()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can revoke roles assigned to users');


        //Login and Variables
        $role = Role::find($I->haveUserRoles());
        $I->haveClientInvestment();
        $user = \User::find($I->haveUser());
        $I->login();


        //Assign Role
        $I->amOnPage('/dashboard/users/details/'.$user->id);
        $I->sendAjaxPostRequest(route('assign_user_role'), [
            'user_id'=>Crypt::encrypt($user->id),
            'role'=> $role->id
        ]);

        //View Memberships
        $I->amOnPage('/dashboard/users/roles/details/'.$role->id);
        $I->see('The following are the members of this role');
        $I->see($user->username);

        //Remove User from Role
        $membership = $role->memberships()->where('authorizable_type', 'User')->first();
        $I->sendAjaxPostRequest('/dashboard/users/roles/removemember', ['member'=>$membership->id]);
        $I->seeResponseCodeIs(200);
        $I->dontSee($user->username);
        $I->dontSee(UserPresenter::presentFullNamesNoTitle($user->id));
    }


    /**
     * Test add permissions to roles
     */
    public function testAddPermissionsToRoles()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can permissions to roles');

        //Login and Variables
        $role = Role::find($I->haveUserRoles());
        $permission = Permission::find($I->haveUserPermissions());
        $I->haveClientInvestment();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/roles/details/'.$role->id);
        $I->see('This role has been assigned the following permissions');

        //Add Permissions to Role
        $formData = [
            'role'=>$role->id,
            $permission->name=> true
        ];
        $I->sendAjaxPostRequest('/dashboard/users/roles/grant', $formData);
        $I->seeResponseCodeIs(200);

        //See Role Added
        $I->see('Permission was successfully granted to role');
        $I->seeInCurrentUrl('/dashboard/users/roles/details/'.$role->id);
        $I->see($permission->id);
        $I->see($permission->name);
        $I->see($permission->description);
    }



    /**
     * Test add permissions to roles
     */
    public function testRevokePermissionsToRoles()
    {
        $I = $this->tester;
        $I->am('a crims admin with permissions');
        $I->wantTo('Test whether an admin can permissions to roles');

        //Login and Variables
        $role = Role::find($I->haveUserRoles());
        $I->haveRolePermissions(['role_id'=>$role->id]);
        $permission = $role->permissions()->first();
        $I->haveClientInvestment();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/roles/details/'.$role->id);
        $I->see('This role has been assigned the following permissions');

        //Revoke Permissions to Role
        $formData = [
            'role'=>$role->id,
            'permission'=> $permission->id,
        ];
        $I->sendAjaxPostRequest('/dashboard/users/roles/revoke', $formData);
        $I->seeResponseCodeIs(200);

        //See Role Added
        $I->see('Permission was revoked on the role');
        $I->seeInCurrentUrl('/dashboard/users/roles/details/'.$role->id);
        $I->dontSee($permission->name);
    }
}
