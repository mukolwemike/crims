<?php
namespace Users;

use Faker\Factory as Faker;
use ClientTransactionApproval;

class ClientsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     * Test add client user
     */
    public function testAddClientUser()
    {
        $I = $this->tester;
        $I->am('a admin with permissions');
        $I->wantTo('Test whether an admin with permissions can add a new client user');

        //Variables
        $faker = Faker::create();
        $I = $this->tester;
        $I->haveClientInvestment();
        $I->haveUser();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/client/add');
        $I->see('Create a new user for a client');

        //Add User
        $formData = [
            'username'=>$faker->userName,
            'email'=>$faker->email,
            'phone_country_code'=>+254,
            'phone'=>$faker->phoneNumber,
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
        ];

        $I->sendAjaxPostRequest('/dashboard/users/client/add', $formData);
        $I->seeResponseCodeIs(200);

        //See client user added
        $I->see('User created successfully. An email will be sent when you approve at least one account');
        $I->seeRecord('client_users', $formData);
    }

    /**
     * Test view client users grid
     */
    public function testViewClientUsersGrid()
    {
        $I = $this->tester;
        $I->am('a admin with permissions');
        $I->wantTo('Test whether an admin with permissions can view client users on grid');

        //Variables
        $client = \ClientUser::find($I->haveClientUser());
        $I->haveClientInvestment();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/clients');
        $I->see('Client\'s user accounts');

        //View CLient Users on Grid
//        $I->sendAjaxGetRequest('/api/users/clients');
//        $I->seeResponseCodeIs(200);
//        $I->see($client->firstname);
    }


    /**
     * Test edit existing client users
     */
    public function testEditExisting()
    {
        $I = $this->tester;
        $I->am('a admin with permissions');
        $I->wantTo('Test whether an admin with permissions can edit an existing client user');

        //Variables
        $faker = Faker::create();
        $client = \ClientUser::find($I->haveClientUser());
        $I->haveClientInvestment();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/client/add/'.$client->id);
        $I->see('Create a new user for a client');

        //Edit Client User
        $formData = [
            'username'=>$faker->userName,
            'email'=>$faker->email,
            'phone_country_code'=>+254,
            'phone'=>$faker->phoneNumber,
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
        ];

        $I->sendAjaxPostRequest('/dashboard/users/client/add/'.$client->id, $formData);
        $I->seeResponseCodeIs(200);

        //See client user added
        $I->see('User details have been saved succesfully');
        $I->seeRecord('client_users', $formData);
    }


    /**
     * Test view existing client users
     */
    public function testViewExistingClientUsers()
    {
        $I = $this->tester;
        $I->am('a admin with permissions');
        $I->wantTo('Test whether an admin with permissions can view an existing client user\'s details');

        //Variables
        $client = \ClientUser::find($I->haveClientUser());
        $I->haveClientInvestment();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/users/client/details/'.$client->id);
        $I->see('User Details');

        //Client User Details
        $I->see($client->username);
        $I->see($client->firstname.' '.$client->lastname);
    }


    /**
     * Test assign user access to clients data
     */
    public function testAssignAccessToUserData()
    {
        $I = $this->tester;
        $I->am('a admin with permissions');
        $I->wantTo('Test whether an admin with permissions can assign users permission to access clients data');

        //Variables
        $user = \ClientUser::find($I->haveClientUser());
        $inv = \ClientInvestment::find($I->haveClientInvestment());
        $signed_user = $I->login();

        //Page
        $I->amOnPage('/dashboard/users/client/details/'.$user->id);
        $I->see('Assign access to a client\'s information');

        //Assign permissions
        $I->sendAjaxPostRequest(route('client_user_assign_path', [$user->id]), ['client_id'=>$inv->client_id]);
        $I->seeResponseCodeIs(200);

        //See permissions Requested
        $I->see('User account has been added for approval');
        $I->seeRecord('client_transaction_approval', ['transaction_type'=>'assign_user_account', 'client_id'=>$inv->client_id]);

        //Approve
        $approve_id = ClientTransactionApproval::where('client_id', $inv->client_id)->where('sent_by', $signed_user->id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);

        //See client permissions granted
        $I->see('Client account successfully given access');
        $I->see($inv->client->client_code);
    }



    /**
     * Test assign user access to financial manager
     */
    public function testAssignUserAccessToFinancialManager()
    {
        $I = $this->tester;
        $I->am('a admin with permissions');
        $I->wantTo('Test whether an admin with permissions can assign users permission to financial managers');

        //Variables
        $user = \ClientUser::find($I->haveClientUser());
        $inv = \ClientInvestment::find($I->haveClientInvestment());
        $recipient = \CommissionRecepient::find($I->haveCommissionRecepient());
        $I->haveFaUsers(['fa_id'=>$recipient->id, 'user_id'=>$user->id]);
        $signed_user = $I->login();

        //Page
        $I->amOnPage('/dashboard/users/client/details/'.$user->id);
        $I->see('Assign access to a Financial Advisor');

        //Assign permissions
        $I->sendAjaxPostRequest(route('fa_user_assign_path', [$user->id]), ['fa_id'=>$recipient->id]);
        $I->seeResponseCodeIs(200);

        //See permissions granted
        $I->see('Access has been granted to '.$recipient->name);
    }
}
