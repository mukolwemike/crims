<?php
use Cytonn\Core\DataStructures\Carbon;

class ActivityLogTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test view activity log for newly recorded CMS/Coop activities
     */
    public function testActivityLog()
    {

        $I = $this->tester;
        $I->wantTo('Test whether a user can view the activity log of a newly recorded activity');

        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $recipient = $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/rollover/".$investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Rollover Investment');


        //Rollover
        $formData = [
            'investment_id'=>Crypt::encrypt($investment->id),
            'reinvest'=>'reinvest',
            'amount'=>2400,
            'interest_rate'=>5,
            'product_id'=>1,
            'invested_date'=>Carbon::today()->addMonthsNoOverflow(11)->toDateString(),
            'maturity_date'=>Carbon::today()->addMonthsNoOverflow(13)->toDateString(),
            'on_call'=>true,
            'commission_recepient'=>$recipient,
            'commission_rate'=>4,
            'commissionRecipient'=>true,
            'interest_payment_interval'=>1,
            'interest_payment_date'=>31
        ];
        $I->sendAjaxPostRequest(route('rollover_schedule_path'), $formData);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('The investment rollover has been scheduled');
        $I->seeRecord('client_transaction_approval', ['client_id'=>$investment->client_id, 'sent_by'=>$user_id]);
        $approval = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first();

        //View Log Grid
        $I->amOnPage('/dashboard/investments/activity');
        $I->see('Activity Log');
        $I->sendAjaxGetRequest('/api/activity_log/client_transactions');
        $I->seeResponseCodeIs(200);


        //View Individual Log
        $I->amOnPage('/dashboard/investments/approve/'.$approval->id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id'=>$investment_id]);
        $I->see('The rollover has been scheduled');
    }
}
