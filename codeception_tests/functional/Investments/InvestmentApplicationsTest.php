<?php

use Faker\Factory as Faker;
use Cytonn\Core\DataStructures\Carbon;

class InvestmentsApplicationsTest extends \Codeception\TestCase\Test
{

    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test make investment application an individual investor
     */
    public function testIndividualInvestmentApplication()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I-> wantTo('Test whether a user can make investments applications for individual client');

        //Login
        $product_id  = $I->haveProduct();
        $client_id = $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->seeAuthentication();

        //Page
        $I->amOnPage('/dashboard/investments/apply/individual');
        $I->see('Individual Investment Application');
        $formData = [
            'new_client'=>1,
            'amount' =>123005000,
            'agreed_rate'=>14,
            'tenor'=>43,
            'title'=> 1,
            'lastname'=>$faker->lastName,
            'middlename'=>$faker->name,
            'firstname'=>$faker->firstName,
            'gender_id'=>1,
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>121312,
            'taxable'=>1,
            'id_or_passport'=>4242434224,
            'email'=>'myemail@example.com',
            'postal_code'=>3453234,
            'postal_address'=>222322,
            'country_id'=>114,
            'telephone_office'=>07052626262,
            'phone'=>07052626262,
            'residential_address'=>'Nairobi West',
            'town'=>'Nairobi',
            'method_of_contact_id'=>1,
            'employment_id'=>1,
            'present_occupation'=>'Investor',
            'employer_name'=>'Cytonn Management',
            'funds_source_id'=>1,
            'funds_source_other'=>'Gampler',
            'investor_account_name'=>'MyOtherInvestment',
            'investor_account_number'=>23232223,
            'investor_bank'=>'Bank2',
            'investor_bank_branch'=>'Branch2',
            'investor_clearing_code'=>2332,
            'investor_swift_code'=>2334,
            'terms_accepted'=>1,
            'product_id'=>$product_id,
            'complete'=>true
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/apply/individual/'.$client_id, $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Investment saved successfully');
        $I->seeRecord('client_investment_application', [
            'amount' =>123005000,
            'agreed_rate'=>14,
            'tenor'=>43,
            'title'=> 1,
            'gender_id'=>1,
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>121312,
            'taxable'=>1,
            'id_or_passport'=>4242434224
        ]);
    }

    /**
     *Test make investment application a corporate investor
     */
    public function testCorporateInvestmentApplication()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can make investments applications for a corporate client');

        //Login
        $product_id  = $I->haveProduct();
        $client_id = $I->haveClient();
        $I->login();
        $I->seeAuthentication();

        //Page
        $I->amOnPage('/dashboard/investments/apply/corporate');
        $I->see('Corporate Investment Application');
        $formData = [
            'new_client'=>1,
            'amount' =>9833030000,
            'partnership_account_number'=> 33120,
            'agreed_rate'=>5,
            'tenor'=>12,
            'email'=>'corporate@gmail.com',
            'postal_code'=>3452234,
            'postal_address'=>222212,
            'country_id'=>114,
            'telephone_office'=>07252626262,
            'phone'=>07672626262,
            'town'=>'Nairobi',
            'method_of_contact_id'=>1,
            'corporate_investor_type'=>1,
            'registered_name'=>'The Corporation',
            'registered_address'=>'67 Thigiri',
            'registration_number'=>'LPP/2015/2342',
            'funds_source_id'=>1,
            'funds_source_other'=>'Money Gampler',
            'investor_account_name'=>'The Corporation',
            'investor_account_number'=>43232323,
            'investor_bank'=>'CorporateBank',
            'investor_bank_branch'=>'CorporateBranch',
            'investor_clearing_code'=>4232,
            'investor_swift_code'=>4234,
            'terms_accepted'=>1,
            'product_id'=>$product_id,
            'contact_person_title'=>1,
            'contact_person_fname'=>'Person',
            'contact_person_lname'=>'Higgins',
            'taxable'=>true,
            'complete'=>true
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/apply/corporate/'.$client_id, $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Investment saved successfully');
        $I->seeRecord('client_investment_application', [
            'partnership_account_number'=> 33120,
            'agreed_rate'=>5,
            'tenor'=>12,
            'email'=>'corporate@gmail.com',
            'postal_code'=>3452234
        ]);
    }

    /**
     *Test make investment application an Coop clients
     */
    public function testCoopClientApplication()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I->wantTo('Test whether a user can make investments applications for a Coop Clients');

        //Login
        $entity_id = $I->haveShareEntities();
        $category_id = $I->haveShareCategries();
        $product_id  = $I->haveProduct();
        $client_id = $I->haveClient();
        $I->login();
        $I->seeAuthentication();

        //Page
        $I->amOnPage('/dashboard/coop/clients/apply');
        $I->see('Cytonn Cooperative Application');
        $formData = [
            'id'=>1,
            'title_id'=>1,
            'lastname'=>$faker->lastName,
            'middlename'=>$faker->name,
            'firstname'=>$faker->firstName,
            'title_id'=>1,
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>4121212,
            'id_or_passport'=>424244224,
            'email'=>'email@example.com',
            'postal_code'=>345234,
            'postal_address'=>22222,
            'country_id'=>114,
            'phone'=>07052626262,
            'town'=>'Nairobi',
            'employment_id'=>1,
            'referee_id'=>$client_id,
            'membership_fee'=>45667,
            'share_purchase' =>9833030000,
            'entity_id'=> $entity_id,
            'category_id'=>$category_id,
            'mode_of_payment'=>'Direct Bank Deposit',
            'payment_options'=>1,
            'amount'=>3452234,
            'date'=>Carbon::today()->toDateString(),
            'narrative'=>'I need to investment',
            'shares'=>4567,
            'product_id'=>$product_id,
            'duration'=>3,
            'amount_paid_for_product'=>1657890,
            'payment_date'=>Carbon::today()->toDateString(),
            'client_contact_person_name'=>[$faker->name],
            'id_number'=>[4567889],
            'relationship_id'=>[1],
            'percentage_share'=>[54]
        ];
        $I->sendAjaxPostRequest(route('store_coop_client'), $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Coop client details have been saved for approval');
        $I->seeRecord('coop_membership_forms', [
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>4121212,
            'id_or_passport'=>424244224,
            'email'=>'email@example.com',
            'postal_code'=>345234
        ]);
        $I->seeInCurrentUrl('/dashboard/clients');
    }


    /**
     *Test view client investment applications on Applications Grid
     */
    public function testViewApplicationsGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view Client Applications');

        //Login
        $client_id = $I->haveClient();
        $application_id = $I->haveApplication();
        $I->login();
        $I->seeAuthentication();

        //page
        $I->amOnPage('/dashboard/investments/applications');
        $I->see('Investment Applications');
        $I->seeResponseCodeIs(200);

        //grid
        $application = ClientInvestmentApplication::where('id', $application_id)->first();
        $I->sendAjaxGetRequest('/api/applications');
        $I->seeResponseCodeIs(200);
        $I->see($application->client->client_code);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($application->client->id));
        $I->see($application->amount);
        $I->see($application->id);
    }


    /**
     *Test view investment application details for individual investors
     */
    public function testViewIndividualApplication()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view Individual Client Applications');

        //Login and variables
        $I->haveClient();
        $application_id = $I->haveApplication();
        $application = ClientInvestmentApplication::where('id', $application_id)->first();
        $I->login();
        $I->seeAuthentication();


        //page
        $I->amOnPage('/dashboard/investments/applications/'.$application->id);
        $I->see('Application Details');
        $I->seeResponseCodeIs(200);

        //Details
        $I->see($application->investor_bank);
        $I->see($application->tenor);
        $I->see($application->investor_account_name);
    }

    /**
     *Test submit investment applications for approval
     */
    public function testSubmitForApproval()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can submit Applications for approval');

        //Login and variables
        $I->haveClient();
        $application = ClientInvestmentApplication::find($I->haveApplication());
        $I->login();
        $I->seeAuthentication();

        //page
        $I->amOnPage('/dashboard/investments/applications/' . $application->id);
        $I->see('Application Details');
        $I->seeResponseCodeIs(200);
        $I->see('Contact Information');

        //Submit for Approval
        $I->see('Send Approval Request');
        $I->click('Send Approval Request');

        //See submitted
        $I->seeRecord('client_transaction_approval', ['client_id' => $application->client_id, 'transaction_type' => 'application']);
        $I->seeInCurrentUrl('/dashboard/investments/applications/' . $application->id);
    }

    /**
     *Test edit and existing investment application
     */
    public function testEditApplicationDetails()
    {
        $I =$this->tester;
        $faker = Faker::create();
        $I->wantTo('Test whether a user can edit application details before application is approved');

        //Login and test variables
        $product_id  = $I->haveProduct();
        $application_id = $I->haveApplication();
        $I->login();
        $I->seeAuthentication();
        $application = ClientInvestmentApplication::where('id', $application_id)->first();
        $application_id = $application->id;

        //page
        $I->amOnPage('/dashboard/investments/applications/edit/'.$application_id);
        $I->see('Edit Application Details');
        $I->seeResponseCodeIs(200);


        //Edit the Investment
        $editData = [
            'amount' =>356487005000,
            'agreed_rate'=>14,
            'tenor'=>43,
            'title'=> 1,
            'lastname'=>$faker->lastName,
            'middlename'=>$faker->name,
            'firstname'=>$faker->firstName,
            'gender_id'=>1,
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>121312,
            'taxable'=>1,
            'id_or_passport'=>4242434224,
            'email'=>'editemail@example.com',
            'postal_code'=>3453234,
            'postal_address'=>222322,
            'country_id'=>114,
            'telephone_office'=>07052626262,
            'phone'=>07052626262,
            'residential_address'=>'Nairobi West',
            'town'=>'Nairobi',
            'method_of_contact_id'=>1,
            'employment_id'=>1,
            'present_occupation'=>'Investor',
            'employer_name'=>'Cytonn Management',
            'funds_source_id'=>1,
            'funds_source_other'=>'Gampler',
            'investor_account_name'=>'MyOtherInvestment',
            'investor_account_number'=>23232223,
            'investor_bank'=>'Bank2',
            'investor_bank_branch'=>'Branch2',
            'investor_clearing_code'=>2332,
            'investor_swift_code'=>2334,
            'terms_accepted'=>1,
            'product_id'=>$product_id,
            'complete'=>true
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/applications/edit/'.$application_id, $editData);
        $I->seeResponseCodeIs(200);
        $I->see('The changes have been saved');
        $I->seeRecord('client_investment_application', [
            'id_or_passport'=>4242434224,
            'email'=>'editemail@example.com',
            'postal_code'=>3453234,
            'postal_address'=>222322,
            'country_id'=>114,
            'telephone_office'=>07052626262,
            'phone'=>07052626262,
            'residential_address'=>'Nairobi West',
        ]);
    }


    /**
     *Test a joint holder to an existing client investment application
     */
    public function testAddJointHolder()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I->wantTo('Test whether a user can add a joint holder client');

        //Login and test variables
        $application_id = $I->haveApplication();
        $I->login();
        $I->seeAuthentication();
        $application = ClientInvestmentApplication::where('id', $application_id)->first();
        $application_id = $application->id;


        //page
        $I->amOnPage('/dashboard/investments/applications/'.$application_id.'/joint');
        $I->see('Joint Holders');
        $I->seeResponseCodeIs(200);

        //Add Joint Holder
        $formData = [
            'client_id'=>$application->client->id,
            'title_id'=> 1,
            'lastname'=>$faker->lastName,
            'middlename'=>$faker->name,
            'firstname'=>$faker->firstName,
            'gender_id'=>1,
            'dob'=>Carbon::today()->toDateString(),
            'pin_no'=>1721312,
            'id_or_passport'=>42427434224,
            'email'=>$faker->email,
            'telephone_home'=>07052626262,
            'postal_code'=>3453234,
            'postal_address'=>222322,
            'country_id'=>114,
            'telephone_office'=>07032626262,
            'telephone_cell'=>07052626262,
            'residential_address'=>'Nairobi West',
            'town'=>'Nairobi',
            'method_of_contact_id'=>1,
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/applications/'.$application_id.'/joint', $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Joint holder successfully added');
        $I->seeRecord('client_joint_details', $formData);

        // View Added Joint Holder On Application Details
        $joint_id = ClientJointDetail::where('client_id', $application->client->id)->first();
        $I->amOnPage('/dashboard/investments/applications/'.$application_id);
        $I->see('Joint Holders');
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($joint_id));
    }

    /**
     *Test add email to an existing client investment application
     */
    public function testAddEmail()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I->wantTo('Test whether a user can add a additional client email');

        //Login and test variables
        $application_id = $I->haveApplication();
        $I->login();
        $I->seeAuthentication();
        $application = ClientInvestmentApplication::where('id', $application_id)->first();
        $application_id = $application->id;

        //page
        $I->amOnPage('/dashboard/investments/applications/'.$application_id);
        $I->see('Application Details');
        $I->seeResponseCodeIs(200);

        //Add email
        $formData = [
            'emails'=>$faker->email
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/applications/'.$application_id.'/email', $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Emails saved successfully');
    }


    /**
     *Test add contact person to an existing client investment application
     */
    public function testAddContactPerson()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I->wantTo('Test whether a user can add a additional contact person');

        //Login and test variables
        $application_id = $I->haveApplication();
        $I->login();
        $I->seeAuthentication();
        $application = ClientInvestmentApplication::where('id', $application_id)->first();
        $application_id = $application->id;

        //page
        $I->amOnPage('/dashboard/investments/applications/'.$application_id);
        $I->see('Application Details');
        $I->seeResponseCodeIs(200);

        //Add email
        $formData = [
            'name'=>$faker->name,
            'email'=>$faker->email,
            'address'=>$faker->address,
            'phone'=>$faker->phoneNumber
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/applications/'.$application_id.'/contact', $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Contact person added');
        $I->seeRecord('client_contact_persons', $formData);

        //View added contact Person On Application Details Page
        $joint_id = ClientContactPerson::where('client_id', $application->client->id)->first()->id;
        $I->amOnPage('/dashboard/investments/applications/'.$application_id);
        $I->see('Contact Persons');
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($joint_id));
        $I->see($formData['address']);


        //Test Edit Contact Person
        $editData = [
            'id'=>$joint_id,
            'name'=>$faker->firstName,
            'email'=>$faker->email,
            'address'=>4567,
            'phone'=>$faker->phoneNumber
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/applications/'.$application_id.'/contact', $editData);
        $I->seeRecord('client_contact_persons', $editData);
        $I->seeResponseCodeIs(200);

        //View Edited Contact Person on Application Details Page
        $I->amOnPage('/dashboard/investments/applications/'.$application_id);
        $I->see('Contact Persons');
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($joint_id));
        $I->dontSee($formData['address']);
        $I->see($editData['address']);
    }
}
