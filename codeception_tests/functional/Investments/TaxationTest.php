<?php

use Cytonn\Core\DataStructures\Carbon;

class TaxationTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test view taxation details for all clients on grid
     */
    public function testTaxationsGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can be able to view taxation grid');

        //Login and variables
        $I->haveProduct();
        $I->haveClient();
        $investment = ClientInvestment::find($I->haveClientInvestment());

        //Page
        $I->login();
        $I->amOnPage('/dashboard/investments/taxation');
        $I->see('Client Taxation');

        //Grid;
        $client = Client::first();
        $I->sendAjaxGetRequest('/api/investments/taxation/'.$investment->product->id);
        $I->seeResponseCodeIs(200);
        $I->see($investment->client->client_code);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client->id));
    }

    /**
     *Test view tax due details for all clients on grid
     */
    public function testIndividualClientTaxation()
    {
        $I = $this->tester;
        $I->wantTo('Test whether user can access tax due details for individual clients');

        //Login and variables
        $I->haveProduct();
        $I->haveClient();
        $investment = ClientInvestment::find($I->haveClientInvestment());
        $due_id = $I->haveRecord('client_tax', [
            'investment_id'=>$investment->id,
            'client_id'=>$investment->client->id,
            'date'=>Carbon::today()->toDateString(),
            'amount'=>4567889
        ]);
        $client_tax = ClientTax::find($due_id);

        //Page
        $I->login();
        $I->amOnPage('/dashboard/investments/taxation');
        $I->see('Client Taxation');
        $I->click('Tax Due');

        //Tax Due Page
        $I->seeInCurrentUrl('/dashboard/investments/taxation/records');
        $I->see('Clients Tax Due');

        //Grid
//        $I->sendAjaxGetRequest('/api/investments/taxation/due/'.$client_tax->investment->product_id);
//        $I->seeResponseCodeIs(200);
//        $I->see($client_tax->client->id);
//        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($client_tax->investment->id));
//        $I->see($client_tax->amount);
    }
}
