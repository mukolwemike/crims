<?php
use Illuminate\Support\Facades\Hash;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/26/16
 * Time: 12:00 PM
 */
class BusinessConfirmationsTest extends \Codeception\TestCase\Test
{

    /**
     * @var \FunctionalTester
     */
    protected $tester;


    /**
     *Test view investment business confirmations on Confirmations Grid
     */
    public function testBusinessConfirmationsGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view the business confirmations grid');

        //Login and variables
        $I->haveClient();
        $I->login();
        $I->seeAuthentication();

        //Page
        $I->amOnPage('/dashboard/investments/confirmation');
        $I->see('Business Confirmations');

        //View Grid
        $inv_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($inv_id);
        $I->sendAjaxGetRequest('/api/investments/businessconfirmations');
        $I->seeResponseCodeIs(200);
        $I->see($investment->id);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id));
    }


    /**
     *Test view details for individual business confirmations
     */
    public function testBusinessConfirmationDetails()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view the business confirmations details');

        //Login and variables
        $I->haveClient();
        $inv_id = $I->haveClientInvestment();
        $I->login();


        //Page
        $I->amOnPage('/dashboard/investments/confirmation/'.$inv_id);
        $I->see('Business Confirmation');
    }
}
