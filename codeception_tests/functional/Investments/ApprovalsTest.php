<?php
use Cytonn\Core\DataStructures\Carbon;

class ApprovalsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test view pending CMS transaction approvals on Grid
     */
    public function testTransactionApprovals()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can access and view CMS transaction approvals');

        //Login and variables
        $I->haveApplication();
        $I->login();


        //Approvals Page
        $I->amOnPage('/dashboard/investments/approve');
        $I->seeResponseCodeIs(200);
        $I->see('Approve Client Transactions');

        $approval = ClientTransactionApproval::find($I->haveTransactionApproval());
        $I->sendAjaxGetRequest('/api/investments/approvals');
        $I->seeResponseCodeIs(200);
        $I->see($approval->transaction_type);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($approval->sent_by));
        $I->see($approval->created_at);
    }
}
