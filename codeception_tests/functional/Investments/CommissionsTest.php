<?php
use Illuminate\Support\Facades\Hash;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/26/16
 * Time: 12:00 PM
 */
class CommissionsTest extends \Codeception\TestCase\Test
{

    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }
    /**
     *Test view investments commissions on  investments grid
     */
    public function testCommissionsGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test if a user can view the Investments Commissions Grid');

        //Login and variables
        $I->haveClient();
        $I->haveClientInvestment();
        $recipient_id = $I->haveCommissionRecepient();
        $I->login();

        //Page
        $I->amOnPage('/dashboard/investments/commission');
        $I->see('Commission');

        //Grid
        $commission_recipient = CommissionRecepient::find($recipient_id);
        $I->sendAjaxGetRequest('/api/investments/commission/1');
        $I->seeResponseCodeIs(200);
        $I->see($commission_recipient->name);
        $I->see($commission_recipient->email);
    }

    /**
     *Test Add FA users
     */
    public function testAddFA()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can add an FA user');

        //Login and variables
        $I->haveClient();
        $I->login();
        $inv_id = $I->haveClientInvestment();
        $recipient_id = $I->haveCommissionRecepient();
        $recipient = CommissionRecepient::find($recipient_id);


        //Page
        $I->amOnPage('/dashboard/investments/commission/add');
        $I->see('Add/Edit FA details');

        //Add/Edit FA
        $formData = [
            'recipient_type_id'=>$recipient->recipient_type_id,
            'name'=> 'New FA',
            'email'=>'emaril@example.com',
            'phone_country_code'=>254,
            'phone'=>657678954,
            'create_account'=>true
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/commission/add/'.$recipient_id, $formData);

        //See Record
        $I->see('The FA records have been saved successfully');
        $I->seeRecord('commission_recepients', [
            'recipient_type_id'=>$recipient->recipient_type_id,
            'name'=> 'New FA',
            'email'=>'emaril@example.com',
            'phone_country_code'=>254,
            'phone'=>657678954
        ]);

        //See Details
        $comm = CommissionRecepient::where('name', $formData['name'])->where('email', $formData['email'])->first();
        $investment = ClientInvestment::where('id', $inv_id)->first();
        $I->amOnPage('/dashboard/investments/commission/owner/'.$comm->id);
        $I->see(CommissionRecepient::where('id', $comm->id)->first()->name);

        //See Grid Values
        $I->see(\Cytonn\Presenters\InvestmentPresenter::tenorInMonths($investment->id));
    }

    /**
     *Test make investment commission payments to respective commission recipients
     */
    public function testCommissionPayment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can make commission payments');

        //Login and variables
        $I->haveClient();
        $I->login();
        $I->seeAuthentication();
        $recipient_id = $I->haveCommissionRecepient();


        //Page
        $I->amOnPage('/dashboard/investments/commission/payment/'.$recipient_id);
        $I->see('Commission Payments');
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames(CommissionRecepient::where('id', $recipient_id)->first()->id));


        //Commission Payment
        $formData = [
            'date'=>Carbon::today()->addMonthsNoOverflow(2)->toDateString(),
            'currency_id'=>1,
            'narrative'=>'Pay them all'
        ];
        $I->sendAjaxPostRequest(route('commission_payment', [$recipient_id]), $formData);
        $I->see('Commission payment saved for approval');
        $I->seeInCurrentUrl('Dashboard/investments/commission');
    }
}
