<?php
use Illuminate\Support\Facades\Hash;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/26/16
 * Time: 12:00 PM
 */
class ComplianceTest extends \Codeception\TestCase\Test
{

    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test clients' compliance details on Investments Compliance Grid
     */
    public function testComplianceGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test if a user can view the Client ComplianceGrid');

        //Login and variables
        $I->haveClient();
        $I->login();
        $I->seeAuthentication();


        //Page
        $I->amOnPage('/dashboard/investments/compliance');
        $I->see('Client Compliance');

        //Grid
        $client = Client::first();
        $I->sendAjaxGetRequest('/api/clients/all');
        $I->seeResponseCodeIs(200);
        $I->see($client->client_code);
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id));

        //View Individual Details
        $I->amOnPage('/dashboard/investments/compliance/'.$client->id);
        $I->see('Client Compliance');

        //Check for compliance
        $I->see('Compliance Checklist');
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id));

        $formData = [
            'client_id'=>Crypt::encrypt($client->id),
            'compliant_date'=>Carbon::today()->toDateString()
        ];
        $I->sendAjaxPostRequest(route('compliance_route'), $formData);
        $I->seeResponseCodeIs(200);
        $I->see('Compliance check saved.');
    }
}
