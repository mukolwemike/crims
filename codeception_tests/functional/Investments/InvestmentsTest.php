<?php

use Faker\Factory as Faker;
use Cytonn\Core\DataStructures\Carbon;

class InvestmentsTest extends \Codeception\TestCase\Test
{

    /**
         * @var \FunctionalTester
         */
    protected $tester;

    protected function _before()
    {
    }

    /**
         *Test view all existing client investments on grid
         */
    public function testInvestmentGrid()
    {
        $I = $this->tester;
        $I->wantTo('Test whether user can view the Investment Grid');

        //Login and test variables
        $I->haveClient();
        $I->login();
        $I->seeAuthentication();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::where('id', $investment_id)->first();

        //page
        $I->amOnPage('/dashboard/investments/clientinvestments');
        $I->see('Client Investments');
        $I->seeResponseCodeIs(200);


        //grid
        $I->sendAjaxGetRequest('/api/investments');
        $I->seeResponseCodeIs(200);
        $I->see($investment->invested_date->toDateString());
        $I->see($investment->maturity_date->toDateString());
        $I->see($investment->amount);
        $I->see($investment->interest_rate);
    }

    /**
         *Test view details for individual investments
         */
    public function testInvestmentDetails()
    {
        $I = $this->tester;

        $I->haveClient();
        $I->login();
        $I->seeAuthentication();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::where('id', $investment_id)->first();

        //Page
        $I->amOnPage("/dashboard/investments/clientinvestments/" . $investment_id);
        $I->seeResponseCodeIs(200);

        //View individual Investment
        $I->see('Client Investment');
        $I->see($investment->id);
        $I->see(\Cytonn\Presenters\AmountPresenter::currency(ClientInvestment::find($investment_id)->amount));
        $I->see($investment->interest_rate);
    }

    /**
         *Test topup and investment
         */
    public function testTopUpInvestment()
    {
        $I = $this->tester;

        $client_id = $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->haveCommission();
        $recipient = $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $I->amOnPage("/dashboard/investments/topup/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Topup Investment');


        //Topup
        $formData = [
            'investment' => Crypt::encrypt($investment_id),
            'product_id' => $I->haveProduct(),
            'invested_date' => Carbon::today()->addMonthsNoOverflow(7)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(9)->toDateString(),
            'on_call' => true,
            'amount' => 2003400,
            'interest_rate' => 5,
            'commission_recepient' => $recipient,
            'commission_rate' => 1,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31,
        ];
        $I->sendAjaxPostRequest(route('topup_path'), $formData);
        $I->seeResponseCodeIs(200);

        //Approval Sent
        $I->see('The topup has been saved for approval');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['maturity_date' => $formData['maturity_date']]);
        $I->seeCurrentUrlEquals('/dashboard/investments/approvalaccept/' . $approve_id);
        $I->see('Investment Topup Successful');
    }


    /**
         *Test edit and existing investment
         */
    public function testEditInvestment()
    {
        $I = $this->tester;

        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $user = $I->login();
        $user_id = $user->id;
        $I->amOnPage("/dashboard/investments/edit/" . $investment_id);
        $I->seeResponseCodeIs(200);

        //Edit Principle
        $editPrinciple = [
            'investment' => Crypt::encrypt($investment_id),
            'invested_date' => Carbon::today()->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(5)->toDateString(),
            'interest_rate' => 2,
            'amount' => 565656

        ];

        $I->sendAjaxPostRequest(route('edit_investment_path', [$investment_id]), $editPrinciple);
        $I->seeResponseCodeIs(200);

        //Request Approval
        $I->see('Edited investment has been saved for approval');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //Approve
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', [
            'invested_date' => Carbon::today()->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(5)->toDateString(),
            'interest_rate' => 2,
            'amount' => 565656

        ]);
        $I->see('The Investment has been changed');
    }

    /**
         *Test make direct investment rollover
         */
    public function testDirectRolloverInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a client can make direct rollover');

        $client_id = $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $recipient = $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/rollover/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Rollover Investment');


        //Rollover
        $formData = [
            'investment_id' => Crypt::encrypt($investment->id),
            'reinvest' => 'reinvest',
            'amount' => 2400,
            'interest_rate' => 5,
            'product_id' => 1,
            'invested_date' => Carbon::today()->addMonthsNoOverflow(11)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(13)->toDateString(),
            'on_call' => true,
            'commission_recepient' => $recipient,
            'commission_rate' => 4,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31
        ];
        $I->sendAjaxPostRequest(route('rollover_schedule_path'), $formData);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('The investment rollover has been scheduled');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record and Approve
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('The rollover has been scheduled');
    }


    /**
         *Test make direct investment withdraw
         */
    public function testDirectWithdraw()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a client can make direct withdraw');

        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $I->amOnPage("/dashboard/investments/withdraw/" . $investment->id);
        $I->seeResponseCodeIs(200);
        $I->see('Withdraw Investment');


        //Withdraw
        $formData = [
            'investment_id' => Crypt::encrypt($investment->id),
            'callback' => true,
            'premature' => true,
            'end_date' => Carbon::today()->addMonthsNoOverflow(1)->toDateString(),
            'partial_withdraw' => false,
            'amount' => 0,
            'new_maturity_date' => false,
        ];
        $I->sendAjaxPostRequest(route('withdraw_path'), $formData);
        $I->seeResponseCodeIs(200);
    }


    /**
         *Test make scheduled investment rollover
         */
    public function testScheduledRolloverInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a client can make scheduled rollover');

        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $recipient = $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $I->amOnPage("/dashboard/investments/transactions/schedule/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Rollover Investment');


        //Rollover
        $formData = [
            'investment_id' => Crypt::encrypt($investment_id),
            'reinvest' => 'reinvest',
            'amount' => 2400,
            'interest_rate' => 5,
            'product_id' => 1,
            'invested_date' => Carbon::today()->addMonthsNoOverflow(11)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(13)->toDateString(),
            'on_call' => true,
            'commission_recepient' => $recipient,
            'commission_rate' => 4,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31
        ];
        $I->sendAjaxPostRequest(route('rollover_schedule_path'), $formData);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('The investment rollover has been scheduled');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('The rollover has been scheduled');
    }

    /**
         *Test make scheduled investment rollover with withdraw
         */
    public function testScheduledRolloverandWithdraw()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can make scheduled withdraw');

        //Login and variables
        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $recipient = $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/transactions/schedule/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Schedule Investment Transaction');


        //Rollover
        $formData = [
            'investment_id' => Crypt::encrypt($investment_id),
            'reinvest' => 'withdraw',
            'amount' => 2400,
            'interest_rate' => 5,
            'product_id' => 1,
            'invested_date' => Carbon::today()->addMonthsNoOverflow(11)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(13)->toDateString(),
            'on_call' => true,
            'commission_recepient' => $recipient,
            'commission_rate' => 4,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31
        ];
        $I->sendAjaxPostRequest(route('rollover_schedule_path'), $formData);

        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record and Approve
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('The rollover has been scheduled');
    }

    /**
         *Test make scheduled investment withdraw
         */
    public function testScheduledWithdraw()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a client can make scheduled withdraw for all amount');

        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);
        $user = $I->login();
        $user_id = $user->id;
        $I->seeAuthentication();

        //Page
        $I->amOnPage("/dashboard/investments/withdraw/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Withdraw Investment');


        //Withdraw
        $formData = [
            'investment_id' => Crypt::encrypt($investment_id),
            'callback' => true,
            'premature' => false,
            'end_date' => false,
            'partial_withdraw' => false,
            'amount' => 4000,
            'new_maturity_date' => false,
        ];
        $I->sendAjaxPostRequest(route('withdraw_schedule_path'), $formData);
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
    }


    /**
         *Test make shecduled partial investment withdraw
         */
    public function testScheduledPartialPrematureWithdraw()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can make scheduled partial premature withdraw');

        //Login and variables
        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $I->amOnPage("/dashboard/investments/withdraw/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Withdraw Investment');


        //Withdraw
        $formData = [
            'investment_id' => Crypt::encrypt($investment_id),
            'callback' => true,
            'premature' => true,
            'end_date' => Carbon::today()->addMonthsNoOverflow(1)->toDateString(),
            'partial_withdraw' => true,
            'amount' => 4000,
            'new_maturity_date' => Carbon::today()->addMonthsNoOverflow(15)->toDateString(),
        ];
        $I->sendAjaxPostRequest(route('withdraw_schedule_path'), $formData);

        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
    }


    /**
         *Test make investment interest payments
         */
    public function testInterestPayment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view and make interest payments on investments');

        //Login and variables
        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/pay/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Interest payment for Investment');


        //Add Payment
        $formData = [
            'investment_id' => $investment_id,
            'date_paid' => Carbon::today()->addWeeks(2)->toDateString(),
            'amount' => 24
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/pay/' . $investment_id, $formData);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('Payment has been saved for approval');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //Approve and See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('interest_payments', $formData);
        $I->see('Payment has been added');


        //View Interests table
        $I->amOnPage('/dashboard/investments/pay/' . $investment->id);
        $I->see('Payment Trail');
        $I->sendAjaxGetRequest('/api/investments/interest/' . $investment->id . '/calculate');
        $I->seeResponseCodeIs(200);
    }


    /**
         *Test make investment deductions
         */
    public function testInvestmentDeductions()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view and make investments deductions');


        //Login and variables
        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $I->amOnPage("/dashboard/investments/deductions/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Investment deductions');

        //Investment Details
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id));
        $I->see($investment->id);

        //deduct From an Investment
        $I->see('Deduct from investment');
        $formData = [
            'amount' => 25000,
            'date' => Carbon::today()->toDateString(),
            'narrative' => 'Just deduct'
        ];
        $I->sendAjaxPostRequest(route('deduct_investment', [$investment_id]), $formData);
        $I->seeResponseCodeIs(200);

        //Approval
        $I->see('The investment deduction has been saved for approval');
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_deductions', [
            'amount' => 25000,
            'date' => Carbon::today()->toDateString()
        ]);
        $I->see('The deduction was succesfully made');

        //See Deductions
        $I->amOnPage("/dashboard/investments/deductions/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Deductions');
        $deduction = ClientDeduction::where('investment_id', $investment_id)->first();
        $I->see(\Cytonn\Presenters\DatePresenter::formatDate($deduction->date));
        $I->see(\Cytonn\Presenters\AmountPresenter::currency($deduction->amount));
    }


    /**
         *Test rollback investments for individual clients
         */
    public function testRollbackInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can view and make investment rollbacks');

        //Login and variables
        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        //Page
        $I->amOnPage("/dashboard/investments/clientinvestments/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Client Investment');

        //Investment Details
        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id));
        $I->see($investment->id);

        //Rollback
        $formData = ['reason' => 'Just rollback again'];
        $I->sendAjaxPostRequest(route('rollback_path', [$investment_id]), $formData);
        $I->seeResponseCodeIs(200);

        //Request Approval
        $I->see('The investment rollback has been saved for approval');
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;


        //Approve
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->see('The investment has been rolled back');
    }


    /**
         *Test rollover investments with topup
         */
    public function testRolloverwithTopup()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can combine and rollover an investment');

        //Login and variables
        $I->haveClient();
        $user = $I->login();
        $user_id = $user->id;
        $recipient = $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/combinedrollover/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Combine and Rollover Investment');


        //Rollover
        $formData = [
            'investment_id' => Crypt::encrypt($investment_id),
            'reinvest' => 'withdraw',
            'amount' => 2400,
            'interest_rate' => 5,
            'product_id' => 1,
            'invested_date' => Carbon::today()->addMonthsNoOverflow(11)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(13)->toDateString(),
            'on_call' => true,
            'commission_recepient' => $recipient,
            'commission_rate' => 4,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31
        ];
        $I->sendAjaxPostRequest(route('rollover_schedule_path'), $formData);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('The investment rollover has been scheduled');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('The rollover has been scheduled');
    }


    /**
         *Test combine rollover with withdrawal
         */
    public function testCombinedRolloverWithWithdraw()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can combine, rollover and Withdraw an investment having same day maturities');

        //Login and variables
        $I->haveClient();
        $I->haveCommission();
        $user = $I->login();
        $user_id = $user->id;
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/combinedrollover/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Combine and Rollover Investment');

        $investment2 = $I->haveClientInvestment(['maturity_date' => $investment->maturity_date]);


        //Combine
        $formData = [
            $investment2 => true,
            $investment_id => true
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/combinedrollover/' . $investment->id, $formData);
        $I->seeResponseCodeIs(200);

        $I->see('Combine and Rollover Investments');
        $combineData = [
            'investment' => Crypt::encrypt($investment->id),
            'investments' => json_encode($investment->lists('id')),
            'reinvest' => 'withdraw',
            'amount' => 2400,
            'interest_rate' => 2,
            'invested_date' => Carbon::today()->addMonthsNoOverflow(16)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(26)->toDateString(),
            'on_call' => false,
            'commission_recepient' => $I->haveCommissionRecepient(),
            'commission_rate' => 4,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31
        ];
        $I->sendAjaxPostRequest(route('combined_rollover_path', [$investment->id]), $combineData);
        $I->seeResponseCodeIs(200);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('Rollover transaction saved for approval');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');

        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('Investment Combined and Rolled Over Successfully');
    }


    /**
         *Test combine investment rollover with topup
         */
    public function testCombinedRolloverwithTopup()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can combine, rollover and Topup an investment having same day maturities');

        //Login and variables
        $I->haveClient();
        $I->haveCommission();
        $user = $I->login();
        $user_id = $user->id;
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);


        //Page
        $I->amOnPage("/dashboard/investments/combinedrollover/" . $investment_id);
        $I->seeResponseCodeIs(200);
        $I->see('Combine and Rollover Investment');

        $investment2 = $I->haveClientInvestment(['maturity_date' => $investment->maturity_date]);


        //Combine
        $formData = [
            $investment2 => true,
            $investment_id => true
        ];
        $I->sendAjaxPostRequest('/dashboard/investments/combinedrollover/' . $investment->id, $formData);
        $I->seeResponseCodeIs(200);

        $I->see('Combine and Rollover Investments');
        $combineData = [
            'investment' => Crypt::encrypt($investment->id),
            'investments' => json_encode($investment->lists('id')),
            'reinvest' => 'topup',
            'amount' => 2400,
            'interest_rate' => 2,
            'invested_date' => Carbon::today()->addMonthsNoOverflow(16)->toDateString(),
            'maturity_date' => Carbon::today()->addMonthsNoOverflow(26)->toDateString(),
            'on_call' => false,
            'commission_recepient' => $I->haveCommissionRecepient(),
            'commission_rate' => 4,
            'commissionRecipient' => true,
            'interest_payment_interval' => 1,
            'interest_payment_date' => 31
        ];
        $I->sendAjaxPostRequest(route('combined_rollover_path', [$investment->id]), $combineData);
        $I->seeResponseCodeIs(200);


        //Approval Sent
        $I->seeCurrentUrlEquals('/dashboard/investments');
        $I->see('Rollover transaction saved for approval');
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user_id]);
        $approve_id = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user_id)->latest()->first()->id;

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approve_id);
        $I->see('Awaiting Approval');


        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('Investment Combined and Rolled Over Successfully');
    }


    /**
         *Test transfer investments to between existing clients
         */
    public function testTransferInvestment()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can transfer investments from one client to another');

        //Login and variables
//        $I->haveCommission();
        $I->haveCommissionRecepient();
        $investment_id = $I->haveClientInvestment();
        $investment = ClientInvestment::find($investment_id);

        $client2 = $I->haveClient();


        //Page
        $user = $I->login();
        $I->amOnPage("/dashboard/investments/transfer/" . $investment->id);
        $I->seeResponseCodeIs(200);
        $I->see('Transfer Investments');


        $formData = [
            'client_id' => $client2,
            'date' => Carbon::today()->toDateString(),
            'narration' => 'Transfer to Mr Client 2'
        ];

        $I->sendAjaxPostRequest('/dashboard/investments/transfer/' . $investment->id, $formData);
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_transaction_approval', ['client_id' => $investment->client_id, 'sent_by' => $user->id]);
        $approval = ClientTransactionApproval::where('client_id', $investment->client_id)->where('sent_by', $user->id)->latest()->first();

        //See Record
        $I->amOnPage('/dashboard/investments/approve/' . $approval->id);
        $I->see('Awaiting Approval');
        $I->click('Approve');
        $I->seeResponseCodeIs(200);
        $I->seeRecord('client_investments', ['id' => $investment_id]);
        $I->see('The transfer was successfully made');
    }
}
