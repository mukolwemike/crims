<?php
use Cytonn\Core\DataStructures\Carbon;

class StatementsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    /**
     *Test add client to campaigns and send campaigns to the individual clients
     */
    public function testAddClientToCampaign()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can add Clients to campaign');

        //Login and Variables
        $I->haveClient();
        $inv_id = $I->haveClientInvestment();
        $inv = ClientInvestment::find($inv_id);
        $camp_id = $I->haveStatementCampaign();
        $camp = StatementCampaign::find($camp_id);
        $I->login();

        //Page
        $I->amOnPage('/dashboard/investments/summary/client/'.$inv->client_id);
        $I->see('Client CMS Summary');

        //Add Client to Statement
        $clientData = [
            'campaign'=>$camp->id,
            'client'=>$inv->client_id
        ];
        $I->sendAjaxPostRequest(route('add_client_to_statement_campaign'), $clientData);
        $I->seeResponseCodeIs(200);
        $I->see('Client has been added to campaign');

        //send Campaign to Individual Client
        $I->wantTo('Test whether a user can send Statement Campaign to Individual Clients');
        $I->seeInCurrentUrl('/dashboard/investments/summary/client/'.$inv->client_id);
        $I->sendAjaxPostRequest(route('send_individual_statement', [$inv->client_id]));
        $I->seeResponseCodeIs(200);
        $I->see('The statement is being sent to the client');

        //Preview Statement
        $I->sendAjaxPostRequest(route('show_statement', [$inv->product_id, $inv->client_id]), ['date'=>Carbon::today()->toDateString()]);
        $I->seeResponseCodeIs(200);
        //TODO cannot preview PDF's: Use Acceptance Tests
    }


    /**
     *Test create, view, and send statement campaigns to all clients
     */
    public function testCreateViewSendCampaign()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can Create and View Campaing Items in the Client CMS Statements Grid');

        //Login and Variables
        $I->haveClient();
        $inv_id = $I->haveClientInvestment();
        $inv = ClientInvestment::find($inv_id);

        //Page
        $I->login();
        $I->amOnPage('/dashboard/investments/statements');
        $I->seeResponseCodeIs(200);
        $I->see('Client CMS Statements');

        //Create Campaign
        $I->haveRecord('statement_campaign_types', $typ = ['id'=>2, 'name'=>'Automated', 'slug'=>'automated']);
        $formData = [
            'name'=>'CMS March2016 statements',
            'type_id'=>$typ['id'],
            'send_date'=>Carbon::today()->toDateString()
        ];
        $I->sendAjaxPostRequest(route('create_statement_campaign'), $formData);
        $I->seeResponseCodeIs(200);

        //See Campaign is created
        $I->seeRecord('statement_campaigns', $formData);
        $statement = StatementCampaign::where('name', $formData['name'])->first();
        $I->see($statement->name);

        //switch user
        $I->login();

        //Send Automated Campaigns
        $I->wantTo('Test whether a user can send campaign statemennt to dedicated users');
        $I->amOnPage('/dashboard/investments/statements/campaign/detail/'.$statement->id);
        $I->see('Client Statement Campaigns');

        $I->sendAjaxPostRequest(route('send_statement_campaign', [$statement->id]), ['campaign'=>$statement->id]);
        $I->seeResponseCodeIs(200);

        //See Campaign sent
        $I->see('The statements are being sent to the clients');
        $I->seeInCurrentUrl('/dashboard/investments/statements/campaign/detail/'.$statement->id);

        \Artisan::call('statements:send', ['type'=>'investments']);

//        $mock = Mockery::mock('Swift_Mailer');
//        \App::make('mailer')->setSwiftMailer($mock);
//
//        $mock->shouldReceive('send')->once()
//            ->andReturnUsing(function($msg) {
//                $this->assertEquals('My subject', $msg->getSubject());
//            });
    }
}
