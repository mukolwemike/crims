<?php
namespace RE;

use Carbon\Carbon;

class ProjectSetupTest extends \Codeception\TestCase\Test
{
    /**
     * @var \RealestateTester
     */
    protected $tester;

    /**
     * @var $user
     */
    protected $user;

    /**
     * @var $project
     */
    protected $project;

    /**
     * Initialize
     */
    protected function _before()
    {
        $I = $this->tester;
        $I->haveClient();
        $this->user = $I->login();
        $I->seeAuthentication();
    }

    /**
     * I want to view setup details of an existing real estate project
     */
    public function testViewProjectSetupDetails()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());

        $I->am('a user');
        $I->wantTo('view setup details of an existing real estate project');

        $I->amOnPage('/dashboard/realestate/projects/setup/' . $project->id);
        $I->see('Setup Project');
        $I->see('Number of Units & Sizes');
        $I->see('Unit Tranches');
        $I->see('Commission rates');
    }

    /**
     * I want to add number and unit size for a real estate project
     */
    public function testAddNumberAndUnitSize()
    {
        $I = $this->tester;
        $project_id = $I->haveProject();
        $I->am('a user');
        $I->wantTo('add number and unit size for a real estate project');

        $I->haveRecord('realestate_unit_sizes', ['id'=>rand(101, 200), 'name'=>'5 Bedroom']);

        $I->amOnPage('/dashboard/realestate/projects/setup/' . $project_id);
        $I->click('Add a size', 'button');
        $I->see('Add the size and total number of units');

        $existing = \RealEstateSizeNumber::find($I->haveSizeNumbers())->lists('size_id');
        $formData = [
            'size_id'           =>  \RealestateUnitSize::whereNotIn('id', $existing)->get()->random()->id,
            'number'            =>  rand(1, 100)
        ];
        $I->sendAjaxPostRequest(route('setup_unit_numbers', [$project_id]), $formData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'set_project_unit_numbers',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null
        ]);
        $I->approveTransaction($I->getLatestTransaction('set_project_unit_numbers'));
        $I->seeRecord('realestate_size_numbers', ['number' => $formData['number']]);
    }

    /**
     * I want to edit number and unit size for a real estate project
     */
    public function testEditNumberAndUnitSize()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('edit number and unit size for a real estate project');

        $project = \Project::find($I->haveProject());

        $I->amOnPage('/dashboard/realestate/projects/setup/'.$project->id);

        $formData = [
            'id'                    =>  \RealEstateSizeNumber::find($I->haveSizeNumbers())->id,
            'size_id'               =>  \RealestateUnitSize::latest()->first()->id,
            'number'                =>  rand(101, 200)
        ];
        $I->sendAjaxPostRequest(route('setup_unit_numbers', [$project->id]), $formData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'set_project_unit_numbers',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null
        ]);
        $I->approveTransaction($I->getLatestTransaction('set_project_unit_numbers'));
        $I->seeRecord('realestate_size_numbers', ['number' => $formData['number']]);
    }

    /**
     * I want to add unit tranche for a real estate project
     */
    public function testAddUnitTranche()
    {
        $I = $this->tester;

        $project = \Project::find($I->haveProject());

        $I->am('a user');
        $I->wantTo('add unit tranche for a real estate project');

        $I->amOnPage('/dashboard/realestate/projects/setup/' . $project->id);
        $I->click('Add a tranche', 'button');
        $I->see('Add a tranche', 'h4');
        $size = (int) explode(' ', \RealestateUnitSize::latest()->first()->name)[0];
        $formData = [
            'name'                  =>  'Christmas Offer',
            'date'                  =>  Carbon::today()->toDateString(),
            'size_id'               =>  [\RealestateUnitSize::latest()->first()->id],
            'ident'                 =>  [$size],
            'number'                =>  [rand(5, 20)],
            'payment_plan_id'       =>  [$size => [\RealEstatePaymentPlan::find($I->havePaymentPlan())->id]],
            'price'                 =>  [$size => [rand(800000, 1200000)]]
        ];
        $I->sendAjaxPostRequest(route('add_realestate_tranche', [$project->id]), $formData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'real_estate_project_tranche',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null
        ]);
        $I->approveTransaction($I->getLatestTransaction('real_estate_project_tranche'));
        $I->seeRecord('realestate_unit_tranches', ['name' => $formData['name']]);
    }

    /**
     * I want to edit unit tranche for a real estate project
     */
    public function testEditUnitTranche()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());

        $I->am('a user');
        $I->wantTo('edit unit tranche for a real estate project');

        $I->amOnPage('/dashboard/realestate/projects/setup/' . $project->id);
        $size = (int) explode(' ', \RealestateUnitSize::latest()->first()->name)[0];
        $formData = [
            'name'                  =>  'Christmas Offer - Edited',
            'date'                  =>  Carbon::today()->toDateString(),
            'sizing_id'             =>  [\RealEstateUnitTrancheSizing::find($I->haveTrancheSizing())->id],
            'size_id'               =>  [\RealestateUnitSize::latest()->first()->id],
            'ident'                 =>  [$size],
            'number'                =>  [rand(5, 20)],
            'price_id'              =>  [\RealEstateUnitTrancheSizing::find($I->haveTrancheSizing())->size_id => [\RealEstateUnitTranchePricing::find($I->haveTranchePricing())->id]],
            'payment_plan_id'       =>  [$size => [\RealEstatePaymentPlan::latest()->first()->id]],
            'price'                 =>  [$size => [rand(800000, 1200000)]]
        ];
        $I->sendAjaxPostRequest(route('add_realestate_tranche', [$project->id, \RealEstateUnitTranche::latest()->first()->id]), $formData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'real_estate_project_tranche',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null
        ]);
        $I->approveTransaction($I->getLatestTransaction('real_estate_project_tranche'));
        $I->seeRecord('realestate_unit_tranches', ['name' => $formData['name']]);
    }

    /**
     * I want to view individual tranche details for a real estate project
     */
    public function testViewIndividualTrancheDetails()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());

        $I->am('a user');
        $I->wantTo('view individual tranche details for a real estate project');

        $tranche = \RealEstateUnitTranche::find($I->haveTranche());

        $I->amOnPage('/dashboard/realestate/projects/setup/'.$project->id);
        $I->see('Setup project');
//        $I->click('//table[2]/tbody/tr[1]/td[last()]/a[last()]');
//        $I->see($tranche->name, 'h4');
//        $I->seeElement('//table/tbody/tr');
    }

    /**
     * I want to add commission rate for a real estate project
     */
    public function testAddCommissionRate()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());
        $I->haveCommissionRecepient();

        $I->am('a user');
        $I->wantTo('add commission rate for a real estate project');

        $I->amOnPage('/dashboard/realestate/projects/setup/' . $project->id);
        $I->click('Create New', 'button');
        $I->see('Create a Commission Rate', 'h4');
        $formData = [
            'project_id'            =>  $project->id,
            'recipient_type_id'     =>  \CommissionRecipientType::latest()->first()->id,
            'type'                  =>  'percentage',
            'amount'                =>  rand(1, 5)];
        $I->sendAjaxPostRequest(route('create_realestate_commission_rate'), $formData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'save_real_estate_commission_rate',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null
        ]);
        $I->approveTransaction($I->getLatestTransaction('save_real_estate_commission_rate'));
        $I->seeRecord('realestate_commission_rates', ['amount' => $formData['amount']]);
    }

    /**
     * I want to edit commission rate for a real estate project
     */
    public function testEditCommissionRate()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());
        $I->haveCommissionRecepient();

        $I->am('a user');
        $I->wantTo('edit commission rate for a real estate project');

        $I->amOnPage('/dashboard/realestate/projects/setup/' . $project->id);
        $formData = [
            'project_id'            =>  $project->id,
            'recipient_type_id'     =>  \CommissionRecipientType::latest()->first()->id,
            'type'                  =>  'percentage',
            'amount'                =>  rand(6, 10)];
        $I->sendAjaxPostRequest(route('create_realestate_commission_rate'), $formData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'save_real_estate_commission_rate',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null
        ]);
        $I->approveTransaction($I->getLatestTransaction('save_real_estate_commission_rate'));
        $I->seeRecord('realestate_commission_rates', ['amount' => $formData['amount']]);
    }
}
