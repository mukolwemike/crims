<?php
namespace RE;

use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Faker\Factory as Faker;

class UnitHoldingTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    /**
     * @var $unit
     */
    protected $unit;

    protected function _before()
    {
        $I = $this->tester;
        $I->haveClient();
        $this->user = $I->login();
        $I->seeAuthentication();
        $this->unit = $I->haveUnit();
        $I->haveRecord('realestate_payment_types', ['id'=>1, 'slug'=>'reservation_fee', 'name'=>'Reservation Fees']);
        $I->haveRecord('realestate_payment_types', ['id'=>2, 'slug'=>'deposit', 'name'=>'10% Deposit']);
        $I->haveRecord('realestate_payment_types', ['id'=>3, 'slug'=>'installment', 'name'=>'Installment']);
    }

    /**
     * I want to reserve a unit by creating individual client for a project in real estate
     */
    public function testReserveUnitByCreatingIndividualClient()
    {
        $I = $this->tester;
        $I->am('a user');
        $unit = $I->haveUnit();
        $I->wantTo('to reserve a unit by creating a new individual client for a project in real estate');
        $I->amOnPage('/dashboard/realestate/projects/'.$unit->project_id.'/units/reserve/'.$unit->id);
        $I->see($unit->project->name . ' - reserve a unit');
        $I->seeElement('//form');
        $clientTestData = array_except($I->getClientTestData(), [
            'corporate_registered_name',
            'corporate_trade_name',
            'registered_address',
            'contact_person_lname',
            'contact_person_fname',
            'telephone_cell',
            'dob',
            'gender_id',
            'residential_address',
            'method_of_contact_id'
        ]);
        $I->submitForm('form', $clientTestData);
        $I->seeRecord('contacts', ['firstname'=>$clientTestData['firstname']]);
        $I->seeRecord('clients', ['pin_no'=>$clientTestData['pin_no']]);
        $I->seeRecord('unit_holdings', ['client_id'=>\Client::latest()->first()->id]);
    }

    /**
     * I want to reserve a unit by creating corporate client for a project in real estate
     */
    public function testReserveUnitByCreatingCorporateClient()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to reserve a unit by creating a new corporate client for a project in real estate');
        $I->amOnPage('/dashboard/realestate/projects/' . $this->unit->project_id . '/units/reserve/' . $this->unit->id);
        $I->see($this->unit->project->name . ' - reserve a unit');
        $I->seeElement('//form');
        $clientTestData = array_except($I->getClientTestData(), [
            'title_id',
            'firstname',
            'middlename',
            'lastname',
            'telephone_cell',
            'dob',
            'gender_id',
            'residential_address',
            'method_of_contact_id'
        ]);
        $clientTestData['client_type_id'] = \ClientType::where('name', 'corporate')->first()->id;
        $I->submitForm('form', $clientTestData);
        $I->seeRecord('contacts', ['corporate_registered_name' => $clientTestData['corporate_registered_name']]);
        $I->seeRecord('clients', ['contact_person_fname' => $clientTestData['contact_person_fname']]);
        $I->seeRecord('unit_holdings', ['client_id' => \Client::latest()->first()->id]);
    }

    /**
     * I want to reserve a unit for existing client for a project in real estate
     */
    public function testReserveUnitForExistingClient()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to reserve a unit for existing client for a project in real estate');
        $I->amOnPage('/dashboard/realestate/projects/'. $this->unit->project_id . '/units/reserve/' . $this->unit->id);
        $I->see($this->unit->project->name . ' - reserve a unit');
        $I->seeElement('//form');
        $clientTestData = array_only($I->getClientTestData(), [
            'client_type_id',
            'new_client',
            'client_id',
            'contact_person_title'
        ]);
        $existing_client = \Client::latest()->first();
        $clientTestData['new_client'] = 'no';
        $clientTestData['client_id'] = $existing_client->id;
        $I->submitForm('form', $clientTestData);
        $I->seeRecord('unit_holdings', ['client_id' => $existing_client->id]);
    }

    /**
     * I want to view a client's reservation application for a unit for a project in real estate
     */
    public function testViewClientReservationApplication()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to view  a client\'s reservation application for a unit for a project in real estate');
        $this->testReserveUnitForExistingClient();
        $holding = \UnitHolding::latest()->first();
        $I->amOnPage('/dashboard/realestate/projects/' . $holding->project->id . '/units/show/' . $holding->unit_id);
        $I->see('Reservation form', 'a');
        $I->click('Reservation form', 'a');
        $reservation_form = \ReservationForm::latest()->first();
        $I->seeCurrentUrlEquals('/dashboard/realestate/reservations/show/'.$reservation_form->id);
        $I->see(ClientPresenter::presentFullNames($reservation_form->client_id));
    }

    /**
     * I want to add an additional purchaser for a unit for a project in real estate
     */
    public function testAddAdditionalPurchaser()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to add an additional purchaser for a unit for a project in real estate');
        $this->testViewClientReservationApplication();
        $I->click('Add additional purchaser', 'a');
        $reservation_form = \ReservationForm::latest()->first();
        $I->seeCurrentUrlEquals('/dashboard/realestate/reservations/joint/'.$reservation_form->id);
        $additionalPurchaserTestData = array_only($I->getClientTestData(), $I->getAdditionalPurchaserRequiredFields());
        $additionalPurchaserTestData['client_id'] = $reservation_form->client_id;
        $I->submitForm('form', $additionalPurchaserTestData);
        $I->seeRecord('client_joint_details', ['lastname'=>$additionalPurchaserTestData['lastname']]);
    }

    /**
     * I want to forfeit a unit for a project in real estate
     */
    public function testForfeitUnit()
    {
        $I = $this->tester;
        $I->haveUnit();
        $I->am('a user');
        $I->wantTo('to forfeit a unit for a project in real estate');
        $this->testReserveUnitForExistingClient();
        $I->click('Forfeit unit', 'a');
        $I->see('Forfeit a unit', 'h4');
        $forfeitUnitTestData = [
            'forfeiture_date'           =>  Carbon::today()->toDateString(),
            'amount'                    =>  0,
            'narration'                 =>  'Umami polaroid pug enamel pin small batch try-hard. Bicycle rights neutra vinyl master cleanse, tumeric squid kickstarter hoodie.'
        ];
        $holding = \UnitHolding::latest()->first();
        $I->sendAjaxPostRequest(route('unit_forfeiture', $holding->id), $forfeitUnitTestData);
        $I->approveTransaction($I->getLatestTransaction('real_estate_unit_forfeiture'));
        $I->seeRecord('unit_holdings', ['forfeit_date'=>$forfeitUnitTestData['forfeiture_date']]);
    }

    /**
     * I want to add reservation fee for a unit for a project in real estate
     */
    public function testAddReservationFee()
    {
        $I = $this->tester;
        $faker = Faker::create();
        $I->am('a user');
        $I->wantTo('to add reservation fee for a unit for a project in real estate');
        $this->testReserveUnitForExistingClient();
        $I->click('Add reservation fee', 'a');
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $sizing = \RealEstateUnitTrancheSizing::find($I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]));
        $I->haveTranchePricing(['sizing_id'=>$sizing->id, 'payment_plan_id'=>$holding->payment_plan_id]);
        $I->haveReservationForm(['holding_id'=>$holding->id, 'client_id'=>$holding->client_id]);

        $I->amOnPage('/dashboard/realestate/payments/create/'.$holding->id.'/reservation_fee');
        $I->see('Make a payment');

        $reservationFeeTestData = [
            'date'                      =>  Carbon::today()->toDateString(),
            'amount'                    =>  rand(1, 1000000000),
            'description'               =>  $faker->paragraph(1),
            'schedule_id'               =>  $I->haveSchedule(),
            'payment_plan_id'           =>  $holding->payment_plan_id,
            'tranche_id'                =>  $holding->tranche_id,
            'recipient_id'              =>  $I->haveCommissionRecepient(),
            'awarded'                   =>  rand(0, 1),
            'client_code'               =>  '45646',
            'negotiated_price'          =>  rand(1, 100000000)
        ];
        $I->sendAjaxPostRequest(route('save_realestate_payment', $holding->id), $reservationFeeTestData);
        $I->seeResponseCodeIs(200);
        $I->approveTransaction($I->getLatestTransaction('make_real_estate_payment'));
        $I->seeResponseCodeIs(200);
        $I->seeRecord('realestate_payments', ['amount'=>$reservationFeeTestData['amount']]);
    }

    /**
     * I want to add payment schedules for a unit for a project in real estate
     */
    public function testAddPaymentSchedules()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to add payment schedules for a unit for a project in real estate');

        $this->testReserveUnitForExistingClient();
        $I->click('Add reservation fee', 'a');
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $sizing = \RealEstateUnitTrancheSizing::find($I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]));
        $I->haveTranchePricing(['sizing_id'=>$sizing->id, 'payment_plan_id'=>$holding->payment_plan_id]);
        $I->haveReservationForm(['holding_id'=>$holding->id, 'client_id'=>$holding->client_id]);

        $I->amOnPage('/dashboard/realestate/payments-schedules/show/'.$holding->id);
        //On Page
        $I->see('Payment Schedules');
        $I->click('Generate');

        //Add Schedule
        $formData = [
            'description'=>['Try to add a payment schedule'],
            'payment_type_id'=>[\RealEstatePaymentType::latest()->first()->id],
            'date'=>[Carbon::today()->toDateString()],
            'amount'=>[rand(1, 29755682)]
        ];
        $I->sendAjaxPostRequest(route('save_realestate_payment_schedule', [$holding->id]), $formData);
        $I->seeResponseCodeIs(200);

        //See schedule added
        $I->see('The schedule item has been saved for approval');
        $I->approveTransaction($I->getLatestTransaction('add_real_estate_payment_schedule'));

        $I->seeRecord('realestate_payments_schedules', ['description'=>$formData['description']]);


        //View Schedule on added on grid
        $I->amOnPage('/dashboard/realestate/payments-schedules/show/'.$holding->id);
        $I->see('Payment Schedules');
        $I->see(DatePresenter::formatDate($formData['date'][0]));
        $I->see(AmountPresenter::currency($formData['amount'][0]));
        $I->see($formData['description'][0]);
    }

    /**
     * I want to edit pricing information for a unit for a project in real estate
     */
    public function testEditPricingInformation()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to edit pricing information for a unit for a project in real estate');
        $this->testReserveUnitForExistingClient();
        $I->click('Add reservation fee', 'a');
        $I->haveCommissionRecepient();
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $sizing = \RealEstateUnitTrancheSizing::find($I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]));
        $recipient = \CommissionRecepient::latest()->first();
        $I->haveTranchePricing(['sizing_id'=>$sizing->id, 'payment_plan_id'=>$holding->payment_plan_id]);
        $I->haveReservationForm(['holding_id'=>$holding->id, 'client_id'=>$holding->client_id]);
        $I->haveRECommissions(['holding_id'=>$holding->id, 'recipient_id'=>$recipient->id]);

        $I->amOnPage('/dashboard/realestate/projects/'.$holding->unit->project_id.'/units/show/'.$holding->unit_id);

        $pricingInformation = [
            'tranche_id'                =>  $holding->tranche_id,
            'payment_plan_id'           =>  \RealEstatePaymentPlan::latest()->first()->id,
            'recipient_id'              => $recipient->id,
            'awarded'                   =>  0,
            'negotiated_price'          =>  rand(900000, 150000000)
        ];

        $I->sendAjaxRequest('PUT', route('update_realestate_pricing_information', [$holding->id]), $pricingInformation);
        $I->seeResponseCodeIs(200);
        $I->approveTransaction($I->getLatestTransaction('update_real_estate_pricing_details'));
        $I->seeRecord('unit_holdings', ['tranche_id'=>$pricingInformation['tranche_id']]);
    }
}
