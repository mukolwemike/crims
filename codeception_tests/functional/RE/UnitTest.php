<?php
namespace RE;

class UnitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \RealestateTester
     */
    protected $tester;

    /**
     * @var $user
     */
    protected $user;

    /**
     * @var $unit
     */
    protected $unit;

    protected function _before()
    {
        $I = $this->tester;
        $I->haveClient();
        $this->user = $I->login();
        $I->seeAuthentication();
        $this->unit = $I->haveUnit();
    }

    /**
     * I want to see units grid for a project in real estate
     */
    public function testUnitsGrid()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to see units grid for a project in realestate');
        $I->amOnPage('/dashboard/realestate/projects/show/'. $this->unit->project_id);
        $I->see('All Units');
        $I->seeElement('//table[last()]/tbody/tr');

        $I->haveRecord('realestate_units', ['number'=>'A001']);

        $I->sendAjaxGetRequest('/api/realestate/projects/' . $this->unit->project_id . '/units');
        $I->canSeeResponseCodeIs(200);
    }

    /**
     * I want to see cancelled units grid for a project in real estate
     */
    public function testcancelledUnitsGrid()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to see cancelled units grid for a project in real estate');
        $I->amOnPage('/dashboard/realestate/projects/show/'. $this->unit->project_id);
        $I->click('Cancelled Units', 'a');
        $I->seeCurrentUrlEquals('/dashboard/realestate/projects/cancelled/'. $this->unit->project_id);
        $I->see('Cancelled Units', 'h3');
        $I->seeElement('//table[last()]/tbody/tr');

        $I->sendAjaxGetRequest('/api/realestate/projects/' . $this->unit->project_id . '/cancelled-units');
        $I->canSeeResponseCodeIs(200);
    }

    /**
     * I want to create a unit for a project in real estate
     */
    public function testCreateUnit()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to create a unit for a project in real estate');
        $I->amOnPage('/dashboard/realestate/projects/show/'. $this->unit->project_id);
        $I->click('Create Unit', 'a');
        $I->seeCurrentUrlEquals('/dashboard/realestate/projects/'. $this->unit->project_id . '/units/create');
        $I->click('Create a unit');
        $unitTestData = $I->getUnitTestData();
        $unitTestData['project_id'] = \Project::latest()->first()->id;
        $I->submitForm('form', $unitTestData);
        $I->seeRecord('realestate_units', ['number'=>$unitTestData['number']]);
    }

    /**
     * I want to edit a unit for a project in real estate
     */
    public function testEditUnit()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to edit a unit for a project in real estate');

        $I->amOnPage('/dashboard/realestate/projects/'. $this->unit->project_id . '/units/create/' . $this->unit->id);
        $I->see('Create a unit');
        $unitTestData = $I->getUnitTestData();
        $unitTestData['project_id'] = \Project::latest()->first()->id;
        $I->sendAjaxPostRequest(route('store_realestate_unit', [$this->unit->project_id, $this->unit->id]), $unitTestData);
        $I->seeRecord('realestate_units', ['number'=>$unitTestData['number']]);
    }

    /**
     * I want to view a unit's reserve page
     */
    public function testViewUnitReservePage()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to view a unit\'s reserve page');

        $I->amOnPage('/dashboard/realestate/projects/'. $this->unit->project_id . '/units/show/' . $this->unit->id);
        $I->see('Reservation', 'h4');
        $I->see('Reserve', 'a');
    }
}
