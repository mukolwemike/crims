<?php
namespace RE;

use Cytonn\Presenters\ClientPresenter;

class CommissionsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Test RE commissions grid
     */
    public function testCommisiosGrid()
    {
        $I = $this->tester;

        $I->am('a user');
        $I->wantTo('Test whether a user can add RE payments');

        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $sizing = \RealEstateUnitTrancheSizing::find($I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]));
        $recipient = \CommissionRecepient::find($I->haveCommissionRecepient());
        $I->haveTranchePricing(['sizing_id'=>$sizing->id, 'payment_plan_id'=>$holding->payment_plan_id]);
        $I->haveReservationForm(['holding_id'=>$holding->id, 'client_id'=>$holding->client_id]);
        $commission = \RealestateCommission::find($I->haveRECommissions(['holding_id'=>$holding->id, 'recipient_id'=>$recipient->id]));

        //Page
        $I->amOnPage('/dashboard/realestate/commissions');
        $I->see('Real Estate Commissions');

        //Grid
        $I->see(ClientPresenter::presentFullNameNoTitle($commission->recipient_id));
    }


    /**
     * Test RE commissions grid
     */
    public function testViewCommissionPaymentDetails()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('Test whether a user can view RE commission payment details');

        //Variables
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $recipient = \CommissionRecepient::find($I->haveCommissionRecepient());
        $commission = \RealestateCommission::find($I->haveRECommissions(['holding_id'=>$holding->id, 'recipient_id'=>$recipient->id]));

        //Page
        $I->amOnPage('/dashboard/realestate/commissions/payment/'.$commission->recipient->id);
        $I->see('Real Estate Commission payments');

        //Details
        $I->see(ClientPresenter::presentFullNameNoTitle($commission->recipient_id));
        $I->see(\Cytonn\Presenters\AmountPresenter::currency($recipient->calculateRealEstateCommission()));
    }
}
