<?php
namespace RE;

use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Core\DataStructures\Carbon;
use Faker\Factory as Faker;

class PaymentsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
        $I = $this->tester;
        $I->haveClient();
        $this->user = $I->login();
        $I->seeAuthentication();
        $this->unit = $I->haveUnit();
    }

    protected function _after()
    {
    }


    /**
     * Test view payments on Grid
     */
    public function testViewPaymentsGrid()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('Test whether a user can view scheduled RE payments on the Payments Grid');

        //Page
        $I->amOnPage('/dashboard/realestate/payments');
        $I->see('Real Estate Payments');

        //Variables
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $payment = \RealEstatePayment::find($I->haveREPayment(['holding_id'=>$holding->id, 'schedule_id'=>$schedule->id]));

        //View Payment
        $I->sendAjaxGetRequest('/api/realestate/payments');
        $I->seeResponseCodeIs(200);
        $I->see(ClientPresenter::presentFullNames($payment->holding->client_id));
        $I->see($payment->holding->client->client_code);
        $I->see($holding->unit->project->name);
    }

    /**
     * View Individual Payment Details
     */
    public function testViewInvidualPaymentDetails()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('Test whether a user can view details for individual scheduled RE payments on the Payments Grid');

        //Variables
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $payment = \RealEstatePayment::find($I->haveREPayment(['holding_id'=>$holding->id, 'schedule_id'=>$schedule->id]));
        $I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]);

        //Page
        $I->amOnPage('/dashboard/realestate/payments/show/'.$payment->id);
        $I->see('Real Estate Payment');

        //Project and Payment Details
        $I->see($holding->unit->project->name);
        $I->see($payment->description);
        $I->see(AmountPresenter::currency($payment->amount));

        //Business Confirmation
        $I->see(\Cytonn\Presenters\AmountPresenter::currency($holding->price()));
        $I->see($payment->present()->scheduleName());
        $I->see(\Cytonn\Presenters\AmountPresenter::currency($payment->amount));
    }

    /**
     * Test add RE payments
     */
    public function testAddREPayment()
    {
        $I = $this->tester;
        $faker = Faker::create();

        $I->am('a user');
        $I->wantTo('Test whether a user can add RE payments');

        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $sizing = \RealEstateUnitTrancheSizing::find($I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]));
        $recipient = \CommissionRecepient::find($I->haveCommissionRecepient());
        $I->haveTranchePricing(['sizing_id'=>$sizing->id, 'payment_plan_id'=>$holding->payment_plan_id]);
        $I->haveReservationForm(['holding_id'=>$holding->id, 'client_id'=>$holding->client_id]);
        $I->haveRECommissions(['holding_id'=>$holding->id, 'recipient_id'=>$recipient->id]);

        //Page
        $I->amOnPage('/dashboard/realestate/payments/create/'.$holding->id);
        $I->see('Make a payment');

        //Project Details
        $I->see($holding->tranche->project->name);
        $I->see($holding->unit->number);

        //Add Payment
        $formData = [
            'date'=>Carbon::today()->toDateString(),
            'amount'=>rand(40000, 900000000),
            'description'=>$faker->paragraph,
            'schedule_id'=>$schedule->id,
            'tranche_id'=>$holding->tranche_id,
            'negotiated_price'=>rand(67700, 89980000),
            'payment_plan_id'=> \RealEstatePaymentPlan::latest()->first()->id,
            'recipient_id'=> $recipient->id,
            'awarded'=>1,
            'client_code'=>$faker->randomDigitNotNull.$faker->randomLetter
        ];
        $I->sendAjaxPostRequest(route('save_realestate_payment', [$holding->id]), $formData);
        $I->seeResponseCodeIs(200);

        //Approval
        $I->see('The payment has been saved for approval');
        $I->approveTransaction($I->getLatestTransaction('make_real_estate_payment'));
        $I->seeRecord('unit_holdings', ['tranche_id'=>$formData['tranche_id']]);
    }

    /**
     * Test edit RE payment schedule
     */
    public function testEditREPayments()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('Test whether a user can edit the details for individual scheduled RE payments on the Payments Grid');

        //Variables
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $payment = \RealEstatePayment::find($I->haveREPayment(['holding_id'=>$holding->id]));
        $I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]);

        //Page
        $I->amOnPage('/dashboard/realestate/payments/show/'.$payment->id);
        $I->see('Real Estate Payment');

        //Payment Details
        $I->see('Payment Detail');
        $I->see($payment->description);
        $I->see(AmountPresenter::currency($payment->amount));

        //Edit Payment
        $formData = [
            'schedule_id'=>$schedule->id,
            'description'=>'I just need to update',
            'payment_type_id'=>$payment->payment_type_id,
            'date'=>Carbon::today()->addMonthsNoOverflow(5)->toDateString(),
            'amount'=>rand(70000, 9000000000)
        ];

        $I->sendAjaxRequest('PUT', route('update_realestate_payment_schedule', [$schedule->id]), $formData);
        $I->seeResponseCodeIs(200);
        $I->see('The schedule payment update request has been saved for approval');
        $I->approveTransaction($I->getLatestTransaction('update_real_estate_payment_schedule'));
        $I->seeRecord('realestate_payments_schedules', ['amount'=>$formData['amount'], 'date'=>$formData['date']]);
    }


    /**
     * Test delete RE payment schedule
     */
    public function testDeleteREPayments()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('Test whether a user can delete existing details RE payments schedules');

        //Variables
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $payment = \RealEstatePayment::find($I->haveREPayment(['holding_id'=>$holding->id]));
        $I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]);

        //Page
        $I->amOnPage('/dashboard/realestate/payments/show/'.$payment->id);
        $I->see('Real Estate Payment');

        //Payment Details
        $I->see('Payment Detail');
        $I->see($payment->description);
        $I->see(AmountPresenter::currency($payment->amount));

        $I->sendAjaxRequest('DELETE', route('remove_realestate_payment_schedule', [$schedule->id]));
        $I->seeResponseCodeIs(200);

        //Approve Transaction
        $I->approveTransaction($I->getLatestTransaction('remove_real_estate_payment_schedule'));
        $I->dontSeeRecord('realestate_payments_schedules', ['description'=>$payment->description, 'amount'=>$payment->amount]);
    }

    /**
     * Test view upcoming payment Schedules on Grid
     */
    public function testViewPaymentSchedules()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('Test whether a user view existing payment schedules on grid');

        //Variables
        $holding = \UnitHolding::find($I->haveUnitHolding());
        $schedule = \RealEstatePaymentSchedule::find($I->haveSchedule(['unit_holding_id'=>$holding->id]));
        $payment = \RealEstatePayment::find($I->haveREPayment(['holding_id'=>$holding->id, 'schedule_id'=>$schedule->id]));
        $I->haveTrancheSizing(['tranche_id'=>$holding->tranche_id]);

        $I->amOnPage('/dashboard/realestate/payments');
        $I->see('Real Estate Payments');
        $I->click('Upcoming Payments');

        //Payment Schedules Grid
        $I->seeInCurrentUrl('/dashboard/realestate/payments-schedules');
        $I->see('Real Estate Upcoming Payments');
        $I->see($schedule->holding->client->client_code);
    }
}
