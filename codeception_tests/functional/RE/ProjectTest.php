<?php

namespace RE;

class ProjectTest extends \Codeception\TestCase\Test
{

    protected $tester;

    /**
     * @var $user
     */
    protected $user;

    /**
     * @var $project
     */
    protected $project;

    /**
     * @var $unitTestData
     */
    protected $unitTestData;

    /**
     * Initialize
     */
    protected function _before()
    {
        $I = $this->tester;
        $I->haveClient();
        $this->user = $I->login();
        $I->seeAuthentication();
        $this->project = $I->haveProject();
        $I->haveRecord('realestate_unit_sizes', $rus = ['id'=>rand(1, 1000), 'name'=>'1 Bedroom']);
        $I->haveRecord('realestate_unit_types', $rut = ['id'=>rand(1, 1000), 'name'=>'Apartment']);
        $this->unitTestData = [
            'number'                    =>  'A001',
            'size_id'                   =>  $rus['id'],
            'type_id'                   =>  $rut['id'],
        ];
    }

    /**
     * I can see realestate projects grid
     */
    public function testProjectsGrid()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('to see realestate projects grid');
        $I->amOnPage('/dashboard/realestate/projects');
        $I->see('Real Estate Projects');
        $I->seeElement('//table/tbody');

        $I->haveRecord('projects', ['name'=>'Amara']);

        $I->sendAjaxGetRequest('/api/realestate/projects');
        $I->canSeeResponseCodeIs(200);
        $I->see('Amara');
    }

    /**
     * I can create a new realestate project
     */
    public function testCreateProject()
    {
        $I = $this->tester;
        $I->am('a user');
        $I->wantTo('create a new real estate project');
        $I->amOnPage('/dashboard/realestate/projects');
        $I->click('Create Project');
        $I->seeCurrentUrlEquals('/dashboard/realestate/projects/create');

        $I->submitForm('form', $projectTestData = $I->getProjectTestData());
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'create_realestate_project',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null,
            'approved_by'           =>  null,
            'approved_on'           =>  null,
        ]);
        $I->approveTransaction($I->getLatestTransaction('create_realestate_project'));
        $I->seeRecord('projects', ['name'=>$projectTestData['name']]);
    }

    /**
     * I can edit a realestate project
     */
    public function testEditProject()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());

        $I->am('a user');
        $I->wantTo('edit an existing real estate project');

        $I->amOnPage('/dashboard/realestate/projects/create/' . $project->id);

        $projectTestData = $I->getProjectTestData();
        $I->submitForm('form', $projectTestData);
        $I->seeRecord('client_transaction_approval', [
            'client_id'             =>  null,
            'transaction_type'      =>  'create_realestate_project',
            'sent_by'               =>  $this->user->id,
            'approved'              =>  null,
            'approved_by'           =>  null,
            'approved_on'           =>  null,
        ]);
        $I->approveTransaction($I->getLatestTransaction('create_realestate_project'));
        $I->seeRecord('projects', ['name'=>$projectTestData['name']]);
    }

    /**
     * I can view details and units of existing real estate project
     */
    public function testViewProjectDetails()
    {
        $I = $this->tester;
        $project = \Project::find($I->haveProject());

        $I->am('a user');
        $I->wantTo('view details and units of existing real estate project');

        $unit = new \RealestateUnit($this->unitTestData);
        $project->units()->save($unit);

        $I->amOnPage('/dashboard/realestate/projects/show/' . $project->id);
        $I->see('Project Details');
        $I->see('Real Estate - ' .$project->name);

        //Units under a project
        $I->sendAjaxGetRequest('/api/realestate/projects/' . $project->id . '/units');
        $I->canSeeResponseCodeIs(200);
        $I->see($this->unitTestData['number']);
    }
}
