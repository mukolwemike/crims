<?php


class DashboardTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     *Test view the activity log and notifications tables as displayed on the CRIMS Dashboard
     */
    public function testActivityAndNotificationLog()
    {
        $I = $this->tester;
        $I->wantTo('Test whether a user can access and view DashBoard Itesms');

        //Login and variables
        $I->haveApplication();
        $I->login();


        //Approvals Page
        $I->amOnPage('/Dashboard');
        $I->seeResponseCodeIs(200);
        $I->see('Dashboard');

        //Notifications
        $approval = ClientTransactionApproval::find($I->haveTransactionApproval());
        $I->sendAjaxGetRequest('/api/activity_log');
        $I->seeResponseCodeIs(200);
//        $I->see(\Cytonn\Presenters\ClientPresenter::presentFullNames($approval->client_id));


        //Notifications
        $I->sendAjaxGetRequest('/api/notifications');
        $I->seeResponseCodeIs(200);
    }
}
