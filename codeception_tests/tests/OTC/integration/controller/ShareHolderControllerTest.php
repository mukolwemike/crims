<?php

namespace App\Tests\OTC\Integration\Controller;

use Illuminate\Support\Facades\Redirect;
use Laracasts\TestDummy\DbTestCase;

class ShareHolderControllerTest extends DbTestCase
{

    /**
     * @var $controller
     */
    protected $controller;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->controller = $this->prophesize(\ShareHolderController::class);
    }

    /** @test */
    function a_shareholder_can_be_registered()
    {
        // TODO - A lot more needs to be done here once I learm more about testing
        $this->controller->postRegister()->shouldBeCalled()->willReturn(Redirect::class);

        $response = $this->controller->reveal()->postRegister();

        $this->assertEquals(Redirect::class, $response);
    }
}
