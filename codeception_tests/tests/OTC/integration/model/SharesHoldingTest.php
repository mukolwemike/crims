<?php

namespace App\Tests\OTC\Integration\Model;

use Laracasts\TestDummy\DbTestCase;

class SharesHoldingTest extends DbTestCase
{

    protected $entity;

    protected $category;

    protected $holding;

    protected $client;

    protected $share_holder_type;

    protected $trail;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->category = createSharesCategory(['name' => 'General']);

        $fund_manager = createFundManager(['name' => 'Coop']);

        $this->entity = createSharesEntity(['fund_manager_id' => $fund_manager->id]);

        $this->trail = createSharesPriceTrail(['category_id' => $this->category->id, 'entity_id' => $this->entity->id, 'price' => 35.5]);

        $this->client = createClient(['client_code' => 'TEST_CODE_001']);

        $this->share_holder_type = createShareHolderType(['name' => 'Entity', 'slug' => 'entity']);

        $this->holding = createSharesHolding(['category_id' => $this->category->id, 'entity_id' => $this->entity->id, 'shareholder_type_id' => $this->share_holder_type->id,
            'client_id' => $this->client->id, 'purchase_price' => $this->trail->getPrice($this->entity->id, $this->trail->date)]);
    }

    /** @test */
    public function a_holding_has_a_category()
    {
        $this->assertEquals($this->category->name, $this->holding->category->name);
    }

    /** @test */
    public function a_holding_has_an_entity()
    {
        $this->assertEquals($this->entity->name, $this->holding->entity->name);
    }

    /** @test */
    public function a_holding_has_a_client()
    {
        $this->assertEquals($this->client->client_code, $this->holding->client->client_code);
    }

    /** @test */
    public function a_holding_has_a_shareholder_type()
    {
        $this->assertEquals($this->share_holder_type->name, $this->holding->shareholderType->name);
    }
}
