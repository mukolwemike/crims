<?php

namespace App\Tests\OTC\Integration\Model;

use Laracasts\TestDummy\DbTestCase;

class SharesBusinessConfirmationTest extends DbTestCase
{

    protected $bc;

    protected $holding;

    protected $user;

    protected $payload = ['number' => 152, 'purchase_price' => 20.0, 'date' => '2016-10-10', 'entity_id' => 1,'client_id'=> 1, 'category_id' => 1, 'payment_id' => 1];

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $category = createSharesCategory(['name' => 'General']);

        $fund_manager = createFundManager(['name' => 'Coop']);

        $entity = createSharesEntity(['fund_manager_id' => $fund_manager->id]);

        $client = createClient(['client_code' => 'TEST_CODE_001']);

        $share_holder_type = createShareHolderType(['name' => 'Entity', 'slug' => 'entity']);

        $this->holding = createSharesHolding(['category_id' => $category->id, 'entity_id' => $entity->id, 'shareholder_type_id' => $share_holder_type->id,
            'client_id' => $client->id, 'purchase_price' => 41.2]);

        $contact = createContact();

        $this->user = createUser(['username' => 'bucky_barnes', 'contact_id' => $contact->id]);

        $this->bc = createSharesBusinessConfirmation(['holding_id' => $this->holding->id, 'payload' => serialize($this->payload), 'sent_by' => $this->user->id]);
    }

    /** @test */
    public function a_bc_has_a_holding()
    {
        $this->assertEquals($this->holding->purchase_price, $this->bc->holding->purchase_price);
    }

    /** @test */
    public function a_bc_has_a_payload()
    {
        $this->assertEquals($this->payload, unserialize($this->bc->payload));
    }
}
