<?php

namespace App\Tests\OTC\Integration\Model;

use Laracasts\TestDummy\DbTestCase;

class SharesPaymentTest extends DbTestCase
{

    protected $payment;

    protected $client;

    protected $approval;

    protected $custodial_transaction;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->client = createClient(['client_code' => 'TestCode001']);

        $type = createCustodialTransactionType();

        $currency = createCurrency();

        $fund_manager = createFundManager();

        $custodial_account = createCustodialAccount(['currency_id' => $currency->id, 'fund_manager_id' => $fund_manager->id]);

        $owner = createTransactionOwner(['fund_manager_id' => $fund_manager->id, 'client_id' => $this->client->id]);

        $this->custodial_transaction = createCustodialTransaction(['type' => $type->id, 'approval_id' => 1, 'custodial_account_id' => $custodial_account->id, 'transaction_owners_id' => $owner->id]);

        $this->payment = createSharesPayment(['client_id' => $this->client->id, 'approval_id' => 1, 'custodial_transaction_id' => $this->custodial_transaction->id]);
    }

    /** @test */
    public function a_payment_has_a_client()
    {
        $this->assertEquals($this->client->client_code, $this->payment->client->client_code);
    }

    /** @test */
    public function a_payment_has_an_approval()
    {
        //TODO
    }

    /** @test */
    public function a_payment_has_a_custodial_transaction()
    {
        $this->assertEquals($this->custodial_transaction->amount, $this->payment->custodialTransaction->amount);
    }
}
