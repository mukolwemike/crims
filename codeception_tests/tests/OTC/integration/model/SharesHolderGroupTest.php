<?php

namespace App\Tests\OTC\Integration\Model;

use Laracasts\TestDummy\DbTestCase;

class SharesHolderGroupTest extends DbTestCase
{

    protected $group;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->group = createShareHolderGroup(['name' => 'Test']);
    }

    /** @test */
    public function a_group_has_members()
    {
        $client_one = createClient();
        $client_two = createClient();

        createShareHolderGroupMember(['shareholder_group_id' => $this->group->id, 'client_id' => $client_one->id]);
        createShareHolderGroupMember(['shareholder_group_id' => $this->group->id, 'client_id' => $client_two->id]);


        $this->assertEquals(2, $this->group->members()->count());
    }
}
