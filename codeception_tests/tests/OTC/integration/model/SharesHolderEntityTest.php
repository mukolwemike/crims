<?php

namespace App\Tests\OTC\Integration\Model;

use Laracasts\TestDummy\DbTestCase;

class SharesHolderEntityTest extends DbTestCase
{

    protected $entity;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();

        $this->entity = createShareHolderEntity(['name' => 'ESOP']);
    }

    /** @test */
    public function an_entity_has_members()
    {
        $client_one = createClient();
        $client_two = createClient();

        createShareHolderEntityMember(['shareholder_entity_id' => $this->entity->id, 'client_id' => $client_one->id]);
        createShareHolderEntityMember(['shareholder_entity_id' => $this->entity->id, 'client_id' => $client_two->id]);


        $this->assertEquals(2, $this->entity->members()->count());
    }
}
