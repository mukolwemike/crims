<?php

namespace App\Tests\OTC\Acceptance;

class ShareHolderTest extends \TestCase
{

    /** @test */
    public function user_can_view_shareholders()
    {
        $crawler = $this->client->request('GET', '/dashboard/otc/shareholders');

        $this->assertTrue($this->client->getResponse()->isOk());

        $this->assertTrue($crawler->filter('li.a:contains("All Shareholders")'));
    }
}
