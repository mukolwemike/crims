<?php

namespace App\Tests\OTC\Unit;

class SharesHolderEntityMemberTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $member
     */
    protected $member;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->member = new \ShareHolderEntityMember(['shareholder_entity_id'=>1, 'client_id'=>1]);
    }

    /** @test */
    function a_member_has_a_shareholder_entity_id()
    {
        $this->assertEquals(1, $this->member->shareholder_entity_id);
    }

    /** @test */
    function a_member_has_a_client_id()
    {
        $this->assertEquals(1, $this->member->client_id);
    }
}
