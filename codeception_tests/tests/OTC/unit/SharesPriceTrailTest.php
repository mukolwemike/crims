<?php

namespace App\Tests\OTC\Unit;

class SharesPriceTrailTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $trail
     */
    protected $trail;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->trail = new \SharesPriceTrail([
            'entity_id'             =>  1,
            'date'                  =>  '2016-10-10',
            'price'                 =>  20.0,
            'category_id'           =>  1,
        ]);
    }

    /** @test */
    function a_trail_has_a_date()
    {
        $this->assertEquals('2016-10-10', $this->trail->date);
    }

    /** @test */
    function a_trail_has_a_price()
    {
        $this->assertEquals(20.0, $this->trail->price);
    }

    /** @test */
    function a_trail_has_a_category_id()
    {
        $this->assertEquals(1, $this->trail->category_id);
    }

    /** @test */
    function a_trail_has_an_entity_id()
    {
        $this->assertEquals(1, $this->trail->entity_id);
    }
}
