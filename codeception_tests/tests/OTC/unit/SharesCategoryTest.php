<?php

namespace App\Tests\OTC\Unit;

class SharesCategoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $category
     */
    protected $category;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->category = new \SharesCategory(['name'=>'General']);
    }

    /** @test */
    function a_category_has_a_name()
    {
        $this->assertEquals('General', $this->category->name);
    }
}
