<?php

namespace App\Tests\OTC\Unit;

class SharesHolderGroupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $holder_group
     */
    protected $holder_group;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->holder_group = new \ShareHolderGroup(['name'=>'Test Group', 'type'=>'test_group']);
    }

    /** @test */
    function a_shareholder_group_has_a_name()
    {
        $this->assertEquals('Test Group', $this->holder_group->name);
    }

    /** @test */
    function a_shareholder_group_has_a_type()
    {
        $this->assertEquals('test_group', $this->holder_group->type);
    }
}
