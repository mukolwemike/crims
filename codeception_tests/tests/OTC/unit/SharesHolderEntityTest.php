<?php

namespace App\Tests\OTC\Unit;

class SharesHolderEntityTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $holder_entity
     */
    protected $holder_entity;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->holder_entity = new \ShareHolderEntity(['name'=>'ESOP', 'type'=>'corporate']);
    }

    /** @test */
    function a_shareholder_entity_has_a_name()
    {
        $this->assertEquals('ESOP', $this->holder_entity->name);
    }

    /** @test */
    function a_shareholder_entity_has_a_type()
    {
        $this->assertEquals('corporate', $this->holder_entity->type);
    }
}
