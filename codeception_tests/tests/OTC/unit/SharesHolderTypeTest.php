<?php

namespace App\Tests\OTC\Unit;

class SharesHolderTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $holder_type
     */
    protected $holder_type;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->holder_type = new \ShareHolderType(['name'=>'Entity', 'slug'=>'entity']);
    }

    /** @test */
    function a_shareholder_type_has_a_name()
    {
        $this->assertEquals('Entity', $this->holder_type->name);
    }
}
