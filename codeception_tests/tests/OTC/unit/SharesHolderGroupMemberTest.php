<?php

namespace App\Tests\OTC\Unit;

class SharesHolderGroupMemberTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $member
     */
    protected $member;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->member = new \ShareHolderGroupMember(['shareholder_group_id'=>1, 'client_id'=>1]);
    }

    /** @test */
    function a_member_has_a_shareholder_group_id()
    {
        $this->assertEquals(1, $this->member->shareholder_group_id);
    }

    /** @test */
    function a_member_has_a_client_id()
    {
        $this->assertEquals(1, $this->member->client_id);
    }
}
