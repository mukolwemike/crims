<?php

namespace App\Tests\OTC\Unit;

class SharesEntityTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $entity
     */
    protected $entity;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->entity = new \SharesEntity([
            'fund_manager_id'       =>  1,
            'default_for'           =>  null,
            'max_holding'           =>  200000,
            'name'                  =>  'Coop',
        ]);
    }

    /** @test */
    function an_entity_has_max_holding()
    {
        $this->assertEquals(200000, $this->entity->max_holding);
    }

    /** @test */
    function an_entity_has_a_name()
    {
        $this->assertEquals('Coop', $this->entity->name);
    }

    /** @test */
    function an_entity_has_a_fund_manager_id()
    {
        $this->assertEquals(1, $this->entity->fund_manager_id);
    }
}
