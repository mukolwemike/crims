<?php

namespace App\Tests\OTC\Unit;

class SharesBusinessConfirmationTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var $bc
     */
    protected $bc;

    /**
     * @var array $payload
     */
    protected $payload = ['number' => 152, 'purchase_price' => 20.0, 'date' => '2016-10-10', 'entity_id' => 1,'client_id'=> 1, 'category_id' => 1, 'payment_id' => 1];

    /**
     * Setup
     */
    public function setUp()
    {
        $this->bc = new \SharesBusinessConfirmation([
            'holding_id'                =>  1,
            'sent_by'                   =>  1,
            'payload'                   =>  serialize($this->payload)
        ]);
    }

    /** @test */
    function a_bc_has_a_holding_id()
    {
        $this->assertEquals(1, $this->bc->holding_id);
    }

    /** @test */
    function a_bc_has_a_sent_by()
    {
        $this->assertEquals(1, $this->bc->sent_by);
    }

    /** @test */
    function a_bc_has_a_payload()
    {
        $this->assertEquals($this->payload, unserialize($this->bc->payload));
    }
}
