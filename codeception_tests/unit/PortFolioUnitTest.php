<?php


class PortFolioUnitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->tester->haveRecord('custodial_accounts', ['id'=>1]);
        $this->tester->haveRecord('portfolio_investors', ['id'=>1]);
        $this->tester->haveRecord('fund_managers', ['id'=>1]);
        $this->tester->haveRecord('fund_types', ['id'=>1]);
        $this->tester->haveRecord('custodial_transactions', ['id'=>1, 'custodial_account_id'=>1]);
        $this->tester->haveRecord('users', ['id'=>1]);
        $this->tester->haveRecord('settings', ['key'=>'withholding_tax_rate', 'value'=>15]);
        $this->tester->haveRecord('custodial_transaction_types', ['id'=>1, 'name'=>'I']);
        $this->tester->haveRecord('custodial_transaction_types', ['id'=>2, 'name'=>'R']);
    }

    protected function _after()
    {
    }

    // tests
    public function testPortfolioInvestment()
    {
        $this->tester->wantTo('Add a new portfolio investment');

        $data = ['amount'=>2000, 'custodial_account_id'=>1, 'invested_date'=>'2015-06-20', 'maturity_date'=>'2015-07-20', 'fund_manager_id'=>1, 'portfolio_investor_id'=>1, 'fund_type_id'=>1, 'interest_rate'=>13, 'taxable'=>1];

        $inv = new \Cytonn\Portfolio\PortfolioInvestment();

        $inv->invest($data);

        $this->assertEquals($data['amount'], $inv->investment->amount);

        $this->tester->assertEquals($inv->investment->amount, -$inv->custodial->amount);
    }
    public function testPortfolioRollover()
    {
//        $this->tester->wantTo('Test rolling over a portfolio');
//
//        $this->tester->haveRecord('portfolio_investments', ['id'=>1, 'amount'=>20000, 'custodial_invest_transaction_id'=>1, 'invested_date'=>'2015-01-01', 'maturity_date'=>'2015-01-10', 'interest_rate'=>12]);
//
//        $inv = \PortfolioInvestment::find(1);
//        $roll = new \Cytonn\Portfolio\PortfolioRollover();
//
//        $data = ['amount'=>1000, 'invested_date'=>'2015-04-05', 'maturity_date'=>'2015-07-05', 'interest_rate'=>13, 'taxable'=>0];
//
//        $roll->rollOver($inv, User::find(1), $data);
//
//        $this->tester->assertGreaterThan(0, $roll->custodial->amount);

        $taxRate = \Setting::where('key', 'withholding_tax_rate')->latest()->first()->value;

        //$this->tester->assertEquals(-$roll->custodial->amount,  $inv->amount + (1-$taxRate)*$inv->amount*$inv->interest_rate*91/(100*365) - $roll->investment->amount);
    }
    public function testPortfolioMatureWithdrawal()
    {
        $this->tester->wantTo('Test withdrawal of a mature portfolio');

        $this->tester->haveRecord('portfolio_investments', ['id'=>1, 'invested_date'=>'2015-03-18', 'maturity_date'=>'2015-07-01', 'amount'=>2000, 'interest_rate'=>14, 'custodial_account_id'=>1]);

        $inv = \PortfolioInvestment::find(1);
        $inv1 = $inv;
        $data = [];

        $with = new \Cytonn\Portfolio\PortfolioWithdraw();
        $with->matureWithdraw($inv, User::find(1), $data);

        $this->tester->assertLessThan($inv->amount, -$with->custodial->amount);
        $this->tester->assertGreaterThan(0, $with->custodial->amount);

        $taxRate = \Setting::where('key', 'withholding_tax_rate')->latest()->first()->value;
        //$this->tester->assertEquals(-$inv->amount-(1-$taxRate)*$inv->amount*$inv->interest_rate*105/(100*365), $with->custodial->amount);
        $this->assertTrue($inv->withdrawn);

        $this->tester->wantTo('Test portfolio premature withdrawal');
        $p_with = new \Cytonn\Portfolio\PortfolioWithdraw();
        $data1 = ['withdrawal_date'=>'2015-04-13', 'end_date'=>'2015-03-19'];
        $p_with->prematureWithdrawal($inv1, User::find(1), $data1);

        $this->tester->assertGreaterThan(0, $p_with->custodial->amount);
        $this->tester->assertLessThan($inv1->amount, -$p_with->custodial->amount);
        //$this->tester->assertEquals($p_with->custodial->amount, -$inv1->amount-(1-$taxRate)*$inv1->amount*$inv1->interest_rate*26/(100*365));
    }
}
