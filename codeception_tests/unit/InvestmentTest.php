<?php


use \Cytonn\Investment\ClientApplications;

class InvestmentTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->tester->haveRecord('contact_entity_types', ['id'=>1, 'name'=>'individual']);
        $this->tester->haveRecord('client_type', ['id'=>1, 'name'=>'']);
        $this->tester->haveRecord('custodial_accounts', ['id'=>1]);
        $this->tester->haveRecord('fund_managers', ['id'=>1]);
        $this->tester->haveRecord('products', ['id'=>1, 'custodial_account_id'=>1, 'fund_manager_id'=>1]);
        $this->tester->haveRecord('contacts', ['id'=>1, 'entity_type_id'=>1]);
        $this->tester->haveRecord('clients', ['id'=>1, 'contact_id'=>1, 'client_type_id'=>1]);
        $this->tester->haveRecord('client_investment_application', ['id'=>1, 'amount'=> 2000, 'tenor'=>3, 'client_id'=>1, 'product_id'=>1]); //application
        $this->tester->haveRecord('transaction_owners', ['id'=>1]); //transaction owners
        $this->tester->haveRecord('staff', ['id'=>2]);
        $this->tester->haveRecord('settings', ['key'=>'withholding_tax_rate', 'value'=>15]);
        $this->tester->haveRecord('users', ['id'=>1]);
        $this->tester->haveRecord('users', ['id'=>2, 'username'=>'someuser', 'email'=>'anemail@example.com']);
        $this->tester->haveRecord('client_investment_application', ['id'=>2]);
        $this->tester->haveRecord('client_investment_types', ['id'=>1, 'name'=>'investment']);
        $this->tester->haveRecord('client_investment_types', ['id'=>2, 'name'=>'topup']);
        $this->tester->haveRecord('client_investment_types', ['id'=>3, 'name'=>'rollover']);
        $this->tester->haveRecord('commission_recepients', ['id'=>1, 'name'=>'dmc']);

        $this->tester->haveRecord('custodial_transaction_types', ['id'=>1, 'name'=>'FI']);
        $this->tester->haveRecord('custodial_transaction_types', ['id'=>2, 'name'=>'FO']);
        $this->tester->haveRecord('client_transaction_approval', ['id'=>1]);
    }

    protected function _after()
    {
    }

    // tests
    public function testInvestmentApplication()
    {
        $this->tester->wantTo('Test whether I can add an individual application');

        $individual = [
            'amount'=>20000
        ];

        $app = new ClientApplications();
        $application = $app->individualApplication($individual);

        $this->tester->assertGreaterThanOrEqual(0, $application->id);
        $this->tester->assertEquals($application->amount, $individual['amount']);

        $this->tester->wantTo('Test whether I can add a corporate application');
        $corporate = [
            'amount'=>20000
        ];
        $corappl = $app->corporateApplication($corporate);

        $this->tester->assertGreaterThanOrEqual(0, $corappl->id);
        $this->tester->assertEquals($corporate['amount'], $corappl->amount);

        $this->tester->wantTo('Test Compliance Approval');
        $this->tester->haveRecord('staff', ['id'=>1]);

        $app->saveCompliance(User::find(1), $application, true);


        $this->tester->assertEquals($application->compliance_approved, true);

        $app->saveCompliance(User::find(1), $application, false);

        $this->tester->assertEquals($application->compliance_approved, false);

        $app->saveCompliance(User::find(1), $application, null);

        $this->tester->assertEquals($application->compliance_approved, null);
    }

    public function testAddnewInvestment()
    {
        $data = ['invested_date'=>date('y-m-d', strtotime('24-09-2015')), 'maturity_date'=>date('y-m-d', strtotime('24-12-2015')) , 'product_id'=>1, 'interest_rate'=>14, 'commission_rate'=>0.5, 'commission_recepient'=>1, 'approval_id'=>1, 'interest_payment_interval'=>null, 'interest_payment_date'=>null, 'client_code'=>100]; //supplementary data


        $inv = new Cytonn\Investment\ClientInvestments();
        $app = ClientInvestmentApplication::find(1);
        $inv->clientInvests($app, $data);
        $this->tester->assertEquals($inv->client->amount, $app->amount);
        $this->tester->seeRecord('client_investments', ['amount'=>'2000.00']);
        $this->tester->seeRecord('custodial_transactions', ['amount'=>'2000.00']);
        $this->assertEquals(3, $inv->client->invested_date->diffInMonths($inv->client->maturity_date));
    }

    public function testClientWithdrawal()
    {
//        $this->tester->wantTo('Test Client Mature Withdrawal');
//        $this->tester->haveRecord('client_investments', ['id'=>1,'client_id'=>1, 'product_id'=>1, 'amount'=>2000, 'interest_rate'=>13, 'invested_date'=>'2015-05-13', 'maturity_date'=>'2015-08-19']);
//        $this->tester->haveRecord('client_investments', ['id'=>2,'client_id'=>1, 'product_id'=>1, 'amount'=>2000, 'interest_rate'=>13, 'invested_date'=>'2015-05-13', 'maturity_date'=>'2015-08-19']);
//
//        $inv = ClientInvestment::find(1);
//        $inv2 = ClientInvestment::find(2);
//
//        $withdraw = new \Cytonn\Investment\ClientWithdrawals();
//        $staff = User::find(2);
//        $data = ['withdrawal_time'=>'now', 'approval_id'=>1];
//
//        $withdraw->clientMatureWithdrawal($inv, $staff, $data);
//        $this->tester->assertGreaterThan(0, $withdraw->client->amount);
//        $this->tester->assertLessThan(0, $withdraw->custodial->amount);
//        $this->tester->assertEquals($withdraw->custodial->amount, -(2000+0.85*(0.13*98*2000/365)));
//        $this->tester->assertEquals(true, $withdraw->client->withdrawn);
//
//        $this->tester->wantTo('Test client premature withdrawal');
//
//        $p_withdraw = new \Cytonn\Investment\ClientWithdrawals();
//        $data = ['end_date'=> '2015-05-14', 'approval_id'=>1];
//
//        $p_withdraw->prematureClientWithdrawal($inv2, $staff, $data);
//        $this->tester->assertGreaterThan($withdraw->custodial->amount, $p_withdraw->custodial->amount);
//        $this->tester->assertEquals($p_withdraw->custodial->amount, -(2000+(0.85*(0.13*1*2000/365))));
//        $this->tester->assertLessThan($p_withdraw->custodial->amount, $withdraw->custodial->amount);
//        $this->tester->assertEquals(true, $p_withdraw->client->withdrawn);
//
//        $this->tester->wantTo('Test client premature partial withdrawal');
        //TODO write test
    }

    public function testClientTopups()
    {
        $this->tester->wantTo('Test a client Top up');

        $this->tester->haveRecord('client_investments', ['id'=>1, 'client_id'=>1, 'application_id'=>2, 'product_id'=>1, 'amount'=>2000, 'interest_rate'=>13, 'invested_date'=>'2015-05-13', 'maturity_date'=>'2015-08-19']);

        $original = ClientInvestment::find(1);

        $appdata = ['amount'=>1000, 'agreed_rate'=>16, 'parent_application_id'=>$original->application_id];
        $data = ['invested_date'=>'2015-02-19', 'maturity_date'=>'2015-04-05', 'interest_rate'=>12,'product_id'=>1, 'approval_id'=>1, 'interest_payment_interval'=>null, 'interest_payment_date'=>null, 'commission_rate'=>0, 'commission_recepient'=>null];

        $application = $original->application;

        $application->fill($appdata);
        $application->save();


        $top = new \Cytonn\Investment\ClientTopup();
        $top->clientTopUp($original, $application, $data);

        $this->tester->assertEquals('1000.00', $top->client->amount);
        $this->tester->assertEquals($top->client->amount, $top->custodial->amount);
        $this->tester->canSeeRecord('client_investments', ['amount'=>'1000.00']);
    }

    public function testClientRollover()
    {
        $this->tester->wantTo('Test a client rolling over investment');


        $this->tester->haveRecord('client_investments', ['id'=>1, 'client_id'=>1, 'application_id'=>2, 'product_id'=>1, 'amount'=>2000, 'interest_rate'=>13, 'invested_date'=>'2015-05-13', 'maturity_date'=>'2015-08-19']);

        $roll = new \Cytonn\Investment\ClientRollover();
        $oldInvestment = ClientInvestment::find(1);

        $appdata = ['amount'=>1000, 'agreed_rate'=>16, 'application_type_id'=>2, 'parent_application_id'=>$oldInvestment->application_id];

        $data = ['invested_date'=>'2015-08-22', 'maturity_date'=>'2015-10-20', 'approval_id'=>1, 'interest_payment_interval'=>null, 'interest_payment_date'=>null, 'commission_rate'=>0, 'commission_recepient'=>null, 'on_call'=>false];

        //TODO test interest payment generation, commission
        $application = $oldInvestment->application;
        $application->fill($appdata);
        $application->save();

        $user = \User::find(2);


        $roll->clientRollOver($oldInvestment, $application, $user, $data);

        $this->assertTrue($oldInvestment->withdrawn);
        $this->tester->canSeeRecord('client_investments', ['interest_rate'=>16]);
    }

    public function testCombinedRollover()
    {
        //TODO complete test
        $this->tester->wantTo('Test combining investments and rollong over');

        $this->tester->haveRecord('client_investments', ['id'=>1,'client_id'=>1, 'application_id'=>2, 'product_id'=>1, 'amount'=>2000, 'interest_rate'=>13, 'invested_date'=>'2015-05-13', 'maturity_date'=>'2015-08-19']);
        $this->tester->haveRecord('client_investments', ['id'=>2, 'client_id'=>1, 'application_id'=>2, 'product_id'=>1, 'amount'=>3000, 'interest_rate'=>13, 'invested_date'=>'2015-05-13', 'maturity_date'=>'2015-08-19']);
    }
}
