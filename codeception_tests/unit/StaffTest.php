<?php

/**
 * Date: 8/11/15
 * Time: 3:08 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class StaffTest extends \Codeception\TestCase\Test
{

    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * Before running tests
     */
    protected function _before()
    {
        $this->tester->haveRecord('contact_entity_types', ['id'=>1, 'name'=>'individual']);
        $this->tester->haveRecord('users', ['id'=>1, 'email'=>'mwaruwac@gmail.com']);
        $this->tester->haveRecord('contacts', ['id'=>100, 'entity_type_id'=>1]);
    }

    /**
     *
     */
    protected function _after()
    {
    }

    /**
     *
     */
    public function testAddStaff()
    {
        $this->tester->wantTo('Test if I can add a staff member');
        $staffdata = [
            'user_id'=>1
        ];

        $repo = new \Cytonn\Staff\StaffRepository();
        $staff = $repo->save($staffdata);

        $this->assertNotNull($staff->id);

        $this->tester->wantTo('Test Staff Relationships');
        $this->tester->assertEquals($staff->user->email, 'mwaruwac@gmail.com');
    }
}
