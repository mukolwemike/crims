<?php

/**
 * Date: 8/11/15
 * Time: 2:55 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
use Cytonn\Clients\ClientRepository;

class ClientTest extends \Codeception\TestCase\Test
{
    protected $tester;

    public function _before()
    {
        $this->tester->haveRecord('contact_entity_types', ['id'=>1]);
        $this->tester->haveRecord('contacts', ['id'=>1, 'firstname'=>'mwaruwa', 'entity_type_id'=>1]);
        $this->tester->haveRecord('users', ['id'=>1, 'email'=>'mwaruwac@gmail.com']);
        $this->tester->haveRecord('client_type', ['id'=>1]);
    }
    public function testAddClient()
    {

        $this->tester->wantTo('Test if I can add a client');

        $clientdata = [
            'residence' => 'here',
            'contact_id'=>1,
            'user_id'=>1,
            'client_type_id'=>1
        ];

        $repo = new ClientRepository();
        $client = $repo->save($clientdata);

        $this->tester->assertEquals($client->residence, $clientdata['residence']);
        $this->tester->assertNotNull($client->id);

        $this->tester->wantTo('Test client (models) relationships');
        $this->tester->assertEquals($client->contact->firstname, 'mwaruwa');
        $this->tester->assertEquals($client->user->email, 'mwaruwac@gmail.com');
    }
}
