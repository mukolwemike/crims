<?php

/**
 * Date: 8/11/15
 * Time: 3:20 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class UserTest extends \Codeception\TestCase\Test
{
    protected $tester;

    public function testAddUser()
    {
        $userdata = [
            'email'=>'mchaka@cytonn.com'
        ];

        $repo = new \Cytonn\Users\UserRepository();

        $user = $repo->save($userdata);

        $this->tester->assertNotNull($user->id);
    }
}
