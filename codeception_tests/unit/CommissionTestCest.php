<?php


use Cytonn\Investment\Commission\RecipientCalculator;

class CommissionTestCest
{
    protected $calculator;
    public function _before(UnitTester $I)
    {
        $investment = ClientInvestment::findOrFail($I->haveClientInvestment());

        $this->calculator = new RecipientCalculator($investment->commission->recipient, $investment->product->currency, \Carbon\Carbon::today());

        (new \Cytonn\Investment\Commission\Generator())->createSchedules($investment);
    }

    public function _after(UnitTester $I)
    {
    }

    // tests
    public function testClassIsCreated(UnitTester $I)
    {
        $I->assertInstanceOf(RecipientCalculator::class, $this->calculator);
    }

    public function testCompliant()
    {
    }

    public function testNonCompliant()
    {
    }

    public function testBroughtForward()
    {
    }

    public function testClawBack()
    {
    }

    public function testExtension()
    {
    }
}
