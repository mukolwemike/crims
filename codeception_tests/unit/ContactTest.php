<?php

use Cytonn\Contacts\ContactsRepository;

class ContactTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->tester->haveRecord('contact_entity_types', ['id'=>1]);
        $this->tester->haveRecord('contact_categories', ['id'=>1, 'name'=>'some cat']);
        $this->tester->haveRecord('contact_types', ['id'=>1, 'name'=>'some type']);
        $this->tester->haveRecord('countries', ['id'=>1, 'iso_abbr'=>'ke', 'name'=>'Kenya']);
        $this->tester->haveRecord('staff', ['id'=>1]);
    }

    protected function _after()
    {
    }

    public function testContact()
    {
        $this->tester->wantTo('Test if I can save a contact');
        $this->tester->haveRecord('client_type', ['id'=>2, 'name'=>'corporate']);

        $data = [
            'entity_type_id'=>1,
            'firstname' => 'Mwaruwa',
            'lastname' => 'Chaka',
            'middlename' => 'M',
            'email'=>'mwaruwac@gmail.com',
            'phone'=>'0738210720',
            'contact_category_id'=>1,'contact_type_id'=>1,
            'country_id'=>1,
            'contact_originator'=>1, 'relationship_contact'=>1
        ];

        $repo = new ContactsRepository();
        $c = $repo->save($data);

        $this->tester->assertEquals($c->email, 'mwaruwac@gmail.com');
        $this->tester->assertEquals($c->firstname, 'Mwaruwa');
        $this->tester->assertNotNull($c->id);

        $this->tester->wantTo('Test contact relationships');
        $this->tester->assertEquals($c->originator->id, 1);
        $this->tester->assertEquals($c->relationshipManager->id, 1);
        $this->tester->haveRecord('clients', ['id'=>1, 'contact_id'=>$c->id, 'residence'=>'ke', 'client_type_id'=>2]);
        $this->tester->assertNotNull($c->client());
        $this->tester->assertEquals($c->client->residence, 'ke');
    }
}
