<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesSalesOrder;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/*
 * Share Entity Factory
 */
$factory->define(\App\Cytonn\Models\SharesEntity::class, function (Faker\Generator $faker) {
    return [
        'fund_manager_id'  =>  1,
        'default_for' => 1,
        'max_holding' => $faker->numberBetween(20, 100),
        'name' => $faker->sentence(4),
        'account_id' => 1,
        'currency_id' => 1,
        'code' => $faker->word
    ];
});

/*
 * Share Holder Factory
 */
$factory->define(ShareHolder::class, function (Faker\Generator $faker) {
    return [
        'client_id'  =>  1,
        'number' => $faker->unique()->word,
        'joint' => null,
        'entity_id' => 1
    ];
});

/*
 * Share Category Factory
 */
$factory->define(\App\Cytonn\Models\SharesCategory::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  'General',
    ];
});

/*
 * Share Purchase Factory
 */
$factory->define(\App\Cytonn\Models\SharePurchases::class, function (Faker\Generator $faker) {
    return [
        'number'  =>  $faker->numberBetween(100, 10000),
        'purchase_price' => $faker->randomFloat(2, 10, 500),
        'payment_id' => null,
        'date' => $faker->date('Y-m-d', 'now'),
        'entity_id' => 1,
        'client_id' => 1,
        'category_id' => 1,
        'approval_id' => null,
        'share_holder_id' => 1,
        'shares_purchase_order_id' => factory(\App\Cytonn\Models\SharesPurchaseOrder::class)->create()->id,
        'shares_sales_order_id' => null,
        'seller_id' => null,
        'client_payment_id' => $faker->numberBetween(10, 1000),
    ];
});

/*
 * Share Sales Factory
 */
$factory->define(\App\Cytonn\Models\ShareSale::class, function (Faker\Generator $faker) {
    return [
        'number'  =>  $faker->numberBetween(100, 10000),
        'sale_price' => $faker->randomFloat(2, 10, 500),
        'payment_id' => $faker->numberBetween(10, 200),
        'date' => $faker->date(),
        'redeem_interest' => $faker->randomFloat(2, 10, 5000),
        'approval_id' => null,
        'shares_purchase_order_id' => 1,
        'shares_sales_order_id' => 1,
        'entity_id' => 1,
        'category_id' => 1,
        'buyer_id' => 1,
        'share_holder_id' => 1,
        'share_purchase_id' => 1,
        'type' => 1
    ];
});

/*
 * Share Sales Order Factory
 */
$factory->define(\App\Cytonn\Models\SharesSalesOrder::class, function (Faker\Generator $faker) {
    return [
        'number' => $faker->numberBetween(10000, 1000000),
        'price' => 60,
        'seller_id' => factory(ShareHolder::class)->create()->id,
    ];
});

/*
 * Share Purchase Order Factory
 */
$factory->define(\App\Cytonn\Models\SharesPurchaseOrder::class, function (Faker\Generator $faker) {
    return [
        'number' => $faker->numberBetween(100, 10000),
        'price' => 60,
        'request_date' => $faker->dateTime(),
        'buyer_id' => factory(ShareHolder::class)->create()->id,
        'matched' => null,
        'commission_recipient_id' => factory(CommissionRecepient::class)->create()->id,
        'commission_rate' => 2,
        'approval_id' => factory(ClientTransactionApproval::class)->create()->id,
        'good_till_filled_cancelled' => null,
        'expiry_date' => null,
        'cancelled' => null,
        'payment_date' => null,
        'commission_start_date' => null
    ];
});
