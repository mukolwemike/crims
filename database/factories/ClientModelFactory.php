<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Cytonn\Models\Contact;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/*
 * User Factory
 */
$factory->define(\App\Cytonn\Models\User::class, function (Faker\Generator $faker) {
    return [
        'email'  =>  $faker->email,
        'username' => $faker->unique()->word,
        'password' => 'password',
        'login_attempt' => 0,
        'active' => 1,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'jobtitle' => $faker->title,
    ];
});

/*
 * Contact Type Factory
 */
$factory->define(\App\Cytonn\Models\ContactType::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});

/*
 * Contact Category Factory
 */
$factory->define(\App\Cytonn\Models\ContactCategory::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});

/*
 * Contact Factory
 */
$factory->define(\App\Cytonn\Models\Contact::class, function (Faker\Generator $faker) {
    return [
        'id' => null,
        'entity_type_id' => 1,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'middlename' => $faker->firstName,
        'title_id' => 1,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'country_id' => 1,
        'contact_category_id' => 1,
        'contact_type_id' => 1,
        'gender_id' => 1,
        'corporate_registered_name' => $faker->word,
        'corporate_trade_name' =>  $faker->word,
        'registered_address' => $faker->word,
        'registration_number' => $faker->word,
        'preferredname' => $faker->name()
    ];
});

/*
 * Client Type Factory
 */
$factory->define(\App\Cytonn\Models\ClientType::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});

/*
 * Contact Method Factory
 */
$factory->define(\App\Cytonn\Models\ContactMethod::class, function (Faker\Generator $faker) {
    return [
        'description'  =>  $faker->word,
    ];
});

/*
 * Corporate Investor Type Factory
 */
$factory->define(\App\Cytonn\Models\CorporateInvestorType::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});

/*
 * Fundmanager Method Factory
 */
$factory->define(\App\Cytonn\Models\FundManager::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
        'logo' => $faker->word,
        'fullname' => $faker->sentence(3),
        'report' => 1
    ];
});

/*
 * Employment Method Factory
 */
$factory->define(\App\Cytonn\Models\Employment::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});

/*
 * Client Factory
 */
$factory->define(\App\Cytonn\Models\Client::class, function (Faker\Generator $faker) {
    $optional = $faker->optional(0.9);

    return [
        'contact_id' => 'factory:' . Contact::class,
        'uuid' => $faker->uuid,
        'client_type_id' => 1,
        'joint' => 0,
        'signing_mandate' => 0,
        'client_code' => $optional ? $optional->numberBetween(1000, 100000) : null,
        'dob' => $faker->date(),
        'residence' => $faker->address,
        'taxable' => true,
        'id_or_passport' => $faker->numberBetween(1000000, 100000000),
        'postal_code' => $faker->postcode,
        'postal_address' => $faker->numberBetween(10000, 100000),
        'pin_no' => $faker->numberBetween(8388389389, 3093209329023),
        'phone' => $faker->phoneNumber,
        'town' => $faker->city,
        'method_of_contact_id' => 1,
        'country_id' => 1,
        'employment_id' => 1,
        'present_occupation' => $faker->word,
        'investor_bank' => $faker->word,
        'investor_bank_branch' => $faker->word,
        'investor_account_name' => $faker->name,
        'investor_account_number' => $faker->bankAccountNumber,
        'complete' => true,
        'fund_manager_id' => 1
    ];
});

/*
 * Risky Client Factory
 *
 */

$factory->define(\App\Cytonn\Models\Client\RiskyClient::class, function (Faker\Generator $faker) {
    return [
       'type' => 1,
       'risk_id' => null,
       'country_id' => \App\Cytonn\Models\Country::first()->id,
       'firstname' => $this->faker->firstname,
       'lastname' => $this->faker->lastname,
       'email' => $this->faker->email,
       'affiliations' => $this->faker->sentence,
       'risk_source' => $this->faker->sentence,
       'bank_reference_no' => str_random(10),
       'date_flagged' => \Carbon\Carbon::today(),
       'flagged_by' => factory(\App\Cytonn\Models\User::class)->create()->id,
       'risky_status_id' => factory(\App\Cytonn\Models\Client\RiskyStatus::class)->create()->id,
       'reason' => $this->faker->sentence,
    ];
});

/*
 * Risky Status Factory
 */
$factory->define(\App\Cytonn\Models\Client\RiskyStatus::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});

$factory->define(\App\Cytonn\Models\ClientUser::class, function (Faker\Generator $faker) {
    return [
        'username'  =>  $faker->unique()->word,
        'email'  =>  $faker->email,
        'password' => 'password',
        'pin' => '1234',
        'login_attempts' => 0,
        'active' => 1,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
    ];
});


/*
 * Risky Client Factory
 *
 */

$factory->define(\App\Cytonn\Models\Client\RiskyClient::class, function (Faker\Generator $faker) {
    return [
       'type' => 1,
       'risk_id' => null,
       'country_id' => \App\Cytonn\Models\Country::first()->id,
       'firstname' => $this->faker->firstname,
       'lastname' => $this->faker->lastname,
       'email' => $this->faker->email,
       'affiliations' => $this->faker->sentence,
       'risk_source' => $this->faker->sentence,
       'bank_reference_no' => str_random(10),
       'date_flagged' => \Carbon\Carbon::today(),
       'flagged_by' => factory(\App\Cytonn\Models\User::class)->create()->id,
       'risky_status_id' => factory(\App\Cytonn\Models\Client\RiskyStatus::class)->create()->id,
       'reason' => $this->faker->sentence,
    ];
});

/*
 * Risky Status Factory
 */
$factory->define(\App\Cytonn\Models\Client\RiskyStatus::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
    ];
});
