<?php

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\ContactEntity;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(UnitFund::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'initial_unit_price' => $faker->numberBetween(100, 1000),
        'minimum_investment_amount' => $faker->numberBetween(10000, 100000),
        'fund_objectives' => $faker->paragraph,
        'currency_id' => 1,
        'fund_manager_id' => $faker->numberBetween(1, 100),
        'short_name' => strtoupper($faker->name),
        'benefits' => $faker->paragraph,
        'minimum_investment_horizon' => $faker->numberBetween(12, 18),
        'custodial_account_id' => null,
        'fund_category_id' => null,
        'contact_person' => $faker->name,
        'email' => $faker->email,
        'active' => 1,
        'allow_premature_withdrawal' => 1,
        'code' => $faker->numberBetween(1000, 9999)
    ];
});

$factory->define(ContactEntity::class, function (Faker\Generator $faker) {
    return [
        'name' => 'individual'
    ];
});


$factory->define(Contact::class, function (Faker\Generator $faker) {
    return [
        'entity_type_id' => (new App\Cytonn\Models\ContactEntity)->create()->id, // (new App\Cytonn\Models\ContactEntity)->where('name', 'individual')->first()->id,
        'title_id' => factory(Title::class)->create()->id,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'middlename' => $faker->lastName,
        'email' => $faker->email
    ];
});

$factory->define(\App\Cytonn\Models\Client::class, function (Faker\Generator $faker) {

    $optional = $faker->optional(0.9)->unique();

    return [
        'contact_id' => factory(Contact::class)->create()->id,
        'client_type_id' => (new App\Cytonn\Models\ClientType)->create()->id,
        'client_code' => $optional ? $optional->numberBetween(1000, 100000) : null,
        'dob' => $faker->date(),
        'taxable' => true,
        'id_or_passport' => $faker->numberBetween(1000000, 100000000),
        'postal_code' => $faker->postcode,
        'postal_address' => $faker->numberBetween(10000, 100000),
        'pin_no' => $faker->numberBetween(8388389389, 3093209329023),
        'phone' => $faker->phoneNumber,
        'town' => $faker->city,
        'country_id' => 114, // Country::all()->random()->id
        'employment_id' => 1, // Employment::all()->random()->id
        'investor_bank' => $faker->company,
        'investor_bank_branch' => $faker->city,
        'investor_account_name' => $faker->name,
        'investor_account_number' => $faker->bankAccountNumber,
        'complete' => true,
        'fund_manager_id' => 1, // FundManager::all()->random()->id
    ];
});

$factory->define(UnitFundPurchase::class, function (Faker\Generator $faker) {
    return [
        'client_id' => factory(\App\Cytonn\Models\Client::class)->create()->id,
        'unit_fund_id' => factory(UnitFund::class)->create()->id,
        'client_payment_id' => factory(\App\Cytonn\Models\ClientPayment::class)->create()->id,
        'number' => $faker->numberBetween(1000, 1000000),
        'date' => new \Jenssegers\Date\Date(),
        'description' => 'Purchase',
        'price' => 1
    ];
});

$factory->define(ClientInvestmentApplication::class, function (Faker\Generator $faker) {
    $client = factory(Client::class)->create();

    return [
        'unit_fund_id' => factory(UnitFund::class)->create()->id,
        "type_id" => (new App\Cytonn\Models\ClientType)->create()->id,
        "joint" => false,
        "application_type_id" => factory(ClientInvestmentType::class)->create()->id,
        "client_id" => $client->id,
        "amount" => $faker->numberBetween(100000, 10000000),
        "tenor" => $faker->numberBetween(3, 12),
        "lastname" => $faker->firstName,
        "middlename" => $faker->lastName,
        "firstname" => $faker->firstName,
        "id_or_passport" => $client->id_or_passport,
        "telephone_home" => $faker->phoneNumber,
        "funds_source_id" => 1,
        "terms_accepted" => true,
    ];
});


$factory->define(ClientInvestment::class, function (Faker\Generator $faker) {
    return [
        'client_id' => 'factory:' . Client::class,
        'application_id' => 'factory' . ClientInvestmentApplication::class,
    ];
});
