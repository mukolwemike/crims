<?php
/**
 * Date: 04/05/2018
 * Time: 18:20
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\Cytonn\Models\Client\Approvals\ClientTransactionApprovalType::class, function ($faker) {
    return [
        'name' => 'some name here',
        'slug' => 'some_slug_here'
    ];
});

$factory->define(\Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage::class, function ($faker) {
    return [
        'name' => 'first stage',
        'slug' => 'first',
        'applies_to_all' => true
    ];
});

$factory->define(\App\Cytonn\Models\ClientTransactionApproval::class, function ($faker) {
    return [
        'client_id' => 1,
        'transaction_type' => 'some_slug_here',
        'payload' => null,
        'sent_by' => 1,
        'approved' => null,
        'approved_by' => null,
        'approved_on' => null,
        'scheduled' => 0,
        'schedule_id' => null,
        'fm_id' => null,
        'action_date' => null,
        'run_date' => null,
        'investment_id' => null,
        'reason' => null,
        'awaiting_stage_id' => 1,
        'client_user_id' => null
    ];
});
