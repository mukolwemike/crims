<?php
/**
 * Date: 04/05/2018
 * Time: 18:20
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Cytonn\Models\UnitHolding::class, function (Faker\Generator $faker) {
    return [
        'unit_id' => 1,
        'client_id' => 1,
        'approval_id' => null,
        'active' => 1,
        'negotiated_price' => null,
        'forfeit_reason' => null,
        'tranche_id' => 1,
        'payment_plan_id' => 1,
        'forfeit_date' => null,
        'discount' => 0,
        'penalty_excempt' => 0,
        'forfeiture_letter_sent_on' => null,
        'purchasers_advocate' => null,
        'reservation_date' => $faker->dateTimeThisMonth()->format('Y-m-d')
    ];
});

$factory->define(\App\Cytonn\Models\RealestateUnit::class, function (Faker\Generator $faker) {
    return [
        'project_id' => 1,
        'number' => $faker->slug,
        'size_id' => 1,
        'type_id' => 1,
        'group_id' => null,
        'project_floor_id' => null,
        'real_estate_type_id' => null
    ];
});

$factory->define(\App\Cytonn\Models\Project::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'long_name' => $faker->sentence(3),
        'project_manager' => null,
        'location' => $faker->sentence,
        'vendor' => $faker->sentence(3),
        'code' => $faker->word,
        'development_description' => $faker->paragraph(3),
        'type_id' => 1,
        'reservation_fee' => $faker->numberBetween(25000, 100000),
        'fund_manager_id' => 1,
        'commission_calculator' => 'payment_plan_based',
        'bank_name' => $faker->word,
        'bank_branch_name' => $faker->word,
        'bank_account_name' => $faker->name,
        'bank_account_number' => $faker->bankAccountNumber,
        'bank_swift_code' => $faker->swiftBicNumber,
        'bank_clearing_code' => $faker->countryCode,
        'vendor_address' => $faker->address,
        'completion_date' => $faker->dateTimeBetween('+2 years', '+10 years'),
        'county_government' => $faker->sentence,
        'levies_interest' => 1,
        'custodial_account_id' => 1,
        'currency_id' => 1,
        'penalty_start_date' => null,
        'property_additional_information' => $faker->paragraph,
        'project_manager_id' => 1,
        'is_loo_assignment' => 0,
        'loo_indicate_land_size' => null,
        'loo_template' => null,
        'practical_completion' => null
    ];
});

$factory->define(\App\Cytonn\Models\ProjectType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
    ];
});

$factory->define(\App\Cytonn\Models\RealestateUnitSize::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'realestate_land_size_id' => null,
        'on_land_size' => null
    ];
});

$factory->define(\App\Cytonn\Models\RealestateUnitType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateUnitTranche::class, function (Faker\Generator $faker) {
    return [
        'project_id' => 1,
        'payment_plan_id' => null,
        'name' => 'Tranche One'
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateUnitTrancheSizing::class, function (Faker\Generator $faker) {
    return [
        'tranche_id' => 1,
        'number' => 20,
        'size_id' => '1',
        'project_floor_id' => null,
        'real_estate_type_id' => null,
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateUnitTranchePricing::class, function (Faker\Generator $faker) {
    return [
        'payment_plan_id' => 1,
        'sizing_id' => 1,
        'price' => $faker->numberBetween(7800000, 100000000),
        'value' => $faker->numberBetween(7800000, 100000000)
    ];
});

$factory->define(\App\Cytonn\Models\RealEstatePaymentPlan::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
    ];
});

$factory->define(\App\Cytonn\Models\RealEstatePaymentType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
    ];
});

$factory->define(\App\Cytonn\Models\ReservationForm::class, function (Faker\Generator $faker) {
    return [
        'holding_id' => 1,
        'reservation_date' => null,
        'client_type_id' => 1,
        'title_id' => 1,
        'client_code' => null,
        'client_id' => null,
        'firstname' => $faker->firstName,
        'middlename' => $faker->lastName,
        'lastname' => $faker->lastName,
        'corporate_registered_name' => null,
        'corporate_trade_name' => null,
        'registered_address' => $faker->address,
        'registration_number' => null,
        'country_id' => 2,
        'nationality_id' => 2
    ];
});

$factory->define(\App\Cytonn\Models\RealEstatePaymentSchedule::class, function (Faker\Generator $faker) {
    return [
        'unit_holding_id' => 1,
        'amount' => $faker->numberBetween(25000, 1000000),
        'description' => "Payment",
        'fully_paid' => null,
        'date' => $faker->dateTimeThisYear()->format('Y-m-d'),
        'payment_type_id' => 1,
        'last_schedule' => 0,
        'days' => null,
        'paid' => 0,
        'penalty' => 1,
        'interest_date' => null
    ];
});

$factory->define(\App\Cytonn\Models\RealEstatePayment::class, function (Faker\Generator $faker) {
    return [
        'date' => $faker->dateTimeThisYear->format('Y-m-d'),
        'holding_id' => 1,
        'description' => 'Payment',
        'amount' => $faker->numberBetween(25000, 1000000),
        'payment_type_id' => 1,
        'approval_id' => null,
        'schedule_id' => 1,
        'narration' => null,
        'client_payment_id' => null
    ];
});

$factory->define(\App\Cytonn\Models\RealestateLetterOfOffer::class, function (Faker\Generator $faker) {
    return [
        'holding_id' => 1,
        'advocate_id' => 1,
        'due_date' => $faker->dateTimeThisYear()->format('Y-m-d'),
        'document_id' => null,
        'date_signed' => $faker->dateTimeThisYear()->format('Y-m-d'),
        'sent_on' => null,
        'sent_by' => 1,
        'authorization_sent_on' => null,
        'pm_approval_id' => null,
        'pm_approved_on' => null,
        'history' => null,
        'sales_agreement_notification_sent_on' => null,
        'date_received' => $faker->dateTimeThisYear()->format('Y-m-d')
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateSalesAgreement::class, function (Faker\Generator $faker) {
    return [
        'holding_id' => 1,
        'date' => $faker->dateTimeThisYear,
        'document_id' => null,
        'date_signed' => $faker->dateTimeThisYear()->format('Y-m-d'),
        'date_sent_to_client' => null,
        'date_sent_to_lawyer' => null,
        'date_received_from_client' => null,
        'uploaded_on' => null,
        'history' => null,
        'date_received' => $faker->dateTimeThisYear()->format('Y-m-d')
    ];
});

$factory->define(\App\Cytonn\Models\Advocate::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'postal_code' => $faker->postcode,
        'postal_address' => $faker->address,
        'street' => $faker->streetAddress,
        'town' => $faker->city,
        'country_id' => 2,
        'building' => $faker->buildingNumber,
        'telephone_office' => $faker->phoneNumber,
        'telephone_cell' => $faker->phoneNumber,
        'email' =>$faker->email,
        'address' => $faker->address,
        'active' => 1
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateCommissionStructure::class, function (Faker\Generator $faker) {
    return [
        'effective_date' => $faker->dateTimeBetween('- 5 years', 'now')->format('Y-m-d'),
    ];
});

$factory->define(\App\Cytonn\Models\RealestateCommissionRate::class, function (Faker\Generator $faker) {
    return [
        'project_id' => 2,
        'recipient_type_id' => 1,
        'type' => 'percentage',
        'amount' => 1
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateCommissionPercentage::class, function (Faker\Generator $faker) {
    return [
        'realestate_commission_structure_id' => 2,
        'realestate_commission_parameter_id' => 1,
        'value' => 10,
        'stage' => null,
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateCommissionPaymentScheduleType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
        'description' => $faker->sentence,
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateCommissionParameter::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
    ];
});

$factory->define(\App\Cytonn\Models\RealestateCommission::class, function (Faker\Generator $faker) {
    return [
        'recipient_id' => 1,
        'amount' => $faker->numberBetween(79000, 500000),
        'holding_id' => 1,
        'awarded' => 1,
        'commission_rate_id' => null,
        'commission_rate' => 1,
        'reason' => null
    ];
});

$factory->define(\App\Cytonn\Models\RealEstateInstructionType::class, function (Faker\Generator $faker) {
    return [
        'name' => 'Holding Payment',
        'slug' => 'holding_payment',
    ];
});
