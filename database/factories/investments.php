<?php
///**
// * Date: 08/10/2016
// * Time: 22:48
// * @author Mwaruwa Chaka <mchaka@cytonn.com>
// * Project: crims
// * Cytonn Technologies
// */
//
//$factory(ClientInvestmentApplication::class, [
//    "type_id"=>ClientType::where('name', 'individual')->first()->id,
//    "joint"=>false,
//    "application_type_id"=>ClientInvestmentType::where('name', 'investment')->first()->id,
//    "client_id"=>'factory:'.Client::class,
//    "amount"=>$faker->numberBetween(100000, 10000000),
//    "agreed_rate"=>$faker->numberBetween(10, 20),
//    "tenor"=>$faker->numberBetween(3, 12),
//    "title"=>Title::all()->random()->id,
//    "lastname"=>$faker->firstName,
//    "middlename"=>$faker->lastName,
//    "firstname"=>$faker->firstName,
//    "gender_id"=>Gender::all()->random()->id,
//    "dob"=>$faker->date(),
//    "pin_no"=>$faker->numberBetween(1000303, 2393993939999),
//    "id_or_passport"=>$faker->numberBetween(2388389839, 49049090490904),
//    "email"=>$faker->email,
//    "postal_code"=>$faker->postcode,
//    "postal_address"=>$faker->numberBetween(10000, 2928299),
//    "country_id"=>Country::all()->random()->id,
////    "telephone_office",
//    "phone"=>$faker->phoneNumber,
////    "telephone_home",
//    "residential_address"=>$faker->address,
//    "street"=>$faker->streetAddress,
//    "town"=>$faker->city,
//    "method_of_contact_id"=>ContactMethod::all()->random()->id,
//    "employment_id"=>Employment::all()->random()->id,
//    "employment_other"=>$faker->sentence,
////    "present_occupation",
////    "business_sector",
//    "employer_name"=>$faker->company,
//    "employer_address"=>$faker->address,
////    "corporate_investor_type",
////    "registered_name",
////    "trade_name",
////    "registered_address",
////    "registration_number",
//    "funds_source_id"=>FundSource::all()->random()->id,
////    "funds_source_other",
//    "investor_account_name"=>$faker->name,
//    "investor_account_number"=>$faker->bankAccountNumber,
//    "investor_bank"=>$faker->word,
//    "investor_bank_branch"=>$faker->streetAddress,
////    "investor_clearing_code",
////    "investor_swift_code",
//    "terms_accepted"=>true,
////    "financial_advisor",
//    "product_id"=>Product::all()->random()->id,
////    "corporate_investor_type_other",
////    "contact_person_title",
////    "contact_person_fname",
////    "contact_person_lname",
////    "approval_id",
//    "taxable"=>true,
////    "form_id",
//    "complete"=>true,
//    "new_client"=>true
//]);
//
//$factory(ClientInvestment::class, [
//    'client_id'=>'factory:'.Client::class,
//    'application_id'=>'factory'.ClientInvestmentApplication::class,
//]);
