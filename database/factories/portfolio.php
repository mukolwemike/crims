<?php
///**
// * Date: 08/10/2016
// * Time: 22:48
// * @author Mwaruwa Chaka <mchaka@cytonn.com>
// * Project: crims
// * Cytonn Technologies
// */
//
//$factory(PortfolioInvestor::class, [
//    'name'=>$faker->company
//]);
//
//$factory(PortfolioInvestment::class, [
//    'custodial_account_id'=>CustodialAccount::first()->id,
//    'portfolio_investor_id'=>'factory:'.PortfolioInvestor::class,
//    'taxable'=>$faker->boolean(90),
//    'fund_manager_id'=>FundManager::where('name', 'CIM')->first()->id,
//    'fund_type_id'=>$faker->randomElement(FundType::lists('id')),
//    'invested_date'=>$faker->date(),
//    'maturity_date'=>$faker->date('Y-m-d', '2018-01-01'),
//    'interest_rate'=>$faker->numberBetween(10, 18),
//    'amount'=>$faker->numberBetween(10000000, 100000000)
//]);


use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioRepaymentType;
use App\Cytonn\Models\Unitization\UnitFund;
use Faker\Generator as Faker;


$factory->define(PortfolioInvestor::class, function (Faker $faker){
    return [
        'name' => $faker->company
    ];
});

$factory->define(PortfolioSecurity::class, function (Faker $faker){
    return [
        'name' =>$faker->company,
        'code' => $faker->numberBetween(100, 1000),
        'portfolio_investor_id' => factory(PortfolioInvestor::class)->create()->id,
        'fund_manager_id' => FundManager::first()->id,
        'sub_asset_class_id' => SubAssetClass::where('slug', 'deposit')->first()->id,
        'currency_id' => Currency::where('code', 'KES')->first()->id
    ];
});

$factory->define(DepositHolding::class, function (Faker $faker) {
    $type = PortfolioInvestmentType::first();

    return [
        'amount' => $amount = $faker->numberBetween(100000, 1000000),
        'bond_purchase_cost' => $amount,
        'portfolio_security_id' => factory(PortfolioSecurity::class)->create()->id,
        'taxable' => true,
        'type_id' => $type ? $type->id : null,
        'interest_rate' => $rate = 11.25,
        'coupon_rate' => $rate,
        'unit_fund_id' => UnitFund::first()->id,
        'invested_date' => $invested = \Carbon\Carbon::today()->subMonth(3),
        'bond_purchase_date' => $invested,
        'maturity_date' => \Carbon\Carbon::today()->subMonth(3),
        'bond_clean_price' => 100,
        'bond_dirty_price' => 100,
        'bond_par_price' => 100,
    ];
});

$factory->define(\App\Cytonn\Models\PortfolioInvestmentRepayment::class, function (Faker $faker){
    $investment = factory(DepositHolding::class)->create();
    return [
        'investment_id' => $investment->id,
        'amount' => 0.25 * $investment->amount,
        'date' => $investment->bond_purchase_date->copy()->addMonths(1)->addDays(2),
        'type_id' => PortfolioRepaymentType::where('slug', 'full_repayment')->first()->id,
        'narrative' => $faker->sentence
    ];
});
