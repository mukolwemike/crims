<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\Unitization\UnitFund;

/*
 * Title Factory
 */
$factory->define(Title::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->title
    ];
});

/*
 * Channels
 */

$factory->define(Channel::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->title,
        'slug' => $faker->unique()->slug()
    ];
});

/*
 * Country Factory
 */
$factory->define(\App\Cytonn\Models\Country::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name
    ];
});

/*
 * Employment Factory
 */
$factory->define(\App\Cytonn\Models\Employment::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name
    ];
});

/*
 * Contact Methods Factory
 */
$factory->define(\App\Cytonn\Models\ContactMethod::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->word
    ];
});

/*
 * Fund Source Factory
 */
$factory->define(\App\Cytonn\Models\FundSource::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

/*
 * Company Nature Factory
 */
$factory->define(\App\Cytonn\Models\CompanyNatures::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

/*
 * Gender Factory
 */
$factory->define(\App\Cytonn\Models\Gender::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'abbr' => $faker->randomLetter
    ];
});

/*
 * Client Bank Factory
 */
$factory->define(\App\Cytonn\Models\ClientBank::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'swift_code' => $faker->unique()->slug(),
        'alias' => $faker->sentence(4),
        'clearing_code' => $faker->unique()->word
    ];
});

/*
 * Product Factory
 */
$factory->define(\App\Cytonn\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->word,
        'currency_id' => 1,
        'custodial_account_id' => 1,
        'fund_manager_id' => 1,
        'shortname' => $faker->word,
        'longname' => $faker->word,
        'type_id' => 1,
        'active' => 1
    ];
});

$factory->define(\App\Cytonn\Models\ClientFilledInvestmentApplication::class, function (Faker\Generator $faker) {

    return [
        'individual' => true,
        'complete' => true,
        'amount' => $faker->numberBetween(1000, 100000),
        'tenor' => $faker->numberBetween(1, 12),
        'product_id' => \App\Cytonn\Models\Product::first()->id,
        'agreed_rate' => 18,
        'business_sector' => $faker->company,
        'country_id' => Country::first()->id,
        'dob' => $faker->date,
        'email' => $faker->email,
        'employer_address' => $faker->company,
        'employer_name' => "nnnmnm",
        'employment_id' => 1,
        'firstname' => $faker->firstname,
        'funds_source_id' => 1,
        'funds_source_other' => "",
        'gender_id' => 1,
        'id_or_passport' => "nnmnmn",
        'investor_account_name' => "nmnmnm",
        'investor_account_number' => "nmnmnm",
        'investor_bank' => 2,
        'investor_bank_branch' => 1091,
//        'jointHolders' => [],
        'kin_email' => "nmnmnm@mmso.cll",
        'kin_name' => "nmnn",
        'kin_phone' => "nmnnm",
        'kin_postal' => "mnmnnm",
        'lastname' => "bnbnbn",
        'method_of_contact_id' => 1,
        'middlename' => "bnbnbn",
        'pin_no' => "nmnmnm",
        'postal_address' => "mnmnnm",
        'postal_code' => "nmnmnm",
        'present_occupation' => "nmnnm",
        'street' => "nmnmnmnm",
        'telephone_home' => "nmnmnm",
        'telephone_office' => "nnnm",
        'title' => Title::first()->id,
        'town' => "nmmnnmnm"
    ];
});
