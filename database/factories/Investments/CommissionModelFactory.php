<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


/** @var \Illuminate\Database\Eloquent\Factory $factory */
/*
 * Commission Recipient Type Factory
 */
$factory->define(\App\Cytonn\Models\CommissionRecipientType::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
        'slug' => $faker->word
    ];
});

/*
 * Commission Recipient Ranks Factory
 */
$factory->define(\App\Cytonn\Models\CommissionRecipientRank::class, function (Faker\Generator $faker) {
    return [
        'name'  =>  $faker->word,
        'level' => $faker->numberBetween(0, 9),
    ];
});

/*
 * Commission Recipient Factory
 */
$factory->define(\App\Cytonn\Models\CommissionRecepient::class, function (Faker\Generator $faker) {
    return [
        'recipient_type_id'  =>  2,
        'email' => $faker->unique()->email,
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'phone_country_code' => $faker->countryCode,
        'zero_commission' => 0,
        'rank_id' => 1,
        'reports_to' => null,
        'employee_id' => null,
        'employee_number' => null,
        'reporting_date' => $faker->date(),
        'active' => 1,
        'crm_id' => null,
        'department_unit_id' => null,
        'account_number' => null,
        'bank_code' => null,
        'branch_code' => null,
        'kra_pin' => null
    ];
});

/*
 * Commission Factory
 */
$factory->define(\App\Cytonn\Models\Commission::class, function (Faker\Generator $faker) {
    return [
        'recipient_id'  =>  1,
        'investment_id' => 1,
        'compliant' => 1,
        'compliance_date' => $faker->date(),
        'rate' => 1,
        'start_date' => null,
        'written_off' => null,
        'per_day' => 0,
        'old_rate' => null
    ];
});

/*
 * Commission Payment Schedule Factory
 */
$factory->define(\App\Cytonn\Models\CommissionPaymentSchedule::class, function (Faker\Generator $faker) {
    return [
        'commission_id'  =>  1,
        'date' => $faker->date(),
        'amount' => $faker->randomNumber(5),
        'description' => 'Commission',
        'paid' => 0,
        'first' => 1,
        'claw_back' => null,
        'full' => 1,
        'payment_id' => null
    ];
});

/*
 * Commission Clawback Factory
 */
$factory->define(\App\Cytonn\Models\CommissionClawback::class, function (Faker\Generator $faker) {
    return [
        'commission_id'  =>  1,
        'amount' => $faker->randomNumber(5),
        'fully_paid' => null,
        'fully_paid_on' => null,
        'narration' => 'Clawback',
        'date' => $faker->date(),
        'no_date' => null,
        'type' => $faker->randomElement(['withdraw', 'rollover']),
        'withdrawal_id' => null,
        'original_commission_id' => null,
    ];
});

/*
 * Commission Clawback Factory
 */
$factory->define(\App\Cytonn\Models\BulkCommissionPayment::class, function (Faker\Generator $faker) {
    return [
        'start' => $faker->date(),
        'end' => $faker->date(),
        'approval_id' => null,
        'description' => 'Bulk Commissions',
        'investments_approval_id' => null,
        'realestate_approval_id' => null,
        'shares_approval_id' => null,
        'unitization_approval_id' => null
    ];
});
