<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Cytonn\Models\ClientInvestmentType;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/*
 * Currency Factory
 */
$factory->define(\App\Cytonn\Models\Currency::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->currencyCode,
        'name' => $faker->sentence(2),
        'symbol' => $faker->currencyCode,
        'base' => 1
    ];
});

/*
 * Custodial Account Factory
 */
$factory->define(\App\Cytonn\Models\CustodialAccount::class, function (Faker\Generator $faker) {
    return [
        'account_name' => $faker->word,
        'bank_name' => $faker->sentence(2),
        'currency_id' => 1,
        'account_no' => $faker->bankAccountNumber,
        'alias' => $faker->word,
        'fund_manager_id' => 1,
        'contact_person' => $faker->name(),
        'address' => $faker->address,
        'investment_account' => 1
    ];
});

/*
 * Product Type Factory
 */
$factory->define(\App\Cytonn\Models\ProductType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug
    ];
});

/*
 * Product Factory
 */
$factory->define(\App\Cytonn\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(3),
        'currency_id' => 1,
        'custodial_account_id' => 1,
        'fund_manager_id' => 1,
        'shortname' => $faker->word,
        'longname' => $faker->sentence(3),
        'type_id' => 1,
        'active' => 1
    ];
});

/*
 * Interest Rate Update Factory
 */
$factory->define(\App\Cytonn\Models\InterestRateUpdate::class, function (Faker\Generator $faker) {
    return [
        'product_id' => 1,
        'description' => $faker->sentence(3),
        'date' => $faker->date()
    ];
});

/*
 * Rate Factory
 */
$factory->define(\App\Cytonn\Models\Rate::class, function (Faker\Generator $faker) {
    return [
        'product_id' => 1,
        'tenor' => $faker->numberBetween(1, 36),
        'tenor_type' => $faker->numberBetween(1, 2),
        'rate' => $faker->numberBetween(10, 25),
        'interest_rate_update_id' => 1,
        'client_type' => $faker->numberBetween(1, 2)
    ];
});

/*
 * Custodial Transaction Type Factory
 */
$factory->define(\App\Cytonn\Models\CustodialTransactionType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->word
    ];
});

/*
 * Custodial Transaction Type Factory
 */
$factory->define(\App\Cytonn\Models\ClientPaymentType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->word
    ];
});

$factory->define(ClientInvestmentType::class, function () {
    return ['name' => 'investment'];
});

$factory->define(\App\Cytonn\Models\ClientInvestment::class, function (Faker\Generator $faker) {
    return [
        'id' => null,
        'application_id' => null,
        'parent_id' => null,
        'approval_id' => 1,
        'investment_type_id' => 1,
        'amount' => $faker->randomNumber(8),
        'interest_rate' => 18,
        'invested_date' => $faker->dateTimeBetween('-1 year', 'now'),
        'maturity_date' => $faker->dateTimeBetween('+1 month', '+1 year'),
        'client_id' => 1,
        'product_id' => 1,
        'product_plan_id' => null,
        'withdrawn' => 0,
        'rolled' => 0,
        'withdraw_amount' => null,
        'withdrawal_date' => null,
        'withdrawal_by' => null,
        'interest_payment_interval' => 1,
        'interest_payment_date' => 31,
        'interest_payment_start_date' => null,
        'on_call' => 0,
        'interest_action_id' => 1,
        'interest_reinvest_tenor' => 12,
        'maturity_notification_sent_on' => null,
        'loyalty_rate' => null
    ];
});

$factory->define(\Cytonn\Models\ClientInvestmentWithdrawal::class, function (Faker\Generator $faker) {
    return [
        'investment_id' => 1,
        'amount' => $faker->randomNumber(5),
        'date' => $faker->date,
        'approval_id' => null,
        'payment_out_id' => null,
        'description' => 'Withdrawal',
        'type_id' => 1,
        'form_id' => null,
        'narration' => null,
        'withdraw_type' => 'withdrawal',
        'reinvested_to' => null
    ];
});

$factory->define(\Cytonn\Models\ClientInvestmentWithdrawalType::class, function (Faker\Generator $faker) {
    return [
        'slug' => $faker->slug,
        'name' => $faker->randomLetter
    ];
});

$factory->define(\App\Cytonn\Models\ClientInstructionType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\App\Cytonn\Models\ClientInvestmentInstruction::class, function (Faker\Generator $faker) {
    return [
        'investment_id' => 1,
        'user_id' => null,
        'type_id' => 1,
        'amount' => $faker->randomNumber(7),
        'amount_select' => 'principal_interest',
        'withdrawal_date' => null,
        'withdrawal_stage' => null,
        'agreed_rate' => 18,
        'tenor' => 12,
        'due_date' => $faker->date,
        'inactive' => 0,
        'reason' => null,
        'client_account_id' => null,
        'interest_payment_interval' => null,
        'approval_id' => null,
        'withdraw_type' => null,
        'topup_amount' => null,
        'automatic_rollover' => 0,
        'filled_by' => null,
        'combine_date' => null,
        'agreed_to_terms' => null
    ];
});

/*
 * Interest Action Factory
 */
$factory->define(\App\Cytonn\Models\InterestAction::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
    ];
});

/*
 * Interest Payment Schedule Factory
 */
$factory->define(\App\Cytonn\Models\InterestPaymentSchedule::class, function (Faker\Generator $faker) {
    return [
        'investment_id' => 1,
        'date' => $faker->date(),
        'amount' => $faker->randomNumber(4),
        'description' => 'Interest Payment',
        'fixed' => 0
    ];
});

/*
 * Client Payment Factory
 */
$factory->define(\App\Cytonn\Models\ClientPayment::class, function (Faker\Generator $faker) {
    return [
        'client_id' => 1,
        'commission_recipient_id' => null,
        'amount' => $faker->randomNumber(7),
        'description' => 'Client Payment',
        'date' => $faker->date(),
        'custodial_transaction_id' => null,
        'type_id' => 1,
        'product_id' => null,
        'project_id' => null,
        'share_entity_id' => null,
        'parent_id' => null,
        'unit_fund_id' => null,
        'value' => $faker->randomNumber(7)
    ];
});

/*
 * Custodial Transactions Factory
 */
$factory->define(\App\Cytonn\Models\CustodialTransaction::class, function (Faker\Generator $faker) {
    return [
        'type' => 1,
        'custodial_account_id' => 1,
        'amount' => $faker->randomNumber(7),
        'description' => 'Custodial Transaction',
        'date' => $faker->date(),
        'transaction_owners_id' => null,
        'received_from' => 'Received from',
        'client_id' => null,
        'commission_recipient_id' => null,
        'approval_id' => null,
        'bank_reference_no' => null,
        'cheque_number' => null,
        'entry_date' => null,
        'mpesa_confirmation_code' => null,
        'source' => null,
        'portfolio_institution_id' => null,
        'exchange_rate' => null,
        'received_date' => null,
        'cheque_document_id' => null,
        'value' => null
    ];
});

$factory->define(\App\Cytonn\Models\DocumentType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
        'folder' => $faker->slug,
        'direct_upload' => 0,
        'client_visible' => null
    ];
});

$factory->define(\App\Cytonn\Models\Document::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'filename' => $faker->uuid,
        'type_id' => 1,
        'date' => $faker->date(),
        'title' => $faker->word,
        'approval_id' => null,
        'project_id' => null,
        'product_id' => null,
        'portfolio_investor_id' => null,
        'compliance_document' => 0,
        'history' => null,
        'client_id' => null,
        'public' => null,
        'user_id' => null,
        'visible' => null
    ];
});

$factory->define(\App\Cytonn\Models\ClientBank::class, function (Faker\Generator $faker) {
    return [
        'name' => $this->faker->name,
        'swift_code' => $this->faker->name,
    ];
});

$factory->define(\App\Cytonn\Models\ClientBankBranch::class, function (Faker\Generator $faker) {
    return [
        'name' => $this->faker->name,
        'bank_id' => factory(\App\Cytonn\Models\ClientBank::class)->create()->id,
    ];
});

$factory->define(\App\Cytonn\Models\ClientBankAccount::class, function (Faker\Generator $faker) {
    return [
        'client_id' => factory(\App\Cytonn\Models\Client::class)->create()->id,
        'account_name' => $this->faker->firstName . ' ' . $this->faker->lastname,
        'account_number' => $this->faker->numberBetween(1000, 1000000),
        'bank_name' => $this->faker->name,
        'branch_name' => $this->faker->name,
        'branch_id' => factory(\App\Cytonn\Models\ClientBankBranch::class)->create()->id,
    ];
});
