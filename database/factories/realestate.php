<?php
/**
 * Date: 08/10/2016
 * Time: 22:48
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

//$factory(RealEstateUnitTranche::class, [
//    'project_id'=>'',
//    'payment_plan_id'=>'',
//    'name'=>$faker->word,
//    'date'=>$faker->date()
//]);
//
//$factory(ReservationForm::class, [
//    //TODO add properties
//    'client_id'=>'factory:'.Client::class
//]);
//
//$factory(UnitHolding::class, [
//    'unit_id'=>'factory:'.RealestateUnit::class,
//    'client_id'=>'factory:'.Client::class,
//    'active'=>true,
//    'tranche_id'=>null,
//    'payment_plan_id'=>null
//]);
//

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Cytonn\Models\FundManager::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->word,
        'fullname'=>$faker->numberBetween(1000, 9999),
        'active'=> 1,
    ];
});

$factory->define(\App\Cytonn\Models\ProjectType::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->word,
    ];
});

$factory->define(\App\Cytonn\Models\Project::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->word,
        'code'=>$faker->numberBetween(1000, 9999),
        'type_id'=> factory(\App\Cytonn\Models\ProjectType::class)->create()->id,
        'reservation_fee'=>25000,
        'commission_calculator'=>'payment_plan_based',
        'fund_manager_id'=> factory(\App\Cytonn\Models\FundManager::class)->create()->id,
    ];
});

$factory->define(\App\Cytonn\Models\RealestateUnitSize::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'realestate_land_size_id'=> factory(\App\Cytonn\Models\RealestateLandSize::class)->create()->id,
        'on_land_size'=> '1/8th Acre',
    ];
});

$factory->define(\App\Cytonn\Models\RealestateLandSize::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
    ];
});

$factory->define(\App\Cytonn\Models\RealestateUnit::class, function (Faker\Generator $faker) {
    return [
        'project_id'=> factory(\App\Cytonn\Models\Project::class)->create()->id,
        'number'=>$faker->numberBetween(100, 1000),
        'size_id'=> factory(\App\Cytonn\Models\RealestateUnitSize::class)->create()->id,
        'type_id'=> \App\Cytonn\Models\RealestateUnitSize::all()->random()->id,
    ];
});

$factory->define(\App\Cytonn\Models\RealEstatePayment::class, function (Faker\Generator $faker) {
    return [
        'date' => $faker->date(),
        'holding_id' => 1,
        'description' => $faker->text,
        'amount' =>  $faker->numberBetween(100000, 10000000),
        'payment_type_id' => 1,
        'approval_id' => 1,
        'schedule_id' => 1,
        'client_payment _id' => $faker->numberBetween(1, 10)
    ];
});

$factory->define(\App\Cytonn\Models\ReservationForm::class, function (Faker\Generator $faker) {
    return [
        'date' => $faker->date(),
        'holding_id' => 1,
        'client_type_id' => 1,
    ];
});
