<?php

use App\Cytonn\Models\ContactMethod;

/**
 * Date: 9/2/15
 * Time: 9:41 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ContactMethodSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $methods = ['email', 'post'];

        foreach ($methods as $method) {
            $m = new ContactMethod();
            $m->description = $method;

            $m->save();
        }
    }
}
