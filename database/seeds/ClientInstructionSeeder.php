<?php

use App\Cytonn\Models\ClientInstructionType;

/**
 * Date: 26/01/2016
 * Time: 12:49 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientInstructionSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $names = ['withdraw', 'rollover'];

        foreach ($names as $name) {
            $t = new ClientInstructionType();
            $t->name = $name;
            $t->save();
        }
    }
}
