<?php
/**
 * Date: 01/10/2015
 * Time: 3:37 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


use App\Cytonn\Models\FundType;
use Illuminate\Database\Seeder;

class PortfolioSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $fundtypes = ['Deposit', 'Commercial Paper', 'T-Bill', 'T-Bond', 'Others'];

        foreach ($fundtypes as $ty) {
            $t = new FundType();
            $t->name = $ty;
            $t->save();
        }
    }
}
