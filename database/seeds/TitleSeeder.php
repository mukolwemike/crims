<?php

use App\Cytonn\Models\Title;

/**
 * Date: 9/2/15
 * Time: 4:11 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class TitleSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $titles = ['Mr', 'Mrs', 'Miss', 'Ms', 'Dr', 'Prof', 'Eng', 'Other'];

        foreach ($titles as $ti) {
            $t = new Title();
            $t->name = $ti;
            $t->save();
        }
    }
}
