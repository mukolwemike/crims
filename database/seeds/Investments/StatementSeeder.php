<?php

namespace Investments;
use App\Cytonn\Models\StatementCampaignType;


/**
 * Date: 10/10/2016
 * Time: 12:10
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class StatementSeeder extends \Illuminate\Database\Seeder
{
    /**
     *
     */
    public function run()
    {
        $types = [
            'manual', 'automated'
        ];

        foreach ($types as $type) {
            $t = new StatementCampaignType();
            $t->name = ucfirst($type);
            $t->slug = $type;

            $t->save();
        }
    }
}
