<?php
/**
 * Date: 10/10/2016
 * Time: 12:16
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Investments;

use Illuminate\Database\Seeder;
use DB;
use Eloquent;

class InvestmentsSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $tables = [
        'statement_campaign_types'
    ];

    /**
     * @var array
     */
    protected $seeders = [
        StatementSeeder::class
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

//        $this->clean();

        foreach ($this->seeders as $seeder) {
            $this->call($seeder);
        }
    }

    /**
     *
     */
    public function clean()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
