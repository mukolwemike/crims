<?php
/**
 * Date: 08/10/2016
 * Time: 21:53
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */


use App\Cytonn\Models\FundManager;
use Illuminate\Database\Seeder;

class FundManagerSeeder extends Seeder
{
    public function run()
    {
        $cim = FundManager::create([
            'name'=>'CIM',
            'logo'=>'cms_logo_md.png',
            're_logo'=>'cre_logo_md.png',
            'fullname'=>'Principal Partner, Cytonn High Yield Solutions LLP'
        ]);

        $coop = FundManager::create([
            'name'=>'Coop',
            'fullname'=>'Cytonn Investments Cooperative Society',
            'logo'=>'coop_logo_md.png'
        ]);

        $coop->modules()->create([
            'name'=>'coop'
        ]);

        $cim->modules()->create([
            'name'=>'cms'
        ]);
    }
}
