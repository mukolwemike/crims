<?php

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\CustodialWithdrawType;
use App\Cytonn\Models\FundManager;

/**
 * Date: 20/11/2015
 * Time: 4:12 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class CustodialSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $types = ['FI'=>'Client funds in', 'FO'=>'Client funds out', 'I'=>'Fund Investment', 'R'=>'Fund Redemption', 'ECF'=>'Custodial fees', 'EMF'=>'Management fees', 'EFC'=>'FA Commission'];

        foreach (array_keys($types) as $t) {
            $tt = new CustodialTransactionType();
            $tt->name = $t;
            $tt->description = $types[$t];
            $tt->save();
        }

        $withdraws = ['ECF'=>'Custodial fees', 'EMF'=>'Management fees'];

        foreach ($withdraws as $key => $val) {
            $w = new CustodialWithdrawType();
            $w->code = $key;
            $w->name = $val;
            $w->save();
        }

        CustodialAccount::create([
            'account_name'=>'Cytonn Cash Mgt KES',
            'bank_name'=>'Standard Chartered',
            'currency_id'=>Currency::where('code', 'KES')->first()->id,
            'account_no'=>329399030390,
            'alias'=>'Investment A/C',
            'fund_manager_id'=>FundManager::where('name', 'CIM')->first()->id
        ]);
    }
}
