<?php

use Illuminate\Database\Seeder;
use Portfolio\AssetClassTableSeeder;
use Portfolio\GenPortfolioSeeder;
use Portfolio\SubAssetClassTableSeeder;

class BasicPortfolioSeeder extends Seeder
{
    public function run()
    {
        $this->call(AssetClassTableSeeder::class);
        $this->call(SubAssetClassTableSeeder::class);
        $this->call(GenPortfolioSeeder::class);
    }
}
