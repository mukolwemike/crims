<?php

use App\Cytonn\Models\Employment;

/**
 * Date: 9/2/15
 * Time: 12:01 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class EmploymentSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $emp = ['Employed', 'Self Employed', 'Unemployed', 'Retired'];

        foreach ($emp as $em) {
            $e = new Employment();
            $e->name = $em;
            $e->save();
        }
    }
}
