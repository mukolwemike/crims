<?php
/**
 * Date: 08/08/2016
 * Time: 8:57 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use App\Cytonn\Models\RealEstatePaymentPlan;
use Illuminate\Database\Seeder;

class RealEstatePaymentPlanSeeder extends Seeder
{
    public function run()
    {
        $plans = [
            'cash'=>'Cash',
            'installment'=>'Installment',
            'mortgage'=>'Mortgage',
            'zero_deposit_mortgage'=>'Zero Deposit Mortgage',
            'zero_deposit_installment'=>'Zero Deposit Installment'
        ];

        foreach ($plans as $slug => $name) {
            $p = new RealEstatePaymentPlan();
            $p->slug = $slug;
            $p->name = $name;
            $p->save();
        }
    }
}
