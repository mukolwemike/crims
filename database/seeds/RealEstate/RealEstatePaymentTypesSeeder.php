<?php
/**
 * Date: 08/08/2016
 * Time: 8:53 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use App\Cytonn\Models\RealEstatePaymentType;
use Illuminate\Database\Seeder;

class RealEstatePaymentTypesSeeder extends Seeder
{
    public function run()
    {
        $types = [
            'reservation_fee'=>'Reservation fees',
            'deposit'=>'10% Deposit',
            'installment'=>'Installment'
        ];

        foreach ($types as $slug => $name) {
            $t = new RealEstatePaymentType();
            $t->slug = $slug;
            $t->name = $name;
            $t->save();
        }
    }
}
