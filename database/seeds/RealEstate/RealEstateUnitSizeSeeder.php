<?php
/**
 * Date: 08/08/2016
 * Time: 8:49 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use App\Cytonn\Models\RealestateUnitSize;
use Illuminate\Database\Seeder;

class RealEstateUnitSizeSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        foreach (range(1, 5) as $s) {
            $size = new RealestateUnitSize();
            $size->name = $s.' Bedroom';
            $size->save();
        }

        $sizes = ['0.125', '0.25', '0.5', '1'];

        foreach ($sizes as $acre) {
            $size = new RealestateUnitSize();
            $size->name = $acre.' Acre';
            $size->save();
        }
    }
}
