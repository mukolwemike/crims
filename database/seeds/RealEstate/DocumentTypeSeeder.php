<?php
/**
 * Date: 08/08/2016
 * Time: 8:38 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use App\Cytonn\Models\DocumentType;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{

    public function run()
    {
        $types = [
            'loo'=>'Letter of Offer',
            'sales_agreement'=>'Sales Agreement'
        ];

        foreach ($types as $slug => $name) {
            $type = new DocumentType();
            $type->slug = $slug;
            $type->name = $name;
            $type->save();
        }
    }
}
