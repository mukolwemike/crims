<?php
/**
 * Date: 08/08/2016
 * Time: 8:36 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use BasicUnitFundSeeder;
use Illuminate\Database\Seeder;
use DB;
use Eloquent;

/**
 * Class RealEstateSeeder
 * @package RealEstate
 */
class RealEstateSeeder extends Seeder
{


    /**
     * @var array
     */
    protected $tables = [
//        'document_types',
        'realestate_unit_types',
        'realestate_unit_sizes',
        'realestate_payment_types',
        'realestate_payment_plans',
        'project_types'
    ];

    /**
     * @var array
     */
    protected $seeders = [
        DocumentTypeSeeder::class,
        RealEstateUnitTypeSeeder::class,
        RealEstateUnitSizeSeeder::class,
        RealEstatePaymentTypesSeeder::class,
        RealEstatePaymentPlanSeeder::class,
        BasicUnitFundSeeder::class
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

//        $this->clean();

        foreach ($this->seeders as $seeder) {
            $this->call($seeder);
        }
    }

    /**
     *
     */
    public function clean()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
