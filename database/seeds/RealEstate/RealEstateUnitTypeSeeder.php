<?php
/**
 * Date: 08/08/2016
 * Time: 8:45 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use App\Cytonn\Models\RealestateUnitType;
use Illuminate\Database\Seeder;

class RealEstateUnitTypeSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $types = ['Apartment', 'Villa', 'Plot'];

        foreach ($types as $type) {
            $t = new RealestateUnitType();
            $t->name = $type;
            $t->save();
        }
    }
}
