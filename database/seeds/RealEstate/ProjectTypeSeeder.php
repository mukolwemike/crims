<?php
/**
 * Date: 08/08/2016
 * Time: 9:00 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace RealEstate;

use App\Cytonn\Models\ProjectType;
use Illuminate\Database\Seeder;

class ProjectTypeSeeder extends Seeder
{
    public function run()
    {
        $types = [
            'apartments'=>'Apartment Blocks',
            'gated_community'=>'Exclusive gated community',
            'sharplands'=>'Sharplands'
        ];

        foreach ($types as $slug => $name) {
            $t = new ProjectType();
            $t->slug = $slug;
            $t->name = $name;
            $t->save();
        }
    }
}
