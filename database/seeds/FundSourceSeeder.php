<?php

use App\Cytonn\Models\FundSource;

/**
 * Date: 9/6/15
 * Time: 10:06 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */



class FundSourceSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $sources = ['Profits', 'Rental/Property Sale', 'Sale of Shares', 'Dividends/interest', 'Maturing Investments', 'Loan', 'Other'];

        foreach ($sources as $s) {
            $src = new FundSource();
            $src->name = $s;
            $src->save();
        }
    }
}
