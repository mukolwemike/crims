<?php

use App\Cytonn\Models\Setting;

/**
 * Date: 9/11/15
 * Time: 11:00 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */




class SettingsSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $settings = [
            'withholding_tax_rate' => 15,
            'custody_fee_rate' => '0.145',
            'commission_first_installment_rate' => '0.5',
            'dividend_tax_rate' => 5
        ];


        foreach (array_keys($settings) as $key)
        {
            $s = new Setting();

            $s->key = $key;

            $s->value = $settings[$key];

            $s->save();
        }

        $s = new Setting();
        $s->key = 'withholding_tax_rate';
        $s->value = 15;
        $s->save();
    }
}
