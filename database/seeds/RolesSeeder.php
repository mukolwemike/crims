<?php
/**
 * Date: 9/21/15
 * Time: 7:02 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


use Cytonn\Authorization\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        $suadmin = Role::create([
            'name'=>'superadmin',
            'description'=>'Super Administrator'
        ]);
        $suadmin->save();

        $admin = Role::create([
            'name'=>'admin',
            'description'=>'System Administrator'
        ]);
        $admin->save();

        $staff = Role::create([
            'name'=>'staff',
            'description'=>'Staff Member'
        ]);
        $staff->save();

        $client = Role::create([
           'name'=>'client',
            'description'=>'Client'
        ]);
        $client->save();
    }
}
