<?php
/**
 * Date: 9/6/15
 * Time: 10:40 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientType;
use Illuminate\Database\Seeder;

class ClientTypeSeeder extends Seeder
{
    public function run()
    {
        $types = ['individual', 'corporate'];

        foreach ($types as $ty) {
            $t = new ClientType();
            $t->name = $ty;
            $t->save();
        }

        $investmenttypes = ['investment', 'topup', 'rollover', 'partial_withdraw'];

        foreach ($investmenttypes as $ty) {
            $t = new ClientInvestmentType();
            $t->name = $ty;
            $t->save();
        }
    }
}
