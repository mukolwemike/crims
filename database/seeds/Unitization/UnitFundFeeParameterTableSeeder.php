<?php

namespace Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use Faker\Factory;
use Illuminate\Database\Seeder;
use function str_slug;

class UnitFundFeeParameterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parameters = [
            ['name' => 'Total Asset Under Management', 'slug' => str_slug('Total Asset Under Management')],
            ['name' => 'Investment Amount', 'slug' => str_slug('Investment Amount')],
        ];

        foreach($parameters as $parameter) {
            UnitFundFeeParameter::create($parameter);
        }
    }
}
