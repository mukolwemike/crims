<?php

namespace Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UnitFundHolderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $holder = new UnitFundHolder();

        $holder->fill([
            'number'                    =>  $faker->numberBetween(10000, 10000000),
            'unit_fund_id'              =>  UnitFund::latest()->first()->id,
            'client_id'                 =>  Client::latest()->first()->id,
        ]);
        $holder->save();

    }
}
