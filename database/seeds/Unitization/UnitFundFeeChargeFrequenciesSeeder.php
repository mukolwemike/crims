<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 30/04/2018
 * Time: 10:24
 */

namespace Unitization;


use App\Cytonn\Models\Unitization\UnitFundFeeChargeFrequency;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeType;
use Illuminate\Database\Seeder;

class UnitFundFeeChargeFrequenciesSeeder extends Seeder
{
    public function run()
    {
        $parameters = [
            ['name' => 'Daily', 'slug' => str_slug('Daily')],
            ['name' => 'Monthly', 'slug' => str_slug('Monthly')],
            ['name' => 'Annually', 'slug' => str_slug('Annually')],
        ];

        foreach($parameters as $parameter) {
            UnitFundFeeChargeFrequency::create($parameter);
        }
    }
}