<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 30/04/2018
 * Time: 10:24
 */

namespace Unitization;


use App\Cytonn\Models\Unitization\UnitFundFeeChargeType;
use Illuminate\Database\Seeder;

class UnitFundFeeChargeTypesSeeder extends Seeder
{
    public function run()
    {
        $parameters = [
            ['name' => 'Once', 'slug' => str_slug('Once')],
            ['name' => 'Recurrent', 'slug' => str_slug('Recurrent')],
        ];

        foreach($parameters as $parameter) {
            UnitFundFeeChargeType::create($parameter);
        }
    }
}