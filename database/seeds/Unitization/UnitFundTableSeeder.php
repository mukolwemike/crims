<?php

namespace Unitization;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFund;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UnitFundTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $unitFund = new UnitFund();

        $unitFund->fill([
            'name'                      =>  $faker->company,
            'initial_unit_price'        =>  $faker->numberBetween(100, 1000),
            'minimum_investment_amount' =>  $faker->numberBetween(10000, 100000),
            'fund_objectives'           =>  $faker->paragraph,
            'benefits'                  =>  $faker->paragraph,
            'minimum_investment_horizon'=>  $faker->numberBetween(12, 18),
            'currency_id'               =>  Currency::where('code', 'KES')->first()->id,
            'fund_manager_id'           =>  FundManager::where('name', 'CIM')->first()->id,
        ]);
        $unitFund->save();

    }
}
