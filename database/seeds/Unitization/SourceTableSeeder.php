<?php

namespace Unitization;

use App\Cytonn\Models\Unitization\Source;
use Illuminate\Database\Seeder;

class SourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sources = [
            ['name' => 'TV'],
            ['name' => 'Newspaper'],
            ['name' => 'Word of Mouth'],
            ['name' => 'Internet'],
            ['name' => 'Other'],
        ];

        foreach($sources as $source)
        {
            Source::create($source);
        }
    }
}
