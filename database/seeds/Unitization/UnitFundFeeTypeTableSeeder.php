<?php

namespace Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UnitFundFeeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'Front-load Fee', 'slug' => 'front-load'],
            ['name' => 'Management Fee', 'slug' => 'management'],
            ['name' => 'Custody Fee', 'slug' => 'custody'],
            ['name' => 'Internet Fee', 'slug' => 'internet'],
            ['name' => 'Premature Withdrawal Fee', 'slug' => 'premature-withdrawal', 'exception'=>true],
            ['name' => 'Switch Rate', 'slug' => 'switch-rate', 'exception'=>true],
        ];

        foreach($types as $type) {
            UnitFundFeeType::create($type);
        }
    }
}
