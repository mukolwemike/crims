<?php
/**
 * Date: 9/2/15
 * Time: 9:06 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


use App\Cytonn\Models\CorporateInvestorType;
use Illuminate\Database\Seeder;

class CorporateInvestorTypeSeeder extends Seeder
{

    public function run()
    {
        $names = ['Company', 'Trust', 'Fund', 'Other'];
        foreach ($names as $name) {
            $inv = new CorporateInvestorType();
            $inv->name = $name;
            $inv->save();
        }
    }
}
