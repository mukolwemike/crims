<?php

use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientType;

/**
 * Date: 07/12/2015
 * Time: 3:10 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ComplianceSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $individual = ClientType::where('name', 'individual')->first()->id;
        $corporate = ClientType::where('name', 'corporate')->first()->id;
        $docs = [
            'Identification Document'=>$individual,
            'PIN/Tax Certificate'=>$individual,
            'Proof of Banking Details'=>$individual,
            'Physical Address Verification'=>$individual,
            'Transfer Advise'=>$individual,


            'Founding Documents'=>$corporate,
            'Resolution mandate authorizing the investment'=>$corporate,
            'Copy of Company PIN/TAX Certificate'=>$corporate,
            'Proof of Banking Details '=>$corporate,
            'Documents from authorized representatives (Copy of ID and PIN)'=>$corporate,
            'Transfer Advise '=>$corporate
        ];

        foreach ($docs as $key => $val) {
            $c = new ClientComplianceChecklist();
            $c->name = $key;
            $c->client_type_id = $val;
            $c->save();
        }
    }
}
