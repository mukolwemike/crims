<?php

use Illuminate\Database\Seeder;
use Unitization\SourceTableSeeder;
use Unitization\UnitFundFeeChargeFrequenciesSeeder;
use Unitization\UnitFundFeeChargeTypesSeeder;
use Unitization\UnitFundFeeParameterTableSeeder;
use Unitization\UnitFundFeeTypeTableSeeder;
use Unitization\UnitFundHolderTableSeeder;
use Unitization\UnitFundTableSeeder;

class BasicUnitFundSeeder extends Seeder
{
    public function run()
    {
        $this->call(UnitFundFeeChargeFrequenciesSeeder::class);
        $this->call(UnitFundFeeChargeTypesSeeder::class);
//        $this->call(SourceTableSeeder::class);
//        $this->call(UnitFundFeeTypeTableSeeder::class);
//        $this->call(UnitFundFeeParameterTableSeeder::class);

//        $this->call(UnitFundTableSeeder::class);
//        $this->call(UnitFundHolderTableSeeder::class);
    }
}
