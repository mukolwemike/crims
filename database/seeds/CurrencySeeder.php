<?php


use App\Cytonn\Models\Currency;

class CurrencySeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $currencies = ['KES'=>'Kenya Shillings', 'USD'=>'US Dollars', 'GBP'=>'Great Britain Pound', 'EUR'=>'Euro'];

        $keys = array_keys($currencies);

        foreach ($keys as $key) {
            $c = new Currency();
            $c->code = $key;
            $c->name = $currencies[$key];
            $c->save();
        }
    }
}
