<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 21/11/2018
 * Time: 08:07
 */

class RiskyStatusSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $names = ['Due Diligence', 'Risky', 'Cleared'];

        foreach ($names as $name) {
            $rs = new \App\Cytonn\Models\Client\RiskyStatus();
            $rs->name = $name;
            $rs->save();
        }
    }
}
