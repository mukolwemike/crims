<?php

use App\Cytonn\Models\CommissionRate;

/**
 * Date: 05/10/2015
 * Time: 11:52 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */



class CommissionRateSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $c = new CommissionRate();
        $c->rate = 0.5;
        $c->name = 'Staff';
        $c->save();

        $co = new CommissionRate();
        $co->rate = 1;
        $co->name = 'FAs';
        $co->save();
    }
}
