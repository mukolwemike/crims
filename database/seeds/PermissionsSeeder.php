<?php

/**
 * Date: 9/21/15
 * Time: 7:11 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

use Cytonn\Authorization\Permission;

class PermissionsSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        foreach ($this->permissions() as $attr) {
            $perm = new Permission();
            $perm->fill($attr);
            $perm->save();
        }
    }

    public function permissions()
    {
        return
            [
                ["name" => "addinvestment","description" => "Add an investment to the system, withdraw, rollover or topup"],
                ["name" => "approveclientinvestment","description" => "Approve a client investment transaction"],
                ["name" => "sendstatements","description" => "Send client statements"],
                ["name" => "approvestatements","description" => "Approve statements to be sent"],
                ["name" => "assignpermissions","description" => "Assign permissions to users"],
                ["name" => "checkcompleteness","description" => "Check completeness of documents"],
                ["name" => "checkkyc","description" => "Check and approve KYC Documents"],
                ["name" => "createapplications","description" => "Create Applications for Client products"],
                ["name" => "addusers","description" => "Add users to the system"],
                ["name" => "viewinvestments","description" => "View Client Investments"],
                ["name" => "viewcontacts","description" => "View contacts"],
                ["name" => "addcontacts","description" => "Add contacts"],
                ["name" => "editclients","description" => "Edit Client Details"],
                ["name" => "viewclients","description" => "View clients in the system"],
                ["name" => "viewportfoliosummary","description" => "View portfolio summary"],
                ["name" => "viewcustodialaccount","description" => "View custodial accounts"],
                ["name" => "addcustodialaccount","description" => "Add custodial account"],
                ["name" => "approveportfoliotransaction","description" => "Approve portfolio transactions"],
                ["name" => "addportfolioinvestment","description" => "Add portfolio investment"],
                ["name" => "addportfolioinstitution","description" => "Add portfolio institution"],
                ["name" => "manageusers","description" => " Manage users in the system"],
                ["name" => "manageclientusers","description" => "Manage client user accounts"],
                ["name" => "create_realestate_projects","description" => "Create real estate projects"],
                ["name" => "viewactivitylog","description" => "View activity log"],
                ["name" => "manage_active_strategy","description" => "Manage active strategy"],
                ["name" => "send_loo","description" => "Send a letter of offer to clients"],
                ["name" => "pm_approve_loo","description" => "Approve a letter of offer as the project manager"],
                ["name" => "viewportfolio","description" => "View portfolio investments"],
                ["name" => "post_transactions","description" => "Post transactions"],
                ["name" => "view_share_entities","description" => "View share entities"],
                ["name" => "create_unit_holdings","description" => "Create Real Estate Unit Holdings"],
                ["name" => "add_real_estate_payments","description" => "Add Real Estate Payments"],
                ["name" => "upload_loo_sa","description" => "Upload Loo\/Sales Agreement"],
                ["name" => "view_loo_sa","description" => "View Loo\/Sales Agreement"],
                ["name" => "view_unit_holdings","description" => "View unit holdings"],
                ["name" => "view_interest_rates","description" => "View prevailing interest rates"],
                ["name" => "client_accounting_reports","description" => "Generate client accounting reports"],
                ["name" => "portfolio_accounting_reports","description" => "Portfolio Accounting Reports"],
                ["name" => "approveExemptions","description" => "Approve Transaction Exemptions"],
                ["name" => "upload_documents","description" => "Upload documents to the system"],
                ["name" => "view_share_holders","description" => "View OTC Shareholders"],
                ["name" => "create_share_holder","description" => "Create a new shareholders"],
                ["name" => "view_share_holding","description" => "View OTC shareholding"],
                ["name" => "update_signing_mandate","description" => "Update signing mandate"],
                ["name" => "store_client_guards","description" => "Store client guards"],
                ["name" => "add_email_indemnity","description" => "Add email indemnity"],
                ["name" => "investments:view-analytics","description" => "View investment analytics"],
                ["name" => "investments:view-commission","description" => "View investment commissions"],
                ["name" => "investments:view","description" => "View investments in the systems"],
                ["name" => "client-instructions:view","description" => "View client instructions in the system"],
                ["name" => "investments:view-compliance","description" => "View investments compliance in the system"],
                ["name" => "portfolio:create-investments","description" => "Create portfolio investments in the system"],
                ["name" => "portfolio:view-investments","description" => "View portfolio investments in the system"],
                ["name" => "client-approvals:view","description" => "View client approvals in the system"],
                ["name" => "client-approvals:approve","description" => "Approval client transactions in the system"],
                ["name" => "clients:view","description" => "View clients in the system"],
                ["name" => "documents:view","description" => "View documents"],
                ["name" => "investments:create","description" => "Create client investments"],
                ["name" => "client_user_management","description" => "Manage client user accounts"],
                ["name" => "investments:view-statements","description" => "View investments statements"],
                ["name" => "cash-transactions","description" => "Perform cash transactions"],
                ["name" => "portfolio-approvals:view","description" => "View portfolio approvals"],
                ["name" => "portfolio-approvals:create","description" => "Create portfolio approvals"],
                ["name" => "investments:create-commission","description" => "Create investment commissions"],
                ["name" => "clients:create","description" => null],
                ["name" => "realestate:view","description" => "View real estate section"],
                ["name" => "realestate:view-commissions","description" => null],
                ["name" => "investments:create-applications","description" => null],
                ["name" => "realestate:create","description" => null],
                ["name" => "portfolio:view-analytics","description" => null],
                ["name" => "bank:view","description" => null],
                ["name" => "bank:create","description" => null],
                ["name" => "investments:reports","description" => null],
                ["name" => "user_management","description" => null],
                ["name" => "documents:create","description" => null],
                ["name" => "realestate:send-client-documents","description" => null],
                ["name" => "investments:send-statements","description" => null],
                ["name" => "shares:send-client-documents","description" => null],
                ["name" => "realestate:reports","description" => "View real estate reports"],
                ["name" => "realestate-approvals:loo","description" => null],
                ["name" => "shares:view","description" => null],
                ["name" => "realestate:statements","description" => "View real estate statements"],
                ["name" => "client-instructions:action","description" => null],
                ["name" => "realestate:create-commissions","description" => null],
                ["name" => "system:administration","description" => null],
                ["name" => "active-strategy:view","description" => null],
                ["name" => "active-strategy:create","description" => null],
                ["name" => "portfolio-approvals:approve","description" => null],
                ["name" => "portfolio-reports","description" => null],
                ["name" => "shares:create","description" => null],
                ["name" => "investments:menu","description" => "View investments menu"],
                ["name" => "realestate:manage-advocates","description" => "Manage real estate advocates"],
                ["name" => "realestate:create-setup","description" => null],
                ["name" => "shares:statements","description" => null],
                ["name" => "portfolio:menu","description" => null],
                ["name" => "shares:reports","description" => null],
                ["name" => "realestate:manage-banks","description" => null],
                ["name" => "investments:manage-banks","description" => null],
                ["name" => "portfolio:reports","description" => null],
                ["name" => "products:view","description" => null],
                ["name" => "shares:applications","description" => null],
                ["name" => "products:create","description" => null],
                ["name" => "realestate:create:sales-agreement","description" => "Creat sales agreements"],
                ["name" => "investments:view_reports","description" => "View the investments reports page"],
                ["name" => "investments:commission_reports","description" => "Generate investments commissions reports"],
                ["name" => "clients:menu","description" => "View Clients Menu"],
                ["name" => "investments:create-compliance","description" => null]
            ];
    }
}
