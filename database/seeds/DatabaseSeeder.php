<?php

use Cytonn\Core\DataStructures\Collection;
use Illuminate\Database\Seeder;
use Investments\InvestmentsSeeder;
use RealEstate\RealEstateSeeder;

class DatabaseSeeder extends Seeder
{

    protected $tables = [
        'fund_managers', 'modules', 'fund_manager_module', 'authoritaire_permissions', 'authoritaire_roles', 'authoritaire_user_permissions',
        'authoritaire_role_permissions', 'authoritaire_memberships',
        'contacts', 'users', 'countries', 'currencies', 'products', 'contact_method', 'employment', 'gender', 'titles',
        'funds_sources', 'client_type','contact_types', 'contact_categories', 'corporate_investor_type', 'settings',
        'fund_types', 'commission_rates', 'contact_entity_types', 'custodial_transaction_types',
        'client_compliance_checklist', 'client_instruction_type'
    ];

    /**
     * @var array
     */
    protected $seeders = [
        FundManagerSeeder::class,
        ContactsSeeder::class,
        'CountrySeeder',
        'CompanySeeder',
        'CurrencySeeder',
        CustodialSeeder::class,
        'ProductSeeder',
        'ContactMethodSeeder',
        'EmploymentSeeder',
        'GenderSeeder',
        'TitleSeeder',
        'FundSourceSeeder',
        'ClientTypeSeeder',
        'CorporateInvestorTypeSeeder',
        'SettingsSeeder',
        'RolesSeeder',
        'PermissionsSeeder',
        'PortfolioSeeder',
        'CommissionRateSeeder',
        'ComplianceSeeder',
        'ClientInstructionSeeder',
        InvestmentsSeeder::class,
        RealEstateSeeder::class,
        UserSeeder::class,
        BasicPortfolioSeeder::class,
        CurrencySeeder::class,
        FundManagerSeeder::class,
        \Unitization\UnitFundTableSeeder::class
    ];

    /**
     * DatabaseSeeder constructor.
     */
    public function __construct()
    {
        if (\App::environment('local')) {
            $database = \DB::connection()->getDatabaseName();

            $this->tables =  collect(\DB::select('SHOW TABLES'))->pluck('Tables_in_'.$database)->all();
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::transaction(function () {
            $this->clean();

            foreach ($this->seeders as $seeder) {
                $this->call($seeder);
            }
        });
    }

    public function clean()
    {
        $driver = \DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME);

        if($driver != 'sqlite'){
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            foreach ($this->tables as $table) {
                DB::table($table)->truncate();
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}
