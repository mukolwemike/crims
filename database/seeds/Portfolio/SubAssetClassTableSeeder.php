<?php

namespace Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use Illuminate\Database\Seeder;

class SubAssetClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssetClass::all()->each(function ($class) {
            switch ($class->slug) {
                case 'equities':
                    $subs = [
                        ['name' => 'Active Strategy', 'slug' => 'active-strategy'],
                    ];
                    break;
                case 'deposits':
                    $subs = [
                        ['name' => 'Real Estate', 'slug' => 'real-estate'],
                        ['name' => 'Cash and Call Deposits', 'slug' => 'cash-and-call-deposits'],
                        ['name' => 'Deposits', 'slug' => 'deposit'],
                        ['name' => 'Banks', 'slug' => 'banks'],
                        ['name' => 'Private Equity', 'slug' => 'private-equity'],
                    ];
                    break;
                case 'bonds':
                    $subs = [
                        ['name' => 'Treasury Bonds', 'slug' => 'treasury-bonds'],
                        ['name' => 'Treasury Bills', 'slug' => 'treasury-bills'],
                        ['name' => 'Corporate Bonds', 'slug' => 'corporate-bonds'],
                    ];
                    break;
                default:
                    $subs = [];
                    break;
            }

            foreach($subs as $sub) {
                $subAssetClass = new SubAssetClass();
                $subAssetClass->fill($sub);
                $subAssetClass->assetClass()->associate($class);
                $subAssetClass->save();
            }
        });
    }
}
