<?php
namespace Portfolio;

use App\Cytonn\Models\PortfolioRepaymentType;
use Illuminate\Database\Seeder;


class GenPortfolioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'Interest repayment', 'slug' => 'interest_repayment'],
            ['name' => 'Principal repayment', 'slug' => 'principal_repayment'],
            ['name' => 'Repayment', 'slug' => 'full_repayment']
        ];

        foreach ($types as $type) {
            PortfolioRepaymentType::create($type);
        }
    }
}
