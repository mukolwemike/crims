<?php

namespace Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use Illuminate\Database\Seeder;

class AssetClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            ['name' => 'Equities', 'slug' => 'equities'],
            ['name' => 'Deposits', 'slug' => 'deposits'],
            ['name' => 'Bonds', 'slug' => 'bonds'],
        ];

        foreach($classes as $class) {
            AssetClass::create($class);
        }
    }
}
