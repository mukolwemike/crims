<?php

namespace Custody;

use Illuminate\Database\Seeder;

/**
 * Date: 06/05/2018
 * Time: 15:05
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

class ClientPaymentTypeSeeder extends Seeder
{

    public function run()
    {
        $types = [
            ["name" => "Funds In","slug" => "FI"],
            ["name" => "Funds Out","slug" => "FO"],
            ["name" => "Investment","slug" => "I"],
            ["name" => "Withdrawal","slug" => "W"],
            ["name" => "Transfer In","slug" => "TI"],
            ["name" => "Transfer Out","slug" => "TO"],
            ["name" => "Membership fees","slug" => "M"],
            ["name" => "Unit Fund Fees","slug" => "FEE"]
        ];

        foreach ($types as $type) {
            \App\Cytonn\Models\ClientPaymentType::create($type);
        }
    }

}