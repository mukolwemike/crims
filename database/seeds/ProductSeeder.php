<?php

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use Illuminate\Database\Seeder;

/**
 * Date: 9/2/15
 * Time: 9:17 AM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */



class ProductSeeder extends Seeder
{

    public function run()
    {
        $p = new Product();
        $p->name = 'CMS - KES';
        $p->description = 'Cash Management Solutions - KES';
        $p->currency_id = Currency::where('code', 'KES')->first()->id;
        $p->fund_manager_id = FundManager::where('name', 'CIM')->first()->id;
        $p->custodial_account_id = CustodialAccount::where('currency_id', $p->currency_id)->first()->id;
        $p->shortname = 'CMS';
        $p->longname = 'Cash Management Solutions';
        $p->save();

        $p = new Product();
        $p->name = 'CRISP';
        $p->description = 'Cytonn Regular Investment Savings Plan';
        $p->currency_id = Currency::where('code', 'KES')->first()->id;
        $p->fund_manager_id = FundManager::where('name', 'Coop')->first()->id;
        $p->custodial_account_id = CustodialAccount::where('currency_id', $p->currency_id)->first()->id;
        $p->shortname = 'CRISP';
        $p->longname = 'Cytonn Regular Investment Savings Plan';
        $p->save();
    }
}
