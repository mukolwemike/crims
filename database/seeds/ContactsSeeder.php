<?php
/**
 * Date: 8/27/15
 * Time: 5:57 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

use App\Cytonn\Models\ContactEntity;
use Illuminate\Database\Seeder;

class ContactsSeeder extends Seeder
{
    public function run()
    {
        $entities = ['individual', 'corporate'];

        foreach ($entities as $entity) {
            $e = new ContactEntity();
            $e->name = $entity;
            $e->save();
        }
    }
}
