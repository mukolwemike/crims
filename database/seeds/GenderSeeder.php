<?php

use App\Cytonn\Models\Gender;

/**
 * Date: 9/2/15
 * Time: 4:03 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */




class GenderSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        $gen = ['M', 'F'];

        foreach ($gen as $ge) {
            $g = new Gender();
            $g->abbr = $ge;
            $g->save();
        }
    }
}
