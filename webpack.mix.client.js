const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    }
});

let extract = [
    'vue', 'vuex', 'vue-router', 'element-ui', 'sweetalert', 'vue-resource', 'moment',
    'vuetable-2', 'jquery', 'lodash', 'collect.js', 'file-saver'
];

// Client
mix
    .setPublicPath('public/client')
    .js('resources/client/assets/js/app.js', 'public/client/js')
    .extract(extract)
    .sass('resources/client/assets/sass/app.scss', 'public/client/css')
    .sourceMaps();

mix.version();