// content of index.js
const http = require('axios');

let results = [];
let failed = 0;
let successes = 0;

const id = Math.floor((Math.random() * 10000) + 1);

const get = () => {
	return http.get('http://crims.local/random/'+id).then((res) => {
	    console.log(res.data);
	    results.push(res.data);
	    successes++;
	}, (res) => {
	    failed++;
        console.log('Failed: '+res)
    });
};

let promises = [];

for(let i=0; i<10; i++) {
	promises.push(get());
}

Promise.all(promises).then(() => {
    let trues = results.filter( res => res.acquired );

    let times = trues.length;
    console.log('Lock acquired '+times+' times. Failed '+failed+' times. Succeeded '+successes+' times');

    if(times === 1) {
        console.info("Test successful!");
    }

});
